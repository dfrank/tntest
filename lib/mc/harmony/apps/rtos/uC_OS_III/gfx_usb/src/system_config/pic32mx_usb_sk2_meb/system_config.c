#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "usb/usb_device.h"
#include "usb/usb_device_hid.h"




/*
*********************************************************************************************************
*                                                GLOBALS
*********************************************************************************************************
*/

/*******************************************
 *  USB Device Descriptor for this demo
 *******************************************/
const USB_DEVICE_DESCRIPTOR fullSpeedDeviceDescriptor =
{
    0x12,                       // Size of this descriptor in bytes
    USB_DESCRIPTOR_DEVICE,      // DEVICE descriptor type
    0x0200,                     // USB Spec Release Number in BCD format
    0x00,                       // Class Code
    0x00,                       // Subclass code
    0x00,                       // Protocol code
    USB_DEVICE_EP0_BUFFER_SIZE, // Max packet size for EP0, see usb_config.h
    0x04D8,                     // Vendor ID
    0x0055,                     // Product ID: HID Keyboard demo
    0x0002,                     // Device release number in BCD format
    0x01,                       // Manufacturer string index
    0x02,                       // Product string index
    0x00,                       // Device serial number string index
    0x01                        // Number of possible configurations
};

/*******************************************
 *  Device Configuration Decriptor
 *******************************************/
const uint8_t fullSpeedConfigurationDescriptor[] =
{
    /* Configuration Descriptor */
   
    0x09,                                               // Size of this descriptor in bytes
    USB_DESCRIPTOR_CONFIGURATION,                       // CONFIGURATION descriptor type
    0x29,0x00,                                          // Total length of data for this cfg
    1,                                                  // Number of interfaces in this cfg
    1,                                                  // Index value of this configuration
    0,                                                  // Configuration string index         
    USB_ATTRIBUTE_DEFAULT | USB_ATTRIBUTE_SELF_POWERED, // Attributes, see usb_device.h
    50,                                                 // Max power consumption (2X mA)

    /* Interface Descriptor */
    
    0x09,                                           // Size of this descriptor in bytes
    USB_DESCRIPTOR_INTERFACE,                       // INTERFACE descriptor type
    0,                                              // Interface Number
    0,                                              // Alternate Setting Number
    2,                                              // Number of endpoints in this interface
    USB_HID_CLASS_CODE,                             // Class code                            
    USB_HID_SUBCLASS_CODE_BOOT_INTERFACE_SUBCLASS,  // Subclass code
    USB_HID_PROTOCOL_CODE_KEYBOARD,                 // Keyboard Protocol
    0,                                              // Interface string index

    /* HID Class-Specific Descriptor */
   
    0x09,                           // Size of this descriptor in bytes
    USB_HID_DESCRIPTOR_TYPES_HID,   // HID descriptor type
    0x11,0x01,                      // HID Spec Release Number in BCD format (1.11)
    0x00,                           // Country Code (0x00 for Not supported)
    0x01,                           // Number of class descriptors, see usbcfg.h   
    USB_HID_DESCRIPTOR_TYPES_REPORT,// Report descriptor type
    0x3F,0x00,                      // Size of the report descriptor

    /* Endpoint Descriptor */

    0x07,
    USB_DESCRIPTOR_ENDPOINT,        // Endpoint Descriptor
    0x01 | USB_EP_DIRECTION_IN,     // EndpointAddress
    USB_TRANSFER_TYPE_INTERRUPT,    // Attributes
    0x40,0x00,                      // size
    0x01,                           // Interval

    /* Endpoint Descriptor */

    0x07,
    USB_DESCRIPTOR_ENDPOINT,        // Endpoint Descriptor
    0x1 | USB_EP_DIRECTION_OUT,     // EndpointAddress
    USB_TRANSFER_TYPE_INTERRUPT,    // Attributes
    0x40,0x00,                      // size
    0x01,                           // Interval
};

/**************************************
 *  String descriptors.
 *************************************/

/* Language code string descriptor 0 */
const struct
{
    uint8_t bLength;    // Length of this descriptor.
    uint8_t bDscType;   // String Descritpor type    
    uint16_t string[1]; // String                    
}
sd000 = 
{
    sizeof(sd000),
    USB_DESCRIPTOR_STRING,
    {0x0409 }
};

/* Manufacturer string descriptor 1*/
const struct
{
    uint8_t bLength;    // Length of this descriptor.        
    uint8_t bDscType;   // String Descritpor type        
    uint16_t string[25];// String                        
}
sd001 = 
{
    sizeof(sd001),
    USB_DESCRIPTOR_STRING,
    {'M','i','c','r','o','c','h','i','p',' ',
    'T','e','c','h','n','o','l','o','g','y',' ','I','n','c','.'}
};

/* Product string descriptor 2 */
const struct
{
    uint8_t bLength;    // Length of this descriptor.
    uint8_t bDscType;   // String Descritpor type    
    uint16_t string[22];// String                    
}
sd002 = 
{
    sizeof(sd002),
    USB_DESCRIPTOR_STRING,
    {'H','I','D',' ','K','e','y','b','o','a','r',
    'd',' ','D','e','m','o',' ',' ',' ',' ',' ' }
};

/******************************************************
 * Class specific descriptor - HID Report descriptor
 ******************************************************/
const uint8_t hid_rpt01[]=
{
    0x05, 0x01, // USAGE_PAGE (Generic Desktop)
    0x09, 0x06, // USAGE (Keyboard)
    0xa1, 0x01, // COLLECTION (Application)
    0x05, 0x07, // USAGE_PAGE (Keyboard)
    0x19, 0xe0, // USAGE_MINIMUM (Keyboard LeftControl)
    0x29, 0xe7, // USAGE_MAXIMUM (Keyboard Right GUI)
    0x15, 0x00, // LOGICAL_MINIMUM (0)
    0x25, 0x01, // LOGICAL_MAXIMUM (1)
    0x75, 0x01, // REPORT_SIZE (1)
    0x95, 0x08, // REPORT_COUNT (8)
    0x81, 0x02, // INPUT (Data,Var,Abs)
    0x95, 0x01, // REPORT_COUNT (1)
    0x75, 0x08, // REPORT_SIZE (8)
    0x81, 0x03, // INPUT (Cnst,Var,Abs)
    0x95, 0x05, // REPORT_COUNT (5)
    0x75, 0x01, // REPORT_SIZE (1)
    0x05, 0x08, // USAGE_PAGE (LEDs)
    0x19, 0x01, // USAGE_MINIMUM (Num Lock)
    0x29, 0x05, // USAGE_MAXIMUM (Kana)
    0x91, 0x02, // OUTPUT (Data,Var,Abs)
    0x95, 0x01, // REPORT_COUNT (1)
    0x75, 0x03, // REPORT_SIZE (3)
    0x91, 0x03, // OUTPUT (Cnst,Var,Abs)
    0x95, 0x06, // REPORT_COUNT (6)
    0x75, 0x08, // REPORT_SIZE (8)
    0x15, 0x00, // LOGICAL_MINIMUM (0)
    0x25, 0x65, // LOGICAL_MAXIMUM (101)
    0x05, 0x07, // USAGE_PAGE (Keyboard)
    0x19, 0x00, // USAGE_MINIMUM (Reserved (no event indicated))
    0x29, 0x65, // USAGE_MAXIMUM (Keyboard Application)
    0x81, 0x00, // INPUT (Data,Ary,Abs)
    0xc0        // End Collection
};

/*************************************
 *  String descriptors.
 *************************************/
USB_DEVICE_STRING_DESCRIPTORS_TABLE stringDescriptors[3]=
{
    (const uint8_t *const)&sd000,
    (const uint8_t *const)&sd001,
    (const uint8_t *const)&sd002
};

/*******************************************
 * Array of full speed config descriptors 
 *******************************************/
USB_DEVICE_CONFIGURATION_DESCRIPTORS_TABLE fullSpeedConfigDescSet[]=
{
    fullSpeedConfigurationDescriptor
};

/*******************************************
 * USB HID Function Driver Initialization 
 * Data Structure.
 *******************************************/
USB_DEVICE_HID_INIT hidInit =
{
    .hidReportDescriptorSize = sizeof(hid_rpt01),   // Size of the HID report
    .hidReportDescriptor = (uint8_t *)&hid_rpt01,   // Pointer to the report
    .queueSizeReportReceive = 1,                    // Size of receive queue size
    .queueSizeReportSend = 1,                       // Size of send queue size
};

/**************************************************
 * USB Device Layer Function Driver Registration 
 * Table
 **************************************************/
const USB_DEVICE_FUNCTION_REGISTRATION_TABLE funcRegistrationTable[1] =
{
    {
        .configurationValue = 1,                        // Configuration Descriptor Index.				
        .driver = (void*)USB_DEVICE_HID_FUNCTION_DRIVER,       // HID APIs exposed to the device layer.
        .funcDriverIndex = 0,                           // Instance index of the HID function Driver
        .funcDriverInit = &hidInit,                     // HID Initialization data for this instance
        .interfaceNumber = 0,                           // Start interface number
        .numberOfInterfaces = 1,                        // Number of interfaces
        .speed = USB_SPEED_FULL|USB_SPEED_HIGH,         // Speed at which this function should operate
    }
   
};

/*******************************************
 * USB Device Layer Master Descriptor Table 
 *******************************************/
const USB_DEVICE_MASTER_DESCRIPTOR usbMasterDescriptor =
{
    &fullSpeedDeviceDescriptor,     /* Full speed descriptor */
    1,                              /* Total number of full speed configurations available */
    &fullSpeedConfigDescSet[0],     /* Pointer to array of full speed configurations descriptors*/

    NULL,                           /* High speed device desc is not supported*/
    0,                              /* Total number of high speed configurations available */
    NULL,                           /* Pointer to array of high speed configurations descriptors. Not supported*/
    
    3,                              /* Total number of string descriptors available */
    stringDescriptors,              /* Pointer to array of string descriptors */
    
    NULL,                           /* Pointer to full speed dev qualifier. Not supported */
    NULL,                           /* Pointer to high speed dev qualifier. Not supported */
    
};

