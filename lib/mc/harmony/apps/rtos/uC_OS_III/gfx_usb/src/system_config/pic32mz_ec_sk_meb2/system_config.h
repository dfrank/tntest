#ifndef __SYSTEM_CONFIG_H
#define __SYSTEM_CONFIG_H

/* This is a temporary workaround for an issue with the peripheral library "Exists"
   functions that causes superfluous warnings.  It "nulls" out the definition of
   The PLIB function attribute that causes the warning.  Once that issue has been
   resolved, this definition should be removed. */
#define _PLIB_UNSUPPORTED

/*
*********************************************************************************************************
*                                         DEFINES 
*********************************************************************************************************
*/
#define SYS_CLK_SOURCE                      SYS_CLK_SOURCE_PRIMARY_SYSPLL
#define SYS_CLK_FREQ                        200000000ul
/*this is the speed of the clock feeding the clock source, speed of crystal
 24 MHz*/
#define SYS_CLK_CONFIG_PRIMARY_XTAL         24000000ul
#define SYS_CLK_CONFIG_SECONDARY_XTAL       200000000ul
#define SYS_CLK_CONFIG_SYSPLL_INP_DIVISOR   3
#define SYS_CLK_CONFIG_FREQ_ERROR_LIMIT     10
#define SYS_CLK_CONFIGBIT_USBPLL_ENABLE     true
#define SYS_CLK_CONFIG_USB_CLOCK            24000000ul
#define SYS_CLK_WAIT_FOR_SWITCH             true
#define SYS_CLK_KEEP_SECONDARY_OSC_ENABLED  true
#define SYS_CLK_ON_WAIT                     OSC_ON_WAIT_IDLE

#define SYS_DEVCON_SYSTEM_CLOCK         200000000
/*** Interrupt System Service Configuration ***/
#define SYS_INT                     true
/*** Timer System Service Configuration ***/
#define SYS_TMR_POWER_STATE             SYS_MODULE_POWER_RUN_FULL
#define SYS_TMR_DRIVER_INDEX            DRV_TMR_INDEX_0
#define SYS_TMR_MAX_CLIENT_OBJECTS      5
#define SYS_TMR_FREQUENCY               1000
#define SYS_TMR_FREQUENCY_TOLERANCE     10
#define SYS_TMR_UNIT_RESOLUTION         10000
#define SYS_TMR_CLIENT_TOLERANCE        10
#define SYS_TMR_INTERRUPT_NOTIFICATION  false

/*** Timer Driver Configuration ***/
#define DRV_TMR_INSTANCES_NUMBER           1
#define DRV_TMR_CLIENTS_NUMBER             1
#define DRV_TMR_INTERRUPT_MODE             true

/*** USB Driver Configuration ***/
/* Enables Device Support */
#define DRV_USB_DEVICE_SUPPORT      true
#define DRV_USB_HOST_SUPPORT        false
#define DRV_USB_INSTANCES_NUMBER    1
#define DRV_USB_INTERRUPT_MODE      true
#define DRV_USB_ENDPOINTS_NUMBER    2
#define USB_DEVICE_INSTANCES_NUMBER     1
#define USB_DEVICE_EP0_BUFFER_SIZE      64
#define USB_DEVICE_SOF_EVENT_ENABLE     true
#define USB_DEVICE_SET_DESCRIPTOR_EVENT_ENABLE
#define USB_DEVICE_SYNCH_FRAME_EVENT_ENABLE
#define USB_DEVICE_HID_INSTANCES_NUMBER     1
#define USB_DEVICE_HID_QUEUE_DEPTH_COMBINED 2

/**gfx driver configuration**/
#define GFX_USE_DISPLAY_CONTROLLER_LCC
#define DRV_GFX_CONFIG_LCC_INTERNAL_MEMORY
#define DMA_CHANNEL_INDEX                                1

/*needed by lcc Harmony driver, inform driver of LCD board specifics*/
#define PIXELCLOCK                              LATDbits.LATD5
#define PIXELCLOCK_TRIS                         TRISDbits.TRISD5
#define DATA_ENABLE                             LATBbits.LATB4
#define DATA_ENABLE_TRIS                        TRISBbits.TRISB4
#define HSYNC                                   LATBbits.LATB1
#define HSYNC_TRIS                              TRISBbits.TRISB1
#define VSYNC                                   LATAbits.LATA9
#define VSYNC_TRIS                              TRISAbits.TRISA9
#define BACKLIGHT                               LATFbits.LATF13
#define BACKLIGHT_TRIS                          TRISFbits.TRISF13
#define LCD_RESET                               LATJbits.LATJ3
#define LCD_RESET_TRIS                          TRISJbits.TRISJ3
#define LCD_CS                                  LATJbits.LATJ6
#define LCD_CS_TRIS                             TRISJbits.TRISJ6
#define ADDR19                                  LATKbits.LATK5
#define ADDR19_TRIS                             TRISKbits.TRISK5
#define DISP_ORIENTATION		                  0
#define DISP_HOR_RESOLUTION		               480
#define DISP_VER_RESOLUTION		               272
#define DISP_DATA_WIDTH			                  24
#define DISP_HOR_PULSE_WIDTH                    41
#define DISP_HOR_BACK_PORCH                     2
#define DISP_HOR_FRONT_PORCH                    2
#define DISP_VER_PULSE_WIDTH                    10
#define DISP_VER_BACK_PORCH                     2
#define DISP_VER_FRONT_PORCH                    2
#define DISP_INV_LSHIFT                         0
#define TCON_MODULE                             NULL
#define BACKLIGHT_ENABLE_LEVEL                  1
#define BACKLIGHT_DISABLE_LEVEL                 0

/*** GFX Library Configuration ***/
#define GFX_INSTANCES_NUMBER                             1
#define GFX_RUN_MODE                                     0
#define GFX_SELF_PREEMPTION_LEVEL                        0
#define GFX_CONFIG_GRADIENT_DISABLE
#define GFX_CONFIG_PALETTE_DISABLE
#define GFX_CONFIG_FONT_ANTIALIASED_DISABLE
#define GFX_CONFIG_TRANSPARENT_COLOR_DISABLE
#define GFX_CONFIG_PALETTE_EXTERNAL_DISABLE
#define GFX_CONFIG_FOCUS_DISABLE
#define GFX_CONFIG_FONT_CHAR_SIZE                        8
#define GFX_CONFIG_FONT_EXTERNAL_DISABLE
#define GFX_CONFIG_FONT_RAM_DISABLE
#define GFX_CONFIG_IMAGE_EXTERNAL_DISABLE
#define GFX_CONFIG_IMAGE_RAM_DISABLE
#define GFX_CONFIG_COLOR_DEPTH                           16
#define GFX_CONFIG_DOUBLE_BUFFERING_DISABLE
#define GFX_CONFIG_USE_KEYBOARD_DISABLE
#define GFX_CONFIG_USE_TOUCHSCREEN_DISABLE
#define GFX_malloc(size)                                 malloc(size)
#define GFX_free(pObj)                                   free(pObj)




#endif	/* SYSTEM_CONFIG_H */

