#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <sys/attribs.h>
#include "system_config.h"
#include "driver/usb/drv_usb.h"
#include "system/system.h"
#include "usb/usb_device.h"
#include "usb/usb_device_hid.h"
#include "driver/tmr/drv_tmr.h"
#include "system/clk/sys_clk.h"
#include "system/tmr/sys_tmr.h"
#include "system_definitions.h"
#include "app_keyboard.h"
#include "app.h"
#include "driver/gfx/controller/lcc/drv_gfx_lcc.h"

/*
*********************************************************************************************************
*                                                PROTOTYPES
*********************************************************************************************************
*/
void DMAInterruptHandler(void);
void Timer2InterruptHandler ( void );
void USBGeneralEventInterruptHandler ( void );
void InterruptHandler_USBDMA ( void );

/*
*********************************************************************************************************
*                             USBGeneralEventInterruptHandler
*
* Description : Calls the USB driver Tasks function in Harmony Framework.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : called from USB vector interrupt wrapper, see system_interrupt_a.S
*
* Note(s)     : none.
*********************************************************************************************************
*/
void USBGeneralEventInterruptHandler ( void )
{
   DRV_USB_Tasks_ISR(usbDevObject);
}



/*
*********************************************************************************************************
*                                          DMA Interrupt Handler()
*
* Description : Calls the appropriate Harmony GFX stack functions needed, based on how we configured this 
*               system.  This function is called in response to a hardware DMA interrupt.  In this system
*               we are using DMA Channel 1, see system_config.h and system_init.c.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : Assembly Interrupt vector wrapper, see system_interrupt_a.S
*
* Note(s)     : none.
*********************************************************************************************************
*/
void DMAInterruptHandler(void)
{
    DRV_GFX_LCC_DisplayRefresh();
    PLIB_INT_SourceFlagClear(INT_ID_0, INT_SOURCE_DMA_1);
}

/*
*********************************************************************************************************
*                                   Timer2InterruptHandler
*
* Description : Calls the Timer2 driver in Harmony Framework to get work done.  This is needed by the
*               USB stack.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : called from interrupt vector, see system_interrupt_a.S
*
* Note(s)     : none.
*********************************************************************************************************
*/
void Timer2InterruptHandler ( void )
{
   DRV_TMR_Tasks_ISR(tmrDevObject);
}

/*
*********************************************************************************************************
*                                   InterruptHandler_USBDMA
*
* Description : Calls the USB DMA Device Task in Harmony Framework to get work done.  This is needed by 
*               the USB stack.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : called from interrupt vector, see system_interrupt_a.S
*
* Note(s)     : none.
*********************************************************************************************************
*/
void InterruptHandler_USBDMA ( void )
{
   USB_DEVICE_Tasks_ISR_USBDMA(usbDevObject);
}