/*
*********************************************************************************************************
*                                                INCLUDES
*********************************************************************************************************
*/
#include    <xc.h>
#include    "os.h"
#include    "system_config.h"
#include    "bsp.h"
#include    "peripheral/int/plib_int.h"
#include    "peripheral/devcon/plib_devcon.h"
#include    "driver/gfx/controller/ssd1926/drv_gfx_ssd1926.h"
#include    "usb/usb_device.h"
#include    "usb/usb_device_hid.h"
#include    "gfx/gfx.h"
#include    "app_keyboard.h"
#include    "app.h"
/*
*********************************************************************************************************
*                                                CONFIGURATION WORDS
*********************************************************************************************************
*/

/*** DEVCFG0 ***/
#pragma config DEBUG =      OFF
#pragma config ICESEL =     ICS_PGx2
#pragma config PWP =        OFF
#pragma config BWP =        OFF
#pragma config CP =         OFF

/*** DEVCFG1 ***/
#pragma config FNOSC =      PRIPLL
#pragma config FSOSCEN =    OFF
#pragma config IESO =       OFF
#pragma config POSCMOD =    HS
#pragma config OSCIOFNC =   OFF
#pragma config FPBDIV =     DIV_2
#pragma config FCKSM =      CSDCMD
#pragma config WDTPS =      PS1048576
#pragma config FWDTEN =     OFF

/*** DEVCFG2 ***/
#pragma config FPLLIDIV =   DIV_2
#pragma config FPLLMUL =    MUL_20
#pragma config FPLLODIV =   DIV_1
#pragma config UPLLIDIV =   DIV_2
#pragma config UPLLEN =     ON

/*** DEVCFG3 ***/
#pragma config USERID =     0xffff
#pragma config FSRSSEL =    PRIORITY_7
#pragma config FMIIEN =     OFF
#pragma config FETHIO =     OFF
#pragma config FCANIO =     OFF
#pragma config FUSBIDIO =   OFF
#pragma config FVBUSONIO =  OFF

/*
*********************************************************************************************************
*                                                VARIABLES
*********************************************************************************************************
*/
/* System Module Handles */
SYS_MODULE_OBJ usbDevObject;
SYS_MODULE_OBJ  gfxObjectHandle;

OS_TCB      SystemDeviceTask_TCB;
CPU_STK     SystemDeviceTask_Stk[SYSTEM_DEVICETASK_STKSIZE];

/****************************************************
 * Endpoint Table needed by the Device Layer.
 ****************************************************/
uint8_t __attribute__((aligned(512))) endPointTable[USB_DEVICE_ENDPOINT_TABLE_SIZE];

extern const      USB_DEVICE_FUNCTION_REGISTRATION_TABLE funcRegistrationTable[1];
extern const      USB_DEVICE_MASTER_DESCRIPTOR usbMasterDescriptor;
//GFX_INIT          gfxInit;


const USB_DEVICE_INIT usbDevInitData =
{
   /* System module initialization */
   .moduleInit = SYS_MODULE_POWER_RUN_FULL,

   /* Identifies peripheral (PLIB-level) ID */
   .usbID = USB_ID_1,

   /* Stop in idle */
   .stopInIdle = false,

   /* Suspend in sleep */
   .suspendInSleep = false,

   /* Interrupt Source for USB module */
   .interruptSource = INT_SOURCE_USB_1,

   /* Endpoint table */
   .endpointTable= endPointTable,

   /* Number of function drivers registered to this instance of the
       USB device layer */
   .registeredFuncCount = 1,

   /* Function driver table registered to this instance of the USB device layer*/
   .registeredFunctions = (USB_DEVICE_FUNCTION_REGISTRATION_TABLE*)funcRegistrationTable,

   /* Pointer to USB Descriptor structure */
   .usbMasterDescriptor = (USB_DEVICE_MASTER_DESCRIPTOR*)&usbMasterDescriptor,

   /* USB Device Speed */
   .deviceSpeed = USB_SPEED_FULL,
};

/*** GFX Initialization Data ***/

DRV_PMP_INIT     pmpInit =
{
    .polarity.addressLatchPolarity = PMP_POLARITY_ACTIVE_HIGH,
    .polarity.rwStrobePolarity = PMP_POLARITY_ACTIVE_LOW,
    .polarity.writeEnableStrobePolarity = PMP_POLARITY_ACTIVE_LOW,
    .polarity.chipselect1Polarity = PMP_POLARITY_ACTIVE_HIGH,
    .polarity.chipselect2Polarity = PMP_POLARITY_ACTIVE_LOW,
    .ports.readWriteStrobe = PORT_ENABLE,
    .ports.writeEnableStrobe = PORT_ENABLE,
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .pmpID            = PMP_ID_0,
    .stopInIdle       = false,
    .muxMode          = PMP_MUX_NONE
};

/*** GFX Initialization Data ***/

GFX_INIT gfxInit0 =
{
    .drvInitialize    = DRV_GFX_SSD1926_Initialize,
    .drvOpen          = DRV_GFX_SSD1926_Open,
    .drvInterfaceSet  = DRV_GFX_SSD1926_InterfaceSet,
    .driverInitData.orientation             = DISP_ORIENTATION,
    .driverInitData.horizontalResolution    = DISP_HOR_RESOLUTION,
    .driverInitData.verticalResolution      = DISP_VER_RESOLUTION,
    .driverInitData.dataWidth               = DISP_DATA_WIDTH,
    .driverInitData.horizontalPulseWidth    = DISP_HOR_PULSE_WIDTH,
    .driverInitData.horizontalBackPorch     = DISP_HOR_BACK_PORCH,
    .driverInitData.horizontalFrontPorch    = DISP_HOR_FRONT_PORCH,
    .driverInitData.verticalPulseWidth      = DISP_VER_PULSE_WIDTH,
    .driverInitData.verticalBackPorch       = DISP_VER_BACK_PORCH,
    .driverInitData.verticalFrontPorch      = DISP_VER_FRONT_PORCH,
    .driverInitData.logicShift              = DISP_INV_LSHIFT,
    .driverInitData.LCDType                 = 1,
    .driverInitData.colorType               = 0,
    .driverInitData.TCON_Init               = TCON_MODULE,
};


/*
*********************************************************************************************************
*                                          SYS_Initialize
*
* Description : This routine initializes the board, services, drivers, application and other modules as
*               configured at build time.
* Caller      : called from main
*********************************************************************************************************
*/
void SYS_Initialize ( void * data )
{
   OS_ERR os_err;
   
   /*initialize RTOS */   
   CPU_Init();
   OSInit(&os_err);
   
   /*setup device to run at maximum performance*/
   SYS_DEVCON_PerformanceConfig(80000000);
   
   /* Initialize the BSP */
   BSP_InitIO( );
   
   /* Initialize the USB device layer */
   usbDevObject = USB_DEVICE_Initialize (USB_DEVICE_INDEX_0 ,
                  ( SYS_MODULE_INIT* ) & usbDevInitData);

   DRV_PMP_Initialize (DRV_PMP_INDEX_0, (SYS_MODULE_INIT *)&pmpInit);

   /* Initialize GFX Library */
   gfxObjectHandle = GFX_Initialize(GFX_INDEX_0, (SYS_MODULE_INIT *)&gfxInit0);

   PLIB_INT_MultiVectorSelect(INT_ID_0);
   
   /*setup rtos tick interrupt source, for 1ms tick*/
   __builtin_mtc0(11,0,40000);
   /*start counting from zero*/
   __builtin_mtc0(9,0,0);
   /*clear interrupt flag, before enabling*/
   PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_CORE);
   /*enable interrupt source and set IPL for core timer*/
   PLIB_INT_VectorPrioritySet(INT_ID_0,INT_VECTOR_CT,INT_PRIORITY_LEVEL4);
   PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_TIMER_CORE);
     
   /* Set priority for USB interrupt source */
   PLIB_INT_VectorPrioritySet(INT_ID_0,INT_VECTOR_USB1,INT_PRIORITY_LEVEL2);

   /*create system_tasks*/
   OSTaskCreate((OS_TCB      *)&SystemDeviceTask_TCB,
                (CPU_CHAR    *)"Sys Device Task",
                (OS_TASK_PTR  )SystemDeviceTask,
                (void        *)0,
                (OS_PRIO      )SYSTEM_DEVICETASK_PRIO,
                (CPU_STK     *)&SystemDeviceTask_Stk[0],
                (CPU_STK_SIZE )SYSTEM_DEVICETASK_STKSIZE_LIMIT,
                (CPU_STK_SIZE )SYSTEM_DEVICETASK_STKSIZE,
                (OS_MSG_QTY   )0u,
                (OS_TICK      )0u,
                (void        *)0,
                (OS_OPT       )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                (OS_ERR      *)&os_err);

   /*initialize application specific items*/
   ApplicationInitialize();

  
}
