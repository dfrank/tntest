#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_config.h"
#include "system/system.h"
#include "usb/usb_device.h"
#include "usb/usb_device_hid.h"
#include "gfx/gfx_primitive.h"
#include "gfx/gfx.h"
#include "app_keyboard.h"
#include "app.h"
#include "bsp.h"
#include "os.h"

/*
*********************************************************************************************************
*                                                DEFINES
*********************************************************************************************************
*/
#define MAXSCREENCOLORS             7

/*
*********************************************************************************************************
*                                                GLOBALS
*********************************************************************************************************
*/
APP_DATA                   appData;
unsigned short             ScreenColor[MAXSCREENCOLORS] = {BRIGHTBLUE,BRIGHTGREEN,BRIGHTCYAN,BRIGHTRED,
                                                           BRIGHTMAGENTA,BRIGHTYELLOW,LIGHTGRAY};
/*Keyboard Report to be transmitted*/
extern KEYBOARD_INPUT_REPORT __attribute__((coherent, aligned(4))) keyboardInputReport ;
/* Keyboard output report */
extern KEYBOARD_OUTPUT_REPORT __attribute__((coherent, aligned(4))) keyboardOutputReport;
/*
*********************************************************************************************************
*                                          ApplicationUSBDevcieTask
*
* Description : USB Device Task calls the USB Device task routine and application tasks routines.
* Arguments   : p_arg   is the argument passed to 'ApplicationUSBDeviceTask()' by the OS through the RTOS
*               task create function.
*********************************************************************************************************
*/
void  ApplicationUSBDeviceTask (void *p_arg)
{
   OS_ERR   err;
   while (1)
   {
      /* Call the application's tasks routine */
      switch(appData.state)
      {
         case APP_STATE_INIT:

            /* Open an instance of the device layer */
            appData.deviceHandle = USB_DEVICE_Open( USB_DEVICE_INDEX_0,
                    DRV_IO_INTENT_READWRITE );

            if(appData.deviceHandle == USB_DEVICE_HANDLE_INVALID)
            {
                break;
            }

            /* Register a callback with device layer to get
             * event notification (for end point 0) */
            USB_DEVICE_EventHandlerSet(appData.deviceHandle, ApplicationUSBDeviceEventHandler, 0);

            appData.state = APP_STATE_WAIT_FOR_CONFIGURATION;
            break;
         case APP_STATE_WAIT_FOR_CONFIGURATION:

            /* Check if the device is configured. The
             * isConfigured flag is updated in the
             * Device Event Handler */
            if(appData.isConfigured)
            {
               /* Initialize the flag and place a request for a
                * output report */
               appData.isReportReceived = false;
               USB_DEVICE_HID_ReportReceive(appData.hidInstance,
                       &appData.receiveTransferHandle,
                       (uint8_t *)&keyboardOutputReport,1);
               appData.state = APP_STATE_CHECK_IF_CONFIGURED;
            }
            break;
         case APP_STATE_CHECK_IF_CONFIGURED:

            /* This state is needed because the device can get
             * unconfigured asynchronously. Any application state
             * machine reset should happen within the state machine
             * context only. */

            if(appData.isConfigured)
            {
               appData.state = APP_STATE_SWITCH_PROCESS;
            }
            else
            {
               /* This means the device got de-configured.
                * We reset the state and the wait for configuration */
               appData.isReportReceived = false;
               appData.isReportSentComplete = true;
               appData.key = USB_HID_KEYBOARD_KEYPAD_KEYBOARD_A;
               appData.keyboardModifierKeys.modifierkeys = 0;
               keyboardOutputReport.data = 0;
               appData.isSwitchPressed = false;
               appData.ignoreSwitchPress = false;

               appData.state = APP_STATE_WAIT_FOR_CONFIGURATION;
            }
            break;
         case APP_STATE_SWITCH_PROCESS:

            appData.isSwitchPressed = false;

            /* Check if the switch was pressed, this state machine must be
            called quickly enough to catch the user pressing the switch. This
            could lead to problems if not implemented correctly.*/
            if(BSP_ReadSwitch(SWITCH_3) == BSP_SWITCH_STATE_PRESSED)
            {
               /*pause the task for 200ms, to ensure user has let go of switch,
               if switch is still pressed, stay here until switch is let go.  If
               user presses switch and holds on, worst case the usb task will not
               progess thorough the state machine, but rest of system will be
               functional.*/
               do
               {
                  OSTimeDly(100,OS_OPT_TIME_DLY,&err);
               }while(BSP_ReadSwitch(SWITCH_3) == BSP_SWITCH_STATE_PRESSED);

               /* get here, this means this is a valid switch press */
               appData.isSwitchPressed = true;
            }
            /*go to the next state. */
            appData.state = APP_STATE_CHECK_FOR_OUTPUT_REPORT;
            break;
         case APP_STATE_CHECK_FOR_OUTPUT_REPORT:
            appData.state = APP_STATE_EMULATE_KEYBOARD;

            /*was a report receieved*/
            if(!appData.isReportReceived)
               break;

            /* Get here report received, update the LED, schedule and request*/
            if(keyboardOutputReport.ledState.numLock == KEYBOARD_LED_STATE_ON)
            {
               BSP_SwitchONLED(LED_2);
            }
            else
            {
               BSP_SwitchOFFLED(LED_2);
            }

            if(keyboardOutputReport.ledState.capsLock == KEYBOARD_LED_STATE_ON)
            {
               BSP_SwitchONLED(LED_3);
            }
            else
            {
               BSP_SwitchOFFLED(LED_3);
            }
            appData.isReportReceived = false;
            USB_DEVICE_HID_ReportReceive(appData.hidInstance,
                    &appData.receiveTransferHandle,
                    (uint8_t *)&keyboardOutputReport,1);
            break;
         case APP_STATE_EMULATE_KEYBOARD:
            /*go to next state no matter what..*/
            appData.state = APP_STATE_CHECK_IF_CONFIGURED;

            /*only process if report is complete*/
            if(!appData.isReportSentComplete)
               break;

            /*Get here, this means report can be sent, Emulate keyboard*/
            if(appData.isSwitchPressed)
            {
               /* Clear the switch pressed flag */
               appData.isSwitchPressed = false;

               if(appData.key == USB_HID_KEYBOARD_KEYPAD_KEYBOARD_RETURN_ENTER)
               {
                  appData.key = USB_HID_KEYBOARD_KEYPAD_KEYBOARD_A;
               }
               appData.keyCodeArray.keyCode[0] = appData.key;
               /* If the switch was pressed, update the key counter and then
                * add the key to the keycode array. */
               appData.key ++;
            }
            else
            {
                /* Indicate no event */
               appData.keyCodeArray.keyCode[0] =
                  USB_HID_KEYBOARD_KEYPAD_RESERVED_NO_EVENT_INDICATED;
            }

            KEYBOARD_InputReportCreate(&appData.keyCodeArray,
               &appData.keyboardModifierKeys, &keyboardInputReport);

            /*clear report complete for next time through*/
            appData.isReportSentComplete = false;

            USB_DEVICE_HID_ReportSend(appData.hidInstance,
                &appData.sendTransferHandle,
                (uint8_t *)&keyboardInputReport,
                sizeof(KEYBOARD_INPUT_REPORT));
            break;
         case APP_STATE_ERROR:
             break;
         default:
             break;
      }
      /* Sleep for 50ms*/
      OSTimeDly(50,OS_OPT_TIME_DLY,&err);
   }
}

/*                                                                                                       
*********************************************************************************************************
*                                          DisplayTask()                                               
*                                                                                                        
* Description : Application task, queues up writes to the LCD screen.  In this case it just changes the 
*               screen color every 500mS.  More complex gfx work can be added here.
*                                                                                                        
* Argument(s) : none                                                                                     
*                                                                                                        
* Return(s)   : none                                                                                     
*                                                                                                        
* Caller(s)   : OS called after waiting time delay()                                                                           
*                                                                                                        
* Note(s)     : none.                                                                                    
*********************************************************************************************************
*/
void  ApplicationDisplayTask (void *p_arg)
{
   OS_ERR  err;
   static unsigned int ScreenColor_Index = 0;
   CPU_SR_ALLOC();
   
   while (1)
   {
      CPU_INT_DIS();
      GFX_ColorSet(GFX_INDEX_0, ScreenColor[ScreenColor_Index++]);
      GFX_ScreenClear(0);
      CPU_INT_EN();
      
      if(ScreenColor_Index == MAXSCREENCOLORS)
         ScreenColor_Index = 0;

      /*delay task to give time back to CPU.*/
      OSTimeDly(500,OS_OPT_TIME_DLY,&err);
   }
}

/*                                                                                                       
*********************************************************************************************************
*                                          LEDblinkTask()                                               
*                                                                                                        
* Description : Blinks LED once every 500ms.                                                                
*                                                                                                        
* Argument(s) : none                                                                                     
*                                                                                                        
* Return(s)   : none                                                                                     
*                                                                                                        
* Caller(s)   : OS called after waiting time delay()                                                                           
*                                                                                                        
* Note(s)     : none.                                                                                    
*********************************************************************************************************
*/  
void ApplicationLEDblinkTask (void *p_arg)
{
   OS_ERR   err;
   while (1) /* Task body, always written as an infinite loop.       */
   {
      LED_Toggle();
      OSTimeDly(500,OS_OPT_TIME_DLY,&err);
   }
}