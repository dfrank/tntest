#ifndef __APP_H
#define	__APP_H
/*
*********************************************************************************************************
*                                            TASK PRIORITIES
*********************************************************************************************************
*/

#define  APPLICATION_LEDBLINKTASK_PRIO                   5u
#define  APPLICATION_USBDEVICETASK_PRIO                  3u
#define  APPLICATION_DISPLAYTASK_PRIO                    6u
#define  SYSTEM_DEVICETASK_PRIO                          4u

/*
*********************************************************************************************************
*                                            TASK STACK SIZES
*********************************************************************************************************
*/
#define  APPLICATION_LEDBLINKTASK_STKSIZE                1024u
#define  APPLICATION_USBDEVICETASK_STKSIZE               1024u
#define  APPLICATION_DISPLAYTASK_STKSIZE                 1024u
#define  SYSTEM_DEVICETASK_STKSIZE                       1024u


/*
*********************************************************************************************************
*                                            TASK STACK SIZES LIMIT
*********************************************************************************************************
*/

#define  APPLICATION_LEDBLINKTASK_STKSIZE_PCT_FULL        90u
#define  APPLICATION_LEDBLINKTASK_STKSIZE_LIMIT  ((APPLICATION_LEDBLINKTASK_STKSIZE * (100u - APPLICATION_LEDBLINKTASK_STKSIZE_PCT_FULL))   / 100u)

#define  APPLICATION_DISPLAYTASK_STKSIZE_PCT_FULL         90u
#define  APPLICATION_DISPLAYTASK_STKSIZE_LIMIT   ((APPLICATION_DISPLAYTASK_STKSIZE * (100u - APPLICATION_DISPLAYTASK_STKSIZE_PCT_FULL))   / 100u)

#define  APPLICATION_USBDEVICETASK_PCT_FULL               90u
#define  APPLICATION_USBDEVICETASK_STKSIZE_LIMIT ((APPLICATION_USBDEVICETASK_STKSIZE * (100u - APPLICATION_USBDEVICETASK_PCT_FULL))   / 100u)

#define  SYSTEM_DEVICETASK_PCT_FULL                    90u
#define  SYSTEM_DEVICETASK_STKSIZE_LIMIT      ((SYSTEM_DEVICETASK_STKSIZE * (100u - SYSTEM_DEVICETASK_PCT_FULL))   / 100u)

/*
*********************************************************************************************************
*                                         ENUM, UNION, STRUCTURES
*********************************************************************************************************
*/
typedef enum
{
   /* Application opens the device layer in this state */
   APP_STATE_INIT,

   /* Application waits for configuration in this state */
   APP_STATE_WAIT_FOR_CONFIGURATION,

   /* Application checks if an output report is available */
   APP_STATE_CHECK_FOR_OUTPUT_REPORT,

   /* Application updates the switch states */
   APP_STATE_SWITCH_PROCESS,

   /* Application checks if it is still configured*/
   APP_STATE_CHECK_IF_CONFIGURED,

   /* Application emulates keyboard */
   APP_STATE_EMULATE_KEYBOARD,

   /* Application error state */
   APP_STATE_ERROR
}APP_STATES;


typedef struct
{
    /* The application's current state */
    APP_STATES state;

    /* Handle to the device layer */
    USB_DEVICE_HANDLE deviceHandle;
 
    /* Application HID instance */
    USB_DEVICE_HID_INDEX hidInstance;

    /* Keyboard modifier keys*/
    KEYBOARD_MODIFIER_KEYS keyboardModifierKeys;

    /* Key code array*/
    KEYBOARD_KEYCODE_ARRAY keyCodeArray;

    /* Is device configured */
    bool isConfigured;

    /* Switch state*/
    bool ignoreSwitchPress;

    /* Tracks switch press*/
    bool isSwitchPressed;
    
    /* Track the send report status */
    bool isReportSentComplete;

    /* Track if a report was received */
    bool isReportReceived;

    /* USB HID current Idle */
    uint8_t idleRate;

    /* Flag determines SOF event occurrence */
    bool sofEventHasOccurred;

    /* Receive transfer handle */
    USB_DEVICE_HID_TRANSFER_HANDLE receiveTransferHandle;

    /* Send transfer handle */
    USB_DEVICE_HID_TRANSFER_HANDLE sendTransferHandle;

    /* Keycode to be sent */
    USB_HID_KEYBOARD_KEYPAD key;

    /* USB HID active Protocol */
    USB_HID_PROTOCOL_CODE activeProtocol;

    /* Switch debounce timer */
    unsigned int switchDebounceTimer;

} APP_DATA;

/*
*********************************************************************************************************
*                                            PROTOTYPES
*********************************************************************************************************
*/
void ApplicationInitialize ( void );
void ApplicationUSBDeviceTask (void *p_arg);
void ApplicationDisplayTask (void *p_arg);
void ApplicationLEDblinkTask (void* p_arg);
USB_DEVICE_HID_EVENT_RESPONSE ApplicationUSBDeviceHIDEventHandler(
    USB_DEVICE_HID_INDEX hidInstance,
    USB_DEVICE_HID_EVENT event,
    void * eventData,
    uintptr_t userData);
void ApplicationUSBDeviceEventHandler(USB_DEVICE_EVENT events,
   void * pData, 
   uintptr_t context);
#endif	/* APP_H */

