#include <xc.h>
#include "bsp.h"



/*
*********************************************************************************************************
*                                              LED TOGGLE
*
* Description : This function is used to alternate the state of an LED
*
*********************************************************************************************************
*/
void  LED_Toggle (void)
{
   PORTDINV = 0x00000007;
}

/*
*********************************************************************************************************
*                                             BSP_InitIO()
* 
* Description: Initialize all the I/O devices.
*
* Arguments  : None
*
* Returns    : None
*********************************************************************************************************
*/

void  BSP_InitIO (void)    
{
   /* Set up the ports to digital */
   AD1PCFG = 0x0000801A;
  
   /* Set the LED pins as output */    
   TRISDCLR = 0x00000007;

   /* Switch off all the LEDS */ 
   LATDCLR = 0x00000007;

   /* Enable pullups on the Switch ports*/
   CNPUESET = 0x0009906C;
   
   /*configure external interrupt controller for multi-vector mode*/
   INTCONSET = 0x1000;
   /*setup CP0 Compare register for period which yields a 1ms tick for rtos*/
   __builtin_mtc0(11,0,40000);
   /*start counting from zero*/
   __builtin_mtc0(9,0,0);

   /*clear interrupt flag*/
   IFS0CLR = 0x00000001;
   /*set interrupt enable bit for core timer, which sources rtos tick*/
   IEC0SET = 0x00000001;
   /*set interrupt priority (IPL) to 3*/
   IPC0SET = 0x0000000C;
}

