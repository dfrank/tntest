#include <xc.h>
#include "bsp.h"

/*
*********************************************************************************************************
*                                              LED TOGGLE
*
* Description : This function is used to alternate the state of an LED
*
*********************************************************************************************************
*/
void  LED_Toggle (void)
{
   PORTHINV = 0x00000007;
}

/*
*********************************************************************************************************
*                                             BSP_InitIO()
* 
* Description: Initialize all the I/O devices.
*
* Arguments  : None
*
* Returns    : None
*********************************************************************************************************
*/
void  BSP_InitIO (void)    
{
   
   /*configure pins as digital*/
   ANSELHCLR = 0x00000007;
   /*set LED pins as output*/
   TRISHCLR = 0x00000007;
   /*set LEDS off to begin with*/
   PORTHCLR = 0x00000007;

   /*set pins for digital mode*/
   ANSELBCLR = 0x00007000;
   /* set the switch pins as input */
   TRISBSET = 00007000;
   /*enable internal pullups on pin.*/
   CNPUB = 0x00007000;

   /*configure external interrupt controller for multi-vectored mode*/
   INTCONSET = 0x1000;

   /*setup CP0 Compare register for period which yields a 1ms tick for rtos*/
   __builtin_mtc0(11,0,100000);
   /*start counting from zero*/
   __builtin_mtc0(9,0,0);
   /*clear core timer interrupt flag*/
   IFS0CLR = 0x00000001;
   /*set interrupt enable bit for core timer, which sources rtos tick*/
   IEC0SET = 0x00000001;
   /*set core timer interrupt priority (IPL) to 3*/
   IPC0SET = 0x0000000C;
}
