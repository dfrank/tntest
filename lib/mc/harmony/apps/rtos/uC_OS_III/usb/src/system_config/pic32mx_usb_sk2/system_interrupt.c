#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "driver/usb/drv_usb.h"
#include "system/system.h"
#include "usb/usb_device.h"
#include "usb/usb_device_hid.h"

/*
*********************************************************************************************************
*                                                VARIABLES
*********************************************************************************************************
*/
extern SYS_MODULE_OBJ usbDevObject;

/*
*********************************************************************************************************
*                                            FUNCTION PROTOTYPES
*********************************************************************************************************
*/
void USBGeneralEventInterruptHandler ( void );


/*
*********************************************************************************************************
*                             USBGeneralEventInterruptHandler
*
* Description : Calls the USB driver Tasks function in Harmony Framework.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : called from USB vector interrupt wrapper, see system_interrupt_a.S
*
* Note(s)     : none.
*********************************************************************************************************
*/
void USBGeneralEventInterruptHandler ( void )
{
   USB_DEVICE_Tasks_ISR(usbDevObject);
}
