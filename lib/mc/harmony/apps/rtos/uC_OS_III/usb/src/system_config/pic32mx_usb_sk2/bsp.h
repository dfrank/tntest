/*
*********************************************************************************************************
*                                                DEFINES
*********************************************************************************************************
*/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "peripheral/ports/plib_ports.h"


/******************************************************************************/


// *****************************************************************************
/* Digital Pin Constant

  Summary:
    Defines the constant which identifies digital pin

  Description:
    This constant identifies digital pin
*/

#define BSP_DIGITAL_PIN                         PORTS_PIN_MODE_DIGITAL

// *****************************************************************************
/* analog Pin Constant

  Summary:
    Defines the constant which identifies analog pin

  Description:
    This constant identifies analog pin
*/

#define BSP_ANALOG_PIN                          PORTS_PIN_MODE_ANALOG


// *****************************************************************************
/* BSP Switch.

  Summary:
    Holds Switch numbers.

  Description:
    This enumeration defines the Switch numbers.

  Remarks:
    None.
*/

typedef enum
{
    /* SWITCH 1 */
     SWITCH_1 = PORTS_BIT_POS_6,

    /* SWITCH 2 */
     SWITCH_2 = PORTS_BIT_POS_7,

    /* SWITCH 3 */
     SWITCH_3 = PORTS_BIT_POS_13

}BSP_SWITCH;


// *****************************************************************************
/* BSP Switch state.

  Summary:
    Holds Switch status.

  Description:
    This enumeration defines the switch state.

  Remarks:
    None.
*/

typedef enum
{
    /* Switch pressed */
    BSP_SWITCH_STATE_PRESSED = 0,

   /* Switch not pressed */
    BSP_SWITCH_STATE_RELEASED = 1
}BSP_SWITCH_STATE;


// *****************************************************************************
/* LED Number.

  Summary:
    Holds LED numbers.

  Description:
    This enumeration defines the LED numbers.

  Remarks:
    None.
*/
#define LED_1              0
#define LED_2              1
#define LED_3              2
/*
*********************************************************************************************************
*                                             PROTOTYPES
*********************************************************************************************************
*/    
BSP_SWITCH_STATE BSP_ReadSwitch( BSP_SWITCH bspSwitch );
void  LED_Toggle (void);
void  BSP_InitIO          (void);
void BSP_SwitchONLED(unsigned int led);
void BSP_SwitchOFFLED(unsigned int led);


