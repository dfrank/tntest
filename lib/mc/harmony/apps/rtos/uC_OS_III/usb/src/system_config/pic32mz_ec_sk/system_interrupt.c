#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <sys/attribs.h>
#include "system_config.h"
#include "driver/usb/drv_usb.h"
#include "system/system.h"
#include "usb/usb_device.h"
#include "usb/usb_device_hid.h"
#include "driver/tmr/drv_tmr.h"
#include "system/clk/sys_clk.h"
#include "system/tmr/sys_tmr.h"
#include "app_keyboard.h"
#include "app.h"
#include "system_definitions.h"

/*
*********************************************************************************************************
*                             USBGeneralEventInterruptHandler
*
* Description : Calls the USB driver Tasks function in Harmony Framework.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : called from USB vector interrupt wrapper, see system_interrupt_a.S
*
* Note(s)     : none.
*********************************************************************************************************
*/
void USBGeneralEventInterruptHandler ( void )
{
   DRV_USB_Tasks_ISR((SYS_MODULE_OBJ)usbDevObject);
}

/*
*********************************************************************************************************
*                                   Timer2InterruptHandler
*
* Description : Calls the Timer2 driver in Harmony Framework to get work done.  This is needed by the
*               USB stack.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : called from interrupt vector, see system_interrupt_a.S
*
* Note(s)     : none.
*********************************************************************************************************
*/
void Timer2InterruptHandler ( void )
{
   DRV_TMR_Tasks_ISR(tmrDevObject);
}

void InterruptHandler_USBDMA ( void )
{
   USB_DEVICE_Tasks_ISR_USBDMA(usbDevObject);
}