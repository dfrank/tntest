#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_config.h"
#include "system/system.h"
#include "usb/usb_device.h"
#include "usb/usb_device_hid.h"
#include "peripheral/devcon/plib_devcon.h"
#include "peripheral/int/plib_int.h"
#include "peripheral/osc/plib_osc.h"
#include "app_keyboard.h"
#include "app.h"
#include "bsp.h"
#include "system_definitions.h"
#include "os.h"

/*
*********************************************************************************************************
*                                                CONFIGURATION WORDS
* Description : Sets the fuses during programming of the device.  This will cause the hardware to come up
*               in a preconfigured state.  
*               We set the CPU clock to 80MHz and use the SystemPLL output as the main clock source.  
*               The input is feed from the crystal at approximately 8MHz.
*********************************************************************************************************
*/
/*** DEVCFG0 ***/
#pragma config DEBUG =      OFF
#pragma config ICESEL =     ICS_PGx2
#pragma config PWP =        0xff
#pragma config BWP =        OFF
#pragma config CP =         OFF

/*** DEVCFG1 ***/
#pragma config FNOSC =      PRIPLL
#pragma config FSOSCEN =    OFF
#pragma config IESO =       OFF
#pragma config POSCMOD =    XT
#pragma config OSCIOFNC =   OFF
#pragma config FPBDIV =     DIV_1
#pragma config FCKSM =      CSDCMD
#pragma config WDTPS =      PS1048576
#pragma config FWDTEN =     OFF
          
/*** DEVCFG2 ***/
#pragma config FPLLIDIV =   DIV_2
#pragma config FPLLMUL =    MUL_20
#pragma config FPLLODIV =   DIV_1
#pragma config UPLLIDIV =   DIV_2
#pragma config UPLLEN =     ON

/*** DEVCFG3 ***/
#pragma config USERID =     0xffff
#pragma config FSRSSEL =    PRIORITY_7
#pragma config FMIIEN =     OFF
#pragma config FETHIO =     OFF
#pragma config FCANIO =     OFF
#pragma config FUSBIDIO =   OFF
#pragma config FVBUSONIO =  OFF

/*
*********************************************************************************************************
*                                                VARIABLES
*********************************************************************************************************
*/
OS_TCB      SystemUSBDeviceTask_TCB;
CPU_STK     SystemUSBDeviceTask_Stk[SYSTEM_USBDEVICETASK_STKSIZE];

SYS_MODULE_OBJ usbDevObject;


/****************************************************
 * Endpoint Table needed by the Device Layer.
 ****************************************************/
uint8_t __attribute__((aligned(512))) endPointTable[USB_DEVICE_ENDPOINT_TABLE_SIZE];

extern const USB_DEVICE_FUNCTION_REGISTRATION_TABLE funcRegistrationTable[1];
extern const USB_DEVICE_MASTER_DESCRIPTOR usbMasterDescriptor;

const USB_DEVICE_INIT usbDevInitData =
{
   /* System module initialization */
   .moduleInit = SYS_MODULE_POWER_RUN_FULL,

   /* Identifies peripheral (PLIB-level) ID */
   .usbID = USB_ID_1,

   /* Stop in idle */
   .stopInIdle = false,

   /* Suspend in sleep */
   .suspendInSleep = false,

   /* Interrupt Source for USB module */
   .interruptSource = INT_SOURCE_USB_1,

   /* Endpoint table */
   .endpointTable= endPointTable,

   /* Number of function drivers registered to this instance of the
       USB device layer */
   .registeredFuncCount = 1,

   /* Function driver table registered to this instance of the USB device layer*/
   .registeredFunctions = (USB_DEVICE_FUNCTION_REGISTRATION_TABLE*)funcRegistrationTable,

   /* Pointer to USB Descriptor structure */
   .usbMasterDescriptor = (USB_DEVICE_MASTER_DESCRIPTOR*)&usbMasterDescriptor,

   /* USB Device Speed */
   .deviceSpeed = USB_SPEED_FULL,
};



/*
*********************************************************************************************************
*                                          SYS_Initialize
*
* Description : This routine initializes the board, services, drivers, application and other modules as
*               configured at build time.
* Caller      : called from main
*********************************************************************************************************
*/
void SYS_Initialize ( void * data )
{
   OS_ERR os_err;

   /* Initialize the uC/CPU services*/
   CPU_Init();

   /* Init uC/OS-III*/
   OSInit(&os_err);
   /*configure the wait states for memory based on processor clock speed*/
   SYS_DEVCON_PerformanceConfig( 80000000 );
   
   /* Initialize the BSP */
   BSP_InitIO( );

   /* Initialize the USB device layer */
   usbDevObject = USB_DEVICE_Initialize (USB_DEVICE_INDEX_0 ,
                  ( SYS_MODULE_INIT* ) & usbDevInitData);

   /*initialize application specific items*/
   ApplicationInitialize();

   /*setup up external interrupt controller to use multi-vectored mode, 
   this is not the internal CP0 register, this sets the SFR register*/
   PLIB_INT_MultiVectorSelect(INT_ID_0);
   
   /* Set priority for USB interrupt source */                                
   PLIB_INT_VectorPrioritySet(INT_ID_0,INT_VECTOR_USB1,INT_PRIORITY_LEVEL2);

   /*setup rtos tick interrupt source, for 1ms tick*/
   __builtin_mtc0(11,0,40000);
   /*start counting from zero*/
   __builtin_mtc0(9,0,0);
   /*clear interrupt flag, before enabling*/
   PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_CORE);   
   /*set priority of core timer, this timer resource is used by the uC/OS-III
   as the OS tick.*/
   PLIB_INT_VectorPrioritySet(INT_ID_0,INT_VECTOR_CT,INT_PRIORITY_LEVEL4);
   PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_TIMER_CORE);
   
   /*create system task(s)*/
   OSTaskCreate((OS_TCB      *)&SystemUSBDeviceTask_TCB,
                (CPU_CHAR    *)"Sys USB Device Task",
                (OS_TASK_PTR  )SystemUSBDeviceTask,
                (void        *)0,
                (OS_PRIO      )SYSTEM_USBDEVICETASK_PRIO,
                (CPU_STK     *)&SystemUSBDeviceTask_Stk[0],
                (CPU_STK_SIZE )SYSTEM_USBDEVICETASK_STKSIZE_LIMIT,
                (CPU_STK_SIZE )SYSTEM_USBDEVICETASK_STKSIZE,
                (OS_MSG_QTY   )0u,
                (OS_TICK      )0u,
                (void        *)0,
                (OS_OPT       )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                (OS_ERR      *)&os_err);
}