#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_config.h"
#include "usb/usb_device.h"
#include "usb/usb_device_hid.h"
#include "app_keyboard.h"
#include "app.h"
#include "os.h"

/*
*********************************************************************************************************
*                                          GLOBALS
*********************************************************************************************************
*/
extern APP_DATA appData;
/*Keyboard Report to be transmitted*/
extern KEYBOARD_INPUT_REPORT __attribute__((coherent, aligned(4))) keyboardInputReport ;
/* Keyboard output report */
extern KEYBOARD_OUTPUT_REPORT __attribute__((coherent, aligned(4))) keyboardOutputReport;

OS_TCB      ApplicationLEDblinkTask_TCB;
OS_TCB      ApplicationUSBDeviceTask_TCB;

CPU_STK     ApplicationLEDblinkTask_Stk[APPLICATION_LEDBLINKTASK_STKSIZE];
CPU_STK     ApplicationUSBDeviceTask_Stk[APPLICATION_USBDEVICETASK_STKSIZE];



/*
*********************************************************************************************************
*                                          ApplicationInitialize
*
* Description :  This function is called by SYS_Initialize() function and run after power up.  Global 
*                are not enabled here (i.e. CP0 Status register).  Interrupts will be enabled by RTOS when
*                the highest priority task is run the first time.
* Arguments   : 
*********************************************************************************************************
*/
void ApplicationInitialize ( void )
{
   OS_ERR os_err;
   
   appData.deviceHandle = USB_DEVICE_HANDLE_INVALID;
   appData.isConfigured = false;

   /* Initialize the keycode array */
   appData.key = USB_HID_KEYBOARD_KEYPAD_KEYBOARD_A;
   appData.keyCodeArray.keyCode[0] = USB_HID_KEYBOARD_KEYPAD_RESERVED_NO_EVENT_INDICATED;
   appData.keyCodeArray.keyCode[1] = USB_HID_KEYBOARD_KEYPAD_RESERVED_NO_EVENT_INDICATED;
   appData.keyCodeArray.keyCode[2] = USB_HID_KEYBOARD_KEYPAD_RESERVED_NO_EVENT_INDICATED;
   appData.keyCodeArray.keyCode[3] = USB_HID_KEYBOARD_KEYPAD_RESERVED_NO_EVENT_INDICATED;
   appData.keyCodeArray.keyCode[4] = USB_HID_KEYBOARD_KEYPAD_RESERVED_NO_EVENT_INDICATED;
   appData.keyCodeArray.keyCode[5] = USB_HID_KEYBOARD_KEYPAD_RESERVED_NO_EVENT_INDICATED;
    
   /* Initialize the modifier keys */
   appData.keyboardModifierKeys.modifierkeys = 0;

   /* Initialise the led state */
   keyboardOutputReport.data = 0;

   /* Intialize the switch state */
   appData.isSwitchPressed = false;
   appData.ignoreSwitchPress = false;

   /* Initialize the HID instance index.  */
   appData.hidInstance = 0;

   /* Initialize tracking variables */
   appData.isReportReceived = false;
   appData.isReportSentComplete = true;

   /* Initialize the application state*/
   appData.state = APP_STATE_INIT;

   OSTaskCreate((OS_TCB      *)&ApplicationLEDblinkTask_TCB,
                (CPU_CHAR    *)"LED Blink Task",
                (OS_TASK_PTR  )ApplicationLEDblinkTask,
                (void        *)0,
                (OS_PRIO      )APPLICATION_LEDBLINKTASK_PRIO,
                (CPU_STK     *)&ApplicationLEDblinkTask_Stk[0],
                (CPU_STK_SIZE )APPLICATION_LEDBLINKTASK_STKSIZE_LIMIT,
                (CPU_STK_SIZE )APPLICATION_LEDBLINKTASK_STKSIZE,
                (OS_MSG_QTY   )0u,
                (OS_TICK      )0u,
                (void        *)0,
                (OS_OPT       )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                (OS_ERR      *)&os_err);


   OSTaskCreate((OS_TCB      *)&ApplicationUSBDeviceTask_TCB,
                (CPU_CHAR    *)"USB Task",
                (OS_TASK_PTR  )ApplicationUSBDeviceTask,
                (void        *)0,
                (OS_PRIO      )APPLICATION_USBDEVICETASK_PRIO,
                (CPU_STK     *)&ApplicationUSBDeviceTask_Stk[0],
                (CPU_STK_SIZE )APPLICATION_USBDEVICETASK_STKSIZE_LIMIT,
                (CPU_STK_SIZE )APPLICATION_USBDEVICETASK_STKSIZE,
                (OS_MSG_QTY   )0u,
                (OS_TICK      )0u,
                (void        *)0,
                (OS_OPT       )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                (OS_ERR      *)&os_err);
   
}

