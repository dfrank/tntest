/*******************************************************************************
 System Interrupt Source File

  File Name:
    sys_int.c

  Summary:
    Raw ISR definitions.

  Description:
    This file contains a definitions of the raw ISRs required to support the 
    interrupt sub-system.
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2011-2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END

/*
*********************************************************************************************************
*                           CONSTANTS USED TO ACCESS TASK CONTEXT STACK
*********************************************************************************************************
*/
.equ    STK_OFFSET_SR,      4
.equ    STK_OFFSET_EPC,     STK_OFFSET_SR    + 4
.equ    STK_OFFSET_LO,      STK_OFFSET_EPC   + 4
.equ    STK_OFFSET_HI,      STK_OFFSET_LO    + 4
.equ    STK_OFFSET_GPR1,    STK_OFFSET_HI    + 4
.equ    STK_OFFSET_GPR2,    STK_OFFSET_GPR1  + 4
.equ    STK_OFFSET_GPR3,    STK_OFFSET_GPR2  + 4
.equ    STK_OFFSET_GPR4,    STK_OFFSET_GPR3  + 4
.equ    STK_OFFSET_GPR5,    STK_OFFSET_GPR4  + 4
.equ    STK_OFFSET_GPR6,    STK_OFFSET_GPR5  + 4
.equ    STK_OFFSET_GPR7,    STK_OFFSET_GPR6  + 4
.equ    STK_OFFSET_GPR8,    STK_OFFSET_GPR7  + 4
.equ    STK_OFFSET_GPR9,    STK_OFFSET_GPR8  + 4
.equ    STK_OFFSET_GPR10,   STK_OFFSET_GPR9  + 4
.equ    STK_OFFSET_GPR11,   STK_OFFSET_GPR10 + 4
.equ    STK_OFFSET_GPR12,   STK_OFFSET_GPR11 + 4
.equ    STK_OFFSET_GPR13,   STK_OFFSET_GPR12 + 4
.equ    STK_OFFSET_GPR14,   STK_OFFSET_GPR13 + 4
.equ    STK_OFFSET_GPR15,   STK_OFFSET_GPR14 + 4
.equ    STK_OFFSET_GPR16,   STK_OFFSET_GPR15 + 4
.equ    STK_OFFSET_GPR17,   STK_OFFSET_GPR16 + 4
.equ    STK_OFFSET_GPR18,   STK_OFFSET_GPR17 + 4
.equ    STK_OFFSET_GPR19,   STK_OFFSET_GPR18 + 4
.equ    STK_OFFSET_GPR20,   STK_OFFSET_GPR19 + 4
.equ    STK_OFFSET_GPR21,   STK_OFFSET_GPR20 + 4
.equ    STK_OFFSET_GPR22,   STK_OFFSET_GPR21 + 4
.equ    STK_OFFSET_GPR23,   STK_OFFSET_GPR22 + 4
.equ    STK_OFFSET_GPR24,   STK_OFFSET_GPR23 + 4
.equ    STK_OFFSET_GPR25,   STK_OFFSET_GPR24 + 4
.equ    STK_OFFSET_GPR26,   STK_OFFSET_GPR25 + 4
.equ    STK_OFFSET_GPR27,   STK_OFFSET_GPR26 + 4
.equ    STK_OFFSET_GPR28,   STK_OFFSET_GPR27 + 4
.equ    STK_OFFSET_GPR30,   STK_OFFSET_GPR28 + 4
.equ    STK_OFFSET_GPR31,   STK_OFFSET_GPR30 + 4
.equ    STK_CTX_SIZE,       STK_OFFSET_GPR31 + 4

/*
*********************************************************************************************************
*                                          PUBLIC sysmbols
*********************************************************************************************************
*/
   .global BSP_TickHandler
   .extern OSIntEnter
   .extern OSIntExit
   .extern OSTCBCurPtr
   .extern OSIntNestingCtr
   .extern CoreTimerIntHandler
   .extern USBGeneralEventInterruptHandler
   

/*
*********************************************************************************************************
*                                          Core Timer Interrupt Handler()
*
* Description : Calls the RTOS Tick function to maintain the RTOS time services.  This is an OS aware 
*               interrupt
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : Hardware Interrupt
*
* Note(s)     : none.
*********************************************************************************************************
*/
   .section	.vector_0,code, keep
   .equ     __vector_dispatch_0, CoreTimerInterruptVector
   .global  __vector_dispatch_0
   .set     nomicromips
   .set     noreorder
   .set     nomips16
   .set     noat

   .ent  CoreTimerInterruptVector
CoreTimerInterruptVector:
   la    $26, CoreTimerIntHandler
   jr    $26
   nop

   .end CoreTimerInterruptVector
   
   
/*
*********************************************************************************************************
*                                          USB 1 Interrupt Vector()
*
* Description : Calls the entry to the sytem USB ISR function to get harmony work done within the context 
*               of the ISR.  This is an OS aware interrupt.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : Hardware Interrupt
*
* Note(s)     : none.
*********************************************************************************************************
*/
   .section	.vector_45,code, keep
   .equ     __vector_dispatch_45, USB1InterruptVector
   .global  __vector_dispatch_45
   .set     nomicromips
   .set     noreorder
   .set     nomips16
   .set     noat

   .ent  USB1InterruptVector
USB1InterruptVector:
   la    $26, USB1InterruptHandler
   jr    $26
   nop

   .end USB1InterruptVector


/*
*********************************************************************************************************
*                                          USB1 Interrupt Handler()
*
* Description : Calls the harmony USB Tasks function to get work done.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : USB 1 Interrupt Vector
*
* Note(s)     : none.
*********************************************************************************************************
*/
   .section	.usb1_vector_text, code, keep
   .set     nomicromips
   .set     noreorder
   .set     nomips16
   .set     noat

   .ent  USB1InterruptHandler
USB1InterruptHandler:

   addiu $29, $29, -STK_CTX_SIZE              /* Adjust the stack pointer                             */

   sw    $1,  STK_OFFSET_GPR1($29)            /* Save the General Pupose Registers                    */
   sw    $2,  STK_OFFSET_GPR2($29)
   sw    $3,  STK_OFFSET_GPR3($29)
   sw    $4,  STK_OFFSET_GPR4($29)
   sw    $5,  STK_OFFSET_GPR5($29)
   sw    $6,  STK_OFFSET_GPR6($29)
   sw    $7,  STK_OFFSET_GPR7($29)
   sw    $8,  STK_OFFSET_GPR8($29)
   sw    $9,  STK_OFFSET_GPR9($29)
   sw    $10, STK_OFFSET_GPR10($29)
   sw    $11, STK_OFFSET_GPR11($29)
   sw    $12, STK_OFFSET_GPR12($29)
   sw    $13, STK_OFFSET_GPR13($29)
   sw    $14, STK_OFFSET_GPR14($29)
   sw    $15, STK_OFFSET_GPR15($29)
   sw    $16, STK_OFFSET_GPR16($29)
   sw    $17, STK_OFFSET_GPR17($29)
   sw    $18, STK_OFFSET_GPR18($29)
   sw    $19, STK_OFFSET_GPR19($29)
   sw    $20, STK_OFFSET_GPR20($29)
   sw    $21, STK_OFFSET_GPR21($29)
   sw    $22, STK_OFFSET_GPR22($29)
   sw    $23, STK_OFFSET_GPR23($29)
   sw    $24, STK_OFFSET_GPR24($29)
   sw    $25, STK_OFFSET_GPR25($29)
   sw    $26, STK_OFFSET_GPR26($29)
   sw    $27, STK_OFFSET_GPR27($29)
   sw    $28, STK_OFFSET_GPR28($29)
   sw    $30, STK_OFFSET_GPR30($29)
   sw    $31, STK_OFFSET_GPR31($29)
                                               /* Save the contents of the LO and HI registers         */
   mflo  $8
   mfhi  $9
   sw    $8,  STK_OFFSET_LO($29)
   sw    $9,  STK_OFFSET_HI($29)

   mfc0  $8,  $14, 0                          /* Save the EPC                                         */
   sw    $8,  STK_OFFSET_EPC($29)

   mfc0  $8,  $12, 0
   sw    $8,  STK_OFFSET_SR($29)


   la    $8,  OSIntNestingCtr                 /* See if OSIntNesting == 0, save Tasks Stack pointer   */
   lbu   $9,  0($8)                           
   bne   $0,  $9, __nested_save
   nop   

   la    $10, OSTCBCurPtr                     /* Save the current task's stack pointer                */
   lw    $11, 0($10)
   sw    $29, 0($11)                          /* OSTCBCurPtr->StkPtr = SP;                            */
    
__nested_save:   
   la    $8, OSIntEnter                       /* Call OSIntEnter                                      */
   jalr  $8
   nop

   la    $8, USBGeneralEventInterruptHandler
   jalr  $8
   nop

   la    $8,  OSIntExit                       /* Call OSIntExit()                                     */
   jalr  $8
   nop

   lw    $8,  STK_OFFSET_SR($29)              /* Restore the Status register                          */
   mtc0  $8,  $12, 0

   lw    $8,  STK_OFFSET_EPC($29)             /* Restore the EPC                                      */
   mtc0  $8,  $14, 0

   lw    $8,  STK_OFFSET_LO($29)              /* Restore the contents of the LO and HI registers      */
   lw    $9,  STK_OFFSET_HI($29)
   mtlo  $8
   mtlo  $9

   lw    $31, STK_OFFSET_GPR31($29)           /* Restore the General Purpose Registers                */
   lw    $30, STK_OFFSET_GPR30($29)
   lw    $28, STK_OFFSET_GPR28($29)
   lw    $27, STK_OFFSET_GPR27($29)
   lw    $26, STK_OFFSET_GPR26($29)
   lw    $25, STK_OFFSET_GPR25($29)
   lw    $24, STK_OFFSET_GPR24($29)
   lw    $23, STK_OFFSET_GPR23($29)
   lw    $22, STK_OFFSET_GPR22($29)
   lw    $21, STK_OFFSET_GPR21($29)
   lw    $20, STK_OFFSET_GPR20($29)
   lw    $19, STK_OFFSET_GPR19($29)
   lw    $18, STK_OFFSET_GPR18($29)
   lw    $17, STK_OFFSET_GPR17($29)
   lw    $16, STK_OFFSET_GPR16($29)
   lw    $15, STK_OFFSET_GPR15($29)
   lw    $14, STK_OFFSET_GPR14($29)
   lw    $13, STK_OFFSET_GPR13($29)
   lw    $12, STK_OFFSET_GPR12($29)
   lw    $11, STK_OFFSET_GPR11($29)
   lw    $10, STK_OFFSET_GPR10($29)
   lw    $9,  STK_OFFSET_GPR9($29)
   lw    $8,  STK_OFFSET_GPR8($29)
   lw    $7,  STK_OFFSET_GPR7($29)
   lw    $6,  STK_OFFSET_GPR6($29)
   lw    $5,  STK_OFFSET_GPR5($29)
   lw    $4,  STK_OFFSET_GPR4($29)
   lw    $3,  STK_OFFSET_GPR3($29)
   lw    $2,  STK_OFFSET_GPR2($29)
   lw    $1,  STK_OFFSET_GPR1($29)

   addiu $29, $29, STK_CTX_SIZE               /* Adjust the stack pointer                             */

   eret
   .end USB1InterruptHandler

/*
*********************************************************************************************************
*                                          RTOS ISR Tick Handler
*
* Description : Reloads the timer used to source the RTOS tick.  For this system the RTOS tick runs from
*               MIPS core timer, and is set to 1mSec.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : core timer interrupt vector wrapper, see crt0.S
*
* Note(s)     : none.
*********************************************************************************************************
*/
   .section .bsp_tickhandler_text,code,keep
   .set     noreorder
   .set     noat
   .set	   nomips16
   .set	   nomicromips

   .ent	BSP_TickHandler
BSP_TickHandler:
   mtc0  $0, $9, 0                          /* clear core timer register                              */
   li    $8, 40000                          /* count value based on sys clock running at 80Mhz       */
   mtc0  $8, $11                            /* reload the core timer period register                  */
   ehb

   li    $8, 1                              /* Clear core timer interrupt                             */
   la    $9, IFS0CLR
   sw	   $8, ($9)
   .end BSP_TickHandler
/*******************************************************************************
 End of File
 */
