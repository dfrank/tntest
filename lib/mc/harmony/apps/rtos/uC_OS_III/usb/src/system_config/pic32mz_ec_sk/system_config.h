#ifndef __SYSTEM_CONFIG_H
#define	__SYSTEM_CONFIG_H

/* This is a temporary workaround for an issue with the peripheral library "Exists"
   functions that causes superfluous warnings.  It "nulls" out the definition of
   The PLIB function attribute that causes the warning.  Once that issue has been
   resolved, this definition should be removed. */
#define _PLIB_UNSUPPORTED

/*
*********************************************************************************************************
*                                         DEFINES
*********************************************************************************************************
*/
// *****************************************************************************
/* Clock System Service Configuration Options
*/

#define SYS_CLK_SOURCE                      SYS_CLK_SOURCE_PRIMARY_SYSPLL
#define SYS_CLK_FREQ                        200000000ul
/*this is the speed of the clock feeding the clock source, internal FRC is
 8 MHz*/
#define SYS_CLK_CONFIG_PRIMARY_XTAL         24000000ul
#define SYS_CLK_CONFIG_SECONDARY_XTAL       200000000ul
#define SYS_CLK_CONFIG_SYSPLL_INP_DIVISOR   3
#define SYS_CLK_CONFIG_FREQ_ERROR_LIMIT     10
#define SYS_CLK_CONFIGBIT_USBPLL_ENABLE     true
#define SYS_CLK_CONFIG_USB_CLOCK            24000000ul
#define SYS_CLK_WAIT_FOR_SWITCH             true
#define SYS_CLK_KEEP_SECONDARY_OSC_ENABLED  true
#define SYS_CLK_ON_WAIT                     OSC_ON_WAIT_IDLE

/*** Common System Service Configuration ***/
#define SYS_BUFFER  false
#define SYS_QUEUE   false

// *****************************************************************************
/* Device Control System Service Configuration Options
*/
#define SYS_DEVCON_SYSTEM_CLOCK         200000000

/*** Interrupt System Service Configuration ***/
#define SYS_INT                     true

/*** Timer System Service Configuration ***/
#define SYS_TMR_POWER_STATE             SYS_MODULE_POWER_RUN_FULL
#define SYS_TMR_DRIVER_INDEX            DRV_TMR_INDEX_0
#define SYS_TMR_MAX_CLIENT_OBJECTS      5
#define SYS_TMR_FREQUENCY               1000
#define SYS_TMR_FREQUENCY_TOLERANCE     10
#define SYS_TMR_UNIT_RESOLUTION         10000
#define SYS_TMR_CLIENT_TOLERANCE        10
#define SYS_TMR_INTERRUPT_NOTIFICATION  false


/*** Timer Driver Configuration ***/

#define DRV_TMR_INSTANCES_NUMBER           1
#define DRV_TMR_CLIENTS_NUMBER             1
#define DRV_TMR_INTERRUPT_MODE             true

#define DRV_TMR_PERIPHERAL_ID_IDX0          TMR_ID_2
#define DRV_TMR_INTERRUPT_SOURCE_IDX0       INT_SOURCE_TIMER_2
#define DRV_TMR_INTERRUPT_VECTOR_IDX0       INT_VECTOR_T2
#define DRV_TMR_ISR_VECTOR_IDX0             _TIMER_2_VECTOR
#define DRV_TMR_INTERRUPT_PRIORITY_IDX0     INT_PRIORITY_LEVEL4
#define DRV_TMR_INTERRUPT_SUB_PRIORITY_IDX0 INT_SUBPRIORITY_LEVEL0
#define DRV_TMR_CLOCK_SOURCE_IDX0           DRV_TMR_CLKSOURCE_INTERNAL
#define DRV_TMR_PRESCALE_IDX0               TMR_PRESCALE_VALUE_256
#define DRV_TMR_OPERATION_MODE_IDX0         DRV_TMR_OPERATION_MODE_16_BIT
#define DRV_TMR_ASYNC_WRITE_ENABLE_IDX0     false
#define DRV_TMR_POWER_STATE_IDX0            SYS_MODULE_POWER_RUN_FULL

/*** USB Driver Configuration ***/
/* Enables Device Support */
#define DRV_USB_DEVICE_SUPPORT      true
/* Enables Device Support */
#define DRV_USB_HOST_SUPPORT        false
#define DRV_USB_INSTANCES_NUMBER    1
#define DRV_USB_INTERRUPT_MODE      true
#define DRV_USB_ENDPOINTS_NUMBER    2
#define USB_DEVICE_INSTANCES_NUMBER     1
#define USB_DEVICE_EP0_BUFFER_SIZE      64
#define USB_DEVICE_SOF_EVENT_ENABLE     true
#define USB_DEVICE_SET_DESCRIPTOR_EVENT_ENABLE
#define USB_DEVICE_SYNCH_FRAME_EVENT_ENABLE
#define USB_DEVICE_HID_INSTANCES_NUMBER     1
#define USB_DEVICE_HID_QUEUE_DEPTH_COMBINED 2


/*
*********************************************************************************************************
*                                       FUNCTION PROTOTYPES
*********************************************************************************************************
*/
void SYS_Initialize ( void * data );
void SystemUSBDeviceTask (void *p_arg);
void SYS_Tasks (void);

#endif	/* SYSTEM_CONFIG_H */

