#include <xc.h>
#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_config.h"
#include "system/system.h"
#include "usb/usb_device.h"
#include "usb/usb_device_hid.h"
#include "app_keyboard.h"
#include "app.h"
#include "os.h"

/*
*********************************************************************************************************
*                                                VARIABLES
*********************************************************************************************************
*/
extern SYS_MODULE_OBJ usbDevObject;

/*
*********************************************************************************************************
*                                          SYS_Tasks()
*
* Description : Starts the OS, never returns
*
* Argument(s) : none
*
* Caller(s)   : main
*********************************************************************************************************
*/
void SYS_Tasks (void)
{
   OS_ERR os_err;
   OSStart(&os_err);       /* Start multitasking (i.e. give control to uC/OS-III).     */

   (void)&os_err;
}


/*
*********************************************************************************************************
*                                          SystemUSBDeviceTask
*
* Description : device layer tasks routine, HID tasks gets called from device layer tasks.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : OS called after waiting time delay()
*
* Note(s)     : none.
*********************************************************************************************************
*/
void SystemUSBDeviceTask (void *p_arg)
{
   OS_ERR err;
   
   while (1)
   {
      USB_DEVICE_Tasks(usbDevObject);
      /* System Timer Tasks Routine */
      OSTimeDly(50,OS_OPT_TIME_DLY,&err);
   }
}