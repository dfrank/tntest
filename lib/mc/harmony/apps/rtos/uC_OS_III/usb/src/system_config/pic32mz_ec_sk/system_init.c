#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_config.h"
#include "system/system.h"
#include "usb/usb_device.h"
#include "usb/usb_device_hid.h"
#include "peripheral/devcon/plib_devcon.h"
#include "peripheral/int/plib_int.h"
#include "peripheral/osc/plib_osc.h"
#include "driver/tmr/drv_tmr.h"
#include "system/clk/sys_clk.h"
#include "system/tmr/sys_tmr.h"
#include "app_keyboard.h"
#include "app.h"
#include "bsp.h"
#include "system_definitions.h"
#include "os.h"

/*
*********************************************************************************************************
*                                                CONFIGURATION WORDS
* Description : Sets the fuses during programming of the device.  This will cause the hardware to come up
*               in a preconfigured state.  
*               We set the CPU clock to 200MHz and use the SystemPLL output as the main clock source.  
*               The input is feed from the internal Fast RC at approximately 8MHz.
*********************************************************************************************************
*/
// DEVCFG3
#pragma config FMIIEN   = OFF
#pragma config FETHIO   = OFF
#pragma config PGL1WAY  = OFF
#pragma config PMDL1WAY = OFF
#pragma config IOL1WAY  = OFF
#pragma config FUSBIDIO = ON
// DEVCFG2
#pragma config FPLLIDIV = DIV_3
#pragma config FPLLRNG = RANGE_5_10_MHZ 
#pragma config FPLLICLK = PLL_POSC
#pragma config FPLLMULT = MUL_50        
#pragma config FPLLODIV = DIV_2
#pragma config UPLLFSEL =   FREQ_24MHZ
#pragma config UPLLEN =     ON
// DEVCFG1
#pragma config FNOSC = SPLL             
#pragma config DMTINTV = WIN_127_128    
#pragma config FSOSCEN = OFF            
#pragma config IESO = OFF               
#pragma config POSCMOD = EC             
#pragma config OSCIOFNC = OFF            
#pragma config FCKSM = CSDCMD           
#pragma config WDTPS = PS1048576        
#pragma config WDTSPGM = STOP           
#pragma config WINDIS = NORMAL          
#pragma config FWDTEN = OFF             
#pragma config FWDTWINSZ = WINSZ_25     
#pragma config FDMTEN = OFF             
#pragma config DMTCNT = 0
/* DEVCFG0 */
#pragma config EJTAGBEN = NORMAL
#pragma config DBGPER =   PG_ALL
#pragma config FSLEEP =   OFF
#pragma config FECCCON = OFF_UNLOCKED
#pragma config BOOTISA = MIPS32
#pragma config TRCEN =   OFF
#pragma config ICESEL = ICS_PGx2
#pragma config DEBUG = OFF
// DEVCP0
#pragma config CP = OFF                 
#pragma config_alt FWDTEN=OFF
#pragma config_alt USERID = 0x1234u

/*
*********************************************************************************************************
*                                                VARIABLES
*********************************************************************************************************
*/
OS_TCB      SystemUSBDeviceTask_TCB;
CPU_STK     SystemUSBDeviceTask_Stk[SYSTEM_USBDEVICETASK_STKSIZE];

SYS_MODULE_OBJ usbDevObject;
SYS_MODULE_OBJ tmrDevObject;
SYS_MODULE_OBJ sysTMRDevObject;

extern const USB_DEVICE_FUNCTION_REGISTRATION_TABLE funcRegistrationTable[1];
extern const USB_DEVICE_MASTER_DESCRIPTOR usbMasterDescriptor;

const USB_DEVICE_INIT usbDevInitData =
{
    /* System module initialization */
    .moduleInit = SYS_MODULE_POWER_RUN_FULL,

/* Identifies peripheral (PLIB-level) ID */
    .usbID = 0,

    /* Stop in idle */
    .stopInIdle = false,

    /* Suspend in sleep */
    .suspendInSleep = false,

    /* Interrupt Source for USB module */
    .interruptSource = INT_SOURCE_USB_1,

    /* Interrupt Source for USB module */
    .interruptSourceUSBDma = INT_SOURCE_USB_1_DMA,


    /* Number of function drivers registered to this instance of the
       USB device layer */
    .registeredFuncCount = 1,

    /* Function driver table registered to this instance of the USB device layer*/
    .registeredFunctions = (USB_DEVICE_FUNCTION_REGISTRATION_TABLE*)funcRegistrationTable,

    /* Pointer to USB Descriptor structure */
    .usbMasterDescriptor = (USB_DEVICE_MASTER_DESCRIPTOR*)&usbMasterDescriptor,

    /* USB Device Speed */
    .deviceSpeed = USB_SPEED_HIGH,
};


// *****************************************************************************
// *****************************************************************************
// Section: Driver Initialization Data
// *****************************************************************************
// *****************************************************************************


/*************************************************
 * Timer Driver Initialization
 ************************************************/
DRV_TMR_INIT drvTMRInit =
{
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .tmrId            = TMR_ID_2,
    .clockSource      = DRV_TMR_CLKSOURCE_INTERNAL,
    .prescale         = TMR_PRESCALE_VALUE_256,
    .interruptSource  = INT_SOURCE_TIMER_2,
    .mode             = DRV_TMR_OPERATION_MODE_16_BIT,
    .asyncWriteEnable = false,
};
/*** TMR Service Initialization Data ***/
const SYS_TMR_INIT sysTmrInit =
{
    .moduleInit = {SYS_MODULE_POWER_RUN_FULL},
    .drvIndex = DRV_TMR_INDEX_0,
    .tmrFreq = 1000,
};

// *****************************************************************************
/* System Clock Initialization Data
*/

const SYS_CLK_INIT sysClkInit =
{
    .moduleInit = {0},
    .systemClockSource = SYS_CLK_SOURCE,
    .systemClockFrequencyHz = SYS_CLK_FREQ,
    .waitTillComplete = true,
    .secondaryOscKeepEnabled = true,
    .onWaitInstruction = SYS_CLK_ON_WAIT,
};


/*
*********************************************************************************************************
*                                          SYS_Initialize
*
* Description : This routine initializes the board, services, drivers, application and other modules as
*               configured at build time.
* Caller      : called from main
*********************************************************************************************************
*/
void SYS_Initialize ( void * data )
{
   OS_ERR os_err;

   /* Initialize the uC/CPU services*/
   CPU_Init();

   /* Init uC/OS-III*/
   OSInit(&os_err);

   /*configure the wait states for memory based on processor clock speed*/
   SYS_DEVCON_PerformanceConfig( 200000000 );

   /* Initialize the BSP */
   BSP_Initialize( );

   /* Set the PBCLK3 divisor to 5 and enable PBCLK3 (for timer2 ) */
   PLIB_DEVCON_SystemUnlock(DEVCON_ID_0);
   PLIB_OSC_PBClockDivisorSet(OSC_ID_0, OSC_PERIPHERAL_BUS_3, 0x05);
   PLIB_OSC_PBOutputClockEnable(OSC_ID_0, OSC_PERIPHERAL_BUS_3);
   PLIB_DEVCON_SystemLock(DEVCON_ID_0);

   /*needed by system timer service, we pass null so it knows not to
     change configuration set by config words*/
   SYS_CLK_Initialize(NULL);
   /* Initialize the USB device layer */
   usbDevObject = USB_DEVICE_Initialize (USB_DEVICE_INDEX_0 ,
                  ( SYS_MODULE_INIT* ) & usbDevInitData);

   /* Intialize the Timer Driver */
   tmrDevObject = DRV_TMR_Initialize(DRV_TMR_INDEX_0,
                  (SYS_MODULE_INIT *) &drvTMRInit);

   /* Intialize the System Timer Service */
   sysTMRDevObject = SYS_TMR_Initialize(SYS_TMR_INDEX_0,
                  (SYS_MODULE_INIT *) &sysTmrInit);

   /*initialize application specific items*/
   ApplicationInitialize();

   /*setup up interrupt controller to use multi-vectored mode, this is not the internal CP0 register,
   this sets the SFR register*/
   PLIB_INT_MultiVectorSelect(INT_ID_0);
   /* Set priority for USB interrupt source */                                
   PLIB_INT_VectorPrioritySet(INT_ID_0,INT_VECTOR_USB1,INT_PRIORITY_LEVEL2);
   /* Set the priority of the timer 2, this system service is used by usb stack*/
   PLIB_INT_VectorPrioritySet(INT_ID_0, INT_VECTOR_T2, INT_PRIORITY_LEVEL1);
   PLIB_INT_SourceEnable(INT_ID_0, INT_SOURCE_TIMER_2);
   
   /* Set the priority of the USB DMA Interrupt */
   PLIB_INT_VectorPrioritySet(INT_ID_0,INT_VECTOR_USB1_DMA,INT_PRIORITY_LEVEL3);

   /*set priority of core timer, this timer resource is used by the uC/OS-III
   as the OS tick.*/
   PLIB_INT_VectorPrioritySet(INT_ID_0,INT_VECTOR_CT,INT_PRIORITY_LEVEL4);
   /*setup rtos tick interrupt source, for 1ms tick*/
   __builtin_mtc0(11,0,100000);
   /*start counting from zero*/
   __builtin_mtc0(9,0,0);
   /*clear interrupt flag, before enabling*/
   PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_CORE);
   /*set priority of core timer, this timer resource is used by the uC/OS-III
   as the OS tick.*/
   PLIB_INT_VectorPrioritySet(INT_ID_0,INT_VECTOR_CT,INT_PRIORITY_LEVEL4);
   PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_TIMER_CORE);
  
   /*create system task(s)*/
   OSTaskCreate((OS_TCB      *)&SystemUSBDeviceTask_TCB,
                (CPU_CHAR    *)"Sys USB Device Task",
                (OS_TASK_PTR  )SystemUSBDeviceTask,
                (void        *)0,
                (OS_PRIO      )SYSTEM_USBDEVICETASK_PRIO,
                (CPU_STK     *)&SystemUSBDeviceTask_Stk[0],
                (CPU_STK_SIZE )SYSTEM_USBDEVICETASK_STKSIZE_LIMIT,
                (CPU_STK_SIZE )SYSTEM_USBDEVICETASK_STKSIZE,
                (OS_MSG_QTY   )0u,
                (OS_TICK      )0u,
                (void        *)0,
                (OS_OPT       )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                (OS_ERR      *)&os_err);
}