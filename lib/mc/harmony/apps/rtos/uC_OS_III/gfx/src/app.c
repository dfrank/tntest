/*******************************************************************************
  GFX Primitive Demo Application
  
  File Name:
    app.c

  Summary:
    GFX Primitive Demo application

  Description:
    This file contains the GFX Primitive Demo application logic.
 *******************************************************************************/


// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


/*
*********************************************************************************************************
*                                                INCLUDES
*********************************************************************************************************
*/
#include    <xc.h>
#include    <stdbool.h>
#include    "os.h"
#include    "app.h"
#include    "system_config.h"
#include    "gfx/gfx.h"




/*
*********************************************************************************************************
*                                                DEFINES
*********************************************************************************************************
*/

#define MAXSCREENCOLORS             7

/*
*********************************************************************************************************
*                                                VARIABLES
*********************************************************************************************************
*/

unsigned short ScreenColor[MAXSCREENCOLORS] = {BRIGHTBLUE,BRIGHTGREEN,BRIGHTCYAN,BRIGHTRED,
                                               BRIGHTMAGENTA,SIENNA,GOLD};
OS_TCB      AppLEDblinkTask_TCB;
OS_TCB      AppDisplayTask_TCB;


CPU_STK     AppLEDblinkTask_Stk[LEDBLINK_TASK_STK_SIZE];
CPU_STK     AppDisplayTaskStk[DISPLAY_TASK_STK_SIZE];
                                              

/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/
void LEDblinkTask(void *p_arg);
void DisplayTask(void *p_arg);


/*                                                                                                       
*********************************************************************************************************
*                                          App Initialize()                                               
*                                                                                                        
* Description : Initializes the application specific items here, in this case, just needs to create the 
*               application tasks, which do application specific work.
*                                                                                                        
* Argument(s) : none                                                                                     
*                                                                                                        
* Return(s)   : none                                                                                     
*                                                                                                        
* Caller(s)   : OS called after waiting time delay()                                                                           
*                                                                                                        
* Note(s)     : none.                                                                                    
*********************************************************************************************************
*/   
void APP_Initialize ( void )
{
   OS_ERR os_err;
 
   OSTaskCreate((OS_TCB      *)&AppDisplayTask_TCB,
                (CPU_CHAR    *)"Display Task",
                (OS_TASK_PTR  )DisplayTask,
                (void        *)0,
                (OS_PRIO      )DISPLAY_TASK_PRIO,
                (CPU_STK     *)&AppDisplayTaskStk[0],
                (CPU_STK_SIZE )DISPLAY_TASK_STK_SIZE_LIMIT,
                (CPU_STK_SIZE )DISPLAY_TASK_STK_SIZE,
                (OS_MSG_QTY   )0u,
                (OS_TICK      )0u,
                (void        *)0,
                (OS_OPT       )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                (OS_ERR      *)&os_err);

   OSTaskCreate((OS_TCB      *)&AppLEDblinkTask_TCB,
                (CPU_CHAR    *)"LED Blink Task",
                (OS_TASK_PTR  )LEDblinkTask,
                (void        *)0,
                (OS_PRIO      )LEDBLINK_TASK_PRIO,
                (CPU_STK     *)&AppLEDblinkTask_Stk[0],
                (CPU_STK_SIZE )LEDBLINK_TASK_STK_SIZE_LIMIT,
                (CPU_STK_SIZE )LEDBLINK_TASK_STK_SIZE,
                (OS_MSG_QTY   )0u,
                (OS_TICK      )0u,
                (void        *)0,
                (OS_OPT       )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                (OS_ERR      *)&os_err); 
               
}

/*                                                                                                       
*********************************************************************************************************
*                                          LEDblinkTask()                                               
*                                                                                                        
* Description : Application task, blinks user LED's on a periodic basis.  Lets the user know the RTOS is
*               up and running
* Argument(s) : none                                                                                     
*                                                                                                        
* Return(s)   : none                                                                                     
*                                                                                                        
* Caller(s)   : OS called after waiting time delay()                                                                           
*                                                                                                        
* Note(s)     : none.                                                                                    
*********************************************************************************************************
*/     
void LEDblinkTask (void *p_arg)
{
   OS_ERR   err;
      
   while (1) /* Task body, always written as an infinite loop.       */
   {                        
      LED_Toggle();
      OSTimeDly(500,OS_OPT_TIME_DLY,&err);
   }
}

/*                                                                                                       
*********************************************************************************************************
*                                          DisplayTask()                                               
*                                                                                                        
* Description : Application task, queues up writes to the LCD screen.  In this case it just changes the 
*               screen color every 500mS.  More complex gfx work can be added here.
*                                                                                                        
* Argument(s) : none                                                                                     
*                                                                                                        
* Return(s)   : none                                                                                     
*                                                                                                        
* Caller(s)   : OS called after waiting time delay()                                                                           
*                                                                                                        
* Note(s)     : none.                                                                                    
*********************************************************************************************************
*/
void  DisplayTask (void *p_arg)
{
   static unsigned int ScreenColor_Index = 0;
   OS_ERR   err;
   CPU_SR_ALLOC();
   while(1)   
   {
      CPU_INT_DIS();
      GFX_ColorSet(GFX_INDEX_0, ScreenColor[ScreenColor_Index++]);
      GFX_ScreenClear(0);
      CPU_INT_EN();
      
      if(ScreenColor_Index == MAXSCREENCOLORS)
         ScreenColor_Index = 0;

      OSTimeDly(500,OS_OPT_TIME_DLY,&err);
   }

}





