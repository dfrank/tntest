/*******************************************************************************
  MPLAB Harmony System Configuration Header

  File Name:
    system_config.h

  Summary:
    Build-time configuration header for the system defined by this MPLAB Harmony
    project.

  Description:
    An MPLAB Project may have multiple configurations.  This file defines the
    build-time options for a single configuration.

  Remarks:
    This configuration header must not define any prototypes or data
    definitions (or include any files that do).  It only provides macro
    definitions for build-time configuration options that are not instantiated
    until used by another MPLAB Harmony module or application.
    
    Created with MPLAB Harmony Version 1.00
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

#ifndef _SYSTEM_CONFIG_H
#define _SYSTEM_CONFIG_H
#include <xc.h>
// *****************************************************************************
// *****************************************************************************
// Section: System Service Configuration
// *****************************************************************************
// *****************************************************************************


/*** Common System Service Configuration ***/

#define SYS_BUFFER  false
#define SYS_QUEUE   false


// *****************************************************************************
/* Device Control System Service Configuration Options
*/

#define SYS_DEVCON_SYSTEM_CLOCK           80000000
#define SYS_DEVCON_PIC32MX_MAX_PB_FREQ    80000000



/*** Interrupt System Service Configuration ***/

#define SYS_INT                           true


// *****************************************************************************
// *****************************************************************************
// Section: Driver Configuration
// *****************************************************************************
// *****************************************************************************
#define GFX_USE_DISPLAY_CONTROLLER_SSD1926
#define USE_8BIT_PMP

#define DisplayResetConfig()           TRISACLR = _TRISA_TRISA10_MASK
#define DisplayResetEnable()           LATACLR = _LATA_LATA10_MASK
#define DisplayResetDisable()          LATASET = _LATA_LATA10_MASK

// Definitions for RS pin
#define DisplayCmdDataConfig()         AD1PCFGSET = _AD1PCFG_PCFG10_MASK, TRISBCLR = _TRISB_TRISB10_MASK
#define DisplaySetCommand()            LATBCLR = _LATB_LATB10_MASK
#define DisplaySetData()               LATBSET = _LATB_LATB10_MASK

// Definitions for CS pin
#define DisplayConfig()                TRISGCLR = _TRISG_TRISG13_MASK
#define DisplayEnable()                LATGCLR = _LATG_LATG13_MASK
#define DisplayDisable()               LATGSET = _LATG_LATG13_MASK

#define DisplayBacklightConfig()       (TRISDbits.TRISD0 = 0)
#define DisplayBacklightOn()           (LATDbits.LATD0 = BACKLIGHT_ENABLE_LEVEL)
#define DisplayBacklightOff()          (LATDbits.LATD0 = BACKLIGHT_DISABLE_LEVEL)

#define DISP_ORIENTATION		          90
#define DISP_HOR_RESOLUTION		       240
#define DISP_VER_RESOLUTION		       320
#define DISP_DATA_WIDTH			          18
#define DISP_INV_LSHIFT                 1
#define DISP_HOR_PULSE_WIDTH            25
#define DISP_HOR_BACK_PORCH             5
#define DISP_HOR_FRONT_PORCH            10
#define DISP_VER_PULSE_WIDTH            4
#define DISP_VER_BACK_PORCH             0
#define DISP_VER_FRONT_PORCH            2
#define GFX_LCD_TYPE                    GFX_LCD_TFT

#define BACKLIGHT_ENABLE_LEVEL         0
#define BACKLIGHT_DISABLE_LEVEL        1

#define USE_GFX_PMP
#define PMP_DATA_SETUP_TIME                (18)
#define PMP_DATA_WAIT_TIME                 (82)  // based on the minimum pulse width requirement of CS signal of SSD1926
#define PMP_DATA_HOLD_TIME                 (0)


extern void GFX_TCON_SSD1289Init(void);
#define USE_TCON_SSD1289
#define TCON_MODULE GFX_TCON_SSD1289Init


// *****************************************************************************
// Section: Middleware & Other Library Configuration
// *****************************************************************************
/*** GFX Library Configuration ***/

#define GFX_INSTANCES_NUMBER                             1
#define GFX_RUN_MODE                                     0
#define GFX_SELF_PREEMPTION_LEVEL                        0
#define GFX_CONFIG_GRADIENT_DISABLE
#define GFX_CONFIG_PALETTE_DISABLE
#define GFX_CONFIG_FONT_ANTIALIASED_DISABLE
#define GFX_CONFIG_TRANSPARENT_COLOR_DISABLE
#define GFX_CONFIG_PALETTE_EXTERNAL_DISABLE
#define GFX_CONFIG_FOCUS_DISABLE
#define GFX_CONFIG_FONT_CHAR_SIZE                        8
#define GFX_CONFIG_FONT_EXTERNAL_DISABLE
#define GFX_CONFIG_FONT_RAM_DISABLE
#define GFX_CONFIG_IMAGE_EXTERNAL_DISABLE
#define GFX_CONFIG_IMAGE_RAM_DISABLE
#define GFX_CONFIG_COLOR_DEPTH                           16
#define GFX_CONFIG_DOUBLE_BUFFERING_DISABLE
#define GFX_CONFIG_USE_KEYBOARD_DISABLE
#define GFX_CONFIG_USE_TOUCHSCREEN_DISABLE
#define GFX_malloc(size)                                 malloc(size)
#define GFX_free(pObj)                                   free(pObj)


#define OSAL_USE_RTOS                                    5

#endif // _SYSTEM_CONFIG_H
/*******************************************************************************
 End of File
*/

