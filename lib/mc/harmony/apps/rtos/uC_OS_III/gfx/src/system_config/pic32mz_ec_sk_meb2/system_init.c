/*******************************************************************************
 System Initialization File

  File Name:
    sys_init.c

  Summary:
    System Initialization.

  Description:
    This file contains source code necessary to initialize the system.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2011-2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

/*
*********************************************************************************************************
*                                                INCLUDES
*********************************************************************************************************
*/
#include    <xc.h>
#include    "os.h"
#include    "app.h"
#include    "system/int/sys_int.h"
#include    "system/dma/sys_dma.h"
#include    "system_config.h"
#include    "bsp.h"
#include    "peripheral/osc/plib_osc.h"
#include    "peripheral/int/plib_int.h"
#include    "peripheral/devcon/plib_devcon.h"
#include    "driver/gfx/controller/lcc/drv_gfx_lcc.h"
#include    "gfx/gfx.h"


/*
*********************************************************************************************************
*                                                CONFIGURATION WORDS
*********************************************************************************************************
*/
// DEVCFG2
#pragma config FPLLIDIV = DIV_1         // System PLL Input Divider (1x Divider)
#pragma config FPLLRNG = RANGE_5_10_MHZ // System PLL Input Range (5-10 MHz Input)
#pragma config FPLLICLK = PLL_FRC       // System PLL Input Clock Selection (FRC is input to the System PLL)
#pragma config FPLLMULT = MUL_50        // System PLL Multiplier (PLL Multiply by 50)
#pragma config FPLLODIV = DIV_2
#pragma config UPLLFSEL = FREQ_24MHZ    // USB PLL Input Frequency Selection (USB PLL input is 12 MHz)
#pragma config UPLLEN = ON              // USB PLL Enable (USB PLL is enabled)


// DEVCFG1
#pragma config FNOSC = SPLL             // Oscillator Selection Bits (Fast RC Osc w/Div-by-N (FRCDIV))
#pragma config DMTINTV = WIN_127_128    // DMT Count Window Interval (Window/Interval value is 127/128 counter value)
#pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Disable SOSC)
#pragma config IESO = OFF               // Internal/External Switch Over (Disabled)
#pragma config POSCMOD = EC             // Primary Oscillator Configuration (Primary osc disabled)
#pragma config OSCIOFNC = ON            // CLKO Output Signal Active on the OSCO Pin (Enabled)
#pragma config FCKSM = CSECMD           // Clock Switching and Monitor Selection (Clock Switch Enabled, FSCM Enabled)
#pragma config WDTPS = PS1048576        // Watchdog Timer Postscaler (1:1048576)
#pragma config WDTSPGM = STOP           // Watchdog Timer Stop During Flash Programming (WDT stops during Flash programming)
#pragma config WINDIS = NORMAL          // Watchdog Timer Window Mode (Watchdog Timer is in non-Window mode)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable (WDT Disabled)
#pragma config FWDTWINSZ = WINSZ_25     // Watchdog Timer Window Size (Window size is 25%)
#pragma config FDMTEN = OFF             // Deadman Timer Enable (Deadman Timer is disabled)

#pragma config DMTCNT = 0

/* DEVCFG0 */
#pragma config EJTAGBEN = NORMAL
#pragma config DBGPER = PG_ALL
#pragma config FSLEEP = OFF
#pragma config FECCCON = OFF_UNLOCKED
#pragma config BOOTISA = MIPS32
#pragma config TRCEN = ON
#pragma config ICESEL = ICS_PGx2
#pragma config DEBUG = ON

// DEVCP0
#pragma config CP = OFF                 // Code Protect (Protection Disabled)
#pragma config_alt FWDTEN=OFF
#pragma config_alt USERID = 0x1234u




/*
*********************************************************************************************************
*                                                VARIABLES
*********************************************************************************************************
*/
OS_TCB      System_GFX_Task_TCB;
CPU_STK     System_GFX_Task_Stk[SYSTEM_GFX_TASK_STK_SIZE];

/*** GFX Initialization Data ***/

GFX_INIT gfxInit0 =
{
    .drvInitialize    = DRV_GFX_LCC_Initialize,
    .drvOpen          = DRV_GFX_LCC_Open,
    .drvInterfaceSet  = DRV_GFX_LCC_InterfaceSet,
    .driverInitData.orientation             = DISP_ORIENTATION,
    .driverInitData.horizontalResolution    = DISP_HOR_RESOLUTION,
    .driverInitData.verticalResolution      = DISP_VER_RESOLUTION,
    .driverInitData.dataWidth               = DISP_DATA_WIDTH,
    .driverInitData.horizontalPulseWidth    = DISP_HOR_PULSE_WIDTH,
    .driverInitData.horizontalBackPorch     = DISP_HOR_BACK_PORCH,
    .driverInitData.horizontalFrontPorch    = DISP_HOR_FRONT_PORCH,
    .driverInitData.verticalPulseWidth      = DISP_VER_PULSE_WIDTH,
    .driverInitData.verticalBackPorch       = DISP_VER_BACK_PORCH,
    .driverInitData.verticalFrontPorch      = DISP_VER_FRONT_PORCH,
    .driverInitData.logicShift              = DISP_INV_LSHIFT,
    .driverInitData.LCDType                 = 1,
    .driverInitData.colorType               = 0,
    .driverInitData.TCON_Init               = TCON_MODULE,
};

/*** System DMA Initialization Data ***/

const SYS_DMA_INIT sysDmaInit =
{
	.sidl = SYS_DMA_SIDL_DISABLE,

};

/* System Module Handles */
SYS_MODULE_OBJ  gfxObjectHandle;
SYS_MODULE_OBJ  sysDmaHandle;


/*******************************************************************************
  Function:
    void SYS_Initialize ( SYS_INIT_DATA *data )

  Summary:
    Initializes the board, services, drivers, application and other modules.

  Remarks:
    See prototype in system/common/sys_module.h.
 ******************************************************************************/

void SYS_Initialize ( void* data )
{
   OS_ERR os_err;

   /*initialize RTOS */
   CPU_Init();
   OSInit(&os_err);

   /*setup device to run at maximum performance*/
   SYS_DEVCON_PerformanceConfig(200000000);
  
   /*setup board specific items*/
   BSP_InitIO();
   
   /* Initialize System Services, used by GFX drivers */
   sysDmaHandle = SYS_DMA_Initialize((SYS_MODULE_INIT *)&sysDmaInit);
	 
   /* Initialize GFX Library */
   gfxObjectHandle = GFX_Initialize(GFX_INDEX_0, (SYS_MODULE_INIT *)&gfxInit0);

   /*the graphics display refresh task function is called from an ISR in this
   system, so setup the priority the ISR will run at*/
   PLIB_INT_MultiVectorSelect(INT_ID_0);
   PLIB_INT_VectorPrioritySet(INT_ID_0,INT_VECTOR_DMA1, INT_PRIORITY_LEVEL3);
   PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_DMA_1);

   /*setup rtos tick interrupt source, for 1ms tick*/
   __builtin_mtc0(11,0,100000);
   /*start counting from zero*/
   __builtin_mtc0(9,0,0);
   /*clear interrupt flag, before enabling*/
   PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_CORE);
   /*enable interrupt source and set IPL for core timer*/
   PLIB_INT_VectorPrioritySet(INT_ID_0,INT_VECTOR_CT,INT_PRIORITY_LEVEL2);
   PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_TIMER_CORE);

   /*create system_tasks*/
   OSTaskCreate((OS_TCB      *)&System_GFX_Task_TCB,
                (CPU_CHAR    *)"Sys GFX Task",
                (OS_TASK_PTR  )System_GFX_Task,
                (void        *)0,
                (OS_PRIO      )SYSTEM_GFX_TASK_PRIO,
                (CPU_STK     *)&System_GFX_Task_Stk[0],
                (CPU_STK_SIZE )SYSTEM_GFX_TASK_STK_SIZE_LIMIT,
                (CPU_STK_SIZE )SYSTEM_GFX_TASK_STK_SIZE,
                (OS_MSG_QTY   )0u,
                (OS_TICK      )0u,
                (void        *)0,
                (OS_OPT       )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                (OS_ERR      *)&os_err);

 
   /* Initialize the Application */
   APP_Initialize();
}



