/*
*********************************************************************************************************
*                                              uC/OS-III
*                                        The Real-Time Kernel
*
*                          (c) Copyright 2004-2010; Micrium, Inc.; Weston, FL               
*
*               All rights reserved.  Protected by international copyright laws.
*********************************************************************************************************
*/
#ifndef  __APP__H
#define  __APP__H

/*
*********************************************************************************************************
*                                            INCLUDES
*********************************************************************************************************
*/
#include <stdbool.h>
#include <stddef.h>

/*
*********************************************************************************************************
*                                            STRUCTURE, ENUM, UNION DEFINITIONS
*********************************************************************************************************
*/



/*
*********************************************************************************************************
*                                            TASK PRIORITIES
*********************************************************************************************************
*/

#define  LEDBLINK_TASK_PRIO                              6u
#define  DISPLAY_TASK_PRIO                               5u
#define  SYSTEM_GFX_TASK_PRIO                            4u
/*
*********************************************************************************************************
*                                            TASK STACK SIZES
*********************************************************************************************************
*/

#define  LEDBLINK_TASK_STK_SIZE                          1024u
#define  DISPLAY_TASK_STK_SIZE                           1024u
#define  SYSTEM_GFX_TASK_STK_SIZE                        1024u

/*
*********************************************************************************************************
*                                            TASK STACK SIZES LIMIT
*********************************************************************************************************
*/
#define SYSTEM_GFX_TASK_STK_SIZE_PCT_FULL   90u
#define SYSTEM_GFX_TASK_STK_SIZE_LIMIT      ((SYSTEM_GFX_TASK_STK_SIZE * (100u - SYSTEM_GFX_TASK_STK_SIZE_PCT_FULL))   / 100u)

#define DISPLAY_TASK_STK_SIZE_PCT_FULL      90u
#define DISPLAY_TASK_STK_SIZE_LIMIT         ((DISPLAY_TASK_STK_SIZE * (100u - DISPLAY_TASK_STK_SIZE_PCT_FULL))   / 100u)

#define LEDBLINK_TASK_STK_SIZE_PCT_FULL     90u
#define LEDBLINK_TASK_STK_SIZE_LIMIT        ((LEDBLINK_TASK_STK_SIZE * (100u - LEDBLINK_TASK_STK_SIZE_PCT_FULL))   / 100u)
/*
*********************************************************************************************************
*                                            FUNCTION PROTOTYPES
*********************************************************************************************************
*/
void SYS_Initialize ( void* data );
void APP_Initialize ( void );

void  System_GFX_Task (void *p_arg);
//void InterruptHandler_GFX ( void );

/*
*********************************************************************************************************
*                                            SEMAPHORES
*********************************************************************************************************
*/


#endif
