#include    <xc.h>
#include    <GenericTypeDefs.h>
#include    "app.h"
#include    "gfx/gfx.h"
#include    "os.h"

/*
*********************************************************************************************************
*                                                VARIABLES
*********************************************************************************************************
*/
extern SYS_MODULE_OBJ  gfxObjectHandle;


/*                                                                                                       
*********************************************************************************************************
*                                          SYS_Tasks()                                               
*                                                                                                        
* Description : Starts the OS, never returns                                                              
*                                                                                                        
* Argument(s) : none                                                                                     
*                                                                                                        
* Return(s)   : none                                                                                     
*                                                                                                        
* Caller(s)   : main                                                                           
*                                                                                                        
* Note(s)     : none.                                                                                    
*********************************************************************************************************
*/   
void SYS_Tasks (void)
{
   OS_ERR os_err;
   OSStart(&os_err);       /* Start multitasking (i.e. give control to uC/OS-III).     */

   (void)&os_err;
}


/*                                                                                                       
*********************************************************************************************************
*                                          SystemTask()                                               
*                                                                                                        
* Description : Maintain GFX state machine, this can run very slow since we directly use primitive
*               functions, which directly go to display buffer.
*                                                                                                        
* Argument(s) : none                                                                                     
*                                                                                                        
* Return(s)   : none                                                                                     
*                                                                                                        
* Caller(s)   : OS called after waiting time delay()                                                                           
*                                                                                                         
* Note(s)     : none.                                                                                    
*********************************************************************************************************
*/   
void  System_GFX_Task (void *p_arg)
{
   OS_ERR err;
   while (1) 
   {                                          
      /* Maintain Graphics Library */
      GFX_Tasks(gfxObjectHandle);
      OSTimeDly(500,OS_OPT_TIME_DLY,&err);
   }
}