#include <xc.h>


   .global  BSP_TickHandler
   .extern  IFS0CLR
/*
*********************************************************************************************************
*                                          RTOS ISR Tick Handler
*
* Description : Reloads the timer used to source the RTOS tick.  For this system the RTOS tick runs from
*               MIPS core timer, and is set to 1mSec.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : core timer interrupt vector wrapper, see crt0.S and Micrium's uC/OS-III os_cpu_a.S
*
* Note(s)     : none.
*********************************************************************************************************
*/  
   .section .text,code
   .set  noreorder
   .set  noat
   .set  nomips16
   .set  nomicromips
   .ent  BSP_TickHandler
	        
BSP_TickHandler:
   mtc0  $0, $9, 0                          /* clear core timer register                              */
   li    $8, 100000                          /* count value based on sys clock running at 100Mhz       */
   mtc0  $8, $11                            /* reload the core timer period register                  */
   ehb
   
   li    $8, 1                              /* Clear core timer interrupt                             */
   la    $9, IFS0CLR
   sw    $8, ($9)
   
   jr    $31
   nop

   .end BSP_TickHandler