/*******************************************************************************
 System Initialization File

  File Name:
    sys_init.c

  Summary:
    System Initialization.

  Description:
    This file contains source code necessary to initialize the system.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2011-2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

/*
*********************************************************************************************************
*                                                INCLUDES
*********************************************************************************************************
*/
#include    <xc.h>
#include    "os.h"
#include    "app.h"
#include    "system/int/sys_int.h"
#include    "system/dma/sys_dma.h"
#include    "system_config.h"
#include    "bsp.h"
#include    "peripheral/osc/plib_osc.h"
#include    "peripheral/int/plib_int.h"
#include    "peripheral/devcon/plib_devcon.h"
#include    "driver/gfx/controller/ssd1926/drv_gfx_ssd1926.h"
#include    "gfx/gfx.h"

/*
*********************************************************************************************************
*                                                CONFIGURATION WORDS
*********************************************************************************************************
*/

/*** DEVCFG0 ***/
#pragma config DEBUG =      OFF
#pragma config ICESEL =     ICS_PGx2
#pragma config PWP =        OFF
#pragma config BWP =        OFF
#pragma config CP =         OFF

/*** DEVCFG1 ***/
#pragma config FNOSC =      PRIPLL
#pragma config FSOSCEN =    OFF
#pragma config IESO =       OFF
#pragma config POSCMOD =    HS
#pragma config OSCIOFNC =   ON
#pragma config FPBDIV =     DIV_2
#pragma config FCKSM =      CSDCMD
#pragma config WDTPS =      PS1048576
#pragma config FWDTEN =     OFF

/*** DEVCFG2 ***/
#pragma config FPLLIDIV =   DIV_1
#pragma config FPLLMUL =    MUL_20
#pragma config FPLLODIV =   DIV_2
#pragma config UPLLIDIV =   DIV_2
#pragma config UPLLEN =     OFF

/*** DEVCFG3 ***/
#pragma config USERID =     0xffff
#pragma config FSRSSEL =    PRIORITY_7
#pragma config FMIIEN =     ON
#pragma config FETHIO =     ON
#pragma config FCANIO =     ON
#pragma config FUSBIDIO =   ON
#pragma config FVBUSONIO =  ON




/*
*********************************************************************************************************
*                                                VARIABLES
*********************************************************************************************************
*/
OS_TCB      System_GFX_Task_TCB;
CPU_STK     System_GFX_Task_Stk[SYSTEM_GFX_TASK_STK_SIZE];

/*** GFX Initialization Data ***/

DRV_PMP_INIT     pmpInit =
{
    .polarity.addressLatchPolarity = PMP_POLARITY_ACTIVE_HIGH,
    .polarity.rwStrobePolarity = PMP_POLARITY_ACTIVE_LOW,
    .polarity.writeEnableStrobePolarity = PMP_POLARITY_ACTIVE_LOW,
    .polarity.chipselect1Polarity = PMP_POLARITY_ACTIVE_HIGH,
    .polarity.chipselect2Polarity = PMP_POLARITY_ACTIVE_LOW,
    .ports.readWriteStrobe = PORT_ENABLE,
    .ports.writeEnableStrobe = PORT_ENABLE,
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .pmpID            = PMP_ID_0,
    .stopInIdle       = false,
    .muxMode          = PMP_MUX_NONE
};

/*** GFX Initialization Data ***/

GFX_INIT gfxInit0 =
{
    .drvInitialize    = DRV_GFX_SSD1926_Initialize,
    .drvOpen          = DRV_GFX_SSD1926_Open,
    .drvInterfaceSet  = DRV_GFX_SSD1926_InterfaceSet,
    .driverInitData.orientation             = DISP_ORIENTATION,
    .driverInitData.horizontalResolution    = DISP_HOR_RESOLUTION,
    .driverInitData.verticalResolution      = DISP_VER_RESOLUTION,
    .driverInitData.dataWidth               = DISP_DATA_WIDTH,
    .driverInitData.horizontalPulseWidth    = DISP_HOR_PULSE_WIDTH,
    .driverInitData.horizontalBackPorch     = DISP_HOR_BACK_PORCH,
    .driverInitData.horizontalFrontPorch    = DISP_HOR_FRONT_PORCH,
    .driverInitData.verticalPulseWidth      = DISP_VER_PULSE_WIDTH,
    .driverInitData.verticalBackPorch       = DISP_VER_BACK_PORCH,
    .driverInitData.verticalFrontPorch      = DISP_VER_FRONT_PORCH,
    .driverInitData.logicShift              = DISP_INV_LSHIFT,
    .driverInitData.LCDType                 = 1,
    .driverInitData.colorType               = 0,
    .driverInitData.TCON_Init               = TCON_MODULE,
};


/* System Module Handles */
SYS_MODULE_OBJ  gfxObjectHandle;


/*******************************************************************************
  Function:
    void SYS_Initialize ( SYS_INIT_DATA *data )

  Summary:
    Initializes the board, services, drivers, application and other modules.

  Remarks:
    See prototype in system/common/sys_module.h.
 ******************************************************************************/

void SYS_Initialize ( void* data )
{
   OS_ERR os_err;

   /*initialize RTOS */
   CPU_Init();
   OSInit(&os_err);
   
   /*setup device to run at maximum performance*/
   SYS_DEVCON_PerformanceConfig(80000000);

   /*setup board specific items*/
   BSP_InitIO();
   
   /* System Services Initialization */    
   SYS_INT_Initialize();  

   /*setup rtos tick interrupt source, for 1ms tick*/
   __builtin_mtc0(11,0,40000);
   /*start counting from zero*/
   __builtin_mtc0(9,0,0);
   /*clear interrupt flag, before enabling*/
   PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_CORE);
   /*enable interrupt source and set IPL for core timer*/
   PLIB_INT_VectorPrioritySet(INT_ID_0,INT_VECTOR_CT,INT_PRIORITY_LEVEL2);
   PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_TIMER_CORE);

   DRV_PMP_Initialize (DRV_PMP_INDEX_0, (SYS_MODULE_INIT *)&pmpInit);

   /* Initialize GFX Library */
   gfxObjectHandle = GFX_Initialize(GFX_INDEX_0, (SYS_MODULE_INIT *)&gfxInit0);

   /*create system_tasks*/
   OSTaskCreate((OS_TCB      *)&System_GFX_Task_TCB,
                (CPU_CHAR    *)"Sys GFX Task",
                (OS_TASK_PTR  )System_GFX_Task,
                (void        *)0,
                (OS_PRIO      )SYSTEM_GFX_TASK_PRIO,
                (CPU_STK     *)&System_GFX_Task_Stk[0],
                (CPU_STK_SIZE )SYSTEM_GFX_TASK_STK_SIZE_LIMIT,
                (CPU_STK_SIZE )SYSTEM_GFX_TASK_STK_SIZE,
                (OS_MSG_QTY   )0u,
                (OS_TICK      )0u,
                (void        *)0,
                (OS_OPT       )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                (OS_ERR      *)&os_err);
   
   /* Initialize the Application */
   APP_Initialize();
}



