/*******************************************************************************
  MPLAB Harmony System Configuration Header

  File Name:
    system_config.h

  Summary:
    Build-time configuration header for the system defined by this MPLAB Harmony
    project.

  Description:
    An MPLAB Project may have multiple configurations.  This file defines the
    build-time options for a single configuration.

  Remarks:
    This configuration header must not define any prototypes or data
    definitions (or include any files that do).  It only provides macro
    definitions for build-time configuration options that are not instantiated
    until used by another MPLAB Harmony module or application.
    
    Created with MPLAB Harmony Version 1.00
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

#ifndef _SYSTEM_CONFIG_H
#define _SYSTEM_CONFIG_H

// *****************************************************************************
// *****************************************************************************
// Section: System Service Configuration
// *****************************************************************************
// *****************************************************************************


/*** Common System Service Configuration ***/

#define SYS_BUFFER  false
#define SYS_QUEUE   false


// *****************************************************************************
/* Device Control System Service Configuration Options
*/

#define SYS_DEVCON_SYSTEM_CLOCK                          200000000



/*** Interrupt System Service Configuration ***/

#define SYS_INT                                          true


// *****************************************************************************
// *****************************************************************************
// Section: Driver Configuration
// *****************************************************************************
// *****************************************************************************


#define GFX_USE_DISPLAY_CONTROLLER_LCC
#define DRV_GFX_CONFIG_LCC_INTERNAL_MEMORY
#define DMA_CHANNEL_INDEX                                1

/*needed by lcc Harmony driver, inform driver of LCD board specifics*/
#define PIXELCLOCK                              LATDbits.LATD5
#define PIXELCLOCK_TRIS                         TRISDbits.TRISD5
#define DATA_ENABLE                             LATBbits.LATB4
#define DATA_ENABLE_TRIS                        TRISBbits.TRISB4
#define HSYNC                                   LATBbits.LATB1
#define HSYNC_TRIS                              TRISBbits.TRISB1
#define VSYNC                                   LATAbits.LATA9
#define VSYNC_TRIS                              TRISAbits.TRISA9
#define BACKLIGHT                               LATFbits.LATF13
#define BACKLIGHT_TRIS                          TRISFbits.TRISF13
#define LCD_RESET                               LATJbits.LATJ3
#define LCD_RESET_TRIS                          TRISJbits.TRISJ3
#define LCD_CS                                  LATJbits.LATJ6
#define LCD_CS_TRIS                             TRISJbits.TRISJ6
#define ADDR19                                  LATKbits.LATK5
#define ADDR19_TRIS                             TRISKbits.TRISK5
#define DISP_ORIENTATION		                  0
#define DISP_HOR_RESOLUTION		               480
#define DISP_VER_RESOLUTION		               272
#define DISP_DATA_WIDTH			                  24
#define DISP_HOR_PULSE_WIDTH                    41
#define DISP_HOR_BACK_PORCH                     2
#define DISP_HOR_FRONT_PORCH                    2
#define DISP_VER_PULSE_WIDTH                    10
#define DISP_VER_BACK_PORCH                     2
#define DISP_VER_FRONT_PORCH                    2
#define DISP_INV_LSHIFT                         0
#define TCON_MODULE                             NULL
#define BACKLIGHT_ENABLE_LEVEL                  1
#define BACKLIGHT_DISABLE_LEVEL                 0
// *****************************************************************************
// *****************************************************************************
// Section: Middleware & Other Library Configuration
// *****************************************************************************
// *****************************************************************************



/*** GFX Library Configuration ***/

#define GFX_INSTANCES_NUMBER                             1
#define GFX_RUN_MODE                                     0
#define GFX_SELF_PREEMPTION_LEVEL                        0
#define GFX_CONFIG_GRADIENT_DISABLE
#define GFX_CONFIG_PALETTE_DISABLE
#define GFX_CONFIG_FONT_ANTIALIASED_DISABLE
#define GFX_CONFIG_TRANSPARENT_COLOR_DISABLE
#define GFX_CONFIG_PALETTE_EXTERNAL_DISABLE
#define GFX_CONFIG_FOCUS_DISABLE
#define GFX_CONFIG_FONT_CHAR_SIZE                        8
#define GFX_CONFIG_FONT_EXTERNAL_DISABLE
#define GFX_CONFIG_FONT_RAM_DISABLE
#define GFX_CONFIG_IMAGE_EXTERNAL_DISABLE
#define GFX_CONFIG_IMAGE_RAM_DISABLE
#define GFX_CONFIG_COLOR_DEPTH                           16
#define GFX_CONFIG_DOUBLE_BUFFERING_DISABLE
#define GFX_CONFIG_USE_KEYBOARD_DISABLE
#define GFX_CONFIG_USE_TOUCHSCREEN_DISABLE
#define GFX_malloc(size)                                 malloc(size)
#define GFX_free(pObj)                                   free(pObj)


#define OSAL_USE_RTOS                                    1




#endif // _SYSTEM_CONFIG_H
/*******************************************************************************
 End of File
*/

