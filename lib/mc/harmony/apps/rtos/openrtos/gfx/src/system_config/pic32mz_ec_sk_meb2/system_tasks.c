#include    <xc.h>
#include    <GenericTypeDefs.h>
#include    "app_cfg.h"
#include    "gfx/gfx.h"
#include    "FreeRTOS.h"

/*
*********************************************************************************************************
*                                                VARIABLES
*********************************************************************************************************
*/
extern SYS_MODULE_OBJ  gfxObjectHandle;


/*                                                                                                       
*********************************************************************************************************
*                                          SYS_Tasks()                                               
*                                                                                                        
* Description : Starts the OS, never returns                                                              
*                                                                                                        
* Argument(s) : none                                                                                     
*                                                                                                        
* Return(s)   : none                                                                                     
*                                                                                                        
* Caller(s)   : main                                                                           
*                                                                                                        
* Note(s)     : none.                                                                                    
*********************************************************************************************************
*/   
void SYS_Tasks (void)
{
   vTaskStartScheduler();       /* Start multitasking (i.e. give control to FreeRTOS).                */
}


/*                                                                                                       
*********************************************************************************************************
*                                          SystemTask()                                               
*                                                                                                        
* Description : Maintain GFX state machine, this can run very slow since we directly use primitive
*               functions, which directly go to display buffer.
*                                                                                                        
* Argument(s) : none                                                                                     
*                                                                                                        
* Return(s)   : none                                                                                     
*                                                                                                        
* Caller(s)   : OS called after waiting time delay()                                                                           
*                                                                                                         
* Note(s)     : none.                                                                                    
*********************************************************************************************************
*/   
void  System_GFX_Task (void *p_arg)
{
   
   while (1) 
   {                                          
      /* Maintain Graphics Library */
      GFX_Tasks(gfxObjectHandle);
      vTaskDelay(5000 / portTICK_RATE_MS);
   }
}