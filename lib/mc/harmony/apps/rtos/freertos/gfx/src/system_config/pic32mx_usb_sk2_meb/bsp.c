#include <xc.h>
#include "peripheral/int/plib_int.h"
#include "peripheral/ports/plib_ports.h"
#include "bsp.h"



/*
*********************************************************************************************************
*                                              LED TOGGLE
*
* Description : This function is used to alternate the state of an LED
*
*********************************************************************************************************
*/
void  LED_Toggle (void)
{
    PLIB_PORTS_Toggle(PORTS_ID_0, PORT_CHANNEL_D, (PORTS_DATA_MASK)0x0E);
    PLIB_PORTS_Toggle(PORTS_ID_0, PORT_CHANNEL_C, (PORTS_DATA_MASK)0x06);
}

/*
*********************************************************************************************************
*                                             BSP_InitIO()
* 
* Description: Initialize all the I/O devices.
*
* Arguments  : None
*
* Returns    : None
*********************************************************************************************************
*/

void  BSP_InitIO (void)    
{
   /* Set up the ports for MEB ports to digital */
   PLIB_PORTS_PinModeSelect(PORTS_ID_0,PORTS_ANALOG_PIN_15,PORTS_PIN_MODE_DIGITAL);
   PLIB_PORTS_PinModeSelect(PORTS_ID_0,PORTS_ANALOG_PIN_0, PORTS_PIN_MODE_DIGITAL);
   PLIB_PORTS_PinModeSelect(PORTS_ID_0,PORTS_ANALOG_PIN_4, PORTS_PIN_MODE_DIGITAL);
   PLIB_PORTS_PinModeSelect(PORTS_ID_0,PORTS_ANALOG_PIN_1, PORTS_PIN_MODE_DIGITAL);
   PLIB_PORTS_PinModeSelect(PORTS_ID_0,PORTS_ANALOG_PIN_3, PORTS_PIN_MODE_DIGITAL);
   
   /* Set the switch pins as input */
   PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 , PORT_CHANNEL_D , PORTS_BIT_POS_6);
   PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 , PORT_CHANNEL_D , PORTS_BIT_POS_7);
   PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 , PORT_CHANNEL_D , PORTS_BIT_POS_13);
   PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 , PORT_CHANNEL_B , PORTS_BIT_POS_15);
   PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 , PORT_CHANNEL_B , PORTS_BIT_POS_0);
   PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 , PORT_CHANNEL_B , PORTS_BIT_POS_4);
   PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 , PORT_CHANNEL_B , PORTS_BIT_POS_1);
   PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 , PORT_CHANNEL_B , PORTS_BIT_POS_3);
   
   /*Set up pins used by CPLD*/
   PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_G, PORTS_BIT_POS_14);
   PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_G, PORTS_BIT_POS_14);
   PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_G, PORTS_BIT_POS_12);
   PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_A, PORTS_BIT_POS_6);
   PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_A, PORTS_BIT_POS_7);
      
    
   /* Set the LED pins as output */    
   PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_0);
   PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_1);
   PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_2);
   PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_3);
   PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_C, PORTS_BIT_POS_1);
   PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_C, PORTS_BIT_POS_2);

   /* Switch off all the LEDS */ 
   PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_0);
   PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_1);
   PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_2);
   PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_3);
   PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_C, PORTS_BIT_POS_1);
   PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_C, PORTS_BIT_POS_2);

   /* Enable pullups on the Switch ports*/
   PLIB_PORTS_ChangeNoticePullUpEnable(PORTS_ID_0, CN15);
   PLIB_PORTS_ChangeNoticePullUpEnable(PORTS_ID_0, CN16);
   PLIB_PORTS_ChangeNoticePullUpEnable(PORTS_ID_0, CN19);
   PLIB_PORTS_ChangeNoticePullUpEnable(PORTS_ID_0, CN2);
   PLIB_PORTS_ChangeNoticePullUpEnable(PORTS_ID_0, CN3);
   PLIB_PORTS_ChangeNoticePullUpEnable(PORTS_ID_0, CN5);
   PLIB_PORTS_ChangeNoticePullUpEnable(PORTS_ID_0, CN6);
   PLIB_PORTS_ChangeNoticePullUpEnable(PORTS_ID_0, CN12);
}

