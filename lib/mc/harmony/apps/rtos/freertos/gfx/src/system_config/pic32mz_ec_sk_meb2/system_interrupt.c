#include <xc.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "peripheral/int/plib_int.h"
#include "driver/gfx/controller/lcc/drv_gfx_lcc.h"

/*
*********************************************************************************************************
*                                          DMA Interrupt Handler()
*
* Description : Calls the appropriate Harmony GFX stack functions needed, based on how we configured this 
*               system.  This function is called in response to a hardware DMA interrupt.  In this system
*               we are using DMA Channel 1, see system_config.h and system_init.c.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : Assembly Interrupt vector wrapper, see system_interrupt_a.S
*
* Note(s)     : none.
*********************************************************************************************************
*/
void DMAInterruptHandler(void)
{
    DRV_GFX_LCC_DisplayRefresh();
    PLIB_INT_SourceFlagClear(INT_ID_0, INT_SOURCE_DMA_1);
}