/*******************************************************************************
  GFX Primitive Demo Application
  
  File Name:
    app.c

  Summary:
    GFX Primitive Demo application

  Description:
    This file contains the GFX Primitive Demo application logic.
 *******************************************************************************/


// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


/*
*********************************************************************************************************
*                                                INCLUDES
*********************************************************************************************************
*/
#include    <xc.h>
#include    <stdbool.h>
#include    "FreeRTOS.h"
#include    "task.h"
#include    "app_cfg.h"
#include    "system_config.h"
#include    <gfx/gfx.h>




/*
*********************************************************************************************************
*                                                DEFINES
*********************************************************************************************************
*/

#define MAXSCREENCOLORS             7
#define ID_SURFACE1                 10
/*
*********************************************************************************************************
*                                                VARIABLES
*********************************************************************************************************
*/

unsigned short ScreenColor[MAXSCREENCOLORS] = {BRIGHTBLUE,BRIGHTGREEN,BRIGHTCYAN,BRIGHTRED,
                                               BRIGHTMAGENTA,SIENNA,GOLD};
                                               
TaskHandle_t          LEDblinkTask_Handle;
TaskHandle_t          DisplayTask_Handle;

GFX_GOL_OBJ_SCHEME AppGOLScheme =
{
    .EmbossDkColor          = GFX_RGBConvert(0x2B, 0x55, 0x87), 
    .EmbossLtColor          = GFX_RGBConvert(0xD4, 0xE4, 0xF7), 
    .TextColor0             = GFX_RGBConvert(0x07, 0x1E, 0x48), 
    .TextColor1             = GFX_RGBConvert(0xFF, 0xFF, 0xFF), 
    .TextColorDisabled      = GFX_RGBConvert( 245,  245,  220), 
    .Color0                 = GFX_RGBConvert(0xA9, 0xDB, 0xEF), 
    .Color1                 = GFX_RGBConvert(0x26, 0xC7, 0xF2), 
    .ColorDisabled          = GFX_RGBConvert(0xB6, 0xD2, 0xFB), 
    .CommonBkColor          = GFX_RGBConvert(0xD4, 0xED, 0xF7), 

    .CommonBkLeft           = 0,                            
    .CommonBkTop            = 0,                            
    .CommonBkType           = GFX_BACKGROUND_COLOR,         
                                                            
    .pCommonBkImage         = NULL,                         

    .pFont                  = NULL,

    .fillStyle              = GFX_FILL_STYLE_COLOR,         

#ifndef GFX_CONFIG_ALPHABLEND_DISABLE
    .AlphaValue             = 100,                          
#endif

    .EmbossSize         = 0       

};
static GFX_GOL_SURFACE*    appDrawingSurface;

/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/
void LEDblinkTask(void *p_arg);
void DisplayTask(void *p_arg);


/*                                                                                                       
*********************************************************************************************************
*                                          App Initialize()                                               
*                                                                                                        
* Description : Initializes the application specific items here, in this case, just needs to create the 
*               application tasks, which do application specific work.
*                                                                                                        
* Argument(s) : none                                                                                     
*                                                                                                        
* Return(s)   : none                                                                                     
*                                                                                                        
* Caller(s)   : OS called after waiting time delay()                                                                           
*                                                                                                        
* Note(s)     : none.                                                                                    
*********************************************************************************************************
*/   
void APP_Initialize ( void )
{
   BaseType_t errStatus;
 
   errStatus = xTaskCreate((TaskFunction_t) LEDblinkTask,
               (const signed char*)"LED Blink Task",
               LEDBLINK_TASK_STK_SIZE,
               NULL,
               LEDBLINK_TASK_PRIO,
               &LEDblinkTask_Handle);

   errStatus = xTaskCreate((TaskFunction_t) DisplayTask,
               (const signed char*)"Display Task",
               DISPLAY_TASK_STK_SIZE,
               NULL,
               DISPLAY_TASK_PRIO,
               &DisplayTask_Handle);   
               
   /*appDrawingSurface = GFX_GOL_SurfaceCreate(GFX_INDEX_0, ID_SURFACE1, 0, 0,
                       GFX_MaxXGet(), GFX_MaxYGet(), GFX_GOL_SURFACE_DRAW_STATE,
                       NULL, (GFX_RESOURCE_HDR *) NULL,
                       (GFX_GOL_OBJ_SCHEME*)&AppGOLScheme);*/


}

/*                                                                                                       
*********************************************************************************************************
*                                          LEDblinkTask()                                               
*                                                                                                        
* Description : Application task, blinks user LED's on a periodic basis.  Lets the user know the RTOS is
*               up and running
* Argument(s) : none                                                                                     
*                                                                                                        
* Return(s)   : none                                                                                     
*                                                                                                        
* Caller(s)   : OS called after waiting time delay()                                                                           
*                                                                                                        
* Note(s)     : none.                                                                                    
*********************************************************************************************************
*/     
void LEDblinkTask (void *p_arg)
{
   
   
   while (1) /* Task body, always written as an infinite loop.       */
   {                        
      LED_Toggle();
      vTaskDelay(500 / portTICK_RATE_MS);
   }
}

void  DisplayTask (void *p_arg)
{
   static unsigned int ScreenColor_Index = 0;
   while(1)   
   {

      //taskENTER_CRITICAL();
      __builtin_disable_interrupts();
      GFX_ColorSet(GFX_INDEX_0, ScreenColor[ScreenColor_Index++]);
      GFX_ScreenClear(0);
      //taskEXIT_CRITICAL();
      __builtin_enable_interrupts();

      if(ScreenColor_Index == MAXSCREENCOLORS)
         ScreenColor_Index = 0;

      vTaskDelay(500 / portTICK_RATE_MS);
   }

}

/*
*********************************************************************************************************
*                                          vApplicationStackOverflowHook()
*
* Description : Hook function called by FreeRTOS if a stack overflow happens.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : APP_StateReset()
*
* Note(s)     : none.
*********************************************************************************************************
*/
void vApplicationStackOverflowHook( TaskHandle_t pxTask, signed char *pcTaskName )
{
   ( void ) pcTaskName;
   ( void ) pxTask;
   
   /* Run time task stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook	function is
   called if a task stack overflow is detected.  Note the system/interrupt
   stack is not checked. */
   taskDISABLE_INTERRUPTS();
   for( ;; );
}



