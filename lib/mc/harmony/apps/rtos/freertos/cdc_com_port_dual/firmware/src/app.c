/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"


// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

APP_DATA appData;

/* Read Data Buffer */
uint8_t com1ReadBuffer[APP_READ_BUFFER_SIZE] APP_MAKE_BUFFER_DMA_READY;

/* Write Data Buffer. Size is same as read buffer size */
uint8_t com1WriteBuffer[1] APP_MAKE_BUFFER_DMA_READY;

/* Read Data Buffer */
uint8_t com2ReadBuffer[APP_READ_BUFFER_SIZE] APP_MAKE_BUFFER_DMA_READY;

/* Write Data Buffer. Size is same as read buffer size */
uint8_t com2WriteBuffer[1] APP_MAKE_BUFFER_DMA_READY;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/*************************************************
 * Application Device Layer Event Handler
 *************************************************/

void APP_USBDeviceEventHandler(USB_DEVICE_EVENT event, void * pData,
                               uintptr_t context)
{
    uint8_t configurationValue;
    portBASE_TYPE xHigherPriorityTaskWoken1 = pdFALSE;

    switch( event )
    {
        case USB_DEVICE_EVENT_POWER_REMOVED:
            /* Attach the device */
            USB_DEVICE_Detach (appData.deviceHandle);
            break;
        case USB_DEVICE_EVENT_RESET:
        case USB_DEVICE_EVENT_DECONFIGURED:

            /* Device was either de-configured or reset */

            /* Update LED indication */
            BSP_LEDOn ( APP_USB_LED_1);
            BSP_LEDOn ( APP_USB_LED_2);
            BSP_LEDOff ( APP_USB_LED_3);
            break;

        case USB_DEVICE_EVENT_CONFIGURED:

            /* pData will point to the configuration. Check the configuration */
            configurationValue = ((USB_DEVICE_EVENT_DATA_CONFIGURED *)pData)->configurationValue;
            if(configurationValue == 1)
            {
                /* Register the CDC Device application event handler here.
                 * Note how the appData object pointer is passed as the
                 * user data */

                USB_DEVICE_CDC_EventHandlerSet(COM1,
                        APP_USBDeviceCDCEventHandler, (uintptr_t)&appData);

                USB_DEVICE_CDC_EventHandlerSet(COM2,
                        APP_USBDeviceCDCEventHandler, (uintptr_t)&appData);

                /* Update LED indication */
                BSP_LEDOff ( APP_USB_LED_1 );
                BSP_LEDOff ( APP_USB_LED_2 );
                BSP_LEDOn ( APP_USB_LED_3 );
            }
            xSemaphoreGiveFromISR(appData.xSemaphoreBlockUsbConfigure, &xHigherPriorityTaskWoken1);
            portEND_SWITCHING_ISR( xHigherPriorityTaskWoken1 );
            break;

        case USB_DEVICE_EVENT_SUSPENDED:
            /* Update LED indication */
            BSP_LEDOff ( APP_USB_LED_1 );
            BSP_LEDOn ( APP_USB_LED_2 );
            BSP_LEDOn ( APP_USB_LED_3 );
            break;

        case USB_DEVICE_EVENT_RESUMED:
            break;
        case USB_DEVICE_EVENT_POWER_DETECTED:
            /* Attach the device */
            USB_DEVICE_Attach (appData.deviceHandle);
            break;
        case USB_DEVICE_EVENT_ERROR:
        default:
            break;
    }
}


/************************************************
 * CDC Function Driver Application Event Handler
 ************************************************/

USB_DEVICE_CDC_EVENT_RESPONSE APP_USBDeviceCDCEventHandler
(
    USB_DEVICE_CDC_INDEX index ,
    USB_DEVICE_CDC_EVENT event ,
    void* pData,
    uintptr_t userData
)
{

    APP_DATA * appDataObject;
    appDataObject = (APP_DATA *)userData;
    USB_CDC_CONTROL_LINE_STATE * controlLineStateData;
    uint16_t * breakData;
    portBASE_TYPE xHigherPriorityTaskWoken1 = pdFALSE;

    switch ( event )
    {
        case USB_DEVICE_CDC_EVENT_GET_LINE_CODING:

            /* This means the host wants to know the current line
             * coding. This is a control transfer request. Use the
             * USB_DEVICE_ControlSend() function to send the data to
             * host.  */

            USB_DEVICE_ControlSend(appDataObject->deviceHandle,
                    &appDataObject->appCOMPortObjects[index].getLineCodingData,
                    sizeof(USB_CDC_LINE_CODING));

            break;

        case USB_DEVICE_CDC_EVENT_SET_LINE_CODING:

            /* This means the host wants to set the line coding.
             * This is a control transfer request. Use the
             * USB_DEVICE_ControlReceive() function to receive the
             * data from the host */

            USB_DEVICE_ControlReceive(appDataObject->deviceHandle,
                    &appDataObject->appCOMPortObjects[index].setLineCodingData,
                    sizeof(USB_CDC_LINE_CODING));

            break;

        case USB_DEVICE_CDC_EVENT_SET_CONTROL_LINE_STATE:

            /* This means the host is setting the control line state.
             * Read the control line state. We will accept this request
             * for now. */

            controlLineStateData = (USB_CDC_CONTROL_LINE_STATE *)pData;
            appDataObject->appCOMPortObjects[index].controlLineStateData.dtr
                    = controlLineStateData->dtr;
            appDataObject->appCOMPortObjects[index].controlLineStateData.carrier
                    = controlLineStateData->carrier;

            USB_DEVICE_ControlStatus(appDataObject->deviceHandle, USB_DEVICE_CONTROL_STATUS_OK);

            break;

        case USB_DEVICE_CDC_EVENT_SEND_BREAK:

            /* This means that the host is requesting that a break of the
             * specified duration be sent. Read the break duration */

            breakData = (uint16_t *)pData;
            appDataObject->appCOMPortObjects[index].breakData = *breakData;
            break;

        case USB_DEVICE_CDC_EVENT_READ_COMPLETE:

            /* This means that the host has sent some data*/
            xSemaphoreGiveFromISR
            (
                appData.appCOMPortObjects[index].xSemaphoreCDCReadComplete,
                &xHigherPriorityTaskWoken1
            );
            break;

        case USB_DEVICE_CDC_EVENT_CONTROL_TRANSFER_DATA_RECEIVED:

            /* The data stage of the last control transfer is
             * complete. For now we accept all the data */

            USB_DEVICE_ControlStatus(appDataObject->deviceHandle, USB_DEVICE_CONTROL_STATUS_OK);
            break;

        case USB_DEVICE_CDC_EVENT_CONTROL_TRANSFER_DATA_SENT:

            /* This means the GET LINE CODING function data is valid. We don't
             * do much with this data in this demo. */

            break;

        case USB_DEVICE_CDC_EVENT_WRITE_COMPLETE:
            break;

        default:
            break;
    }
    return USB_DEVICE_CDC_EVENT_RESPONSE_NONE;
}



// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************

void APP_USB_DEVICE_AttachTask(void);
void APP_CDC1Read(void);
void APP_CDC1Write(void);
void APP_CDC2Read(void);
void APP_CDC2Write(void);
// *****************************************************************************
/* Function:
    void APP_USB_DEVICE_AttachTask(void)

  Summary:
    It is an RTOS task for Attaching and Configuring USB Device to Host.

  Description:
    This function is an RTOS task for attaching the USB Device to Host. Following
 are the actions done by this Task.
 1) Open an instance of Harmony USB Device Framework by periodically calling
    (in every 1 milli Second) USB_DEVICE_Open()function until Harmony USB Device
     framework is successfully opened.
 2) If the USB Device Framework is opened successfully pass an application event
    Handler to the USB framework for receiving USB Device Events.
 3) Attach to the USB Host by calling USB attach function.
 4) Acquire a binary semaphore to wait until USB Host Configures the Device. The
    semaphore is released when a USB_DEVICE_EVENT_CONFIGURED event is received at
    the USB Device event handler.
 5) Resume all CDC read/write tasks.
 6) Suspend USB attach task.

  Returns:
     None
*/
void APP_USB_DEVICE_AttachTask(void)
{
    /* Create the semaphore used to signal that USB Device is configured. */
    appData.xSemaphoreBlockUsbConfigure = xSemaphoreCreateBinary();

    /* Create a semaphore used to Signal the CDC1_Read Task */
    appData.appCOMPortObjects[COM1].xSemaphoreCDCReadComplete = xSemaphoreCreateBinary();

    /* Create a semaphore used to Signal the CDC2_Read Task */
    appData.appCOMPortObjects[COM2].xSemaphoreCDCReadComplete = xSemaphoreCreateBinary();

    /* Create the queue. */
    appData.appCOMPortObjects[COM1].xQueue = xQueueCreate( QUEUE_LENGTH, sizeof( unsigned long ) );
    appData.appCOMPortObjects[COM2].xQueue = xQueueCreate( QUEUE_LENGTH, sizeof( unsigned long ) );

    xTaskCreate((TaskFunction_t) APP_CDC1Read,
        "CDC1_Read",
        USB_CDC1_READ_TASK_SIZE,
        NULL,
        APP_USB_CDC1_READ_TASK_PRIO,
        &appData.appCOMPortObjects[COM1].taskHandleCDCRead);

    xTaskCreate((TaskFunction_t) APP_CDC1Write,
        "CDC1_Write",
        USB_CDC1_WRITE_TASK_SIZE ,
        NULL,
        APP_USB_CDC1_WRITE_TASK_PRIO,
        &appData.appCOMPortObjects[COM1].taskHandleCDCWrite);

    xTaskCreate((TaskFunction_t) APP_CDC2Read,
        "CDC2_Read",
        USB_CDC2_READ_TASK_SIZE,
        NULL,
        APP_USB_CDC2_READ_TASK_PRIO,
        &appData.appCOMPortObjects[COM2].taskHandleCDCRead);

    xTaskCreate((TaskFunction_t) APP_CDC2Write,
        "CDC2_Write",
        USB_CDC2_WRITE_TASK_SIZE,
        NULL,
        APP_USB_CDC2_WRITE_TASK_PRIO,
        &appData.appCOMPortObjects[COM2].taskHandleCDCWrite);

    /* Suspend these tasks as of know. Resume these task only after USB device
     is configured */
    vTaskSuspend (appData.appCOMPortObjects[COM1].taskHandleCDCRead);
    vTaskSuspend (appData.appCOMPortObjects[COM1].taskHandleCDCWrite);
    vTaskSuspend (appData.appCOMPortObjects[COM2].taskHandleCDCRead);
    vTaskSuspend (appData.appCOMPortObjects[COM2].taskHandleCDCWrite);
    appData.deviceHandle = USB_DEVICE_HANDLE_INVALID;
    for(;;)
    {
        /* Open the device layer */
        appData.deviceHandle = USB_DEVICE_Open( USB_DEVICE_INDEX_0,
                    DRV_IO_INTENT_READWRITE );
        if(appData.deviceHandle != USB_DEVICE_HANDLE_INVALID)
        {
            /* Register a callback with device layer to get event notification  */
            USB_DEVICE_EventHandlerSet(appData.deviceHandle,
                    APP_USBDeviceEventHandler, 0);

            /* Attach the device */
            USB_DEVICE_Attach (appData.deviceHandle);

            /* Block on the binary semaphore given by USB Configure event */
            xSemaphoreTake(appData.xSemaphoreBlockUsbConfigure, portMAX_DELAY );

            /* Resume CDC read/write tasks as the USB Device is configured now. */
            vTaskResume(appData.appCOMPortObjects[COM1].taskHandleCDCRead);
            vTaskResume(appData.appCOMPortObjects[COM1].taskHandleCDCWrite);
            vTaskResume(appData.appCOMPortObjects[COM2].taskHandleCDCRead);
            vTaskResume(appData.appCOMPortObjects[COM2].taskHandleCDCWrite);
            vTaskSuspend( NULL );
        }
        vTaskDelay(1 / portTICK_RATE_MS);
    }
}

// *****************************************************************************
/* Function:
    void APP_CDC1Read(void)

  Summary:
    It is an RTOS task for Reading data from an instance of CDC COM1

  Description:
    This function is an RTOS task for reading data from an instance of CDC COM1.
 This task should get executed only if the USB Device is configured.

 Following are the actions done by this Task.
   1) Issue a CDC Read command.
   2) Acquire a binary semaphore to wait for CDC read.
   3) The semaphore is released when a CDC read complete event is received at CDC
      event handler.
   4) Send the received data to the Queue so that data is available for the Write
      Task.

  Returns:
    None
*/
void APP_CDC1Read(void)
{
    USB_DEVICE_CDC_TRANSFER_HANDLE readTransferHandle = USB_DEVICE_CDC_TRANSFER_HANDLE_INVALID;
    appData.appCOMPortObjects[COM1].getLineCodingData.dwDTERate = 9600;
    appData.appCOMPortObjects[COM1].getLineCodingData.bDataBits = 8;
    appData.appCOMPortObjects[COM1].getLineCodingData.bParityType = 0;
    appData.appCOMPortObjects[COM1].getLineCodingData.bCharFormat = 0;
    for(;;)
    {
        /* Schedule a CDC read on COM1 */
        USB_DEVICE_CDC_Read(COM1, &readTransferHandle, com1ReadBuffer,
                                                          APP_READ_BUFFER_SIZE);

        /* Wait for read complete */
        xSemaphoreTake(appData.appCOMPortObjects[COM1].xSemaphoreCDCReadComplete,
                                                                portMAX_DELAY );

        /* Send the data received from COM1 to Queue 1 */
        xQueueSend( appData.appCOMPortObjects[COM1].xQueue, com1ReadBuffer, 0U );
    }
}

// *****************************************************************************
/* Function:
    void APP_CDC1Write(void)

  Summary:
    It is an RTOS task for writing data to an instance of CDC COM1

  Description:
    This function is an RTOS task for writing data to an instance of CDC COM1.
 This task should get executed only if the USB Device is configured.
     Following are the actions done by this Task.
   1) Wait to receive from Queue, for the data from COM2.
   2) Send the received data to COM1.

  Returns:
    None
*/
void APP_CDC1Write(void)
{
    USB_DEVICE_CDC_TRANSFER_HANDLE writeTransferHandle = USB_DEVICE_CDC_TRANSFER_HANDLE_INVALID;;
    for(;;)
    {
        /* Wait on the queue to receive data from CDC2 */
        xQueueReceive( appData.appCOMPortObjects[COM2].xQueue, &com1WriteBuffer, portMAX_DELAY );

        /* Send the received data to COM1 */
        USB_DEVICE_CDC_Write(COM1, &writeTransferHandle, &com1WriteBuffer, 1,
                                  USB_DEVICE_CDC_TRANSFER_FLAGS_DATA_COMPLETE);
    }
}

// *****************************************************************************
/* Function:
    void APP_CDC2Read(void)

  Summary:
    It is an RTOS task for Reading data from an instance of CDC COM2

  Description:
    This function is an RTOS task for reading data from an instance of CDC COM2.
 This task should get executed only if the USB Device is configured.
    Following are the actions done by this Task.
   1) Issue a CDC Read command.
   2) Acquire a binary semaphore to wait for CDC read.
   3) The semaphore is released when a CDC read complete event is received at CDC
      event handler.
   4) Send the received data to the Queue so that data is available for the Write
      Task.

  Returns:
    None
*/
void APP_CDC2Read(void)
{
    USB_DEVICE_CDC_TRANSFER_HANDLE readTransferHandle = USB_DEVICE_CDC_TRANSFER_HANDLE_INVALID;
    appData.appCOMPortObjects[COM2].getLineCodingData.dwDTERate = 9600;
    appData.appCOMPortObjects[COM2].getLineCodingData.bDataBits = 8;
    appData.appCOMPortObjects[COM2].getLineCodingData.bParityType = 0;
    appData.appCOMPortObjects[COM2].getLineCodingData.bCharFormat = 0;
    for(;;)
    {
        /* Schedule a CDC read on COM2 */
        USB_DEVICE_CDC_Read(COM2,&readTransferHandle, com2ReadBuffer,
                                                          APP_READ_BUFFER_SIZE);

        /* wait on a semaphore until data is received on the CDC */
        xSemaphoreTake(appData.appCOMPortObjects[COM2].xSemaphoreCDCReadComplete,
                                                        portMAX_DELAY );

        /* Send the data received from COM2 to Queue 2 */
        xQueueSend( appData.appCOMPortObjects[COM2].xQueue,com2ReadBuffer, 0U );
    }
}

// *****************************************************************************
/* Function:
    void APP_CDC2Write(void)

  Summary:
    It is an RTOS task for writing data to an instance of CDC COM2

  Description:
    This function is an RTOS task for writing data to an instance of CDC COM2.
 This task should get executed only if the USB Device is configured.
     Following are the actions done by this Task.
   1) Wait to receive from Queue, for the data from COM1.
   2) Send the received data to COM2.

  Returns:
    Refer to usb_device.h for usage information.
*/
void APP_CDC2Write(void)
{
    USB_DEVICE_CDC_TRANSFER_HANDLE writeTransferHandle = USB_DEVICE_CDC_TRANSFER_HANDLE_INVALID;
    for(;;)
    {
        /* Wait on the queue to receive data from CDC1 */
        xQueueReceive( appData.appCOMPortObjects[COM1].xQueue, &com2WriteBuffer, portMAX_DELAY );

        /* Send the received data to COM2 */
        USB_DEVICE_CDC_Write(COM2, &writeTransferHandle, &com2WriteBuffer, 1,
                                   USB_DEVICE_CDC_TRANSFER_FLAGS_DATA_COMPLETE);
    }
}


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    appData.state = APP_STATE_INIT;
	/* Initialize the application object */
    appData.deviceHandle = USB_DEVICE_HANDLE_INVALID;
    appData.appCOMPortObjects[COM1].getLineCodingData.dwDTERate = 9600;
    appData.appCOMPortObjects[COM1].getLineCodingData.bDataBits = 8;
    appData.appCOMPortObjects[COM1].getLineCodingData.bParityType = 0;
    appData.appCOMPortObjects[COM1].getLineCodingData.bCharFormat = 0;

    appData.appCOMPortObjects[COM2].getLineCodingData.dwDTERate = 9600;
    appData.appCOMPortObjects[COM2].getLineCodingData.bDataBits = 8;
    appData.appCOMPortObjects[COM2].getLineCodingData.bParityType = 0;
    appData.appCOMPortObjects[COM2].getLineCodingData.bCharFormat = 0;
}


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Tasks ( void )
{
    BaseType_t errStatus;
    errStatus = xTaskCreate((TaskFunction_t) APP_USB_DEVICE_AttachTask,
                "USB_AttachTask",
                USB_ATTACH_TASK_SIZE,
                NULL,
                APP_USB_ATTACH_TASK_PRIO,
                NULL);
}
 

/*******************************************************************************
 End of File
 */

