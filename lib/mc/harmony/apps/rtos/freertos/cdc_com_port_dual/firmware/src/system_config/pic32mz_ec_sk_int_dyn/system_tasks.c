/*******************************************************************************
 System Tasks File

  File Name:
    system_tasks.c

  Summary:
    This file contains source code necessary to maintain system's polled state
    machines.

  Description:
    This file contains source code necessary to maintain system's polled state
    machines.  It implements the "SYS_Tasks" function that calls the individual
    "Tasks" functions for all the MPLAB Harmony modules in the system.

  Remarks:
    This file requires access to the systemObjects global data structure that
    contains the object handles to all MPLAB Harmony module objects executing
    polled in the system.  These handles are passed into the individual module
    "Tasks" functions to identify the instance of the module to maintain.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "system_config.h"
#include "system_definitions.h"
#include "app.h"


// *****************************************************************************
// *****************************************************************************
// Section: System "Tasks" Routine
// *****************************************************************************
// *****************************************************************************
void FrameworkTasks (void);
/*******************************************************************************
  Function:
    void SYS_Tasks ( void )

  Remarks:
    See prototype in system/common/sys_module.h.
*/

void SYS_Tasks ( void )
{
   BaseType_t errStatus;

    /* Create System Tasks (for Harmony framework modules)task(s)*/
    errStatus = xTaskCreate((TaskFunction_t) FrameworkTasks,
                "Framework Tasks",
                SYSTEM_TASKS_SIZE,
                NULL,
                SYSTEM_TASKS_PRIO,
                NULL);

      /* Create Application Tasks */
      APP_Tasks();

     /* Start multitasking (i.e. give control to FreeRTOS). This function never
      returns */
    vTaskStartScheduler();
}
/*
*********************************************************************************************************
*                                          FrameworkTasks
*
* Description : RTOS task for running Harmony framework tasks.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : OS called after waiting time delay()
*
* Note(s)     : none.
*********************************************************************************************************
*/
void FrameworkTasks (void)
{
   for(;;)
   {
      /* Maintain system services */
        SYS_DEVCON_Tasks(sysObj.sysDevcon);
    SYS_TMR_Tasks(sysObj.sysTmr);
    
    /* Maintain Device Drivers */
    DRV_TMR_Tasks(sysObj.drvTmr0);

    
    /* Maintain USB Stack */
    /* Device layer tasks routine */ 
    USB_DEVICE_Tasks(sysObj.usbDevObject0);
   }
}

/*******************************************************************************
 End of File
 */

