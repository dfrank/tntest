/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"


// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************
/* This is the string that will written to the file */
uint8_t writeData[15];

/* This is the string that will written to the CDC device */
uint8_t prompt[10] = "\r\nData : ";

/* Application MSD Task Object */
APP_MSD_DATA appMSDData;

/* Application CDC Task Object */
APP_CDC_DATA  appCDCData ;

static uint8_t  count = 0 ;


// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

APP_DATA appData;



// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************
 * USB HOST Layer Events - Host Event Handler
 *******************************************************/
USB_HOST_EVENT_RESPONSE APP_USBHostEventHandler (USB_HOST_EVENTS event, void * eventData, uintptr_t context)
{
    switch(event)
    {
        case USB_HOST_EVENT_VBUS_REQUEST_POWER:
            break;
        case USB_HOST_EVENT_UNSUPPORTED_DEVICE:
            break;
        case USB_HOST_EVENT_CANNOT_ENUMERATE:
            break;
        case USB_HOST_EVENT_CONFIGURATION_COMPLETE:
            break;
        case USB_HOST_EVENT_CONFIGURATION_FAILED:
            break;
        case USB_HOST_EVENT_DEVICE_SUSPENDED:
            break;
        case USB_HOST_EVENT_DEVICE_RESUMED:
            break;
    }
    return USB_HOST_EVENT_RESPONSE_NONE;
}

/*************************************************************
 * CDC Host Application Event Handler function.
 ************************************************************/

USB_HOST_CDC_EVENT_RESPONSE APP_USBHostCDCEventHandler
(
    USB_HOST_CDC_INDEX index,
    USB_HOST_CDC_EVENT event,
    void * eventData,
    uintptr_t context
)
{
    uint8_t deviceAddress;

    USB_HOST_CDC_EVENT_DATA_SET_LINE_CODING_COMPLETE * setLineCodingEventData;
    USB_HOST_CDC_EVENT_DATA_GET_LINE_CODING_COMPLETE * getLineCodingEventData;
    USB_HOST_CDC_EVENT_DATA_SET_CONTROL_LINE_STATE_COMPLETE * setControlLineStateEventData;
    USB_HOST_CDC_EVENT_DATA_READ_COMPLETE * readCompleteEventData;
    USB_HOST_CDC_EVENT_DATA_WRITE_COMPLETE * writeCompleteEventData;

    switch(event)
    {
        case USB_HOST_CDC_EVENT_ATTACH:

            /* The event data in this case is the address of the
             * attached device. */

            /* Initialize the Application */
            APP_CDC_Initialize (&appCDCData );
            appCDCData.state = APP_CDC_STATE_DEVICE_CONNECTED;
            deviceAddress = *((uint8_t *)eventData);
            vTaskResume(appData.taskHandleCDC);
            xSemaphoreGive(appCDCData.xSemaphoreBlockUsbCDCAttach );
            break;

        case USB_HOST_CDC_EVENT_DETACH:

            /* This means the device was detached. There is no event data
             * associated with this event.*/


            vTaskSuspend (appData.taskHandleCDC);
            appCDCData.state = APP_CDC_STATE_WAIT_FOR_DEVICE_ATTACH;
            break;

        case USB_HOST_CDC_EVENT_SET_LINE_CODING_COMPLETE:

            /* This means the Set Line Coding request completed. We can
             * find out if the request was successful. */

            setLineCodingEventData =
                (USB_HOST_CDC_EVENT_DATA_SET_LINE_CODING_COMPLETE *)eventData;

            if(setLineCodingEventData->transferStatus
                    == USB_HOST_CDC_TRANSFER_STATUS_ERROR)
            {
                /* This means the transfer terminated because of an
                 * error. */
                appCDCData.state = APP_CDC_STATE_ERROR;
            }
            else
            {

                /* The application should next send the Set Control Line
                 * state request to the attached device */

                appCDCData.state = APP_CDC_STATE_SEND_SET_CONTROL_LINE_STATE;
            }
            break;

        case USB_HOST_CDC_EVENT_GET_LINE_CODING_COMPLETE:

            /* This means the Get Line Coding request completed. We can
             * find out if the request was successful. */

            getLineCodingEventData =
                (USB_HOST_CDC_EVENT_DATA_GET_LINE_CODING_COMPLETE *)eventData;

            if(getLineCodingEventData->transferStatus
                    == USB_HOST_CDC_TRANSFER_STATUS_ERROR)
            {
                /* This means the transfer terminated because of an
                 * error. */
                appCDCData.state = APP_CDC_STATE_ERROR;
            }
            else
            {
                /* The application should next send the Set Line Coding
                 * request to the device */

                appCDCData.state = APP_CDC_STATE_SEND_SET_LINE_CODING;
            }
            break;

        case USB_HOST_CDC_EVENT_SET_CONTROL_LINE_STATE_COMPLETE:

            /* This means the Set Control Line State request completed. We can
             * find out if the request was successful. */

            setControlLineStateEventData =
                (USB_HOST_CDC_EVENT_DATA_SET_CONTROL_LINE_STATE_COMPLETE *)eventData;

            if(setControlLineStateEventData->transferStatus
                    == USB_HOST_CDC_TRANSFER_STATUS_ERROR)
            {
                /* This means the transfer terminated because of an
                 * error. */
                appCDCData.state = APP_CDC_STATE_ERROR;
            }
            else
            {
                /* The application must now send the prompt message
                 * to the attached device */

                appCDCData.state =  APP_CDC_STATE_SEND_PROMPT_TO_DEVICE;
            }
            break;

        case USB_HOST_CDC_EVENT_SEND_BREAK_COMPLETE:

            /* This event is not used in this application */

            break;

        case USB_HOST_CDC_EVENT_WRITE_COMPLETE:

            /* This means the Write request completed. We can
             * find out if the request was successful. */

            writeCompleteEventData =
                (USB_HOST_CDC_EVENT_DATA_WRITE_COMPLETE *)eventData;

            if(writeCompleteEventData->transferStatus
                    == USB_HOST_CDC_TRANSFER_STATUS_ERROR)
            {
                /* This means there transfer terminated because of an
                   error.*/

                appCDCData.state = APP_CDC_STATE_ERROR;
            }
            else
            {

                /* The application now waits for data from the attache device */

                appCDCData.state = APP_CDC_STATE_GET_DATA_FROM_DEVICE;
            }

            break;

        case USB_HOST_CDC_EVENT_READ_COMPLETE:

            /* This means the Read request completed. We can
             * find out if the request was successful. */

            readCompleteEventData =
                (USB_HOST_CDC_EVENT_DATA_READ_COMPLETE *)eventData;

            if(readCompleteEventData->transferStatus
                    == USB_HOST_CDC_TRANSFER_STATUS_ERROR)
            {
                /* This means there transfer terminated because of an
                 * error. */
                appCDCData.state = APP_CDC_STATE_ERROR;
            }
            else
            {
                /* The application can now process the received data */
                appCDCData.state = APP_CDC_STATE_DATA_RECEIVED_FROM_DEVICE;


            }
            break;

        case USB_HOST_CDC_EVENT_SERIAL_STATE_NOTIFICATION_RECEIVED:

            /* This event is not used in the application */

            break;

        default:
            break;
    }

    return USB_HOST_CDC_EVENT_RESPONSE_NONE;
}

/*******************************************************
 * USB HOSR MSD Layer Events - Application Event Handler
 *******************************************************/
bool APP_USBHostMSDEventHandler(USB_HOST_MSD_INDEX index, USB_HOST_MSD_EVENT event, void* pData)
{
    switch ( event)
    {
        case USB_HOST_MSD_EVENT_ATTACH:

            /* Initialize the Application. Update the MSD Task state
             * and main application state. */

            APP_MSD_Initialize (&appMSDData );
            appMSDData.state =  APP_MSD_STATE_DEVICE_CONNECTED;

            vTaskResume(appData.taskHandleMSD);
            xSemaphoreGive(appMSDData.xSemaphoreBlockUsbMSDAttach );


            break;

        case USB_HOST_MSD_EVENT_DETACH:

            /* Device is detached. Unmount the disk */
            appMSDData.state = APP_MSD_STATE_UNMOUNT_DISK;
          //  vTaskSuspend (appData.taskHandleMSD);

            break;

        default:
            break;
    }

    return true;
}


// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary local functions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    /* Initialize the application state machine */
    appData.state =  APP_STATE_OPEN_HOST_LAYER;
}

/*************************************************
 * Application MSD Task Initialize Function.
 *************************************************/

void APP_MSD_Initialize (APP_MSD_DATA * appMSDData )
{
     appMSDData->state = APP_MSD_STATE_DEVICE_CONNECTED;
}

/*************************************************
 * Application CDC Task Initialize Function.
 *************************************************/

void APP_CDC_Initialize ( APP_CDC_DATA *appCDCData )
{
    /* Initialize the application state machine */
    appCDCData->cdcHostLineCoding.dwDTERate     = APP_HOST_CDC_BAUDRATE_SUPPORTED;
    appCDCData->cdcHostLineCoding.bDataBits     = (uint8_t)APP_HOST_CDC_NO_OF_DATA_BITS;
    appCDCData->cdcHostLineCoding.bParityType   = (uint8_t)APP_HOST_CDC_PARITY_TYPE;
    appCDCData->cdcHostLineCoding.bCharFormat   = (uint8_t)APP_HOST_CDC_STOP_BITS;
    appCDCData->controlLineState.dtr = 0;
    appCDCData->controlLineState.carrier = 0;
}

/************************************************
 * Application Tasks Routine
 ************************************************/
void APP_Tasks(void)
{
    BaseType_t errStatus;
    errStatus = xTaskCreate((TaskFunction_t) APP_USB_Host_Open,
                "USB_AttachTask",
                USB_ATTACH_TASK_SIZE,
                NULL,
                APP_USB_HOST_OPEN_PRIO,
                NULL);

}

void APP_USB_Host_Open( )
{
    BaseType_t errStatus;

     errStatus = xTaskCreate((TaskFunction_t)  APP_MSD_Tasks,
        "MSD",
        USB_MSD_TASK_SIZE,
        NULL,
         APP_USB_MSD_TASK_PRIO ,
        &appData.taskHandleMSD);

    errStatus = xTaskCreate((TaskFunction_t) APP_CDC_Tasks,
        "CDC",
        USB_CDC_TASK_SIZE ,
        NULL,
         APP_USB_CDC_TASK_PRIO ,
        &appData.taskHandleCDC );

     /* Create the semaphore used to signal that USB Device is configured. */
    appCDCData.xSemaphoreBlockUsbCDCAttach = xSemaphoreCreateBinary();
    appMSDData.xSemaphoreBlockUsbMSDAttach = xSemaphoreCreateBinary();
    appCDCData.xSemaphoreCDCReadComplete = xSemaphoreCreateBinary();
     vTaskSuspend (appData.taskHandleMSD);
     vTaskSuspend (appData.taskHandleCDC);
    for(;;)
    {
        /* Open the host layer and then enable Host layer operation */
            appData.hostHandle = USB_HOST_Open(USB_HOST_INDEX_0, DRV_IO_INTENT_EXCLUSIVE);

            if (appData.hostHandle != USB_HOST_HANDLE_INVALID)
            {
                /* Host layer was opened successfully. Enable operation
                 * and then wait for operation to be enabled  */

                USB_HOST_OperationEnable(appData.hostHandle );
		USB_HOST_EventCallBackSet(appData.hostHandle,APP_USBHostEventHandler , 0 );
                USB_HOST_MSD_EventHandlerSet (APP_USBHostMSDEventHandler );
                USB_HOST_CDC_EventHandlerSet (APP_USBHostCDCEventHandler );
                appData.state = APP_STATE_WAIT_FOR_HOST_ENABLE;
               
              
                vTaskSuspend (NULL );
            }
            else
            {
                vTaskDelay(1 / portTICK_RATE_MS);
            }


    }
}


void APP_MSD_Tasks ( )
{
    /* The application task state machine */
    while( 1)
    {

        /* Block on thappMSDData.e binary semaphore given by USB Configure event */

    switch(appMSDData.state)
    {
        case APP_MSD_STATE_WAIT_FOR_DEVICE_ATTACH:

            /* Wait for device attach. The state machine will move
             * to the next state when the attach event
             * is received.  */

            break;

        case APP_MSD_STATE_DEVICE_CONNECTED:

            /* Command request was successful. Wait for the
             * command request to complete */
         xSemaphoreTake(appMSDData.xSemaphoreBlockUsbMSDAttach , portMAX_DELAY );
            appMSDData.state = APP_MSD_STATE_MOUNT_DISK;

            break;

        case  APP_MSD_STATE_MOUNT_DISK:

            if(SYS_FS_Mount("/dev/sda1", "/mnt/myDrive", FAT, 0, NULL) != 0)
            {
                /* The disk could not be mounted. Try
                 * mounting again untill success. */

                appMSDData.state = APP_MSD_STATE_MOUNT_DISK;
            }
            else
            {
                /* Mount was successful. */
                appMSDData.state = APP_MSD_STATE_OPEN_FILE;
            }
            break;

        case APP_MSD_STATE_UNMOUNT_DISK:

            /* Update the LED status and unmount the disk */

            BSP_LEDOff(APP_USB_LED_2);

             if(SYS_FS_Unmount("/mnt/myDrive") != 0)
            {
                /* The disk could not be un mounted. Try
                 * un mounting again untill success. */

                appData.state = APP_MSD_STATE_UNMOUNT_DISK;
                break;
            }
            else
            {
                /* UnMount was successful. Wait for device attach */
                appData.state =  APP_MSD_STATE_WAIT_FOR_DEVICE_ATTACH;

            }
            
            vTaskSuspend (appData.taskHandleMSD);

            break;

        case APP_MSD_STATE_OPEN_FILE:

            xSemaphoreTake(appCDCData.xSemaphoreCDCReadComplete , portMAX_DELAY );
            appMSDData.fileHandle = SYS_FS_FileOpen("/mnt/myDrive/file.txt", (SYS_FS_FILE_OPEN_APPEND_PLUS));
            if(appMSDData.fileHandle == SYS_FS_HANDLE_INVALID)
            {
                /* Could not open the file. Error out*/
                appMSDData.state = APP_MSD_STATE_ERROR;
            }
            else
            {
                /* Could not open the file. Error out*/
                appMSDData.state = APP_MSD_STATE_WRITE_TO_FILE;
            }
            break;

        case APP_MSD_STATE_WRITE_TO_FILE:

          
            /* If read was success, try writing to the new file */
            if (SYS_FS_FileWrite( appMSDData.fileHandle, (const void *) writeData, count ) == -1)
            {
                /* Write was not successful. Close the file
                 * and error out.*/
                SYS_FS_FileClose(appMSDData.fileHandle);
                appMSDData.state = APP_MSD_STATE_ERROR;
            }
            else
            {
                appMSDData.state = APP_MSD_STATE_CLOSE_FILE;
            }

            break;

        case APP_MSD_STATE_CLOSE_FILE:

            /* Close the file */
            SYS_FS_FileClose(appMSDData.fileHandle);

            /* The test was successful. Lets idle. */
            appMSDData.state = APP_MSD_STATE_IDLE;
            break;

        case APP_MSD_STATE_IDLE:

            /* The appliction comes here when the demo
             * has completed successfully. Switch on
             * green LED. */

            BSP_LEDOn(APP_USB_LED_2);
            appMSDData.state = APP_MSD_STATE_OPEN_FILE;
            appCDCData.state =  APP_CDC_STATE_SEND_PROMPT_TO_DEVICE;
            
            break;

        case APP_MSD_STATE_ERROR:

            /* The appliction comes here when the demo
             * has failed. Switch on the red LED.*/

            BSP_LEDOn(APP_USB_LED_1);
            break;

        default:
            break;
    }
     vTaskDelay(1 / portTICK_RATE_MS);
    }
}


/**********************************************
 * Application CDC Task Routine.
 ***********************************************/

void APP_CDC_Tasks()
{
    USB_HOST_CDC_RESULT result;
    uint8_t  temp;
    uint8_t  i;
    

    while(1)
    {
    switch (appCDCData.state)
    {
        case APP_CDC_STATE_WAIT_FOR_DEVICE_ATTACH:

            /* Wait for device attach. The state machine will move
             * to the next state when the USB_HOST_CDC_EVENT_ATTACH
             * is received. The application state is update in the
             * CDC Host event handler */

            break;

        case APP_CDC_STATE_DEVICE_CONNECTED:

            /* CDC Device was attached. Send a Get Line Coding
             * command to the attached device and then wait
             * for the command to complete */

            xSemaphoreTake(appCDCData.xSemaphoreBlockUsbCDCAttach , portMAX_DELAY );
            result = USB_HOST_CDC_LineCodingGet(USB_HOST_CDC_INDEX_0,
                    &appCDCData.getLinecodeTransferHandle, &appCDCData.cdcDeviceLineCoding);

            if(USB_HOST_CDC_RESULT_OK == result)
            {
                /* Command request was successful. Wait for the
                 * command request to complete */

                appCDCData.state = APP_CDC_STATE_WAIT_FOR_GET_LINE_CODING;
            }
            else
            {
                /* There was an error in the command request.
                 * Goto error state */

                appCDCData.state = APP_CDC_STATE_ERROR;
            }

            break;

        case APP_CDC_STATE_WAIT_FOR_GET_LINE_CODING:

            /* The application waits here for the Get Line
             * coding request to be complete. This state
             * is updated in the
             * USB_HOST_CDC_EVENT_GET_LINE_CODING_COMPLETE event */

            break;

        case APP_CDC_STATE_SEND_SET_LINE_CODING:

            /* In this state, the application sends the
             * Set Line Coding request to the device. */

            result = USB_HOST_CDC_LineCodingSet(USB_HOST_CDC_INDEX_0,
                    &appCDCData.setLinecodeTransferHandle, &appCDCData.cdcHostLineCoding);

            if(USB_HOST_CDC_RESULT_OK == result)
            {
                /* Command request was successful. Wait for the
                 * command request to complete */

                appCDCData.state = APP_CDC_STATE_WAIT_FOR_SET_LINE_CODING;
            }
            else
            {
                /* There was an error in the command request.
                 * Goto error state */

                appCDCData.state = APP_CDC_STATE_ERROR;
            }

            break;

        case APP_CDC_STATE_WAIT_FOR_SET_LINE_CODING:

            /* The application waits here for the Set Line
             * coding request to be complete. This state
             * is updated in the
             * USB_HOST_CDC_EVENT_SET_LINE_CODING_COMPLETE event */

            break;

        case APP_CDC_STATE_SEND_SET_CONTROL_LINE_STATE :

            result = USB_HOST_CDC_ControlLineStateSet(USB_HOST_CDC_INDEX_0,
                    &appCDCData.transferHandle, &appCDCData.controlLineState);

            if(USB_HOST_CDC_RESULT_OK == result)
            {
                /* Command request was successful. Wait for the
                 * command request to complete */

                appCDCData.state = APP_CDC_STATE_WAIT_FOR_SET_CONTROL_LINE_STATE;
            }
            else
            {
                /* There was an error in the command request.
                 * Goto error state */

                appCDCData.state = APP_CDC_STATE_ERROR;
            }

            break;

        case APP_CDC_STATE_WAIT_FOR_SET_CONTROL_LINE_STATE:

            /* The application waits here for the Set Control
             * Line State request to be complete. This state
             * is updated in the
             * USB_HOST_CDC_EVENT_SET_CONTROL_LINE_STATE_COMPLETE
             * event */
            break;

        case APP_CDC_STATE_SEND_PROMPT_TO_DEVICE:

            /* In this state, the application sends a prompt
             * message to the attached CDC device. */

             BSP_LEDOff(APP_USB_LED_2);
             count = 0;

             for( i = 0; i < 15 ; i++ )
             {
                 writeData[i] = 0 ;
             }

            result = USB_HOST_CDC_Write(USB_HOST_CDC_INDEX_0, &appCDCData.transferHandle, prompt, 8);

            if(USB_HOST_CDC_RESULT_OK == result)
            {
                /* Command request was successful. Wait for the
                 * command request to complete */

               
                appCDCData.state = APP_CDC_STATE_WAIT_FOR_PROMPT_SEND_COMPLETE;
            }
            else
            {
                /* There was an error in the command request.
                 * Goto error state */

                appCDCData.state = APP_CDC_STATE_ERROR;
            }

            break;

        case APP_CDC_STATE_WAIT_FOR_PROMPT_SEND_COMPLETE:

            
            /* The application waits here for the prompt
             * to be send to the device. The state gets
             * updated in the
             * USB_HOST_CDC_EVENT_WRITE_COMPLETE event */

            break;

        case APP_CDC_STATE_GET_DATA_FROM_DEVICE:

            /* Now the application waits for some data from
             * the device. This data will indicate which
             * LED on the PIC32 starter kit should be
             * switched ON */

            result = USB_HOST_CDC_Read(USB_HOST_CDC_INDEX_0,
                                &appCDCData.transferHandle, appCDCData.inDataArray, 1);

            if(USB_HOST_CDC_RESULT_OK == result)
            {
                /* Command request was successful. Wait for the
                 * command request to complete */

                appCDCData.state = APP_CDC_STATE_WAIT_FOR_DATA_FROM_DEVICE;
            }
            else
            {
                /* There was an error in the command request.
                 * Goto error state */

                appCDCData.state = APP_CDC_STATE_ERROR;
            }

            break;

        case APP_CDC_STATE_WAIT_FOR_DATA_FROM_DEVICE:

            /* Here the application waits for data to
             * arrive from the attached device. This
             * state get updated in the
             * USB_HOST_CDC_EVENT_READ_COMPLETE event */

            break;

        case APP_CDC_STATE_DATA_RECEIVED_FROM_DEVICE:

            /* We got data from the attached device.
             * This should be a number; 1, 2 or 3 which
             * which decides which LED on the PIC32 starter
             * kit should be switched on */

            temp = appCDCData.inDataArray[0];

           writeData[count] =  appCDCData.inDataArray[0] ;

            count++;
            if ( temp == 13 || count == 13 )
            {

                 writeData[count] = ' ';
                 count++;

                xSemaphoreGive(appCDCData.xSemaphoreCDCReadComplete );
                
            }


            /* Send the prompt to the device and wait
             * for data again */
            appCDCData.state =  APP_CDC_STATE_GET_DATA_FROM_DEVICE ;

            break;

        case APP_CDC_STATE_ERROR:

            /* Some error has occurred */

            break;
        default:
            break;
    }

    vTaskDelay(1 / portTICK_RATE_MS);
    }

}




/*******************************************************************************
 End of File
 */

