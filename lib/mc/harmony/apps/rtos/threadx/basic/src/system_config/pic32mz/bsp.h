#ifndef __BSP_H
#define __BSP_H

/*based on bit position in PORTx register, not an enumeration list*/
#define USER_LED1       1
#define USER_LED2       2
#define USER_LED3       4

void ToggleLED (unsigned int led_bit_pos);
void BSP_InitIO(void);
void SetLED (unsigned int led_bit_pos);
void ClearLED (unsigned int led_bit_pos);
void BSP_ReloadTimer(void);
void BSP_SetupTimer(void);
#endif