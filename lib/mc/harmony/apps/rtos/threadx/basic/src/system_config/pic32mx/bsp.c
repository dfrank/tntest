#include <xc.h>

void BSP_InitIO(void)
{
   /*configure pins tied to LEDS as outputs*/
   TRISDCLR = 0x00000007;
   /*clear port pins tied to LEDS, turn LEDS off initially*/
   PORTDCLR = 0x00000007;
}

/*written for PIC32 starter kits*/
void ToggleLED (unsigned int led_bit_pos)
{
   PORTDINV = led_bit_pos;
}

void SetLED (unsigned int led_bit_pos)
{
   PORTDSET = led_bit_pos;
}

void ClearLED (unsigned int led_bit_pos)
{
   PORTDCLR = led_bit_pos;
}

void BSP_ReloadTimer(void)
{
   unsigned int interrupt_save;
   /*do this operation atomically*/
   interrupt_save = __builtin_disable_interrupts();

   /*make sure the core timer starts counting next period*/
   __builtin_mtc0(11,0,(__builtin_mfc0(9,0) + 40000));
   /*clear core timer interrupt flag*/
   IFS0CLR = 0x00000001;

   __builtin_mtc0(12,0,interrupt_save);
}

void BSP_SetupTimer(void)
{
   /*
   * core timer is enabled in crt0.S, file included by XC32 compiler,
   * responsible for initializing C runtime environment.  The reset vector
   * jumps to the entry of crt0.S.  crt0.S will enable core timer to start
   * counting.  Core timer increments every other CPU clock cycle.  This is
   * designed as part of the MIPS core.
   *
   * The CPU is set to run at 80MHz, per configuartion words, see
   * config_words.c.  Therefore, the core timer counts as a rate of 13ns
   * (40MHz).
   *
   */

   /*setup core timer priority, IPL = 2*/
   IPC0 = 0x0000008;
   /*setup CP0 compare register, to yield a 1ms timer tick*/
   __builtin_mtc0(11,0,40000);
   /*make sure the core timer starts counting from zero*/
   __builtin_mtc0(9,0,0);
   /*clear core timer interrupt flag, incase it has been triggered since
   counting started during crt0.S*/
   IFS0CLR = 0x00000001;
   /*enable core timer interrupt*/
   IEC0SET = 0x00000001;

}
