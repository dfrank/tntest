/*******************************************************************************
  Sample Application

  File Name:
    app.c

  Summary:
    

  Description:
    
 *******************************************************************************/


// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

//#include "system_config.h"
#include "app.h"
#include "system_config.h"

// Include all headers for any enabled tcpip functions
#include "tcpip/tcpip.h"


#include "app_commands.h"
#include "config.h"
#include "system/tmr/sys_tmr.h"
#include <cyassl/ssl.h>
#include <cyassl/internal.h>

int32_t _APP_ParseUrl(char *uri, char **host, char **path);
int8_t _APP_PumpDNS(const char * hostname, IPV4_ADDR *ipv4Addr);

// *****************************************************************************
// *****************************************************************************
// Section: Global Variable Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    Application strings and buffers are be defined outside this structure.
*/
APP_DATA appData;

// *****************************************************************************
/* Driver objects.

  Summary:
    Contains driver objects.

  Description:
    This structure contains driver objects returned by the driver init routines
    to the application. These objects are passed to the driver tasks routines.
*/

APP_DRV_OBJECTS appDrvObjects;


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize( void )
{

    appData.state = APP_TCPIP_WAIT_INIT;

    APP_Commands_Init();
    TCPIP_WSSL_Initialize();

}


/*******************************************************************************
  Function:
    void APP_Tasks( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Tasks( void )
{
    SYS_STATUS          tcpipStat;
    const char          *netName, *netBiosName;
    static IPV4_ADDR    dwLastIP[2] = { {-1}, {-1} };
    IPV4_ADDR           ipAddr;
    int                 i, nNets;
    TCPIP_NET_HANDLE    netH;

    switch(appData.state)
    {
        case APP_TCPIP_WAIT_INIT:
            tcpipStat = TCPIP_STACK_Status(appDrvObjects.tcpipObject);
            if(tcpipStat < 0)
            {   // some error occurred
                SYS_CONSOLE_MESSAGE(" APP: TCP/IP stack initialization failed!\r\n");
                appData.state = APP_TCPIP_ERROR;
            }
            else if(tcpipStat == SYS_STATUS_READY)
            {
                // now that the stack is ready we can check the 
                // available interfaces 
                nNets = TCPIP_STACK_NumberOfNetworksGet();
                for(i = 0; i < nNets; i++)
                {

                    netH = TCPIP_STACK_IndexToNet(i);
                    netName = TCPIP_STACK_NetNameGet(netH);
                    netBiosName = TCPIP_STACK_NetBIOSName(netH);

#if defined(TCPIP_STACK_USE_NBNS)
                    SYS_CONSOLE_PRINT("    Interface %s on host %s - NBNS enabled\r\n", netName, netBiosName);
#else
                    SYS_CONSOLE_PRINT("    Interface %s on host %s - NBNS disabled\r\n", netName, netBiosName);
#endif  // defined(TCPIP_STACK_USE_NBNS)

                }
                appData.state = APP_TCPIP_WAIT_FOR_IP;

            }
            break;

        case APP_TCPIP_WAIT_FOR_IP:

            // if the IP address of an interface has changed
            // display the new value on the system console
            nNets = TCPIP_STACK_NumberOfNetworksGet();

            for (i = 0; i < nNets; i++)
            {
                netH = TCPIP_STACK_IndexToNet(i);
                ipAddr.Val = TCPIP_STACK_NetAddress(netH);
                if(dwLastIP[i].Val != ipAddr.Val)
                {
                    dwLastIP[i].Val = ipAddr.Val;

                    SYS_CONSOLE_MESSAGE(TCPIP_STACK_NetNameGet(netH));
                    SYS_CONSOLE_MESSAGE(" IP Address: ");
                    SYS_CONSOLE_PRINT("%d.%d.%d.%d \r\n", ipAddr.v[0], ipAddr.v[1], ipAddr.v[2], ipAddr.v[3]);
                    if (ipAddr.v[0] != 0 && ipAddr.v[0] != 169) // Wait for a Valid IP
                    {
                        appData.state = APP_TCPIP_WAITING_FOR_COMMAND;
                        SYS_CONSOLE_MESSAGE("Waiting for command type: openurl <url>\r\n");
                    }
                }
            }
            break;

        case APP_TCPIP_WAITING_FOR_COMMAND:
        {
            // if the IP address of an interface has changed
            // display the new value on the system console
            nNets = TCPIP_STACK_NumberOfNetworksGet();

            for (i = 0; i < nNets; i++)
            {
                netH = TCPIP_STACK_IndexToNet(i);
                ipAddr.Val = TCPIP_STACK_NetAddress(netH);
                if(dwLastIP[i].Val != ipAddr.Val)
                {
                    dwLastIP[i].Val = ipAddr.Val;

                    SYS_CONSOLE_MESSAGE(TCPIP_STACK_NetNameGet(netH));
                    SYS_CONSOLE_MESSAGE(" IP Address: ");
                    SYS_CONSOLE_PRINT("%d.%d.%d.%d \r\n", ipAddr.v[0], ipAddr.v[1], ipAddr.v[2], ipAddr.v[3]);
                    if (ipAddr.v[0] != 0 && ipAddr.v[0] != 169) // Wait for a Valid IP
                    {
                        //appData.state = APP_TCPIP_WAITING_FOR_COMMAND;
                        SYS_CONSOLE_MESSAGE("Waiting for command type: openurl <url>\r\n");
                    }
                }
            }
        }
        {
            if (APP_URL_Buffer[0] != '\0')
            {
                if (_APP_ParseUrl(APP_URL_Buffer, &appData.host, &appData.path))
                {
                    SYS_CONSOLE_PRINT("Could not parse URL '%s'\r\n", APP_URL_Buffer);
                    APP_URL_Buffer[0] = '\0';
                    break;
                }

                appData.testStart = SYS_TMR_TickCountGet();
                appData.dnsComplete = 0;
                appData.connectionOpened = 0;
                appData.sslConnected = 0;
                appData.firstPacketReceived = 0;
                appData.lastPacketReceived = 0;


                if (APP_URL_Buffer[4] == 's')
                {
                    appData.sslInfo.testStart = &appData.testStart;
                    appData.sslInfo.dnsComplete = &appData.dnsComplete;
                    appData.sslInfo.connectionOpened = &appData.connectionOpened;
                    appData.sslInfo.sslConnected = &appData.sslConnected;
                    appData.sslInfo.firstPacketReceived = &appData.firstPacketReceived;
                    appData.sslInfo.lastPacketReceived = &appData.lastPacketReceived;
                    // https
                    appData.state = APP_TCPIP_OPENING_SSL;
                    TCPIP_WSSL_CreateConnection(&appData.sslInfo, appData.host, 443);
                    break;
                }

                TCPIP_DNS_RESULT result = TCPIP_DNS_Resolve(appData.host, DNS_TYPE_A);
                if (result != DNS_RES_OK)
                {
                    SYS_CONSOLE_MESSAGE("Error in DNS aborting\r\n");
                    APP_URL_Buffer[0] = '\0';
                    break;
                }
                appData.state = APP_TCPIP_WAIT_ON_DNS;
                APP_URL_Buffer[0] = '\0';
            }
            if (APP_Cmd_Timings != 0)
            {
                uint32_t delta = ((appData.dnsComplete - appData.testStart) * 1000) / SYS_TMR_TickCounterFrequencyGet();
                SYS_CONSOLE_PRINT("Dns Complete: %d ms\r\n", delta);
                delta = ((appData.connectionOpened - appData.dnsComplete) * 1000) / SYS_TMR_TickCounterFrequencyGet();
                SYS_CONSOLE_PRINT("Connect Opened Complete: %d ms\r\n", delta);
                delta = ((appData.sslConnected - appData.connectionOpened) * 1000) / SYS_TMR_TickCounterFrequencyGet();
                SYS_CONSOLE_PRINT("SSL Negotiation Complete: %d ms\r\n", delta);
                delta = ((appData.firstPacketReceived - appData.sslConnected) * 1000) / SYS_TMR_TickCounterFrequencyGet();
                SYS_CONSOLE_PRINT("First Secure Packet Complete: %d ms\r\n", delta);
                delta = ((appData.lastPacketReceived - appData.firstPacketReceived) * 1000) / SYS_TMR_TickCounterFrequencyGet();
                SYS_CONSOLE_PRINT("Last Secure Packet Complete: %d ms\r\n", delta);
                APP_Cmd_Timings = 0;



            }
        }
        break;

        case APP_TCPIP_WAIT_ON_DNS:
        {
            IPV4_ADDR addr;
            switch (_APP_PumpDNS(appData.host, &addr))
            {
                case -1:
                {
                    // Some sort of error, already reported
                    appData.state = APP_TCPIP_WAITING_FOR_COMMAND;
                }
                break;
                case 0:
                {
                    // Still waiting
                }
                break;
                case 1:
                {
                    appData.dnsComplete = SYS_TMR_TickCountGet();
                    appData.socket = TCPIP_TCP_ClientOpen(IP_ADDRESS_TYPE_IPV4,
                                                          TCPIP_HTTP_SERVER_PORT,
                                                          (IP_MULTI_ADDRESS*) &addr);
                    if (appData.socket == INVALID_SOCKET)
                    {
                        SYS_CONSOLE_MESSAGE("Could not start connection\r\n");
                        appData.state = APP_TCPIP_WAITING_FOR_COMMAND;
                    }
                    //SYS_CONSOLE_MESSAGE("Starting connection\r\n");
                    appData.state = APP_TCPIP_WAIT_FOR_CONNECTION;
                }
                break;
            }
        }
        break;

        case APP_TCPIP_WAIT_FOR_CONNECTION:
        {
            char buffer[MAX_URL_SIZE];
            if (!TCPIP_TCP_IsConnected(appData.socket))
            {
                break;
            }
            if(TCPIP_TCP_PutIsReady(appData.socket) == 0)
            {
                break;
            }
            appData.connectionOpened = SYS_TMR_TickCountGet();
            appData.sslConnected = SYS_TMR_TickCountGet();
            sprintf(buffer, "GET /%s HTTP/1.1\r\n"
                    "Host: %s\r\n"
                    "Connection: close\r\n\r\n", appData.path, appData.host);

            appData.bytesTransfered = TCPIP_TCP_ArrayPut(appData.socket, (uint8_t*)buffer, strlen(buffer));
            appData.state = APP_TCPIP_WAIT_FOR_RESPONSE;
        }
        break;

        case APP_TCPIP_WAIT_FOR_RESPONSE:
        {
            char buffer[180];
            memset(buffer, 0, sizeof(buffer));
            if (!TCPIP_TCP_IsConnected(appData.socket))
            {
                SYS_CONSOLE_MESSAGE("\r\nConnection Closed\r\n");
                SYS_CONSOLE_PRINT("Raw Data RX: %d TX: %d\n", appData.bytesReceived, appData.bytesTransfered);
                appData.bytesReceived = 0;
                appData.bytesTransfered = 0;
                appData.state = APP_TCPIP_WAITING_FOR_COMMAND;
                break;
            }
            if (TCPIP_TCP_GetIsReady(appData.socket))
            {
                if (appData.firstPacketReceived == 0)
                {
                    appData.firstPacketReceived = SYS_TMR_TickCountGet();
                }
                appData.lastPacketReceived = SYS_TMR_TickCountGet();
                appData.bytesReceived += TCPIP_TCP_ArrayGet(appData.socket, (uint8_t*)buffer, sizeof(buffer) - 1);
                //SYS_CONSOLE_MESSAGE(buffer);
            }
        }
        break;
        case APP_TCPIP_OPENING_SSL:
        {
            TCPIP_WSSL_PumpConnection(&appData.sslInfo);
            if (appData.sslInfo.state == WOLF_SSL_CS_SSL_CONNECTED)
            {
             char buffer[MAX_URL_SIZE];
             sprintf(buffer, "GET /%s HTTP/1.1\r\n"
                    "Host: %s\r\n"
                    "Connection: close\r\n\r\n", appData.path, appData.host);
             int ret;
             if ((ret = CyaSSL_write(appData.sslInfo.ssl, buffer, strlen(buffer))) != strlen(buffer))
             {
                  SYS_CONSOLE_PRINT("Could not send to server, got a return %d\n", ret);
             }
             appData.bytesTransfered = ret;
             appData.state = APP_TCPIP_WAITING_FOR_SSL_RESPONSE;
            }
             break;
        }

        case APP_TCPIP_WAITING_FOR_SSL_RESPONSE:
        {
            char buffer[180];
            memset(buffer, 0, sizeof(buffer));

            if (!TCPIP_TCP_IsConnected(appData.socket))
            {
                TCPIP_WSSL_CloseConnection(&appData.sslInfo);
                SYS_CONSOLE_MESSAGE("\r\nConnection Closed\r\n");
                SYS_CONSOLE_PRINT("Raw Data RX: %d TX: %d\n", appData.bytesReceived, appData.bytesTransfered);
                appData.bytesReceived = 0;
                appData.bytesTransfered = 0;
                appData.state = APP_TCPIP_WAITING_FOR_COMMAND;
                APP_URL_Buffer[0] = '\0';
                break;
            }


            int ret = CyaSSL_read(appData.sslInfo.ssl, buffer, sizeof(buffer));
            if (ret < 0)
            {
                int error = CyaSSL_get_error(appData.sslInfo.ssl, ret);
                if ((error == SSL_ERROR_WANT_READ) ||
                    (error == SSL_ERROR_WANT_WRITE))
                {
                    //This is fine go along
                }
                else
                {
                    TCPIP_WSSL_CloseConnection(&appData.sslInfo);
                    APP_URL_Buffer[0] = '\0';
                }
            }
            else
            {
                if (appData.firstPacketReceived == 0)
                {
                    appData.firstPacketReceived = SYS_TMR_TickCountGet();
                }
                appData.lastPacketReceived = SYS_TMR_TickCountGet();
                // we got something
                //SYS_CONSOLE_PRINT("%s", buffer);
                appData.bytesReceived += ret;
            }

        }
        default:
            break;
    }
}


int32_t _APP_ParseUrl(char *uri, char **host, char **path)
{
    char * pos;
    pos = strstr(uri, "//"); //Check to see if its a proper URL

    if ( !pos )
    {
        return -1;
    }
    *host = pos + 2; // This is where the host should start

    pos = strchr( * host, '/');

    if ( !pos )
    {
        *path = NULL;
    }
    else
    {
        * pos = '\0';
        *path = pos + 1;
    }
    return 0;
}

int8_t _APP_PumpDNS(const char * hostname, IPV4_ADDR *ipv4Addr)
{
    TCPIP_DNS_RESULT result = TCPIP_DNS_IsResolved(hostname, ipv4Addr);
    switch (result)
    {
        case DNS_RES_OK:
        {
            // We now have an IPv4 Address
            // Open a socket
            return 1;
        }
        case DNS_RES_PENDING:
            return 0;
        case DNS_RES_SERVER_TMO:
        case DNS_RES_NO_ENTRY:
        default:
            SYS_CONSOLE_MESSAGE("TCPIP_DNS_IsResolved returned failure\r\n");
            return -1;
    }
    // Should not be here!
    return -1;

}


/*******************************************************************************
 End of File
*/

