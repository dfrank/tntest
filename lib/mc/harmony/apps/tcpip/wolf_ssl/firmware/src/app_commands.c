/*******************************************************************************
  Sample Application

  File Name:
    app_commands.c

  Summary:
    commands for the tcp client demo app.

  Description:
    
 *******************************************************************************/


// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END

#include "tcpip/tcpip.h"
#include "app_commands.h"
#include "app.h"
#include "wolf_ssl_glue.h"
#include "config.h"
#include <cyassl/ssl.h>
#include <cyassl/internal.h>


#if defined(TCPIP_STACK_COMMAND_ENABLE)



static int _APP_Commands_OpenURL(_CMDIO_DEV_NODE* pCmdIO, int argc, char** argv);
static int _APP_Commands_WolfSSLDebugging(_CMDIO_DEV_NODE* pCmdIO, int argc, char** argv);
static int _APP_Commands_Timings(_CMDIO_DEV_NODE* pCmdIO, int argc, char** argv);

static const _SYS_CMD_DCPT    appCmdTbl[]=
{
    {"openurl",     _APP_Commands_OpenURL,              ": Connect to a url and do a GET"},
    {"wolfssldebug",     _APP_Commands_WolfSSLDebugging,              ": Enable and disable the debug statements"},
    {"timing",     _APP_Commands_Timings,              ": Turn on and off timings"},

};

bool APP_Commands_Init()
{
    if (!SYS_COMMAND_ADDGRP(appCmdTbl, sizeof(appCmdTbl)/sizeof(*appCmdTbl), "app", ": app commands"))
    {
        SYS_ERROR(SYS_ERROR_ERROR, "Failed to create TCPIP Commands\r\n");
        return false;
    }
    memset(APP_URL_Buffer, 0, MAX_URL_SIZE);
    APP_Cmd_Timings = 0;
    return true;
}

char APP_URL_Buffer[MAX_URL_SIZE];

int _APP_Commands_OpenURL(_CMDIO_DEV_NODE* pCmdIO, int argc, char** argv)
{
    const void* cmdIoParam = pCmdIO->cmdIoParam;


    if (argc != 2)
    {
        (*pCmdIO->pCmdApi->msg)(cmdIoParam, "Usage: openurl <url>\r\n");
        (*pCmdIO->pCmdApi->msg)(cmdIoParam, "Ex: openurl http://www.google.com/\r\n");
        return false;
    }

    strncpy(APP_URL_Buffer, argv[1], MAX_URL_SIZE);
    return true;
}

int _APP_Commands_WolfSSLDebugging(_CMDIO_DEV_NODE* pCmdIO, int argc, char** argv)
{
    const void* cmdIoParam = pCmdIO->cmdIoParam;

#if 0//defined(DEBUG_CYASSL)
    (*pCmdIO->pCmdApi->msg)(cmdIoParam, "wolfssl debuging is not enabled\r\n");
    return false;
#else
    if (argc != 2)
    {
        (*pCmdIO->pCmdApi->msg)(cmdIoParam, "Usage: wolfssldebug <on|off>\r\n");
        (*pCmdIO->pCmdApi->msg)(cmdIoParam, "Ex: wolfssldebug on");
        return false;
    }
    switch(argv[1][1])
    {
        case 'n':
        {
            CyaSSL_Debugging_ON();
            return true;
        }
        case 'f':
        {
            CyaSSL_Debugging_OFF();
            return true;
        }
        default:
        (*pCmdIO->pCmdApi->msg)(cmdIoParam, "Usage: wolfssldebug <on|off>\r\n");
        (*pCmdIO->pCmdApi->msg)(cmdIoParam, "Ex: wolfssldebug on");
        return false;
    }
#endif
}

uint8_t APP_Cmd_Timings;

int _APP_Commands_Timings(_CMDIO_DEV_NODE* pCmdIO, int argc, char** argv)
{

    APP_Cmd_Timings = 1;

    return true;
}


#endif
