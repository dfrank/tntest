/*******************************************************************************
Hardware specific definitions

  Summary:

  Description:
    - PIC32 Ethernet Starter Kit
    - PIC32MX795F512L
*******************************************************************************/

/*******************************************************************************
File Name:  hardware_config.h
Copyright � 2012 released Microchip Technology Inc.  All rights
reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/

#ifndef __HARDWARE_PROFILE_H
#define __HARDWARE_PROFILE_H

#include <xc.h>

// using the PIC32 internal MAC interface
#define TCPIP_IF_PIC32INT

// Hardware I/O pin mappings
// LEDs
#define LED0_TRIS           (TRISHbits.TRISH0)  // Ref LED1
#define LED0_IO             (LATHbits.LATH0)
#define LED1_TRIS           (TRISHbits.TRISH1)  // Ref LED2
#define LED1_IO             (LATHbits.LATH1)
#define LED2_TRIS           (TRISHbits.TRISH2)  // Ref LED3
#define LED2_IO             (LATHbits.LATH2)
#define LED3_TRIS           (LED2_TRIS)         // No such LED
#define LED3_IO             (LATDbits.LATD6)
#define LED4_TRIS           (LED2_TRIS)         // No such LED
#define LED4_IO             (LATDbits.LATD6)
#define LED5_TRIS           (LED2_TRIS)         // No such LED
#define LED5_IO             (LATDbits.LATD6)
#define LED6_TRIS           (LED2_TRIS)         // No such LED
#define LED6_IO             (LATDbits.LATD6)
#define LED7_TRIS           (LED2_TRIS)         // No such LED
#define LED7_IO             (LATDbits.LATD6)

#define LED_GET()           ((uint8_t)LATH & 0x07)
#define LED_PUT(a)          do{LATH = (LATH & 0xFFF8) | ((a)&0x07);}while(0)

// Momentary push buttons
#define BUTTON0_TRIS        (TRISBbits.TRISB12)  // Ref SW1
#define BUTTON0_IO          (PORTBbits.RB12)
#define BUTTON1_TRIS        (TRISBbits.TRISB13)  // Ref SW2
#define BUTTON1_IO          (PORTBbits.RB13)
#define BUTTON2_TRIS        (TRISBbits.TRISB14) // Ref SW3
#define BUTTON2_IO          (PORTBbits.RB14)
#define BUTTON3_TRIS        (TRISBbits.TRISD13) // No BUTTON3 on this board
#define BUTTON3_IO          (1)


#endif // #ifndef HARDWARE_PROFILE_H

