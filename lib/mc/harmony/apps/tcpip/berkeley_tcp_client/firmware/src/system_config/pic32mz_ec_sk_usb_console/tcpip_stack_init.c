/*******************************************************************************
 System Initialization File

  File Name:
    tcpip_stack_init.c

  Summary:
    TCP/IP Stack Initialization.

  Description:
    This file contains source code necessary to initialize the tcp/ip stack.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END

#include "tcpip_config.h"
#include "tcpip_stack_init.h"
#include "tcpip/tcpip.h"
#include "tcpip_stack_config.h"

// Arp Configuration
const TCPIP_ARP_MODULE_CONFIG arpConfigData =
{
    TCPIP_ARP_CACHE_ENTRIES,
    true,
    TCPIP_ARP_CACHE_SOLVED_ENTRY_TMO,
    TCPIP_ARP_CACHE_PENDING_ENTRY_TMO,
    TCPIP_ARP_CACHE_PENDING_RETRY_TMO,
    TCPIP_ARP_CACHE_PERMANENT_QUOTA,
    TCPIP_ARP_CACHE_PURGE_THRESHOLD,
    TCPIP_ARP_CACHE_PURGE_QUANTA,
    TCPIP_ARP_CACHE_ENTRY_RETRIES,
    TCPIP_ARP_GRATUITOUS_PROBE_COUNT,
};

#if defined(TCPIP_STACK_USE_DNS)
const TCPIP_DNS_CLIENT_MODULE_CONFIG dnsConfigData = 
{
    true,
    TCPIP_DNS_CLIENT_CACHE_ENTRIES,
    TCPIP_DNS_CLIENT_CACHE_ENTRY_TMO,
    TCPIP_DNS_CLIENT_CACHE_PER_IPV4_ADDRESS,
    TCPIP_DNS_CLIENT_OPEN_ADDRESS_TYPE,
#ifdef TCPIP_STACK_USE_IPV6
    TCPIP_DNS_CLIENT_CACHE_PER_IPV6_ADDRESS,
#endif	
};
#endif /* TCPIP_STACK_USE_DNS*/

#if defined(TCPIP_STACK_USE_SNTP_CLIENT)
const TCPIP_SNTP_MODULE_CONFIG sntpConfigData = 
{
    TCPIP_NTP_SERVER,
    TCPIP_NTP_DEFAULT_IF,
    TCPIP_NTP_DEFAULT_CONNECTION_TYPE,
    TCPIP_NTP_REPLY_TIMEOUT,
    TCPIP_NTP_TIME_STAMP_TMO,
    TCPIP_NTP_QUERY_INTERVAL,
    TCPIP_NTP_FAST_QUERY_INTERVAL,
};
#endif /* TCPIP_STACK_USE_SNTP_CLIENT*/


#if defined(TCPIP_STACK_USE_DNS_SERVER)
const TCPIP_DNSS_MODULE_CONFIG dnssConfigData = 
{
	true,
    TCPIP_DNSS_REPLY_BOARD_ADDR,
    TCPIP_DNSS_CACHE_PER_IPV4_ADDRESS,
#ifdef TCPIP_STACK_USE_IPV6
    TCPIP_DNSS_CACHE_PER_IPV6_ADDRESS,
#endif	
};
#endif /* TCPIP_STACK_USE_DNS_SERVER */

#if defined(TCPIP_STACK_USE_DHCP_SERVER)
TCPIP_DHCPS_ADDRESS_CONFIG DHCP_POOL_CONFIG[] =
{
    {
        0,  // interface index
// DHCP server Address per interface. DHCP server Address selection should  be in the same subnet.
        TCPIP_DHCPS_DEFAULT_SERVER_IP_ADDRESS,    // Server address
// IPv4 Address range is starting from 100, because the from 1 to 100 is reserved.
// Reserved Address will be used for the gateway address.
// Start address 192.168.1.100 per client.
        TCPIP_DHCPS_DEFAULT_IP_ADDRESS_RANGE_START,  // Start IPv4 Address
// DHCP server subnet Address  per interface.
        TCPIP_DHCPS_DEFAULT_SERVER_NETMASK_ADDRESS,    // subnet net mask
        TCPIP_DHCPS_DEFAULT_SERVER_PRIMARY_DNS_ADDRESS,    // priDNS
        TCPIP_DHCPS_DEFAULT_SERVER_SECONDARY_DNS_ADDRESS,    // secondDNS
        true,
    },
};
const TCPIP_DHCPS_MODULE_CONFIG dhcpsConfigData =
{
    true,
    true,
    TCPIP_DHCPS_LEASE_ENTRIES_DEFAULT,
    TCPIP_DHCPS_LEASE_SOLVED_ENTRY_TMO,
    (TCPIP_DHCPS_ADDRESS_CONFIG*)DHCP_POOL_CONFIG,
};
#endif

#if defined(TCPIP_STACK_USE_DHCP_CLIENT)
const TCPIP_DHCP_MODULE_CONFIG dhcpConfigData =
{
    TCPIP_DHCP_CLIENT_ENABLED,
    TCPIP_DHCP_TIMEOUT,
    TCPIP_DHCP_CLIENT_PORT,
    TCPIP_DHCP_SERVER_PORT,
};
#endif



#if defined(TCPIP_STACK_USE_FTP_SERVER)
const TCPIP_FTP_MODULE_CONFIG ftpConfigData =
{
    TCPIP_FTP_DATA_SKT_TX_BUFF_SIZE,
    TCPIP_FTP_DATA_SKT_RX_BUFF_SIZE,
    "admin",
    "microchip",
};
#endif

#if defined(TCPIP_STACK_USE_HTTP_SERVER)
const TCPIP_HTTP_MODULE_CONFIG httpConfigData =
{
    TCPIP_HTTP_MAX_CONNECTIONS,
    TCPIP_HTTP_MAX_DATA_LEN,
    TCPIP_HTTP_SKT_TX_BUFF_SIZE,
    TCPIP_HTTP_SKT_RX_BUFF_SIZE,
    TCPIP_HTTP_CONFIG_FLAGS,

};
#endif

#if defined(TCPIP_STACK_USE_IPV6)
const TCPIP_IPV6_MODULE_CONFIG ipv6ConfigData ={
    TCPIP_IPV6_RX_FRAGMENTED_BUFFER_SIZE,
    TCPIP_IPV6_FRAGMENT_PKT_TIMEOUT,
};
#endif


const TCPIP_NETWORK_CONFIG __attribute__((unused))  TCPIP_HOSTS_CONFIGURATION[] =
{
    {
        TCPIP_NETWORK_DEFAULT_INTERFACE_NAME,       // interface
        TCPIP_NETWORK_DEFAULT_HOST_NAME,            // hostName
        TCPIP_NETWORK_DEFAULT_MAC_ADDR,             // macAddr
        TCPIP_NETWORK_DEFAULT_IP_ADDRESS,           // ipAddr
        TCPIP_NETWORK_DEFAULT_IP_MASK,              // ipMask
        TCPIP_NETWORK_DEFAULT_GATEWAY,              // gateway
        TCPIP_NETWORK_DEFAULT_DNS,                  // priDNS
        TCPIP_NETWORK_DEFAULT_SECOND_DNS,           // secondDNS
        TCPIP_NETWORK_DEFAULT_POWER_MODE,           // powerMode
        TCPIP_NETWORK_DEFAULT_INTERFACE_FLAGS,      // startFlags
    },
};


const TCPIP_TCP_MODULE_CONFIG  tcpConfigData =
{
    TCPIP_TCP_MAX_SOCKETS,
    TCPIP_TCP_SOCKET_DEFAULT_TX_SIZE,
    TCPIP_TCP_SOCKET_DEFAULT_RX_SIZE,

};

const TCPIP_MODULE_MAC_PIC32INT_CONFIG macPIC32INTConfigData =
{
    TCPIP_EMAC_TX_DESCRIPTORS,    // nTxDescriptors
    TCPIP_EMAC_RX_BUFF_SIZE,      // rxBuffSize
    TCPIP_EMAC_RX_DESCRIPTORS,    // nRxDescriptors
    TCPIP_EMAC_ETH_OPEN_FLAGS, // ethFlags
    TCPIP_EMAC_PHY_CONFIG_FLAGS, // phyFlags
    TCPIP_EMAC_PHY_LINK_INIT_DELAY, // linkInitDelay
    TCPIP_EMAC_PHY_ADDRESS, // phyAddress
};


const TCPIP_MODULE_MAC_MRF24W_CONFIG macMRF24WConfigData =
{
};

#if defined(TCPIP_STACK_USE_UDP)
const TCPIP_UDP_MODULE_CONFIG udpConfigData =
{
    TCPIP_UDP_MAX_SOCKETS,
    TCPIP_UDP_SOCKET_DEFAULT_TX_SIZE,
    TCPIP_UDP_SOCKET_POOL_BUFFERS,
    TCPIP_UDP_SOCKET_POOL_BUFFER_SIZE,
};
#endif


const TCPIP_STACK_MODULE_CONFIG TCPIP_STACK_MODULE_CONFIG_TBL [] =
{
     //ModuleID                  // configData
#if defined(TCPIP_STACK_USE_IPV4)
    {TCPIP_MODULE_IPV4,          0},
#endif
#if defined(TCPIP_STACK_USE_ICMP_CLIENT) || defined(TCPIP_STACK_USE_ICMP_SERVER)
    {TCPIP_MODULE_ICMP,          0},                           // TCPIP_MODULE_ICMP
#endif
    {TCPIP_MODULE_ARP,           &arpConfigData},              // TCPIP_MODULE_ARP
#if defined(TCPIP_STACK_USE_IPV6)
    {TCPIP_MODULE_IPV6,          &ipv6ConfigData},                           // TCPIP_MODULE_IPV6
    {TCPIP_MODULE_ICMPV6,        0},                           // TCPIP_MODULE_ICMPV6
    {TCPIP_MODULE_NDP,           0},                           // TCPIP_MODULE_NDP
#endif
#if defined(TCPIP_STACK_USE_UDP)
    {TCPIP_MODULE_UDP,           &udpConfigData},              // TCPIP_MODULE_UDP,
#endif
#if defined(TCPIP_STACK_USE_TCP)
    {TCPIP_MODULE_TCP,           &tcpConfigData},              // TCPIP_MODULE_TCP,
#endif
#if defined(TCPIP_STACK_USE_DHCP_CLIENT)
    {TCPIP_MODULE_DHCP_CLIENT,   &dhcpConfigData},             // TCPIP_MODULE_DHCP_CLIENT,
#endif
#if defined(TCPIP_STACK_USE_DHCP_SERVER)
    {TCPIP_MODULE_DHCP_SERVER,   &dhcpsConfigData},                           // TCPIP_MODULE_DHCP_SERVER,
#if defined(TCPIP_STACK_USE_AUTOIP)
    {TCPIP_MODULE_AUTOIP,        0},                           // TCPIP_MODULE_AUTOIP
#endif
#endif
#if defined(TCPIP_STACK_USE_ANNOUNCE)
    {TCPIP_MODULE_ANNOUNCE,      0},                           // TCPIP_MODULE_ANNOUNCE,
#endif
#if defined(TCPIP_STACK_USE_DNS)
    {TCPIP_MODULE_DNS_CLIENT,&dnsConfigData}, // TCPIP_MODULE_DNS_CLIENT,
#endif // TCPIP_STACK_USE_DNS

#if defined(TCPIP_STACK_USE_DNS_SERVER)
    {TCPIP_MODULE_DNS_SERVER,&dnssConfigData}, // TCPIP_MODULE_DNS_SERVER,
#endif // TCPIP_STACK_USE_DNS_SERVER


#if defined(TCPIP_STACK_USE_NBNS)
    {TCPIP_MODULE_NBNS,          0},                           // TCPIP_MODULE_NBNS
#endif
#if defined(TCPIP_STACK_USE_SNTP_CLIENT)
    {TCPIP_MODULE_SNTP,    &sntpConfigData},                            // TCPIP_MODULE_SNTP,
#endif
#if defined(TCPIP_STACK_USE_BERKELEY_API)
    {TCPIP_MODULE_BERKELEY,      0},                           // TCPIP_MODULE_BERKELEY,
#endif
#if defined(TCPIP_STACK_USE_HTTP_SERVER)
    {TCPIP_MODULE_HTTP_SERVER,   &httpConfigData},              // TCPIP_MODULE_HTTP_SERVER,
#endif
#if defined(TCPIP_STACK_USE_TELNET_SERVER)
    {TCPIP_MODULE_TELNET_SERVER,   0},                        // TCPIP_MODULE_TELNET_SERVER,
#endif
#if defined(TCPIP_STACK_USE_SSL_SERVER) || defined(TCPIP_STACK_USE_SSL_CLIENT)
    {TCPIP_MODULE_RSA,           0},                           // TCPIP_MODULE_RSA,
#endif
#if defined(TCPIP_STACK_USE_SSL_SERVER) || defined(TCPIP_STACK_USE_SSL_CLIENT)
    {TCPIP_MODULE_SSL,           0},                           // TCPIP_MODULE_SSL,
#endif
#if defined(TCPIP_STACK_USE_FTP_SERVER)
    {TCPIP_MODULE_FTP_SERVER,    0},                           // TCPIP_MODULE_FTP,
#endif
#if defined(TCPIP_STACK_USE_SNMP_SERVER)
    {TCPIP_MODULE_SNMP_SERVER,   0},                           // TCPIP_MODULE_SNMP_SERVER,
#endif
#if defined(TCPIP_STACK_USE_DYNAMICDNS_CLIENT)
    {TCPIP_MODULE_DYNDNS_CLIENT, 0},                           // TCPIP_MODULE_DYNDNS_CLIENT,
#endif
#if defined(TCPIP_STACK_USE_REBOOT_SERVER)
    {TCPIP_MODULE_REBOOT_SERVER, 0},                           // TCPIP_MODULE_REBOOT_SERVER,
#endif
#if defined(TCPIP_STACK_USE_ZEROCONF_LINK_LOCAL)
    {TCPIP_MODULE_ZCLL, 0},                                    // TCPIP_MODULE_ZCLL,
    {TCPIP_MODULE_MDNS, 0},                                    // TCPIP_MODULE_MDNS,
#endif
#if defined(TCPIP_STACK_COMMAND_ENABLE)
    {TCPIP_MODULE_TCPIP_COMMAND, 0},                           // TCPIP_MODULE_TCPIP_COMMAND
#endif


    // MAC modules
#if defined(TCPIP_IF_PIC32INT)
    {TCPIP_MODULE_MAC_PIC32INT, &macPIC32INTConfigData},     // TCPIP_MODULE_MAC_PIC32INT
#endif
#if defined(TCPIP_IF_MRF24W)
    {TCPIP_MODULE_MAC_MRF24W, &macMRF24WConfigData},         // TCPIP_MODULE_MAC_MRF24W
#endif
};
/*********************************************************************
 * Function:        SYS_MODULE_OBJ TCPIP_STACK_Init()
 *
 * PreCondition:    None
 *
 * Input:
 *
 * Output:          valid system module object if Stack and its componets are initialized
 *                  SYS_MODULE_OBJ_INVALID otherwise
 *
 * Overview:        The function starts the initialization of the stack.
 *                  If an error occurs, the SYS_ERROR() is called
 *                  and the function de-initialize itself and will return false.
 *
 * Side Effects:    None
 *
 * Note:            This function must be called before any of the
 *                  stack or its component routines are used.
 *
 ********************************************************************/


SYS_MODULE_OBJ TCPIP_STACK_Init()
{
    TCPIP_STACK_INIT    tcpipInit;

    tcpipInit.moduleInit.sys.powerState = SYS_MODULE_POWER_RUN_FULL;
    tcpipInit.pNetConf = TCPIP_HOSTS_CONFIGURATION;
    tcpipInit.nNets = sizeof (TCPIP_HOSTS_CONFIGURATION) / sizeof (*TCPIP_HOSTS_CONFIGURATION);
    tcpipInit.pModConfig = TCPIP_STACK_MODULE_CONFIG_TBL;
    tcpipInit.nModules = sizeof (TCPIP_STACK_MODULE_CONFIG_TBL) / sizeof (*TCPIP_STACK_MODULE_CONFIG_TBL);

    return TCPIP_STACK_Initialize(0, &tcpipInit.moduleInit);
}


