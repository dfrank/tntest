/*******************************************************************************
 System Initialization File

  File Name:
    sys_init.c

  Summary:
    System Initialization.

  Description:
    This file contains source code necessary to initialize the system.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2011-2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END
#include "app.h"
#include "system_config.h"
#include "tcpip/tcpip.h"

#include "tcpip_stack_init.h"
#include "driver/usart/drv_usart.h"
#include "system/clk/sys_clk.h"
#include "system/random/sys_random.h"
#include "system/devcon/sys_devcon.h"
#include "system/console/sys_console.h"
#include "system/tmr/sys_tmr.h"

extern void    BSP_Initialize(void);

// Device Configuration
// DEVCFG3
// USERID = No Setting
#pragma config FMIIEN = OFF             // Ethernet RMII/MII Enable (RMII Enabled)
#pragma config FETHIO = ON              // Ethernet I/O Pin Select (Default Ethernet I/O)
#pragma config PGL1WAY = ON             // Permission Group Lock One Way Configuration (Allow only one reconfiguration)
#pragma config PMDL1WAY = ON            // Peripheral Module Disable Configuration (Allow only one reconfiguration)
#pragma config IOL1WAY = ON             // Peripheral Pin Select Configuration (Allow only one reconfiguration)
#pragma config FUSBIDIO = OFF           // USB USBID Selection (Controlled by Port Function)

// DEVCFG2
#pragma config FPLLIDIV = DIV_3         // System PLL Input Divider (1x Divider)
#pragma config FPLLRNG = RANGE_8_16_MHZ // System PLL Input Range (5-10 MHz Input)
#pragma config FPLLICLK = PLL_POSC      // System PLL Input Clock Selection (POSC is input to the System PLL)
#pragma config FPLLMULT = MUL_50        // System PLL Multiplier (PLL Multiply by 50)
#pragma config FPLLODIV = DIV_2
#pragma config UPLLFSEL = FREQ_24MHZ    // USB PLL Input Frequency Selection (USB PLL input is 12 MHz)
#pragma config UPLLEN   = OFF           // USB PLL Enable (USB PLL is disabled)

// DEVCFG1
#pragma config FNOSC = SPLL             // Oscillator Selection Bits (SPLL)
#pragma config DMTINTV = WIN_127_128    // DMT Count Window Interval (Window/Interval value is 127/128 counter value)
#pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Disable SOSC)
#pragma config IESO = OFF               // Internal/External Switch Over (Disabled)
#pragma config POSCMOD = EC             // Primary Oscillator Configuration (Primary osc disabled)
#pragma config OSCIOFNC = ON            // CLKO Output Signal enabled, otherwise HS mode doesn't work.
#pragma config FCKSM = CSDCMD           // Clock Switching and Monitor Selection (Clock Switch Disabled, FSCM Disabled)
#pragma config WDTPS = PS1048576        // Watchdog Timer Postscaler (1:1048576)
#pragma config WDTSPGM = STOP           // Watchdog Timer Stop During Flash Programming (WDT stops during Flash programming)
#pragma config WINDIS = NORMAL          // Watchdog Timer Window Mode (Watchdog Timer is in non-Window mode)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable (WDT Disabled)
#pragma config FWDTWINSZ = WINSZ_25     // Watchdog Timer Window Size (Window size is 25%)
// DMTCNT = No Setting
#pragma config FDMTEN = OFF             // Deadman Timer Enable (Deadman Timer is disabled)

// DEVCFG0
#pragma config DEBUG = ON              // Background Debugger Enable (Debugger is disabled)
//#pragma config DEBUG = OFF
#pragma config ICESEL = ICS_PGx2        // ICE/ICD Comm Channel Select
#pragma config TRCEN = ON               // Trace Enable (Trace features in the CPU are enabled)
#pragma config BOOTISA = MIPS32         // Boot ISA Selection (Boot code and Exception code is MIPS32)
#pragma config FECCCON = OFF_UNLOCKED   // Dynamic Flash ECC Configuration (ECC and Dynamic ECC are disabled (ECCCON bits are writable))
#pragma config FSLEEP = OFF             // Flash Sleep Mode (Flash is powered down when the device is in Sleep mode)
//#pragma config DBGPER = ALLOW_PG2       // Debug Mode CPU Access Permission (Allow CPU access to Permission Group 2 permission regions)
#pragma config DBGPER = PG_ALL
#pragma config EJTAGBEN = NORMAL        // EJTAG Boot (Normal EJTAG functionality)
//#pragma config JTAGEN = OFF

// DEVCP0
#pragma config CP = OFF                 // Code Protect (Protection Disabled)
#pragma config_alt FWDTEN=OFF
#pragma config_alt USERID = 0x1234u

const DRV_TMR_INIT tmrInitData =
{
    .moduleInit = {SYS_MODULE_POWER_RUN_FULL},
    .tmrId = TMR_ID_2,
    .clockSource = DRV_TMR_CLKSOURCE_INTERNAL,
    .prescale = TMR_PRESCALE_VALUE_256,
    .interruptSource = INT_SOURCE_TIMER_2,
    .mode = DRV_TMR_OPERATION_MODE_16_BIT,
    .asyncWriteEnable = false,

};

SYS_TMR_INIT sysTmrInitData =
{
    .moduleInit = {SYS_TMR_MODULE_INIT},
    .drvIndex = SYS_TMR_DRIVER_INDEX,
    .tmrFreq = SYS_TMR_FREQUENCY,
};

SYS_MODULE_OBJ    sysTmrObject, sysConsoleObject, usartDevObject, sysDebugObject, sysDEVCONObject = SYS_MODULE_OBJ_INVALID;
SYS_MODULE_OBJ    sysConsoleObjects[] = { SYS_MODULE_OBJ_INVALID };

/* Declared in console device implementation (sys_console_uart_cdc.c) */
extern SYS_CONSOLE_DEV_DESC consUsartDevDesc;

/*************************************************
 * USART Driver Initialization
 ************************************************/

DRV_USART_INIT usartInit =
{
    /* Initial baud rate*/
    .baud = SYS_DEBUG_BAUDRATE,

    /* System module initialization */
    .moduleInit = {0},

    /* UART operates in normal mode */
    .mode = DRV_USART_OPERATION_MODE_NORMAL,

    /* No wake on start bit or stop in idle */
    .flags = DRV_USART_INIT_FLAG_NONE,

    /* UART peripheral clock */
    .brgClock = SYS_DEVCON_SYSTEM_CLOCK / 2,

    /* No handshaking */
    .handshake = DRV_USART_HANDSHAKE_NONE,

    /* Error Interrupt */
    .interruptError = INT_SOURCE_USART_2_ERROR,

    /* Receive Interrupt */
    .interruptReceive = INT_SOURCE_USART_2_RECEIVE,

    /* Transmit Interrupt */
    .interruptTransmit = INT_SOURCE_USART_2_TRANSMIT,

    /* Line control mode */
    .lineControl = DRV_USART_LINE_CONTROL_8NONE1,

    /* Operation mode initialization data */
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,

    /* Not using queuing here */
    .queueSizeReceive = DRV_USART_READ_QUEUE_DEPTH,

    /* Not using queueing here */
    .queueSizeTransmit = DRV_USART_WRITE_QUEUE_DEPTH,

    /* This is the UART to be used */
    .usartID = USART_ID_2
};

SYS_STATUS      sysStatus;

SYS_DEVCON_INIT devConInit =
{
    .moduleInit = {0},
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL
};

SYS_CONSOLE_INIT consInit =
{
    .moduleInit = {0},
    .consDevDesc = &consUsartDevDesc,
};

SYS_DEBUG_INIT debugInit =
{
    .moduleInit = {0},
    .errorLevel = SYS_ERROR_DEBUG
};

// *****************************************************************************
// *****************************************************************************
// Section: Driver Initialization Data
// *****************************************************************************
// *****************************************************************************
static void APP_ETHMAC_PinPinitialize(void)
{
    //MDC
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_OUTPUT,
                                 PORT_CHANNEL_D,
                                 PORTS_BIT_POS_11);
    //MDIO
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_J,
                                 PORTS_BIT_POS_1);
    //TXEN
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_OUTPUT,
                                 PORT_CHANNEL_D,
                                 PORTS_BIT_POS_6);
    //TXD0
    ANSELJbits.ANSJ8 = 0;
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_OUTPUT,
                                 PORT_CHANNEL_J,
                                 PORTS_BIT_POS_8);
    //TXD1
    ANSELJbits.ANSJ9 = 0;
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_OUTPUT,
                                 PORT_CHANNEL_J,
                                 PORTS_BIT_POS_9);
    //RXCLK
    ANSELJbits.ANSJ11 = 0;
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_J,
                                 PORTS_BIT_POS_11);
    //RXDV
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_H,
                                 PORTS_BIT_POS_13);
    //RXD0
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_H,
                                 PORTS_BIT_POS_8);
    //RXD1
    ANSELHbits.ANSH5 = 0;
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_H,
                                 PORTS_BIT_POS_5);

    //RXERR
    ANSELHbits.ANSH4 = 0;
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_H,
                                 PORTS_BIT_POS_4);
}

// ****************************************************************************
// ****************************************************************************
// Section: System Initialization
// ****************************************************************************
// ****************************************************************************
/*******************************************************************************
  Function:
    bool SYS_Initialize ( void *data )

  Summary:
    Initializes the board, services, drivers, application and other modules

  Description:
    This routine initializes the board, services, drivers, application and other
    modules as configured at build time.  In a bare-metal environment (where no
    OS is supported), this routine should be called almost immediately after
    entering the "main" routine.

  Precondition:
    The C-language run-time environment and stack must have been initialized.

  Parameters:
    data        - Pointer to the system initialzation data structure containing
                  pointers to the board, system service, and driver
                  initialization routines
  Returns:
    None.

  Example:
    <code>
    SYS_INT_Initialize(NULL);
    </code>

  Remarks:
    Basic System Initialization Sequence:

    1.  Initilize minimal board services and processor-specific items
        (enough to use the board to initialize drivers and services)
    2.  Initialize all supported system services
    3.  Initialize all supported modules
        (libraries, drivers, middleware, and application-level modules)
    4.  Initialize the main (static) application, if present.

    The order in which services and modules are initialized and started may be
    important.

    For a static system (a system not using the ISP's dynamic implementation
    of the initialization and "Tasks" services) this routine is implemented
    for the specific configuration of an application.
 */
void SYS_Initialize(void* data)
{
    PLIB_PORTS_PinModePerPortSelect(PORTS_ID_0,PORT_CHANNEL_B,PORTS_BIT_POS_15,PORTS_PIN_MODE_DIGITAL);

    /* PPS setup */
    SYS_PORTS_RemapOutput(PORTS_ID_0,OTPUT_FUNC_U2TX,OUTPUT_PIN_RPB14);
    SYS_PORTS_RemapInput(PORTS_ID_0,INPUT_FUNC_U2RX,INPUT_PIN_RPB15);

    PLIB_USART_ReceiverInterruptModeSelect(usartInit.usartID, USART_RECEIVE_FIFO_ONE_CHAR);

    sysDEVCONObject = SYS_DEVCON_Initialize (SYS_DEVCON_INDEX_0, (SYS_MODULE_INIT*)&devConInit);
    if (SYS_MODULE_OBJ_INVALID == sysDEVCONObject)
    {
        return;
    }

    SYS_DEVCON_PerformanceConfig( SYS_DEVCON_SYSTEM_CLOCK );

    BSP_Initialize();

    sysConsoleObjects[0] = SYS_CONSOLE_Initialize(SYS_CONSOLE_INDEX_0, (SYS_MODULE_INIT*)&consInit);

    sysDebugObject = SYS_DEBUG_Initialize(SYS_DEBUG_INDEX_0, (SYS_MODULE_INIT*)&debugInit);

    APP_ETHMAC_PinPinitialize();

    /* Initializethe interrupt system  */
    SYS_INT_Initialize();

    SYS_CLK_Initialize(NULL);

    appDrvObjects.drvTmrObject = DRV_TMR_Initialize(DRV_TMR_INDEX_0, (const SYS_MODULE_INIT  * const)&tmrInitData);
    sysTmrObject = SYS_TMR_Initialize(SYS_TMR_INDEX_0, (const SYS_MODULE_INIT  * const)&sysTmrInitData);

    /* Set priority for USART interrupt transmit source */
    SYS_INT_VectorPrioritySet(INT_VECTOR_UART2_TX, INT_PRIORITY_LEVEL3);

    /* set sub-priority for USART interrupt transmit source */
    SYS_INT_VectorSubprioritySet(INT_VECTOR_UART2_TX, INT_SUBPRIORITY_LEVEL3);

    /* Set priority for USART interrupt recieve source */
    SYS_INT_VectorPrioritySet(INT_VECTOR_UART2_RX, INT_PRIORITY_LEVEL3);

    /* set sub-priority for USART interrupt recieve source */
    SYS_INT_VectorSubprioritySet(INT_VECTOR_UART2_RX, INT_SUBPRIORITY_LEVEL3);

     /* set priority for TMR2 interrupt source */
    SYS_INT_VectorPrioritySet(INT_VECTOR_T2, INT_PRIORITY_LEVEL4);

    /* set sub-priority for TMR2 interrupt source */
    SYS_INT_VectorSubprioritySet(INT_VECTOR_T2, INT_SUBPRIORITY_LEVEL3);

    /* set priority for ETHERNET interrupt source */
    SYS_INT_VectorPrioritySet(DRV_ETHMAC_INTERRUPT_VECTOR, INT_PRIORITY_LEVEL5);

    /* set sub-priority for ETHERNET interrupt source */
    SYS_INT_VectorSubprioritySet(DRV_ETHMAC_INTERRUPT_VECTOR, INT_SUBPRIORITY_LEVEL3);

    /* Enable multi-vectored interrupts, enable the generation of interrupts to the CPU */
    PLIB_INT_MultiVectorSelect(INT_ID_0);

    SYS_INT_Enable();


    SYS_RANDOM_Initialize(0, 0);

    if (!SYS_COMMAND_INIT())
    {
        return;
    }

    // TCP/IP stack initialization
    SYS_MESSAGE("\r\nTCPStack " TCPIP_STACK_VERSION_STR "  ""                ");

    // Initialize the TCPIP stack
    appDrvObjects.tcpipObject = TCPIP_STACK_Init();
    if (appDrvObjects.tcpipObject == SYS_MODULE_OBJ_INVALID)
    {
       return;
    }

    APP_Initialize();

    /* Initialize the USART driver */
    usartDevObject = DRV_USART_Initialize(DRV_USART_INDEX_0,
            (SYS_MODULE_INIT *) &usartInit);

      /* Check if the object returned by the device layer is valid */
    if (SYS_MODULE_OBJ_INVALID == usartDevObject)
    {
        SYS_ERROR(SYS_ERROR_FATAL, "Invalid USART Driver object", 0);
        return;
    }

    /* Check the usart status */
    sysStatus = DRV_USART_Status(usartDevObject);

    return;

} //SYS_Initialize

/*******************************************************************************/
/*******************************************************************************
 End of File
*/
