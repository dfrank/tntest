#ifndef _ASSERT_TO_IDE_H_
#define _ASSERT_TO_IDE_H_

#include "system/debug/sys_debug.h"

void AssertToIDE(char *test, char *message, uint16_t line, char *file);

#endif //_ASSERT_TO_IDE_H_
