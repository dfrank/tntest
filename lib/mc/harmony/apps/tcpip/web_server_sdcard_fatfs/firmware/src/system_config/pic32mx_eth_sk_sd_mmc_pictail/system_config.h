/*******************************************************************************
  MPLAB Harmony System Configuration Header

  File Name:
    system_config.h

  Summary:
    Build-time configuration header for the system defined by this MPLAB Harmony
    project.

  Description:
    An MPLAB Project may have multiple configurations.  This file defines the
    build-time options for a single configuration.

  Remarks:
    This configuration header must not define any prototypes or data
    definitions (or include any files that do).  It only provides macro
    definitions for build-time configuration options that are not instantiated
    until used by another MPLAB Harmony module or application.
    
    Created with MPLAB Harmony Version 1.00
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

#ifndef _SYSTEM_CONFIG_H
#define _SYSTEM_CONFIG_H


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
/*  This section Includes other configuration headers necessary to completely
    define this configuration.
*/

#include "bsp_config.h"

// *****************************************************************************
// *****************************************************************************
// Section: System Service Configuration
// *****************************************************************************
// *****************************************************************************


// *****************************************************************************
/* Clock System Service Configuration Options
*/

#define SYS_CLK_SOURCE                      SYS_CLK_SOURCE_PRIMARY_SYSPLL
#define SYS_CLK_FREQ                        80000000ul
#define SYS_CLK_CONFIG_PRIMARY_XTAL         8000000ul
#define SYS_CLK_CONFIG_SECONDARY_XTAL       32768ul
#define SYS_CLK_CONFIG_SYSPLL_INP_DIVISOR   2
#define SYS_CLK_CONFIG_FREQ_ERROR_LIMIT     10
#define SYS_CLK_CONFIGBIT_USBPLL_ENABLE     false
#define SYS_CLK_CONFIGBIT_USBPLL_DIVISOR    1
#define SYS_CLK_WAIT_FOR_SWITCH             true
#define SYS_CLK_KEEP_SECONDARY_OSC_ENABLED  true
#define SYS_CLK_ON_WAIT                     OSC_ON_WAIT_IDLE

 
/*** Common System Service Configuration ***/

#define SYS_BUFFER  false
#define SYS_QUEUE   false


// *****************************************************************************
/* Device Control System Service Configuration Options
*/

#define SYS_DEVCON_SYSTEM_CLOCK         80000000
#define SYS_DEVCON_PIC32MX_MAX_PB_FREQ  80000000



/*** Interrupt System Service Configuration ***/

#define SYS_INT                     true


/*** Ports System Service Configuration ***/



/*** Timer System Service Configuration ***/

#define SYS_TMR_POWER_STATE             SYS_MODULE_POWER_RUN_FULL
#define SYS_TMR_DRIVER_INDEX            DRV_TMR_INDEX_0
#define SYS_TMR_MAX_CLIENT_OBJECTS      5
#define SYS_TMR_FREQUENCY               1000
#define SYS_TMR_FREQUENCY_TOLERANCE     10
#define SYS_TMR_UNIT_RESOLUTION         10000
#define SYS_TMR_CLIENT_TOLERANCE        10
#define SYS_TMR_INTERRUPT_NOTIFICATION  true


/*** File System Service Configuration ***/

#define SYS_FS_MEDIA_NUMBER         	1
#define SYS_FS_VOLUME_NUMBER        	5
#define SYS_FS_MAX_FILES	    	25
#define SYS_FS_MAX_FILE_SYSTEM_TYPE 	1
#define SYS_FS_MEDIA_MAX_BLOCK_SIZE  	512

// *****************************************************************************
/* Random System Service Configuration Options
*/

#define SYS_RANDOM_CRYPTO_SEED_SIZE  55


// *****************************************************************************
// *****************************************************************************
// Section: Driver Configuration
// *****************************************************************************
// *****************************************************************************


/*** Timer Driver Configuration ***/

#define DRV_TMR_INSTANCES_NUMBER           1
#define DRV_TMR_CLIENTS_NUMBER             1
#define DRV_TMR_INTERRUPT_MODE             true

#define DRV_TMR_PERIPHERAL_ID_IDX0          TMR_ID_2
#define DRV_TMR_INTERRUPT_SOURCE_IDX0       INT_SOURCE_TIMER_2
#define DRV_TMR_INTERRUPT_VECTOR_IDX0       INT_VECTOR_T2
#define DRV_TMR_ISR_VECTOR_IDX0             _TIMER_2_VECTOR
#define DRV_TMR_INTERRUPT_PRIORITY_IDX0     INT_PRIORITY_LEVEL4
#define DRV_TMR_INTERRUPT_SUB_PRIORITY_IDX0 INT_SUBPRIORITY_LEVEL0
#define DRV_TMR_CLOCK_SOURCE_IDX0           DRV_TMR_CLKSOURCE_INTERNAL
#define DRV_TMR_PRESCALE_IDX0               TMR_PRESCALE_VALUE_256
#define DRV_TMR_OPERATION_MODE_IDX0         DRV_TMR_OPERATION_MODE_16_BIT
#define DRV_TMR_ASYNC_WRITE_ENABLE_IDX0     false
#define DRV_TMR_POWER_STATE_IDX0            SYS_MODULE_POWER_RUN_FULL








 
 
/*** SDCARD Driver Configuration ***/
#define DRV_SDCARD_INSTANCES_NUMBER     	1
#define DRV_SDCARD_CLIENTS_NUMBER        	1
#define DRV_SDCARD_INDEX_MAX			1
#define DRV_SDCARD_INDEX			DRV_SDCARD_INDEX_0
#define DRV_SDCARD_QUEUE_POOL_SIZE		10

/* Defines the card detect logic as designed in hardware */
#define DRV_SDCARD_CD_LOGIC_ACTIVE_LOW


/*** SPI Driver Configuration ***/
/*** Driver Compilation and static configuration options. ***/
/*** Select SPI compilation units.***/
#define DRV_SPI_ISR 				1
#define DRV_SPI_POLLED 				0
#define DRV_SPI_MASTER 				1
#define DRV_SPI_SLAVE 				0
#define DRV_SPI_RM 				0
#define DRV_SPI_EBM 				1
#define DRV_SPI_8BIT 				1
#define DRV_SPI_16BIT				0
#define DRV_SPI_32BIT 				0
#define DRV_SPI_DMA 				0

/*** SPI Driver Static Allocation Options ***/
#define DRV_SPI_INSTANCES_NUMBER 		1
#define DRV_SPI_CLIENTS_NUMBER 			1
#define DRV_SPI_ELEMENTS_PER_QUEUE 		10

/*** SPI Driver Index 0 configuration ***/
#define DRV_SPI_SPI_ID_IDX0 			SPI_ID_1
#define DRV_SPI_TASK_MODE_IDX0 			DRV_SPI_TASK_MODE_ISR
#define DRV_SPI_SPI_MODE_IDX0 			DRV_SPI_MODE_MASTER
#define DRV_SPI_ALLOW_IDLE_RUN_IDX0 		false
#define DRV_SPI_SPI_PROTOCOL_TYPE_IDX0 		DRV_SPI_PROTOCOL_TYPE_STANDARD
#define DRV_SPI_COMM_WIDTH_IDX0 		SPI_COMMUNICATION_WIDTH_8BITS
#define DRV_SPI_SPI_CLOCK_IDX0 			CLK_BUS_PERIPHERAL_1
#define DRV_SPI_BAUD_RATE_IDX0 			10000000
#define DRV_SPI_BUFFER_TYPE_IDX0 		DRV_SPI_BUFFER_TYPE_ENHANCED
#define DRV_SPI_CLOCK_MODE_IDX0 		DRV_SPI_CLOCK_MODE_IDLE_LOW_EDGE_FALL
#define DRV_SPI_INPUT_PHASE_IDX0 		SPI_INPUT_SAMPLING_PHASE_AT_END
#define DRV_SPI_TX_INT_SOURCE_IDX0 		INT_SOURCE_SPI_1_TRANSMIT
#define DRV_SPI_RX_INT_SOURCE_IDX0 		INT_SOURCE_SPI_1_RECEIVE
#define DRV_SPI_ERROR_INT_SOURCE_IDX0 		INT_SOURCE_SPI_1_ERROR
#define DRV_SPI_INT_VECTOR_IDX0                	INT_VECTOR_SPI1
#define DRV_SPI_INT_PRIORITY_IDX0               INT_PRIORITY_LEVEL3
#define DRV_SPI_INT_SUB_PRIORITY_IDX0           INT_SUBPRIORITY_LEVEL0
#define DRV_SPI_QUEUE_SIZE_IDX0 		10
#define DRV_SPI_RESERVED_JOB_IDX0 		1


// *****************************************************************************
// *****************************************************************************
// Section: Middleware & Other Library Configuration
// *****************************************************************************
// *****************************************************************************


#define HAVE_MCAPI
#define NO_MD5
#define NO_SHA
#define NO_SHA256
#define NO_AES
#define NO_RSA
#define NO_HMAC


// *****************************************************************************
// *****************************************************************************
// Section: TCPIP Stack Configuration
// *****************************************************************************
// *****************************************************************************
#define TCPIP_STACK_USE_IPV4
#define TCPIP_STACK_USE_TCP
#define TCPIP_STACK_USE_UDP
#define TCPIP_STACK_USE_ICMP_SERVER
#define TCPIP_STACK_USE_ICMP_CLIENT
#define TCPIP_STACK_DRAM_SIZE		        		39250
#define TCPIP_STACK_DRAM_RUN_LIMIT		    		2048
#define TCPIP_STACK_DRAM_DEBUG_ENABLE
#define TCPIP_STACK_DRAM_TRACE_ENABLE
#define TCPIP_STACK_TICK_RATE		        		5
#define TCPIP_STACK_DRAM_TRACE_SLOTS				12

#define TCPIP_STACK_MALLOC_FUNC		    	malloc

#define TCPIP_STACK_CALLOC_FUNC		    	calloc

#define TCPIP_STACK_FREE_FUNC		    	free

/* Console and Debug Symbols */
#ifndef SYSTEM_CURRENT_ERROR_LEVEL
    #define SYSTEM_CURRENT_ERROR_LEVEL  SYS_ERROR_WARNING
#endif

typedef enum
{
    SYS_MODULE_UART_1,
    SYS_MODULE_UART_2,
} SYS_MODULE_ID;

#define SYS_CONSOLE_ENABLE					true
#define SYS_DEBUG_ENABLE					true
#define SYS_DEBUG_PORT            				SYS_MODULE_UART_2
#define SYS_CONSOLE_PORT          				SYS_MODULE_UART_2
#define SYS_CONSOLE_BAUDRATE                            	115200
#define SYS_CONSOLE_BUFFER_LEN                          	64
#define SYS_DEBUG_BAUDRATE                              	115200


/*** ARP Configuration ***/
#define TCPIP_ARP_CACHE_ENTRIES                 		5
#define TCPIP_ARP_CACHE_DELETE_OLD		        	true
#define TCPIP_ARP_CACHE_SOLVED_ENTRY_TMO			1200
#define TCPIP_ARP_CACHE_PENDING_ENTRY_TMO			60
#define TCPIP_ARP_CACHE_PENDING_RETRY_TMO			2
#define TCPIP_ARP_CACHE_PERMANENT_QUOTA		    		50
#define TCPIP_ARP_CACHE_PURGE_THRESHOLD		    		75
#define TCPIP_ARP_CACHE_PURGE_QUANTA		    		1
#define TCPIP_ARP_CACHE_ENTRY_RETRIES		    		3
#define TCPIP_ARP_GRATUITOUS_PROBE_COUNT			1
#define TCPIP_ARP_TASK_PROCESS_RATE		        	2


/*** DHCP Configuration ***/
#define TCPIP_DHCP_TIMEOUT		        		2
#define TCPIP_DHCP_TASK_TICK_RATE	    			200
#define TCPIP_DHCP_CLIENT_CONNECT_PORT  			68
#define TCPIP_DHCP_SERVER_LISTEN_PORT				67
#define TCPIP_DHCP_CLIENT_ENABLED             			true
#define TCPIP_STACK_USE_DHCP_CLIENT


/*** DHCP Server Configuration ***/


/*** DNS Client Configuration ***/
#define TCPIP_DNS_CLIENT_SERVER_TMO		    		60
#define TCPIP_DNS_CLIENT_TASK_PROCESS_RATE		    	200
#define TCPIP_DNS_CLIENT_CACHE_ENTRIES				5
#define TCPIP_DNS_CLIENT_CACHE_ENTRY_TMO			0
#define TCPIP_DNS_CLIENT_CACHE_PER_IPV4_ADDRESS			5
#define TCPIP_DNS_CLIENT_CACHE_PER_IPV6_ADDRESS	    		
#define TCPIP_DNS_CLIENT_OPEN_ADDRESS_TYPE		    	IP_ADDRESS_TYPE_IPV4
#define TCPIP_DNS_CLIENT_CACHE_DEFAULT_TTL_VAL			1200
#define TCPIP_DNS_CLIENT_CACHE_UNSOLVED_ENTRY_TMO		10
#define TCPIP_DNS_CLIENT_MAX_HOSTNAME_LEN			32
#define TCPIP_DNS_CLIENT_DELETE_OLD_ENTRIES			true
#define TCPIP_STACK_USE_DNS


/*** DNS Server Configuration ***/


/*** FTP Configuration ***/
#define TCPIP_FTP_USER_NAME_LEN		    			10
#define TCPIP_FTP_PASSWD_LEN		    			10
#define TCPIP_FTP_MAX_CONNECTIONS				1
#define TCPIP_FTP_DATA_SKT_TX_BUFF_SIZE				0
#define TCPIP_FTP_DATA_SKT_RX_BUFF_SIZE				0
#define TCPIP_FTPS_TASK_TICK_RATE	    			333
#define TCPIP_FTP_USER_NAME		        		"Microchip"
#define TCPIP_FTP_PASSWORD		        		"Harmony"
#define TCPIP_STACK_USE_FTP_SERVER
/***Comment this line out to disable MPFS***/
#define TCPIP_TCPIP_FTP_PUT_ENABLED				true


/*** HTTP Configuration ***/
#define TCPIP_HTTP_MAX_HEADER_LEN		    		15
#define TCPIP_HTTP_CACHE_LEN		        		"600"
#define TCPIP_HTTP_TIMEOUT		            		45
#define TCPIP_HTTP_MAX_CONNECTIONS		    		4
#define TCPIP_HTTP_DEFAULT_FILE		        		"index.htm"
#define TCPIP_HTTPS_DEFAULT_FILE	        		"index.htm"
#define TCPIP_HTTP_DEFAULT_LEN		        		10
#define TCPIP_HTTP_MAX_DATA_LEN		        		100
#define TCPIP_HTTP_MIN_CALLBACK_FREE				16
#define TCPIP_HTTP_SKT_TX_BUFF_SIZE		    		0
#define TCPIP_HTTP_SKT_RX_BUFF_SIZE		    		0
#define TCPIP_HTTP_CFG_FLAGS		        		TCPIP_HTTP_MODULE_FLAG_ADJUST_SKT_FIFOS
#define TCPIP_HTTP_CONFIG_FLAGS		        		1
#define TCPIP_HTTP_USE_POST             			true
#define TCPIP_HTTP_USE_COOKIES             			true
#define TCPIP_STACK_USE_BASE64_DECODE             		true
#define TCPIP_HTTP_USE_AUTHENTICATION             		true
#define TCPIP_HTTP_NO_AUTH_WITHOUT_SSL             		false
#define TCPIP_HTTP_TASK_RATE				33
#define TCPIP_STACK_USE_HTTP_SERVER

/*** HTTP FS Wrapper ***/
#define SYS_FS_MAX_PATH						80
#define LOCAL_WEBSITE_PATH_FS					"/mnt/mchpSite1"
#define LOCAL_WEBSITE_PATH					"/mnt/mchpSite1/"
#define SYS_FS_DRIVE						"SDCARD"
#define SYS_FS_SD_VOL						"/dev/mmcblka1"
#define SYS_FS_FATFS_STRING					"FATFS"
#define SYS_FS_MPFS_STRING					"MPFS2"



/*** ICMP Configuration ***/




/*** iperf Configuration ***/
#define TCPIP_IPERF_TX_BUFFER_SIZE				4096
#define TCPIP_IPERF_RX_BUFFER_SIZE  				4096
#define TCPIP_IPERF_TX_WAIT_TMO     				100
#define TCPIP_IPERF_TX_QUEUE_LIMIT  				2
#define TCPIP_STACK_USE_IPERF



/*** NBNS Configuration ***/
#define TCPIP_NBNS_TASK_TICK_RATE    				110
#define TCPIP_STACK_USE_NBNS


/*** SMTP Configuration ***/
#define TCPIP_SMTP_SERVER_REPLY_TIMEOUT 			8
#define TCPIP_SMTP_TASK_TICK_RATE				55
#define TCPIP_STACK_USE_SMTP_CLIENT


/*** SNTP Configuration ***/
#define TCPIP_NTP_DEFAULT_IF		        		"PIC32INT"
#define TCPIP_NTP_VERSION             			    	4
#define TCPIP_NTP_DEFAULT_CONNECTION_TYPE   			IP_ADDRESS_TYPE_IPV4
#define TCPIP_NTP_EPOCH		                		2208988800ul
#define TCPIP_NTP_REPLY_TIMEOUT		        		6
#define TCPIP_NTP_MAX_STRATUM		        		15
#define TCPIP_NTP_TIME_STAMP_TMO				660
#define TCPIP_NTP_SERVER		        		"pool.ntp.org"
#define TCPIP_NTP_SERVER_MAX_LENGTH				30
#define TCPIP_NTP_QUERY_INTERVAL				600
#define TCPIP_NTP_FAST_QUERY_INTERVAL	    			14
#define TCPIP_NTP_TASK_TICK_RATE				1100
#define TCPIP_NTP_RX_QUEUE_LIMIT				2
#define TCPIP_STACK_USE_SNTP_CLIENT





#define TCPIP_STACK_USE_SSL_SERVER
#define TCPIP_STACK_USE_SSL_CLIENT
#define TCPIP_SSL_MIN_SESSION_LIFETIME 				1
#define TCPIP_SSL_RSA_LIFETIME_EXTENSION 				8
#define TCPIP_SSL_MAX_CONNECTIONS 					2
#define TCPIP_SSL_MAX_SESSIONS 					2
#define TCPIP_SSL_MULTIPLE_INTERFACES 				true
#define TCPIP_SSL_MAX_BUFFERS 					4
#define TCPIP_SSL_MAX_HASHES 						5
#define TCPIP_SSL_RSA_SERVER_KEY_SIZE 					1024
#define TCPIP_SSL_RSA_CLIENT_KEY_SIZE 					1024
#define TCPIP_STACK_USE_SSL_SERVER
#define TCPIP_STACK_USE_SSL_CLIENT
#define TCPIP_SSL_VERSION                       (0x0300u)
#define TCPIP_SSL_VERSION_HI                    (0x03u)
#define TCPIP_SSL_VERSION_LO                    (0x00u)


/*** TCP Configuration ***/
#define TCPIP_TCP_MAX_SEG_SIZE_TX		        	1460
#define TCPIP_TCP_MAX_SEG_SIZE_RX_LOCAL		    		1460
#define TCPIP_TCP_MAX_SEG_SIZE_RX_NON_LOCAL			536
#define TCPIP_TCP_SOCKET_DEFAULT_TX_SIZE			512
#define TCPIP_TCP_SOCKET_DEFAULT_RX_SIZE			512
#define TCPIP_TCP_START_TIMEOUT_VAL		        	1000
#define TCPIP_TCP_DELAYED_ACK_TIMEOUT		    		100
#define TCPIP_TCP_FIN_WAIT_2_TIMEOUT		    		5000
#define TCPIP_TCP_KEEP_ALIVE_TIMEOUT		    		10000
#define TCPIP_TCP_CLOSE_WAIT_TIMEOUT		    		200
#define TCPIP_TCP_MAX_RETRIES		            		5
#define TCPIP_TCP_MAX_UNACKED_KEEP_ALIVES			6
#define TCPIP_TCP_MAX_SYN_RETRIES		        	2
#define TCPIP_TCP_AUTO_TRANSMIT_TIMEOUT_VAL			40
#define TCPIP_TCP_WINDOW_UPDATE_TIMEOUT_VAL			200
#define TCPIP_TCP_MAX_SOCKETS		            		10
#define TCPIP_TCP_TASK_TICK_RATE		        	5


/*** announce Configuration ***/
#define TCPIP_ANNOUNCE_MAX_PAYLOAD  				512
#define TCPIP_ANNOUNCE_TASK_RATE    				333
#define TCPIP_STACK_USE_ANNOUNCE


/*** TCPIP MAC Configuration ***/
#define TCPIP_EMAC_TX_DESCRIPTORS				8
#define TCPIP_EMAC_RX_DESCRIPTORS				6
#define TCPIP_EMAC_RX_BUFF_SIZE		    			1536
#define TCPIP_EMAC_RX_MAX_FRAME		    			1536
#define TCPIP_EMAC_RX_FRAGMENTS		    			1
#define TCPIP_EMAC_ETH_OPEN_FLAGS       			0x11f
#define TCPIP_EMAC_PHY_CONFIG_FLAGS     			0x10
#define TCPIP_EMAC_PHY_LINK_INIT_DELAY  			500
#define TCPIP_EMAC_PHY_ADDRESS		    			1
#define TCPIP_EMAC_INTERRUPT_MODE        			true
#define TCPIP_STACK_USE_EVENT_NOTIFICATION
#define DRV_ETHPHY_INSTANCES_NUMBER				1
#define DRV_ETHPHY_CLIENTS_NUMBER				1
#define DRV_ETHPHY_INDEX		        		1
#define DRV_ETHPHY_PERIPHERAL_ID				1
#define DRV_ETHPHY_NEG_INIT_TMO		    			1
#define DRV_ETHPHY_NEG_DONE_TMO		    			2000
#define DRV_ETHPHY_RESET_CLR_TMO				500
#define DRV_ETHMAC_INSTANCES_NUMBER				1
#define DRV_ETHMAC_CLIENTS_NUMBER				1
#define DRV_ETHMAC_INDEX	    	    			1
#define DRV_ETHMAC_PERIPHERAL_ID				1
#define DRV_ETHMAC_INTERRUPT_VECTOR				INT_VECTOR_ETHERNET
#define DRV_ETHMAC_INTERRUPT_SOURCE				INT_SOURCE_ETH_1
#define DRV_ETHMAC_POWER_STATE		    			SYS_MODULE_POWER_RUN_FULL

#define DRV_ETHMAC_INTERRUPT_MODE        			true
#define TCPIP_STACK_USE_EVENT_NOTIFICATION


/*** TCP/IP Reboot Configuration ***/


/*** telnet Configuration ***/
#define TCPIP_TELNET_MAX_CONNECTIONS    			2
#define TCPIP_TELNET_USERNAME           			"admin"
#define TCPIP_TELNET_PASSWORD           			"microchip"
#define TCPIP_TELNET_TASK_TICK_RATE     			100

#define TCPIP_TELNET_REJECT_UNSECURED             		false
#define TCPIP_STACK_USE_TELNET_SERVER


/*** UDP Configuration ***/
#define TCPIP_UDP_MAX_SOCKETS		                	10
#define TCPIP_UDP_SOCKET_DEFAULT_TX_SIZE		    	512
#define TCPIP_UDP_SOCKET_DEFAULT_TX_QUEUE_LIMIT    	 	3
#define TCPIP_UDP_SOCKET_DEFAULT_RX_QUEUE_LIMIT			5
#define TCPIP_UDP_SOCKET_POOL_BUFFERS		        	4
#define TCPIP_UDP_SOCKET_POOL_BUFFER_SIZE		    	512

#define TCPIP_UDP_USE_TX_CHECKSUM             			true

#define TCPIP_UDP_USE_RX_CHECKSUM             			true

#define TCPIP_STACK_USE_ZEROCONF_LINK_LOCAL
#define TCPIP_STACK_USE_ZEROCONF_MDNS_SD

/*** Network Configuration Index 0 ***/
#define TCPIP_NETWORK_DEFAULT_INTERFACE_NAME 			"PIC32INT"
#define TCPIP_IF_PIC32INT
#define TCPIP_NETWORK_DEFAULT_HOST_NAME 			"MCHPBOARD_E"
#define TCPIP_NETWORK_DEFAULT_MAC_ADDR	 			0
#define TCPIP_NETWORK_DEFAULT_IP_ADDRESS 			"192.168.100.115"
#define TCPIP_NETWORK_DEFAULT_IP_MASK 				"255.255.255.0"
#define TCPIP_NETWORK_DEFAULT_GATEWAY	 			"192.168.100.1"
#define TCPIP_NETWORK_DEFAULT_DNS 				"192.168.100.1"
#define TCPIP_NETWORK_DEFAULT_SECOND_DNS 			"0.0.0.0"
#define TCPIP_NETWORK_DEFAULT_POWER_MODE 			"full"
#define TCPIP_NETWORK_DEFAULT_INTERFACE_FLAGS   		TCPIP_NETWORK_CONFIG_DHCP_CLIENT_ON
#define TCPIP_NETWORK_DEFAULT_IPV6_ADDRESS 			0
#define TCPIP_NETWORK_DEFAULT_IPV6_PREFIX_LENGTH 		0
#define TCPIP_NETWORK_DEFAULT_IPV6_GATEWAY 		        0

/*** tcpip_cmd Configuration ***/
#define TCPIP_STACK_COMMANDS_STORAGE_ENABLE             true
#define TCPIP_STACK_COMMANDS_ICMP_ECHO_REQUESTS         4
#define TCPIP_STACK_COMMANDS_ICMP_ECHO_REQUEST_DELAY    1000
#define TCPIP_STACK_COMMANDS_ICMP_ECHO_TIMEOUT          5000
#define TCPIP_STACK_COMMANDS_WIFI_ENABLE             	true
#define TCPIP_STACK_COMMAND_ENABLE


/* BSP LED Re-directs */
#define APP_TCPIP_LED_1 BSP_LED_1
#define APP_TCPIP_LED_2 BSP_LED_2
#define APP_TCPIP_LED_3 BSP_LED_3

#define APP_TCPIP_SWITCH_1 BSP_SWITCH_1
#define APP_TCPIP_SWITCH_2 BSP_SWITCH_2
#define APP_TCPIP_SWITCH_3 BSP_SWITCH_3
#include "system/console/sys_console.h"
#include "system/debug/sys_debug.h"
#include "tcpip/src/system/system_command.h"
#include "tcpip/src/system/system_mapping.h"






#endif // _SYSTEM_CONFIG_H
/*******************************************************************************
 End of File
*/

