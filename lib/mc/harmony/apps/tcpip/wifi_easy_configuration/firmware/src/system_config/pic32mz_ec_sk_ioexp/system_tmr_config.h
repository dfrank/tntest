/*******************************************************************************
  Timer System Service Configuration Definitions for the Template Version

  Company:
    Microchip Technology Inc.

  File Name:
    sys_tmr_config_template.h

  Summary:
    Contains configuration definitions that are common to timer system services
    and aggregates the configuration files for the system services.

  Description:
    This file contains configuration definitions that are common to timer drivers
    and aggregates the configuration files for the system services.

*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END


#ifndef _SYS_TMR_CONFIG_H
#define _SYS_TMR_CONFIG_H

// *****************************************************************************
// *****************************************************************************
// Section: Drv Tmr Configuration
// *****************************************************************************
// *****************************************************************************

// number of different timer modules the driver needs to serve
#define DRV_TMR_INSTANCES_NUMBER                    3

// number of clients per timer instance
// For this implementation the valid value is 1!
#define DRV_TMR_CLIENTS_NUMBER                      1

// default driver initialization state
#define DRV_TMR_MODULE_INIT                         (SYS_MODULE_POWER_RUN_FULL)

// default timer module to use
#define DRV_TMR_MODULE_ID                           (TMR_ID_2)

// default clock source
#define DRV_TMR_CLOCK_SOURCE                        (DRV_TMR_CLKSOURCE_INTERNAL)

// default prescaler value
#define DRV_TMR_CLOCK_PRESCALER                     (TMR_PRESCALE_VALUE_256)

// default operating mode
#define DRV_TMR_MODE                                (DRV_TMR_OPERATION_MODE_16_BIT)

// default interrupt source (needs to match the selected DRV_TMR_MODULE_ID)
#define DRV_TMR_INTERRUPT_SOURCE                    (INT_SOURCE_TIMER_2)

// enable/disable operation in interrupt mode
#define DRV_TMR_INTERRUPT_MODE                      true

// asynchronous write enable/disable for timers that support it
#define DRV_TMR_ASYNC_WRITE_ENABLE                  false 


/* SYS TMR Configuration */

#define SYS_TMR_MODULE_INIT                       (SYS_MODULE_POWER_RUN_FULL)

#define SYS_TMR_DRIVER_INDEX                      (DRV_TMR_INDEX_0)

#define SYS_TMR_MAX_CLIENT_OBJECTS                (5)

#define SYS_TMR_FREQUENCY                         (1250)

#define SYS_TMR_FREQUENCY_TOLERANCE               (10)

#define SYS_TMR_UNIT_RESOLUTION                   (10000)

#define SYS_TMR_CLIENT_TOLERANCE                  (10)

#define SYS_TMR_INTERRUPT_NOTIFICATION            (false)









#endif // _SYS_TMR_CONFIG_H

/*******************************************************************************
 End of File
*/

