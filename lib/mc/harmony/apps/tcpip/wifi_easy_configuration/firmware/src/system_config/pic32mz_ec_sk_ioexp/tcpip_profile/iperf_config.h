#ifndef _IPERF_CONFIG_H_
#define _IPERF_CONFIG_H_



#define TCPIP_IPERF_TX_BUFFER_SIZE            4096    // default size of the Iperf TX buffer
#define TCPIP_IPERF_RX_BUFFER_SIZE            4096    // default size of the Iperf RX buffer
                                                // bigger buffers will help obtain higher performance numbers
                                                // adjust as needed

#define TCPIP_IPERF_TX_WAIT_TMO               200     // timeout to wait for TX channel to be ready to transmit a new packet; ms
                                                // depends on the channel bandwidth

#define TCPIP_IPERF_TX_QUEUE_LIMIT              2     // for Iperf UDP Client, the limit to set to avoid memory allocation
                                                // overflow on slow connections
#endif /* __IPERFAPP_H__ */

