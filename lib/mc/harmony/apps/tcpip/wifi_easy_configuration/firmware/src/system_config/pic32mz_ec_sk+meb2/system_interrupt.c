/*******************************************************************************
 System Interrupt Source File

  File Name:
    system_interrupt.c

  Summary:
    Raw ISR definitions.

  Description:
    This file contains a definitions of the raw ISRs required to support the
    interrupt sub-system.
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2011-2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END

#include "app.h"
#include <xc.h>
#include <sys/attribs.h>
#include <stdbool.h>

#define INT_SOURCE_MAX_INT 190

bool SYS_INT_SourceRestore(INT_SOURCE src, int level)
{
    if(level)
    {
        SYS_INT_SourceEnable(src);
    }

    return level;
}

/* TEMPORARY FIXES*/
typedef void(*iHandler)(void* param);

typedef struct
{
    iHandler    h;  // handler
    void*       p;  // handler param
}isrDcpt;       // ISR descriptor


// registered handlers
//
static isrDcpt _sys_isrDcpt_tbl[INT_SOURCE_MAX_INT] =
{
    {0, 0 },
};

// an usual implementation will dynamically adjust the required handler for a specified vector.
// this is just a static approach
void SYS_INT_DynamicRegisterPrvt(INT_SOURCE src, void(*handler)(void* param), void* param )
{
    // minimal check for this one
    if(src <0 || src >= sizeof(_sys_isrDcpt_tbl)/sizeof(*_sys_isrDcpt_tbl) )
    {
        SYS_ASSERT(false, "");
        return;
    }

    bool iLev = SYS_INT_SourceDisable(src);

    _sys_isrDcpt_tbl[src].h = handler;
    _sys_isrDcpt_tbl[src].p = param;

    SYS_INT_SourceRestore(src, iLev);

}

void __ISR(_EXTERNAL_0_VECTOR, ipl5srs) _InterruptHandler_MRF24W_Ext0(void)
{
    DRV_WIFI_MRF24W_Tasks_ISR((SYS_MODULE_OBJ)0);
}

void __ISR ( _FLASH_CONTROL_VECTOR,ipl3srs) _InterruptHandler_NVM_stub ( void )
{
    DRV_NVM_Tasks((SYS_MODULE_OBJ)appDrvObjects.drvNVMObject);
}

void __ISR(_TIMER_2_VECTOR, ipl4) _InterruptHandler_TMR2(void)
{
    DRV_TMR_Tasks_ISR(appDrvObjects.drvTmrObject);
}

/*******************************************************************************
 End of File
 */


