/*******************************************************************************
  Audio Codec Interface

  Company:
    Microchip Technology Inc.

  File Name:
    audio_codec.h

  Summary:
    Audio Codec Interface 

  Description:
    This file describes the audio codec interface routines. 
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END
#if defined AUDIO_CODEC_AK4645A
    #include "ak4645a/ak4645a.h"
    #define AudioCodecInit  AK4645AInit
    #define AudioCodecOpen  AK4645AOpen
    #define AudioCodecWrite AK4645AWrite
    #define AudioCodecRead  AK4645ARead
    #define AudioCodecState AK4645AState
    #define AudioCodecOpenMode OPEN_MODE
    #define AudioCodecSetDacVolume AK4645ASetDACVolume
    #define AudioCodecAdjustSampleRateTx AK4645AAdjustSampleRateTx
    #define AudioCodecSetSampleRate AK4645ASetSampleRate
    #define AudioCodecStartAudio AK4645AStartAudio
    #define AudioCodecSetAdcDacOptions AK4645ASetADCDACOptions
    #define AudioCodecDacMute AK4645ADACMute
    #define AudioCodecBufferClear AK4645ABufferClear
#elif defined AUDIO_CODEC_AK4953A
    #include "audio_codec/ak4953a/ak4953a.h"
    #define AudioCodecInit  AK4953AInit
    #define AudioCodecOpen  AK4953AOpen
    #define AudioCodecWrite AK4953AWrite
    #define AudioCodecRead  AK4953ARead
    #define AudioCodecState AK4953AState
    #define AudioCodecOpenMode OPEN_MODE
    #define AudioCodecSetDacVolume AK4953ASetDACVolume
    #define AudioCodecAdjustSampleRateTx AK4953AAdjustSampleRateTx
    #define AudioCodecSetSampleRate AK4953ASetSampleRate
    #define AudioCodecStartAudio AK4953AStartAudio
    #define AudioCodecSetAdcDacOptions AK4953ASetADCDACOptions
    #define AudioCodecDacMute AK4953ADACMute
    #define AudioCodecBufferClear AK4953ABufferClear
#endif