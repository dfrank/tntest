/*******************************************************************************
 System Configuration file.

  Company:
    Microchip Technology Inc.

  File Name:
    system_config.c

  Summary:
    USB Descriptor and data structure declarations required by the USB Stack.

  Description:
    This file contains USB Descriptors and data structure declarations required
    by the USB Stack.
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "system_definitions.h" 
#include "app.h"

// *****************************************************************************
// *****************************************************************************
// Section: File Scope or Global Constants
// *****************************************************************************
// *****************************************************************************


/*******************************************
 *  USB Device Desciptor for this demo
 *******************************************/
const USB_DEVICE_DESCRIPTOR fullSpeedDeviceDescriptor =
{

    0x12,                           // Size of this descriptor in bytes
    USB_DESCRIPTOR_DEVICE,          // DEVICE descriptor type
    0x0200,                         // USB Spec Release Number in BCD format
    0xEF,                           // Class Code
    0x02,                           // Subclass code
    0x01,                           // Protocol code
    USB_DEVICE_EP0_BUFFER_SIZE,     // Max packet size for EP0, see system_config.h
    0x04D8,                         // Vendor ID
    0x0057,                         // Product ID: CDC + MSD Composite Demo
    0x0001,                         // Device release number in BCD format
    0x01,                           // Manufacturer string index
    0x02,                           // Product string index
    0x00,                           // Device serial number string index
    0x01                            // Number of possible configurations
};

/*******************************************
 *  Device Configuration Decriptor
 *******************************************/
const uint8_t configDescriptor1[]=
{
    /* Configuration Descriptor */

    0x09,                           // Size of this descriptor in bytes
    USB_DESCRIPTOR_CONFIGURATION,   // Descriptor Type
    0x62,0x00,                      // Size of the descriptor
    0x03,                           // Number of interfaces in this cfg
    0x01,                           // Index value of this configuration
    0x02,                           // Configuration string index
    0xC0,                           // Attributes
    50,                             // Max power consumption (2X mA)

    /* Interface Descriptor */

    0x09,                           // Size of this descriptor in bytes
    USB_DESCRIPTOR_INTERFACE,       // INTERFACE descriptor type
    0x00,                           // Interface Number
    0x00,                           // Alternate Setting Number
    0x02,                           // Number of endpoints in this intf
    USB_MSD_CLASS_CODE,             // Class cod
    USB_MSD_SUBCLASS_CODE_SCSI_TRANSPARENT_COMMAND_SET, // Subclass code
    USB_MSD_PROTOCOL,               // Protocol code
    0x0,                            // Interface string index

     /* Endpoint Descriptor */

    0x07,                           // Size of this descriptor
    USB_DESCRIPTOR_ENDPOINT,        // Endpoint Descriptor
    0x01 | USB_EP_DIRECTION_IN,     // EndpointAddress
    USB_TRANSFER_TYPE_BULK,         // Attributes
    0x40, 0x00,                     // Size
    0x00,                           // Interval

    0x07,                           // Size of this descriptor
    USB_DESCRIPTOR_ENDPOINT,        // Endpoint Descriptor
    0x01 | USB_EP_DIRECTION_OUT,    // EndpointAddress
    USB_TRANSFER_TYPE_BULK,         // Attributes
    0x40,                           // Size
    0x00,                           // Interval
    0x00,

    /* Interface Association Descriptor: CDC Function 1*/

    0x08,   // Size of this descriptor in bytes
    0x0B,   // Interface assocication descriptor type
    0x01,   // The first associated interface
    0x02,   // Number of contiguous associated interface
    0x02,   // bInterfaceClass of the first interface
    0x02,   // bInterfaceSubclass of the first interface
    0x01,   // bInterfaceProtocol of the first interface
    0x00,   // Interface string index

    /* Interface Descriptor */

    0x09,                                       // Size of this descriptor in bytes
    USB_DESCRIPTOR_INTERFACE,                   // Descriptor Type
    0x01,                                       // Interface Number
    0x00,                                       // Alternate Setting Number
    0x01,                                       // Number of endpoints in this intf
    USB_CDC_COMMUNICATIONS_INTERFACE_CLASS_CODE,// Class code
    USB_CDC_SUBCLASS_ABSTRACT_CONTROL_MODEL,    // Subclass code
    USB_CDC_PROTOCOL_AT_V250,                   // Protocol code
    0x00,                                       // Interface string index

    /* CDC Class-Specific Descriptors */

    sizeof(USB_CDC_HEADER_FUNCTIONAL_DESCRIPTOR),               // Size of the descriptor
    USB_CDC_DESC_CS_INTERFACE,                                  // CS_INTERFACE
    USB_CDC_FUNCTIONAL_HEADER,                                  // Type of functional descriptor
    0x20,0x01,                                                  // CDC spec version

    sizeof(USB_CDC_ACM_FUNCTIONAL_DESCRIPTOR),                  // Size of the descriptor
    USB_CDC_DESC_CS_INTERFACE,                                  // CS_INTERFACE
    USB_CDC_FUNCTIONAL_ABSTRACT_CONTROL_MANAGEMENT,             // Type of functional descriptor
    USB_CDC_ACM_SUPPORT_LINE_CODING_LINE_STATE_AND_NOTIFICATION,// bmCapabilities of ACM

    sizeof(USB_CDC_UNION_FUNCTIONAL_DESCRIPTOR_HEADER) + 1,     // Size of the descriptor
    USB_CDC_DESC_CS_INTERFACE,                                  // CS_INTERFACE
    USB_CDC_FUNCTIONAL_UNION,                                   // Type of functional descriptor
    0x01,                                                       // Communications interface number
    0x02,                                                       // Data Interface Number

    sizeof(USB_CDC_CALL_MANAGEMENT_DESCRIPTOR),                 // Size of the descriptor
    USB_CDC_DESC_CS_INTERFACE,                                  // CS_INTERFACE
    USB_CDC_FUNCTIONAL_CALL_MANAGEMENT,                         // Type of functional descriptor
    0x00,                                                       // bmCapabilities of CallManagement
    0x02,                                                       // Data interface number

    /* Interrupt Endpoint (IN)Descriptor */

    0x07,                       // Size of this descriptor
    USB_DESCRIPTOR_ENDPOINT,    // Endpoint Descriptor
    0x82,                       // EndpointAddress ( EP2 IN INTERRUPT)
    0x03,                       // Attributes type of EP (INTERRUPT)
    0x0A,0x00,                  // Max packet size of this EP
    0x02,                       // Interval (in ms)

    /* Interface Descriptor */

    0x09,                               // Size of this descriptor in bytes
    USB_DESCRIPTOR_INTERFACE,           // INTERFACE descriptor type
    0x02,                               // Interface Number
    0x00,                               // Alternate Setting Number
    0x02,
    USB_CDC_DATA_INTERFACE_CLASS_CODE,  // Class code
    0x00,                               // Subclass code
    USB_CDC_PROTOCOL_NO_CLASS_SPECIFIC, // Protocol code
    0x00,                               // Interface string index

    /* Interrupt Endpoint (IN)Descriptor */

    0x07,                       // Size of this descriptor
    USB_DESCRIPTOR_ENDPOINT,    // Endpoint Descriptor
    0x03,                       // EndpointAddress ( EP3 OUT BULK)
    0x02,                       // Attributes type of EP (BULK)
    0x40,0x00,                  // Max packet size of this EP
    0x00,                       // Interval (in ms)

     /* Interrupt Endpoint (OUT)Descriptor */

    0x07,                       // Size of this descriptor
    USB_DESCRIPTOR_ENDPOINT,    // Endpoint Descriptor
    0x83,                       // EndpointAddress ( EP3 IN )
    0x02,                       // Attributes type of EP (BULK)
    0x40,0x00,                  // Max packet size of this EP
    0x00                        // Interval (in ms)
};

/**************************************
 *  String descriptors.
 *************************************/

/* Language code string descriptor 0 */
const struct
{
    uint8_t bLength;    // Length of this descriptor
    uint8_t bDscType;   // String type descriptor
    uint16_t string[1]; // String
}
sd000 =
{
    sizeof(sd000),
    USB_DESCRIPTOR_STRING,
    {0x0409 }
};

/* Manufacturer string descriptor 1 */
const struct
{
    uint8_t bLength;    // Length of this descriptor
    uint8_t bDscType;   // String type descriptor
    uint16_t string[25];// String
}
sd001 =
{
    sizeof(sd001),
    USB_DESCRIPTOR_STRING,
    {'M','i','c','r','o','c','h','i','p',' ',
    'T','e','c','h','n','o','l','o','g','y',' ','I','n','c','.' }
};

/* Product string descriptor 2 */
const struct
{
    uint8_t bLength;    // Length of this descriptor
    uint8_t bDscType;   // String type descriptor
    uint16_t string[14];// String
}
sd002 =
{
    sizeof(sd002),
    USB_DESCRIPTOR_STRING,
    {'C','D','C',' ',
    '+',' ','M','S','D',' ','D','e','m','o' }
};

/*******************************************
 * String Descriptor Table.
 ******************************************/
USB_DEVICE_STRING_DESCRIPTORS_TABLE stringDescriptors[3]=
{
    (const uint8_t *const)&sd000,
    (const uint8_t *const)&sd001,
    (const uint8_t *const)&sd002
};

/*******************************************
 * Array of full speed config descriptors
 *******************************************/
USB_DEVICE_CONFIGURATION_DESCRIPTORS_TABLE fullSpeedConfigDescSet[]=
{
    configDescriptor1
};

/***********************************************
 * Sector buffer needed by for the MSD LUN.
 ***********************************************/
uint8_t sectorBuffer[512];

/***********************************************
 * CBW and CSW data buffers needed by the MSD
 * function driver.
 ***********************************************/
USB_MSD_CBW msdCBW;
USB_MSD_CSW msdCSW;

/*******************************************
 * MSD Function Driver initalization
 *******************************************/

USB_DEVICE_MSD_MEDIA_INIT_DATA msdMediaInit[1] =
{
    {
        DRV_NVM_INDEX_0,
        512,
        sectorBuffer,
        NULL,
        (void *)diskImage,
        {
            0x00,	// peripheral device is connected, direct access block device
            0x80,   // removable
            0x04,	// version = 00=> does not conform to any standard, 4=> SPC-2
            0x02,	// response is in format specified by SPC-2
            0x20,	// n-4 = 36-4=32= 0x20
            0x00,	// sccs etc.
            0x00,	// bque=1 and cmdque=0,indicates simple queueing 00 is obsolete,
                    // but as in case of other device, we are just using 00
            0x00,	// 00 obsolete, 0x80 for basic task queueing
            {
                'M','i','c','r','o','c','h','p'
            },
            {
                'M','a','s','s',' ','S','t','o','r','a','g','e',' ',' ',' ',' '
            },
            {
                '0','0','0','1'
            }
        },
        {
            DRV_NVM_IsAttached,
            DRV_NVM_BLOCK_Open,
            DRV_NVM_BLOCK_Close,
            DRV_NVM_GeometryGet,
            DRV_NVM_BlockRead,
            DRV_NVM_BlockEraseWrite,
            DRV_NVM_IsWriteProtected,
            DRV_NVM_BLOCK_EventHandlerSet,
            DRV_NVM_BlockStartAddressSet
        }
    }
};


USB_DEVICE_MSD_INIT msdInit =
{
    /* Number of LUNS */
    1,
    &msdCBW,
    &msdCSW,
    &msdMediaInit[0]
};

/*************************************************
 * USB CDC Device Function Driver Initialization
 * Data Structure
 *************************************************/
USB_DEVICE_CDC_INIT  cdcInit =
{
    .queueSizeRead = 1,                     // Read Queue Size
    .queueSizeWrite = 1,                    // Write Queue Size
    .queueSizeSerialStateNotification = 1   // Serial State Notification Queue Size
};

/***********************************************
 * Function Driver Registration Table
 **********************************************/
const USB_DEVICE_FUNCTION_REGISTRATION_TABLE funcRegistrationTable[2] =
{
    {
        .speed = USB_SPEED_FULL|USB_SPEED_HIGH,     // Speed at which this device can operate
        .configurationValue = 1,                    // Configuration number to which this device belongs
        .interfaceNumber = 1,                       // Starting interface number for this function driver
        .numberOfInterfaces = 2,                    // Number of interfaces that this function driver owns.
        .funcDriverIndex = 0,                       // Function Driver index
        .funcDriverInit = &cdcInit,                 // Function Driver initialization data structure
        .driver = (void *)USB_DEVICE_CDC_FUNCTION_DRIVER    // CDC Function Driver.
    },
    {
        .speed = USB_SPEED_FULL|USB_SPEED_HIGH,     // Speed at which this device can operate
        .configurationValue = 1,                    // Configuration number to which this device belongs
        .interfaceNumber = 0,                       // Starting interface number for this function driver
        .numberOfInterfaces = 1,                    // Number of interfaces that this function driver owns.
        .funcDriverIndex = 0,                       // Function Driver index
        .funcDriverInit = &msdInit,                 // Function Driver initialization data structure
        .driver = USB_DEVICE_MSD_FUNCTION_DRIVER    // MSD Function Driver.
    },
};

/*************************************************
 * USB Device Master Descriptor
 ************************************************/
const USB_DEVICE_MASTER_DESCRIPTOR usbMasterDescriptor =
{
    &fullSpeedDeviceDescriptor, /* Full speed descriptor */
    1,                          /* Total number of full speed configurations available */
    &fullSpeedConfigDescSet[0], /* Pointer to array of full speed configurations descriptors*/

    NULL,                       /* High speed device desc is not supported*/
    0,                          /* Total number of high speed configurations available */
    NULL,                       /* Pointer to array of high speed configurations descriptors. Not supported*/

    3,                          /* Total number of string descriptors available */
    stringDescriptors,          /* Pointer to array of string descriptors */

    NULL,                       /* Pointer to full speed dev qualifier. Not supported */
    NULL,                       /* Pointer to high speed dev qualifier. Not supported */
};


