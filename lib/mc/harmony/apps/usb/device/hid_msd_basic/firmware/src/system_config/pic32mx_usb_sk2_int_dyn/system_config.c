/*******************************************************************************
 System Configuration file.

  Company:
    Microchip Technology Inc.

  File Name:
    system_config.c

  Summary:
    USB Descriptor and data structure declarations required by the USB Stack.

  Description:
    This file contains USB Descriptors and data structure declarations required
    by the USB Stack.
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "system_definitions.h" 
#include "app.h"

// *****************************************************************************
// *****************************************************************************
// Section: File Scope or Global Constants
// *****************************************************************************
// *****************************************************************************


/*******************************************
 *  USB Device Desciptor for this demo
 *******************************************/
const USB_DEVICE_DESCRIPTOR fullSpeedDeviceDescriptor =
{

    0x12,                           // Size of this descriptor in bytes
    USB_DESCRIPTOR_DEVICE,          // DEVICE descriptor type
    0x0200,                         // USB Spec Release Number in BCD format
    0x00,                           // Class Code
    0x00,                           // Subclass code
    0x00,                           // Protocol code
    USB_DEVICE_EP0_BUFFER_SIZE,     // Max packet size for EP0, see system_config.h
    0x04D8,                         // Vendor ID
    0x0054,                         // Product ID: CDC + MSD Composite Demo
    0x0001,                         // Device release number in BCD format
    0x01,                           // Manufacturer string index
    0x02,                           // Product string index
    0x00,                           // Device serial number string index
    0x01                            // Number of possible configurations
};

/*******************************************
 *  Device Configuration Decriptor
 *******************************************/
const uint8_t configDescriptor1[]=
{
    /* Configuration Descriptor */

    0x09,                           // Size of this descriptor in bytes
    USB_DESCRIPTOR_CONFIGURATION,   // Descriptor Type
    64,0x00,                        // Size of the descriptor
    0x02,                           // Number of interfaces in this cfg
    0x01,                           // Index value of this configuration
    0x00,                           // Configuration string index
    0xC0,                           // Attributes
    50,                             // Max power consumption (2X mA)


    /* Interface Descriptor */

    0x09,                           // Size of this descriptor in bytes
    USB_DESCRIPTOR_INTERFACE,       // INTERFACE descriptor type
    0x00,                           // MSD Interface Number
    0x00,                           // Alternate Setting Number
    0x02,                           // Number of endpoints in this intf
    USB_MSD_CLASS_CODE,             // Class code
    USB_MSD_SUBCLASS_CODE_SCSI_TRANSPARENT_COMMAND_SET, // Subclass code
    USB_MSD_PROTOCOL,               // Protocol code
    0x0,                            // Interface string index

     /* Endpoint Descriptor */

    0x07,                           // Size of this descriptor
    USB_DESCRIPTOR_ENDPOINT,        // Endpoint Descriptor
    0x01 | USB_EP_DIRECTION_IN,     // EndpointAddress
    USB_TRANSFER_TYPE_BULK,         // Attributes
    0x40, 0x00,                     // Size
    0x00,                           // Interval

    0x07,                           // Size of this descriptor
    USB_DESCRIPTOR_ENDPOINT,        // Endpoint Descriptor
    0x01 | USB_EP_DIRECTION_OUT,    // EndpointAddress
    USB_TRANSFER_TYPE_BULK,         // Attributes
    0x40,                           // Size
    0x00,                           // Interval
    0x00,

    /* Interface Descriptor */

    0x09,			            // Size of this descriptor in bytes
    USB_DESCRIPTOR_INTERFACE,   // INTERFACE descriptor type
    0x01,                       // HID Interface Number
    0x00,                       // Alternate Setting Number
    0x02,                       // Number of endpoints in this intf
    USB_HID_CLASS_CODE,         // Class code
    0x00,     		            // Subclass code
    0x00,     			        // Protocol code
    0x00,                       // Interface string index

    /* HID Class-Specific Descriptor */

    0x09,			    			// Size of this descriptor in bytes
    USB_HID_DESCRIPTOR_TYPES_HID,   // HID descriptor type
    0x11,0x01,                      // HID Spec Release Number in BCD format (1.11)
    0x00,                           // Country Code (0x00 for Not supported)
    0x01,                           // Number of class descriptors, see usbcfg.h
    USB_HID_DESCRIPTOR_TYPES_REPORT,// Report descriptor type
    0x1C,0x00,            // Size of the report descriptor

    /* Endpoint Descriptor */

    0x07,
    USB_DESCRIPTOR_ENDPOINT,        // Endpoint Descriptor
    0x2 | USB_EP_DIRECTION_IN,      // EndpointAddress
    USB_TRANSFER_TYPE_INTERRUPT,    // Attributes
    0x40,0x00,                      // Size
    0x01,                           // Interval

    /* Endpoint Descriptor */

    0x07,
    USB_DESCRIPTOR_ENDPOINT,        // Endpoint Descriptor
    0x2 | USB_EP_DIRECTION_OUT,     // EndpointAddress
    USB_TRANSFER_TYPE_INTERRUPT,    // Attributes
    0x40,0x00,                      // Size
    0x01,                           // Interval

};

/**************************************
 *  String descriptors.
 *************************************/

/* Language code string descriptor 0 */
const struct
{
    uint8_t bLength;    // Length of this descriptor
    uint8_t bDscType;   // String type descriptor
    uint16_t string[1]; // String
}
sd000 =
{
    sizeof(sd000),
    USB_DESCRIPTOR_STRING,
    {0x0409 }
};

/* Manufacturer string descriptor 1 */
const struct
{
    uint8_t bLength;    // Length of this descriptor
    uint8_t bDscType;   // String type descriptor
    uint16_t string[25];// String
}
sd001 =
{
    sizeof(sd001),
    USB_DESCRIPTOR_STRING,
    {'M','i','c','r','o','c','h','i','p',' ',
    'T','e','c','h','n','o','l','o','g','y',' ','I','n','c','.' }
};

/* Product string descriptor 2 */
const struct
{
    uint8_t bLength;    // Length of this descriptor
    uint8_t bDscType;   // String type descriptor
    uint16_t string[14];// String
}
sd002 =
{
    sizeof(sd002),
    USB_DESCRIPTOR_STRING,
    {'H','I','D',' ',
    '+',' ','M','S','D',' ','D','e','m','o' }
};

/*******************************************
 * String Descriptor Table.
 ******************************************/
USB_DEVICE_STRING_DESCRIPTORS_TABLE stringDescriptors[3]=
{
    (const uint8_t *const)&sd000,
    (const uint8_t *const)&sd001,
    (const uint8_t *const)&sd002
};

/*******************************************
 * Array of full speed config descriptors
 *******************************************/
USB_DEVICE_CONFIGURATION_DESCRIPTORS_TABLE fullSpeedConfigDescSet[]=
{
    configDescriptor1
};

/***********************************************
 * Sector buffer needed by for the MSD LUN.
 ***********************************************/
uint8_t sectorBuffer[512];

/***********************************************
 * CBW and CSW data buffers needed by the MSD
 * function driver.
 ***********************************************/
USB_MSD_CBW msdCBW;
USB_MSD_CSW msdCSW;

/*******************************************
 * MSD Function Driver initalization
 *******************************************/

USB_DEVICE_MSD_MEDIA_INIT_DATA msdMediaInit[1] =
{
    {
        DRV_NVM_INDEX_0,
        512,
        sectorBuffer,
        NULL,
        (void *)diskImage,
        {
            0x00,	// peripheral device is connected, direct access block device
            0x80,   // removable
            0x04,	// version = 00=> does not conform to any standard, 4=> SPC-2
            0x02,	// response is in format specified by SPC-2
            0x20,	// n-4 = 36-4=32= 0x20
            0x00,	// sccs etc.
            0x00,	// bque=1 and cmdque=0,indicates simple queueing 00 is obsolete,
                    // but as in case of other device, we are just using 00
            0x00,	// 00 obsolete, 0x80 for basic task queueing
            {
                'M','i','c','r','o','c','h','p'
            },
            {
                'M','a','s','s',' ','S','t','o','r','a','g','e',' ',' ',' ',' '
            },
            {
                '0','0','0','1'
            }
        },
        {
            DRV_NVM_IsAttached,
            DRV_NVM_BLOCK_Open,
            DRV_NVM_BLOCK_Close,
            DRV_NVM_GeometryGet,
            DRV_NVM_BlockRead,
            DRV_NVM_BlockEraseWrite,
            DRV_NVM_IsWriteProtected,
            DRV_NVM_BLOCK_EventHandlerSet,
            DRV_NVM_BlockStartAddressSet
        }
    }
};


USB_DEVICE_MSD_INIT msdInit =
{
    /* Number of LUNS */
    1,
    &msdCBW,
    &msdCSW,
    &msdMediaInit[0]
};


/******************************************************
 * Class specific descriptor - HID Report descriptor
 ******************************************************/
const uint8_t hid_rpt01[] =
{
    0x06, 0x00, 0xFF,       // Usage Page = 0xFF00 (Vendor Defined Page 1)
    0x09, 0x01,             // Usage (Vendor Usage 1)
    0xA1, 0x01,             // Collection (Application)
    0x19, 0x01,             // Usage Minimum
    0x29, 0x40,             // Usage Maximum 	//64 input usages total (0x01 to 0x40)
    0x15, 0x01,             // Logical Minimum (data bytes in the report may have minimum value = 0x00)
    0x25, 0x40,      	    // Logical Maximum (data bytes in the report may have maximum value = 0x00FF = unsigned 255)
    0x75, 0x08,             // Report Size: 8-bit field size
    0x95, 0x40,             // Report Count: Make sixty-four 8-bit fields (the next time the parser hits an "Input", "Output", or "Feature" item)
    0x81, 0x00,             // Input (Data, Array, Abs): Instantiates input packet fields based on the above report size, count, logical min/max, and usage.
    0x19, 0x01,             // Usage Minimum
    0x29, 0x40,             // Usage Maximum 	//64 output usages total (0x01 to 0x40)
    0x91, 0x00,             // Output (Data, Array, Abs): Instantiates output packet fields.  Uses same report size and count as "Input" fields, since nothing new/different was specified to the parser since the "Input" item.
    0xC0                    // End Collection
};


/*******************************************
 * USB HID Function Driver Initialization
 * Data Structure.
 *******************************************/
USB_DEVICE_HID_INIT hidInit =
{
    .hidReportDescriptorSize = sizeof(hid_rpt01),   // Size of the HID report
    .hidReportDescriptor = (uint8_t *)&hid_rpt01,   // Pointer to the report
    .queueSizeReportReceive = 1,                    // Size of receive queue size
    .queueSizeReportSend = 1,                       // Size of send queue size
};

/***********************************************
 * Function Driver Registration Table
 **********************************************/
const USB_DEVICE_FUNCTION_REGISTRATION_TABLE funcRegistrationTable[2] =
{
      {
        .speed = USB_SPEED_FULL,                    // Speed at which this function can operate
        .configurationValue = 1,                    // Configuration number to which this device belongs
        .interfaceNumber = 0,                       // Starting interface number for this function driver
        .numberOfInterfaces = 1,                    // Number of interfaces that this function driver owns.
        .funcDriverIndex = 0,                       // Function Driver index
        .funcDriverInit = &msdInit,                 // Function Driver initialization data structure
        .driver = USB_DEVICE_MSD_FUNCTION_DRIVER    // MSD Function Driver.
    },
    {
        .speed = USB_SPEED_FULL,                    // Speed at which this function should operate
        .configurationValue = 1,                    // Configuration Descriptor Index.
        .interfaceNumber = 1,                       // Start interface number
        .numberOfInterfaces = 1,                    // Number of interfaces
        .funcDriverIndex = 0,                       // Instance index of the HID function Driver
        .funcDriverInit = &hidInit,                 // HID Initialization data for this instance
        .driver = (void *)USB_DEVICE_HID_FUNCTION_DRIVER,   // HID APIs exposed to the device layer.
    },
};

/*************************************************
 * USB Device Master Descriptor
 ************************************************/
const USB_DEVICE_MASTER_DESCRIPTOR usbMasterDescriptor =
{
    &fullSpeedDeviceDescriptor, /* Full speed descriptor */
    1,                          /* Total number of full speed configurations available */
    &fullSpeedConfigDescSet[0], /* Pointer to array of full speed configurations descriptors*/

    NULL,                       /* High speed device desc is not supported*/
    0,                          /* Total number of high speed configurations available */
    NULL,                       /* Pointer to array of high speed configurations descriptors. Not supported*/

    3,                          /* Total number of string descriptors available */
    stringDescriptors,          /* Pointer to array of string descriptors */

    NULL,                       /* Pointer to full speed dev qualifier. Not supported */
    NULL,                       /* Pointer to high speed dev qualifier. Not supported */
};
