/*******************************************************************************
 System Configuration file.

  Company:
    Microchip Technology Inc.

  File Name:
    system_config.c

  Summary:
    USB Descriptor and data structure declarations required by the USB Stack.

  Description:
    This file contains USB Descriptors and data structure declarations required
    by the USB Stack.
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "system_definitions.h" 
#include "app.h"

// *****************************************************************************
// *****************************************************************************
// Section: File Scope or Global Constants
// *****************************************************************************
// *****************************************************************************



/*******************************************
 *  USB Device Desciptor for this demo
 *******************************************/
const USB_DEVICE_DESCRIPTOR device_dsc=
{
    0x12,                       // Size of this descriptor in bytes
    USB_DESCRIPTOR_DEVICE,      // DEVICE descriptor type
    0x0200,                     // USB Spec Release Number in BCD format
    0x00,                       // Class Code
    0x00,                       // Subclass code
    0x00,                       // Protocol code
    USB_DEVICE_EP0_BUFFER_SIZE, // Max packet size for EP0, see usbcfg.h
    0x04D8,                     // Vendor ID
    0x0009,                     // Product ID: mass storage device demo
    0x0001,                     // Device release number in BCD format
    0x01,                       // Manufacturer string index
    0x02,                       // Product string index
    0x03,                       // Device serial number string index
    0x01                        // Number of possible configurations
};

/*******************************************
 *  USB Device Qualifier Descriptor
 *******************************************/
const USB_DEVICE_QUALIFIER deviceQualifierDescriptor1 =
{
    0x0A,                               // Size of this descriptor in bytes
    USB_DESCRIPTOR_DEVICE_QUALIFIER,    // Device Qualifier Type
    0x0200,                             // USB Specification Release number
    0x02,                               // Class Code
    0x00,                               // SubClass Code
    0x00,                               // Protocol code
    0x00,                               // Maximum packet size for endpoint 0
    0x64,                               // Number of possible configurations
    0x00,                               // Reserved for future use.
};

/*******************************************
 *  Configuration Descriptor
 *******************************************/
const uint8_t configurationDescriptor1[] =
{
    /* Configuration Descriptor */

    9,                                                          // Size of this descriptor in bytes
    USB_DESCRIPTOR_CONFIGURATION,                               // CONFIGURATION descriptor type
    0x20,0x00,                                                  // Total length of data for this cfg
    1,                                                          // Number of interfaces in this cfg
    1,                                                          // Index value of this configuration
    0,                                                          // Configuration string index
    USB_ATTRIBUTE_DEFAULT | USB_ATTRIBUTE_SELF_POWERED,         // Attributes, see usbdefs_std_dsc.h
    50,                                                         // Max power consumption (2X mA)

    /* Interface Descriptor */

    9,                          // Size of this descriptor in bytes
    USB_DESCRIPTOR_INTERFACE,   // INTERFACE descriptor type
    0,                          // Interface Number
    0,                          // Alternate Setting Number
    2,                          // Number of endpoints in this intf
    USB_MSD_CLASS_CODE,         // Class code
    USB_MSD_SUBCLASS_CODE_SCSI_TRANSPARENT_COMMAND_SET,      // Subclass code
    USB_MSD_PROTOCOL, 		// Protocol code
    0,                          // Interface string index

    /* Endpoint Descriptor */

    7,                          // Size of this descriptor in bytes
    USB_DESCRIPTOR_ENDPOINT,    // Endpoint Descriptor
    0x01 | USB_EP_DIRECTION_IN, // EndpointAddress ( EP1 IN BULK)
    USB_TRANSFER_TYPE_BULK,     // Attributes type of EP (BULK)
    0x40,0x00,                  // Max packet size of this EP
    0x00,                       // Interval (in ms)


    7,                          // Size of this descriptor in bytes
    USB_DESCRIPTOR_ENDPOINT,    // Endpoint Descriptor
    0x01 | USB_EP_DIRECTION_OUT,// EndpointAddress ( EP1 IN BULK)
    USB_TRANSFER_TYPE_BULK,     // Attributes type of EP (BULK)
    0x40,0x00,                  // Max packet size of this EP
    0x00                       // Interval (in ms)
};

/*****************************
 * String Descriptors
 *****************************/

/* Language code string descriptor */
const struct
{
    uint8_t bLength;
    uint8_t bDscType;
    uint16_t string[1];
}
sd000 =
{
    sizeof(sd000),
    USB_DESCRIPTOR_STRING,
    {0x0409}
};

/* Manufacturer string descriptor */
const struct
{
    const uint8_t bLength;
    uint8_t bDscType;
    uint16_t string[25];
}
sd001 =
{
    sizeof(sd001),
    USB_DESCRIPTOR_STRING,
    {'M','i','c','r','o','c','h','i','p',' ',
    'T','e','c','h','n','o','l','o','g','y',' ','I','n','c','.' }
};

/* Product string descriptor */
const struct
{
    const uint8_t bLength;
    uint8_t bDscType;
    uint16_t string[22];
}
sd002 =
{
    sizeof(sd002),
    USB_DESCRIPTOR_STRING,
    {'S','i','m','p','l','e',' ','M','S','D',' ',
    'D','e','v','i','c','e',' ','D','e','m','o' }
};

/******************************************************************************
 * Serial number string descriptor.  Note: This should be unique for each unit
 * built on the assembly line.  Plugging in two units simultaneously with the
 * same serial number into a single machine can cause problems.  Additionally,
 * not all hosts support all character values in the serial number string.  The
 * MSD Bulk Only Transport (BOT) specs v1.0 restrict the serial number to
 * consist only of ASCII characters "0" through "9" and capital letters "A"
 * through "F".
 ******************************************************************************/
const struct
{
    uint8_t bLength;
    uint8_t bDscType;
    uint16_t string[12];
}
sd003 =
{
    sizeof(sd003),
    USB_DESCRIPTOR_STRING,
    {'1','2','3','4','5','6','7','8','9','9','9','9'}
};

/***************************************
 * Array of string descriptors
 ***************************************/
USB_DEVICE_STRING_DESCRIPTORS_TABLE stringDescriptors[4]=
{
    (const uint8_t *const)&sd000,
    (const uint8_t *const)&sd001,
    (const uint8_t *const)&sd002,
    (const uint8_t *const)&sd003
};

/*******************************************
 * Array of speed config descriptors
 *******************************************/
USB_DEVICE_CONFIGURATION_DESCRIPTORS_TABLE configurationDescTable[1] =
{
    configurationDescriptor1
};

/***********************************************
 * Sector buffer needed by for the MSD LUN.
 ***********************************************/
uint8_t sectorBuffer[512] __attribute__((coherent)) __attribute__((aligned(4)));

/***********************************************
 * CBW and CSW structure needed by for the MSD
 * function driver instance.
 ***********************************************/
USB_MSD_CBW msdCBW __attribute__((coherent)) __attribute__((aligned(4)));
USB_MSD_CSW msdCSW __attribute__((coherent)) __attribute__((aligned(4)));

/***********************************************
 * Because the PIC32MZ flash row size if 2048
 * and the media sector size if 512 bytes, we
 * have to allocate a buffer of size 2048
 * to backup the row. A pointer to this row
 * is passed in the media initalization data
 * structure.
 ***********************************************/
uint8_t flashRowBackupBuffer [DRV_NVM_ROW_SIZE];

/*******************************************
 * MSD Function Driver initalization
 *******************************************/

USB_DEVICE_MSD_MEDIA_INIT_DATA msdMediaInit[1] =
{
    {
        DRV_NVM_INDEX_0,
        512,
        sectorBuffer,
        flashRowBackupBuffer,
        (void *)diskImage,
        {
            0x00,	// peripheral device is connected, direct access block device
            0x80,       // removable
            0x04,	// version = 00=> does not conform to any standard, 4=> SPC-2
            0x02,	// response is in format specified by SPC-2
            0x20,	// n-4 = 36-4=32= 0x20
            0x00,	// sccs etc.
            0x00,	// bque=1 and cmdque=0,indicates simple queueing 00 is obsolete,
                        // but as in case of other device, we are just using 00
            0x00,	// 00 obsolete, 0x80 for basic task queueing
            {
                'M','i','c','r','o','c','h','p'
            },
            {
                'M','a','s','s',' ','S','t','o','r','a','g','e',' ',' ',' ',' '
            },
            {
                '0','0','0','1'
            }
        },
        {
            DRV_NVM_IsAttached,
            DRV_NVM_BLOCK_Open,
            DRV_NVM_BLOCK_Close,
            DRV_NVM_GeometryGet,
            DRV_NVM_BlockRead,
            DRV_NVM_BlockEraseWrite,
            DRV_NVM_IsWriteProtected,
            DRV_NVM_BLOCK_EventHandlerSet,
            DRV_NVM_BlockStartAddressSet
        }
    }
};

/*******************************************
 * MSD Function Driver initalization
 *******************************************/

USB_DEVICE_MSD_INIT msdInit =
{
    /* Number of LUNS */
    1,

    /* Pointer to a CBW structure */
    &msdCBW,

    /* Pointer to a CSW structure */
    &msdCSW,

    /* Pointer to a table of Media Intialization data strucutres */
    &msdMediaInit[0]
};


/*****************************************************
 * USB Device Function Registration Table
 *****************************************************/
const USB_DEVICE_FUNCTION_REGISTRATION_TABLE funcRegistrationTable[1] =
{
    {
        .speed = USB_SPEED_FULL | USB_SPEED_HIGH,   // Device Speed
        .configurationValue =  1,                   // Configuration value
        .interfaceNumber = 0,                       // Start interface number
        .numberOfInterfaces = 1,                    // Number of interfaces owned
        .funcDriverIndex = 0,                       // Function driver index
        .funcDriverInit = (void*)&msdInit,          // Pointer to initialization data stucture
        .driver = USB_DEVICE_MSD_FUNCTION_DRIVER    // Pointer to function driver
    }
};

/***********************************************
 * USB Device Master Descriptor
 ***********************************************/

const USB_DEVICE_MASTER_DESCRIPTOR usbMasterDescriptor =
{
    &device_dsc,                /* Full speed descriptor */
    1,                          /* Total number of full speed configurations available */
    &configurationDescTable[0], /* Pointer to array of full speed configurations descriptors*/

    &device_dsc,                /* High speed device desc is not supported*/
    1,                          /* Total number of high speed configurations available */
    &configurationDescTable[0], /* Pointer to array of high speed configurations descriptors. Not supported*/

    4,                          /* Total number of string descriptors available */
    stringDescriptors,          /* Pointer to array of string descriptors */

    &deviceQualifierDescriptor1, /* Pointer to full speed dev qualifier. Not supported */
    NULL,                       /* Pointer to high speed dev qualifier. Not supported */
};

