/*******************************************************************************
  CDC Basic Demo Configuration Header

  Company:
    Microchip Technology Inc.

  File Name:
    system_config.h

  Summary:
    Top-level configuration header for the PIC32 USB Starter Kit II.

  Description:
    This file is the top-level configuration header for the CDC Basic demo
    application for the PIC32 USB Starter Kit II.
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

#ifndef _SYS_CONFIG_H
#define _SYS_CONFIG_H

/* This is a temporary workaround for an issue with the peripheral library "Exists"
   functions that causes superfluous warnings.  It "nulls" out the definition of
   The PLIB function attribute that causes the warning.  Once that issue has been
   resolved, this definition should be removed. */
#define _PLIB_UNSUPPORTED




// *****************************************************************************
// *****************************************************************************
// Section: USB Host Layer Configuration
// *****************************************************************************
// **************************************************************************

/* Target peripheral list entries */
#define  USB_HOST_TPL_ENTRIES                1

/* Total number of drivers in this application */
#define USB_HOST_MAX_DRIVER_SUPPORTED        1    

/* Number of Host Layer Instances */
#define USB_HOST_INSTANCES_NUMBER            1   

/* Total number of devices to be supported */
#define USB_HOST_MAX_DEVICES_NUMBER          1    

/* Maximum number of configurations supported per device */
#define USB_HOST_MAX_CONFIGURATION           1    

/* Maximum number of interfaces supported per device */
#define USB_HOST_MAX_INTERFACES              2    

/* Number of Host Layer Clients */
#define USB_HOST_CLIENTS_NUMBER              1

// *****************************************************************************
// *****************************************************************************
// Section: USB Host Controller Driver Configuration
// *****************************************************************************
// *****************************************************************************

/* Only one instance of the USB Host Controller */
#define DRV_USB_INSTANCES_NUMBER    1

/* Enables interrupt mode */
#define DRV_USB_INTERRUPT_MODE  	false

/* Enables Host Support */
#define DRV_USB_HOST_SUPPORT		true

/* Disables Device support */
#define DRV_USB_DEVICE_SUPPORT	 	false

/* Provides Host pipes number */
#define DRV_USB_HOST_PIPES_NUMBER 	5

/* In Host mode, only one endpoint is needed */
#define DRV_USB_ENDPOINTS_NUMBER	1

/* NAK Limit for Control transfer data stage and Status Stage */
#define DRV_USB_HOST_NAK_LIMIT		200

// *****************************************************************************
// *****************************************************************************
// Section: CDC Class Driver Configuration
// *****************************************************************************
// *****************************************************************************

/* Number of CDC class driver instances */
#define  USB_HOST_CDC_INSTANCES_NUMBER   1

/* CDC Class Driver App Event Handler */
#define  USB_HOST_CDC_APP_EVENT_HANDLER      APP_USBHostCDCEventHandler

// *****************************************************************************
// *****************************************************************************
// Section: SYS CLK Configuration
// *****************************************************************************
// *****************************************************************************

/* Primary crystal frequency */
#define SYS_CLK_CONFIG_PRIMARY_XTAL               8000000L

/* PLL input clock divider */
#define SYS_CLK_CONFIG_SYSPLL_INP_DIVISOR         2

/* No secondary crystal */
#define SYS_CLK_CONFIG_SECONDARY_XTAL             0L

/* USB PLL is enabled */
#define SYS_CLK_CONFIGBIT_USBPLL_ENABLE           true

/* Frequency tolerance while calculating frequency */
#define SYS_CLK_CONFIG_FREQ_ERROR_LIMIT           10

/* USB PLL input divider */
#define SYS_CLK_CONFIGBIT_USBPLL_DIVISOR          2

// *****************************************************************************
// *****************************************************************************
// Section: Timer Driver Configuration
// *****************************************************************************
// *****************************************************************************

/* Timer driver runs in interrupt mode */
#define DRV_TMR_INTERRUPT_MODE          true

/* Number of timer driver instances */
#define DRV_TMR_INSTANCES_NUMBER        1

/* Number of timer driver clients */
#define DRV_TMR_CLIENTS_NUMBER          1

// *****************************************************************************
// *****************************************************************************
// Section: System Timer Service Configuration
// *****************************************************************************
// *****************************************************************************

/* Number of System timer delay objects */
#define SYS_TMR_MAX_DELAY_EVENTS        10
	
// *****************************************************************************
// *****************************************************************************
// Section: DEVCON System Service Configuration
// *****************************************************************************
// *****************************************************************************
#define SYS_DEVCON_PIC32MX_MAX_PB_FREQ          80000000

#endif // _SYS_CONFIG_H
/* ******************************************************************************
 End of File
*/

