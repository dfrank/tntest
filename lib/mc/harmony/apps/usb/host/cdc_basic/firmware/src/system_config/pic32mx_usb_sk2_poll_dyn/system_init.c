/*******************************************************************************
 System Initialization File

  Company:
    Microchip Technology Inc.

  File Name:
    system_init.c

  Summary:
    System Initialization.

  Description:
    This file contains source code necessary to initialize the system.
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2011-2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

#include "app.h"
#include "system_definitions.h"

// ****************************************************************************
// ****************************************************************************
// Section: Configuration Bits
// ****************************************************************************
// ****************************************************************************

#pragma config UPLLEN   = ON            // USB PLL Enabled
#pragma config FPLLMUL  = MUL_20        // PLL Multiplier
#pragma config UPLLIDIV = DIV_2         // USB PLL Input Divider
#pragma config FPLLIDIV = DIV_2         // PLL Input Divider
#pragma config FPLLODIV = DIV_1         // PLL Output Divider
#pragma config FPBDIV   = DIV_1         // Peripheral Clock divisor
#pragma config FWDTEN   = OFF           // Watchdog Timer
#pragma config WDTPS    = PS1           // Watchdog Timer Postscale
#pragma config FCKSM    = CSDCMD        // Clock Switching & Fail Safe Clock Monitor
#pragma config OSCIOFNC = OFF           // CLKO Enable
#pragma config POSCMOD  = HS            // Primary Oscillator
#pragma config IESO     = OFF           // Internal/External Switch-over
#pragma config FSOSCEN  = OFF           // Secondary Oscillator Enable 
#pragma config FNOSC    = PRIPLL        // Oscillator Selection
#pragma config CP       = OFF           // Code Protect
#pragma config BWP      = OFF           // Boot Flash Write Protect
#pragma config PWP      = OFF           // Program Flash Write Protect
#pragma config ICESEL   = ICS_PGx2      // ICE/ICD Comm Channel Select

/******************************************************************************
 * USB Host Layer System Module Object.
 *****************************************************************************/

SYSTEM_OBJECTS sysObjects;

/****************************************************
 * Endpoint Table needed by the controller driver .
 ****************************************************/

uint8_t __attribute__((aligned(512))) endpointTable[USB_HOST_ENDPOINT_TABLE_SIZE];

/******************************************************************************
 * USB Embedded Host Layer Initialization data.
 *****************************************************************************/

USB_HOST_INIT    usbHostInitData =
{
    /* System module initialization */
    .moduleInit = {SYS_MODULE_POWER_RUN_FULL},

    /* Identifies peripheral (PLIB-level) ID */
    .usbID = USB_ID_1,

    /* Identifies the Idle mode behavior. If true the USB module will
     * stop when the microcontroller enters IDLE mode */
    .stopInIdle = false ,

    /* If true, the USB module will automatically suspend when the the
     * microcontroller enter sleep mode */
    .suspendInSleep = false ,

    /* Pointer to endpoint Table */
    .endpointTable = endpointTable,

    /* Interrupt Source for USB module */
    .interruptSource = INT_SOURCE_USB_1,

    /* Number of entries in the TPL table */
    .nTPLEntries     = USB_HOST_TPL_ENTRIES,

    /* USB Device Speed */
    .usbSpeed = USB_SPEED_FULL,

    /* Pointer to the TPL list */
    .tplList = (USB_HOST_TARGET_PERIPHERAL_LIST *)(USBTPList)
};

/****************************************************
 * Timer Driver Initialization Data
 ****************************************************/

DRV_TMR_INIT drvTMR1Init =
{
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .tmrId            = TMR_ID_2,
    .clockSource      = TMR_CLOCK_SOURCE_PERIPHERAL_CLOCK,
    .syncMode         = DRV_TMR_SYNC_MODE_SYNCHRONOUS_INTERNAL,
    .timerPeriod      = 0xC35, //For 10MS
    .prescale         = TMR_PRESCALE_VALUE_256,
    .combineTimers    = false,
    .interruptSource  = INT_SOURCE_TIMER_2,
};

/****************************************************
 *  System Timer service Initialization Data
 ****************************************************/

SYS_TMR_INIT sysTmrInit =
{
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .drvIndex = DRV_TMR_INDEX_0,
    .alarmPeriodMs = 10,
};

/*************************************************
 * DEVCON System Service Initialization
 ************************************************/

SYS_DEVCON_INIT devconInit = 
{
    .moduleInit = {0},
};

// ****************************************************************************
// ****************************************************************************
// Section: System Initialization
// ****************************************************************************
// ****************************************************************************

/*******************************************************************************
  Function:
    void SYS_Initialize ( SYS_INIT_DATA *data )

  Summary:
    Initializes the board, services, drivers, application and other modules

  Description:
    This routine initializes the board, services, drivers, application and other
    modules as configured at build time.  In a bare-metal environment (where no
    OS is supported), this routine should be called almost immediately after
    entering the "main" routine.

  Remarks:
    Refer to sys_module.h for usage information.
*/

void SYS_Initialize ( void* data )
{
    /* Set up cache and wait states for maximum performance. */
    SYS_DEVCON_Initialize(SYS_DEVCON_INDEX_0, (SYS_MODULE_INIT *)&devconInit);
    SYS_DEVCON_PerformanceConfig(80000000);
    SYS_CLK_Initialize(NULL);

    /* Initialize the BSP */
    BSP_Initialize( );

    /* Timer driver Initialize */
    sysObjects.tmrObject =  DRV_TMR_Initialize( DRV_TMR_INDEX_0 ,
            (SYS_MODULE_INIT *)&drvTMR1Init);

    sysObjects.systmrObject = SYS_TMR_Initialize(SYS_TMR_INDEX_0,
            (SYS_MODULE_INIT *)&sysTmrInit);

    /* Initialize the USB host layer */
    sysObjects.usbHostObject = USB_HOST_Initialize (USB_HOST_INDEX_0 ,
            ( SYS_MODULE_INIT *)& usbHostInitData );

    /* Check if the object returned by the device layer is valid */
    SYS_ASSERT((SYS_MODULE_OBJ_INVALID != sysObjects.usbHostObject),
            "Invalid USB HOST object");

    /* Initialize the Application */
    APP_Initialize ();

    /* Initializethe interrupt system  */
    SYS_INT_Initialize();

    /* Set the interrupt vector priority for USB and Timer 2 interrupt */
    SYS_INT_VectorPrioritySet(INT_VECTOR_USB, INT_PRIORITY_LEVEL3);
    SYS_INT_VectorSubprioritySet(INT_VECTOR_USB, INT_SUBPRIORITY_LEVEL0);
    SYS_INT_VectorPrioritySet (INT_VECTOR_T2, INT_PRIORITY_LEVEL2);
    SYS_INT_VectorSubprioritySet (INT_VECTOR_T2, INT_PRIORITY_LEVEL0);

    /* Enable the global interrupts */
    SYS_INT_Enable();

}


/*******************************************************************************
 End of File
*/
