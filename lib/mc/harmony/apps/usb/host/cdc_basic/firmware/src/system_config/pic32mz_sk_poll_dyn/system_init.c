/*******************************************************************************
 System Initialization File

  Company:
    Microchip Technology Inc.

  File Name:
    system_init.c

  Summary:
    System Initialization.

  Description:
    This file contains source code necessary to initialize the system.
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2011-2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

#include "app.h"
#include "system_definitions.h"

// ****************************************************************************
// ****************************************************************************
// Section: Configuration Bits
// ****************************************************************************
// ****************************************************************************

#pragma config FMIIEN   = OFF               // Ethernet RMII/MII Enable (MII Enabled)
#pragma config FETHIO   = ON                // Ethernet I/O Pin Select (Default Ethernet I/O)
#pragma config PGL1WAY  = OFF               // Permission Group Lock One Way Configuration (Allow only one reconfiguration)
#pragma config PMDL1WAY = OFF               // Peripheral Module Disable Configuration (Allow only one reconfiguration)
#pragma config IOL1WAY  = OFF               // Peripheral Pin Select Configuration (Allow only one reconfiguration)
#pragma config FUSBIDIO = OFF               // USB USBID Selection (Controlled by Port Function)
#pragma config FPLLIDIV = DIV_3             // System PLL Input Divider (2x Divider)
#pragma config FPLLRNG  = RANGE_5_10_MHZ    // System PLL Input Range (8-16 MHz Input)
#pragma config FPLLICLK = PLL_POSC          // System PLL Input Clock Selection (POSC is input to the System PLL)
#pragma config FPLLMULT = MUL_50            // 50 System PLL Multiplier (PLL Multiply by 33)
#pragma config FPLLODIV = DIV_2             // System PLL Output Divider
#pragma config UPLLFSEL = FREQ_24MHZ        // USB PLL Input Frequency Selection (USB PLL input is 12 MHz)
#pragma config UPLLEN   = ON                // USB PLL Enable (USB PLL is enabled)
#pragma config FNOSC    = SPLL              // Oscillator Selection Bits (SPLL)
#pragma config DMTINTV  = WIN_127_128       // DMT Count Window Interval (Window/Interval value is 127/128 counter value)
#pragma config FSOSCEN  = OFF               // Secondary Oscillator Enable (Disable SOSC)
#pragma config IESO     = OFF               // Internal/External Switch Over (Disabled)
#pragma config POSCMOD  = EC                // Primary Oscillator Configuration (Primary osc enabled)
#pragma config OSCIOFNC = OFF               // CLKO Output Signal Active on the OSCO Pin (Disabled)
#pragma config FCKSM    = CSDCMD            // Clock Switching and Monitor Selection (Clock Switch Enabled, FSCM Enabled)
#pragma config WDTPS    = PS1048576         // Watchdog Timer Postscaler (1:1048576)
#pragma config WDTSPGM  = STOP              // Watchdog Timer Stop During Flash Programming (WDT stops during Flash programming)
#pragma config WINDIS   = NORMAL            // Watchdog Timer Window Mode (Watchdog Timer is in non-Window mode)
#pragma config FWDTEN   = OFF               // Watchdog Timer Enable (WDT Disabled)
#pragma config FWDTWINSZ = WINSZ_25         // Watchdog Timer Window Size (Window size is 25%)
#pragma config FDMTEN   = OFF               // Deadman Timer Enable (Deadman Timer is disabled)
#pragma config EJTAGBEN = NORMAL
#pragma config DBGPER   = PG_ALL
#pragma config FSLEEP   = OFF
#pragma config FECCCON  = OFF_UNLOCKED
#pragma config BOOTISA  = MIPS32
#pragma config TRCEN    = OFF
#pragma config ICESEL   = ICS_PGx2
#pragma config DEBUG    = ON
#pragma config CP       = OFF               // Code Protect (Protection Disabled)

/*******************************
 * System Objects
 ******************************/

SYSTEM_OBJECTS sysObjects;

/*********************************
 * USB Host Layer Initialization 
 *********************************/

USB_HOST_INIT usbHostInitData =
{
    /* System module initialization */
    .moduleInit = {SYS_MODULE_POWER_RUN_FULL},

    /* Identifies peripheral (PLIB-level) ID */
    .usbID = USBHS_ID_0,

    /* Identifies the Idle mode behavior. If true the USB module will stop when
       the microcontroller enter IDLE mode */
    .stopInIdle = false ,

    /* If true, the USB module will automatically suspend when the the
       microcontroller enter sleep mode */
    .suspendInSleep = false ,

    .endpointTable = NULL,

    /* Interrupt Source for USB module */
    .interruptSource = INT_SOURCE_USB_1,

    /* Number of entries in the TPL table */
    .nTPLEntries     = 1,

    /* USB Device Speed */
    .usbSpeed = USB_SPEED_HIGH,

    /* Pointer to the TPL list for this host layer */
    .tplList = (USB_HOST_TARGET_PERIPHERAL_LIST *)(USBTPList)
};

/****************************************************
 * Timer Driver Initialization Data
 ****************************************************/

DRV_TMR_INIT drvTMR1Init =
{
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .tmrId            = TMR_ID_2,
    .clockSource      = TMR_CLOCK_SOURCE_PERIPHERAL_CLOCK,
    .syncMode         = DRV_TMR_SYNC_MODE_SYNCHRONOUS_INTERNAL,
    .timerPeriod      = 0xF42, //For 10MS
    .prescale         = TMR_PRESCALE_VALUE_256,
    .combineTimers    = false,
    .interruptSource  = INT_SOURCE_TIMER_2,
};

/****************************************************
 *  System Timer service Initialization Data
 ****************************************************/
SYS_TMR_INIT sysTmrInit =
{
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .drvIndex = DRV_TMR_INDEX_0,
    .alarmPeriodMs = 10,
};

/*************************************************
 * DEVCON System Service Initialization
 ************************************************/
 SYS_DEVCON_INIT devconInit =
{
    .moduleInit = {0},
};

// ****************************************************************************
// ****************************************************************************
// Section: System Initialization
// ****************************************************************************
// ****************************************************************************
/*******************************************************************************
  Function:
    void SYS_Initialize (void * data)

  Summary:
    Initializes the board, services, drivers, application and other modules

  Description:
    This routine initializes the board, services, drivers, application and other
    modules as configured at build time.  In a bare-metal environment (where no
    OS is supported), this routine should be called almost immediately after
    entering the "main" routine.

  Remarks:
    See sys_module.h for usage information.
*/

void SYS_Initialize ( void* data )
{
    /* Initialize the BSP */
    BSP_Initialize( );

    SYS_DEVCON_Initialize(SYS_DEVCON_INDEX_0, (SYS_MODULE_INIT *)&devconInit);
    SYS_DEVCON_PerformanceConfig(200000000);
    SYS_CLK_Initialize(NULL);

    /* Enable the VBUS switch */
    BSP_VBUSSwitchEnable();

    /* Initialize the USB host layer */
    sysObjects.usbHostObject = USB_HOST_Initialize (USB_HOST_INDEX_0 ,
            ( SYS_MODULE_INIT *)& usbHostInitData );

    /* Timer driver Initialize */
    sysObjects.tmrObject =  DRV_TMR_Initialize( DRV_TMR_INDEX_0 ,
            (SYS_MODULE_INIT *)&drvTMR1Init);

    sysObjects.systmrObject = SYS_TMR_Initialize(SYS_TMR_INDEX_0,
            (SYS_MODULE_INIT *)&sysTmrInit);

    /* Initialize the Application */
    APP_Initialize ();

    /* Initializethe interrupt system  */
    SYS_INT_Initialize();

    /* Set the interrupt vector priority for USB and Timer 2 interrupt */
    SYS_INT_VectorPrioritySet(INT_VECTOR_USB, INT_PRIORITY_LEVEL3);
    SYS_INT_VectorSubprioritySet(INT_VECTOR_USB, INT_SUBPRIORITY_LEVEL3);
    SYS_INT_VectorPrioritySet(INT_VECTOR_T2, INT_PRIORITY_LEVEL2 );
    SYS_INT_VectorSubprioritySet(INT_VECTOR_T2, INT_SUBPRIORITY_LEVEL0);

    /* Enable the global interrupts */
    SYS_INT_Enable();
}

/*******************************************************************************
 End of File
*/
