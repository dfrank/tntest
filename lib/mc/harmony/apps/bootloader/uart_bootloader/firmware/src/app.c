/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "bsp_config.h"
#include "bootloader.h"
#include "datastream.h"
#include "app.h"
#include "system/system.h"
#include "peripheral/nvm/plib_nvm.h"
#include <sys/kmem.h>
#include <GenericTypeDefs.h>


// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

APP_DATA appData __attribute__((coherent));

unsigned char nvmBufferData[NVM_ROW_SIZE] __attribute__((coherent));

static const UINT8 BootInfo[2] =
{
    MAJOR_VERSION,
    MINOR_VERSION
};

#define DATA_RECORD         0
#define END_OF_FILE_RECORD  1
#define EXT_SEG_ADRS_RECORD 2
#define EXT_LIN_ADRS_RECORD 4

typedef struct
{
    UINT8 RecDataLen;
    DWORD_VAL Address;
    UINT8 RecType;
    UINT8* Data;
    UINT8 CheckSum;
    DWORD_VAL ExtSegAddress;
    DWORD_VAL ExtLinAddress;
}T_HEX_RECORD;


// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback funtions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary local functions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    /* Place the App state machine in it's initial state. */
    appData.currentState = APP_INIT;
    appData.cmdBufferLength = 0;
    appData.streamHandle = DRV_HANDLE_INVALID;
    appData.datastreamBufferHandle = DRV_HANDLE_INVALID;
    appData.datastreamStatus = DRV_CLIENT_STATUS_ERROR;
    appData.usrBufferEventComplete = false;
}

void APP_BufferEventHandler(DATASTREAM_BUFFER_EVENT buffEvent,
                            DATASTREAM_BUFFER_HANDLE hBufferEvent,
                            uintptr_t context )
{
    static bool Escape = false;
    WORD_VAL crc;

    switch(buffEvent)
    {
        /* Buffer event is completed successfully */
        case DATASTREAM_BUFFER_EVENT_COMPLETE:
        {
            if (context == APP_USR_CONTEXT)
            {
                /* Check previous state to know what to check */
                if (APP_GET_COMMAND == appData.prevState)
                {
                    // If we were in an Escape sequence, copy the data on and reset the flag.
                    if (Escape)
                    {
                        appData.cmdBuffer[appData.cmdBufferLength++] = appData.buffer[0];
                        Escape = false;
                    }
                    else
                    {
                        switch (appData.buffer[0])
                        {
                            case SOH:   // Start of header
                                appData.cmdBufferLength = 0;
                                break;

                            case EOT:   // End of transmission
                                // Calculate CRC and see if this is valid
                                if (appData.cmdBufferLength > 2)
                                {
                                    crc.byte.LB = appData.cmdBuffer[appData.cmdBufferLength-2];
                                    crc.byte.HB = appData.cmdBuffer[appData.cmdBufferLength-1];
                                    if (APP_CalculateCrc(appData.cmdBuffer, appData.cmdBufferLength-2) == crc.Val)
                                    {
                                        // CRC matches so frame is valid.
                                        appData.usrBufferEventComplete = true;
                                    }
                                }
                                break;

                            case DLE:   // Escape sequence
                                Escape = true;
                                break;

                            default:
                                appData.cmdBuffer[appData.cmdBufferLength++] = appData.buffer[0];
                                break;
                        }
                    }
                    if (!appData.usrBufferEventComplete)
                    {
                        /* We don't have a complete command yet. Continue reading. */
                        DATASTREAM_Data_Read( appData.streamHandle,
                                &(appData.datastreamBufferHandle),
                                appData.buffer, appData.bufferSize);
                    }
                }
                else    /* APP_SEND_RESPONSE */
                {
                    appData.usrBufferEventComplete = true;
                }
            }
            break;
        }

        /* Buffer event has some error */
        case DATASTREAM_BUFFER_EVENT_ERROR:
            break;
    }
}

void APP_NVMOperation(UINT nvmop)
{
    // Disable flash write/erase operations
    PLIB_NVM_MemoryModifyInhibit(NVM_ID_0);

    PLIB_NVM_MemoryOperationSelect(NVM_ID_0, nvmop);

    // Allow memory modifications
    PLIB_NVM_MemoryModifyEnable(NVM_ID_0);

    /* Unlock the Flash */
    PLIB_NVM_FlashWriteKeySequence(NVM_ID_0, 0);
    PLIB_NVM_FlashWriteKeySequence(NVM_ID_0, NVM_PROGRAM_UNLOCK_KEY1);
    PLIB_NVM_FlashWriteKeySequence(NVM_ID_0, NVM_PROGRAM_UNLOCK_KEY2);

    PLIB_NVM_FlashWriteStart(NVM_ID_0);
}

void APP_FlashErase( void )
{
#if (USE_PAGE_ERASE)
    unsigned int flashAddr = APP_FLASH_BASE_ADDRESS;

    while (flashAddr < APP_FLASH_END_ADDRESS)
    {
        while (PLIB_NVM_FlashWriteCycleHasCompleted(NVM_ID_0));
        PLIB_NVM_FlashAddressToModify(NVM_ID_0, KVA_TO_PA(flashAddr));
        APP_NVMOperation(PAGE_ERASE_OPERATION);
        flashAddr += NVM_PAGE_SIZE;
    }
#else
    APP_NVMOperation(FLASH_ERASE_OPERATION);
#endif
}

void APP_NVMWordWrite(void* address, UINT data)
{
    PLIB_NVM_FlashAddressToModify(NVM_ID_0, KVA_TO_PA((unsigned int)address));

    PLIB_NVM_FlashProvideData(NVM_ID_0, data);

    APP_NVMOperation(WORD_PROGRAM_OPERATION);
}

void APP_NVMRowWrite(void* address, void* data)
{
    PLIB_NVM_FlashAddressToModify(NVM_ID_0, KVA_TO_PA((unsigned int)address));

    PLIB_NVM_DataBlockSourceAddress(NVM_ID_0, KVA_TO_PA((unsigned int)data));

    APP_NVMOperation(ROW_PROGRAM_OPERATION);
}

void APP_NVMQuadWordWrite(void* address, UINT* data)
{
#if (USE_QUAD_WORD_WRITE)
    if (PLIB_NVM_ExistsProvideQuadData(NVM_ID_0))
    {
        PLIB_NVM_FlashAddressToModify(NVM_ID_0, KVA_TO_PA((unsigned int)address));

        PLIB_NVM_FlashProvideQuadData(NVM_ID_0, data);

        APP_NVMOperation(QUAD_WORD_PROGRAM_OPERATION);
    }
#endif
}

void APP_NVMClearError(void)
{
    APP_NVMOperation(NO_OPERATION);
}


void APP_ProgramHexRecord(UINT8* HexRecord, INT totalLen)
{
    static T_HEX_RECORD HexRecordSt;
    UINT8 Checksum = 0;
    UINT i;
    UINT WrData;
    UINT QuadData[4];   /* For Quad-write operations */
    void* ProgAddress;
    UINT nextRecStartPt = 0;

    while(totalLen>=5) // A hex record must be atleast 5 bytes. (1 Data Len byte + 1 rec type byte+ 2 address bytes + 1 crc)
    {
        HexRecord = &HexRecord[nextRecStartPt];
        HexRecordSt.RecDataLen = HexRecord[0];
        HexRecordSt.RecType = HexRecord[3];
        HexRecordSt.Data = &HexRecord[4];

        //Determine next record starting point.
        nextRecStartPt = HexRecordSt.RecDataLen + 5;

        // Decrement total hex record length by length of current record.
        totalLen = totalLen - nextRecStartPt;

        // Hex Record checksum check.
        Checksum = 0;
        for(i = 0; i < HexRecordSt.RecDataLen + 5; i++)
        {
            Checksum += HexRecord[i];
        }

        if(Checksum != 0)
        {
            //Error. Hex record Checksum mismatch.
        }
        else
        {
            // Hex record checksum OK.
            switch(HexRecordSt.RecType)
            {
                case DATA_RECORD:  //Record Type 00, data record.
                    HexRecordSt.Address.byte.MB = 0;
                    HexRecordSt.Address.byte.UB = 0;
                    HexRecordSt.Address.byte.HB = HexRecord[1];
                    HexRecordSt.Address.byte.LB = HexRecord[2];

                    // Derive the address.
                    HexRecordSt.Address.Val = HexRecordSt.Address.Val + HexRecordSt.ExtLinAddress.Val + HexRecordSt.ExtSegAddress.Val;

                    while(HexRecordSt.RecDataLen) // Loop till all bytes are done.
                    {
                        // Convert the Physical address to Virtual address.
                        ProgAddress = PA_TO_KVA0(HexRecordSt.Address.Val);

                        // Make sure we are not writing boot area and device configuration bits.
                        if(((ProgAddress >= (void *)APP_FLASH_BASE_ADDRESS) && (ProgAddress <= (void *)APP_FLASH_END_ADDRESS)))
//                           && ((ProgAddress < (void*)DEV_CONFIG_REG_BASE_ADDRESS) || (ProgAddress > (void*)DEV_CONFIG_REG_END_ADDRESS)))
                        {
                            /* Determine if we can do this with a quad-word write */
                            if (PLIB_NVM_ExistsProvideQuadData(NVM_ID_0) &&
                                    (HexRecordSt.RecDataLen >= 16) &&
                                    (0 == ((unsigned int)ProgAddress & 0xF)))
                            {
                                memcpy(QuadData, HexRecordSt.Data, 16); // Copy 4 words of data in.
                                APP_NVMQuadWordWrite(ProgAddress, QuadData);
                                // Increment the address.
                                HexRecordSt.Address.Val += 16;
                                // Increment the data pointer.
                                HexRecordSt.Data += 16;
                                // Decrement data len.
                                if(HexRecordSt.RecDataLen > 15)
                                {
                                    HexRecordSt.RecDataLen -= 16;
                                }
                                else
                                {
                                    HexRecordSt.RecDataLen = 0;
                                }
                            }
                            else
                            {
                                if(HexRecordSt.RecDataLen < 4)
                                {
                                    // Sometimes record data length will not be in multiples of 4. Appending 0xFF will make sure that..
                                    // we don't write junk data in such cases.
                                    WrData = 0xFFFFFFFF;
                                    memcpy(&WrData, HexRecordSt.Data, HexRecordSt.RecDataLen);
                                }
                                else
                                {
                                    memcpy(&WrData, HexRecordSt.Data, 4);
                                }
                                // Write the data into flash.
                                APP_NVMWordWrite(ProgAddress, WrData);
                                // Increment the address.
                                HexRecordSt.Address.Val += 4;
                                // Increment the data pointer.
                                HexRecordSt.Data += 4;
                                // Decrement data len.
                                if(HexRecordSt.RecDataLen > 3)
                                {
                                    HexRecordSt.RecDataLen -= 4;
                                }
                                else
                                {
                                    HexRecordSt.RecDataLen = 0;
                                }
                            }
                            while (PLIB_NVM_FlashWriteCycleHasCompleted(NVM_ID_0));
                                // Assert on error. This must be caught during debug phase.
    //                            ASSERT(Result==0);
                        }
                        else    // Out of boundaries. Adjust and move on.
                        {
                            // Increment the address.
                            HexRecordSt.Address.Val += 4;
                            // Increment the data pointer.
                            HexRecordSt.Data += 4;
                            // Decrement data len.
                            if(HexRecordSt.RecDataLen > 3)
                            {
                                HexRecordSt.RecDataLen -= 4;
                            }
                            else
                            {
                                HexRecordSt.RecDataLen = 0;
                            }
                        }

                    }
                    break;

                case EXT_SEG_ADRS_RECORD:  // Record Type 02, defines 4th to 19th bits of the data address.
                    HexRecordSt.ExtSegAddress.byte.MB = 0;
                    HexRecordSt.ExtSegAddress.byte.UB = HexRecordSt.Data[0];
                    HexRecordSt.ExtSegAddress.byte.HB = HexRecordSt.Data[1];
                    HexRecordSt.ExtSegAddress.byte.LB = 0;
                    // Reset linear address.
                    HexRecordSt.ExtLinAddress.Val = 0;
                    break;

                case EXT_LIN_ADRS_RECORD:   // Record Type 04, defines 16th to 31st bits of the data address.
                    HexRecordSt.ExtLinAddress.byte.MB = HexRecordSt.Data[0];
                    HexRecordSt.ExtLinAddress.byte.UB = HexRecordSt.Data[1];
                    HexRecordSt.ExtLinAddress.byte.HB = 0;
                    HexRecordSt.ExtLinAddress.byte.LB = 0;
                    // Reset segment address.
                    HexRecordSt.ExtSegAddress.Val = 0;
                    break;

                case END_OF_FILE_RECORD:  //Record Type 01, defines the end of file record.
                default:
                    HexRecordSt.ExtSegAddress.Val = 0;
                    HexRecordSt.ExtLinAddress.Val = 0;
                    break;
            }
        }
    }//while(1)


}

/*******************************************************************************

 */
void APP_ProcessCommand( void )
{
    UINT8 Cmd;
    DWORD_VAL Address;
    DWORD_VAL Length;
    WORD_VAL crc;

    /* First, check that we have a valid command. */
    Cmd = appData.cmdBuffer[0];

    /* Build the response frame from the command. */
    appData.buffer[0] = appData.cmdBuffer[0];
    appData.bufferSize = 0;

    switch (Cmd)
    {
        case READ_BOOT_INFO:
            memcpy(&appData.buffer[1], BootInfo, 2);
            appData.bufferSize = 2 + 1;

            appData.currentState = APP_SEND_RESPONSE;
            break;

        case ERASE_FLASH:
            APP_FlashErase();
            appData.currentState = APP_WAIT_FOR_NVM;
            appData.bufferSize = 1;
            break;

        case PROGRAM_FLASH:
            APP_ProgramHexRecord(&appData.cmdBuffer[1], appData.cmdBufferLength-3);
            appData.bufferSize = 1;
            appData.currentState = APP_SEND_RESPONSE;
            break;

        case READ_CRC:
            memcpy(&Address.v[0], &appData.cmdBuffer[1], sizeof(Address.Val));
            memcpy(&Length.v[0], &appData.cmdBuffer[5], sizeof(Length.Val));
            crc.Val = APP_CalculateCrc((UINT8 *)Address.Val, Length.Val);
            memcpy(&appData.buffer[1], &crc.v[0], 2);

            appData.bufferSize = 1 + 2;
            appData.currentState = APP_SEND_RESPONSE;
            break;

        case JMP_TO_APP:
            appData.currentState = APP_ENTER_APPLICATION;
            break;

        default:
            break;
    }
}

/**
 * Static table used for the table_driven implementation.
 *****************************************************************************/
static const UINT16 crc_table[16] =
{
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
    0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef
};

/********************************************************************
* Function: 	CalculateCrc()
*
* Precondition:
*
* Input: 		Data pointer and data length
*
* Output:		CRC.
*
* Side Effects:	None.
*
* Overview:     Calculates CRC for the given data and len
*
*
* Note:		 	None.
********************************************************************/
UINT16 APP_CalculateCrc(UINT8 *data, UINT32 len)
{
    UINT i;
    UINT16 crc = 0;

    while(len--)
    {
        i = (crc >> 12) ^ (*data >> 4);
	    crc = crc_table[i & 0x0F] ^ (crc << 4);
	    i = (crc >> 12) ^ (*data >> 0);
	    crc = crc_table[i & 0x0F] ^ (crc << 4);
	    data++;
	}

    return (crc & 0xFFFF);
}

/*******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Tasks ( void )
{
    unsigned int BuffLen = 0;
    WORD_VAL crc;
    unsigned int i;
    void (*fptr)(void);

    /* Check the application state*/
    switch ( appData.currentState )
    {
        case APP_INIT:
        {
            appData.currentState = APP_CHECK_FOR_BUTTON;
            break;
        }

        case APP_CHECK_FOR_BUTTON:
        {
            if (BSP_SWITCH_STATE_PRESSED == BSP_SwitchStateGet(BTL_SWITCH))
            {
                appData.currentState = APP_OPEN_DATASTREAM;
            }
            else
            {
                appData.currentState = APP_CHECK_FOR_PROGRAM;
            }
            break;
        }
        
        case APP_CHECK_FOR_PROGRAM:
        {
            /* Check if the User reset address is erased. */
            if (0xFFFFFFFF == *(unsigned int *)USER_APP_RESET_ADDRESS)
            {
                appData.currentState = APP_OPEN_DATASTREAM;
            }
            else
            {
                /* User reset address is not erased. Start program. */
                appData.currentState = APP_ENTER_APPLICATION;
            }
            break;
        }

        case APP_OPEN_DATASTREAM:
        {
            /* open an instance of USART driver */
            appData.streamHandle = DATASTREAM_Open(
                    DRV_IO_INTENT_READWRITE | DRV_IO_INTENT_NONBLOCKING);

            if (appData.streamHandle != DRV_HANDLE_INVALID )
            {
                DATASTREAM_BufferEventHandlerSet(appData.streamHandle,
                        APP_BufferEventHandler, APP_USR_CONTEXT);
                appData.currentState = APP_GET_COMMAND;
            }
            else
            {
                appData.currentState = APP_ERROR;
            }
            break;
        }

        case APP_GET_COMMAND:
        {
            /* Get the datastream driver status */
            appData.datastreamStatus = DATASTREAM_ClientStatus( appData.streamHandle );
            /* Check if client is ready or not */
            if ( appData.datastreamStatus == DRV_CLIENT_STATUS_READY )
            {
                appData.bufferSize = 1;

                DATASTREAM_Data_Read( appData.streamHandle,
                        &(appData.datastreamBufferHandle),
                        appData.buffer, appData.bufferSize);

                if ( appData.datastreamBufferHandle == DRV_HANDLE_INVALID )
                {
                    /* Set the app state to invalid */
                    appData.currentState = APP_ERROR;
                }
                else
                {
                    /* Set the App. state to wait for done */
                    appData.prevState    = APP_GET_COMMAND;
                    appData.currentState = APP_WAIT_FOR_DONE;
                }
            }
            break;
        }

        case APP_WAIT_FOR_DONE:
        {
            /* check if the datastream buffer event is complete or not */
            if (appData.usrBufferEventComplete)
            {
                appData.usrBufferEventComplete = false;
                /* Get the next App. State */
                switch (appData.prevState)
                {
                    case APP_GET_COMMAND:
                        appData.currentState = APP_PROCESS_COMMAND;
                        break;
                    case APP_SEND_RESPONSE:
                    default:
                        appData.currentState = APP_GET_COMMAND;
                        break;
                }
            }
            break;
        }

        case APP_WAIT_FOR_NVM:
        {
            if (!PLIB_NVM_FlashWriteCycleHasCompleted(NVM_ID_0))
            {
                PLIB_NVM_MemoryModifyInhibit(NVM_ID_0);
                appData.currentState = APP_SEND_RESPONSE;
            }
            break;
        }

        case APP_PROCESS_COMMAND:
        {
            APP_ProcessCommand();
            break;
        }

        case APP_SEND_RESPONSE:
        {
            if (appData.bufferSize)
            {
                /* Calculate the CRC of the response*/
                crc.Val = APP_CalculateCrc(appData.buffer, appData.bufferSize);
                appData.buffer[appData.bufferSize++] = crc.byte.LB;
                appData.buffer[appData.bufferSize++] = crc.byte.HB;

                appData.cmdBuffer[BuffLen++] = SOH;

                for (i = 0; i < appData.bufferSize; i++)
                {
                    if ((appData.buffer[i] == EOT) || (appData.buffer[i] == SOH)
                        || (appData.buffer[i] == DLE))
                    {
                        appData.cmdBuffer[BuffLen++] = DLE;
                    }
                    appData.cmdBuffer[BuffLen++] = appData.buffer[i];
                }

                appData.cmdBuffer[BuffLen++] = EOT;
                appData.bufferSize = 0;

                DATASTREAM_Data_Write( appData.streamHandle,
                        &(appData.datastreamBufferHandle),
                        appData.cmdBuffer, BuffLen);
                
                if ( appData.datastreamBufferHandle == DRV_HANDLE_INVALID )
                {
                    appData.currentState = APP_ERROR;
                }
                else
                {
                    appData.prevState = APP_SEND_RESPONSE;
                    appData.currentState = APP_WAIT_FOR_DONE;
                }
            }
            break;
        }

        case APP_ENTER_APPLICATION:
            DATASTREAM_Close();
            fptr = (void (*)(void))USER_APP_RESET_ADDRESS;
            fptr();
            break;

        case APP_ERROR:
            /* The appliction comes here when the demo
             * has failed. Switch on the LED 9.*/
            BSP_LEDStateSet(LED_RED,1);
            break;
            
        default:
            appData.currentState = APP_ERROR;
            break;
    }
    // Blink the LED
    if ((_CP0_GET_COUNT() & 0x2000000) != 0)
    {
        BSP_LEDStateSet(LED_GRN,1);
    }
    else
    {
        BSP_LEDStateSet(LED_GRN,0);
    }
}
 

/*******************************************************************************
 End of File
 */

