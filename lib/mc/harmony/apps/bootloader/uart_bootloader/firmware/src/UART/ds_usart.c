/*******************************************************************************
 Data Stream USART Source File

  File Name:
    ds_usart.c

  Summary:
 Data Stream USART source

  Description:
    This file contains source code necessary for the data stream interface.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END

#include "system/common/sys_module.h"
#include "datastream.h"
#include "peripheral/ports/plib_ports.h"
#include "peripheral/usart/plib_usart.h"
#include "peripheral/dma/plib_dma.h"
#include "system_config.h"
#include "bsp_config.h"
#include "bootloader.h"
#include "system/clk/sys_clk.h"

typedef void DATASTREAM_HandlerType(DATASTREAM_BUFFER_EVENT,
                            DATASTREAM_BUFFER_HANDLE,
                            uintptr_t);
static DATASTREAM_HandlerType* handler = (DATASTREAM_HandlerType*)NULL;
static uintptr_t _context = 0;
static uintptr_t *_bufferHandle = NULL;

#define DMA_RX  DMA_CHANNEL_0
#define DMA_TX  DMA_CHANNEL_1
static int currDMADir = DMA_RX;

void DATASTREAM_Tasks(void)
{
    if (handler == (DATASTREAM_HandlerType*)NULL)
    {
        return;
    }
    /* Check the DMA channels */
    if (PLIB_DMA_ChannelXINTSourceFlagGet( DMA_ID_0, currDMADir, DMA_INT_BLOCK_TRANSFER_COMPLETE))
    {
        PLIB_DMA_ChannelXINTSourceFlagClear( DMA_ID_0, currDMADir, DMA_INT_BLOCK_TRANSFER_COMPLETE);
        handler(DATASTREAM_BUFFER_EVENT_COMPLETE, (DATASTREAM_BUFFER_HANDLE)_bufferHandle, _context);
    }
}

void DATASTREAM_Initialize(void)
{
    PLIB_PORTS_PinModePerPortSelect( PORTS_ID_0, UART_OUT_PORT, UART_OUT_PORT_PIN, PORTS_PIN_MODE_DIGITAL );
    PLIB_PORTS_PinModePerPortSelect( PORTS_ID_0, UART_IN_PORT, UART_IN_PORT_PIN, PORTS_PIN_MODE_DIGITAL );
    PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, UART_OUT_PORT, UART_OUT_PORT_PIN );
    /* Set up serial port lines */
#if (UART_REMAPPABLE_PINS)
    if (PLIB_PORTS_ExistsRemapInput( PORTS_ID_0 ))
    {
        PLIB_PORTS_RemapInput( PORTS_ID_0, UART_IN_FUNC, UART_IN_PIN );
        PLIB_PORTS_RemapOutput( PORTS_ID_0, UART_OUT_FUNC, UART_OUT_PIN );
    }
#endif

    /* Initialize the UART using PLIBs */
    PLIB_USART_BaudRateSet(SYS_USART_ID, USART_PERIPHERAL_BUS, APP_UART_BAUDRATE);
    PLIB_USART_LineControlModeSelect(SYS_USART_ID, USART_8N1);
    PLIB_USART_TransmitterEnable(SYS_USART_ID);
    PLIB_USART_ReceiverEnable(SYS_USART_ID);
    PLIB_USART_Enable(SYS_USART_ID);

    /* Initialize the DMA using PLIBs */
    PLIB_DMA_Enable(DMA_ID_0);
    PLIB_DMA_ChannelXSourceStartAddressSet(DMA_ID_0, DMA_RX, KVA_TO_PA(&UxRXREG));
    PLIB_DMA_ChannelXSourceSizeSet(DMA_ID_0, DMA_RX, 1);
    PLIB_DMA_ChannelXCellSizeSet(DMA_ID_0, DMA_RX, 1);
    PLIB_DMA_ChannelXStartIRQSet(DMA_ID_0, DMA_RX, USART_INTERRUPT_SOURCE_RX);
    PLIB_DMA_ChannelXTriggerEnable(DMA_ID_0, DMA_RX, DMA_CHANNEL_TRIGGER_TRANSFER_START);

    PLIB_DMA_ChannelXDestinationStartAddressSet(DMA_ID_0, DMA_TX, KVA_TO_PA(&UxTXREG));
    PLIB_DMA_ChannelXDestinationSizeSet(DMA_ID_0, DMA_TX, 1);
    PLIB_DMA_ChannelXCellSizeSet(DMA_ID_0, DMA_TX, 1);
    PLIB_DMA_ChannelXStartIRQSet(DMA_ID_0, DMA_TX, USART_INTERRUPT_SOURCE_TX);
    PLIB_DMA_ChannelXTriggerEnable(DMA_ID_0, DMA_TX, DMA_CHANNEL_TRIGGER_TRANSFER_START);
}

DRV_HANDLE DATASTREAM_Open(const DRV_IO_INTENT ioIntent)
{
    return 1;
}

void DATASTREAM_BufferEventHandlerSet
(
    const DRV_HANDLE hClient,
    const void * eventHandler,
    const uintptr_t context
)
{
    handler = (DATASTREAM_HandlerType*)eventHandler;
    _context = context;
}

DRV_CLIENT_STATUS  DATASTREAM_ClientStatus(DRV_HANDLE handle)
{
    return DRV_CLIENT_STATUS_READY;
}

int DATASTREAM_Data_Read(const DRV_HANDLE handle, uintptr_t * const bufferHandle, unsigned char* buffer, const int maxsize)
{
    if (*bufferHandle == DRV_HANDLE_INVALID)
    {
        *bufferHandle = 0;
        _bufferHandle = *bufferHandle;
    }
    PLIB_DMA_ChannelXDestinationStartAddressSet(DMA_ID_0, DMA_RX, KVA_TO_PA(buffer));
    PLIB_DMA_ChannelXDestinationSizeSet(DMA_ID_0, DMA_RX, maxsize);
    PLIB_DMA_ChannelXEnable(DMA_ID_0, DMA_RX);
    currDMADir = DMA_RX;
}

int DATASTREAM_Data_Write(const DRV_HANDLE handle, uintptr_t * const bufferHandle, unsigned char* buffer, const int bufsize)
{
    if (*bufferHandle == DRV_HANDLE_INVALID)
    {
        *bufferHandle = 0;
        _bufferHandle = *bufferHandle;
    }
    PLIB_DMA_ChannelXSourceStartAddressSet(DMA_ID_0, DMA_TX, KVA_TO_PA(buffer));
    PLIB_DMA_ChannelXSourceSizeSet(DMA_ID_0, DMA_TX, bufsize);
    PLIB_DMA_ChannelXEnable(DMA_ID_0, DMA_TX);
    currDMADir = DMA_TX;
}

void DATASTREAM_Close(void)
{
    PLIB_DMA_Disable(DMA_ID_0);
    PLIB_USART_Disable(SYS_USART_ID);
}