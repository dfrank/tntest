/*******************************************************************************
  System Definitions

  File Name:
    bootloader.h

  Summary:
 Bootloader system definitions.

  Description:
    This file contains the definitions needed for a UART bootloader on this
    platform (PIC32MZ_EC_SK)
 *******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
//DOM-IGNORE-END

#ifndef _BOOTLOADER_H
#define _BOOTLOADER_H

// *****************************************************************************
// *****************************************************************************
// Section: Type Definitions
// *****************************************************************************
// *****************************************************************************

/* APP_FLASH_BASE_ADDRESS and APP_FLASH_END_ADDRESS reserves program Flash for the application*/
/* Rule:
    1)The memory regions kseg0_program_mem, kseg0_boot_mem, exception_mem and
    kseg1_boot_mem of the application linker script must fall with in APP_FLASH_BASE_ADDRESS
    and APP_FLASH_END_ADDRESS

    2)The base address and end address must align on  4K address boundary */

#define APP_FLASH_BASE_ADDRESS 	0x9D006000
#define APP_FLASH_END_ADDRESS   0x9D07FFFF
#define DEV_CONFIG_REG_BASE_ADDRESS 0x9FC02FF0
#define DEV_CONFIG_REG_END_ADDRESS  0x9FC02FFF

/* Address of  the Flash from where the application starts executing */
/* Rule: Set APP_FLASH_BASE_ADDRESS to _RESET_ADDR value of application linker script*/

#define USER_APP_RESET_ADDRESS 	(0x9D006000 + 0x1000)

#define USE_PAGE_ERASE  1
#define USE_QUAD_WORD_WRITE	0

/* Define the size of the application's message buffer. */
#define APP_BUFFER_SIZE         255
#define APP_UART_BAUDRATE       115200
#define APP_NO_OF_BYTES_TO_READ 1
#define APP_ESC_MSG             0x1B
#define APP_UART_ID             2
#define BTL_SWITCH              BSP_SWITCH_3
#define LED_RED                 BSP_LED_1
#define LED_GRN                 BSP_LED_3

// *****************************************************************************
// *****************************************************************************
// Section: System Services Configuration
// *****************************************************************************
// *****************************************************************************

/* Define the index for the driver we'll use. */
#define SYS_USART_DRIVER_INDEX          DRV_USART_INDEX_0

/* Define the hardware (PLIB) index associted with this instance of the driver. */
#define SYS_USART_ID                    USART_ID_2

// *****************************************************************************
// *****************************************************************************
// Section: UART Configuration
// *****************************************************************************
// *****************************************************************************
#define _USART_INT_SOURCE(x,y)        DMA_TRIGGER_USART_##x##y
#define USART_INT_SOURCE_FLAG(x,y)     _USART_INT_SOURCE(x,y)
#define USART_INTERRUPT_SOURCE_TX   USART_INT_SOURCE_FLAG(APP_UART_ID, _TRANSMIT)
#define USART_INTERRUPT_SOURCE_RX   USART_INT_SOURCE_FLAG(APP_UART_ID, _RECEIVE)
#define USART_INTERRUPT_SOURCE_ERR  USART_INT_SOURCE_FLAG(APP_UART_ID, _ERROR)

#define _U(x,y)    U##x##y
#define Ux(x,y)  _U(x,y)
#define UxRXREG Ux(APP_UART_ID, RXREG)
#define UxTXREG Ux(APP_UART_ID, TXREG)

#define USART_PERIPHERAL_BUS        80000000

/* PPS Pin configuration */
#define UART_REMAPPABLE_PINS    0
#define UART_OUT_ANALOG     (PORTS_ANALOG_PIN_9)
#define UART_IN_ANALOG      (PORTS_ANALOG_PIN_12)
#define UART_OUT_FUNC       0
#define UART_OUT_PIN        0
#define UART_OUT_PORT       (PORT_CHANNEL_F)
#define UART_OUT_PORT_PIN   (PORTS_BIT_POS_5)
#define UART_IN_FUNC        0
#define UART_IN_PORT        (PORT_CHANNEL_F)
#define UART_IN_PIN         0
#define UART_IN_PORT_PIN    (PORTS_BIT_POS_4)


// *****************************************************************************
// *****************************************************************************
// Section: NVM Driver Configuration
// *****************************************************************************
// *****************************************************************************

/* NVM Driver Flash Memory row size
 * in bytes */
#define NVM_ROW_SIZE                512

/* NVM Driver Flash Memory page size
 * in bytes */
#define NVM_PAGE_SIZE               4096

/*NVM Base Address*/
#define     NVM_BASE_ADDRESS            0x9D006000

/* NVM Flash Memory programming
 * key 1*/

#define NVM_PROGRAM_UNLOCK_KEY1     0xAA996655

/* NVM Driver Flash Memory programming
 * key 1*/

#define NVM_PROGRAM_UNLOCK_KEY2     0x556699AA

#endif // _BOOTLOADER_H
/*******************************************************************************
 End of File
*/
