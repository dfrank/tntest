/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"
#include <sys/kmem.h>

void Camera_I2C_Init(void);
extern volatile GFX_COLOR GraphicsFrame[1][(272)][(480)];

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

APP_DATA appData;


// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback funtions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary local functions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    appData.state = APP_STATE_INIT;

    /* Switch SYSCLK */
    SYSKEY = 0x00000000;
    SYSKEY = 0xAA996655;
    SYSKEY = 0x556699AA;
    RPB15Rbits.RPB15R = 0x0b; //OC5
    INT2Rbits.INT2R = 4;
    SYSKEY = 0x33333333;

    ANSELA = 0;
    ANSELB = 0;
    ANSELC = 0;
    ANSELD = 0;
    ANSELE = 0;
    ANSELF = 0;
    ANSELG = 0;
    ANSELH = 0;
    ANSELJ = 0;
    ANSELK = 0;

    //Turn on Camera Clock source
    OC5CON = 0;
    OC5R = 0x0003;
    OC5RS = 0x0003;
    OC5CON = 0x0006;
    PR2 = 0x0006;
    T2CONSET = 0x8000;
    OC5CONSET = 0x8000;

    Camera_I2C_Init();
    
    /* TODO: Initialize your application's state machine and other
     * parameters.
     */
}

void Camera_Init(void)
{
   LATJbits.LATJ7 = 0;

   CNENAbits.CNIEA1 = 1;
   TRISAbits.TRISA1 = 1; //HSYNC IO Pin

   CNENJbits.CNIEJ2 = 1; //VSYNC
   TRISJbits.TRISJ2 = 1; //VSYNC IO Pin
   CNCONJbits.ON = 1;
   CNCONAbits.ON = 1;

   IFS3bits.CNJIF = 0; //Clear intterupt flag

   IPC31bits.CNJIP = 6;
   IPC29bits.CNAIP = 7;

   IEC3bits.CNJIE = 1;
   IEC3bits.CNAIE = 1;

   // set the transfer event control: what event is to start the DMA transfer
   INTCONbits.INT2EP = 1; // rising edge trigger
   IEC0bits.INT2IE = 1;
   TRISFbits.TRISF5 = 1; //Input for CLK

   TRISK = 0xff; //Input for portk

   DCH0CONbits.CHPRI = 0b11;  //Camera DMA channel has highest priority
   DCH0ECONbits.CHSIRQ = _EXTERNAL_2_VECTOR;
   DCH0ECONbits.SIRQEN = 1;
   DCH0DSA = KVA_TO_PA(&GraphicsFrame[0][0][0]);
   DCH0SSA = KVA_TO_PA((void*)&PORTK);
   DCH0SSIZ = 1;
   DCH0DSIZ = (480<<1);
   DCH0CSIZ = 1;

   EBISMT0 = 0x00001300;

}

void Camera_I2C_Init(void)
{
    static uint8_t buffer[9];

    buffer[2] = 0x46;
    buffer[3] = 0x44;
    buffer[4] = 0x00;
    buffer[5] = 0x00;
    buffer[6] = 0x00;
    buffer[7] = 0x03;
    buffer[8] = 0x56;

    TRISJbits.TRISJ7 = 0;
    LATJbits.LATJ7 = 0;

    PLIB_I2C_Disable(2);
    I2C3BRG = 175; // 300 kHz Baudrate
    PLIB_I2C_Enable(2);

    while(I2C_WriteBlock(2, 0x42, 0x0c, &buffer[0], 8) == false);  //Slow down pixel clock

}


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Tasks ( void )
{
    /* Check the application's current state. */
    switch ( appData.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
        {
            Camera_Init();
            appData.state = 4;
            break;
        }

        /* TODO: implement your application state machine.*/

        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}
 

/*******************************************************************************
 End of File
 */

