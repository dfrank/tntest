/*******************************************************************************
  System Initialization File

  File Name:
    system_init.c

  Summary:
    This file contains source code necessary to initialize the system.

  Description:
    This file contains source code necessary to initialize the system.  It
    implements the "SYS_Initialize" function, configuration bits, and allocates
    any necessary global system resources, such as the systemObjects structure
    that contains the object handles to all the MPLAB Harmony module objects in
    the system.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "system_config.h"
#include "app.h"
#include "system_definitions.h"


// ****************************************************************************
// ****************************************************************************
// Section: Configuration Bits
// ****************************************************************************
// ****************************************************************************

/*** DEVCFG0 ***/

#pragma config DEBUG =      ON
#pragma config JTAGEN =     OFF
#pragma config ICESEL =     ICS_PGx2
#pragma config PWP =        0xff
#pragma config BWP =        OFF
#pragma config CP =         OFF

/*** DEVCFG1 ***/

#pragma config FNOSC =      PRIPLL
#pragma config FSOSCEN =    ON
#pragma config IESO =       ON
#pragma config POSCMOD =    HS
#pragma config OSCIOFNC =   ON
#pragma config FPBDIV =     DIV_1
#pragma config FCKSM =      CSECME
#pragma config WDTPS =      PS1
#pragma config FWDTEN =     OFF
#pragma config WINDIS =     OFF
#pragma config FWDTWINSZ =  WINSZ_50

/*** DEVCFG2 ***/

#pragma config FPLLIDIV =   DIV_2
#pragma config FPLLMUL =    MUL_20
#pragma config FPLLODIV =   DIV_1
#pragma config UPLLIDIV =   DIV_1
#pragma config UPLLEN =     OFF

/*** DEVCFG3 ***/

#pragma config USERID =     0xffff
#pragma config FSRSSEL =    PRIORITY_7
#pragma config PMDL1WAY =   OFF
#pragma config IOL1WAY =    OFF
#pragma config FUSBIDIO =   OFF
#pragma config FVBUSONIO =  OFF


// *****************************************************************************
// *****************************************************************************
// Section: Library/Stack Initialization Data
// *****************************************************************************
// *****************************************************************************/



// *****************************************************************************
// *****************************************************************************
// Section: Driver Initialization Data
// *****************************************************************************
// *****************************************************************************

    DRV_PMP_INIT     pmpInit =
{
    .polarity.addressLatchPolarity = PMP_POLARITY_ACTIVE_HIGH,
    .polarity.rwStrobePolarity = PMP_POLARITY_ACTIVE_LOW,
    .polarity.writeEnableStrobePolarity = PMP_POLARITY_ACTIVE_LOW,
    .polarity.chipselect1Polarity = PMP_POLARITY_ACTIVE_HIGH,
    .polarity.chipselect2Polarity = PMP_POLARITY_ACTIVE_LOW,
    .ports.readWriteStrobe = PORT_ENABLE,
    .ports.writeEnableStrobe = PORT_ENABLE,
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .pmpID            = PMP_ID_0,
    .stopInIdle       = false,
    .muxMode          = PMP_MUX_NONE
};

/*** GFX Initialization Data ***/

GFX_INIT gfxInit0 =
{
    .drvInitialize    = DRV_GFX_OTM2201A_Initialize,
    .drvOpen          = DRV_GFX_OTM2201A_Open,
    .drvInterfaceSet  = DRV_GFX_OTM2201A_InterfaceSet,
    .driverInitData.orientation             = DISP_ORIENTATION,
    .driverInitData.horizontalResolution    = DISP_HOR_RESOLUTION,
    .driverInitData.verticalResolution      = DISP_VER_RESOLUTION,
    .driverInitData.dataWidth               = DISP_DATA_WIDTH,
    .driverInitData.horizontalPulseWidth    = DISP_HOR_PULSE_WIDTH,
    .driverInitData.horizontalBackPorch     = DISP_HOR_BACK_PORCH,
    .driverInitData.horizontalFrontPorch    = DISP_HOR_FRONT_PORCH,
    .driverInitData.verticalPulseWidth      = DISP_VER_PULSE_WIDTH,
    .driverInitData.verticalBackPorch       = DISP_VER_BACK_PORCH,
    .driverInitData.verticalFrontPorch      = DISP_VER_FRONT_PORCH,
    .driverInitData.logicShift              = DISP_INV_LSHIFT,
    .driverInitData.LCDType                 = 1,
    .driverInitData.colorType               = 0,
    .driverInitData.TCON_Init               = TCON_MODULE,
};

// *****************************************************************************
// *****************************************************************************
// Section: System Data
// *****************************************************************************
// *****************************************************************************

/* Structure to hold the object handles for the modules in the system. */
SYSTEM_OBJECTS sysObj;

// *****************************************************************************
// *****************************************************************************
// Section: Module Initialization Data
// *****************************************************************************
// *****************************************************************************


/*** System Device Control Initialization Data ***/

const SYS_DEVCON_INIT sysDevconInit =
{
    .moduleInit = {0},
};


// *****************************************************************************
// *****************************************************************************
// Section: Static Initialization Functions
// *****************************************************************************
// *****************************************************************************


// *****************************************************************************
// *****************************************************************************
// Section: System Initialization
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void SYS_Initialize ( SYS_INIT_DATA *data )

  Summary:
    Initializes the board, services, drivers, application and other modules.

  Remarks:
    See prototype in system/common/sys_module.h.
 */

void SYS_Initialize ( void* data )
{
    /* Core Processor Initialization */
    sysObj.sysDevcon = SYS_DEVCON_Initialize(SYS_DEVCON_INDEX_0, NULL);
    SYS_DEVCON_PerformanceConfig(80000000);

    /* Board Support Package Initialization */
    BSP_Initialize();

    /* System Services Initialization */    
    SYS_INT_Initialize();  

    /* Initialize Drivers */
    DRV_PMP_Initialize (DRV_PMP_INDEX_0, (SYS_MODULE_INIT *)&pmpInit);
    
    /* Initialize System Services */

    /* Initialize Middleware */

    sysObj.gfxObject0 = GFX_Initialize(GFX_INDEX_0, (SYS_MODULE_INIT *)&gfxInit0);



    /* Enable Global Interrupts */
    SYS_INT_Enable();

    /* Initialize the Application */
    APP_Initialize();
}

/*******************************************************************************
 End of File
*/

