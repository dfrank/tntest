/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"


// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

APP_DATA appData;

// *****************************************************************************
// *****************************************************************************
// Section: MACROS
// *****************************************************************************
// *****************************************************************************
#define MIN(x,y)                ((x > y)? y: x)
#define DEMODELAY 400000
#define ID_SURFACE1              10
#define ID_SURFACE2              11
#define APP_WaitUntilFinished(x)    while(!x)
#define APP_SCREEN_DELAY_MS         (1000)
// *****************************************************************************
// *****************************************************************************
// Section: Global Variable Definitions
// *****************************************************************************
// *****************************************************************************
static short                width, height;
static short                counter;
static uint32_t             timeCount = 0;
static short                renderCount = 0;
static GFX_INDEX            gfxIndex=0;
static GFX_GOL_OBJ_SCHEME * surfaceScheme; // style scheme the surface
static GFX_GOL_SURFACE *    pS1;
static GFX_GOL_SURFACE *    pS2;

const int16_t polyPoints[] =
{
    (GFX_MaxXGet()+1)/2,    (GFX_MaxYGet()+1)/4,
    (GFX_MaxXGet()+1)*3/4,  (GFX_MaxYGet()+1)/2,
    (GFX_MaxXGet()+1)/2,    (GFX_MaxYGet()+1)*3/4,
    (GFX_MaxXGet()+1)/4,    (GFX_MaxYGet()+1)/2,
    (GFX_MaxXGet()+1)/2,    (GFX_MaxYGet()+1)/4,
};


// *****************************************************************************
// *****************************************************************************
// Section: Application Local Routines
// *****************************************************************************
void APP_CalculatePaletteColor(GFX_COLOR *red, GFX_COLOR *green, GFX_COLOR *blue, uint16_t pos);
void APP_AlphaFillDrawFunction(uint16_t loop);
void APP_DrawLinesAndCircleScreen(void);
void APP_DrawLineTypesScreen(void);
void APP_DrawRoundedRectanglesScreen(GFX_LINE_STYLE lineStyle);
void APP_DrawRectangleScreen(void);
void APP_DrawPolygonScreen(void);// *****************************************************************************
void InitializeSchemes();
void APP_TMR_DelayMS ( unsigned int delayMs );
void APP_PrimitiveDraw ( void );
GFX_STATUS APP_PrimitiveSurface1 (void);
GFX_STATUS APP_PrimitiveSurface2 (void);
bool APP_DrawCallback ();
bool APP_MessageCallback (
                              GFX_GOL_TRANSLATED_ACTION translatedMsg,
                              GFX_GOL_OBJ_HEADER *pObj,
                              GFX_GOL_MESSAGE * pMsg);


const GFX_GOL_OBJ_SCHEME GOLSchemeDefault =
{
    .EmbossDkColor          = GFX_RGBConvert(0x2B, 0x55, 0x87), // Emboss dark color used for 3d effect.
    .EmbossLtColor          = GFX_RGBConvert(0xD4, 0xE4, 0xF7), // Emboss light color used for 3d effect.
    .TextColor0             = GFX_RGBConvert(0x07, 0x1E, 0x48), // Character color 0 used for objects that supports text.
    .TextColor1             = GFX_RGBConvert(0xFF, 0xFF, 0xFF), // Character color 1 used for objects that supports text.
    .TextColorDisabled      = GFX_RGBConvert( 245,  245,  220), // Character color used when object is in a disabled state.
    .Color0                 = GFX_RGBConvert(0xA9, 0xDB, 0xEF), // Color 0 usually assigned to an Object state.
    .Color1                 = GFX_RGBConvert(0x26, 0xC7, 0xF2), // Color 1 usually assigned to an Object state.
    .ColorDisabled          = GFX_RGBConvert(0xB6, 0xD2, 0xFB), // Color used when an Object is in a disabled state.
    .CommonBkColor          = GFX_RGBConvert(0xD4, 0xED, 0xF7), // Background color used to hide Objects.

    .CommonBkLeft           = 0,                            // Horizontal starting position of the background.
    .CommonBkTop            = 0,                            // Horizontal starting position of the background.
    .CommonBkType           = GFX_BACKGROUND_COLOR,         // Background type color since widgets uses the
                                                            // common background color as default hide setting.
    .pCommonBkImage         = NULL,                         // pointer to the background image used

    .pFont                  = (GFX_RESOURCE_HDR*)&Font25,              // Font for the scheme.

    .fillStyle              = GFX_FILL_STYLE_COLOR,         // fill style

#ifndef GFX_CONFIG_ALPHABLEND_DISABLE
    .AlphaValue             = 100,                          // Alpha value used (percent based)
#endif

#ifndef GFX_CONFIG_GRADIENT_DISABLE
#ifndef GFX_CONFIG_PALETTE_DISABLE
    #error "Gradient feature is not currently supported when palette is enabled. Declare the macro GFX_CONFIG_PALETTE_DISABLE to disable the palette mode"
#endif

    .gradientStartColor = GFX_RGBConvert(0xA9, 0xDB, 0xEF), // Starting color
    .gradientEndColor   = GFX_RGBConvert(0x26, 0xC7, 0xF2), // Ending color

#endif
    .EmbossSize         = 0       // Emboss size of the panel for 3-D effect default to macro defined.

};

#define GFX_SCHEMEDEFAULT GOLSchemeDefault

extern const GFX_GOL_OBJ_SCHEME GFX_SCHEMEDEFAULT;

GFX_GOL_OBJ_SCHEME *APP_SchemeCreate(void)
{
    GFX_GOL_OBJ_SCHEME  *pTemp;

    pTemp = (GFX_GOL_OBJ_SCHEME *)GFX_malloc(sizeof(GFX_GOL_OBJ_SCHEME));

    if(pTemp != NULL)
    {
        memcpy(pTemp, &GFX_SCHEMEDEFAULT, sizeof(GFX_GOL_OBJ_SCHEME));
    }

    return (pTemp);
}

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback funtions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary local functions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    appData.state = APP_STATE_INIT;
    
    /* TODO: Initialize your application's state machine and other
     * parameters.
     */
}


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Tasks ( void )
{
    /* Check the application's current state. */
    switch ( appData.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
        {
            if ( GFX_Status (sysObj.gfxObject0) == SYS_STATUS_READY )
                appData.state = APP_STATE_GFX_INIT;
            break;
        }

        /* TODO: implement your application state machine.*/
        case APP_STATE_GFX_INIT:

            InitializeSchemes();

            pS1 = GFX_GOL_SurfaceCreate(
                GFX_INDEX_0,
                ID_SURFACE1, // ID
                0,
                0,
                GFX_MaxXGet(),
                GFX_MaxYGet(), // dimension
                GFX_GOL_SURFACE_DRAW_STATE, // will be dislayed after creation
                APP_PrimitiveSurface1,
                (GFX_RESOURCE_HDR *) NULL, // icon
                surfaceScheme
             ); // default GOL scheme

            pS2 = GFX_GOL_SurfaceCreate(
                GFX_INDEX_0,
                ID_SURFACE2, // ID
                0,
                0,
                GFX_MaxXGet(),
                GFX_MaxYGet(), // dimension
                GFX_GOL_SURFACE_DRAW_STATE, // will be dislayed after creation
                APP_PrimitiveSurface2,
                (GFX_RESOURCE_HDR *) NULL, // icon
                surfaceScheme
             ); // default GOL scheme

            GFX_GOL_MessageCallbackSet(GFX_INDEX_0, &APP_MessageCallback);
            GFX_GOL_DrawCallbackSet(GFX_INDEX_0, &APP_DrawCallback);

            /* set app state done - surface object callback takes over */
            appData.state = APP_STATE_DONE;

            break;
    }

}

void APP_PrimitiveDraw ( void )
{
    uint16_t polyPoints[] = {
        (GFX_MaxXGet()+1)/2,    (GFX_MaxYGet()+1)/4,
        (GFX_MaxXGet()+1)*3/4,  (GFX_MaxYGet()+1)/2,
        (GFX_MaxXGet()+1)/2,    (GFX_MaxYGet()+1)*3/4,
        (GFX_MaxXGet()+1)/4,    (GFX_MaxYGet()+1)/2,
        (GFX_MaxXGet()+1)/2,    (GFX_MaxYGet()+1)/4,
                                    };
    int x, y;

            GFX_ColorSet(gfxIndex, BLACK);
            while(GFX_ScreenClear(gfxIndex) == GFX_STATUS_FAILURE);

            GFX_ColorSet(gfxIndex, BRIGHTRED);
            if(GFX_LineDraw(gfxIndex, 0,0,GFX_MaxXGet(),0) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, YELLOW);
            if(GFX_LineDraw(gfxIndex, 0,0,0,GFX_MaxYGet()) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, GREEN);
            if(GFX_LineDraw(gfxIndex, 0,GFX_MaxYGet(),GFX_MaxXGet(),GFX_MaxYGet()) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, BLUE);
            if(GFX_LineDraw(gfxIndex, GFX_MaxXGet(),0,GFX_MaxXGet(),GFX_MaxYGet()) == GFX_STATUS_FAILURE)
                return;
            counter = 0;

            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, WHITE);

            while(counter < GFX_MaxXGet())
            {
                if(GFX_LineDraw(gfxIndex, counter, 0, GFX_MaxXGet() - 1 - counter, GFX_MaxYGet() - 1) == GFX_STATUS_FAILURE)
                    return;
                counter += 20;
            }
            GFX_LineDraw(gfxIndex, counter, 0, GFX_MaxXGet() - 1 - counter, GFX_MaxYGet() - 1);

            counter = 10;
            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BRIGHTRED);
            while(counter < MIN(GFX_MaxXGet(), GFX_MaxYGet()) >> 1)
            {
                if(GFX_CircleDraw(gfxIndex, GFX_MaxXGet() >> 1, GFX_MaxYGet() >> 1, counter) == GFX_STATUS_FAILURE)
                    return;
                counter += 10;
            }
            timeCount = 0;
            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BLUE);
            if(GFX_CircleFillDraw(gfxIndex, GFX_MaxXGet() >> 1, GFX_MaxYGet() >> 1, 60) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, GREEN);
            if(GFX_CircleFillDraw(gfxIndex, GFX_MaxXGet() >> 1, GFX_MaxYGet() >> 1, 40) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, BRIGHTRED);
            if(GFX_CircleFillDraw(gfxIndex, GFX_MaxXGet() >> 1, GFX_MaxYGet() >> 1, 20) == GFX_STATUS_FAILURE)
                return;
            timeCount = 0;
            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BLACK);
            if(GFX_ScreenClear(gfxIndex) == GFX_STATUS_FAILURE)
                return;

            // draw concentric beveled objects in the middle of the screen
            GFX_ColorSet(gfxIndex, BLUE);
            if(GFX_RectangleRoundDraw(gfxIndex, (GFX_MaxXGet() >> 1) - 60, (GFX_MaxYGet() >> 1) - 60, (GFX_MaxXGet() >> 1) + 60, (GFX_MaxYGet() >> 1) + 60, 30) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, GREEN);
            if(GFX_RectangleRoundDraw(gfxIndex, (GFX_MaxXGet() >> 1) - 40, (GFX_MaxYGet() >> 1) - 40, (GFX_MaxXGet() >> 1) + 40, (GFX_MaxYGet() >> 1) + 40, 30) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, BRIGHTRED);
            if(GFX_RectangleRoundDraw(gfxIndex, (GFX_MaxXGet() >> 1) - 20, (GFX_MaxYGet() >> 1) - 20, (GFX_MaxXGet() >> 1) + 20, (GFX_MaxYGet() >> 1) + 20, 30) == GFX_STATUS_FAILURE)
                return;

            timeCount = 0;
            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BLACK);
            if(GFX_ScreenClear(gfxIndex) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, BLUE);

            if(GFX_ThickBevelDraw
                    (
                        gfxIndex,
                        (GFX_MaxXGet() >> 1) - 60,
                        (GFX_MaxYGet() >> 1) - 60,
                        (GFX_MaxXGet() >> 1) + 60,
                        (GFX_MaxYGet() >> 1) + 60,
                        20,
                        30,
                        0xFF
                    ) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, GREEN);

            if(GFX_ThickBevelDraw
                    (
                        gfxIndex,
                        (GFX_MaxXGet() >> 1) - 40,
                        (GFX_MaxYGet() >> 1) - 40,
                        (GFX_MaxXGet() >> 1) + 40,
                        (GFX_MaxYGet() >> 1) + 40,
                        20,
                        30,
                        0xFF
                    ) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, BRIGHTRED);

            if(GFX_ThickBevelDraw
                    (
                        gfxIndex,
                        (GFX_MaxXGet() >> 1) - 20,
                        (GFX_MaxYGet() >> 1) - 20,
                        (GFX_MaxXGet() >> 1) + 20,
                        (GFX_MaxYGet() >> 1) + 20,
                        20,
                        30,
                        0xFF
                    ) == GFX_STATUS_FAILURE)
                return;
            timeCount = 0;
            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BLACK);
            if(GFX_ScreenClear(gfxIndex) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, BLUE);

                if(GFX_RectangleRoundFillDraw
                    (
                        gfxIndex,
                        (GFX_MaxXGet() >> 1) - 60,
                        (GFX_MaxYGet() >> 1) - 60,
                        (GFX_MaxXGet() >> 1) + 60,
                        (GFX_MaxYGet() >> 1) + 60,
                        30
                    ) == GFX_STATUS_FAILURE)
                    return;

            GFX_ColorSet(gfxIndex, GREEN);

                if(GFX_RectangleRoundFillDraw
                    (
                        gfxIndex,
                        (GFX_MaxXGet() >> 1) - 40,
                        (GFX_MaxYGet() >> 1) - 40,
                        (GFX_MaxXGet() >> 1) + 40,
                        (GFX_MaxYGet() >> 1) + 40,
                        30
                    ) == GFX_STATUS_FAILURE)
                    return;

            GFX_ColorSet(gfxIndex, BRIGHTRED);

                if(GFX_RectangleRoundFillDraw
                    (
                        gfxIndex,
                        (GFX_MaxXGet() >> 1) - 20,
                        (GFX_MaxYGet() >> 1) - 20,
                        (GFX_MaxXGet() >> 1) + 20,
                        (GFX_MaxYGet() >> 1) + 20,
                        30
                    ) == GFX_STATUS_FAILURE)
                    return;
            timeCount = 0;
            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BLACK);
            if(GFX_ScreenClear(gfxIndex) == GFX_STATUS_FAILURE)
                return;
            APP_TMR_DelayMS(1000);

            // draw concentric thick beveled objects in the middle of the screen
            GFX_ColorSet(gfxIndex, BLUE);
            if(GFX_ThickBevelDraw(gfxIndex, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) - 50, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) + 50, 50, 60, 0x11) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, GREEN);
            if(GFX_ThickBevelDraw(gfxIndex, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) - 50, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) + 50, 50, 60, 0x22) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, BRIGHTRED);
            if(GFX_ThickBevelDraw(gfxIndex, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) - 50, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) + 50, 50, 60, 0x44) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, YELLOW);
            if(GFX_ThickBevelDraw(gfxIndex, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) - 50, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) + 50, 50, 60, 0x88) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, BLUE);
            if(GFX_ThickBevelDraw(gfxIndex, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) - 30, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) + 30, 35, 45, 0x11) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, GREEN);
            if(GFX_ThickBevelDraw(gfxIndex, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) - 30, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) + 30, 35, 45, 0x22) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, BRIGHTRED);
            if(GFX_ThickBevelDraw(gfxIndex, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) - 30, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) + 30, 35, 45, 0x44) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, YELLOW);
            if(GFX_ThickBevelDraw(gfxIndex, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) - 30, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1) + 30, 35, 45, 0x88) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, BLUE);
            if(GFX_ThickBevelDraw(gfxIndex, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1), (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1), 20, 30, 0x11) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, GREEN);
            if(GFX_ThickBevelDraw(gfxIndex, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1), (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1), 20, 30, 0x22) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, BRIGHTRED);
            if(GFX_ThickBevelDraw(gfxIndex, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1), (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1), 20, 30, 0x44) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, YELLOW);
            if(GFX_ThickBevelDraw(gfxIndex, (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1), (GFX_MaxXGet() >> 1), (GFX_MaxYGet() >> 1), 20, 30, 0x88) == GFX_STATUS_FAILURE)
                return;
            counter = 0;
            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BLACK);
            if(GFX_ScreenClear(gfxIndex) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, BLUE);
            while(counter < MIN(GFX_MaxXGet(), GFX_MaxYGet()) >> 1)
            {
                    if(GFX_RectangleDraw
                        (
                            gfxIndex,
                            GFX_MaxXGet() / 2 - counter,
                            GFX_MaxYGet() / 2 - counter,
                            GFX_MaxXGet() / 2 + counter,
                            GFX_MaxYGet() / 2 + counter
                        ) == GFX_STATUS_FAILURE)
                        return;

                    counter += 20;
            }
            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BLUE);
            if(GFX_RectangleFillDraw(gfxIndex, GFX_MaxXGet() / 2 - 80, GFX_MaxYGet() / 2 - 80, GFX_MaxXGet() / 2 + 80, GFX_MaxYGet() / 2 + 80) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, GREEN);
            if(GFX_RectangleFillDraw(gfxIndex, GFX_MaxXGet() / 2 - 60, GFX_MaxYGet() / 2 - 60, GFX_MaxXGet() / 2 + 60, GFX_MaxYGet() / 2 + 60) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, BRIGHTRED);
            if(GFX_RectangleFillDraw(gfxIndex, GFX_MaxXGet() / 2 - 40, GFX_MaxYGet() / 2 - 40, GFX_MaxXGet() / 2 + 40, GFX_MaxYGet() / 2 + 40) == GFX_STATUS_FAILURE)
                return;

            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BLACK);
            if(GFX_ScreenClear(gfxIndex) == GFX_STATUS_FAILURE)
                return;

            // draw ploygon shape in the middle of the screen

            GFX_ColorSet(gfxIndex, WHITE);
            if(GFX_PolygonDraw(gfxIndex, 4, (uint16_t *)polyPoints) == GFX_STATUS_FAILURE)
                return;
            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BLACK);
            if(GFX_ScreenClear(gfxIndex) == GFX_STATUS_FAILURE)
                return;

            GFX_FontSet(gfxIndex, (GFX_RESOURCE_HDR *) &Font25);
            GFX_ColorSet(gfxIndex, GREEN);
            width = GFX_TextStringWidthGet((GFX_XCHAR*)"Microchip Technology Inc.\nRocks!", (GFX_RESOURCE_HDR *) &Font25);
            height = GFX_TextStringHeightGet((GFX_RESOURCE_HDR *) &Font25);
            if(GFX_TextStringBoxDraw(gfxIndex, (GFX_MaxXGet() - width) >> 1, (GFX_MaxYGet() - height) >> 1, width, height*2, (GFX_XCHAR*)"Microchip Technology Inc.\nRocks!",100,GFX_ALIGN_LEFT) == GFX_STATUS_FAILURE)

            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BLACK);
            if(GFX_ScreenClear(gfxIndex) == GFX_STATUS_FAILURE)
                return;

            if(GFX_ImageDraw(gfxIndex, 0, 0, (GFX_RESOURCE_HDR *) &flower1bit) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, WHITE);
            if(GFX_TextStringDraw(gfxIndex, 200, 0, (GFX_XCHAR*)"1BPP",0)== GFX_STATUS_FAILURE)
                return;
            timeCount = 0;
            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BLACK);
            if(GFX_ScreenClear(gfxIndex) == GFX_STATUS_FAILURE)
                return;
            APP_TMR_DelayMS(1000);

            if(GFX_ImageDraw(gfxIndex, 0, 0, (GFX_RESOURCE_HDR *) &flower4bit) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, WHITE);
            if(GFX_TextStringDraw(gfxIndex, 200, 0, (GFX_XCHAR*)"4BPP",0)== GFX_STATUS_FAILURE)
                return;
            timeCount = 0;
            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BLACK);
            if(GFX_ScreenClear(gfxIndex) == GFX_STATUS_FAILURE)
                return;

            if(GFX_ImageDraw(gfxIndex, 0, 0, (GFX_RESOURCE_HDR *) &flower8bit) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, WHITE);
            if(GFX_TextStringDraw(gfxIndex, 200, 0, (GFX_XCHAR*)"8BPP",0) == GFX_STATUS_FAILURE)
                return;
            timeCount = 0;
            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BLACK);
            if(GFX_ScreenClear(gfxIndex) == GFX_STATUS_FAILURE)
                return;

            if(GFX_ImageDraw(gfxIndex, 0, 0, (GFX_RESOURCE_HDR *) &flower16bit) == GFX_STATUS_FAILURE)
                return;

            GFX_ColorSet(gfxIndex, WHITE);
            if(GFX_TextStringDraw(gfxIndex, 200, 0, (GFX_XCHAR*)"16BPP",0) == GFX_STATUS_FAILURE)
                return;
            timeCount = 0;
            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BLACK);
            if(GFX_ScreenClear(gfxIndex) == GFX_STATUS_FAILURE)
                return;

            width = GFX_ImageWidthGet((GFX_RESOURCE_HDR *) &flower1bit);
            height = GFX_ImageHeightGet((GFX_RESOURCE_HDR *) &flower1bit);

            if(GFX_ImageDraw(gfxIndex, (GFX_MaxXGet() + 1) / 2 - width  , (GFX_MaxYGet() + 1) / 2 - height  , (void *) &flower1bit) == GFX_STATUS_FAILURE)
                return;

            if(GFX_ImageDraw(gfxIndex, (GFX_MaxXGet() + 1) / 2          , (GFX_MaxYGet() + 1) / 2 - height  , (void *) &flower4bit)== GFX_STATUS_FAILURE)
                return;

            if(GFX_ImageDraw(gfxIndex, (GFX_MaxXGet() + 1) / 2 - width  , (GFX_MaxYGet() + 1) / 2           , (void *) &flower8bit) == GFX_STATUS_FAILURE)
                return;

            if(GFX_ImageDraw(gfxIndex, (GFX_MaxXGet() + 1) / 2          , (GFX_MaxYGet() + 1) / 2           , (void *) &flower16bit) == GFX_STATUS_FAILURE)
                return;
            timeCount = 0;
            APP_TMR_DelayMS(1000);

            GFX_ColorSet(gfxIndex, BLACK);
            if(GFX_ScreenClear(gfxIndex) == GFX_STATUS_FAILURE)
                return;

            /* RLE Demonstration starts here */
            width = GFX_ImageWidthGet((GFX_RESOURCE_HDR *) &Sun8bit_RLE);
            height = GFX_ImageHeightGet((GFX_RESOURCE_HDR *) &Sun8bit_RLE);

            GFX_ColorSet(gfxIndex, WHITE);
            GFX_FontSet(gfxIndex, (GFX_RESOURCE_HDR *) &Font25);

            if(GFX_ImageDraw(gfxIndex, width + 90, 40, (GFX_RESOURCE_HDR *) &Sun8bit_RLE) == GFX_STATUS_FAILURE)
                return;

            if(GFX_TextStringDraw(gfxIndex, width + 90, height + 60, (GFX_XCHAR*)"RLE 8bit",0) == GFX_STATUS_FAILURE)
                return;

            if(GFX_TextStringDraw(gfxIndex, width + 90, height + 90, (GFX_XCHAR*)"(3997 Bytes)",0) == GFX_STATUS_FAILURE)
                return;

            if(GFX_ImageDraw(gfxIndex, 0, 40, (GFX_RESOURCE_HDR *) &Gaming4bit_RLE) == GFX_STATUS_FAILURE)
                return;

            if(GFX_TextStringDraw(gfxIndex, 0, height + 60, (GFX_XCHAR*)"RLE 4bit",0) == GFX_STATUS_FAILURE)
                return;

            if(GFX_TextStringDraw(gfxIndex, 0, height + 90, (GFX_XCHAR*)"(1378 Bytes)",0) == GFX_STATUS_FAILURE)
                return;

            APP_TMR_DelayMS(1000);

} //End of APP_Tasks


bool APP_MessageCallback(GFX_GOL_TRANSLATED_ACTION objMsg, GFX_GOL_OBJ_HEADER *pObj, GFX_GOL_MESSAGE *pMsg)
{
    return true;
}

/////////////////////////////////////////////////////////////////////////////
// Function: uint16_t GFX_GOL_DrawCallback()
// Output: if the function returns non-zero the draw control will be passed to GOL
// Overview: this function must be implemented by user. GFX_GOL_Draw() function calls it each
//           time when GOL objects drawing is completed. User drawing should be done here.
//           GOL will not change color, line type and clipping region settings while
//           this function returns zero.
/////////////////////////////////////////////////////////////////////////////

bool APP_DrawCallback()
{
    GFX_GOL_ObjectStateSet(pS1, GFX_GOL_SURFACE_DRAW_STATE);
    GFX_GOL_ObjectStateSet(pS2, GFX_GOL_SURFACE_DRAW_STATE);
    return true;
}

GFX_STATUS APP_PrimitiveSurface1 (void)
{
    APP_PrimitiveDraw();
    return GFX_STATUS_SUCCESS;
}

GFX_STATUS APP_PrimitiveSurface2 (void)
{
    uint16_t    counter, trueXPos, z;
    uint16_t    centerX, centerY, radius;
    GFX_COLOR   red = 0, blue = 0, green = 0;
    GFX_COLOR line[480];
    int i;

    /* DrawLinesAndCircleScreen */
    APP_TMR_DelayMS(400);
    APP_DrawLinesAndCircleScreen();

    /* DrawLineTypesScreen */

    APP_TMR_DelayMS(400);
    APP_DrawLineTypesScreen();

    /* DrawRoundedRectanglesScreen */
    APP_DrawRoundedRectanglesScreen(GFX_LINE_STYLE_THIN_SOLID);

    /* DrawRectangleScreen */
    APP_TMR_DelayMS(400);
    APP_DrawRectangleScreen();

    /* DrawPolygonScreen */
    APP_TMR_DelayMS(400);
    APP_DrawPolygonScreen();


    /* Application random drawings*/
    APP_TMR_DelayMS(400);
    GFX_ColorSet(gfxIndex,  BLACK);
    GFX_ScreenClear(gfxIndex);

    // draw border lines to show the limits of the
    // left, right, top and bottom pixels of the screen
    // draw the top most horizontal line
    GFX_ColorSet(gfxIndex,  RED);
    APP_WaitUntilFinished(GFX_LineDraw(gfxIndex,  0,0,GFX_MaxXGet(),0));
    // draw the right most vertical line
    GFX_ColorSet(gfxIndex,  YELLOW);
    APP_WaitUntilFinished(GFX_LineDraw(gfxIndex,  GFX_MaxXGet(),0,GFX_MaxXGet(),GFX_MaxYGet()));
    // draw the bottom most horizontal line
    GFX_ColorSet(gfxIndex,  GREEN);
    APP_WaitUntilFinished(GFX_LineDraw(gfxIndex,  0,GFX_MaxYGet(),GFX_MaxXGet(),GFX_MaxYGet()));
    // draw the left most vertical line
    GFX_ColorSet(gfxIndex,  BLUE);
    APP_WaitUntilFinished(GFX_LineDraw(gfxIndex,  0,0,0,GFX_MaxYGet()));

    APP_TMR_DelayMS(400);

    // draw lines across the screen with RGB colors
    for(counter = 0; counter <= GFX_MaxXGet()-2; counter++)
    {
        APP_CalculatePaletteColor(&red, &green, &blue, counter);
        trueXPos = counter + 1;

        GFX_ColorSet(gfxIndex,  GFX_RGBConvert(red, green, blue));
        APP_WaitUntilFinished(GFX_LineDraw(gfxIndex,  trueXPos, 1, trueXPos, GFX_MaxYGet()-1));
    }

    APP_TMR_DelayMS(400);

    GFX_ColorSet(gfxIndex,  BLACK);
    GFX_ScreenClear(gfxIndex);

    // draw circles randomly on the screen
    for(counter = 0; counter <= GFX_MaxXGet(); counter++)
    {
        APP_CalculatePaletteColor(&red, &green, &blue, counter);
        trueXPos = counter + 1;

        for(z = 0; z <= 2; z++)
        {

            GFX_ColorSet(gfxIndex,  GFX_RGBConvert(red, green, blue));
            centerX = rand() % GFX_MaxXGet();
            centerY = rand() % GFX_MaxYGet();
            radius  = rand() & 0x1E;

            if (centerX < radius)
                centerX = radius + 1;
            if (centerX > (GFX_MaxXGet()-radius))
                centerX = (GFX_MaxXGet()-radius)-1;
            if (centerY < radius)
                centerY = radius + 1;
            if (centerY > (GFX_MaxYGet()-radius))
                centerY = (GFX_MaxYGet()-radius)-1;

            APP_WaitUntilFinished(GFX_CircleDraw(gfxIndex,  centerX, centerY, radius));
            APP_TMR_DelayMS(100);                }
    }

    APP_TMR_DelayMS(400);

    // draw filled circles randomly on the screen
    for(counter = 0; counter <= GFX_MaxXGet(); counter++)
    {
        APP_CalculatePaletteColor(&red, &green, &blue, counter);
        trueXPos = counter + 1;

        for(z = 0; z <= 2; z++)
        {

            GFX_ColorSet(gfxIndex,  GFX_RGBConvert(red, green, blue));
            centerX = rand() % GFX_MaxXGet();
            centerY = rand() % GFX_MaxYGet();
            radius  = rand() & 0x1E;

            if (centerX < radius)
                centerX = radius + 1;
            if (centerX > (GFX_MaxXGet()-radius))
                centerX = (GFX_MaxXGet()-radius)-1;
            if (centerY < radius)
                centerY = radius + 1;
            if (centerY > (GFX_MaxYGet()-radius))
                centerY = (GFX_MaxYGet()-radius)-1;

            APP_WaitUntilFinished(GFX_CircleFillDraw(gfxIndex,  centerX, centerY, radius));
            APP_TMR_DelayMS(10);
        }
    }
    return GFX_STATUS_SUCCESS;
}

void APP_TextStringBoxDrawFunction(uint16_t loop)
{
    GFX_XCHAR        TestString[1];// = "This \nis tes88ing\nthe text\n box function!\n3RD Line is a long line and should show the clipping effect!!!!";

    GFX_ALIGNMENT  alignment;
    uint16_t         x, y, width, height, align;
    GFX_RESOURCE_HDR *pFont;
    GFX_COLOR        color;
    uint16_t         testMode;
    GFX_XCHAR        *pString;

    // set testMode to 1 if testing for anti-aliased fonts
    // set testMode to 0 if testing for normal fonts
    testMode = 1;

    if (testMode)
    {
        pFont = (GFX_RESOURCE_HDR*)&Font35_Antialiased;
//        pString =  (GFX_XCHAR *)AntialisedWithNewLines;
    }
    else
    {
        pFont = (GFX_RESOURCE_HDR*)&Font25;
        pString =  TestString;
    }

    GFX_FontSet(gfxIndex, pFont);

    while(loop)
    {
        GFX_ColorSet(gfxIndex, BLACK);
        GFX_ScreenClear(gfxIndex);

        x     = rand()%(GFX_MaxXGet() >> 1);
        y     = rand()%50;
//        width = rand()%150;
//        if (width < 50)
//            width = 50;
//        height = rand()%200;
//        if (height < 20)
//            height = 20;
        color = rand();
//        x = 5;
//        y = 5;

        width = 50;//150;
        height = 50; //200;
        width = 150;
        height =175;


        GFX_ColorSet(gfxIndex, RED);
        APP_WaitUntilFinished(GFX_RectangleDraw(gfxIndex, x, y, x + width - 1, y + height - 1));

        for (align = 0; align < 9; align++)
        {

            GFX_ColorSet(gfxIndex, BLACK);
            APP_WaitUntilFinished(GFX_RectangleFillDraw(gfxIndex, x+1, y+1, x + width - 2, y + height - 2));

//            if ((rand()&0x01) == 1)
//            {
//                GFX_FontAntiAliasSet(gfxIndex, GFX_FONT_ANTIALIAS_TRANSLUCENT);
//                if (GFX_FontAntiAliasGet(gfxIndex) != GFX_FONT_ANTIALIAS_TRANSLUCENT)
//                {
//                    GFX_ColorSet(gfxIndex, BLUE);
//                    APP_WaitUntilFinished(GFX_RectangleFillDraw(gfxIndex, 10, 10, 20, 20));
//                }
//            }
//            else
//            {
//                GFX_FontAntiAliasSet(gfxIndex, GFX_FONT_ANTIALIAS_OPAQUE);
//                if (GFX_FontAntiAliasGet(gfxIndex) != GFX_FONT_ANTIALIAS_OPAQUE)
//                {
//                    GFX_ColorSet(gfxIndex, GREEN);
//                    APP_WaitUntilFinished(GFX_RectangleFillDraw(gfxIndex, 10, 10, 20, 20));
//                }
//            }

            switch (align)
            {
                case 0:
                    alignment = (GFX_ALIGN_LEFT | GFX_ALIGN_TOP);
                    break;
                case 1:
                    alignment = (GFX_ALIGN_HCENTER  | GFX_ALIGN_TOP);
                    break;
                case 2:
                    alignment = (GFX_ALIGN_RIGHT  | GFX_ALIGN_TOP);
                    break;
                case 3:
                    alignment = (GFX_ALIGN_LEFT  | GFX_ALIGN_VCENTER);
                    break;
                case 4:
                    alignment = (GFX_ALIGN_HCENTER | GFX_ALIGN_VCENTER);
                    break;
                case 5:
                    alignment = (GFX_ALIGN_RIGHT | GFX_ALIGN_VCENTER);
                    break;
                case 6:
                    alignment = (GFX_ALIGN_LEFT | GFX_ALIGN_BOTTOM);
                    break;
                case 7:
                    alignment = (GFX_ALIGN_HCENTER | GFX_ALIGN_BOTTOM);
                    break;
                case 8:
                    alignment = (GFX_ALIGN_RIGHT | GFX_ALIGN_BOTTOM);
                default:
                    break;
            }

            GFX_ColorSet(gfxIndex, WHITE);
            APP_WaitUntilFinished(GFX_TextStringBoxDraw(gfxIndex, x, y, width, height,(GFX_XCHAR *)"Microchip.", 9, GFX_ALIGN_LEFT));


            APP_TMR_DelayMS(700);


        }

        loop--;
    }

}

void APP_AlphaFillDrawFunction(uint16_t loop)
{
    uint16_t rad;
    GFX_RESOURCE_HDR *pBgImage;
    uint16_t x, y, alpha, w;
    uint16_t width, height;
    uint16_t left, top, right, bottom;
    GFX_COLOR color;

    rad = 10;
    GFX_LineStyleSet(gfxIndex, GFX_LINE_STYLE_THIN_SOLID);
//    pBgImage = (void*)&leaves;

    GFX_ColorSet(gfxIndex, GREEN);
    GFX_ScreenClear(gfxIndex);

    width = GFX_ImageWidthGet(pBgImage);
    height = GFX_ImageHeightGet(pBgImage);
    x = ( GFX_MaxXGet() - width) >> 1;
    y =  ( GFX_MaxYGet() - height) >> 1;
    GFX_ImageDraw(gfxIndex, x, y, pBgImage);
    w = 0;

    while(loop)
    {
        GFX_FillStyleSet(gfxIndex, GFX_FILL_STYLE_ALPHA_COLOR);

        left = x + 10 + rad;
        top = y + 10 + rad;
        right = (x + width) - 10 - rad;
        bottom = (y + height) - 10 - rad;

        color = rand();
        GFX_ColorSet(gfxIndex, color);

       // show the actual color that overlays the area
        GFX_AlphaBlendingValueSet(gfxIndex, 100);
        while(GFX_RectangleFillDraw(gfxIndex, 10,10,30,30) != GFX_STATUS_SUCCESS);

        if (w == 2)
            w = 0;

        switch(w)
        {
            case 0:
                GFX_BackgroundSet(gfxIndex, x, y, pBgImage, RED);
                GFX_BackgroundTypeSet(gfxIndex, GFX_BACKGROUND_COLOR);
                break;
            case 1:
                GFX_BackgroundSet(gfxIndex, x, y, pBgImage, 0);
                GFX_BackgroundTypeSet(gfxIndex, GFX_BACKGROUND_IMAGE);
                break;
            case 2:
                GFX_BackgroundTypeSet(gfxIndex, GFX_BACKGROUND_DISPLAY_BUFFER);
                break;
            default:
                break;
        }
        w++;

        switch(rand()%4)
        {
            case 0:
                alpha = 0;
                break;
            case 1:
                alpha = 25;
                break;
            case 2:
                alpha = 50;
                break;
            case 3:
                alpha = 75;
                break;
            case 4:
                alpha = 100;
                break;
            default:
                alpha = 50;
                break;
        }

        GFX_AlphaBlendingValueSet(gfxIndex, alpha);

        switch(alpha)
        {
            case 0:
            case 25:
                    APP_WaitUntilFinished(GFX_RectangleFillDraw(gfxIndex,
                                    left, top, right, bottom));
                break;
            case 100:
            case 75:
                    APP_WaitUntilFinished(GFX_RectangleRoundFillDraw(gfxIndex,
                                    left, top, right, bottom, rad));
                break;
            case 50:
#if 1
                    APP_WaitUntilFinished(GFX_CircleFillDraw(gfxIndex,
                                    left + ((right-left+1) >> 1),
                                    top + ((bottom - top + 1) >> 1), (bottom - top) >> 1
                            ));

#else
                for(z = 0; z <= 240>>1;  z++)
                {
                    GFX_ColorSet(color);

#if 0
                    APP_TESTWaitUntilFinished(GFX_CircleFillDraw(
                                    left + ((right-left+1) >> 1),
                                    top + ((bottom - top + 1) >> 1), z
                            ));
#endif
                    APP_TESTWaitUntilFinished(GFX_RectangleRoundFillDraw(
                                    (GFX_MaxXGet() >> 1) - z,
                                    GFX_MaxYGet()>>1,
                                    (GFX_MaxXGet() >> 1) + z,
                                    GFX_MaxYGet()>>1,
                                    15
                                    ));


                    __delay_ms(300);
                    GFX_ColorSet(GFX_X11_GREEN);
                    GFX_ScreenClear();
                    GFX_ImageDraw(x, y, pBgImage);

                }
#endif
                break;
            default:
                break;
        }

        APP_TMR_DelayMS(APP_SCREEN_DELAY_MS>>1);
        GFX_ImageDraw(gfxIndex, x, y, pBgImage);

        loop--;
    }

}

// *****************************************************************************
// void APP_DrawLinesAndCircleScreen(void)
// *****************************************************************************
void APP_DrawLinesAndCircleScreen(void)
{
    uint16_t    counter, trueXPos, z;
    uint16_t    centerX, centerY, radius;
    GFX_COLOR   red = 0, blue = 0, green = 0;

    GFX_ColorSet(gfxIndex,  BLACK);
    GFX_ScreenClear(gfxIndex);

    // draw border lines to show the limits of the
    // left, right, top and bottom pixels of the screen
    // draw the top most horizontal line
    GFX_ColorSet(gfxIndex,  RED);
    APP_WaitUntilFinished(GFX_LineDraw(gfxIndex,  0,0,GFX_MaxXGet(),0));
    // draw the right most vertical line
    GFX_ColorSet(gfxIndex,  YELLOW);
    APP_WaitUntilFinished(GFX_LineDraw(gfxIndex,  GFX_MaxXGet(),0,GFX_MaxXGet(),GFX_MaxYGet()));
        // draw the bottom most horizontal line
    GFX_ColorSet(gfxIndex,  GREEN);
    APP_WaitUntilFinished(GFX_LineDraw(gfxIndex,  0,GFX_MaxYGet(),GFX_MaxXGet(),GFX_MaxYGet()));
    // draw the left most vertical line
    GFX_ColorSet(gfxIndex,  BLUE);
    APP_WaitUntilFinished(GFX_LineDraw(gfxIndex,  0,0,0,GFX_MaxYGet()));

    APP_TMR_DelayMS(APP_SCREEN_DELAY_MS * 2);

    // draw lines across the screen with RGB colors
    for(counter = 0; counter <= GFX_MaxXGet()-2; counter++)
    {
        APP_CalculatePaletteColor(&red, &green, &blue, counter);
        trueXPos = counter + 1;

        GFX_ColorSet(gfxIndex,  GFX_RGBConvert(red, green, blue));
        APP_WaitUntilFinished(GFX_LineDraw(gfxIndex,  trueXPos, 1, trueXPos, GFX_MaxYGet()-1));
    }

    APP_TMR_DelayMS(APP_SCREEN_DELAY_MS * 3);

    GFX_ColorSet(gfxIndex,  BLACK);
    GFX_ScreenClear(gfxIndex);

    // draw circles randomly on the screen
    for(counter = 0; counter <= GFX_MaxXGet(); counter++)
    {
        APP_CalculatePaletteColor(&red, &green, &blue, counter);
        trueXPos = counter + 1;

        for(z = 0; z <= 2; z++)
        {

            GFX_ColorSet(gfxIndex,  GFX_RGBConvert(red, green, blue));
            centerX = rand() % GFX_MaxXGet();
            centerY = rand() % GFX_MaxYGet();
            radius  = rand() & 0x1E;

            if (centerX < radius)
                centerX = radius + 1;
            if (centerX > (GFX_MaxXGet()-radius))
                centerX = (GFX_MaxXGet()-radius)-1;
            if (centerY < radius)
                centerY = radius + 1;
            if (centerY > (GFX_MaxYGet()-radius))
                centerY = (GFX_MaxYGet()-radius)-1;

            APP_WaitUntilFinished(GFX_CircleDraw(gfxIndex,  centerX, centerY, radius));
            APP_TMR_DelayMS(3);
        }
    }

    APP_TMR_DelayMS(APP_SCREEN_DELAY_MS);

//    // draw filled circles randomly on the screen
//    for(counter = 0; counter <= GFX_MaxXGet(); counter++)
//    {
//        APP_CalculatePaletteColor(&red, &green, &blue, counter);
//        trueXPos = counter + 1;
//
//        for(z = 0; z <= 2; z++)
//        {
//
//            GFX_ColorSet(gfxIndex,  GFX_RGBConvert(red, green, blue));
//            centerX = rand() % GFX_MaxXGet();
//            centerY = rand() % GFX_MaxYGet();
//            radius  = rand() & 0x1E;
//
//            if (centerX < radius)
//                centerX = radius + 1;
//            if (centerX > (GFX_MaxXGet()-radius))
//                centerX = (GFX_MaxXGet()-radius)-1;
//            if (centerY < radius)
//                centerY = radius + 1;
//            if (centerY > (GFX_MaxYGet()-radius))
//                centerY = (GFX_MaxYGet()-radius)-1;
//
//            APP_WaitUntilFinished(GFX_CircleFillDraw(gfxIndex,  centerX, centerY, radius));
//            APP_TMR_DelayMS(3);
//        }
//    }

    APP_TMR_DelayMS(APP_SCREEN_DELAY_MS);

}
// *****************************************************************************
// void APP_DrawRoundedRectanglesScreen(GFX_LINE_STYLE lineStyle)
// *****************************************************************************
void APP_DrawRoundedRectanglesScreen(GFX_LINE_STYLE lineStyle)
{
    uint16_t    centerX, centerY;
    uint16_t    rad, counter;
    uint16_t    left, top, right, bottom;
    uint16_t    width, height;
    GFX_COLOR   red, green, blue;

    GFX_ColorSet(gfxIndex,  BLACK);
    GFX_ScreenClear(gfxIndex);

    GFX_LineStyleSet(gfxIndex, lineStyle);

    // draw rounded rectangles shapes randomly on the screen
    for(counter = 0; counter <= 300; counter += 1)
    {
        APP_CalculatePaletteColor(&red, &green, &blue, counter);
        GFX_ColorSet(gfxIndex,  GFX_RGBConvert(red, green, blue));
        centerX = rand() % GFX_MaxXGet();
        centerY = rand() % GFX_MaxYGet();

        width   = rand() & 0x1E;
        height  = rand() & 0x1E;

        rad = 5;

        left   = centerX - (width  >> 1);
        top    = centerY - (height >> 1);
        right  = centerX + (width  >> 1);
        bottom = centerY + (height >> 1);

        // check if the shape goes beyond the screen
        if ((int16_t)(left - rad) <= 0)
        {
            left  = (rad + 1);
            right = left + width + 1;
        }
        if (right + rad > GFX_MaxXGet())
        {
            right = GFX_MaxXGet() - (rad + 1);
            left  = right - width + 1;
        }
        if ((int16_t)(top - rad) <= 0)
        {
            top    = (rad + 1);
            bottom = top + height + 1;
        }
        if (bottom + rad > GFX_MaxYGet())
        {
            bottom = GFX_MaxYGet() - (rad + 1);
            top    = bottom - height + 1;
        }

        APP_WaitUntilFinished
        (
            GFX_RectangleRoundDraw
            (   gfxIndex,
                left, top, right, bottom,
                rad
            )

        );
         APP_TMR_DelayMS(10);
    }

    APP_TMR_DelayMS(APP_SCREEN_DELAY_MS);

    // reset line style
    GFX_LineStyleSet(gfxIndex, GFX_LINE_STYLE_THIN_SOLID);

}

// *****************************************************************************
// void APP_DrawRectangleScreen(void)
// *****************************************************************************
void APP_DrawRectangleScreen(void)
{
    uint16_t    count1, count2, width, height, left, top;
    GFX_COLOR   red, green, blue;
    uint16_t    seed;

    GFX_ColorSet(gfxIndex,  BLACK);
    GFX_ScreenClear(gfxIndex);

    seed = rand();

    // draw rectangles and bars
    for(count1 = 0; count1 < 2; count1++)
    {
        srand(seed);
        for(count2 = 0; count2 < 8000; count2++)
        {

            // generate light colors
            red   = (rand() & 0x7F) + 0x80;
            green = (rand() & 0x7F) + 0x80;
            blue  = (rand() & 0x7F) + 0x80;
            GFX_ColorSet(gfxIndex,  GFX_RGBConvert(red, green, blue));

            // generate the random rectangle
            width   = rand() & 0x1F;
            height  = rand() & 0x1F;
            left    = rand() % GFX_MaxXGet();
            top     = rand() % GFX_MaxYGet();
            if ((left + width) > GFX_MaxXGet())
                width = GFX_MaxXGet() - left;
            if ((top + height) > GFX_MaxYGet())
                height = GFX_MaxYGet() - top;

            if (count1 == 0)
            {
                APP_WaitUntilFinished
                (
                    GFX_RectangleDraw
                        (
                            gfxIndex,
                            left,
                            top,
                            left + width,
                            top + height
                        )
                );
            }
            else
            {
                APP_WaitUntilFinished
                (
                    GFX_BarDraw
                        (
                            gfxIndex,
                            left,
                            top,
                            left + width,
                            top + height
                        )
                );
            }
        }
        APP_TMR_DelayMS(APP_SCREEN_DELAY_MS);
        GFX_ColorSet(gfxIndex,  BLACK);
        GFX_ScreenClear(gfxIndex);
    }

}

// *****************************************************************************
// void APP_DrawLineTypesScreen(void)
// *****************************************************************************
void APP_DrawLineTypesScreen(void)
{
    uint16_t        count1, count2, width, height, left, top;
    GFX_COLOR       red, green, blue;
    GFX_LINE_STYLE  lineStyle = GFX_LINE_STYLE_THIN_SOLID;
    uint16_t        lineSize;

    GFX_ColorSet(gfxIndex,  BLACK);
    GFX_ScreenClear(gfxIndex);

    // draw rectangles and bars
    for(count1 = 0; count1 <= 2; count1++)
    {
        lineSize = ((rand() & 0x1) << 4);

        switch (count1 + lineSize)
        {
            case 0x00 : lineStyle = GFX_LINE_STYLE_THIN_SOLID;  break;
            case 0x01 : lineStyle = GFX_LINE_STYLE_THIN_DOTTED; break;
            case 0x02 : lineStyle = GFX_LINE_STYLE_THIN_DASHED; break;
            case 0x10 : lineStyle = GFX_LINE_STYLE_THICK_SOLID;  break;
            case 0x11 : lineStyle = GFX_LINE_STYLE_THICK_DOTTED; break;
            case 0x12 : lineStyle = GFX_LINE_STYLE_THICK_DASHED; break;
            default: break;
        }

        GFX_LineStyleSet(gfxIndex, lineStyle);

        for(count2 = 0; count2 < 2000; count2++)
        {
            // generate light colors
            red   = (rand() & 0x7F) + 0x80;
            green = (rand() & 0x7F) + 0x80;
            blue  = (rand() & 0x7F) + 0x80;
            GFX_ColorSet(gfxIndex,  GFX_RGBConvert(red, green, blue));

            // generate the random rectangle
            width   = rand() & 0x1F;
            height  = rand() & 0x1F;
            left    = rand() % GFX_MaxXGet();
            top     = rand() % GFX_MaxYGet();
            if ((left + width) > GFX_MaxXGet())
                width = GFX_MaxXGet() - left;
            if ((top + height) > GFX_MaxYGet())
                height = GFX_MaxYGet() - top;

            APP_WaitUntilFinished
            (
                GFX_RectangleDraw
                    (
                        gfxIndex,
                        left,
                        top,
                        left + width,
                        top + height
                    )
            );
            APP_TMR_DelayMS(1);
        }
        APP_TMR_DelayMS(APP_SCREEN_DELAY_MS);
        GFX_ColorSet(gfxIndex,  BLACK);
        GFX_ScreenClear(gfxIndex);
    }
    // reset the line style to default
    GFX_LineStyleSet(gfxIndex, GFX_LINE_STYLE_THIN_SOLID);

}

// *****************************************************************************
// void APP_DrawImagePolygonScreen(void)
// *****************************************************************************
void APP_DrawPolygonScreen(void)
{
    GFX_ColorSet(gfxIndex,  BLACK);
    GFX_ScreenClear(gfxIndex);

    // draw ploygon shape in the middle of the screen
    GFX_LineStyleSet(gfxIndex, GFX_LINE_STYLE_THIN_SOLID);
    GFX_ColorSet(gfxIndex,  WHITE);
    APP_WaitUntilFinished(GFX_PolygonDraw(gfxIndex,  4, (uint16_t *)polyPoints));

    APP_TMR_DelayMS(APP_SCREEN_DELAY_MS * 2);

}

void InitializeSchemes()
{
    // create the alternate schemes
    surfaceScheme = APP_SchemeCreate(); // alternative scheme for the navigate buttons

    /* for Truly display */
    surfaceScheme->Color0 = GFX_RGBConvert(0x4C, 0x8E, 0xFF);
    surfaceScheme->Color1 = GFX_RGBConvert(255, 102, 0);
    surfaceScheme->EmbossDkColor = BLACK;
    surfaceScheme->EmbossLtColor = BLACK;
    surfaceScheme->ColorDisabled = GFX_RGBConvert(0xD4, 0xE1, 0xF7);
    surfaceScheme->TextColor1 = BRIGHTBLUE;
    surfaceScheme->TextColor0 = GFX_RGBConvert(255, 102, 0);
    surfaceScheme->TextColorDisabled = GFX_RGBConvert(0xB8, 0xB9, 0xBC);
    surfaceScheme->CommonBkColor = BLACK;
}

void APP_CalculatePaletteColor(GFX_COLOR *red, GFX_COLOR *green, GFX_COLOR *blue, uint16_t pos)
{
    #define DIVISION        ((GFX_MaxXGet() - 2)/6)
    #define REDMAX          255
    #define GREENMAX        255
    #define BLUEMAX         255


    uint16_t    temp, loc;

    // calculate the effect of x location
    loc = pos / DIVISION;   // gets the location in the palette
    temp = pos % DIVISION;  // calculates the x position in each division
    switch(loc)
    {
        case 0:
            *red = (temp * REDMAX) / DIVISION;                  // red here is increasing as x increases
            *green = 0;
            *blue = BLUEMAX;
            break;

        case 1:
            *red = REDMAX;
            *green = 0;
            *blue = BLUEMAX - ((temp * BLUEMAX) / DIVISION);    // blue here is decreasing as x increases
            break;

        case 2:
            *red = REDMAX;
            *green = (temp * GREENMAX) / DIVISION;              // green here is increasing as x increases
            *blue = 0;
            break;

        case 3:
            *red = REDMAX - ((temp * REDMAX) / DIVISION);       // red here is decreasing as x increases
            *green = GREENMAX;
            *blue = 0;
            break;

        case 4:
            *red = 0;
            *green = GREENMAX;
            *blue = (temp * BLUEMAX) / DIVISION;                // blue here is increasing as x increases
            break;

        case 5:
            *red = 0;
            *green = GREENMAX - ((temp * GREENMAX) / DIVISION); // green here is decreasing as x increases
            *blue = BLUEMAX;
            break;

        default:
            break;
    }
}

void APP_TMR_DelayMS ( unsigned int delayMs )
{
    if(delayMs)
    {
        uint32_t sysClk = SYS_DEVCON_SYSTEM_CLOCK;
        uint32_t t0;
        #if   defined (__C32__)
        t0 = _CP0_GET_COUNT();
        while (_CP0_GET_COUNT() - t0 < (sysClk/2000)*delayMs);
        #elif defined (__C30__)
        t0 = ReadTimer23();
        while (ReadTimer23() -  t0 < (sysClk/2000)*mSec);
        #else
        #error "Neither __C32__ nor __C30__ is defined!"
        #endif
    }
}
/*******************************************************************************
 End of File
 */

