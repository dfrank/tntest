#include "system_config.h"
#include "system_definitions.h"
#include "app.h"


// DEVCFG3
#pragma config FMIIEN   = OFF           // Ethernet MII Enable
#pragma config FETHIO   = ON            // Ethernet I/O Pin Select
#pragma config PGL1WAY  = OFF           // Permission Group Lock One Way Configuration
#pragma config PMDL1WAY = OFF           // Peripheral Module Disable Configuration
#pragma config IOL1WAY  = OFF           // Peripheral Pin Select Configuration
#pragma config FUSBIDIO = OFF           // USB USBID Selection

// DEVCFG2
#pragma config FPLLIDIV = DIV_3          // System PLL Input Divider
#pragma config FPLLRNG  = RANGE_8_16_MHZ // System PLL Input Range
#pragma config FPLLICLK = PLL_POSC       // System PLL Input Clock Selection
#pragma config FPLLMULT = MUL_50         // System PLL Multiplier
#pragma config FPLLODIV = DIV_2          // System PLL Output Divisor
#pragma config UPLLFSEL = FREQ_24MHZ     // USB PLL Input Frequency Selection
#pragma config UPLLEN   = ON             // USB PLL Enable

// DEVCFG1
#pragma config FNOSC    = SPLL          // Oscillator Selection Bits
#pragma config DMTINTV  = WIN_127_128   // DMT Count Window Interval
#pragma config FSOSCEN  = OFF           // Secondary Oscillator Enable
#pragma config IESO     = OFF           // Internal/External Switch Over
#pragma config POSCMOD  = EC            // Primary Oscillator
#pragma config OSCIOFNC = ON            // CLKO Output Signal
#pragma config FCKSM    = CSDCMD        // Clock Switching and Monitor Selection
#pragma config WDTPS    = PS1048576     // Watchdog Timer Postscaler
#pragma config WDTSPGM  = STOP          // Watchdog Timer Stop During Flash Programming
#pragma config WINDIS   = NORMAL        // Watchdog Timer Window Mod
#pragma config FWDTEN   = OFF           // Watchdog Timer Enable
#pragma config FWDTWINSZ= WINSZ_25      // Watchdog Timer Window Siz
#pragma config FDMTEN   = OFF           // Deadman Timer Enable

/* DEVCFG0 */
#pragma config DEBUG    = ON            // Background Debugger
#pragma config JTAGEN   = OFF           // JTAG Port
#pragma config ICESEL   = ICS_PGx2      // ICE/ICD Comm Channel
#pragma config TRCEN    = OFF           // Trace features in CPU
#pragma config BOOTISA  = MIPS32        // MIPS32 or MICROMIPS
#pragma config FECCCON  = OFF_UNLOCKED  // Dynamic Flash ECC
#pragma config FSLEEP   = OFF           // Flash Sleep Mode
#pragma config DBGPER   = PG_ALL        // Allow CPU access to all permission regions
#pragma config EJTAGBEN = NORMAL        // Normal EJTAG functionality

// DEVCP0
#pragma config CP = OFF                 // Code Protect
#pragma config_alt FWDTEN=OFF
#pragma config_alt USERID = 0x1234u

/* TMR Driver Initialization Data */
const DRV_TMR_INIT drvTmrInitData =
{
    {APP_TMR_DRV_POWER_MODE},
    APP_TMR_DRV_HW_ID,
    APP_TMR_DRV_CLOCK_SOURCE,
    APP_TMR_DRV_PRESCALE,
    APP_TMR_DRV_INT_SOURCE,
    APP_TMR_DRV_OPERATION_MODE
};

/* Structure to hold the system objects. */
SYSTEM_OBJECTS sysObj;


/* System Initialization Function */
void SYS_Initialize ( void* data )
{
    /* Initialize services, drivers & libraries */
    SYS_CLK_Initialize(NULL);
    sysObj.sysDevcon = SYS_DEVCON_Initialize(SYS_DEVCON_INDEX_0, NULL);
    SYS_DEVCON_PerformanceConfig(SYS_CLOCK_FREQENCY);
    sysObj.drvTmr    = DRV_TMR_Initialize(APP_TMR_DRV_INDEX, (SYS_MODULE_INIT *)&drvTmrInitData);

    /* Initialize the Application */
    APP_Initialize();
}
