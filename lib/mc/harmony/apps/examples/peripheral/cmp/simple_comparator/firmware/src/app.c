/*******************************************************************************
  MPLAB Harmony Application 
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    Application Template

  Description:
    This file contains the application logic.
 *******************************************************************************/


// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"
#include "peripheral/cmp/plib_cmp.h"
#include "peripheral/int/plib_int.h"
#include "peripheral/ports/plib_ports.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Variable Definitions
// *****************************************************************************
// *****************************************************************************


/*****************************************************
 * Initialize the application data structure. All
 * application related variables are stored in this
 * data structure.
 *****************************************************/

APP_DATA appData = 
{
    //TODO - Initialize appData structure. 

};
// *****************************************************************************
/* Driver objects.

  Summary:
    Contains driver objects.

  Description:
    This structure contains driver objects returned by the driver init routines
    to the application. These objects are passed to the driver tasks routines.
*/


APP_DRV_OBJECTS appDrvObject;

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Routines
// *****************************************************************************
// *****************************************************************************


// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Routines
// *****************************************************************************
// *****************************************************************************



// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine
// *****************************************************************************
// *****************************************************************************

/******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    appData.state = APP_STATE_INIT;
}




/**********************************************************
 * Application tasks routine. This function implements the
 * application state machine.
 ***********************************************************/
void APP_Tasks ( void )
{
    /* check the application state*/
    switch ( appData.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
            /* Configure CVREF for comparator use. */
            PLIB_CMP_CVREF_SourceVoltageSelect(CMP_ID_1, CMP_CVREF_VOLTAGE_SOURCE_VDD);
            PLIB_CMP_CVREF_WideRangeEnable(CMP_ID_1);
            PLIB_CMP_CVREF_ValueSelect(CMP_ID_1, CMP_CVREF_VALUE_15);
            PLIB_CMP_CVREF_Enable(CMP_ID_1);

            /* Use CVREF as the positive comparator input, use C1IN+ pin as negative
               comparator input (C1INA on some devices), output is non-inverted, comparator
               output not driven to C1OUT pin, interrupts generated on all events,
               enable the comparator 1 module */
            PLIB_CMP_NonInvertingInputChannelSelect(CMP_ID_2, CMP_INPUT_INTERNAL_CVREF);
            PLIB_CMP_InvertingInputChannelSelect(CMP_ID_2, CMP_INPUT_C2IN_NEGATIVE);
            PLIB_CMP_OutputInvertDisable(CMP_ID_2);
            PLIB_CMP_OutputDisable(CMP_ID_2);
            PLIB_CMP_InterruptEventSelect(CMP_ID_2, CMP_INTERRUPT_GENERATION_BOTH);
            PLIB_CMP_Enable(CMP_ID_2);

            /* Enable the comparator 1 interrupt source, set its priority level to 2,
               set its subpriority level to 0 */
            PLIB_INT_SourceEnable(INT_ID_0, INT_SOURCE_COMPARATOR_2);
            PLIB_INT_VectorPrioritySet(INT_ID_0, INT_VECTOR_CMP2, INT_PRIORITY_LEVEL2);
            PLIB_INT_VectorSubPrioritySet(INT_ID_0, INT_VECTOR_CMP2, INT_SUBPRIORITY_LEVEL0);

            /* Enable multi-vectored interrupts, enable the generation of interrupts to the CPU */
            PLIB_INT_MultiVectorSelect(INT_ID_0);
            PLIB_INT_Enable(INT_ID_0);

            appData.state = APP_STATE_RUN;
            break;

        case APP_STATE_RUN:
            if (appData.status) /* If the non-inverting input of the comparator > inverting input */
            {
                /* Turn on upper four LEDs */
                PLIB_PORTS_Clear(PORTS_ID_0, PORT_CHANNEL_A, (PORTS_DATA_MASK)0x00FF);
                PLIB_PORTS_Write(PORTS_ID_0, PORT_CHANNEL_A, (PORTS_DATA_MASK)0x00F0);
            }

            else /* If the non-inverting input of the comparator < inverting input */
            {
                /* Turn on lower four LEDs */
                PLIB_PORTS_Clear(PORTS_ID_0, PORT_CHANNEL_A, (PORTS_DATA_MASK)0x00FF);
                PLIB_PORTS_Write(PORTS_ID_0, PORT_CHANNEL_A, (PORTS_DATA_MASK)0x000F);
            }
            break;

        /* The default state should never be executed. */
        default:
            break;

    }


   
} 


/*******************************************************************************
 End of File
 */

