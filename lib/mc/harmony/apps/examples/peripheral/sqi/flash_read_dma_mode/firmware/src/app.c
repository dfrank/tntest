/*******************************************************************************
  MPLAB Harmony Application 
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    Application Template

  Description:
    This file contains the application logic.
 *******************************************************************************/


// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Variable Definitions
// *****************************************************************************
// *****************************************************************************


/*****************************************************
 * Initialize the application data structure. All
 * application related variables are stored in this
 * data structure.
 *****************************************************/

APP_DATA appData = 
{
    //TODO - Initialize appData structure. 

};
// *****************************************************************************
/* Driver objects.

  Summary:
    Contains driver objects.

  Description:
    This structure contains driver objects returned by the driver init routines
    to the application. These objects are passed to the driver tasks routines.
*/


APP_DRV_OBJECTS appDrvObject;

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Routines
// *****************************************************************************
// *****************************************************************************


// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Routines
// *****************************************************************************
// *****************************************************************************



// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine
// *****************************************************************************
// *****************************************************************************

/******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    // Enable and Reset SQI
    SQI1CFG = 0x80000000;

    PLIB_SQI_SoftReset(SQI_ID_0);

    // Configure SQI1CFG
    PLIB_SQI_TransferModeSet(SQI_ID_0, SQI_XFER_MODE_PIO);
    PLIB_SQI_DataModeSet(SQI_ID_0, SQI_DATA_MODE_3);
    PLIB_SQI_BurstEnable(SQI_ID_0);
    PLIB_SQI_DataOutputEnableSelect(SQI_ID_0, SQI_DATA_OEN_QUAD);
    PLIB_SQI_CSOutputEnableSelect(SQI_ID_0, SQI_CS_OEN_1);

    // Configure SQI1CLKCON
    PLIB_SQI_ClockEnable(SQI_ID_0);
    while(!PLIB_SQI_ClockIsStable(SQI_ID_0));
    PLIB_SQI_ClockDividerSet(SQI_ID_0, CLK_DIV_2);

    /* Place the App state machine in it's initial state. */
    appData.state = APP_STATE_INIT_FLASH;
}

/********************************************************
 * Application switch press routine
 ********************************************************/



/**********************************************************
 * Application tasks routine. This function implements the
 * application state machine.
 ***********************************************************/
void APP_Tasks ( void )
{
    SQI_STATUS sqiStatus;

    /* Check the application state*/
    switch ( appData.state )
    {
        /* Initialize the Flash for writes and reads. */
        case APP_STATE_INIT_FLASH:
        {
            SQI_Flash_Setup();

            /* Update the state */
            appData.state = APP_STATE_FLASH_ID_READ;

            break;
        }

        /* Read flash ID until succesful */
        case APP_STATE_FLASH_ID_READ:
        {
            /* Get the ID read status */
            sqiStatus = SQI_FlashID_Read();
            if ( sqiStatus !=  SQI_STATUS_SUCCESS)
                /* Update the state */
                appData.state = APP_STATE_FLASH_ID_READ;
            else
                /* Update the state */
                appData.state = APP_STATE_WRITE_FLASH;
            break;

        }

        /* Write flash*/
        case APP_STATE_WRITE_FLASH:
        {
            SQI_PIO_PageWrite(FLASH_PAGE_ADDR);

            /* Update the state */
            appData.state = APP_STATE_READ_FLASH_DMA_MODE;
        }

        /* Read flash ID until succesful */
        case APP_STATE_READ_FLASH_DMA_MODE:
        {
            /* Get the ID read status */
            sqiStatus = SQI_DMA_Read(FLASH_PAGE_ADDR);
            if ( sqiStatus !=  SQI_STATUS_SUCCESS)
                /* Update the state */
                appData.state = APP_STATE_INIT_FLASH;
            else
                /* Update the state */
                appData.state = APP_STATE_DONE;
            break;

        }

        /* Idle state (do nothing) */
        case APP_STATE_DONE:
        default:
            BSP_LEDStateSet(BSP_LED_3, BSP_LED_STATE_ON);
            break;
    }
} 

/*******************************************************************************
 End of File
 */

