/*******************************************************************************
  SQI functions

  File Name:
    sqi.c

  Summary:
    SQI functions

  Description:
    This is the sample driver that implements all the SQI functions.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

//Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
#include "app.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Variables
// *****************************************************************************
// *****************************************************************************
uint32_t __attribute__((coherent)) JedecID;
uint32_t __attribute__((coherent)) JedecIDGolden;

// *****************************************************************************
// *****************************************************************************
// Function: Core Timer Read
// *****************************************************************************
// *****************************************************************************
static uint32_t APP_ReadCoreTimer()
{
    volatile uint32_t timer;

    // get the count reg
    asm volatile("mfc0   %0, $9" : "=r"(timer));

    return(timer);
}

// *****************************************************************************
// *****************************************************************************
// Function: Core Timer Sart
// *****************************************************************************
// *****************************************************************************
static void APP_StartCoreTimer(uint32_t period)
{
    /* Reset the coutner */
    volatile uint32_t loadZero = 0;

    asm volatile("mtc0   %0, $9" : "+r"(loadZero));
    asm volatile("mtc0   %0, $11" : "+r" (period));
}
// *****************************************************************************
// *****************************************************************************
// Function: Core Timer Delay
// *****************************************************************************
// *****************************************************************************
static void APP_CoreTimer_Delay(uint32_t delayValue)
{
    while ((APP_ReadCoreTimer() <= delayValue))
        asm("nop");
}

// *****************************************************************************
// *****************************************************************************
// Section: SQI PIO Read
// *****************************************************************************
// *****************************************************************************
int SQI_PIO_Read (uint32_t address)
{
    uint32_t readLoop, bufLoop, checkLoop;
    uint32_t *readBuf = (uint32_t *) PIO_READ_BUF_ADDR;
    uint32_t * writeBuf = (uint32_t *) WRITE_BUF_ADDR;
    uint8_t tempAddress1, tempAddress2, tempAddress3;
    uint32_t errCount = 1;

    // Address manipulation (LaZ logic)
    tempAddress1 = (uint8_t) (address >> 16);
    tempAddress2 = (uint8_t) (address >> 8);
    tempAddress3 = (uint8_t) address;
    address = tempAddress1 | tempAddress2 << 8 | tempAddress3 << 16;

        // Configure SQI1CLKCON
    PLIB_SQI_ClockEnable(SQI_ID_0);
    while(!PLIB_SQI_ClockIsStable(SQI_ID_0));
    PLIB_SQI_ClockDividerSet(SQI_ID_0, CLK_DIV_2);

    // SQI Transfer Configuration
    // Setup control word to send NOP command
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_SINGLE,SQI_CMD_TRANSMIT,1);
    // Setup control word to send NOP command
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_SINGLE,SQI_CMD_TRANSMIT,1);
    // Setup control word to send EQIO command
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_SINGLE,SQI_CMD_TRANSMIT,1);
    // Setup control word to send FAST READ command
    PLIB_SQI_ControlWordSet(SQI_ID_0,0,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_TRANSMIT,1);
    // Setup control word to send Address bytes command
    PLIB_SQI_ControlWordSet(SQI_ID_0,0,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_TRANSMIT,4);

    //Write the command to the transfer buffer {WEN,EQIO,NOP,NOP}
    PLIB_SQI_TransmitData(SQI_ID_0, SST26VF_FAST_READ << 24 |
                          SST26VF_EQIO << 16 |
                          SST26VF_NOP << 8 |
                          SST26VF_NOP);

    // Write address and dummy bytes {DUMMY, ADDRESS[3:0]}
    PLIB_SQI_TransmitData(SQI_ID_0, 0x0 << 24 |
                          address);

    // Setup control word to read 256 bytes
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_RECEIVE,0x100);

    // Setup receive buffer threshold
    PLIB_SQI_RxBufferThresholdSet(SQI_ID_0,0x1F);
    PLIB_SQI_RxBufferThresholdIntSet(SQI_ID_0,0x1F);

    for (readLoop=0; readLoop < 8; readLoop++)
    {
        while ((PLIB_SQI_RxBufferThresholdGet(SQI_ID_0)& 0xFF) != 0x1F);
        for (bufLoop=0; bufLoop < MAX_READ_BUF_DEPTH; bufLoop++){
            *readBuf++ = PLIB_SQI_ReceiveData(SQI_ID_0);
        }
    }

    readBuf = (uint32_t *) PIO_READ_BUF_ADDR;

    for (checkLoop=0; checkLoop <64; checkLoop++){
        if (*readBuf++ != *writeBuf++)
            errCount++;
    }
    
    return errCount;
}

// *****************************************************************************
// *****************************************************************************
// Section: SQI PIO Write
// *****************************************************************************
// *****************************************************************************
void SQI_PIO_PageWrite (uint32_t address)
{
    uint32_t writeLoop, bufLoop;
    uint8_t writeLoopChar = 0;
    uint8_t * writeBufAddrChar = (uint8_t *) WRITE_BUF_ADDR;
    uint8_t * txBufChar  = (uint8_t *) SQI_TXBUF_ADDR;
    uint8_t tempAddress1, tempAddress2, tempAddress3;

    // Setup transmit data
    for (writeLoop=0;writeLoop < 256; writeLoop++)
        *writeBufAddrChar++= writeLoopChar++;

    writeBufAddrChar = (uint8_t *) WRITE_BUF_ADDR;

    // Address manipulation (LaZ logic)
    tempAddress1 = (uint8_t) (address >> 16);
    tempAddress2 = (uint8_t) (address >> 8);
    tempAddress3 = (uint8_t) address;
    address = tempAddress1 | tempAddress2 << 8 | tempAddress3 << 16;

    PLIB_SQI_ClockEnable(SQI_ID_0);
    while(!PLIB_SQI_ClockIsStable(SQI_ID_0));
    PLIB_SQI_ClockDividerSet(SQI_ID_0, CLK_DIV_1);

    //Setup SQI transmit buffer thresholds
    PLIB_SQI_TxBufferThresholdSet(SQI_ID_0,1);
    PLIB_SQI_TxBufferThresholdIntSet(SQI_ID_0,1);

    // SQI Transfer Configuration
    // Setup control word to send NOP command
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_SINGLE,SQI_CMD_TRANSMIT,1);
    // Setup control word to send NOP command
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_SINGLE,SQI_CMD_TRANSMIT,1);
    // Setup control word to send EQIO command
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_SINGLE,SQI_CMD_TRANSMIT,1);
    // Setup control word to send WEN command
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_TRANSMIT,1);

    //Write the command to the transfer buffer {WEN,EQIO,NOP,NOP}
    PLIB_SQI_TransmitData(SQI_ID_0, SST26VF_WEN << 24 |
                          SST26VF_EQIO << 16 |
                          SST26VF_NOP << 8 |
                          SST26VF_NOP);

    //Start Write
    // Setup control word to send PAGE WRITE command
    PLIB_SQI_ControlWordSet(SQI_ID_0,0,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_TRANSMIT,1);
    // Setup control word to send ADDRESS
    PLIB_SQI_ControlWordSet(SQI_ID_0,0,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_TRANSMIT,3);
    // Setup control word to send 256 bytes of DATA
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_TRANSMIT,0x100);

    // Write the command to the transfer buffer
    PLIB_SQI_TransmitData(SQI_ID_0, address << 8 |
                          SST26VF_PAGE_WRITE);

    //Write the data to flash
    for (writeLoop=0; writeLoop < 16; writeLoop++){
        APP_StartCoreTimer(0);
        APP_CoreTimer_Delay(1000);
        while (!(PLIB_SQI_InterruptFlagGet(SQI_ID_0,SQI_TXTHR)));
        for (bufLoop=0; bufLoop < MAX_WRITE_BUF_DEPTH; bufLoop++){
            *txBufChar = *writeBufAddrChar++;          // Next byte of write data
        }
    }

    APP_StartCoreTimer(0);
    APP_CoreTimer_Delay(10000000);
}

// *****************************************************************************
// *****************************************************************************
// Section: SQI Flash Setup
// *****************************************************************************
// *****************************************************************************
int SQI_FlashID_Read (void)
{
    JedecIDGolden = SST26VF_JEDECID;

    // Setup SQI FIFO Thresholds
    PLIB_SQI_ControlBufferThresholdSet(SQI_ID_0, 1);
    PLIB_SQI_TxBufferThresholdSet(SQI_ID_0,1);
    PLIB_SQI_RxBufferThresholdSet(SQI_ID_0,4);
    PLIB_SQI_TxBufferThresholdIntSet(SQI_ID_0,1);
    PLIB_SQI_RxBufferThresholdIntSet(SQI_ID_0,4);
    
    // Setup control word to send NOP command
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_SINGLE,SQI_CMD_TRANSMIT,1);
    // Setup control word to send NOP command
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_SINGLE,SQI_CMD_TRANSMIT,1);
    // Setup control word to send EQIO command
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_SINGLE,SQI_CMD_TRANSMIT,1);
    // Setup control word to send FAST READ command
    PLIB_SQI_ControlWordSet(SQI_ID_0,0,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_TRANSMIT,1);

    //Write the command to the transfer buffer {WEN,EQIO,NOP,NOP}
    PLIB_SQI_TransmitData(SQI_ID_0, SST26VF_QJID << 24 |
                          SST26VF_EQIO << 16 |
                          SST26VF_NOP << 8 |
                          SST26VF_NOP);

    // Setup control word to read 256 bytes
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_RECEIVE,4);

    PLIB_SQI_RxBufferThresholdSet(SQI_ID_0,4);
    PLIB_SQI_RxBufferThresholdIntSet(SQI_ID_0,4);

    while ((PLIB_SQI_RxBufferThresholdGet(SQI_ID_0)& 0xFF) != 0x04);
    {
        APP_StartCoreTimer(0);
        APP_CoreTimer_Delay(1000);
        JedecID = PLIB_SQI_ReceiveData(SQI_ID_0);
    }

    if (JedecID != JedecIDGolden)
        return false;
    else
        return true;

}

// *****************************************************************************
// *****************************************************************************
// Section: SQI Flash Setup
// *****************************************************************************
// *****************************************************************************
void SQI_Flash_Setup (void)
{
    uint32_t blockProtectLoop;

    // Setup SQI FIFO Thresholds
    PLIB_SQI_ControlBufferThresholdSet(SQI_ID_0, 1);
    PLIB_SQI_TxBufferThresholdSet(SQI_ID_0,1);
    PLIB_SQI_RxBufferThresholdSet(SQI_ID_0,1);
    PLIB_SQI_TxBufferThresholdIntSet(SQI_ID_0,1);
    PLIB_SQI_RxBufferThresholdIntSet(SQI_ID_0,1);

    // SQI Transfer Configuration
    // Setup control word to send NOP command
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_SINGLE,SQI_CMD_TRANSMIT,1);
    // Setup control word to send EQIO command
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_SINGLE,SQI_CMD_TRANSMIT,1);
    // Setup control word to send WEN command
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_TRANSMIT,1);
    // Setup control word to send ERASE command
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_TRANSMIT,1);

    // Write the command into the transmit buffer ({CHIP ERASE,WEN,EQIO,NOP})
    PLIB_SQI_TransmitData(SQI_ID_0, SST26VF_ERASE << 24 |
                          SST26VF_WEN << 16 |
                          SST26VF_EQIO << 8 |
                          SST26VF_NOP);

    APP_StartCoreTimer(0);
    APP_CoreTimer_Delay(5000000);                // 38 ms for erase

    // Setup control word to send three NOP commands
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_TRANSMIT,1);
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_TRANSMIT,1);
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_TRANSMIT,1);
    // Setup control word to send block unprotect command followed by the bits
    PLIB_SQI_ControlWordSet(SQI_ID_0,0,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_TRANSMIT,1);
    PLIB_SQI_ControlWordSet(SQI_ID_0,1,SQI_CS_1,SQI_LANE_QUAD,SQI_CMD_TRANSMIT,0xC);

    // Setup transfer threshold
    PLIB_SQI_TxBufferThresholdSet(SQI_ID_0,0xC);
    PLIB_SQI_TxBufferThresholdIntSet(SQI_ID_0,0xC);

    // Write the command and partial write ptotect bits to the transmit register
    // {CHIP ERASE,WEN,EQIO,NOP}
    PLIB_SQI_TransmitData(SQI_ID_0, SST26VF_BLKUP << 24 |
                          SST26VF_NOP << 16 |
                          SST26VF_NOP << 8 |
                          SST26VF_NOP);

    for (blockProtectLoop=0; blockProtectLoop <3; blockProtectLoop++)
    {
        PLIB_SQI_TransmitData(SQI_ID_0, 0x00000000);
    }


}

/*******************************************************************************
 End of File
*/
