/*******************************************************************************
  MPLAB Harmony Application I2C Master Slave Example
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    MPLAB Harmony master_slave application logic

  Description:
    This file contains the MPLAB Harmony master_slave application logic.
 *******************************************************************************/


// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"
#include "i2c.h"
#include "bsp_config.h"


// *****************************************************************************
// *****************************************************************************
// Section: Global Variable Definitions
// *****************************************************************************
// *****************************************************************************

bool startTransfer;
char *someString = "Microchip I2C";
char *strPtr;
static unsigned char len_of_str = 9;
volatile unsigned int delay = 5000;

/*****************************************************
 * Initialize the application data structure. All
 * application related variables are stored in this
 * data structure.
 *****************************************************/

APP_DATA appData = 
{
    //TODO - Initialize appData structure. 

};
// *****************************************************************************
/* Application Data

  Summary:
    Contains application data

  Description:
    This structure contains driver objects returned by the driver init routines
    to the application. These objects are passed to the driver tasks routines.
*/


APP_DATA appObject;

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Routines
// *****************************************************************************
// *****************************************************************************


// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Routines
// *****************************************************************************
// *****************************************************************************



// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine
// *****************************************************************************
// *****************************************************************************

/******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
   /* Clear the core timer. */
//    BSP_StartTimer(0);

    /* Put the application into its initial state */
    appObject.state = I2C_STATE_IDLE;

    strPtr = someString;
    startTransfer = true;
}

/********************************************************
 * Application switch press routine
 ********************************************************/



/**********************************************************
 * Application tasks routine. This function implements the
 * application state machine.
 ***********************************************************/
void APP_Tasks(void)
{
     LEDIndicateWorking();

    if (masterTransferIsComplete() )         
    {
        switch (appObject.state)
        {
            case I2C_STATE_IDLE:
                if (startTransfer) {
                    appObject.state = I2C_STATE_SEND_START_CONDITION;
                }
                break;

            case I2C_STATE_SEND_START_CONDITION:
                while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_MODULE_MASTER));   //Wait as long as TRSTAT == 1    
                while(PLIB_I2C_ArbitrationLossHasOccurred(I2C_MODULE_MASTER));     //Wait as long as BCL = 1
                I2C1STATCLR = 0x00000080;           //clear IWCOL bit
                PLIB_I2C_MasterStart(I2C_MODULE_MASTER);
                delay_some(5000);
                appObject.state = I2C_STATE_SEND_ADDRESS_BYTE;
                break;

            case I2C_STATE_SEND_ADDRESS_BYTE:    
                while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_MODULE_MASTER));   //Wait as long as TRSTAT == 1
                while(PLIB_I2C_ArbitrationLossHasOccurred(I2C_MODULE_MASTER));     //Wait as long as BCL = 1         
                PLIB_I2C_TransmitterByteSend(I2C_MODULE_MASTER, EEPROMslaveAddress);
                while(PLIB_I2C_TransmitterIsBusy(I2C_MODULE_MASTER));               //Wait as long as TBF = 1
                while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_MODULE_MASTER));   //Wait as long as TRSTAT == 1
                delay_some(5000);
                appObject.state = I2C_STATE_SEND_DATA;
                break;

            case I2C_STATE_SEND_DATA:
                 while (len_of_str > 0)
                 {
                     PLIB_I2C_TransmitterByteSend(I2C_MODULE_MASTER, *strPtr);
                     strPtr++;
                     while(PLIB_I2C_TransmitterIsBusy(I2C_MODULE_MASTER));               //Wait as long as TBF = 1
                     while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_MODULE_MASTER));    //Wait as long as TRSTAT == 1
                     appObject.state = I2C_STATE_SEND_DATA;
                     len_of_str--;
                 }       
                 appObject.state = I2C_STATE_SEND_STOP_CONDITION;       
                break;

            case I2C_STATE_SEND_STOP_CONDITION:
                PLIB_I2C_MasterStop(I2C_MODULE_MASTER);
                startTransfer = false;
                appObject.state = I2C_READ_SEND_START_CONDITON;
                break;

            case I2C_READ_SEND_START_CONDITON:
                PLIB_I2C_Disable(I2C_MODULE_MASTER);
                PLIB_I2C_Enable(I2C_MODULE_MASTER);
                delay_some(5000);                       //MHCTEST_0728
                while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_MODULE_MASTER));   //Wait as long as TRSTAT == 1
                while(PLIB_I2C_ArbitrationLossHasOccurred(I2C_MODULE_MASTER));     //Wait as long as BCL = 1
                I2C1STATCLR = 0x00000080;           //clear IWCOL bit
                PLIB_I2C_MasterStart(I2C_MODULE_MASTER);
                delay_some(5000);
                appObject.state = I2C_STATE_SEND_READ_ADDRESS_BYTE;
                break;

            case I2C_STATE_SEND_READ_ADDRESS_BYTE:
                while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_MODULE_MASTER));   //Wait as long as TRSTAT == 1    
                while(PLIB_I2C_ArbitrationLossHasOccurred(I2C_MODULE_MASTER));     //Wait as long as BCL = 1
                PLIB_I2C_TransmitterByteSend(I2C_MODULE_MASTER, (EEPROMslaveAddress | 0x01));   //Send Read Address         
                while(PLIB_I2C_TransmitterIsBusy(I2C_MODULE_MASTER));       //Wait as long as TBF = 1                    
                while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_MODULE_MASTER));   //Wait as long as TRSTAT == 1
                delay_some(5000);
                appObject.state = I2C_STATE_READ_DATA;
                break;

            case I2C_STATE_READ_DATA:
                len_of_str = 8;
                  while (len_of_str > 0)
                  {                      
                      PLIB_I2C_MasterReceiverClock1Byte(I2C_MODULE_MASTER);     //Set Rx enable in MSTR which causes SLAVE to send data               
                      while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_MODULE_MASTER));  // Wait till RBF = 1; Which means data is available in I2C2RCV reg                  
                      rxBuff[len_of_str] = PLIB_I2C_ReceivedByteGet(I2C_MODULE_MASTER); //Read from I2CxRCV
                      if (len_of_str > 1)
                      {
                         if ( PLIB_I2C_MasterReceiverReadyToAcknowledge ( I2C_MODULE_MASTER ) )
                             PLIB_I2C_ReceivedByteAcknowledge ( I2C_MODULE_MASTER, true );     //Send ACK to Slave
                      }
                      len_of_str--;
                      delay_some(5000);
                  }             
                  if ( PLIB_I2C_MasterReceiverReadyToAcknowledge ( I2C_MODULE_MASTER ) )
                  {
                         PLIB_I2C_ReceivedByteAcknowledge ( I2C_MODULE_MASTER, false );     //last byte; send NACK to Slave, no more data needed
                  }
                  while( PLIB_I2C_MasterReceiverReadyToAcknowledge (I2C_MODULE_MASTER)); // wait till NACK sequence is complete i.e ACKEN = 0
                  appObject.state = I2C_STATE_READ_STOP_CONDITION;
                  break;

            case I2C_STATE_READ_STOP_CONDITION:
                PLIB_I2C_MasterStop(I2C_MODULE_MASTER);
                startTransfer = false;
                appObject.state = I2C_STATE_IDLE;
                break;

            default:
                DBPRINTF("ERROR! Invalid state %d\n", testState);
                while (1);
        }
    }
}

/*******************************************************************************
 End of File
 */

