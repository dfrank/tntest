/*******************************************************************************
  MPLAB Harmony I2C Functions

  Company:
    Microchip Technology Inc.

  File Name:
    i2c_functions.c

  Summary:
    MPLAB Harmony i2c_functions source file

  Description:
    Contains basic functions to write characters and strings to the UART module.

  Tested with:
    -PIC32MX795F512L on the Explorer-16 Demo Board
    -XC32 compiler, MPLAB X IDE
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "i2c.h"
#include "app.h"
#include "bsp_config.h"

void LEDIndicateWorking(void)
{
    if (APP_ReadCoreTimer() >= APP_LED_BLINK_DELAY_1S)
    {
        /* Toggle LED */
        BSP_LEDToggle(BSP_LED_3);

        /* Clear the core timer to restart count. */
        APP_StartTimer(0);
    }
}

bool masterTransferIsComplete(void)
{
    return !PLIB_I2C_TransmitterIsBusy(I2C_MODULE_MASTER);
}

/*******************************************************************************
/*
  Function:
    void initMaster (int baudRate, int clockFrequency)

  Summary:
    Initializes I2C1 module as the master.
*/
void initMaster(int baudRate, int clockFrequency)
{
    PLIB_I2C_Disable(I2C_MODULE_MASTER);
    PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_I2C_1_MASTER);
    PLIB_INT_SourceDisable(INT_ID_0,INT_SOURCE_I2C_1_MASTER);
    PLIB_I2C_HighFrequencyEnable(I2C_MODULE_MASTER);
    PLIB_I2C_BaudRateSet(I2C_MODULE_MASTER, clockFrequency, baudRate);
    while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_MODULE_MASTER));   //Wait as long as TRSTAT == 1
    while(PLIB_I2C_ArbitrationLossHasOccurred(I2C_MODULE_MASTER));     //Wait as long as BCL = 1
    PLIB_I2C_Enable(I2C_MODULE_MASTER);
}


/*******************************************************************************
/*
  Function:
    void initSlave (int baudRate, int clockFrequency, int address)

  Summary:
    Initializes I2C2 module as the slave.
*/
void initSlave(int baudRate, int clockFrequency, int address)
{
    PLIB_INT_VectorPrioritySet(INT_ID_0,INT_SOURCE_I2C_3_SLAVE,INT_PRIORITY_LEVEL4);
    PLIB_INT_VectorSubPrioritySet(INT_ID_0,INT_SOURCE_I2C_3_SLAVE,INT_SUBPRIORITY_LEVEL0 );
    PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_I2C_3_SLAVE);
    PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_I2C_3_SLAVE);
    PLIB_I2C_HighFrequencyEnable(I2C_MODULE_SLAVE);
    PLIB_I2C_SlaveAddress7BitSet(I2C_MODULE_SLAVE, address);
    PLIB_I2C_SlaveClockStretchingEnable ( I2C_MODULE_SLAVE );
    PLIB_I2C_SlaveClockRelease(I2C_MODULE_SLAVE);
    PLIB_I2C_SlaveMask7BitSet (I2C_MODULE_SLAVE, address7bit_mask_all );
    PLIB_I2C_Enable(I2C_MODULE_SLAVE);
    delay_some(500);
    Nop();
}

/*******************************************************************************
/*
  Function:
    void waitForIdleMaster (void)

  Summary:
    Blocking loop that waits for the I2C master to become idle.
*/
bool masterIsIdle(void)
{
    /* Wait until I2C Bus is Inactive */
    while(PLIB_I2C_BusIsIdle(I2C_MODULE_MASTER) == 0);
}


/*******************************************************************************
/*
  Function:
    char SlaveRead(void)

  Summary:
    Reads out a byte of data from the slave receive buffer.
*/
char SlaveRead(void)
{
     while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_MODULE_SLAVE)); // wait for receive buffer to fill

     PLIB_I2C_ReceiverOverflowClear(I2C_MODULE_SLAVE); // clear overflow flag

     return (PLIB_I2C_ReceivedByteGet(I2C_MODULE_SLAVE));
}

void initMasterStatusRegister(void)
{
    MASTER_STATUS_CLEAR;
}

void initSlaveStatusRegister(void)
{
    SLAVE_STATUS_CLEAR;
}

void delay_some(unsigned int delay)
{
    while (delay > 0) delay--;
    Nop();
}

uint32_t APP_ReadCoreTimer()
{
    uint32_t timer;

    // get the count reg
    asm volatile("mfc0   %0, $9" : "=r"(timer));

    return(timer);
}

void APP_StartTimer(uint32_t period)
{
    /* Reset the coutner */

    uint32_t loadZero = 0;

    asm volatile("mtc0   %0, $9" : "+r"(loadZero));
    asm volatile("mtc0   %0, $11" : "+r" (period));

}


/*******************************************************************************
 End of File
*/
