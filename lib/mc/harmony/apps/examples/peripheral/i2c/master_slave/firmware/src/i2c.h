/* 
 * File:   i2c.h
 * Author: C15287
 *
 * Created on August 12, 2013, 2:32 PM
 */

#ifndef I2C_H
#define	I2C_H

#include <stdbool.h>
#include <stdint.h>

#define MASTER_STATUS_CLEAR                 I2C1STATCLR = 0xFFFF;
#define SLAVE_STATUS_CLEAR                  I2C3STATCLR = 0xFFFF;

void LEDIndicateWorking(void);
bool masterTransferIsComplete(void);
void initMaster(int baudRate, int clockFrequency);
void initSlave(int baudRate, int clockFrequency, int address);
void initMasterStatusRegister(void);
void initSlaveStatusRegister(void);
char SlaveRead(void);
unsigned int APP_ReadCoreTimer();
void APP_StartTimer(uint32_t period);
void delay_some(unsigned int);

char rxBuff[10];

#endif	/* I2C_H */

