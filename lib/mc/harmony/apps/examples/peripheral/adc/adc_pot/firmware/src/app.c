/*******************************************************************************
  MPLAB Harmony Application 
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the application logic.
 *******************************************************************************/


// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"
#include "peripheral/ports/plib_ports.h"
#include "peripheral/adc/plib_adc.h"
#include "system/system.h"
#include "system/devcon/sys_devcon.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Variable Definitions
// *****************************************************************************
// *****************************************************************************


/*****************************************************
 * Initialize the application data structure. All
 * application related variables are stored in this
 * data structure.
 *****************************************************/

APP_DATA appData = 
{
    //TODO - Initialize appData structure. 

};
// *****************************************************************************
/* Driver objects.

  Summary:
    Contains driver objects.

  Description:
    This structure contains driver objects returned by the driver init routines
    to the application. These objects are passed to the driver tasks routines.
*/


APP_DRV_OBJECTS appDrvObject;

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Routines
// *****************************************************************************
// *****************************************************************************


// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Routines
// *****************************************************************************
// *****************************************************************************



// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine
// *****************************************************************************
// *****************************************************************************

/******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    appData.state = APP_STATE_INIT;
}




/**********************************************************
 * Application tasks routine. This function implements the
 * application state machine.
 ***********************************************************/
void APP_Tasks ( void )
{
    int i;

    /* check the application state*/
    switch ( appData.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
            /* Turn off JTAG so we have full LEDs */
            SYS_DEVCON_JTAGDisable();
            /* Set pin AN2 as analog input */
            PLIB_PORTS_PinModeSelect(PORTS_ID_0, PORTS_ANALOG_PIN_2, PORTS_PIN_MODE_ANALOG);
            appData.state = APP_STATE_SETUP_ADC;
            break;

        case APP_STATE_SETUP_ADC:
            /* ADC setup - ouput in integer format, trigger mode auto, enable autosample */
            PLIB_ADC_ResultFormatSelect(ADC_ID_1, ADC_RESULT_FORMAT_INTEGER_16BIT);
            PLIB_ADC_SamplingModeSelect(ADC_ID_1, ADC_SAMPLING_MODE_MUXA);
            PLIB_ADC_ConversionTriggerSourceSelect(ADC_ID_1, ADC_CONVERSION_TRIGGER_INTERNAL_COUNT);
            PLIB_ADC_VoltageReferenceSelect(ADC_ID_1, ADC_REFERENCE_VDD_TO_AVSS );
            PLIB_ADC_SampleAcqusitionTimeSet(ADC_ID_1, 0x1F);
            PLIB_ADC_ConversionClockSet(ADC_ID_1, SYS_DEVCON_SYSTEM_CLOCK, 32);

            /* Connect AN2 as positive input, Vref- as negative input */
            PLIB_ADC_MuxChannel0InputPositiveSelect(ADC_ID_1, ADC_MUX_A, ADC_INPUT_POSITIVE_AN2);
            PLIB_ADC_MuxChannel0InputNegativeSelect(ADC_ID_1, ADC_MUX_A, ADC_INPUT_NEGATIVE_VREF_MINUS);

            /* Enable the ADC module */
            PLIB_ADC_Enable(ADC_ID_1);

            appData.state = APP_STATE_START_CAPTURE;
            break;

        case APP_STATE_START_CAPTURE:
            PLIB_ADC_SamplingStart(ADC_ID_1);

            appData.state = APP_STATE_WAIT_FOR_ADC;
            break;

        case APP_STATE_WAIT_FOR_ADC:
            if (PLIB_ADC_ConversionHasCompleted(ADC_ID_1))
                appData.state = APP_STATE_OUTPUT_RESULT;
            break;

        case APP_STATE_OUTPUT_RESULT:
            appData.potValue = PLIB_ADC_ResultGetByIndex(ADC_ID_1, 0);
            appData.potValue >>= 7; /* 10-bit value to 3-bit value */

            appData.ledMask = 0;

            /* Creates a mask for the LEDs, corresponding to the value read from
               the potentiometer */
            for (i = 0; i <= appData.potValue; i++)
            {
                appData.ledMask |=  1<<(i);
    		}

            /* Write the mask to the LEDs */
            PLIB_PORTS_Write(PORTS_ID_0, PORT_CHANNEL_A, (PORTS_DATA_MASK)appData.ledMask);

            appData.state = APP_STATE_START_CAPTURE;
            break;
   
        /* The default state should never be executed. */
        default:
            break;
	} 
}


/*******************************************************************************
 End of File
 */

