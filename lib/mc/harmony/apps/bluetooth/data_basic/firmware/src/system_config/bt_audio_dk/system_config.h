/*******************************************************************************
  System Configuration file

  Company:
    Microchip Technology Inc.

  File Name:
    system_config.h

  Summary:
    This file contains the system configuration.

  Description:
    This file contains the system configuration.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
******************************************************************************/
//DOM-IGNORE-END

#ifndef _SYSTEM_CONFIG_H_
#define _SYSTEM_CONFIG_H_

// *****************************************************************************
// *****************************************************************************
// Section: Constants
// *****************************************************************************
// *****************************************************************************
/* Prevent superfluous PLIB warnings. */
#define _PLIB_UNSUPPORTED

// *****************************************************************************
// *****************************************************************************
// Section: Clock System Service Configuration
// *****************************************************************************
// *****************************************************************************
/* Input crystal frequency for this board */
#define SYS_CLK_SOURCE                      SYS_CLK_SOURCE_PRIMARY
#define SYS_CLK_FREQ                        96000000ul
#define SYS_CLK_CONFIG_PRIMARY_XTAL         12000000ul
#define SYS_CLK_CONFIG_SECONDARY_XTAL       0ul
#define SYS_CLK_CONFIG_SYSPLL_INP_DIVISOR   3
#define SYS_CLK_CONFIG_FREQ_ERROR_LIMIT     10
#define SYS_CLK_CONFIGBIT_USBPLL_ENABLE     true
#define SYS_CLK_CONFIGBIT_USBPLL_DIVISOR    3
#define SYS_CLK_WAIT_FOR_SWITCH             true
#define SYS_CLK_KEEP_SECONDARY_OSC_ENABLED  true
#define SYS_CLK_ON_WAIT                     OSC_ON_WAIT_IDLE

#define SYS_DEVCON_PIC32MX_MAX_PB_FREQ              48000000UL
// *****************************************************************************
// *****************************************************************************
// Section: USART Driver Configuration
// *****************************************************************************
// *****************************************************************************
/* Number of USART Driver instance is 1*/
#define DRV_USART_INSTANCES_NUMBER                  1

/* This applications creates 2 clients */
#define DRV_USART_CLIENTS_NUMBER                    1

/* USART Driver should be built for interrup mode*/
#define DRV_USART_INTERRUPT_MODE                    true

/* Combined queue depth is 2*/
#define DRV_USART_QUEUE_DEPTH_COMBINED              2

/* This application uses buffer queues */
#define DRV_USART_BUFFER_QUEUE_SUPPORT              true

/* The application does not use byte model */
#define DRV_USART_BYTE_MODEL_SUPPORT                false

/* The applicationdoes not use read write model */
#define DRV_USART_READ_WRITE_MODEL_SUPPORT          false


// *****************************************************************************
// *****************************************************************************
// Section: Timer Driver Configuration
// *****************************************************************************
// *****************************************************************************
/* Timer driver is in interrupt mode */
#define DRV_TMR_INTERRUPT_MODE                      true

/* Two instance of the timer driver is needed */
#define DRV_TMR_INSTANCES_NUMBER                    2

/* 2 client for the timer driver */
#define DRV_TMR_CLIENTS_NUMBER                      2

#define DRV_TMR_PERIPHERAL_ID_IDX0          TMR_ID_1
#define DRV_TMR_INTERRUPT_SOURCE_IDX0       INT_SOURCE_TIMER_1
#define DRV_TMR_INTERRUPT_VECTOR_IDX0       INT_VECTOR_T1
#define DRV_TMR_ISR_VECTOR_IDX0             _TIMER_1_VECTOR
#define DRV_TMR_INTERRUPT_PRIORITY_IDX0     INT_PRIORITY_LEVEL1
#define DRV_TMR_INTERRUPT_SUB_PRIORITY_IDX0 INT_SUBPRIORITY_LEVEL0
#define DRV_TMR_CLOCK_SOURCE_IDX0           DRV_TMR_CLKSOURCE_INTERNAL
#define DRV_TMR_PRESCALE_IDX0               TMR_PRESCALE_VALUE_8
#define DRV_TMR_OPERATION_MODE_IDX0         DRV_TMR_OPERATION_MODE_16_BIT
#define DRV_TMR_ASYNC_WRITE_ENABLE_IDX0     false
#define DRV_TMR_POWER_STATE_IDX0            SYS_MODULE_POWER_RUN_FULL

#define DRV_TMR_PERIPHERAL_ID_IDX1          TMR_ID_2
#define DRV_TMR_INTERRUPT_SOURCE_IDX1       INT_SOURCE_TIMER_3
#define DRV_TMR_INTERRUPT_VECTOR_IDX1       INT_VECTOR_T3
#define DRV_TMR_ISR_VECTOR_IDX1             _TIMER_3_VECTOR
#define DRV_TMR_INTERRUPT_PRIORITY_IDX1     INT_PRIORITY_LEVEL3
#define DRV_TMR_INTERRUPT_SUB_PRIORITY_IDX1 INT_SUBPRIORITY_LEVEL0
#define DRV_TMR_CLOCK_SOURCE_IDX1           DRV_TMR_CLKSOURCE_INTERNAL
#define DRV_TMR_PRESCALE_IDX1               TMR_PRESCALE_VALUE_256
#define DRV_TMR_OPERATION_MODE_IDX1         DRV_TMR_OPERATION_MODE_32_BIT
#define DRV_TMR_ASYNC_WRITE_ENABLE_IDX1     false
#define DRV_TMR_POWER_STATE_IDX1            SYS_MODULE_POWER_RUN_FULL


// *****************************************************************************
// *****************************************************************************
// Section: Timer System Service Configuration
// *****************************************************************************
// *****************************************************************************
/* Total delay objects needed in the application*/
#define SYS_TMR_MAX_DELAY_EVENTS        	  3
/*** Timer System Service Configuration ***/

#define SYS_TMR_POWER_STATE             SYS_MODULE_POWER_RUN_FULL
#define SYS_TMR_DRIVER_INDEX            DRV_TMR_INDEX_0
#define SYS_TMR_MAX_CLIENT_OBJECTS      3
#define SYS_TMR_FREQUENCY               100
#define SYS_TMR_FREQUENCY_TOLERANCE     5
#define SYS_TMR_UNIT_RESOLUTION         1000
#define SYS_TMR_CLIENT_TOLERANCE        1
#define SYS_TMR_INTERRUPT_NOTIFICATION  true



// *****************************************************************************
// *****************************************************************************
// Section: Application Specific System Definitions
// *****************************************************************************

/* Peripheral Bus Clock frequency */
#define SYS_PBCLK_CLOCK_HZ                              (uint32_t) 48000000UL

/* BT Tick Timer1 configuration settings */
#define SYS_BT_TICK_TIMER_PRESCALE                      TMR_PRESCALE_VALUE_8
#define SYS_BT_TICK_TIMER_PRESCALE_VAL                  8
#define APP_BT_TICK_TIMER_MS                            10
#define SYS_BT_TICK_TIMER_RATE_HZ                       (uint32_t)100
#define SYS_BT_TICK_TIMER_PERIOD                        (uint32_t)(((SYS_PBCLK_CLOCK_HZ/SYS_BT_TICK_TIMER_PRESCALE_VAL) / SYS_BT_TICK_TIMER_RATE_HZ) - 1)

/* BT connection name */
#define APP_BT_SPP_CONNECTION_NAME                      "BTAD"

/* BT Repeat Timer configuration settings */
#define SYS_BT_BUTTON_REPEAT_TIMER_PRESCALE          TMR_PRESCALE_VALUE_256
#define SYS_BT_BUTTON_REPEAT_TIMER_INIT_PERIOD       (SYS_PBCLK_CLOCK_HZ/SYS_BT_BUTTON_REPEAT_TIMER_PRESCALE/2)
#define APP_BT_BUTTON_REPEAT_TIMER_INIT_PERIOD       SYS_BT_BUTTON_REPEAT_TIMER_INIT_PERIOD
#define APP_BT_BUTTON_REPEAT_TIMER_REPEAT_PERIOD     (SYS_PBCLK_CLOCK_HZ/SYS_BT_BUTTON_REPEAT_TIMER_PRESCALE/128)

/* DMA USART Tx Channel related Macroes */
#define SYS_BT_USART_TX_DMA_CHANNEL                     DMA_CHANNEL_0
/* DMA USART Rx Channel related Macroes */
#define SYS_BT_USART_RX_DMA_CHANNEL                     DMA_CHANNEL_3

/* BT USART Baud Rate Settings */

 // *****************************************************************************
/* USART Driver Configuration Options
*/

#define DRV_USART_INTERRUPT_MODE                    true
#define DRV_USART_BYTE_MODEL_SUPPORT                false
#define DRV_USART_READ_WRITE_MODEL_SUPPORT          false
#define DRV_USART_BUFFER_QUEUE_SUPPORT              true
#define DRV_USART_QUEUE_DEPTH_COMBINED              2
#define DRV_USART_QUEUE_DEPTH_COMBINED              2
#define DRV_USART_CLIENTS_NUMBER                    1
#define DRV_USART_SUPPORT_TRANSMIT_DMA              true
#define DRV_USART_SUPPORT_RECEIVE_DMA               true
#define DRV_USART_INSTANCES_NUMBER                  1

#define DRV_USART_PERIPHERAL_ID_IDX0                USART_ID_2
#define DRV_USART_OPER_MODE_IDX0                    DRV_USART_OPERATION_MODE_NORMAL
#define DRV_USART_OPER_MODE_DATA_IDX0               0x00
#define DRV_USART_INIT_FLAG_WAKE_ON_START_IDX0      false
#define DRV_USART_INIT_FLAG_AUTO_BAUD_IDX0          false
#define DRV_USART_INIT_FLAG_STOP_IN_IDLE_IDX0       false
#define DRV_USART_INIT_FLAGS_IDX0                   0
#define DRV_USART_BRG_CLOCK_IDX0                    48000000
#define DRV_USART_BAUD_RATE_IDX0                    115200
#define DRV_USART_LINE_CNTRL_IDX0                   DRV_USART_LINE_CONTROL_8NONE1
#define DRV_USART_HANDSHAKE_MODE_IDX0               DRV_USART_HANDSHAKE_FLOWCONTROL
#define DRV_USART_XMIT_INT_SRC_IDX0                 INT_SOURCE_USART_2_TRANSMIT
#define DRV_USART_RCV_INT_SRC_IDX0                  INT_SOURCE_USART_2_RECEIVE
#define DRV_USART_ERR_INT_SRC_IDX0                  INT_SOURCE_USART_2_ERROR
#define DRV_USART_XMIT_QUEUE_SIZE_IDX0              1
#define DRV_USART_RCV_QUEUE_SIZE_IDX0               1
#define DRV_USART_XMIT_DMA_CH_IDX0                  DMA_CHANNEL_0
#define DRV_USART_XMIT_DMA_INT_SRC_IDX0             INT_SOURCE_DMA_0
#define DRV_USART_RCV_DMA_CH_IDX0                   DMA_CHANNEL_3
#define DRV_USART_RCV_DMA_INT_SRC_IDX0              INT_SOURCE_DMA_3
#define DRV_USART_POWER_STATE_IDX0                  SYS_MODULE_POWER_RUN_FULL
#define SYS_BT_USART_BAUD_CLOCK                         (uint32_t) SYS_PBCLK_CLOCK_HZ
#define APP_BT_USART_BAUD_CLOCK                         (uint32_t) SYS_PBCLK_CLOCK_HZ
#define SYS_BT_USART_INITIAL_BAUD_RATE                  115200
#define APP_BT_USART_WORKING_BAUD_RATE                  4000000

/* BT Reset PORT settings */
#define APP_BT_RESET_PORT                               PORT_CHANNEL_G
#define APP_BT_RESET_BIT                                PORTS_BIT_POS_15

/* BT Buttons PORT Settings */
#define SYS_BT_SWITCH_1_PORT                            PORT_CHANNEL_A
#define SYS_BT_SWITCH_1_BIT                             PORTS_BIT_POS_0
#define SYS_BT_SWITCH_2_PORT                            PORT_CHANNEL_A
#define SYS_BT_SWITCH_2_BIT                             PORTS_BIT_POS_1
#define SYS_BT_SWITCH_3_PORT                            PORT_CHANNEL_A
#define SYS_BT_SWITCH_3_BIT                             PORTS_BIT_POS_10
#define SYS_BT_SWITCH_4_PORT                            PORT_CHANNEL_B
#define SYS_BT_SWITCH_4_BIT                             PORTS_BIT_POS_12
#define SYS_BT_SWITCH_5_PORT                            PORT_CHANNEL_B
#define SYS_BT_SWITCH_5_BIT                             PORTS_BIT_POS_13
#define SYS_BT_SWITCH_1TO3_PORT                         SYS_BT_SWITCH_1_PORT
#define SYS_BT_SWITCH_4TO5_PORT                         SYS_BT_SWITCH_4_PORT
#define APP_BUTTON1_PIN                                 (1<<SYS_BT_SWITCH_1_BIT)
#define APP_BUTTON2_PIN                                 (1<<SYS_BT_SWITCH_2_BIT)
#define APP_BUTTON3_PIN                                 (1<<SYS_BT_SWITCH_3_BIT)
#define APP_BUTTON4_PIN                                 (1<<SYS_BT_SWITCH_4_BIT)
#define APP_BUTTON5_PIN                                 (1<<SYS_BT_SWITCH_5_BIT)

/* USART PPS Settings */
    /* The USART module USART_2 is connected to the BT Radio */
    /* RTS pin(RG9/AN19) settings. USART RTS -> BT CTS */
    /* TX pin(RF5) settings. USART TX -> BT RX */
    /* RX pin(RF4) settings. USART RX <- BT TX */
    /* CTS pin(RB2) settings. USART CTS <- BT RTS */
#define SYS_BT_USART_RTS_PORT                           PORT_CHANNEL_G
#define SYS_BT_USART_RTS_BIT                            PORTS_BIT_POS_9
#define SYS_BT_USART_TX_PORT                            PORT_CHANNEL_F
#define SYS_BT_USART_TX_BIT                             PORTS_BIT_POS_5
#define SYS_BT_USART_RX_PORT                            PORT_CHANNEL_F
#define SYS_BT_USART_RX_BIT                             PORTS_BIT_POS_4
#define SYS_BT_USART_CTS_PORT                           PORT_CHANNEL_B
#define SYS_BT_USART_CTS_BIT                            PORTS_BIT_POS_2

/* PPS functions for USART pins */
#define SYS_BT_USART_RTS_FUNCTION                       OTPUT_FUNC_U2RTS
#define SYS_BT_USART_RTS_PIN                            OUTPUT_PIN_RPG9
#define SYS_BT_USART_TX_FUNCTION                        OTPUT_FUNC_U2TX
#define SYS_BT_USART_TX_PIN                             OUTPUT_PIN_RPF5
#define SYS_BT_USART_RX_FUNCTION                        INPUT_FUNC_U2RX
#define SYS_BT_USART_RX_PIN                             INPUT_PIN_RPF4
#define SYS_BT_USART_CTS_FUNCTION                       INPUT_FUNC_U2CTS
#define SYS_BT_USART_CTS_PIN                            INPUT_PIN_RPB2

/* Selecting Digital mode for Analog pins */
#define SYS_PINMODESELECT_BT_USART_RTS_PIN_DIGITAL()    SYS_PORTS_PinModeSelect(PORTS_ID_0,PORTS_ANALOG_PIN_19,PORTS_PIN_MODE_DIGITAL)
#define SYS_PINMODESELECT_BT_USART_TX_PIN_DIGITAL()
#define SYS_PINMODESELECT_BT_USART_RX_PIN_DIGITAL()
#define SYS_PINMODESELECT_BT_USART_CTS_PIN_DIGITAL()

/* Interrupt Priorties of System Components */
#define SYS_BT_TICK_TIMER_PRIO                          INT_PRIORITY_LEVEL1
#define SYS_BT_TICK_TIMER_SUBPRIO                       INT_SUBPRIORITY_LEVEL0
#define SYS_BT_BUTTON_REPEAT_TIMER_PRIO                 INT_PRIORITY_LEVEL3
#define SYS_BT_BUTTON_REPEAT_TIMER_SUBPRIO              INT_SUBPRIORITY_LEVEL0
#define SYS_BT_BUTTON_CN_PRIO                           INT_PRIORITY_LEVEL2
#define SYS_BT_BUTTON_CN_SUBPRIO                        INT_SUBPRIORITY_LEVEL0
#define SYS_BT_USART_RX_DMA_CHANNEL_PRIO                INT_PRIORITY_LEVEL4
#define SYS_BT_USART_RX_DMA_CHANNEL_SUBPRIO             INT_SUBPRIORITY_LEVEL1
#define SYS_BT_USART_TX_DMA_CHANNEL_PRIO                INT_PRIORITY_LEVEL5
#define SYS_BT_USART_TX_DMA_CHANNEL_SUBPRIO             INT_SUBPRIORITY_LEVEL0

/* Interrupt Vectors of System Components */
#define SYS_BT_TICK_TIMER_VECTOR                        INT_VECTOR_T1
#define SYS_BT_BUTTON_REPEAT_TIMER_VECTOR               INT_VECTOR_T3
#define SYS_BT_BUTTON_CN_VECTOR                         INT_VECTOR_CHANGE_NOTICE_A
#define SYS_BT_USART_RX_DMA_CHANNEL_VECTOR              INT_VECTOR_DMA3
#define SYS_BT_USART_TX_DMA_CHANNEL_VECTOR              INT_VECTOR_DMA0

/* Interrupt Sources of System Components */
#define SYS_BT_TICK_TIMER_SOURCE                        INT_SOURCE_TIMER_1
#define SYS_BT_BUTTON_REPEAT_TIMER_SOURCE               INT_SOURCE_TIMER_3
#define SYS_BT_BUTTON_CN_SOURCE_1                       INT_SOURCE_CHANGE_NOTICE_A
#define SYS_BT_BUTTON_CN_SOURCE_2                       INT_SOURCE_CHANGE_NOTICE_B
#define SYS_BT_USART_RX_DMA_CHANNEL_SOURCE              INT_SOURCE_DMA_3
#define SYS_BT_USART_TX_DMA_CHANNEL_SOURCE              INT_SOURCE_DMA_0
#define SYS_BT_USART_RX_DMA_TRIGGER_SOURCE              INT_SOURCE_USART_2_RECEIVE
#define SYS_BT_USART_TX_DMA_TRIGGER_SOURCE              INT_SOURCE_USART_2_TRANSMIT

/* Interrupt ISR of System Components */
#define SYS_BT_TICK_TIMER_ISR                           _TIMER_1_VECTOR
#define SYS_BT_BUTTON_REPEAT_TIMER_ISR                  _TIMER_3_VECTOR
#define SYS_BT_BUTTON_CN_ISR                            _CHANGE_NOTICE_VECTOR
#define SYS_BT_USART_RX_DMA_CHANNEL_ISR                 _DMA3_VECTOR
#define SYS_BT_USART_TX_DMA_CHANNEL_ISR                 _DMA0_VECTOR

/* Application LED's */
#define APP_LED1_ON()                                   BSP_LEDOn(BSP_LED_5)
#define APP_LED1_OFF()                                  BSP_LEDOff(BSP_LED_5)
#define APP_LED1_TOGGLE()                               BSP_LEDToggle(BSP_LED_5)
#define APP_LED2_ON()                                   BSP_LEDOn(BSP_LED_6)
#define APP_LED2_OFF()                                  BSP_LEDOff(BSP_LED_6)
#define APP_LED2_TOGGLE()                               BSP_LEDToggle(BSP_LED_6)
#define APP_LED3_ON()                                   BSP_LEDOn(BSP_LED_7)
#define APP_LED3_OFF()                                  BSP_LEDOff(BSP_LED_7)
#define APP_LED3_TOGGLE()                               BSP_LEDToggle(BSP_LED_7)
#define APP_LED4_ON()                                   BSP_LEDOn(BSP_LED_8)
#define APP_LED4_OFF()                                  BSP_LEDOff(BSP_LED_8)
#define APP_LED4_TOGGLE()                               BSP_LEDToggle(BSP_LED_8)
#define APP_LED5_ON()                                   BSP_LEDOn(BSP_LED_9)
#define APP_LED5_OFF()                                  BSP_LEDOff(BSP_LED_9)
#define APP_LED5_TOGGLE()                               BSP_LEDToggle(BSP_LED_9)

/* App Buttons */
#define APP_READ_BUTTON_PORTS()                         (SYS_PORTS_Read(PORTS_ID_0,SYS_BT_SWITCH_1TO3_PORT)| SYS_PORTS_Read(PORTS_ID_0,SYS_BT_SWITCH_4TO5_PORT))
#define APP_ENABLE_BUTTON_CHANGE_NOTICE()               PLIB_PORTS_PinChangeNoticePerPortEnable(PORTS_ID_0, SYS_BT_SWITCH_1_PORT, SYS_BT_SWITCH_1_BIT);\
                                                        PLIB_PORTS_PinChangeNoticePerPortEnable(PORTS_ID_0, SYS_BT_SWITCH_2_PORT, SYS_BT_SWITCH_2_BIT);\
                                                        PLIB_PORTS_PinChangeNoticePerPortEnable(PORTS_ID_0, SYS_BT_SWITCH_3_PORT, SYS_BT_SWITCH_3_BIT);\
                                                        PLIB_PORTS_PinChangeNoticePerPortEnable(PORTS_ID_0, SYS_BT_SWITCH_4_PORT, SYS_BT_SWITCH_4_BIT);\
                                                        PLIB_PORTS_PinChangeNoticePerPortEnable(PORTS_ID_0, SYS_BT_SWITCH_5_PORT, SYS_BT_SWITCH_5_BIT)

#endif
