/*******************************************************************************
 System Initialization file

  Company:
    Microchip Technology Inc.

  File Name:
    system_init.c

  Summary:
    This file contains definition of the system initialization procedure.

  Description:
    This file contains definition of the system initialization procedure.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
******************************************************************************/
//DOM-IGNORE-END

#include "app.h"
#include "system_definitions.h"

// ****************************************************************************
// ****************************************************************************
// Section: Configuration Bits
// ****************************************************************************
// ****************************************************************************
/*
 * Application is setup for MEB-II board with MZ starter kit PIC32mZ2048ECM144 part.
 * The PIC32MZ is configured to operate at 200MHz using primary oscillator.
 */
// ****************************************************************************
// ****************************************************************************
// Section: Configuration Bits
// ****************************************************************************
// ****************************************************************************

///*** DEVCFG0 ***/
//
#pragma config DEBUG =      OFF
#pragma config JTAGEN =     OFF
#pragma config ICESEL =     ICS_PGx2
#pragma config TRCEN =      OFF
#pragma config BOOTISA =    MIPS32
#pragma config FECCCON =    OFF_UNLOCKED
#pragma config FSLEEP =     OFF
#pragma config DBGPER =     PG_ALL
#pragma config EJTAGBEN =   NORMAL
#pragma config CP =         OFF
//
///*** DEVCFG1 ***/
//
#pragma config FNOSC =      SPLL
#pragma config DMTINTV =    WIN_0
#pragma config FSOSCEN =    OFF
#pragma config IESO =       OFF
#pragma config POSCMOD =    EC
#pragma config OSCIOFNC =   OFF
#pragma config FCKSM =      CSDCMD
#pragma config WDTPS =      PS1048576
#pragma config WDTSPGM =    STOP
#pragma config FWDTEN =     OFF
#pragma config WINDIS =     NORMAL
#pragma config FWDTWINSZ =  WINSZ_25
#pragma config DMTCNT =     DMT31
#pragma config FDMTEN =     OFF
//
///*** DEVCFG2 ***/
//
#pragma config FPLLIDIV =   DIV_3
#pragma config FPLLRNG =    RANGE_5_10_MHZ
#pragma config FPLLICLK =   PLL_POSC
#pragma config FPLLMULT =   MUL_50
#pragma config FPLLODIV =   DIV_2
#pragma config UPLLFSEL =   FREQ_24MHZ
#pragma config UPLLEN =     ON
//
///*** DEVCFG3 ***/
//
#pragma config USERID =     0xffff
#pragma config FMIIEN =     OFF
#pragma config FETHIO =     OFF
#pragma config PGL1WAY =    OFF
#pragma config PMDL1WAY =   OFF
#pragma config IOL1WAY =    OFF
#pragma config FUSBIDIO =   OFF



/****************************************************
 * System Module Objects.
 ****************************************************/

SYSTEM_OBJECTS sysObj;

/*************************************************
 * Clock System Service Initialization
 ************************************************/
SYS_CLK_INIT sysClkInit =
{
    .moduleInit = {0},
    .systemClockSource = SYS_CLK_SOURCE,
    .systemClockFrequencyHz = SYS_CLK_FREQ,
    .waitTillComplete = true,
    .secondaryOscKeepEnabled = true,
    .onWaitInstruction = SYS_CLK_ON_WAIT
};

/*************************************************
 * DEVCON System Service Initialization
 ************************************************/

SYS_DEVCON_INIT devconInit =
{
    .moduleInit = 0
};


/*************************************************
 * Timer1 Driver Initialization
 ************************************************/

DRV_TMR_INIT drvTmr0InitData =
{
    .moduleInit = DRV_TMR_POWER_STATE_IDX0,
    .tmrId = DRV_TMR_PERIPHERAL_ID_IDX0,
    .clockSource = DRV_TMR_CLOCK_SOURCE_IDX0,
    .prescale = DRV_TMR_PRESCALE_IDX0,
    .mode = DRV_TMR_OPERATION_MODE_IDX0,
    .interruptSource = DRV_TMR_INTERRUPT_SOURCE_IDX0,
    .asyncWriteEnable = false
};

/*************************************************
 * Timer2/3 Driver Initialization
 ************************************************/
DRV_TMR_INIT drvTmr1InitData =
{
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .tmrId            = TMR_ID_2,
    .clockSource      = DRV_TMR_CLKSOURCE_INTERNAL,
    .prescale         = SYS_BT_BUTTON_REPEAT_TIMER_PRESCALE,
    .mode             = DRV_TMR_OPERATION_MODE_32_BIT,
    .interruptSource  = SYS_BT_BUTTON_REPEAT_TIMER_SOURCE
};

/*************************************************
 * USART2 Driver Initialization
 ************************************************/
DRV_USART_INIT usart2Init =
{
    .moduleInit = DRV_USART_POWER_STATE_IDX0,
    .usartID = DRV_USART_PERIPHERAL_ID_IDX0,
    .mode = DRV_USART_OPER_MODE_IDX0,
    .modeData = DRV_USART_OPER_MODE_DATA_IDX0,
    .flags = DRV_USART_INIT_FLAGS_IDX0,
    .brgClock = DRV_USART_BRG_CLOCK_IDX0,
    .lineControl = DRV_USART_LINE_CNTRL_IDX0,
    .baud = DRV_USART_BAUD_RATE_IDX0,
    .handshake = DRV_USART_HANDSHAKE_MODE_IDX0,
    .interruptTransmit = DRV_USART_XMIT_INT_SRC_IDX0,
    .interruptReceive = DRV_USART_RCV_INT_SRC_IDX0,
    .queueSizeTransmit = DRV_USART_XMIT_QUEUE_SIZE_IDX0,
    .queueSizeReceive = DRV_USART_RCV_QUEUE_SIZE_IDX0,
    .dmaChannelTransmit = DRV_USART_XMIT_DMA_CH_IDX0,
    .dmaChannelReceive = DRV_USART_RCV_DMA_CH_IDX0,
    .dmaInterruptTransmit = DRV_USART_XMIT_DMA_INT_SRC_IDX0,
    .dmaInterruptReceive = DRV_USART_RCV_DMA_INT_SRC_IDX0
};

/*************************************************
 * System Timer Service Initialization
 ************************************************/
SYS_TMR_INIT sysTmrInit =
{
    .moduleInit = {SYS_MODULE_POWER_RUN_FULL},
    .drvIndex = DRV_TMR_INDEX_0,
    .tmrFreq = 100
};

/*************************************************
 * System DMA Service Initialization
 ************************************************/
SYS_DMA_INIT sysDmaInit =
{
    .sidl = SYS_DMA_SIDL_DISABLE
};


// ****************************************************************************
// ****************************************************************************
// Section: System Initialization
// ****************************************************************************
// ****************************************************************************

/*******************************************************************************
  Function:
    void SYS_Initialize ( void * data )

  Summary:
    Initializes the board, services, drivers, application and other modules

  Description:
    This routine initializes the board, services, drivers, application and other
    modules as configured at build time.  In a bare-metal environment (where no
    OS is supported), this routine should be called almost immediately after
    entering the "main" routine.

  Remarks:

*/
void SYS_Initialize(void * data)
{
    /* Set up cache and wait states for
     * maximum performance. */
    SYS_DEVCON_Initialize(SYS_DEVCON_INDEX_0, (SYS_MODULE_INIT *)&devconInit);
    SYS_DEVCON_PerformanceConfig(sysClkInit.systemClockFrequencyHz);

    /* Intialize the System Clock Service */
    SYS_CLK_Initialize(NULL);
    if(SYS_CLK_SystemFrequencyGet()  != sysClkInit.systemClockFrequencyHz)
    {
        SYS_DEBUG(0, "Something wrong System Clock, Check the Configuration bit settings");
    }
    if(SYS_PBCLK2_CLOCK_HZ != SYS_CLK_PeripheralFrequencySet(
       CLK_BUS_PERIPHERAL_2,CLK_SOURCE_PERIPHERAL_SYSTEMCLK,SYS_PBCLK2_CLOCK_HZ,true))
    {
        SYS_DEBUG(0, "Something wrong PBCLK2, Check the Configuration bit settings");
    }
    if(SYS_PBCLK3_CLOCK_HZ != SYS_CLK_PeripheralFrequencySet(
       CLK_BUS_PERIPHERAL_3,CLK_SOURCE_PERIPHERAL_SYSTEMCLK,SYS_PBCLK3_CLOCK_HZ,true))
    {
        SYS_DEBUG(0, "Something wrong PBCLK3, Check the Configuration bit settings");
    }

    /* Initialize the System interrupt dervice  */
    SYS_INT_Initialize();

    /* Initialize the BSP */
    BSP_Initialize();

    /* Initialize the PPS for USART module */
    /* Note: Eventually the settings below would be done by the driver. Since the driver as of now
             does not support this settings, it is initialized below */
    SYS_PORTS_RemapOutput(PORTS_ID_0,SYS_BT_USART_RTS_FUNCTION, SYS_BT_USART_RTS_PIN);
    SYS_PINMODESELECT_BT_USART_RTS_PIN_DIGITAL();
    SYS_PORTS_DirectionSelect(PORTS_ID_0, SYS_PORTS_DIRECTION_OUTPUT,SYS_BT_USART_RTS_PORT, (1<<SYS_BT_USART_RTS_BIT));
    SYS_PORTS_PinSet(PORTS_ID_0, SYS_BT_USART_RTS_PORT, SYS_BT_USART_RTS_BIT);
    SYS_PORTS_RemapOutput(PORTS_ID_0,SYS_BT_USART_TX_FUNCTION,SYS_BT_USART_TX_PIN);
    SYS_PINMODESELECT_BT_USART_TX_PIN_DIGITAL();
    SYS_PORTS_DirectionSelect(PORTS_ID_0, SYS_PORTS_DIRECTION_OUTPUT,SYS_BT_USART_TX_PORT, (1<<SYS_BT_USART_TX_BIT));
    SYS_PORTS_PinSet(PORTS_ID_0, SYS_BT_USART_TX_PORT, SYS_BT_USART_TX_BIT);
    SYS_PORTS_RemapInput(PORTS_ID_0,SYS_BT_USART_RX_FUNCTION,SYS_BT_USART_RX_PIN);
    SYS_PINMODESELECT_BT_USART_RX_PIN_DIGITAL();
    SYS_PORTS_DirectionSelect(PORTS_ID_0, SYS_PORTS_DIRECTION_INPUT, SYS_BT_USART_RX_PORT, (1<<SYS_BT_USART_RX_BIT));
    SYS_PORTS_RemapInput(PORTS_ID_0,SYS_BT_USART_CTS_FUNCTION,SYS_BT_USART_CTS_PIN);
    SYS_PINMODESELECT_BT_USART_CTS_PIN_DIGITAL();
    SYS_PORTS_DirectionSelect(PORTS_ID_0, SYS_PORTS_DIRECTION_INPUT, SYS_BT_USART_CTS_PORT, (1<<SYS_BT_USART_CTS_BIT));

    /* Application specific Initialization */
    APP_Initialize();

    /* Initialize the System DMA Service */
    sysObj.sysDma = SYS_DMA_Initialize((SYS_MODULE_INIT*)&sysDmaInit);
    /* Intialize the USART2 Driver */
    sysObj.drvUsart0 = DRV_USART_Initialize(DRV_USART_INDEX_0,
            (SYS_MODULE_INIT*)&usart2Init);

    /* Intialize the Timer1 Driver */
    sysObj.drvTmr0 = DRV_TMR_Initialize(DRV_TMR_INDEX_0,
            (SYS_MODULE_INIT *) &drvTmr0InitData);

    /* Intialize the System Timer Service. Associate with Timer1 Driver object */
    sysObj.sysTmr = SYS_TMR_Initialize(SYS_TMR_INDEX_0,
            (SYS_MODULE_INIT *) &sysTmrInit);

    /* Intialize the Timer2/3 Driver */
    sysObj.drvTmr1 = DRV_TMR_Initialize(DRV_TMR_INDEX_1,
            (SYS_MODULE_INIT *) &drvTmr1InitData);


    /* Set priority for Timer 1 interrupt source */
    SYS_INT_VectorPrioritySet(SYS_BT_TICK_TIMER_VECTOR, SYS_BT_TICK_TIMER_PRIO);
    /* Set sub-priority for Timer 1 interrupt source */
    SYS_INT_VectorSubprioritySet(SYS_BT_TICK_TIMER_VECTOR, SYS_BT_TICK_TIMER_SUBPRIO);
    /* Set priority for TX DMA interrupt source */
    SYS_INT_VectorPrioritySet(SYS_BT_USART_TX_DMA_CHANNEL_VECTOR, SYS_BT_USART_TX_DMA_CHANNEL_PRIO);
    /* Set sub-priority for TX DMA interrupt source */
    SYS_INT_VectorSubprioritySet(SYS_BT_USART_TX_DMA_CHANNEL_VECTOR, SYS_BT_USART_TX_DMA_CHANNEL_SUBPRIO);
    /* Set priority for RX DMA interrupt source */
    SYS_INT_VectorPrioritySet(SYS_BT_USART_RX_DMA_CHANNEL_VECTOR, SYS_BT_USART_RX_DMA_CHANNEL_PRIO);
    /* Set sub-priority for RX DMA interrupt source */
    SYS_INT_VectorSubprioritySet(SYS_BT_USART_RX_DMA_CHANNEL_VECTOR, SYS_BT_USART_RX_DMA_CHANNEL_SUBPRIO);
     /* Set priority for Timer 3 interrupt source */
    SYS_INT_VectorPrioritySet(SYS_BT_BUTTON_REPEAT_TIMER_VECTOR, SYS_BT_BUTTON_REPEAT_TIMER_PRIO);
    /* Set sub-priority for Timer 3 interrupt source */
    SYS_INT_VectorSubprioritySet(SYS_BT_BUTTON_REPEAT_TIMER_VECTOR, SYS_BT_BUTTON_REPEAT_TIMER_SUBPRIO);
    /* Set priority for Change notice interrupt source */
    SYS_INT_VectorPrioritySet(SYS_BT_BUTTON_CN_VECTOR, SYS_BT_BUTTON_CN_PRIO);
    /* Set sub-priority Change notice interrupt source */
    SYS_INT_VectorSubprioritySet(SYS_BT_BUTTON_CN_VECTOR, SYS_BT_BUTTON_CN_SUBPRIO);
    /* Enable the CN interrupt at the interrupt controller level */
    SYS_INT_SourceEnable(SYS_BT_BUTTON_CN_SOURCE);

    APP_CHANGE_NOTICE_ON();
    APP_ENABLE_PULL_UP();

    SYS_INT_Enable();
}
/*******************************************************************************
 End of File
 */