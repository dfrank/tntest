/*******************************************************************************
 System Initialization file

  Company:
    Microchip Technology Inc.

  File Name:
    system_init.c

  Summary:
    This file contains definition of the system initialization procedure.

  Description:
    This file contains definition of the system initialization procedure.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
******************************************************************************/
//DOM-IGNORE-END

#include "app.h"
#include "system_definitions.h"

// ****************************************************************************
// ****************************************************************************
// Section: Configuration Bits
// ****************************************************************************
// ****************************************************************************
// Configuration Bit settings
// SYSCLK = 96 MHz (12MHz Crystal/ FPLLIDIV * FPLLMUL / FPLLODIV)
// PBCLK = 48 MHz
// Primary Osc w/PLL (XT+,HS+,EC+PLL)
// WDT OFF
// Peripheral Pin Select Configuration (Allow multiple reconfigurations)
//
#pragma config FPLLMUL = MUL_24
#pragma config FPLLIDIV = DIV_3
#pragma config FPLLODIV = DIV_1
#pragma config FWDTEN = OFF
#pragma config POSCMOD = HS
#pragma config FNOSC = PRIPLL
#pragma config FPBDIV = DIV_2
#pragma config UPLLEN = ON
#pragma config UPLLIDIV = DIV_3
#pragma config FUSBIDIO = OFF           // USB USID Selection (Controlled by Port Function)
#pragma config FVBUSONIO = OFF          // USB VBUS ON Selection (Controlled by Port Function)
#pragma config DEBUG = ON               // Background Debugger Enable (Debugger is enabled)
#pragma config ICESEL = ICS_PGx2        // ICE/ICD Comm Channel Select (ICE EMUC2/EMUD2 pins shared with PGC2/PGD2)
#pragma config PWP = OFF                // Program Flash Write Protect (Disable)
#pragma config BWP = OFF                // Boot Flash Write Protect bit (Protection Disabled)
#pragma config CP = OFF                 // Code Protect (Protection Disabled)


/****************************************************
 * System Module Objects.
 ****************************************************/

SYSTEM_OBJECTS sysObj;

/*************************************************
 * Clock System Service Initialization
 ************************************************/
SYS_CLK_INIT sysClkInit =
{
    .moduleInit = {0},
    .systemClockSource = SYS_CLK_SOURCE,
    .systemClockFrequencyHz = SYS_CLK_FREQ,
    .waitTillComplete = true,
    .secondaryOscKeepEnabled = true,
    .onWaitInstruction = SYS_CLK_ON_WAIT
};

/*************************************************
 * DEVCON System Service Initialization
 ************************************************/

SYS_DEVCON_INIT devconInit =
{
    .moduleInit = 0
};


/*************************************************
 * Timer1 Driver Initialization
 ************************************************/

DRV_TMR_INIT drvTmr0InitData =
{
    .moduleInit = DRV_TMR_POWER_STATE_IDX0,
    .tmrId = DRV_TMR_PERIPHERAL_ID_IDX0,
    .clockSource = DRV_TMR_CLOCK_SOURCE_IDX0,
    .prescale = DRV_TMR_PRESCALE_IDX0,
    .mode = DRV_TMR_OPERATION_MODE_IDX0,
    .interruptSource = DRV_TMR_INTERRUPT_SOURCE_IDX0
};

/*************************************************
 * Timer2/3 Driver Initialization
 ************************************************/

DRV_TMR_INIT drvTmr1InitData =
{
    .moduleInit = DRV_TMR_POWER_STATE_IDX1,
    .tmrId = DRV_TMR_PERIPHERAL_ID_IDX1,
    .clockSource = DRV_TMR_CLOCK_SOURCE_IDX1,
    .prescale = DRV_TMR_PRESCALE_IDX1,
    .mode = DRV_TMR_OPERATION_MODE_IDX1,
    .interruptSource = DRV_TMR_INTERRUPT_SOURCE_IDX1
};

/*************************************************
 * USART2 Driver Initialization
 ************************************************/
DRV_USART_INIT usart2Init =
{
    .moduleInit = DRV_USART_POWER_STATE_IDX0,
    .usartID = DRV_USART_PERIPHERAL_ID_IDX0,
    .mode = DRV_USART_OPER_MODE_IDX0,
    .modeData = DRV_USART_OPER_MODE_DATA_IDX0,
    .flags = DRV_USART_INIT_FLAGS_IDX0,
    .brgClock = DRV_USART_BRG_CLOCK_IDX0,
    .lineControl = DRV_USART_LINE_CNTRL_IDX0,
    .baud = DRV_USART_BAUD_RATE_IDX0,
    .handshake = DRV_USART_HANDSHAKE_MODE_IDX0,
    .interruptTransmit = DRV_USART_XMIT_INT_SRC_IDX0,
    .interruptReceive = DRV_USART_RCV_INT_SRC_IDX0,
    .queueSizeTransmit = DRV_USART_XMIT_QUEUE_SIZE_IDX0,
    .queueSizeReceive = DRV_USART_RCV_QUEUE_SIZE_IDX0,
    .dmaChannelTransmit = DRV_USART_XMIT_DMA_CH_IDX0,
    .dmaChannelReceive = DRV_USART_RCV_DMA_CH_IDX0,
    .dmaInterruptTransmit = DRV_USART_XMIT_DMA_INT_SRC_IDX0,
    .dmaInterruptReceive = DRV_USART_RCV_DMA_INT_SRC_IDX0
};

/*************************************************
 * System Timer Service Initialization
 ************************************************/
SYS_TMR_INIT sysTmrInit =
{
    .moduleInit = {SYS_MODULE_POWER_RUN_FULL},
    .drvIndex = DRV_TMR_INDEX_0,
    .tmrFreq = 100
};

/*************************************************
 * System DMA Service Initialization
 ************************************************/
SYS_DMA_INIT sysDmaInit =
{
    .sidl = SYS_DMA_SIDL_DISABLE
};


// ****************************************************************************
// ****************************************************************************
// Section: System Initialization
// ****************************************************************************
// ****************************************************************************

/*******************************************************************************
  Function:
    void SYS_Initialize ( void * data )

  Summary:
    Initializes the board, services, drivers, application and other modules

  Description:
    This routine initializes the board, services, drivers, application and other
    modules as configured at build time.  In a bare-metal environment (where no
    OS is supported), this routine should be called almost immediately after
    entering the "main" routine.

  Remarks:

*/
void SYS_Initialize(void * data)
{
       SYS_CLK_Initialize(NULL);
    sysObj.sysDevcon = SYS_DEVCON_Initialize(SYS_DEVCON_INDEX_0, NULL);
    SYS_DEVCON_PerformanceConfig(96000000);

    /* Board Support Package Initialization */
    BSP_Initialize();

    /* System Services Initialization */
    SYS_INT_Initialize();

    /* Initialize the PPS for USART module */
    /* Note: Eventually the settings below would be done by the driver. Since the driver as of now
             does not support this settings, it is initialized below */
    SYS_PORTS_RemapOutput(PORTS_ID_0,SYS_BT_USART_RTS_FUNCTION, SYS_BT_USART_RTS_PIN);
    SYS_PINMODESELECT_BT_USART_RTS_PIN_DIGITAL();
    SYS_PORTS_DirectionSelect(PORTS_ID_0, SYS_PORTS_DIRECTION_OUTPUT,SYS_BT_USART_RTS_PORT, (1<<SYS_BT_USART_RTS_BIT));
    SYS_PORTS_PinSet(PORTS_ID_0, SYS_BT_USART_RTS_PORT, SYS_BT_USART_RTS_BIT);
    SYS_PORTS_RemapOutput(PORTS_ID_0,SYS_BT_USART_TX_FUNCTION,SYS_BT_USART_TX_PIN);
    SYS_PINMODESELECT_BT_USART_TX_PIN_DIGITAL();
    SYS_PORTS_DirectionSelect(PORTS_ID_0, SYS_PORTS_DIRECTION_OUTPUT,SYS_BT_USART_TX_PORT, (1<<SYS_BT_USART_TX_BIT));
    SYS_PORTS_PinSet(PORTS_ID_0, SYS_BT_USART_TX_PORT, SYS_BT_USART_TX_BIT);
    SYS_PORTS_RemapInput(PORTS_ID_0,SYS_BT_USART_RX_FUNCTION,SYS_BT_USART_RX_PIN);
    SYS_PINMODESELECT_BT_USART_RX_PIN_DIGITAL();
    SYS_PORTS_DirectionSelect(PORTS_ID_0, SYS_PORTS_DIRECTION_INPUT, SYS_BT_USART_RX_PORT, (1<<SYS_BT_USART_RX_BIT));
    SYS_PORTS_RemapInput(PORTS_ID_0,SYS_BT_USART_CTS_FUNCTION,SYS_BT_USART_CTS_PIN);
    SYS_PINMODESELECT_BT_USART_CTS_PIN_DIGITAL();
    SYS_PORTS_DirectionSelect(PORTS_ID_0, SYS_PORTS_DIRECTION_INPUT, SYS_BT_USART_CTS_PORT, (1<<SYS_BT_USART_CTS_BIT));

    /* Initialize Drivers */
    sysObj.drvTmr0 = DRV_TMR_Initialize(DRV_TMR_INDEX_0, (SYS_MODULE_INIT *)&drvTmr0InitData);
    sysObj.drvTmr1 = DRV_TMR_Initialize(DRV_TMR_INDEX_1, (SYS_MODULE_INIT *)&drvTmr1InitData);

    SYS_INT_VectorPrioritySet(INT_VECTOR_T1, INT_PRIORITY_LEVEL1);
    SYS_INT_VectorSubprioritySet(INT_VECTOR_T1, INT_SUBPRIORITY_LEVEL0);
    SYS_INT_VectorPrioritySet(INT_VECTOR_T3, INT_PRIORITY_LEVEL3);
    SYS_INT_VectorSubprioritySet(INT_VECTOR_T3, INT_SUBPRIORITY_LEVEL0);

    sysObj.drvUsart0 = DRV_USART_Initialize(DRV_USART_INDEX_0, (SYS_MODULE_INIT *)&usart2Init);

    /* Initialize System Services */
    sysObj.sysDma = SYS_DMA_Initialize((SYS_MODULE_INIT *)&sysDmaInit);

    SYS_INT_VectorPrioritySet(INT_VECTOR_DMA0, INT_PRIORITY_LEVEL5);
    SYS_INT_VectorSubprioritySet(INT_VECTOR_DMA0, INT_SUBPRIORITY_LEVEL0);
    SYS_INT_VectorPrioritySet(INT_VECTOR_DMA3, INT_PRIORITY_LEVEL4);
    SYS_INT_VectorSubprioritySet(INT_VECTOR_DMA3, INT_SUBPRIORITY_LEVEL1);
    /* Set priority for Change notice interrupt source */
    SYS_INT_VectorPrioritySet(SYS_BT_BUTTON_CN_VECTOR, SYS_BT_BUTTON_CN_PRIO);
    /* Set sub-priority Change notice interrupt source */
    SYS_INT_VectorSubprioritySet(SYS_BT_BUTTON_CN_VECTOR, SYS_BT_BUTTON_CN_SUBPRIO);
    /* Enable the CN interrupt at the interrupt controller level */
    SYS_INT_SourceEnable(SYS_BT_BUTTON_CN_SOURCE_1);
    SYS_INT_SourceEnable(SYS_BT_BUTTON_CN_SOURCE_2);

    SYS_INT_SourceEnable(INT_SOURCE_DMA_0);
    SYS_INT_SourceEnable(INT_SOURCE_DMA_3);



    /*** TMR Service Initialization Code ***/

    sysObj.sysTmr  = SYS_TMR_Initialize(SYS_TMR_INDEX_0, (const SYS_MODULE_INIT  * const)&sysTmrInit);




    /* Initialize Middleware */

    /* Enable Global Interrupts */
    SYS_INT_Enable();

    /* Initialize the Application */
    APP_Initialize();
}
/*******************************************************************************
 End of File
 */