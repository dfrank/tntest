/*******************************************************************************
 System Interrupts File

  File Name:
    system_int.c

  Summary:
    Raw ISR definitions.

  Description:
    This file contains a definitions of the raw ISRs required to support the
    interrupt sub-system.

  Summary:
    This file contains source code for the interrupt vector functions in the
    system.

  Description:
    This file contains source code for the interrupt vector functions in the
    system.  It implements the system and part specific vector "stub" functions
    from which the individual "Tasks" functions are called for any modules
    executing interrupt-driven in the MPLAB Harmony system.

  Remarks:
    This file requires access to the systemObjects global data structure that
    contains the object handles to all MPLAB Harmony module objects executing
    interrupt-driven in the system.  These handles are passed into the individual
    module "Tasks" functions to identify the instance of the module to maintain.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2011-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <xc.h>
#include <sys/attribs.h>
#include "app.h"
#include "system_definitions.h"


// *****************************************************************************
// *****************************************************************************
// Section: System Interrupt Vector Functions
// *****************************************************************************
// *****************************************************************************
void __ISR(SYS_BT_TICK_TIMER_ISR,ipl1) _InterruptHandler_BT_TICK_TIMER(void)
{
    APP_LED1_TOGGLE();
    DRV_TMR_Tasks_ISR(sysObj.drvTmr0);
}
void __ISR(SYS_BT_BUTTON_REPEAT_TIMER_ISR,ipl3) _InterruptHandler_BT_BUTTON_REPEAT_TIMER(void)
{
    DRV_TMR_Tasks_ISR(sysObj.drvTmr1);
}
void __ISR(SYS_BT_USART_TX_DMA_CHANNEL_ISR,ipl5) _InterruptHandler_BT_USART_TX_DMA_CHANNEL(void)
{
	SYS_DMA_TasksISR(sysObj.sysDma, DMA_CHANNEL_0);
        SYS_DMA_TasksErrorISR(sysObj.sysDma, DMA_CHANNEL_0);
}
void __ISR(SYS_BT_USART_RX_DMA_CHANNEL_ISR, ipl4) _InterruptHandler_BT_USART_RX_DMA_CHANNEL(void)
{
        SYS_DMA_TasksISR(sysObj.sysDma, DMA_CHANNEL_3);
	SYS_DMA_TasksErrorISR(sysObj.sysDma, DMA_CHANNEL_3);
}

/* ISR for Change notice */
void __ISR(SYS_BT_BUTTON_CN_ISR,ipl2) _InterruptHandler_BT_BUTTON_CN_ISR(void)
{
    uint32_t portAState;

    // Read port to clear mismatch on change notice pin
    portAState = SYS_PORTS_Read(PORTS_ID_0, SYS_BT_SWITCH_S1_PORT) & APP_BUTTON1_PIN;
    // Clear the interrupt flag
    SYS_INT_SourceStatusClear(SYS_BT_BUTTON_CN_SOURCE);
    // Handle buttons interrupt
    buttons_handleInterrupt(portAState);
}



/*******************************************************************************
 End of File
*/

