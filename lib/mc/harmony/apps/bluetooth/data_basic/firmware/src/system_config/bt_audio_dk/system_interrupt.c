/*******************************************************************************
 System Interrupt file

  Company:
    Microchip Technology Inc.

  File Name:
    system_interrupt.c

  Summary:
    This file contains the raw ISR defintions for this configuration.

  Description:
    This file contains the raw ISR defintions for this configuration.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
******************************************************************************/
//DOM-IGNORE-END

#include <xc.h>
#include <sys/attribs.h>
#include "app.h"
#include "system_definitions.h"

/* ISR for BT Tick Timer */
void __ISR(_TIMER_1_VECTOR, ipl1) _IntHandlerDrvTmrInstance0(void)
{
    DRV_TMR_Tasks_ISR(sysObj.drvTmr0);
}

/* ISR for USART RX DMA Channel */
void __ISR(_DMA3_VECTOR, ipl4) _IntHandlerSysDmaCh3(void)
{
    SYS_DMA_TasksISR(sysObj.sysDma, SYS_BT_USART_RX_DMA_CHANNEL);
    SYS_DMA_TasksErrorISR(sysObj.sysDma, SYS_BT_USART_RX_DMA_CHANNEL);
}

/* ISR for USART TX DMA Channel */
  void __ISR(_DMA0_VECTOR, ipl5) _IntHandlerSysDmaCh0(void)
{
    SYS_DMA_TasksISR(sysObj.sysDma, SYS_BT_USART_TX_DMA_CHANNEL);
    SYS_DMA_TasksErrorISR(sysObj.sysDma, SYS_BT_USART_TX_DMA_CHANNEL);
}

/* ISR for Change notice */
void __ISR(SYS_BT_BUTTON_CN_ISR,ipl2) _InterruptHandler_BT_BUTTON_CN_ISR(void)
{
    if(true == SYS_INT_SourceStatusGet(SYS_BT_BUTTON_CN_SOURCE_1) ||
            true == SYS_INT_SourceStatusGet(SYS_BT_BUTTON_CN_SOURCE_2))
    {
        uint32_t portAState=0;
        uint32_t portBState=0;
        // Read port to clear mismatch on change notice pin
        portAState = (SYS_PORTS_Read(PORTS_ID_0, SYS_BT_SWITCH_1TO3_PORT) & (APP_BUTTON1_PIN | APP_BUTTON2_PIN | APP_BUTTON3_PIN));
        portBState = (SYS_PORTS_Read(PORTS_ID_0, SYS_BT_SWITCH_4TO5_PORT) & (APP_BUTTON4_PIN | APP_BUTTON5_PIN));
        // Clear the interrupt flag
        SYS_INT_SourceStatusClear(SYS_BT_BUTTON_CN_SOURCE_1);
        SYS_INT_SourceStatusClear(SYS_BT_BUTTON_CN_SOURCE_2);
        // Handle buttons interrupt
        buttons_handleInterrupt((portAState | portBState));
    }
}

/* ISR for BT Button Repeat Timer */
void __ISR(_TIMER_3_VECTOR, ipl3) _IntHandlerDrvTmrInstance1(void)
{
    DRV_TMR_Tasks_ISR(sysObj.drvTmr1);
}
/*******************************************************************************
 End of File
 */