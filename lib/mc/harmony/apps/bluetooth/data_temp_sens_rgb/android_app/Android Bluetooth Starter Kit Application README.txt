BTSK Android App Kit
Android Application ReadMe

These files are included with the Bluetooth Data demonstration application for use on an Android (v4.0 and later) mobile platform.  These functions are intended to show a simple visual demonstration of Serial Port Profile (SPP) upload and download to the PIC32 Bluetooth Starter Kit (DM320018).

The embedded application is part of MPLAB Harmony and can be found within <install-dir>/apps/bluetooth/android_app.  This application is specifically written for the Bluetooth Starter Kit, which runs a PIC32MX270F256L processor.

A precompiled executable (*.apk) is also included in this package which can be immediately installed on a cellular phone.

Please see the embedded application help file for additional information about the functions of the Android application and the embedded functions working together.

The code provided is an example and provided on an as-is basis per the license agreement.  The source code is also subject to other licenses that come from graphical elements used.  More information about the licenses can be found in the "LICENSE" file under the PIC32_Bluetooth_Starter_Kit folder.