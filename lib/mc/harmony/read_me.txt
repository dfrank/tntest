Welcome to the MPLAB Harmony Integrated Software Framework!

Your installation is almost complete. Only a few steps remain.

If you already have the MPLAB X IDE installed on your system, you need to open
it and install the MPLAB Harmony Configurator plug-in.

   <install-dir>/utilities/mhc/com-microchip-mplab-modules-mhc.nbm

Installing this plug-in will add the "MPLAB Harmony Configurator" (MHC) to the
"Tools" > "Embedded" menu in MPLAB X IDE.  It will also add the "MPLAB Harmony
Project" project type to the "Microchip Embedded" category of projects in the 
MPLAB X New Project wizard.  The MHC is your quickest and easiest way to create
and configure new MPLAB Harmony projects.

(Refer to the MPLAB Harmony help documentation for additional information.)

To install this plug-in into MPLAB X IDE, follow these steps:

1. From the MPLAB X IDE "Tools" menu, choose "Plugins". A dialog box appears.
2. Select the "Downloaded" tab in the dialog box.
3. Click "Add Plugins..." and navigate to the directory paths given above.
4. Select the plug-in (.nbm) file and click "Open".
5. Ensure that the selected plug-in is listed and the "Install" box is checked.
6. Click "Install" and follow the on-screen directions.

   Note:  You may need to restart MPLAB X IDE for the plug-in to become 
          effective.

(Refer to the MPLAB X IDE documentation for additional information on 
plug-ins.)

Note: If you are not using MPLAB X IDE or have not yet installed it, you still 
have access to the full MPLAB Harmony help system, as it is provided in three 
formats: Compiled Help (.chm), Adobe Portable Document Format (PDF), and HTML
(.html). These files are available in the following locations:

   <install-dir>/doc/help_harmony.pdf
   <install-dir>/doc/help_harmony.chm (your computer must be configured to support 
                                       Unicode (utf-8) encoding to use this file)
   <install-dir/doc/html/index.html

Enjoy!

