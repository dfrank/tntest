/*******************************************************************************
  Reboot Module

  Summary:
    Module for Microchip TCP/IP Stack
    
  Description:
    -Remotely resets the PIC
    -Reference: Internet Bootloader documentation
*******************************************************************************/

/*******************************************************************************
File Name:  Reboot.c
Copyright � 2012 released Microchip Technology Inc.  All rights
reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/

#define __REBOOT_C

#include "tcpip/src/tcpip_private.h"



#if defined(TCPIP_STACK_USE_REBOOT_SERVER)

#include "system/reset/sys_reset.h"

#define TCPIP_THIS_MODULE_ID    TCPIP_MODULE_REBOOT_SERVER


static UDP_SOCKET	    rebootSocket = INVALID_UDP_SOCKET;
static int              rebootInitCount = 0;
static tcpipAsyncHandle rebootTimerHandle = 0;

/*****************************************************************************
  Function:
    bool TCPIP_REBOOT_Initialize(const TCPIP_STACK_MODULE_CTRL* const stackCtrl, const REBOOT_MODULE_CONFIG* pRebootConfig);

  Summary:
	Resets the reboot server module for the specified interface.

  Description:
	Resets the reboot server module for the specified interface.

  Precondition:
	None

  Parameters:
	stackCtrl - pointer to stack structure specifying the interface to initialize

  Returns:
	None

  Remarks:
	This function should be called internally just once per interface 
    by the stack manager.
***************************************************************************/
bool TCPIP_REBOOT_Initialize(const TCPIP_STACK_MODULE_CTRL* const stackCtrl, const void* pRebootConfig)
{
    if(stackCtrl->stackAction == TCPIP_STACK_ACTION_IF_UP)
    {   // interface restart
        return true;
    }

    // stack init
    
    if(rebootInitCount == 0)
    {   // first time we're run

        rebootSocket = TCPIP_UDP_ServerOpen(IP_ADDRESS_TYPE_IPV4, TCPIP_REBOOT_SERVER_PORT, 0);

        if(rebootSocket == INVALID_UDP_SOCKET)
        {   // failed
            return false;
        }

        // create the reboot timer
        rebootTimerHandle = _TCPIPStackAsyncHandlerRegister(TCPIP_REBOOT_Task, 0, TCPIP_REBOOT_TASK_TICK_RATE);
        if(rebootTimerHandle == 0)
        {   // cannot create the reboot timer
            TCPIP_UDP_Close(rebootSocket);
            rebootSocket = INVALID_UDP_SOCKET;
            return false;
        }

    }
            
    rebootInitCount++;
    return true;
}

/*****************************************************************************
  Function:
    bool TCPIP_REBOOT_Deinitialize(const TCPIP_STACK_MODULE_CTRL* const stackCtrl);

  Summary:
	Turns off the reboot server module for the specified interface.

  Description:
	Closes the reboot server.

  Precondition:
	None

  Parameters:
	stackData - pointer to stack structure specifying the interface to deinitialize

  Returns:
	None

  Remarks:
	This function should be called internally just once per interface 
    by the stack manager.
***************************************************************************/
void TCPIP_REBOOT_Deinitialize(const TCPIP_STACK_MODULE_CTRL* const stackCtrl)
{

    // if(stackCtrl->stackAction == TCPIP_STACK_ACTION_DEINIT) // stack shut down
    // if(stackCtrl->stackAction == TCPIP_STACK_ACTION_IF_DOWN) // interface down

    if(rebootInitCount > 0)
    {   // we're up and running
        if(stackCtrl->stackAction == TCPIP_STACK_ACTION_DEINIT)
        {   // whole stack is going down
            if(--rebootInitCount == 0)
            {   // all closed
                // release resources
                TCPIP_UDP_Close(rebootSocket);
                rebootSocket = INVALID_UDP_SOCKET;
                if(rebootTimerHandle)
                {
                    _TCPIPStackAsyncHandlerDeRegister(rebootTimerHandle);
                    rebootTimerHandle = 0;
                }
            }
        }
    }

}


/*********************************************************************
 * Function:        void TCPIP_REBOOT_Task(void)
 *
 * PreCondition:    Stack is initialized()
 *
 * Input:           None 
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        Checks for incomming traffic on port 69.  
 *					Resets the PIC if a 'R' is received.
 *
 * Note:            This module is primarily for use with the 
 *					Ethernet bootloader.  By resetting, the Ethernet 
 *					bootloader can take control for a second and let
 *					a firmware upgrade take place.
 ********************************************************************/
void TCPIP_REBOOT_Task(void)
{
    int     nBytes;
    int     rebootMsgSize;
    bool    msgFail, needReboot;
    uint8_t rebootBuffer[sizeof(TCPIP_REBOOT_MESSAGE)];

#if defined(TCPIP_REBOOT_SAME_SUBNET_ONLY)
    UDP_SOCKET_INFO     sktInfo;
#endif  // defined(TCPIP_REBOOT_SAME_SUBNET_ONLY)

    rebootMsgSize = strlen(TCPIP_REBOOT_MESSAGE);

    while(true)
    {
        // Do nothing if no data is waiting
        nBytes = TCPIP_UDP_GetIsReady(rebootSocket);

        if(nBytes == 0)
        {   // no more data pending
            return;
        }

        msgFail = needReboot = 0;
        if(nBytes < rebootMsgSize)
        {   // wrong message received
            msgFail = true;
        }
#if defined(TCPIP_REBOOT_SAME_SUBNET_ONLY)
        else
        {   // Respond only to name requests sent to us from nodes on the same subnet
            TCPIP_UDP_SocketInfoGet(rebootSocket, &sktInfo);
            if(_TCPIPStackIpAddFromAnyNet(0, &sktInfo.remoteIPaddress.v4Add) == 0)
            {
                msgFail = true;
            }
        }
#endif  // defined(TCPIP_REBOOT_SAME_SUBNET_ONLY)

        if(!msgFail)
        {   // check that we got the reset message
            TCPIP_UDP_ArrayGet(rebootSocket, rebootBuffer, rebootMsgSize);
            rebootBuffer[rebootMsgSize] = 0;

            if(strcmp((char*)rebootBuffer, TCPIP_REBOOT_MESSAGE) == 0)
            {   // got the reset message
                needReboot = true;
            }
        }

        TCPIP_UDP_Discard(rebootSocket);
        if(needReboot)
        {
            SYS_ERROR(SYS_ERROR_WARN, "System remote reset requested\r\n");
            SYS_RESET_SoftwareReset();
        }
    }

}

#endif //#if defined(TCPIP_STACK_USE_REBOOT_SERVER)

