/*
 * errno.h : error number definitions
 *
 * Copyright (c) 1996-2001 MIPS Technologies Inc. All Rights Reserved.
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)errno.h	7.1 (Berkeley) 6/4/86
 */

#ifndef __SYSTEM_ERRNO_H
#define __SYSTEM_ERRNO_H

extern int errno;

//#include <errno.h> // Use this one if using xc32 v1.0
#include <sys/errno.h>// Use this one if using xc32 >= v1.10


#endif /* !__SYSTEM_ERRNO_H */
