<#--
/*******************************************************************************
  DHCP server Freemarker Template File

  Company:
    Microchip Technology Inc.

  File Name:
    dhcps.h.ftl

  Summary:
    DHCP server Freemarker Template File

  Description:

*******************************************************************************/

/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS  WITHOUT  WARRANTY  OF  ANY  KIND,
EITHER EXPRESS  OR  IMPLIED,  INCLUDING  WITHOUT  LIMITATION,  ANY  WARRANTY  OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A  PARTICULAR  PURPOSE.
IN NO EVENT SHALL MICROCHIP OR  ITS  LICENSORS  BE  LIABLE  OR  OBLIGATED  UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,  BREACH  OF  WARRANTY,  OR
OTHER LEGAL  EQUITABLE  THEORY  ANY  DIRECT  OR  INDIRECT  DAMAGES  OR  EXPENSES
INCLUDING BUT NOT LIMITED TO ANY  INCIDENTAL,  SPECIAL,  INDIRECT,  PUNITIVE  OR
CONSEQUENTIAL DAMAGES, LOST  PROFITS  OR  LOST  DATA,  COST  OF  PROCUREMENT  OF
SUBSTITUTE  GOODS,  TECHNOLOGY,  SERVICES,  OR  ANY  CLAIMS  BY  THIRD   PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE  THEREOF),  OR  OTHER  SIMILAR  COSTS.
*******************************************************************************/
-->

/*** DHCP Server Configuration ***/
<#if CONFIG_TCPIP_STACK_USE_DHCP_SERVER == true>
#define TCPIP_DHCPS_TASK_PROCESS_RATE           		${CONFIG_TCPIP_DHCPS_TASK_PROCESS_RATE}
#define TCPIP_DHCPS_LEASE_ENTRIES_DEFAULT			${CONFIG_TCPIP_DHCPS_LEASE_ENTRIES_DEFAULT}
#define TCPIP_DHCPS_LEASE_SOLVED_ENTRY_TMO			${CONFIG_TCPIP_DHCPS_LEASE_SOLVED_ENTRY_TMO}
#define TCPIP_DHCPS_LEASE_REMOVED_BEFORE_ACK			${CONFIG_TCPIP_DHCPS_LEASE_REMOVED_BEFORE_ACK}

<#if CONFIG_TCPIP_DHCPS_DEFAULT_IP_ADDRESS_RANGE_START?has_content>
#define TCPIP_DHCPS_DEFAULT_IP_ADDRESS_RANGE_START			"${CONFIG_TCPIP_DHCPS_DEFAULT_IP_ADDRESS_RANGE_START}"
<#else>
#define TCPIP_DHCPS_DEFAULT_IP_ADDRESS_RANGE_START 			0
</#if>

<#if CONFIG_TCPIP_DHCPS_DEFAULT_SERVER_IP_ADDRESS?has_content>
#define TCPIP_DHCPS_DEFAULT_SERVER_IP_ADDRESS		    	"${CONFIG_TCPIP_DHCPS_DEFAULT_SERVER_IP_ADDRESS}"
<#else>
#define TCPIP_DHCPS_DEFAULT_SERVER_IP_ADDRESS				0
</#if>

<#if CONFIG_TCPIP_DHCPS_DEFAULT_SERVER_NETMASK_ADDRESS?has_content>
#define TCPIP_DHCPS_DEFAULT_SERVER_NETMASK_ADDRESS			"${CONFIG_TCPIP_DHCPS_DEFAULT_SERVER_NETMASK_ADDRESS}"
<#else>
#define TCPIP_DHCPS_DEFAULT_SERVER_NETMASK_ADDRESS			0
</#if>

<#if CONFIG_TCPIP_DHCPS_DEFAULT_SERVER_GATEWAY_ADDRESS?has_content>
#define TCPIP_DHCPS_DEFAULT_SERVER_GATEWAY_ADDRESS			"${CONFIG_TCPIP_DHCPS_DEFAULT_SERVER_GATEWAY_ADDRESS}"
<#else>
#define TCPIP_DHCPS_DEFAULT_SERVER_GATEWAY_ADDRESS			0
</#if>

<#if CONFIG_TCPIP_DHCPS_DEFAULT_SERVER_PRIMARY_DNS_ADDRESS?has_content>
#define TCPIP_DHCPS_DEFAULT_SERVER_PRIMARY_DNS_ADDRESS			"${CONFIG_TCPIP_DHCPS_DEFAULT_SERVER_PRIMARY_DNS_ADDRESS}"
<#else>
#define TCPIP_DHCPS_DEFAULT_SERVER_PRIMARY_DNS_ADDRESS			0
</#if>

<#if CONFIG_TCPIP_DHCPS_DEFAULT_SERVER_SECONDARY_DNS_ADDRESS?has_content>
#define TCPIP_DHCPS_DEFAULT_SERVER_SECONDARY_DNS_ADDRESS			"${CONFIG_TCPIP_DHCPS_DEFAULT_SERVER_SECONDARY_DNS_ADDRESS}"
<#else>
#define TCPIP_DHCPS_DEFAULT_SERVER_SECONDARY_DNS_ADDRESS			0
</#if>

#define TCPIP_DHCP_SERVER_INTERFACE_INDEX			${CONFIG_TCPIP_DHCP_SERVER_INTERFACE_INDEX}

<#if CONFIG_TCPIP_DHCP_SERVER_POOL_ENABLED == true>
#define TCPIP_DHCP_SERVER_POOL_ENABLED				true
<#else>
#define TCPIP_DHCP_SERVER_POOL_ENABLED				false
</#if>

<#if CONFIG_TCPIP_DHCP_SERVER_DELETE_OLD_ENTRIES == true>
#define TCPIP_DHCP_SERVER_DELETE_OLD_ENTRIES			true
<#else>
#define TCPIP_DHCP_SERVER_DELETE_OLD_ENTRIES			false
</#if>
#define TCPIP_DHCPS_LEASE_DURATION	TCPIP_DHCPS_LEASE_SOLVED_ENTRY_TMO
#define TCPIP_STACK_USE_DHCP_SERVER
</#if>

<#--
/*******************************************************************************
 End of File
*/
-->
