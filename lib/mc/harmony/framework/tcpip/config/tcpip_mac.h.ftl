<#--
/*******************************************************************************
  TCPIP MAC Freemarker Template File

  Company:
    Microchip Technology Inc.

  File Name:
    tcpip_mac.h.ftl

  Summary:
    TCPIP MAC Freemarker Template File

  Description:

*******************************************************************************/

/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS  WITHOUT  WARRANTY  OF  ANY  KIND,
EITHER EXPRESS  OR  IMPLIED,  INCLUDING  WITHOUT  LIMITATION,  ANY  WARRANTY  OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A  PARTICULAR  PURPOSE.
IN NO EVENT SHALL MICROCHIP OR  ITS  LICENSORS  BE  LIABLE  OR  OBLIGATED  UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,  BREACH  OF  WARRANTY,  OR
OTHER LEGAL  EQUITABLE  THEORY  ANY  DIRECT  OR  INDIRECT  DAMAGES  OR  EXPENSES
INCLUDING BUT NOT LIMITED TO ANY  INCIDENTAL,  SPECIAL,  INDIRECT,  PUNITIVE  OR
CONSEQUENTIAL DAMAGES, LOST  PROFITS  OR  LOST  DATA,  COST  OF  PROCUREMENT  OF
SUBSTITUTE  GOODS,  TECHNOLOGY,  SERVICES,  OR  ANY  CLAIMS  BY  THIRD   PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE  THEREOF),  OR  OTHER  SIMILAR  COSTS.
*******************************************************************************/
-->

/*** TCPIP MAC Configuration ***/
<#if CONFIG_TCPIP_USE_ETH_MAC == true>
#define TCPIP_EMAC_TX_DESCRIPTORS				${CONFIG_TCPIP_EMAC_TX_DESCRIPTORS}
#define TCPIP_EMAC_RX_DESCRIPTORS				${CONFIG_TCPIP_EMAC_RX_DESCRIPTORS}
#define TCPIP_EMAC_RX_BUFF_SIZE		    			${CONFIG_TCPIP_EMAC_RX_BUFF_SIZE}
#define TCPIP_EMAC_RX_MAX_FRAME		    			${CONFIG_TCPIP_EMAC_RX_MAX_FRAME}
#define TCPIP_EMAC_RX_FRAGMENTS		    			${CONFIG_TCPIP_EMAC_RX_FRAGMENTS}
#define TCPIP_EMAC_ETH_OPEN_FLAGS       			${CONFIG_TCPIP_EMAC_ETH_OPEN_FLAGS}
#define TCPIP_EMAC_PHY_CONFIG_FLAGS     			${CONFIG_TCPIP_EMAC_PHY_CONFIG_FLAGS}
#define TCPIP_EMAC_PHY_LINK_INIT_DELAY  			${CONFIG_TCPIP_EMAC_PHY_LINK_INIT_DELAY}
#define TCPIP_EMAC_PHY_ADDRESS		    			${CONFIG_TCPIP_EMAC_PHY_ADDRESS}
<#if CONFIG_TCPIP_EMAC_INTERRUPT_MODE == true>
#define TCPIP_EMAC_INTERRUPT_MODE        			true
#define TCPIP_STACK_USE_EVENT_NOTIFICATION
<#else>
#define TCPIP_EMAC_INTERRUPT_MODE        			true
</#if>
#define DRV_ETHPHY_INSTANCES_NUMBER				${CONFIG_DRV_ETHPHY_INSTANCES_NUMBER}
#define DRV_ETHPHY_CLIENTS_NUMBER				${CONFIG_DRV_ETHPHY_CLIENTS_NUMBER}
#define DRV_ETHPHY_INDEX		        		${CONFIG_DRV_ETHPHY_INDEX}
#define DRV_ETHPHY_PERIPHERAL_ID				${CONFIG_DRV_ETHPHY_PERIPHERAL_ID}
#define DRV_ETHPHY_NEG_INIT_TMO		    			${CONFIG_DRV_ETHPHY_NEG_INIT_TMO}
#define DRV_ETHPHY_NEG_DONE_TMO		    			${CONFIG_DRV_ETHPHY_NEG_DONE_TMO}
#define DRV_ETHPHY_RESET_CLR_TMO				${CONFIG_DRV_ETHPHY_RESET_CLR_TMO}
#define DRV_ETHMAC_INSTANCES_NUMBER				${CONFIG_DRV_ETHMAC_INSTANCES_NUMBER}
#define DRV_ETHMAC_CLIENTS_NUMBER				${CONFIG_DRV_ETHMAC_CLIENTS_NUMBER}
#define DRV_ETHMAC_INDEX	    	    			${CONFIG_DRV_ETHMAC_INDEX}
#define DRV_ETHMAC_PERIPHERAL_ID				${CONFIG_DRV_ETHMAC_PERIPHERAL_ID}
#define DRV_ETHMAC_INTERRUPT_VECTOR				${CONFIG_DRV_ETHMAC_INTERRUPT_VECTOR}
#define DRV_ETHMAC_INTERRUPT_SOURCE				${CONFIG_DRV_ETHMAC_INTERRUPT_SOURCE}
#define DRV_ETHMAC_POWER_STATE		    			${CONFIG_DRV_ETHMAC_POWER_STATE}

<#if CONFIG_DRV_ETHMAC_INTERRUPT_MODE == true>
#define DRV_ETHMAC_INTERRUPT_MODE        			true
#define TCPIP_STACK_USE_EVENT_NOTIFICATION
<#else>
#define DRV_ETHMAC_INTERRUPT_MODE        			true
</#if>
</#if>

<#--
/*******************************************************************************
 End of File
*/
-->
