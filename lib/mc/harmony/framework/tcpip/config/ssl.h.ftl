<#--
/*******************************************************************************
  SSL Freemarker Template File

  Company:
    Microchip Technology Inc.

  File Name:
    ssl.h.ftl

  Summary:
    SSL Freemarker Template File

  Description:

*******************************************************************************/

/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS  WITHOUT  WARRANTY  OF  ANY  KIND,
EITHER EXPRESS  OR  IMPLIED,  INCLUDING  WITHOUT  LIMITATION,  ANY  WARRANTY  OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A  PARTICULAR  PURPOSE.
IN NO EVENT SHALL MICROCHIP OR  ITS  LICENSORS  BE  LIABLE  OR  OBLIGATED  UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,  BREACH  OF  WARRANTY,  OR
OTHER LEGAL  EQUITABLE  THEORY  ANY  DIRECT  OR  INDIRECT  DAMAGES  OR  EXPENSES
INCLUDING BUT NOT LIMITED TO ANY  INCIDENTAL,  SPECIAL,  INDIRECT,  PUNITIVE  OR
CONSEQUENTIAL DAMAGES, LOST  PROFITS  OR  LOST  DATA,  COST  OF  PROCUREMENT  OF
SUBSTITUTE  GOODS,  TECHNOLOGY,  SERVICES,  OR  ANY  CLAIMS  BY  THIRD   PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE  THEREOF),  OR  OTHER  SIMILAR  COSTS.
*******************************************************************************/
-->

<#if CONFIG_TCPIP_STACK_USE_SSL_SERVER == true || CONFIG_TCPIP_STACK_USE_SSL_CLIENT == true>
    <#if CONFIG_TCPIP_STACK_USE_SSL_SERVER == true>
#define TCPIP_STACK_USE_SSL_SERVER
    </#if>
    <#if CONFIG_TCPIP_STACK_USE_SSL_CLIENT == true>
#define TCPIP_STACK_USE_SSL_CLIENT
    </#if>
#define TCPIP_SSL_MIN_SESSION_LIFETIME 				${CONFIG_TCPIP_SSL_MIN_SESSION_LIFETIME}
#define TCPIP_SSL_RSA_LIFETIME_EXTENSION 				${CONFIG_TCPIP_SSL_RSA_LIFETIME_EXTENSION}
#define TCPIP_SSL_MAX_CONNECTIONS 					${CONFIG_TCPIP_SSL_MAX_CONNECTIONS}
#define TCPIP_SSL_MAX_SESSIONS 					${CONFIG_TCPIP_SSL_MAX_SESSIONS}
<#if CONFIG_TCPIP_SSL_MULTIPLE_INTERFACES == true>
#define TCPIP_SSL_MULTIPLE_INTERFACES 				true
<#else>
#define TCPIP_SSL_MULTIPLE_INTERFACES 				false
</#if>
#define TCPIP_SSL_MAX_BUFFERS 					${CONFIG_TCPIP_SSL_MAX_BUFFERS}
#define TCPIP_SSL_MAX_HASHES 						${CONFIG_TCPIP_SSL_MAX_HASHES}
#define TCPIP_SSL_RSA_SERVER_KEY_SIZE 					${CONFIG_TCPIP_SSL_RSA_SERVER_KEY_SIZE}
#define TCPIP_SSL_RSA_CLIENT_KEY_SIZE 					${CONFIG_TCPIP_SSL_RSA_CLIENT_KEY_SIZE}
#define TCPIP_STACK_USE_SSL_SERVER
#define TCPIP_STACK_USE_SSL_CLIENT
#define TCPIP_SSL_VERSION                       (0x0300u)
#define TCPIP_SSL_VERSION_HI                    (0x03u)
#define TCPIP_SSL_VERSION_LO                    (0x00u)
</#if>

<#--
/*******************************************************************************
 End of File
*/
-->
