<#--
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->

<#--
// tcpip_stack_init.c.ftl: TCP/IP modules initialization data
-->

<#if CONFIG_TCPIP_USE_ARP == true>
/*** ARP Service Initialization Data ***/
const TCPIP_ARP_MODULE_CONFIG tcpipARPInitData =
{ 
    .cacheEntries       = ${CONFIG_TCPIP_ARP_CACHE_ENTRIES}, 
    <#if CONFIG_TCPIP_ARP_CACHE_DELETE_OLD == true>
    .deleteOld          = true, 
    <#else>
    .deleteOld          = false, 
    </#if>
    .entrySolvedTmo     = ${CONFIG_TCPIP_ARP_CACHE_SOLVED_ENTRY_TMO}, 
    .entryPendingTmo    = ${CONFIG_TCPIP_ARP_CACHE_PENDING_ENTRY_TMO}, 
    .entryRetryTmo      = ${CONFIG_TCPIP_ARP_CACHE_PENDING_RETRY_TMO}, 
    .permQuota          = ${CONFIG_TCPIP_ARP_CACHE_PERMANENT_QUOTA}, 
    .purgeThres         = ${CONFIG_TCPIP_ARP_CACHE_PURGE_THRESHOLD}, 
    .purgeQuanta        = ${CONFIG_TCPIP_ARP_CACHE_PURGE_QUANTA}, 
    .retries            = ${CONFIG_TCPIP_ARP_CACHE_ENTRY_RETRIES}, 
    .gratProbeCount     = ${CONFIG_TCPIP_ARP_GRATUITOUS_PROBE_COUNT},
};

</#if>


<#if CONFIG_TCPIP_USE_TELNET == true>
/*** telnet Server Initialization Data ***/
const TCPIP_TELNET_MODULE_CONFIG tcpipTelnetInitData =
{ 
};

</#if>

<#if CONFIG_TCPIP_USE_ANNOUNCE == true>
/*** Announce Discovery Initialization Data ***/
const TCPIP_ANNOUNCE_MODULE_CONFIG tcpipAnnounceInitData =
{ 
};

</#if>



<#if CONFIG_TCPIP_USE_UDP == true>
/*** UDP Sockets Initialization Data ***/
const TCPIP_UDP_MODULE_CONFIG tcpipUDPInitData =
{
    .nSockets       = ${CONFIG_TCPIP_UDP_MAX_SOCKETS},
    .sktTxBuffSize  = ${CONFIG_TCPIP_UDP_SOCKET_DEFAULT_TX_SIZE}, 
    .poolBuffers    = ${CONFIG_TCPIP_UDP_SOCKET_POOL_BUFFERS},
    .poolBufferSize = ${CONFIG_TCPIP_UDP_SOCKET_POOL_BUFFER_SIZE},
};

</#if>

<#if CONFIG_TCPIP_USE_TCP == true>
/*** TCP Sockets Initialization Data ***/
const TCPIP_TCP_MODULE_CONFIG tcpipTCPInitData =
{
    .nSockets       = ${CONFIG_TCPIP_TCP_MAX_SOCKETS},
    .sktTxBuffSize  = ${CONFIG_TCPIP_TCP_SOCKET_DEFAULT_TX_SIZE}, 
    .sktRxBuffSize  = ${CONFIG_TCPIP_TCP_SOCKET_DEFAULT_RX_SIZE},
};

</#if>


<#if CONFIG_TCPIP_STACK_USE_HTTP_SERVER == true>
/*** HTTP Server Initialization Data ***/
const TCPIP_HTTP_MODULE_CONFIG tcpipHTTPInitData =
{
    .nConnections   	= ${CONFIG_TCPIP_HTTP_MAX_CONNECTIONS},
    .dataLen		= ${CONFIG_TCPIP_HTTP_MAX_DATA_LEN},
    .sktTxBuffSize	= ${CONFIG_TCPIP_HTTP_SKT_TX_BUFF_SIZE},
    .sktRxBuffSize	= ${CONFIG_TCPIP_HTTP_SKT_RX_BUFF_SIZE},
    .configFlags	= ${CONFIG_TCPIP_HTTP_CONFIG_FLAGS},
};

</#if>

<#if CONFIG_TCPIP_USE_SNTP_CLIENT == true>
/*** SNTP Client Initialization Data ***/
const TCPIP_SNTP_MODULE_CONFIG tcpipSNTPInitData =
{
    .ntp_server		        = "${CONFIG_TCPIP_NTP_SERVER}",
    .ntp_interface		    = "${CONFIG_TCPIP_NTP_DEFAULT_IF}",
    .ntp_connection_type	= ${CONFIG_TCPIP_NTP_DEFAULT_CONNECTION_TYPE},
    .ntp_reply_timeout		= ${CONFIG_TCPIP_NTP_REPLY_TIMEOUT},
    .ntp_stamp_timeout		= ${CONFIG_TCPIP_NTP_TIME_STAMP_TMO},
    .ntp_success_interval	= ${CONFIG_TCPIP_NTP_QUERY_INTERVAL},
    .ntp_error_interval		= ${CONFIG_TCPIP_NTP_FAST_QUERY_INTERVAL},
};

</#if>


<#if CONFIG_TCPIP_USE_SMTP_CLIENT == true>
/*** SMTP client Initialization Data ***/
const TCPIP_SMTP_CLIENT_MODULE_CONFIG tcpipSMTPInitData =
{ 
};

</#if>


<#if CONFIG_TCPIP_DHCP_CLIENT_ENABLED == true>
/*** DHCP client Initialization Data ***/
const TCPIP_DHCP_MODULE_CONFIG tcpipDHCPInitData =
{ 
    <#if CONFIG_TCPIP_DHCP_CLIENT_ENABLED == true>
    .dhcpEnable		= true,
    <#else>
    .dhcpEnable		= false,
    </#if>
    .dhcpTmo		= ${CONFIG_TCPIP_DHCP_TIMEOUT},
    .dhcpCliPort    	= ${CONFIG_TCPIP_DHCP_CLIENT_CONNECT_PORT},
    .dhcpSrvPort	= ${CONFIG_TCPIP_DHCP_SERVER_LISTEN_PORT},

};

</#if>

<#if CONFIG_TCPIP_STACK_USE_BERKELEY_API == true>
/*** Berkeley API Initialization Data ***/
const BERKELEY_MODULE_CONFIG tcpipBerkeleyInitData = 
{
    .maxSockets = MAX_BSD_SOCKETS,
};
</#if>

<#if CONFIG_TCPIP_STACK_USE_SSL_SERVER == true || CONFIG_TCPIP_STACK_USE_SSL_CLIENT == true>
/*** SSL Initialization Data ***/
const SSL_MODULE_CONFIG tcpipSSLInitData = {};
</#if>


<#if CONFIG_TCPIP_STACK_USE_ICMP_SERVER == true>
/*** ICMP Server Initialization Data ***/
const TCPIP_ICMP_MODULE_CONFIG tcpipICMPInitData = 
{
};
</#if>


<#if CONFIG_TCPIP_USE_NBNS == true>
/*** NBNS Server Initialization Data ***/
const TCPIP_NBNS_MODULE_CONFIG tcpipNBNSInitData =
{ 
};
</#if>
<#if CONFIG_TCPIP_USE_ETH_MAC == true>
/*** ETH MAC Initialization Data ***/
const TCPIP_MODULE_MAC_PIC32INT_CONFIG tcpipMACPIC32INTInitData =
{ 
    .nTxDescriptors		= ${CONFIG_TCPIP_EMAC_TX_DESCRIPTORS},
    .rxBuffSize		    	= ${CONFIG_TCPIP_EMAC_RX_BUFF_SIZE},
    .nRxDescriptors     	= ${CONFIG_TCPIP_EMAC_RX_DESCRIPTORS},
    .ethFlags		    	= ${CONFIG_TCPIP_EMAC_ETH_OPEN_FLAGS},
    .phyFlags		   	= ${CONFIG_TCPIP_EMAC_PHY_CONFIG_FLAGS},
    .linkInitDelay		= ${CONFIG_TCPIP_EMAC_PHY_LINK_INIT_DELAY},
    .phyAddress		    	= ${CONFIG_TCPIP_EMAC_PHY_ADDRESS},
};

</#if>
<#if CONFIG_TCPIP_USE_DDNS == true>
/*** DDNS Initialization Data ***/
const DDNS_MODULE_CONFIG tcpipDDNSInitData =
{
};
</#if>

<#if CONFIG_TCPIP_USE_LINK_ZERO_CONFIG == true>
/*** Zeroconfig initialization data ***/
const ZCLL_MODULE_CONFIG tcpipZCLLInitData =
{
};
</#if>

<#if CONFIG_TCPIP_STACK_USE_DHCP_SERVER == true>
/*** DHCP server initialization data ***/
TCPIP_DHCPS_ADDRESS_CONFIG DHCP_POOL_CONFIG[]=
{
	{
		.interfaceIndex		= TCPIP_DHCP_SERVER_INTERFACE_INDEX,
		.serverIPAddress	= TCPIP_DHCPS_DEFAULT_SERVER_IP_ADDRESS,
		.startIPAddRange	= TCPIP_DHCPS_DEFAULT_IP_ADDRESS_RANGE_START,
		.ipMaskAddress		= TCPIP_DHCPS_DEFAULT_SERVER_NETMASK_ADDRESS,
		.priDNS			    = TCPIP_DHCPS_DEFAULT_SERVER_PRIMARY_DNS_ADDRESS,
		.secondDNS		    = TCPIP_DHCPS_DEFAULT_SERVER_SECONDARY_DNS_ADDRESS,
<#if CONFIG_TCPIP_DHCP_SERVER_POOL_ENABLED == true >
		.poolEnabled		=true,
<#else>
		.poolEnabled = false,
</#if>
	},
};
const TCPIP_DHCPS_MODULE_CONFIG tcpipDHCPSInitData =
{
	.enabled			= true,
<#if CONFIG_TCPIP_DHCP_SERVER_DELETE_OLD_ENTRIES == true>
	.deleteOldLease		=true,
<#else>
	.deleteOldLease		=false,
</#if>	
	.leaseEntries		=${CONFIG_TCPIP_DHCPS_LEASE_ENTRIES_DEFAULT},
	.entrySolvedTmo		=${CONFIG_TCPIP_DHCPS_LEASE_SOLVED_ENTRY_TMO},
	.dhcpServer		=(TCPIP_DHCPS_ADDRESS_CONFIG*)DHCP_POOL_CONFIG,
};
</#if>

<#if CONFIG_TCPIP_USE_FTP_MODULE == true>
/*** FTP Server Initialization Data ***/
const TCPIP_FTP_MODULE_CONFIG tcpipFTPInitData =
{ 
    .dataSktTxBuffSize		= ${CONFIG_TCPIP_FTP_DATA_SKT_TX_BUFF_SIZE},
    .dataSktRxBuffSize		= ${CONFIG_TCPIP_FTP_DATA_SKT_RX_BUFF_SIZE},
    .userName			= "${CONFIG_TCPIP_FTP_USER_NAME}",
    .password		    	= "${CONFIG_TCPIP_FTP_PASSWORD}",
};
</#if>

<#if CONFIG_TCPIP_USE_DNS_CLIENT == true>
/*** DNS Client Initialization Data ***/
const TCPIP_DNS_CLIENT_MODULE_CONFIG tcpipDNSClientInitData =
{ 
<#if CONFIG_TCPIP_DNS_CLIENT_DELETE_OLD_ENTRIES == true >
    .deleteOldLease			= true,
<#else>
    .deleteOldLease			= false,
</#if>
    .cacheEntries			= ${CONFIG_TCPIP_DNS_CLIENT_CACHE_ENTRIES},
    .entrySolvedTmo			= ${CONFIG_TCPIP_DNS_CLIENT_CACHE_ENTRY_TMO},    
    .IPv4EntriesPerDNSName 	= ${CONFIG_TCPIP_DNS_CLIENT_CACHE_PER_IPV4_ADDRESS},
	.dnsIpAddressType 		= ${CONFIG_TCPIP_DNS_CLIENT_OPEN_ADDRESS_TYPE},
<#if CONFIG_TCPIP_STACK_USE_IPV6 == true >	
	.IPv6EntriesPerDNSName		= ${CONFIG_TCPIP_DNS_CLIENT_CACHE_PER_IPV6_ADDRESS},
</#if>
};
</#if>

<#if CONFIG_TCPIP_USE_DNSS == true>
/*** DNS Server Initialization Data ***/
const TCPIP_DNSS_MODULE_CONFIG tcpipDNSServerInitData =
{ 
<#if CONFIG_TCPIP_DNSS_DELETE_OLD_LEASE == true>
    .deleteOldLease			= true,
<#else>
	.deleteOldLease			= false,
</#if>
<#if CONFIG_TCPIP_DNSS_REPLY_BOARD_ADDR == true>
    .replyBoardAddr			= true,
<#else>
	.replyBoardAddr			= false,
</#if>
    .IPv4EntriesPerDNSName 	= ${CONFIG_TCPIP_DNSS_CACHE_PER_IPV4_ADDRESS},
<#if CONFIG_TCPIP_STACK_USE_IPV6 == true >
	.IPv6EntriesPerDNSName 	= ${CONFIG_TCPIP_DNSS_CACHE_PER_IPV6_ADDRESS},
<#else>
	.IPv6EntriesPerDNSName 	= 0,
</#if>
};
</#if>
<#if CONFIG_TCPIP_STACK_USE_IPV6 == true>
/*** IPv6 Initialization Data ***/
const TCPIP_IPV6_MODULE_CONFIG  tcpipIPv6InitData = 
{
    .rxfragmentBufSize = TCPIP_IPV6_RX_FRAGMENTED_BUFFER_SIZE,
    .fragmentPktRxTimeout = TCPIP_IPV6_FRAGMENT_PKT_TIMEOUT,
};
</#if>

<#if CONFIG_TCPIP_USE_SNMP == true>
TCPIP_SNMP_COMMUNITY_CONFIG tcpipSNMPInitReadcommunity[] =
{
<#if CONFIG_TCPIP_SNMP_STACK_CONFIG_IDX0>
/*** Network Configuration Index 0 ***/
    {
        TCPIP_SNMP_STACK_READCOMMUNITY_NAME_IDX0,
    },
</#if>
<#if CONFIG_TCPIP_SNMP_STACK_CONFIG_IDX1>
/*** Network Configuration Index 1 ***/
    {
        TCPIP_SNMP_STACK_READCOMMUNITY_NAME_IDX1,
    },
</#if>	
<#if CONFIG_TCPIP_SNMP_STACK_CONFIG_IDX2>
/*** Network Configuration Index 2 ***/	
    {
        TCPIP_SNMP_STACK_READCOMMUNITY_NAME_IDX2,
    },
</#if>	
};

TCPIP_SNMP_COMMUNITY_CONFIG tcpipSNMPInitWritecommunity[] =
{
<#if CONFIG_TCPIP_SNMP_STACK_CONFIG_IDX0>
/*** Network Configuration Index 0 ***/
    {
        TCPIP_SNMP_STACK_WRITECOMMUNITY_NAME_IDX0,
    },
</#if>	
<#if CONFIG_TCPIP_SNMP_STACK_CONFIG_IDX1>
/*** Network Configuration Index 1 ***/
    {
        TCPIP_SNMP_STACK_WRITECOMMUNITY_NAME_IDX1,
    },
</#if>	
<#if CONFIG_TCPIP_SNMP_STACK_CONFIG_IDX2>
/*** Network Configuration Index 2 ***/
    {
        TCPIP_SNMP_STACK_WRITECOMMUNITY_NAME_IDX2,
    },
</#if>	
};

<#if CONFIG_TCPIP_USE_SNMPv3 == true>
// SNMPv3 USM configuration
TCPIP_SNMPV3_USM_USER_CONFIG tcpipSNMPv3InitUSM[] =
{
<#if CONFIG_TCPIP_SNMP_STACK_CONFIG_IDX0>
/*** Network Configuration Index 0 ***/
    {
        TCPIP_SNMPV3_STACK_USM_NAME_IDX0,            			/*** securityName ***/
        TCPIP_SNMPV3_STACK_SECURITY_LEVEL_IDX0,              	/*** authentication and privacy security-level ***/
        /*** auth ***/
        TCPIP_SNMPV3_STACK_AUTH_PROTOCOL_IDX0,        			/*** MD5 auth protocol ***/
        TCPIP_SNMPV3_STACK_AUTH_PASSWORD_IDX0,            		/*** auth passphrase ***/
        /*** priv ***/
        TCPIP_SNMPV3_STACK_PRIV_PROTOCOL_IDX0,        			/*** AES priv protocol ***/
        TCPIP_SNMPV3_STACK_PRIV_PASSWORD_IDX0,            		/*** priv passphrase ***/
    },
</#if>	
<#if CONFIG_TCPIP_SNMP_STACK_CONFIG_IDX1>
/*** Network Configuration Index 1 ***/	
    {
        TCPIP_SNMPV3_STACK_USM_NAME_IDX1,            			/*** securityName ***/
        TCPIP_SNMPV3_STACK_SECURITY_LEVEL_IDX1,              	/*** authentication and privacy security-level ***/
        /*** auth ***/
        TCPIP_SNMPV3_STACK_AUTH_PROTOCOL_IDX1,        			/*** MD5 auth protocol ***/
        TCPIP_SNMPV3_STACK_AUTH_PASSWORD_IDX1,            		/*** auth passphrase ***/
        /*** priv ***/
        TCPIP_SNMPV3_STACK_PRIV_PROTOCOL_IDX1,        			/*** AES priv protocol ***/
        TCPIP_SNMPV3_STACK_PRIV_PASSWORD_IDX1,            		/*** priv passphrase ***/
    },
</#if>	
<#if CONFIG_TCPIP_SNMP_STACK_CONFIG_IDX2>
/*** Network Configuration Index 2 ***/	
    {
        TCPIP_SNMPV3_STACK_USM_NAME_IDX2,            			/*** securityName ***/
        TCPIP_SNMPV3_STACK_SECURITY_LEVEL_IDX2,              	/*** authentication and privacy security-level ***/
        /*** auth ***/
        TCPIP_SNMPV3_STACK_AUTH_PROTOCOL_IDX2,        			/*** MD5 auth protocol ***/
        TCPIP_SNMPV3_STACK_AUTH_PASSWORD_IDX2,            		/*** auth passphrase ***/
        /*** priv ***/
        TCPIP_SNMPV3_STACK_PRIV_PROTOCOL_IDX2,        			/*** AES priv protocol ***/
        TCPIP_SNMPV3_STACK_PRIV_PASSWORD_IDX2,            		/*** priv passphrase ***/
    },
</#if>	
};

// SNMPv3 USM based Trap configuration
// User name should be exacly same to the above USM table.
TCPIP_SNMPV3_TARGET_ENTRY_CONFIG tcpipSNMPv3InitTargetTrap[]=
{
<#if CONFIG_TCPIP_SNMP_STACK_CONFIG_IDX0>
/*** Network Configuration Index 0 ***/
    {
        TCPIP_SNMPV3_TARGET_ENTRY_SEC_NAME_IDX0,                    /*** securityName ***/
        TCPIP_SNMPV3_TARGET_ENTRY_MESSAGE_PROTOCOL_TYPE_IDX0,    	/*** Message processing model ***/
        TCPIP_SNMPV3_TARGET_ENTRY_SEC_MODEL_TYPE_IDX0,      		/*** Security Model ***/
        TCPIP_SNMPV3_TARGET_ENTRY_SEC_LEVEL_IDX0,             		/*** Security-level ***/
    },
</#if>	
<#if CONFIG_TCPIP_SNMP_STACK_CONFIG_IDX1>
/*** Network Configuration Index 1 ***/	
     {
        TCPIP_SNMPV3_TARGET_ENTRY_SEC_NAME_IDX1,                    /*** securityName ***/
        TCPIP_SNMPV3_TARGET_ENTRY_MESSAGE_PROTOCOL_TYPE_IDX1,    	/*** Message processing model ***/
        TCPIP_SNMPV3_TARGET_ENTRY_SEC_MODEL_TYPE_IDX1,      		/*** Security Model ***/
        TCPIP_SNMPV3_TARGET_ENTRY_SEC_LEVEL_IDX1,             		/*** Security-level ***/
    },
</#if>	
<#if CONFIG_TCPIP_SNMP_STACK_CONFIG_IDX2>
/*** Network Configuration Index 2 ***/	
	{
        TCPIP_SNMPV3_TARGET_ENTRY_SEC_NAME_IDX2,                    /*** securityName ***/
        TCPIP_SNMPV3_TARGET_ENTRY_MESSAGE_PROTOCOL_TYPE_IDX2,    	/*** Message processing model ***/
        TCPIP_SNMPV3_TARGET_ENTRY_SEC_MODEL_TYPE_IDX2,      		/*** Security Model ***/
        TCPIP_SNMPV3_TARGET_ENTRY_SEC_LEVEL_IDX2,             		/*** Security-level ***/
    },
</#if>
};
</#if>

const TCPIP_SNMP_MODULE_CONFIG tcpipSNMPInitData =
{
<#if CONFIG_TCPIP_USE_SNMP == true>
	.trapEnable = TCPIP_SNMP_USE_TRAP_SUPPORT,
	.snmp_trapv2_use = TCPIP_SNMP_STACK_USE_V2_TRAP,
<#if CONFIG_TCPIP_USE_SNMPv3 == true>
	.snmpv3_trapv1v2_use = TCPIP_SNMPV3_STACK_USE_V1_V2_TRAP,
<#else>
	.snmpv3_trapv1v2_use = false,
</#if>	
	.snmp_bib_file = TCPIP_SNMP_BIB_FILE_NAME,
</#if>
	.read_community_config = (TCPIP_SNMP_COMMUNITY_CONFIG*)tcpipSNMPInitReadcommunity,
	.write_community_config = (TCPIP_SNMP_COMMUNITY_CONFIG*)tcpipSNMPInitWritecommunity,
<#if CONFIG_TCPIP_USE_SNMPv3 == true>
	.usm_config = (TCPIP_SNMPV3_USM_USER_CONFIG*)tcpipSNMPv3InitUSM,
	.trap_target_config = (TCPIP_SNMPV3_TARGET_ENTRY_CONFIG*)tcpipSNMPv3InitTargetTrap,
<#else>
	.usm_config = NULL,
	.trap_target_config = NULL,
</#if>
};

</#if>

const TCPIP_NETWORK_CONFIG __attribute__((unused))  TCPIP_HOSTS_CONFIGURATION[] =
{
<#if CONFIG_TCPIP_STACK_NETWORK_CONFIG_IDX0>
/*** Network Configuration Index 0 ***/
    {
        TCPIP_NETWORK_DEFAULT_INTERFACE_NAME,       // interface
        TCPIP_NETWORK_DEFAULT_HOST_NAME,            // hostName
        TCPIP_NETWORK_DEFAULT_MAC_ADDR,             // macAddr
        TCPIP_NETWORK_DEFAULT_IP_ADDRESS,           // ipAddr
        TCPIP_NETWORK_DEFAULT_IP_MASK,              // ipMask
        TCPIP_NETWORK_DEFAULT_GATEWAY,              // gateway
        TCPIP_NETWORK_DEFAULT_DNS,                  // priDNS
        TCPIP_NETWORK_DEFAULT_SECOND_DNS,           // secondDNS
        TCPIP_NETWORK_DEFAULT_POWER_MODE,           // powerMode
        TCPIP_NETWORK_DEFAULT_INTERFACE_FLAGS,      // startFlags
    },
</#if>
<#if CONFIG_TCPIP_STACK_NETWORK_CONFIG_IDX1>
/*** Network Configuration Index 1 ***/
    {
        TCPIP_NETWORK_DEFAULT_INTERFACE_NAME_IDX1,       // interface
        TCPIP_NETWORK_DEFAULT_HOST_NAME_IDX1,            // hostName
        TCPIP_NETWORK_DEFAULT_MAC_ADDR_IDX1,             // macAddr
        TCPIP_NETWORK_DEFAULT_IP_ADDRESS_IDX1,           // ipAddr
        TCPIP_NETWORK_DEFAULT_IP_MASK_IDX1,              // ipMask
        TCPIP_NETWORK_DEFAULT_GATEWAY_IDX1,              // gateway
        TCPIP_NETWORK_DEFAULT_DNS_IDX1,                  // priDNS
        TCPIP_NETWORK_DEFAULT_SECOND_DNS_IDX1,           // secondDNS
        TCPIP_NETWORK_DEFAULT_POWER_MODE_IDX1,           // powerMode
        TCPIP_NETWORK_DEFAULT_INTERFACE_FLAGS_IDX1,      // startFlags
    },
</#if>
};

const TCPIP_STACK_MODULE_CONFIG TCPIP_STACK_MODULE_CONFIG_TBL [] =
{
<#if CONFIG_TCPIP_STACK_USE_IPV4 == true>
    {TCPIP_MODULE_IPV4,          0},
</#if>
<#if CONFIG_TCPIP_STACK_USE_ICMP_CLIENT == true || CONFIG_TCPIP_STACK_USE_ICMP_SERVER == true>
    {TCPIP_MODULE_ICMP,          0},                           // TCPIP_MODULE_ICMP
</#if>
    {TCPIP_MODULE_ARP,           &tcpipARPInitData},              // TCPIP_MODULE_ARP
<#if CONFIG_TCPIP_STACK_USE_IPV6 == true>
    {TCPIP_MODULE_IPV6,          &tcpipIPv6InitData},                           // TCPIP_MODULE_IPV6
    {TCPIP_MODULE_ICMPV6,        0},                           // TCPIP_MODULE_ICMPV6
    {TCPIP_MODULE_NDP,           0},                           // TCPIP_MODULE_NDP
</#if>
<#if CONFIG_TCPIP_USE_UDP == true>
    {TCPIP_MODULE_UDP,           &tcpipUDPInitData},              // TCPIP_MODULE_UDP,
</#if>
<#if CONFIG_TCPIP_USE_TCP == true>
    {TCPIP_MODULE_TCP,           &tcpipTCPInitData},              // TCPIP_MODULE_TCP,
</#if>
<#if CONFIG_TCPIP_DHCP_CLIENT_ENABLED == true>
    {TCPIP_MODULE_DHCP_CLIENT,   &tcpipDHCPInitData},             // TCPIP_MODULE_DHCP_CLIENT,
</#if>
<#if CONFIG_TCPIP_STACK_USE_DHCP_SERVER == true>
    {TCPIP_MODULE_DHCP_SERVER,   &tcpipDHCPSInitData},                           // TCPIP_MODULE_DHCP_SERVER,
</#if>
<#if CONFIG_TCPIP_USE_ANNOUNCE == true>
    {TCPIP_MODULE_ANNOUNCE,      &tcpipAnnounceInitData},                     // TCPIP_MODULE_ANNOUNCE,
</#if>
<#if CONFIG_TCPIP_USE_DNS_CLIENT == true>
    {TCPIP_MODULE_DNS_CLIENT,&tcpipDNSClientInitData}, // TCPIP_MODULE_DNS_CLIENT,
</#if>
<#if CONFIG_TCPIP_USE_DNSS == true>
    {TCPIP_MODULE_DNS_SERVER,&tcpipDNSServerInitData}, // TCPIP_MODULE_DNS_SERVER,
</#if>
<#if CONFIG_TCPIP_USE_NBNS == true>
    {TCPIP_MODULE_NBNS,          &tcpipNBNSInitData},                           // TCPIP_MODULE_NBNS
</#if>
<#if CONFIG_TCPIP_USE_SNTP_CLIENT == true>
    {TCPIP_MODULE_SNTP,    &tcpipSNTPInitData},                            // TCPIP_MODULE_SNTP,
</#if>

<#if CONFIG_TCPIP_STACK_USE_BERKELEY_API == true>
    {TCPIP_MODULE_BERKELEY,      &tcpipBerkeleyInitData},                           // TCPIP_MODULE_BERKELEY,
</#if>
<#if CONFIG_TCPIP_STACK_USE_HTTP_SERVER == true>
    {TCPIP_MODULE_HTTP_SERVER,   &tcpipHTTPInitData},              // TCPIP_MODULE_HTTP_SERVER,
</#if>
<#if CONFIG_TCPIP_USE_TELNET == true>
    {TCPIP_MODULE_TELNET_SERVER,   &tcpipTelnetInitData},                        // TCPIP_MODULE_TELNET_SERVER,
</#if>
<#if CONFIG_TCPIP_STACK_USE_SSL_SERVER == true || CONFIG_TCPIP_STACK_USE_SSL_CLIENT == true>
    {TCPIP_MODULE_RSA,           0},                           // TCPIP_MODULE_RSA,
    {TCPIP_MODULE_SSL,           &tcpipSSLInitData},                           // TCPIP_MODULE_SSL,
</#if>
<#if CONFIG_TCPIP_USE_FTP_MODULE == true>
    {TCPIP_MODULE_FTP_SERVER,    &tcpipFTPInitData},                           // TCPIP_MODULE_FTP,
</#if>
<#if CONFIG_TCPIP_USE_SNMP == true>
    {TCPIP_MODULE_SNMP_SERVER,   &tcpipSNMPInitData},                           // TCPIP_MODULE_SNMP_SERVER,
</#if>
<#if CONFIG_TCPIP_USE_DDNS == true>
    {TCPIP_MODULE_DYNDNS_CLIENT, &tcpipDDNSInitData},                           // TCPIP_MODULE_DYNDNS_CLIENT,
</#if>
<#if CONFIG_TCPIP_USE_REBOOT_SERVER == true>
    {TCPIP_MODULE_REBOOT_SERVER, 0},                           // TCPIP_MODULE_REBOOT_SERVER,
</#if>
<#if CONFIG_TCPIP_USE_LINK_ZERO_CONFIG == true>
    {TCPIP_MODULE_ZCLL, 0},                                    // TCPIP_MODULE_ZCLL,
</#if>
<#if CONFIG_TCPIP_USE_MULTI_CAST_DNS_ZERO_CONFIG == true>
    {TCPIP_MODULE_MDNS, 0},                                    // TCPIP_MODULE_MDNS,
</#if>

    // MAC modules
<#if (CONFIG_TCPIP_STACK_NETWORK_CONFIG_IDX0 && CONFIG_TCPIP_NETWORK_DEFAULT_INTERFACE_NAME_IDX0 == "PIC32INT") || (CONFIG_TCPIP_STACK_NETWORK_CONFIG_IDX1 && CONFIG_TCPIP_NETWORK_DEFAULT_INTERFACE_NAME_IDX1 == "PIC32INT")>
    {TCPIP_MODULE_MAC_PIC32INT, &tcpipMACPIC32INTInitData},     // TCPIP_MODULE_MAC_PIC32INT
</#if>
<#--#if (CONFIG_TCPIP_STACK_NETWORK_CONFIG_IDX0 && CONFIG_TCPIP_NETWORK_DEFAULT_INTERFACE_NAME_IDX0 == "MRF24W") || (CONFIG_TCPIP_STACK_NETWORK_CONFIG_IDX1 && CONFIG_TCPIP_NETWORK_DEFAULT_INTERFACE_NAME_IDX1 == "MRF24W")>
    {TCPIP_MODULE_MAC_MRF24W, &macMRF24WConfigData},         // TCPIP_MODULE_MAC_MRF24W
</#if-->
};

/*********************************************************************
 * Function:        SYS_MODULE_OBJ TCPIP_STACK_Init()
 *
 * PreCondition:    None
 *
 * Input:
 *
 * Output:          valid system module object if Stack and its componets are initialized
 *                  SYS_MODULE_OBJ_INVALID otherwise
 *
 * Overview:        The function starts the initialization of the stack.
 *                  If an error occurs, the SYS_ERROR() is called
 *                  and the function de-initialize itself and will return false.
 *
 * Side Effects:    None
 *
 * Note:            This function must be called before any of the
 *                  stack or its component routines are used.
 *
 ********************************************************************/


SYS_MODULE_OBJ TCPIP_STACK_Init()
{
    TCPIP_STACK_INIT    tcpipInit;

    tcpipInit.moduleInit.sys.powerState = SYS_MODULE_POWER_RUN_FULL;
    tcpipInit.pNetConf = TCPIP_HOSTS_CONFIGURATION;
    tcpipInit.nNets = sizeof (TCPIP_HOSTS_CONFIGURATION) / sizeof (*TCPIP_HOSTS_CONFIGURATION);
    tcpipInit.pModConfig = TCPIP_STACK_MODULE_CONFIG_TBL;
    tcpipInit.nModules = sizeof (TCPIP_STACK_MODULE_CONFIG_TBL) / sizeof (*TCPIP_STACK_MODULE_CONFIG_TBL);

    return TCPIP_STACK_Initialize(0, &tcpipInit.moduleInit);
}

<#if CONFIG_PIC32MZ == true>
/*********************************************************************
 * Function:        static void APP_ETHMAC_PinInitialize(void)
 *
 * PreCondition:    None
 *
 * Input:	    None
 *
 * Output:          Sets the Ethernet MAC pins
 *
 * Overview:        This function sets the MAC pin configurations 
 ********************************************************************/
static void APP_ETHMAC_PinInitialize(void)
{
    //MDC
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_OUTPUT,
                                 PORT_CHANNEL_D,
                                 PORTS_BIT_POS_11);
    //MDIO
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_J,
                                 PORTS_BIT_POS_1);
    //TXEN
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_OUTPUT,
                                 PORT_CHANNEL_D,
                                 PORTS_BIT_POS_6);
    //TXD0
    SYS_PORTS_PinModeSelect(PORTS_ID_0, PORTS_ANALOG_PIN_35, PORTS_PIN_MODE_DIGITAL);
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_OUTPUT,
                                 PORT_CHANNEL_J,
                                 PORTS_BIT_POS_8);
    //TXD1
    SYS_PORTS_PinModeSelect(PORTS_ID_0, PORTS_ANALOG_PIN_36, PORTS_PIN_MODE_DIGITAL);
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_OUTPUT,
                                 PORT_CHANNEL_J,
                                 PORTS_BIT_POS_9);
    //RXCLK
    SYS_PORTS_PinModeSelect(PORTS_ID_0, PORTS_ANALOG_PIN_37, PORTS_PIN_MODE_DIGITAL);
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_J,
                                 PORTS_BIT_POS_11);
    //RXDV
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_H,
                                 PORTS_BIT_POS_13);
    //RXD0
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_H,
                                 PORTS_BIT_POS_8);
    //RXD1
    SYS_PORTS_PinModeSelect(PORTS_ID_0, PORTS_ANALOG_PIN_41, PORTS_PIN_MODE_DIGITAL);
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_H,
                                 PORTS_BIT_POS_5);

    //RXERR
    SYS_PORTS_PinModeSelect(PORTS_ID_0, PORTS_ANALOG_PIN_40, PORTS_PIN_MODE_DIGITAL);
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_H,
                                 PORTS_BIT_POS_4);
}
<#elseif CONFIG_PIC32MX == true>
/*********************************************************************
 * Function:        static void APP_ETHMAC_PinInitialize(void)
 *
 * PreCondition:    None
 *
 * Input:	    None
 *
 * Output:          Sets the Ethernet MAC pins
 *
 * Overview:        This function sets the MAC pin configurations 
 ********************************************************************/
static void APP_ETHMAC_PinInitialize(void)
{
    //MDC
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_OUTPUT,
                                 PORT_CHANNEL_D,
                                 PORTS_BIT_POS_11);
    //MDIO
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_D,
                                 PORTS_BIT_POS_8);
    //TXEN
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_OUTPUT,
                                 PORT_CHANNEL_A,
                                 PORTS_BIT_POS_15);
    //TXD0
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_OUTPUT,
                                 PORT_CHANNEL_D,
                                 PORTS_BIT_POS_14);
    //TXD1
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_OUTPUT,
                                 PORT_CHANNEL_D,
                                 PORTS_BIT_POS_15);
    //RXCLK
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_G,
                                 PORTS_BIT_POS_9);
    //RXDV
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_G,
                                 PORTS_BIT_POS_8);
    //RXD0
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_E,
                                 PORTS_BIT_POS_8);
    //RXD1
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_E,
                                 PORTS_BIT_POS_9);

    //RXERR
    SYS_PORTS_PinDirectionSelect(PORTS_ID_0,
                                 SYS_PORTS_DIRECTION_INPUT,
                                 PORT_CHANNEL_G,
                                 PORTS_BIT_POS_15);
}
</#if>

<#--
/*******************************************************************************
 End of File
*/
-->

