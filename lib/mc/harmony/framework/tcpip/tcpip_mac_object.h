/*******************************************************************************
  Multiple MAC Module implementation for Microchip Stack

  Company:
    Microchip Technology Inc.
    
  File Name:
    tcpip_mac_object.h

  Summary:
    
  Description:
*******************************************************************************/
// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright � 2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

#ifndef _TCPIP_MAC_OBJECT_H_ 
#define _TCPIP_MAC_OBJECT_H_ 

#include "tcpip/tcpip_mac.h"
#include "osal/osal.h"


/************************************
 *  the Harmony PIC32 MAC parameterized interface implementation
 *************************************/

typedef struct
{
    void                (*TCPIP_MAC_Close)(DRV_HANDLE hMac);
    bool                (*TCPIP_MAC_LinkCheck)(DRV_HANDLE hMac);
    TCPIP_MAC_RES       (*TCPIP_MAC_RxFilterHashTableEntrySet)(DRV_HANDLE hMac, TCPIP_MAC_ADDR* DestMACAddr);
    bool 	            (*TCPIP_MAC_PowerMode)(DRV_HANDLE hMac, TCPIP_MAC_POWER_MODE pwrMode);
    TCPIP_MAC_RES       (*TCPIP_MAC_PacketTx)(DRV_HANDLE hMac, TCPIP_MAC_PACKET * ptrPacket);
    TCPIP_MAC_PACKET*   (*TCPIP_MAC_PacketRx)(DRV_HANDLE hMac, TCPIP_MAC_RES* pRes, const TCPIP_MAC_PACKET_RX_STAT** ppPktStat);
    TCPIP_MAC_RES       (*TCPIP_MAC_Process)(DRV_HANDLE hMac);
    TCPIP_MAC_RES       (*TCPIP_MAC_StatisticsGet)(DRV_HANDLE hMac, TCPIP_MAC_RX_STATISTICS* pRxStatistics, TCPIP_MAC_TX_STATISTICS* pTxStatistics);
    TCPIP_MAC_RES       (*TCPIP_MAC_ParametersGet)(DRV_HANDLE hMac, TCPIP_MAC_PARAMETERS* pMacParams);
    TCPIP_MAC_RES       (*TCPIP_MAC_RegisterStatisticsGet)(DRV_HANDLE hMac, TCPIP_MAC_STATISTICS_REG_ENTRY* pRegEntries, int nEntries, int* pHwEntries);


    bool                (*TCPIP_MAC_EventMaskSet)(DRV_HANDLE hMac, TCPIP_MAC_EVENT macEvents, bool enable);
    bool                (*TCPIP_MAC_EventAcknowledge)(DRV_HANDLE hMac, TCPIP_MAC_EVENT macEvents);
    TCPIP_MAC_EVENT     (*TCPIP_MAC_EventPendingGet)(DRV_HANDLE hMac);

    OSAL_SEM_HANDLE_TYPE semaphore;     // Semaphore information.
 
}TCPIP_MAC_OBJECT;        // TCPIP MAC object descriptor

typedef struct
{
    const TCPIP_MAC_OBJECT* pObj;       // associated object
                                        // pointer to the object here is intended to allow
                                        // multiple MAC objects of the same type
                                        // to share an unique const object table
    void*               mac_data[0];    // specific MAC object data
}TCPIP_MAC_DCPT; 
    
// supported MAC objects

SYS_MODULE_OBJ      DRV_ETHMAC_PIC32MACInitialize(const SYS_MODULE_INDEX index, const SYS_MODULE_INIT * const init);
void                DRV_ETHMAC_PIC32MACDeinitialize(SYS_MODULE_OBJ object);
SYS_STATUS          DRV_ETHMAC_PIC32MACStatus ( SYS_MODULE_OBJ object );
DRV_HANDLE          DRV_ETHMAC_PIC32MACOpen(const SYS_MODULE_INDEX drvIndex, const DRV_IO_INTENT intent);
void                DRV_ETHMAC_PIC32MACTasks(SYS_MODULE_OBJ object);

size_t              DRV_ETHMAC_PIC32MACGetConfig(TCPIP_STACK_MODULE modId, void* configBuff, size_t buffSize, size_t* pConfigSize);


SYS_MODULE_OBJ      MRF24W_MACInitialize(const SYS_MODULE_INDEX index, const SYS_MODULE_INIT * const init);
void                MRF24W_MACDeinitialize(SYS_MODULE_OBJ object);
SYS_STATUS          MRF24W_MACStatus ( SYS_MODULE_OBJ object );
DRV_HANDLE          MRF24W_MACOpen(const SYS_MODULE_INDEX drvIndex, const DRV_IO_INTENT intent);
void                MRF24W_MACTasks(SYS_MODULE_OBJ object);

size_t              MRF24W_MACGetConfig(TCPIP_STACK_MODULE modId, void* configBuff, size_t buffSize, size_t* pConfigSize);



#endif  // _TCPIP_MAC_OBJECT_H_ 

