/*******************************************************************************
  Interrupt System Service Interface Definition

  Company:
    Microchip Technology Inc.

  File Name:
    sys_int_private.h

  Summary:
    Interrupt System Service.
    
  Description:
    This file contains the interface definition for the Interrupt System
    Service.  It provides a way to interact with the interrupt sub-system to
    manage the occurance of interrupts for sources supported by the system.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

#ifndef _SYS_INT_PRIVATE_H
#define _SYS_INT_PRIVATE_H


// *****************************************************************************
// *****************************************************************************
// Section: Version Numbers
// *****************************************************************************
// *****************************************************************************
/* Versioning of the System service */

// *****************************************************************************
/* INT System service Version Macros

  Summary:
    INT System service version.

  Description:
    These constants provide INT System service version information. The System
    service version is:
    SYS_INT_VERSION_MAJOR.SYS_INT_VERSION_MINOR.SYS_INT_VERSION_PATCH.
    It is represented in SYS_INT_VERSION as:
    MAJOR *10000 + MINOR * 100 + PATCH, so as to allow comparisons.
    It is also represented in string format in _SYS_INT_VERSION_STR.
    _SYS_INT_VERSION_TYPE provides the type of the release when the release is
    alpha or beta. The interfaces SYS_INT_VersionGet and SYS_INT_VersionStrGet
    provide interfaces to the access the version and the version string.

  Remarks:
    Modify the return value of SYS_INT_VersionStrGet and _SYS_INT_VERSION_MAJOR,
    _SYS_INT_VERSION_MINOR, _SYS_INT_VERSION_PATCH, and _SYS_INT_VERSION_TYPE.
*/

#define _SYS_INT_VERSION_MAJOR         0
#define _SYS_INT_VERSION_MINOR         5
#define _SYS_INT_VERSION_PATCH         0
#define _SYS_INT_VERSION_TYPE          "beta"
#define _SYS_INT_VERSION_STR           "0.50 beta"


// *****************************************************************************
// *****************************************************************************
// Section: Includes
// *****************************************************************************
// *****************************************************************************

#ifdef __PIC32MX__

#include "peripheral/int/plib_int_private_pic32.h"

#endif


// *****************************************************************************
// *****************************************************************************
// Section: Extern Declarations
// *****************************************************************************
// *****************************************************************************

extern SYS_INT_TASKS_POINTER _SYS_INT_isr[INT_IRQ_MAX];

extern SYS_MODULE_OBJ _SYS_INT_taskparam_source[INT_IRQ_MAX];



#endif // _SYS_INT_PRIVATE_H

