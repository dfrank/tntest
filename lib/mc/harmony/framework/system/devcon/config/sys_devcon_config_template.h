/*******************************************************************************
  Device Control System Service Configuration Templates

  Company:
    Microchip Technology Inc.

  File Name:
    sys_devcon_config_template.h

  Summary:
    Device Control System Service configuration templates.

  Description:
    This file contains constants to configure the Device Control System Service.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

#ifndef _SYS_DEVCON_CONFIG_TEMPLATE_H
#define _SYS_DEVCON_CONFIG_TEMPLATE_H


// *****************************************************************************
/* System Clock frequency

  Summary:
    Sets up the System clock frequency.

  Description:
    This macro sets up the System clock frequency.

  Remarks:
    None.
*/

#define SYS_DEVCON_SYSTEM_CLOCK                                       200000000L


// *****************************************************************************
/* PIC32MX Max Peripheral Bus Frequency

  Summary:
    Defines the maximum peripheral bus clock frequency for PIC32MX.

  Description:
    This macro defines the maximum peripheral bus clock frequency for PIC32MX.


  Remarks:
    None.
*/
#define SYS_DEVCON_PIC32MX_MAX_PB_FREQ                                 80000000L


#endif //_SYS_DEVCON_CONFIG_TEMPLATE_H

/*******************************************************************************
 End of File
*/

