/*******************************************************************************
  Command Processor System Service Interface Definition

  Company:
    Microchip Technology Inc.

  File Name:
    sys_command.h

  Summary:
    Command Processor System Service interface definition.

  Description:
    This file contains the interface definition for the Command Processor System
    Service.  It provides a way to interact with the Command Processor subsystem
    to manage the ASCII command requests from the user supported by the system.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

#ifndef _SYS_COMMAND_H
#define _SYS_COMMAND_H

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stdint.h>
#include <stdbool.h>
#include "system_config.h"

// *****************************************************************************
// *****************************************************************************
// Section: SYS CMD Data Types and Definitions
// *****************************************************************************
// *****************************************************************************

#ifndef BUFFER_DMA_READY
    #define BUFFER_DMA_READY
#endif

// *****************************************************************************
/* SYS CMD Processor Buffer Maximum Length

  Summary:
    Command Processor System Service Command Buffer Maximum Length definition.

  Description:
    This defines the maximum length of the command buffer.

  Remarks:
    None.

*/
#define         SYS_CMD_MAX_LENGTH  128


// *****************************************************************************
/* SYS CMD Processor Read Buffer Size

  Summary:
    Command Processor System Service Read Buffer Size definition.

  Description:
    This defines the maximum size of the command buffer.

  Remarks:
    None.

*/
#define         SYS_CMD_READ_BUFFER_SIZE 1


// *****************************************************************************
/* SYS CMD Processor Maximum Number of Command Group

  Summary:
    Command Processor System Service Maximum Number of Command Group defintions.

  Description:
    This defines the maximum nuber of command groups.

  Remarks:
    None.

*/
#define         MAX_CMD_GROUP   4


// *****************************************************************************
/* SYS CMD Processor Maximum Number of Command Arguments

  Summary:
    Command Processor System Service Maximum Number of Argument defintions.

  Description:
    This defines the maximum nuber of arguments per command.

  Remarks:
    None.

*/
#define         MAX_CMD_ARGS    15


// *****************************************************************************
/* SYS CMD Processor Command History Depth

  Summary:
    Command Processor System Service Maximum Depth of Command History.

  Description:
    This defines the maximum depth of the command history.

  Remarks:
    None.

*/
#define         COMMAND_HISTORY_DEPTH   3


// *****************************************************************************
/* SYS CMD Processor Command Terminal Support Definitions

  Summary:
    Command Processor System Service Terminal Support Definitions.

  Description:
    These definitions are used by the Command Processor System Service to
    support VT100 ASCII terminal.

  Remarks:
    None.

*/
#define         LINE_TERM       "\r\n"          // line terminator
#define         CMD_TOKEN_SEP   ", \r\n"        // command token separator
#define         _promptStr      ">"             // prompt string
#define         ESC_SEQ_SIZE    2               // standard VT100 escape sequences


/************************************************************************
  Summary:
    Identifies a particular Command IO instance.
  Description:
    Command IO Handle

    This event handle identifies a registered instance of a Command IO object.
    Every time the application that tries to access the parameters with respect
    to a particular event, shall used this event handle to refer to that
    event.
  Remarks:
    None.
*/
typedef const void* _CMDIO_DEV_HANDLE;


/************************************************************************
  Summary:
    Identifies a message function API.
  Description:
    Message funciton API

    This handle identifies the interface structure of message function API
    within the Command IO encapsulation.
  Remarks:
    None.
*/
typedef void (*_CMDIO_DEV_MSG)(const void* cmdIoParam, const char* str);


/************************************************************************
  Summary:
    Identifies a print function API.
  Description:
    Print funciton API

    This handle identifies the interface structure of print function API
    within the Command IO encapsulation.
  Remarks:
    None.
*/
typedef void (*_CMDIO_DEV_PRINT)(const void* cmdIoParam, const char* format, ...);


/************************************************************************
  Summary:
    Identifies a single character print function API.
  Description:
    Single Character Print funciton API

    This handle identifies the interface structure of single character print
    function API within the Command IO encapsulation.
  Remarks:
    None.
*/
typedef void (*_CMDIO_DEV_PUTC)(const void* cmdIoParam, char c);


/************************************************************************
  Summary:
    Identifies a data available function API.
  Description:
    Ready Status Check funciton API

    This handle identifies the interface structure of data available
    function API within the Command IO encapsulation.
  Remarks:
    None.
*/
typedef bool (*_CMDIO_DEV_DATA_RDY)(const void* cmdIoParam);


/************************************************************************
  Summary:
    Identifies a get single character function API.
  Description:
    Get Single Character funciton API

    This handle identifies the interface structure of get single character
    function API within the Command IO encapsulation.
  Remarks:
    None.
*/
typedef char (*_CMDIO_DEV_GETC)(const void* cmdIoParam);


/************************************************************************
  Summary:
    Identifies a read single character function API.
  Description:
    Read Single Character funciton API

    This handle identifies the interface structure of read single character
    function API within the Command IO encapsulation.
  Remarks:
    None.
*/
typedef size_t (*_CMDIO_DEV_READC)(const void* cmdIoParam);


// *****************************************************************************
/* SYS CMD API structure

  Summary:
    Identifies the Command API structure.

  Description:
    This structure identifies the Command API structure.

  Remarks:
    None.
*/
typedef struct
{
    // Message function API
    _CMDIO_DEV_MSG msg;
	
    // Print function API
    _CMDIO_DEV_PRINT print;

    // Put single char function API
    _CMDIO_DEV_PUTC  putc;

    // Data available API
    _CMDIO_DEV_DATA_RDY  isRdy;

    // Get single data API
    _CMDIO_DEV_GETC      getc;

    // Read single data API
    _CMDIO_DEV_READC   readc;
    
}_CMDIO_DEV_API;


// *****************************************************************************
/* SYS CMD State Machine States

   Summary
    Defines the various states that can be achieved by a Command instance.

   Description
    This enumeration defines the various states that can be achieved by the
    command operation.

   Remarks:
    None.
*/
typedef enum
{
    CMDIO_STATE_DISABLE,

    CMDIO_STATE_SETUP_READ,

    CMDIO_STATE_WAIT_FOR_READ_DONE,

    CMDIO_STATE_ESC_SEQUENCE_READ,

    CMDIO_STATE_PROCESS_ESC_SEQUENCE

}_CMDIO_CMD_STATE;


// *****************************************************************************
/* SYS CMD Command Instance Node Structure

   Summary
    Defines the data structure to store each command instance.

   Description
    This data structure stores all the data relevant to a uniquely entered 
    command instance.  It is a node for a linked list structure to support the
    Command Processor Sysetm Service's command history feature

   Remarks:
    None.
*/
typedef struct _CMDIO_DEV_NODE
{
	char*            cmdPnt;
	char*            cmdEnd;
	char             cmdBuff[SYS_CMD_MAX_LENGTH+1];
	const _CMDIO_DEV_API*  pCmdApi;	// Cmd IO APIs
        const void*         cmdIoParam; // channel specific parameter
	struct _CMDIO_DEV_NODE* next;
        _CMDIO_CMD_STATE  cmdState;

        SYS_MODULE_INDEX  moduleIndices[SYS_CONSOLE_DEVICE_MAX_INSTANCES];
        int               moduleInFd;
        int               moduleOutFd;        
}_CMDIO_DEV_NODE;


// *****************************************************************************
/* SYS CMD Command List Structure

   Summary
    Defines the list structure to store a list of command instances.

   Description
    This data structure defines he linked list structure to support the
    Command Processor Sysetm Service's command history feature

   Remarks:
    None.
*/
typedef struct
{
	int num;
	_CMDIO_DEV_NODE* head;
	_CMDIO_DEV_NODE* tail;
}_CMDIO_DEV_LST;


// *****************************************************************************
/* SYS CMD Command App Init Structure

   Summary
    Defines the data structure to store each command.

   Description
    This data structure stores all the data relevant to a uniquely entered
    command.  It is a node for a linked list structure to support the command
    history functionality

   Remarks:
    None.
*/
typedef struct
{
        size_t          bytesRead;
        int             seqBytesRead;
        char            seqBuff[ESC_SEQ_SIZE + 1];
        char            readBuff[SYS_CMD_READ_BUFFER_SIZE] BUFFER_DMA_READY;
}_CMDIO_APP_DATA;


// *****************************************************************************
// *****************************************************************************
// Section: SYS CMD External Interface Definitions
// *****************************************************************************
// *****************************************************************************

/************************************************************************
/* SYS CMD Command Function

  Summary:
    Identifies the command process function API.

  Description:
    Command Process Funciton API

    This handle identifies the interface structure of command process
    function API
  Remarks:
    None.
*/
typedef int (*_SYS_CMD_FNC)(_CMDIO_DEV_NODE* pCmdIO, int argc, char **argv);


// *****************************************************************************
/* SYS CMD Command Descriptor Structure

   Summary
    Defines the descriptor structure for a single command process.

   Description
    This data structure stores the information for a single unique command
    process.

   Remarks:
    None.
*/
typedef struct
{
    const char*     cmdStr;         // string identifying the command
    _SYS_CMD_FNC    cmdFnc;         // function to execute for this command
    const char*     cmdDescr;       // simple command description
}_SYS_CMD_DCPT;                     // a simple command descriptor


// *****************************************************************************
/* SYS CMD Command Procss Table Structure

   Summary
    Defines the command table structure for the Command Processor
    System Service.

   Description
    This data structure stores is used to store a list of command process

   Remarks:
    None.
*/
typedef struct
{
    int                     nCmds;          // number of commands available in the table
    const _SYS_CMD_DCPT*    pCmd;           // pointer to an array of command descriptors
    const char*             cmdGroupName;   // name identifying the commands
    const char*             cmdMenuStr;     // help string
}_SYS_CMD_DCPT_TBL;                         // table containing the supported commands


// *****************************************************************************
// *****************************************************************************
// Section: SYS CMD Operation Routines
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Function:
    bool SYS_COMMAND_Initialize( void )

  Summary:
    Initializes data for the instance of the Command Processor module.

  Description:
    This function initializes the Command Processor module.
    It also initializes any internal data structures.

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    If successful, returns true.
    If there is an error, returns false.

  Remarks:
    This routine should only be called once during system initialization.
*/
bool SYS_COMMAND_INIT(void);


// *****************************************************************************
/* Function:
    bool SYS_COMMAND_ADDGRP(const _SYS_CMD_DCPT* pCmdTbl, int nCmds,
                            const char* groupName,
                            const char* menuStr)

  Summary:
    API to allow clients to add command process to the Command Processor System
    Service.

  Description:
    Client modules may call this function to add command process to the Command
    Processor System Service

  Precondition:
    SYS_COMMAND_Initialize was successfully run once.

  Parameters:
    None.

  Example:

    static const _SYS_CMD_DCPT    cmdTbl[]=
    {
        {"command_as_typed_at_the_prompt",
        _Function_Name_That_Supports_The_Command,
        ": Helpful description of the command for the user"},
    };

    bool APP_AddCommandFunction()
    {
        if (!SYS_COMMAND_ADDGRP(cmdTbl, sizeof(cmdTbl)/sizeof(*cmdTbl),
                        "Command Group Name", ": Command Group Description"))
        {
            return false;
        }
        return true;
    }

  Returns:
    If successful, returns true.
    If there is an error, returns false.

  Remarks:
    None.
*/
bool    SYS_COMMAND_ADDGRP(const _SYS_CMD_DCPT* pCmdTbl, int nCmds, const char* groupName, const char* menuStr);

// *****************************************************************************
/* Function:
    bool SYS_COMMAND_READY_TO_READ( void )

  Summary:
    This function allows upper layer application to confirm that the command
    module is ready to accept command input

  Description:
    This function allows upper layer application to confirm that the command
    module is ready to accept command input

  Precondition:
    SYS_COMMAND_Initialize was successfully run once.

  Parameters:
    None.

  Returns:
    If ready, returns true.
    If not ready, returns false.

  Remarks:
    None.
*/
bool    SYS_COMMAND_READY_TO_READ( void );

// *****************************************************************************
/* Function:
    bool SYS_COMMAND_READY_TO_WRITE( void )

  Summary:
    This function allows upper layer application to confirm that the command
    module is ready to write output to the Console System Service

  Description:
    This function allows upper layer application to confirm that the command
    module is ready to write output to the Console System Service

  Precondition:
    SYS_COMMAND_Initialize was successfully run once.

  Parameters:
    None.

  Returns:
    If ready, returns true.
    If not ready, returns false.

  Remarks:
    None.
*/
bool    SYS_COMMAND_READY_TO_WRITE( void );

// *****************************************************************************
/* Function:
    bool SYS_COMMAND_TASK( void )

  Summary:
    Maintains the Command Processor System Service's internal state machine.

  Description:
    This function is used to maintain the Command Processor System Service
    internal state machine.

  Precondition:
    SYS_COMMAND_Initialize was successfully run once.

  Parameters:
    None.

  Returns:
    If successfully, returns true.
    If there is an error, returns false.

  Remarks:
    None.
*/
bool     SYS_COMMAND_TASK(void);


// *****************************************************************************
/* Function:
    void SYS_COMMAND_MESSAGE (const char* message)

  Summary:
    Outputs a message to the Command Processor System Service console.

  Description:
    This function outputs a message to the Command Processor System Service
    console.
.
  Precondition:
    SYS_COMMAND_Initialize was successfully run once.

  Parameters:
    None.

  Returns:
    None.

  Remarks:
    None.
*/
void    SYS_COMMAND_MESSAGE(const char* message);


// *****************************************************************************
/* Function:
    void SYS_COMMAND_PRINT(const char *format, ...)

  Summary:
    Outputs a printout to the Command Processor System Service console.

  Description:
    This function outputs a printout to the Command Processor System Service
    console.
.
  Precondition:
    SYS_COMMAND_Initialize was successfully run once.

  Parameters:
    None.

  Returns:
    None.

  Remarks:
    None.
*/
void    SYS_COMMAND_PRINT(const char *format, ...);


// *****************************************************************************
/* TODO: Do not include in help.  They will be refactored in the next release */
_CMDIO_DEV_NODE* SYS_CMDIO_GET_HANDLE(short num);
_CMDIO_DEV_NODE* SYS_CMDIO_ADD(const _CMDIO_DEV_API* opApi, const void* cmdIoParam);
bool SYS_CMDIO_DELETE(_CMDIO_DEV_NODE* pDevNode);

#endif


