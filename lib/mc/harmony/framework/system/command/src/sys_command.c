/*******************************************************************************
  Command Processor System Service Implementation

  Company:
    Microchip Technology Inc.

  File Name:
    sys_command.c

  Summary:
    Command Processor System Service Implementation.

  Description:
    This file contains the source code for the Command Processor System
    Service.  It provides a way to interact with the Command Processor subsystem
    to manage the ASCII command requests from the user supported by the system.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include "system_config.h"
#include "system/reset/sys_reset.h"
#include "system/command/sys_command.h"
#include "system/debug/sys_debug.h"
#include "system/console/sys_console.h"

// *****************************************************************************
// *****************************************************************************
// Section: Type Definitions
// *****************************************************************************
// *****************************************************************************

typedef struct _tagCmdNode
{
    struct _tagCmdNode* next;
    struct _tagCmdNode* prev;
    char    cmdBuff[SYS_CMD_MAX_LENGTH+1];  // command itself
}cmdNode;   // simple command history

typedef struct
{
    cmdNode*    head;
    cmdNode*    tail;
}cmdDlList;     // doubly linked command list

// *****************************************************************************
// *****************************************************************************
// Section: Global Variable Definitions
// *****************************************************************************
// *****************************************************************************

extern SYS_MODULE_OBJ sysConsoleObjects[];

static _CMDIO_DEV_LST cmdIODevList = {0, 0, 0};

static char printBuff[SYS_COMMAND_PRINT_BUFFER_SIZE];
static int printBuffPtr = 0;

static _CMDIO_APP_DATA _cmdAppData =
{
    .bytesRead = 0,
    .seqBytesRead = 0,
};

// data
static char             _cmdAlive = 0;

static _SYS_CMD_DCPT_TBL   _usrCmdTbl[MAX_CMD_GROUP] = { {0} };    // current command table

static cmdDlList        _cmdList = {0, 0};       // root of the command history

static cmdNode*         _pCurrCmdN;      // history pointer

static const char       _seqUpArrow[ESC_SEQ_SIZE] = "[A";
static const char       _seqDownArrow[ESC_SEQ_SIZE] = "[B";
static const char       _seqRightArrow[ESC_SEQ_SIZE] = "[C";
static const char       _seqLeftArrow[ESC_SEQ_SIZE] = "[D";  // standard VT100 escape sequences

// prototypes

static int      CommandReset(_CMDIO_DEV_NODE* pCmdIO, int argc, char** argv);
static int      CommandQuit(_CMDIO_DEV_NODE* pCmdIO, int argc, char** argv);              // command quit
static int      CommandHelp(_CMDIO_DEV_NODE* pCmdIO, int argc, char** argv);              // help
static void     CommandCleanup(void);           // resources clean-up

static int      StringToArgs(char *pRawString, char *argv[]); // Convert string to argc & argv[]
static bool     ParseCmdBuffer(_CMDIO_DEV_NODE* pCmdIO);      // parse the command buffer


static void     ProcessEscSequence(_CMDIO_DEV_NODE* pCmdIO);       // process an escape sequence
//static void     DisplayPrompt(void);

static void     CmdAddHead(cmdDlList* pL, cmdNode* pN);
static cmdNode* CmdRemoveTail(cmdDlList* pL);

static void     DisplayNodeMsg(_CMDIO_DEV_NODE* pCmdIO, cmdNode* pNext);

static void SYS_CMD_READCOMPLETE(void *arg);
static void SYS_CMD_WRITECOMPLETE(void *arg);
static void SYS_CMD_MESSAGE(const void* cmdIoParam, const char* str);
static void SYS_CMD_PRINT(const void* cmdIoParam, const char* format, ...);
static void SYS_CMD_PUTC(const void* cmdIoParam, char c);
static bool SYS_CMD_DATA_RDY(const void* cmdIoParam);
static char SYS_CMD_GETC(const void* cmdIoParam);
static size_t SYS_CMD_READC(const void* cmdIoParam);

const _CMDIO_DEV_API sysConsoleApi =
{
    SYS_CMD_MESSAGE,
    SYS_CMD_PRINT,
    SYS_CMD_PUTC,
    SYS_CMD_DATA_RDY,
    SYS_CMD_GETC,
    SYS_CMD_READC,
};

// built-in command table
static const _SYS_CMD_DCPT    _builtinCmdTbl[]=
{
    {"reset",   CommandReset,   ": Reset host"},
    {"q",       CommandQuit,    ": quit command processor"},
    {"help",    CommandHelp,    ": help"},
};

// *****************************************************************************
// *****************************************************************************
// Section: SYS CMD Operation Routines
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Function:
    bool SYS_COMMAND_Initialize( void )

  Summary:
    Initializes data for the instance of the Command Processor module.

  Description:
    This function initializes the Command Processor module.
    It also initializes any internal data structures.

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    If successful, returns true.
    If there is an error, returns false.

  Remarks:
    This routine should only be called once during system initialization.
*/
bool SYS_COMMAND_INIT(void)
{
    int         ix;

    CommandCleanup();       // just in case we have to deallocate previous data

    // construct the command history list
    for(ix = 0; ix<COMMAND_HISTORY_DEPTH; ix++)
    {
        cmdNode* pN;
        pN = (cmdNode*)malloc(sizeof(*pN));

        if(!pN)
        {
            return false;
        }
        pN->cmdBuff[0] = '\0';
        CmdAddHead(&_cmdList, pN);
    }
    _pCurrCmdN = 0;

    // the console handle should be needed here but there's only one console for now
    SYS_CMDIO_ADD(&sysConsoleApi, 0);

    //SYS_MESSAGE(_promptStr);
    _cmdAlive = true;

    return true;
}

// *****************************************************************************
/* Function:
    bool SYS_COMMAND_READY_TO_WRITE( void )

  Summary:
    This function allows upper layer application to confirm that the command
    module is ready to write output to the Console System Service

  Description:
    This function allows upper layer application to confirm that the command
    module is ready to write output to the Console System Service

  Precondition:
    SYS_COMMAND_Initialize was successfully run once.

  Parameters:
    None.

  Returns:
    If ready, returns true.
    If not ready, returns false.

  Remarks:
    None.
*/
bool SYS_COMMAND_READY_TO_WRITE()
{
    short i;
    _CMDIO_DEV_NODE* pCmdIO;
    const void*      cmdIoParam;

    for (i=0; i<cmdIODevList.num; i++)
    {
        pCmdIO = SYS_CMDIO_GET_HANDLE(i);

        if(pCmdIO)
        {
            cmdIoParam = pCmdIO->cmdIoParam;

            //Check if this command IO is ready
            if(!(*pCmdIO->pCmdApi->isRdy)(cmdIoParam))
            {
                return false;
            }
        }
    }
    return true;
}

// *****************************************************************************
/* Function:
    bool SYS_COMMAND_READY_TO_READ( void )

  Summary:
    This function allows upper layer application to confirm that the command
    module is ready to accept command input

  Description:
    This function allows upper layer application to confirm that the command
    module is ready to accept command input

  Precondition:
    SYS_COMMAND_Initialize was successfully run once.

  Parameters:
    None.

  Returns:
    If ready, returns true.
    If not ready, returns false.

  Remarks:
    None.
*/
bool SYS_COMMAND_READY_TO_READ()
{
    short i;
    _CMDIO_DEV_NODE* pCmdIO;
    const void*      cmdIoParam;

    for (i=0; i<cmdIODevList.num; i++)
    {
        pCmdIO = SYS_CMDIO_GET_HANDLE(i);

        if(pCmdIO)
        {
            cmdIoParam = pCmdIO->cmdIoParam;

            //Check if this command IO is ready
            if(!(*pCmdIO->pCmdApi->isRdy)(cmdIoParam))
            {
                return false;
            }

            SYS_CONSOLE_Flush(SYS_CONSOLE_INDEX_0);

            (*pCmdIO->pCmdApi->msg)(pCmdIO, LINE_TERM LINE_TERM _promptStr);
            pCmdIO->cmdState = CMDIO_STATE_SETUP_READ;
        }
    }
    return true;
}

// *****************************************************************************
/* Function:
    bool SYS_COMMAND_ADDGRP(const _SYS_CMD_DCPT* pCmdTbl, int nCmds,
                            const char* groupName,
                            const char* menuStr)

  Summary:
    API to allow clients to add command process to the Command Processor System
    Service.

  Description:
    Client modules may call this function to add command process to the Command
    Processor System Service

  Precondition:
    SYS_COMMAND_Initialize was successfully run once.

  Parameters:
    None.

  Example:

    static const _SYS_CMD_DCPT    cmdTbl[]=
    {
        {"command_as_typed_at_the_prompt",
        _Function_Name_That_Supports_The_Command,
        ": Helpful description of the command for the user"},
    };

    bool APP_AddCommandFunction()
    {
        if (!SYS_COMMAND_ADDGRP(cmdTbl, sizeof(cmdTbl)/sizeof(*cmdTbl),
                        "Command Group Name", ": Command Group Description"))
        {
            return false;
        }
        return true;
    }

  Returns:
    If successful, returns true.
    If there is an error, returns false.

  Remarks:
    None.
*/
bool  SYS_COMMAND_ADDGRP(const _SYS_CMD_DCPT* pCmdTbl, int nCmds, const char* groupName, const char* menuStr)
{
    int i, groupIx = -1, emptyIx = -1;
    int insertIx;

    // Check if there is space for new command group; If this table already added, also simply update.
    for (i=0; i<MAX_CMD_GROUP; i++)
    {
        if(_usrCmdTbl[i].pCmd == 0)
        {   // empty slot
            emptyIx = i;
        }
        else if(_usrCmdTbl[i].pCmd == pCmdTbl)
        {   // already have this group; sanity check against the group name
            if(strcmp(groupName, _usrCmdTbl[i].cmdGroupName) != 0)
            {   // name mismatch
                return false;
            }

            groupIx = i;
            break;
        }
    }

    // reference the command group
    if (groupIx != -1)
    {
        insertIx = groupIx;
    }
    else if(emptyIx != -1)
    {
        insertIx = emptyIx;
    }
    else
    {
        return false;
    }

    _usrCmdTbl[insertIx].pCmd = pCmdTbl;
    _usrCmdTbl[insertIx].nCmds = nCmds;
    _usrCmdTbl[insertIx].cmdGroupName = groupName;
    _usrCmdTbl[insertIx].cmdMenuStr = menuStr;
    return true;

}

// *****************************************************************************
/* Function:
    bool SYS_COMMAND_TASK( void )

  Summary:
    Maintains the Command Processor System Service's internal state machine.

  Description:
    This function is used to maintain the Command Processor System Service
    internal state machine.

  Precondition:
    SYS_COMMAND_Initialize was successfully run once.

  Parameters:
    None.

  Returns:
    If successfully, returns true.
    If there is an error, returns false.

  Remarks:
    None.
*/
bool SYS_COMMAND_TASK(void)
{
    short i;
    _CMDIO_DEV_NODE* pCmdIO;

    if (_cmdAlive == false)
    {
       return false;
    }


    for (i=0; i<cmdIODevList.num; i++)
    {
        pCmdIO = SYS_CMDIO_GET_HANDLE(i);
        //DisplayPrompt();

        if(pCmdIO)
        {
            //Check if this command IO is ready
            if(!(*pCmdIO->pCmdApi->isRdy)(pCmdIO))
            {
                continue;
            }

            switch(pCmdIO->cmdState)
            {
                case CMDIO_STATE_DISABLE:
                    //App layer is not ready to process commands yet
                    break;
                case CMDIO_STATE_SETUP_READ:
                    {
                        _cmdAppData.bytesRead = (*pCmdIO->pCmdApi->readc)(pCmdIO); /* Read data from console. */
                        _cmdAppData.seqBytesRead = 0;

                        pCmdIO->cmdState = CMDIO_STATE_WAIT_FOR_READ_DONE;
                    }
                    break;
                case CMDIO_STATE_WAIT_FOR_READ_DONE:
                    {
                        if((_cmdAppData.readBuff[0] == '\r') || (_cmdAppData.readBuff[0] == '\n'))
                        {
                            pCmdIO->cmdState = CMDIO_STATE_SETUP_READ;

                            // new command assembled
                            if(pCmdIO->cmdPnt ==  pCmdIO->cmdBuff)
                            {   // just an extra \n or \r
                                (*pCmdIO->pCmdApi->msg)(pCmdIO, LINE_TERM _promptStr);
                                return true;
                            }
                            (*pCmdIO->pCmdApi->msg)(pCmdIO, LINE_TERM);
                            *pCmdIO->cmdPnt = 0;
                            pCmdIO->cmdPnt = pCmdIO->cmdEnd = pCmdIO->cmdBuff;

                            if(!ParseCmdBuffer(pCmdIO))
                            {
                                //Command not processed, show prompt
                                (*pCmdIO->pCmdApi->msg)(pCmdIO, _promptStr);
                            }
                            else
                            {
                                //Command being processed, temporarily disable prompt until command completes
                                pCmdIO->cmdState = CMDIO_STATE_DISABLE;
                            }

                            return true;
                        }
                        else if(_cmdAppData.readBuff[0] == '\b')
                        {
                            pCmdIO->cmdState = CMDIO_STATE_SETUP_READ;

                            if(pCmdIO->cmdPnt > pCmdIO->cmdBuff)
                            {
                                (*pCmdIO->pCmdApi->msg)(pCmdIO, "\b \b");
                                pCmdIO->cmdPnt--; pCmdIO->cmdEnd--;
                            }
                        }
                        else if(_cmdAppData.readBuff[0] == 0x1b)
                        {
                            // This is an escape sequence, start reading which cursor character
                            _cmdAppData.bytesRead = (*pCmdIO->pCmdApi->readc)(pCmdIO); /* Read data from console. */

                            pCmdIO->cmdState = CMDIO_STATE_ESC_SEQUENCE_READ;
                        }
                        else if(pCmdIO->cmdEnd-pCmdIO->cmdBuff<SYS_CMD_MAX_LENGTH)
                        {
                            pCmdIO->cmdState = CMDIO_STATE_SETUP_READ;

                            (*pCmdIO->pCmdApi->msg)(pCmdIO, _cmdAppData.readBuff);
                            *pCmdIO->cmdPnt++ = _cmdAppData.readBuff[0];
                            if(pCmdIO->cmdPnt > pCmdIO->cmdEnd)
                            {
                                pCmdIO->cmdEnd = pCmdIO->cmdPnt;
                            }
                        }
                        else
                        {
                            pCmdIO->cmdState = CMDIO_STATE_SETUP_READ;

                            (*pCmdIO->pCmdApi->msg)(pCmdIO, " *** Command Processor buffer exceeded. Retry. ***" LINE_TERM);
                            pCmdIO->cmdPnt = pCmdIO->cmdEnd = pCmdIO->cmdBuff;
                            (*pCmdIO->pCmdApi->msg)(pCmdIO, _promptStr);
                        }
                    }
                    break;
                case CMDIO_STATE_ESC_SEQUENCE_READ:
                    {
                        if (_cmdAppData.readBuff[0] == '[')
                        {
                            _cmdAppData.bytesRead = (*pCmdIO->pCmdApi->readc)(pCmdIO); /* Read data from console. */
                            _cmdAppData.seqBuff[_cmdAppData.seqBytesRead++] = _cmdAppData.readBuff[0];

                            //Reset the buffer
                            _cmdAppData.readBuff[0] = '\0';
                        }
                        else if (_cmdAppData.readBuff[0] != '\0')
                        {
                            _cmdAppData.seqBuff[_cmdAppData.seqBytesRead++] = _cmdAppData.readBuff[0];
                            _cmdAppData.seqBuff[_cmdAppData.seqBytesRead] = '\0';
                            pCmdIO->cmdState = CMDIO_STATE_PROCESS_ESC_SEQUENCE;
                        }
                    }
                    break;
                case CMDIO_STATE_PROCESS_ESC_SEQUENCE:
                    {
                        ProcessEscSequence(pCmdIO);

                        pCmdIO->cmdState = CMDIO_STATE_SETUP_READ;
                        break;
                    }
                }
            }
        }
    return true;
}

// *****************************************************************************
/* Function:
    void SYS_COMMAND_MESSAGE (const char* message)

  Summary:
    Outputs a message to the Command Processor System Service console.

  Description:
    This function outputs a message to the Command Processor System Service
    console.
.
  Precondition:
    SYS_COMMAND_Initialize was successfully run once.

  Parameters:
    None.

  Returns:
    None.

  Remarks:
    None.
*/
void SYS_COMMAND_MESSAGE(const char* message)
{
    SYS_CMD_MESSAGE(NULL, message);
}

// *****************************************************************************
/* Function:
    void SYS_COMMAND_PRINT(const char *format, ...)

  Summary:
    Outputs a printout to the Command Processor System Service console.

  Description:
    This function outputs a printout to the Command Processor System Service
    console.
.
  Precondition:
    SYS_COMMAND_Initialize was successfully run once.

  Parameters:
    None.

  Returns:
    None.

  Remarks:
    None.
*/
void SYS_COMMAND_PRINT(const char* format, ...)
{
    char tmpBuf[SYS_COMMAND_PRINT_BUFFER_SIZE];
    size_t len;
    va_list args;
    va_start( args, format );

    len = vsnprintf(tmpBuf, SYS_COMMAND_PRINT_BUFFER_SIZE, format, args);
    tmpBuf[len] = '\0';

    if (len + printBuffPtr + 1 > SYS_COMMAND_PRINT_BUFFER_SIZE)
    {
        printBuffPtr = 0;
    }
    strcpy(&printBuff[printBuffPtr], tmpBuf);
    SYS_CMD_MESSAGE(NULL, &printBuff[printBuffPtr]);

    va_end( args );

    printBuffPtr += (strlen(tmpBuf) + 1);
}

static void SYS_CMD_READCOMPLETE (void *arg)
{
    size_t *readSize = arg;
    _cmdAppData.bytesRead = *readSize;
}

static void SYS_CMD_WRITECOMPLETE (void *arg)
{
}

_CMDIO_DEV_NODE* SYS_CMDIO_GET_HANDLE(short num)
{
    _CMDIO_DEV_NODE* pNode = cmdIODevList.head;

    if (num == 0) return ((pNode)?pNode:NULL);

    while(num && pNode)
    {
        pNode = pNode->next;
        num--;
    }

    return pNode;
}

_CMDIO_DEV_NODE* SYS_CMDIO_ADD(const _CMDIO_DEV_API* opApi, const void* cmdIoParam)
{
    // Create node
    _CMDIO_DEV_NODE* pDevNode;
    int moduleIndex = 0;

    pDevNode = (_CMDIO_DEV_NODE*)malloc(sizeof(*pDevNode));
    if (!pDevNode)
    {
        return 0;
    }
    pDevNode->pCmdApi = opApi;
    pDevNode->cmdIoParam = cmdIoParam;
    pDevNode->cmdPnt = pDevNode->cmdEnd = pDevNode->cmdBuff;
    pDevNode->cmdState = CMDIO_STATE_DISABLE;

    //pDevNode->index = cmdIODevList.num;

    pDevNode->moduleInFd = STDIN_FILENO;
    pDevNode->moduleOutFd = STDOUT_FILENO;

    // Insert node at end
    cmdIODevList.num++;

    for (moduleIndex = 0; moduleIndex < SYS_CONSOLE_DEVICE_MAX_INSTANCES; ++moduleIndex)
    {
        pDevNode->moduleIndices[moduleIndex] = moduleIndex;

        SYS_CONSOLE_RegisterCallback(pDevNode->moduleIndices[moduleIndex], SYS_CMD_READCOMPLETE, SYS_CONSOLE_EVENT_READ_COMPLETE);
        SYS_CONSOLE_RegisterCallback(pDevNode->moduleIndices[moduleIndex], SYS_CMD_WRITECOMPLETE, SYS_CONSOLE_EVENT_WRITE_COMPLETE);
    }

    pDevNode->next = NULL;
    if(cmdIODevList.head == NULL)
    {
        cmdIODevList.head = pDevNode;
        cmdIODevList.tail = pDevNode;
    }
    else
    {
        cmdIODevList.tail->next = pDevNode;
        cmdIODevList.tail = pDevNode;
    }

    return pDevNode;
}


bool SYS_CMDIO_DELETE(_CMDIO_DEV_NODE* pDevNode)
{
    _CMDIO_DEV_NODE* p_listnode = cmdIODevList.head;
    _CMDIO_DEV_NODE* pre_listnode;

    //root list is empty or null node to be delete
    if((p_listnode==NULL) || (pDevNode==NULL))
    {
        return false;
    }

    // Head will be delted
    if(p_listnode == pDevNode)
    {
        cmdIODevList.num--;
        //Root list has only one node
        if(cmdIODevList.tail == pDevNode)
        {
            cmdIODevList.head = NULL;
            cmdIODevList.tail = NULL;
        }
        else
        {
            cmdIODevList.head = p_listnode->next;
        }
        free(pDevNode);
        return true;
    }
    // Not head delete
    pre_listnode = p_listnode;
    while (p_listnode)
    {
        if(p_listnode == pDevNode)
        {
            pre_listnode->next = p_listnode->next;
            // Deleted node is tail
            if (cmdIODevList.tail==pDevNode) {
                cmdIODevList.tail = pre_listnode;
            }
            cmdIODevList.num--;
            free(pDevNode);
            return true;
        }
        pre_listnode = p_listnode;
        p_listnode   = p_listnode->next;
    }

    return false;
}

// ignore the console handle for now, we support a single system console
static void SYS_CMD_MESSAGE(const void* cmdIoParam, const char* message)
{
    int consoleIndex = 0;

    for (consoleIndex = 0; consoleIndex < SYS_CONSOLE_DEVICE_MAX_INSTANCES; ++consoleIndex)
    {
        SYS_CONSOLE_Write(consoleIndex, STDOUT_FILENO, message, strlen(message));
    }
}

static void SYS_CMD_PRINT(const void* cmdIoParam, const char* format, ...)
{
    char tmpBuf[SYS_COMMAND_PRINT_BUFFER_SIZE];
    size_t len;
    va_list args;
    va_start( args, format );

    len = vsnprintf(tmpBuf, SYS_COMMAND_PRINT_BUFFER_SIZE, format, args);
    tmpBuf[len] = '\0';

    if (len + printBuffPtr + 1 > SYS_COMMAND_PRINT_BUFFER_SIZE)
    {
        printBuffPtr = 0;
    }
    strcpy(&printBuff[printBuffPtr], tmpBuf);
    SYS_CMD_MESSAGE(cmdIoParam, &printBuff[printBuffPtr]);

    va_end( args );

    printBuffPtr += (strlen(tmpBuf) + 1);
}

static void SYS_CMD_PUTC(const void* cmdIoParam, char c)
{
    _CMDIO_DEV_NODE* cmdIoNode = (_CMDIO_DEV_NODE*)cmdIoParam;
    int consoleIndex = 0;

    for (consoleIndex = 0; consoleIndex < SYS_CONSOLE_DEVICE_MAX_INSTANCES; ++consoleIndex)
    {
        if (SYS_CONSOLE_Status((SYS_MODULE_OBJ)consoleIndex) != SYS_STATUS_READY)
            continue;

        SYS_CONSOLE_Write(consoleIndex, STDOUT_FILENO, (const char*)&c, 1);
    }
}

static bool SYS_CMD_DATA_RDY(const void* cmdIoParam)
{
    int consoleIndex = 0;
    bool ready = false;

    for (consoleIndex = 0; consoleIndex < SYS_CONSOLE_DEVICE_MAX_INSTANCES; ++consoleIndex)
    {
        ready |= SYS_CONSOLE_Status((SYS_MODULE_OBJ)consoleIndex) == SYS_STATUS_READY;
    }

    return ready;
}


static char SYS_CMD_GETC(const void* cmdIoParam)
{
    return '\0';
}

static size_t SYS_CMD_READC(const void* cmdIoParam)
{
    _CMDIO_DEV_NODE* cmdIoNode = (_CMDIO_DEV_NODE*)cmdIoParam;
    int readConsoleIndex = 0;
    int echoConsoleIndex = 0;

    size_t readyBytes = 0;

    for (readConsoleIndex = 0; readConsoleIndex < SYS_CONSOLE_DEVICE_MAX_INSTANCES; ++readConsoleIndex)
    {
        if (SYS_CONSOLE_Status((SYS_MODULE_OBJ)readConsoleIndex) == SYS_STATUS_READY)
        {
            readyBytes = SYS_CONSOLE_Read(readConsoleIndex, cmdIoNode->moduleInFd, _cmdAppData.readBuff, 1);
        }

        if (readyBytes > 0)
            break;
    }

    return readyBytes;
}

// implementation
static int CommandReset(_CMDIO_DEV_NODE* pCmdIO, int argc, char** argv)
{
    const void* cmdIoParam = pCmdIO->cmdIoParam;
    (*pCmdIO->pCmdApi->msg)(cmdIoParam, LINE_TERM " *** System Reboot ***\r\n" );

    SYS_RESET_SoftwareReset();

    return 1;
}

// quit
static int CommandQuit(_CMDIO_DEV_NODE* pCmdIO, int argc, char** argv)
{
    _CMDIO_DEV_NODE* pCmdIoNode;
    const void* cmdIoParam = pCmdIO->cmdIoParam;

    (*pCmdIO->pCmdApi->msg)(cmdIoParam, LINE_TERM " *** Quitting the Command Processor. Bye ***\r\n" );

    CommandCleanup();

    while ((pCmdIoNode = SYS_CMDIO_GET_HANDLE(0)) != NULL)
    {
        if(SYS_CMDIO_DELETE(pCmdIoNode)) free(pCmdIoNode);
    }

    return 1;
}

// Remove command history list
static void CommandCleanup(void)
{
    cmdNode* pN;

    memset(_usrCmdTbl, 0x0, sizeof(_usrCmdTbl));

    while( (pN = CmdRemoveTail(&_cmdList)) )
    {
        free(pN);
    }

    _pCurrCmdN = 0;
    _cmdAlive = false;
}

static int CommandHelp(_CMDIO_DEV_NODE* pCmdIO, int argc, char** argv)
{
    int             ix, groupIx;
    const _SYS_CMD_DCPT*  pDcpt;
    const _SYS_CMD_DCPT_TBL* pTbl, *pDTbl;
    const void* cmdIoParam = pCmdIO->cmdIoParam;

    if(argc == 1)
    {   // no params help; display basic info
        bool hadHeader = false;
        pTbl = _usrCmdTbl;
        for (groupIx=0; groupIx < MAX_CMD_GROUP; groupIx++)
        {
            if (pTbl->pCmd)
            {
                if(!hadHeader)
                {
                    (*pCmdIO->pCmdApi->msg)(cmdIoParam, LINE_TERM "------- Supported command groups ------");
                    hadHeader = true;
                }
                (*pCmdIO->pCmdApi->msg)(cmdIoParam, LINE_TERM " *** ");
                (*pCmdIO->pCmdApi->msg)(cmdIoParam, pTbl->cmdGroupName);
                (*pCmdIO->pCmdApi->msg)(cmdIoParam, pTbl->cmdMenuStr);
                (*pCmdIO->pCmdApi->msg)(cmdIoParam, " ***");
            }
            pTbl++;
        }

        // display the basic commands
        (*pCmdIO->pCmdApi->msg)(cmdIoParam, LINE_TERM "---------- Built in commands ----------");
        for(ix = 0, pDcpt = _builtinCmdTbl; ix < sizeof(_builtinCmdTbl)/sizeof(*_builtinCmdTbl); ix++, pDcpt++)
        {
            (*pCmdIO->pCmdApi->msg)(cmdIoParam, LINE_TERM " *** ");
            (*pCmdIO->pCmdApi->msg)(cmdIoParam, pDcpt->cmdStr);
            (*pCmdIO->pCmdApi->msg)(cmdIoParam, pDcpt->cmdDescr);
            (*pCmdIO->pCmdApi->msg)(cmdIoParam, " ***");
        }

        (*pCmdIO->pCmdApi->msg)(cmdIoParam, LINE_TERM);
    }
    else
    {   // we have a command group name
        pDTbl = 0;
        pTbl = _usrCmdTbl;
        for (groupIx=0; groupIx < MAX_CMD_GROUP; groupIx++)
        {
            if (pTbl->pCmd)
            {
                if(strcmp(pTbl->cmdGroupName, argv[1]) == 0)
                {   // match
                    pDTbl = pTbl;
                    break;
                }
            }
            pTbl++;
        }

        if(pDTbl)
        {
            for(ix = 0, pDcpt = pDTbl->pCmd; ix < pDTbl->nCmds; ix++, pDcpt++)
            {
                (*pCmdIO->pCmdApi->msg)(cmdIoParam, LINE_TERM " *** ");
                (*pCmdIO->pCmdApi->msg)(cmdIoParam, pDcpt->cmdStr);
                (*pCmdIO->pCmdApi->msg)(cmdIoParam, pDcpt->cmdDescr);
                (*pCmdIO->pCmdApi->msg)(cmdIoParam, " ***");
            }

            (*pCmdIO->pCmdApi->msg)(cmdIoParam, LINE_TERM);
        }
        else
        {
            (*pCmdIO->pCmdApi->msg)(cmdIoParam, LINE_TERM "Unknown command group. Try help" LINE_TERM );
        }
    }

    return 0;
}

static bool ParseCmdBuffer(_CMDIO_DEV_NODE* pCmdIO)
{
    int  argc;
    char *argv[MAX_CMD_ARGS + 1];
    static char saveCmd[SYS_CMD_MAX_LENGTH+1];
    const void* cmdIoParam = pCmdIO->cmdIoParam;

    int            ix, grp_ix;
    const _SYS_CMD_DCPT* pDcpt;

    strncpy(saveCmd, pCmdIO->cmdBuff, sizeof(saveCmd));     // make a copy of the command

    // parse a command string to *argv[]
    argc = StringToArgs(saveCmd, argv);

    if(argc != 0)
    {   // ok, there's smth here

        // add it to the history list
        cmdNode* pN = CmdRemoveTail(&_cmdList);
        strncpy(pN->cmdBuff, pCmdIO->cmdBuff, sizeof(saveCmd)); // Need save non-parsed string
        CmdAddHead(&_cmdList, pN);
        _pCurrCmdN = 0;

        // try built-in commands first
        for(ix = 0, pDcpt = _builtinCmdTbl; ix < sizeof(_builtinCmdTbl)/sizeof(*_builtinCmdTbl); ix++, pDcpt++)
        {
            if(!strcmp(argv[0], pDcpt->cmdStr))
            {   // command found
                return (*pDcpt->cmdFnc)(pCmdIO, argc, argv);     // call command handler
            }
        }
        // search user commands
        for (grp_ix=0; grp_ix<MAX_CMD_GROUP; grp_ix++)
        {
            if (_usrCmdTbl[grp_ix].pCmd == 0)
            {
               continue;
            }

            for(ix = 0, pDcpt = _usrCmdTbl[grp_ix].pCmd; ix < _usrCmdTbl[grp_ix].nCmds; ix++, pDcpt++)
            {
                if(!strcmp(argv[0], pDcpt->cmdStr))
                {
                    // command found
                    return !(*pDcpt->cmdFnc)(pCmdIO, argc, argv);
                }
            }
        }

        // command not found
        (*pCmdIO->pCmdApi->msg)(cmdIoParam, " *** Command Processor: unknown command. ***\r\n");
    }
    else
    {
        (*pCmdIO->pCmdApi->msg)(cmdIoParam, " *** Command Processor: Please type in a command***" LINE_TERM);
    }

    return false;
}

/*
  parse a tring into '*argv[]', delimitor is space or tab
  param pRawString, the whole line of command string
  param argv, parsed argument string array
  return number of parsed argument
*/
static int StringToArgs(char *pRawString, char *argv[]) {
  int argc = 0, i = 0, strsize = 0;

  if(pRawString == NULL)
    return 0;

  strsize = strlen(pRawString);

  while(argc < MAX_CMD_ARGS) {

    // skip white space characters of string head
    while ((*pRawString == ' ') || (*pRawString == '\t')) {
      ++pRawString;
      if (++i >= strsize) {
        return (argc);
      }
    }

    if (*pRawString == '\0') {
      argv[argc] = NULL;
      return (argc);
    }

    argv[argc++] = pRawString;

    // find end of string
    while (*pRawString && (*pRawString != ' ') && (*pRawString != '\t')) {
      ++pRawString;
    }

    if (*pRawString == '\0') {
    argv[argc] = NULL;
    return (argc);
    }

    *pRawString++ = '\0';
  }

  SYS_PRINT("\n\r Too many arguments. Maximum argus supported is %d!\r\n", MAX_CMD_ARGS);

  return (0);
}


static void ProcessEscSequence(_CMDIO_DEV_NODE* pCmdIO)
{
    cmdNode *pNext;

    if(!strcmp(_cmdAppData.seqBuff, _seqUpArrow))
    { // up arrow
        if(_pCurrCmdN)
        {
            pNext = _pCurrCmdN->next;
            if(pNext == _cmdList.head)
            {
                return; // reached the end of list
            }
        }
        else
        {
            pNext = _cmdList.head;
        }

        DisplayNodeMsg(pCmdIO, pNext);
    }
    else if(!strcmp(_cmdAppData.seqBuff, _seqDownArrow))
    { // down arrow
        if(_pCurrCmdN)
        {
            pNext = _pCurrCmdN->prev;
            if(pNext != _cmdList.tail)
            {
                DisplayNodeMsg(pCmdIO, pNext);
            }
        }
    }
    else if(!strcmp(_cmdAppData.seqBuff, _seqRightArrow))
    { // right arrow
        if(pCmdIO->cmdPnt < pCmdIO->cmdEnd)
        {
            (*pCmdIO->pCmdApi->msg)(pCmdIO, _seqRightArrow);
            pCmdIO->cmdPnt++;
        }
    }
    else if(!strcmp(_cmdAppData.seqBuff, _seqLeftArrow))
    { // left arrow
        if(pCmdIO->cmdPnt > pCmdIO->cmdBuff)
        {
            pCmdIO->cmdPnt--;
            (*pCmdIO->pCmdApi->msg)(pCmdIO, _seqLeftArrow);
        }
    }
    else
    {
        (*pCmdIO->pCmdApi->msg)(pCmdIO, " *** Command Processor: unknown command. ***" LINE_TERM);
        (*pCmdIO->pCmdApi->msg)(pCmdIO, _promptStr);
    }

}

static void DisplayNodeMsg(_CMDIO_DEV_NODE* pCmdIO, cmdNode* pNext)
{
    int oCmdLen, nCmdLen;
    const void* cmdIoParam = pCmdIO->cmdIoParam;

    if((nCmdLen = strlen(pNext->cmdBuff)))
    {   // something there
        oCmdLen = pCmdIO->cmdEnd-pCmdIO->cmdBuff;
        while(oCmdLen>nCmdLen)
        {
            (*pCmdIO->pCmdApi->msg)(pCmdIO, "\b \b");     // clear the old command
            oCmdLen--;
        }
        while(oCmdLen--)
        {
            (*pCmdIO->pCmdApi->msg)(pCmdIO, "\b");
        }
        strcpy(pCmdIO->cmdBuff, pNext->cmdBuff);
        (*pCmdIO->pCmdApi->msg)(pCmdIO, "\r\n>");
        (*pCmdIO->pCmdApi->msg)(pCmdIO, pCmdIO->cmdBuff);
        pCmdIO->cmdPnt = pCmdIO->cmdEnd = pCmdIO->cmdBuff+nCmdLen;
        _pCurrCmdN = pNext;
    }
}


static void CmdAddHead(cmdDlList* pL, cmdNode* pN)
{
    if(pL->head == 0)
    { // empty list, first node
        pL->head = pL->tail = pN;
        pN->next = pN->prev = pN;
    }
    else
    {
        pN->next = pL->head;
        pN->prev = pL->tail;
        pL->tail->next = pN;
        pL->head->prev = pN;
        pL->head = pN;
    }
}


static cmdNode* CmdRemoveTail(cmdDlList* pL)
{
    cmdNode* pN;
    if(pL->head == pL->tail)
    {
        pN = pL->head;
        pL->head = pL->tail = 0;
    }
    else
    {
        pN = pL->tail;
        pL->tail = pN->prev;
        pL->tail->next = pL->head;
        pL->head->prev = pL->tail;
    }
    return pN;
}


