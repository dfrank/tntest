/*******************************************************************************
  Watchdog Timer System Service Local Data

  Company:
    Microchip Technology Inc.

  File Name:
    sys_wdt_local.h

  Summary:
    Watchdog Timer (WDT) System Service declarations and definitions.

  Description:
    This file contains the WDT System Service local declarations.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

#ifndef _SYS_WDT_LOCAL_H
#define _SYS_WDT_LOCAL_H


// *****************************************************************************
// *****************************************************************************
// Section: File includes
// *****************************************************************************
// *****************************************************************************

#include "system/common/sys_common.h"

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>


// *****************************************************************************
// *****************************************************************************
// Section: Version Numbers
// *****************************************************************************
// *****************************************************************************
/* Versioning of the System service */

// *****************************************************************************
/* WDT System service Version Macros

  Summary:
    WDT System service version.

  Description:
    These constants provide WDT System service version information. The System
    service version is:
    SYS_WDT_VERSION_MAJOR.SYS_WDT_VERSION_MINOR.SYS_WDT_VERSION_PATCH.
    It is represented in SYS_WDT_VERSION as:
    MAJOR *10000 + MINOR * 100 + PATCH, so as to allow comparisons.
    It is also represented in string format in _SYS_WDT_VERSION_STR.
    _SYS_WDT_VERSION_TYPE provides the type of the release when the release is
    alpha or beta. The interfaces SYS_WDT_VersionGet and SYS_WDT_VersionStrGet
    provide interfaces to the access the version and the version string.

  Remarks:
    Modify the return value of SYS_WDT_VersionStrGet and _SYS_WDT_VERSION_MAJOR,
    _SYS_WDT_VERSION_MINOR, _SYS_WDT_VERSION_PATCH, and _SYS_WDT_VERSION_TYPE.
*/

#define _SYS_WDT_VERSION_MAJOR         0
#define _SYS_WDT_VERSION_MINOR         5
#define _SYS_WDT_VERSION_PATCH         0
#define _SYS_WDT_VERSION_TYPE          "beta"
#define _SYS_WDT_VERSION_STR           "0.50 beta"


// *****************************************************************************
// *****************************************************************************
// Section: Watchdog timer PLIB Id
// *****************************************************************************
// *****************************************************************************
/* Watchdog timer module PLIB id */

// *****************************************************************************
/* Watchdog timer Peripheral Library ID.

  Summary:
    Defines the WDT module ID.

  Description:
    The module ID for the WDT Peripheral Library will be constant as
    long as there is no more than one WDT in a device.

  Remarks:
    None.

*/

#define WDT_PLIB_ID     				WDT_ID_0


#endif //#ifndef _SYS_WDT_LOCAL_H

/*******************************************************************************
 End of File
*/

