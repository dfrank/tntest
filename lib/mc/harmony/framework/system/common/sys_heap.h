/*******************************************************************************
  SYS Heap Allocation Manager

  Company:
    Microchip Technology Inc.
    
  File Name:
    SYS_heap.h

  Summary:
    
  Description:
*******************************************************************************/
// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright � 2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

#ifndef _SYS_HEAP_H_
#define _SYS_HEAP_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

// *****************************************************************************
/*
  Enum: System heap error

  Summary:
    Derscribes the error for system heap.

  Description:
    The enum describes the error status during the system heap operation.

  Example:
    <code>
    </code>

  Remarks:
    None.

******************************************************************************/

typedef enum
{
    /* Successful creation of heap */
    SYS_HEAP_RES_OK               = 0,   

    /* Invalid supplied heap buffer error */
    SYS_HEAP_RES_BUFF_ERR         = -1,   

    /* Supplied heap buffer not properly aligned */
    SYS_HEAP_RES_BUFF_ALIGN_ERR   = -2,   

    /* Supplied heap buffer too small error */
    SYS_HEAP_RES_BUFF_SIZE_ERR    = -3,   

    /* Out of memory error*/
    SYS_HEAP_RES_NO_MEM           = -4,

    /* Heap in use, cannot be deallocated */
    SYS_HEAP_RES_IN_USE           = -5,   

    /* Corrupt pointer error*/
    SYS_HEAP_RES_PTR_ERR          = -6,

    /* No such heap exists error*/
    SYS_HEAP_RES_NO_HEAP          = -7,   

} SYS_HEAP_RES;

// *****************************************************************************
/*
  Enum: System heap flag

  Summary:
    Flags used during system heap initialization.

  Description:
    The enum has suitble flags to initialize the system heap

  Remarks:
    None.

******************************************************************************/

typedef enum
{
    SYS_HEAP_FLAG_NONE      = 0x00,
    
}SYS_HEAP_FLAGS;  // heap creation/allocation flags

// *****************************************************************************
/*
  Struct: System heap trace entry
  
  Summary:
    System heap trace entry

  Description:
    The struct maintains the system heap trace parameters

  Precondition:
    None.

  Example:
    <code>
    </code>

  Remarks:
    None.

******************************************************************************/

typedef struct
{
    /* Info belonging to this module; <0 means slot free */
    int         moduleId;

    /* Total number of bytes allocated successfully by this module */
    uint32_t    totAllocated;

    /* Number of bytes still allocated by this module */
    uint32_t    currAllocated;

    /* Total number of bytes that failed for this module */
    uint32_t    totFailed;

    /* maximum number of bytes that could not be allocated */
    uint32_t    maxFailed;
    
} SYS_HEAP_TRACE_ENTRY;

// *****************************************************************************
/*
  Pointer: System heap out of memory handler

  Summary:
    System heap out of memory error handler

  Description:
    The out of memory error need to be handled by the function register to this
    pointer

  Precondition:
    None.

  Example:
    <code>
    </code>

  Remarks:
    None.

*******************************************************************************/
typedef void (* SYS_HEAP_NO_MEM_HANDLER)( SYS_MODULE_OBJ heapH, size_t nBytes,
                                         int moduleId, int lineNo);

#if defined( SYS_STACK_DRAM_DEBUG_ENABLE )

typedef void* (* _SYS_HEAP_MallocDebugPtr)( SYS_MODULE_OBJ heapH,
                                            size_t nBytes, int moduleId,
                                            int lineNo);

typedef void* (*_SYS_HEAP_CallocDebugPtr)( SYS_MODULE_OBJ heapH,
                                           size_t nElems, size_t elemSize,
                                           int moduleId, int lineNo);
    
typedef int   (*_SYS_HEAP_FreeDebugPtr)( SYS_MODULE_OBJ heapH,
                                         const void* pBuff, int moduleId);

#define       _SYS_HEAP_MALLOC_PTR  _SYS_HEAP_MallocDebugPtr

#define       _SYS_HEAP_CALLOC_PTR  _SYS_HEAP_CallocDebugPtr

#define       _SYS_HEAP_FREE_PTR    _SYS_HEAP_FreeDebugPtr

void*         _SYS_HEAP_MallocDebug( SYS_MODULE_OBJ heapH, size_t nBytes,
                                     int moduleId, int lineNo);
void*         _SYS_HEAP_CallocDebug( SYS_MODULE_OBJ heapH, size_t nElems,
                                     size_t elemSize, int moduleId, int lineNo);
int           _SYS_HEAP_FreeDebug( SYS_MODULE_OBJ heapH, const void* pBuff,
                                   int moduleId );

#define       _SYS_HEAP_MALLOC_FNC  _SYS_HEAP_MallocDebug
#define       _SYS_HEAP_CALLOC_FNC  _SYS_HEAP_CallocDebug
#define       _SYS_HEAP_FREE_FNC    _SYS_HEAP_FreeDebug

#define       _SYS_HEAP_MALLOC_BY_PTR(ptr, heapH, nBytes) (*ptr)( \
                                                         heapH, \
                                                         nBytes, \
                                                         SYS_THIS_MODULE_ID, \
                                                         __LINE__ )

#define       _SYS_HEAP_CALLOC_BY_PTR(ptr, heapH, nElems, elemSize)(*ptr)( \
                                                         heapH, \
                                                         nElems, \
                                                         elemSize, \
                                                         SYS_THIS_MODULE_ID, \
                                                         __LINE__ )
                                                                 
#define       _SYS_HEAP_FREE_BY_PTR(ptr, heapH, buff) (*ptr)( \
                                                             heapH, \
                                                             buff,  \
                                                             SYS_THIS_MODULE_ID)

#define       SYS_HEAP_Malloc(heapH, nBytes)            _SYS_HEAP_MallocDebug( \
                                                         heapH, nBytes, \
                                                         SYS_THIS_MODULE_ID, \
                                                         __LINE__ )
                                                             
#define       SYS_HEAP_Calloc(heapH, nElems, elemSize)  _SYS_HEAP_CallocDebug( \
                                                         heapH,              \
                                                         nElems,             \
                                                         elemSize,           \
                                                         SYS_THIS_MODULE_ID, \
                                                         __LINE__)

#define       SYS_HEAP_Free(heapH, ptr)                 _SYS_HEAP_FreeDebug( \
                                                                      heapH, \
                                                                      ptr,   \
                                                            SYS_THIS_MODULE_ID)

#else

typedef void *(*_SYS_HEAP_MallocPtr)( SYS_MODULE_OBJ heapH, size_t nBytes );
typedef void *(*_SYS_HEAP_CallocPtr)( SYS_MODULE_OBJ heapH, size_t nElems,
                                      size_t elemSize );
typedef int  (*_SYS_HEAP_FreePtr)(SYS_MODULE_OBJ heapH,  const void* pBuff);

#define      _SYS_HEAP_MALLOC_PTR  _SYS_HEAP_MallocPtr
#define      _SYS_HEAP_CALLOC_PTR  _SYS_HEAP_CallocPtr
#define      _SYS_HEAP_FREE_PTR    _SYS_HEAP_FreePtr

void*        _SYS_HEAP_Malloc( SYS_MODULE_OBJ heapH, size_t nBytes);
void*        _SYS_HEAP_Calloc( SYS_MODULE_OBJ heapH, size_t nElems,
                               size_t elemSize );
int          _SYS_HEAP_Free( SYS_MODULE_OBJ heapH,  const void* pBuff);

#define      _SYS_HEAP_MALLOC_FNC  _SYS_HEAP_Malloc
#define      _SYS_HEAP_CALLOC_FNC  _SYS_HEAP_Calloc
#define      _SYS_HEAP_FREE_FNC    _SYS_HEAP_Free

#define      _SYS_HEAP_MALLOC_BY_PTR(ptr, heapH, nBytes) (*ptr)( heapH, \
                                                                 nBytes )

#define      _SYS_HEAP_CALLOC_BY_PTR(ptr, heapH, nElems, elemSize)(*ptr)(   \
                                                                    heapH,  \
                                                                    nElems, \
                                                                    elemSize )

#define      _SYS_HEAP_FREE_BY_PTR(ptr, heapH, buff)(*ptr)( heapH,\
                                                             buff )

#define      SYS_HEAP_Malloc(heapH, nBytes) _SYS_HEAP_Malloc( heapH, \
                                                              nBytes )
                                                                    
#define      SYS_HEAP_Calloc(heapH, nElems, elemSize)  _SYS_HEAP_Calloc(heapH, \
                                                                        nElems,\
                                                                       elemSize)

#define      SYS_HEAP_Free(heapH, ptr)                 _SYS_HEAP_Free(heapH, \
                                                                        ptr)


#endif  // defined(SYS_STACK_DRAM_DEBUG_ENABLE)
    
/*******************************************************************************
 * Function:        SYS_MODULE_OBJ SYS_HEAP_Initialize( uint8_t*   heapBuffer,
 *                                                   uint8_t size_t buffSize,
 *                                                   SYS_HEAP_FLAGS flags,
 *                                                   SYS_HEAP_RES*  pRes )
 *
 * PreCondition:    heapBuffer, buffSize    - valid buffer and size values
 *                  heapBuffer - properly aligned buffer
 *
 * Input:           heapBuffer  - buffer to contain the heap
 *                  buffSize    - size of the supplied buffer
 *                  flags       - heap creation/allocation flags.
 *                                Not used for now.
 *                  pRes        - Address to store the operation result.
 *                                Can be NULL if result not needed         
 *
 * Output:          A valid handle if the creation succeeded,
 *                  NULL otherwise.         
 *
 * Side Effects:    None
 *
 * Overview:        The function creates a heap of the maximum possible size in 
 *                  the supplied buffer space. The size of the heap cannot be
 *                  changed afterwards.
 *
 * Note:            The current heap alignment unit is an unsigned long, i.e. 
 *                  32 bits. The supplied heapBuffer value has to be correctly
 *                  32 bits aligned. Other wise a SYS_HEAP_RES_ALIGN_ERR error
 *                  code will be returned!
 *
 ******************************************************************************/
SYS_MODULE_OBJ   SYS_HEAP_Initialize(  uint8_t         *heapBuffer,
                                        size_t          buffSize,
                                        SYS_HEAP_FLAGS  flags,
                                        SYS_HEAP_RES    *pRes);

/*******************************************************************************
 * Function:        SYS_HEAP_RES SYS_HEAP_Delete(SYS_MODULE_OBJ heapH);
 *
 * PreCondition:    heapH       - valid heap handle 
 *
 * Input:           heapH       - handle to a heap that was created with
 *                                SYS_HEAP_Create()
 *
 * Output:          SYS_HEAP_RES_OK if deletion succeeded
 *                  SYS_HEAP_RES_IN_USE if the heap is still used
 *                  SYS_HEAP_RES_NO_HEAP if no such heap exists
 *                  
 * Side Effects:    None
 *
 * Overview:        The function deletes a previously created heap.
 *
 * Note:            The call will fail if blocks are still in use.
 ******************************************************************************/
SYS_HEAP_RES      SYS_HEAP_Delete(SYS_MODULE_OBJ heapHandle);

/*******************************************************************************
 * Function:        void* SYS_HEAP_Malloc(SYS_MODULE_OBJ heapH, size_t nBytes);
 *
 * PreCondition:    heapH       - valid heap handle 
 *
 * Input:           heapH       - handle to a heap
 *                  nBytes      - size of the block to be allocated
 *
 * Output:          a valid pointer to an allocated block if allocation 
 *                  succeeded,
 *                  NULL otherwise.         
 *
 * Side Effects:    None
 *
 * Overview:        The function checks the heap for finding a block large 
 *                  enough to accommodate the request. A first fit algorithm is
 *                  used.
 *
 * Note:            None
 ******************************************************************************/
void*               SYS_HEAP_Malloc( SYS_MODULE_OBJ heapHandle, size_t nBytes);

/*******************************************************************************
 * Function:        void* SYS_HEAP_Calloc( SYS_MODULE_OBJ heapH, size_t nElems,
                                           size_t elemSize );
 *
 * PreCondition:    heapH       - valid heap handle 
 *
 * Input:           heapH       - handle to a heap
 *                  nElems      - number of elements to be allocated
 *                  elemSize    - size of each element, in bytes
 *
 * Output:          a valid pointer to an allocated block if allocation 
 *                  succeeded,
 *                  NULL otherwise.
 *
 * Side Effects:    None
 *
 * Overview:        The function checks the heap for finding a block large
 *                  enough to accommodate
 *                  nElems*elemSize request.
 *                  If the block is found, it is zero initialized and returned 
 *                  to user. A first fit algorithm is used.
 *
 * Note:            None
 ******************************************************************************/
void*               SYS_HEAP_Calloc( SYS_MODULE_OBJ heapHandle,
                                     size_t nElems,
                                     size_t elemSize);

/*******************************************************************************
 * Function:        int SYS_HEAP_Free(SYS_MODULE_OBJ heapH, void* pBuff);
 *
 * PreCondition:    heapH       - valid heap handle 
 *
 * Input:           heapH       - handle to a heap
 *                  pBuff       - pointer to a buffer previously allocated from
 *                                the heap
 *
 * Output:          true if the operation succeeded,
 *                  false otherwise       
 *
 * Side Effects:    None
 *
 * Overview:        The function returns the buffer to the heap.
 *                  Left and/or right defragment operations are performed if
 *                  possible.
 *
 * Note:            None
 ******************************************************************************/
int                 SYS_HEAP_Free( SYS_MODULE_OBJ heapHandle,
                                   const void* pBuff );

/*******************************************************************************
 * Function:        size_t SYS_HEAP_Size(SYS_MODULE_OBJ heapH);
 *
 * PreCondition:    heapH       - valid heap handle 
 *
 * Input:           heapH       - handle to a heap
 *
 * Output:          the size of the heap as it was created
 *
 * Side Effects:    None
 *
 * Overview:        The function returns the size of the heap.
 *                  This is the size that was specified when the heap was
 *                  created.
 *
 * Note:            None
 ******************************************************************************/
size_t                 SYS_HEAP_Size( SYS_MODULE_OBJ heapHandle);


/*******************************************************************************
 * Function:        size_t SYS_HEAP_MaxSize(SYS_MODULE_OBJ heapH);
 *
 * PreCondition:    heapH       - valid heap handle 
 *
 * Input:           heapH       - handle to a heap
 *
 * Output:          the max size of a block that can be allocated from the heap
 *
 * Side Effects:    None
 *
 * Overview:        The function returns the maximum size of a block that can be 
 *                  currently allocated from this heap.
 *
 * Note:            This is info only.
 *                  It can change is the heap has multiple clients.
 *
 *                  The call is expensive.
 *                  The whole heap has to be traversed to find the maximum.
 *                  If the heap is really fragmented this might take some time.
 *
 ******************************************************************************/
size_t                 SYS_HEAP_MaxSize( SYS_MODULE_OBJ heapHandle );

/*******************************************************************************
 * Function:        size_t SYS_HEAP_FreeSize(SYS_MODULE_OBJ heapH);
 *
 * PreCondition:    heapH       - valid heap handle 
 *
 * Input:           heapH       - handle to a heap
 *
 * Output:          the size of the available space in the heap
 *
 * Side Effects:    None
 *
 * Overview:        The function returns the size of the space currently
 *                  available in the heap.
 *
 * Note:            This is a cumulative number, counting all the existent free
 *                  space.
 *                  It is not the maximum blocks size that could be allocated
 *                  from the heap.
 ******************************************************************************/
size_t                 SYS_HEAP_FreeSize( SYS_MODULE_OBJ heapHandle );

/*******************************************************************************
 * Function:        SYS_HEAP_RES SYS_HEAP_LastError(SYS_MODULE_OBJ heapH)
 *
 * PreCondition:    heapH       - valid heap handle
 *
 * Input:           heapH       - handle of a heap
 *
 * Output:          The last error encountered in an operation
 *                  or SYS_HEAP_RES_OK if no error occurred
 *
 * Side Effects:    None
 *
 * Overview:        The function returns the last error encountered in a heap 
 *                  operation. It clears the value of the last error variable.
 *
 * Note:            The heap holds an error variable storing the last error
 *                  encountered in an operation.
 *                  This should be consulted by the caller after each operation
 *                  that returns an invalid result for checking what the error 
 *                  condition was.
 ******************************************************************************/
SYS_HEAP_RES      SYS_HEAP_LastError(SYS_MODULE_OBJ heapHandle );

/*******************************************************************************
 * Function:      SYS_HEAP_RES SYS_HEAP_SetNoMemHandler(SYS_MODULE_OBJ heapH,
 *                                              SYS_HEAP_NO_MEM_HANDLER handler)
 *
 * PreCondition:    heapH       - valid heap handle
 *
 * Input:           heapH       - handle of a heap
 *                  handler     - notification handler to be called when a 
 *                                memory allocation failed use 0 to clear
 *
 * Output:          SYS_HEAP_RES_OK if no error occurred
 *
 * Side Effects:    None
 *
 * Overview:        The function sets the out of memory notification handler.
 *                  It is a very simple notification and only one client is 
 *                  supported, basically it is meant for the creator of the
 *                  heap. When the debug is enabled and some allocation fails
 *                  some system warning message will be sent to the debug port.
 *                  Also if the handler is not null it will be called.
 *
 * Note:            The handler could be a signaling function, short
 *                  and just setting an event of some sort.
 *                  However since it is not called from ISR context this is not 
 *                  really a requirement and some more extensive processing can
 *                  be done.
 *
 *                  This function is implemented only when the system debug and
 *                  heap debug (SYS_STACK_DRAM_DEBUG_ENABLE) are enabled
 ******************************************************************************/
SYS_HEAP_RES SYS_HEAP_SetNoMemHandler( SYS_MODULE_OBJ heapHandle,
                                       SYS_HEAP_NO_MEM_HANDLER handler );

/*******************************************************************************
 * Function:      bool  SYS_HEAP_TraceGetEntry( SYS_MODULE_OBJ heapH,
 *                                              int entryIx,
 *                                              SYS_HEAP_TRACE_ENTRY* tEntry )
 *
 * PreCondition:    None
 *
 * Input:           heapH       - handle of a heap
 *                  entryIx     - index of the requested heap trace entry
 *                  tEntry      - address of a HEAP_TRACE_ENTRY that will be
 *                                updated with corresponding info
 *
 * Output:          true if tEntry was populated with the info
 *                  false if entryIx record does not exist or if tracing is not
 *                  enabled
 *
 * Side Effects:    None
 *
 * Overview:        The function returns the heap trace info for the requested
 *                  entry.
 *
 * Note:            
 *                  Trace info is recorded only when
 *                  SYS_STACK_DRAM_DEBUG_ENABLE and SYS_STACK_DRAM_TRACE_ENABLE 
 *                  are enabled Also, the number of defined slots
 *                  SYS_STACK_DRAM_TRACE_SLOTS has to be big enough to
 *                  accommodate all the modules.
 *
 *                  The moduleId is the one from SYS.h::SYS_STACK_MODULE
 ******************************************************************************/
bool  SYS_HEAP_TraceGetEntry( SYS_MODULE_OBJ heapHandle, int entryIx,
                              SYS_HEAP_TRACE_ENTRY* tEntry );

/*******************************************************************************
 * Function:      int  SYS_HEAP_TraceGetEntriesNo( SYS_MODULE_OBJ heapH,
 *                                                 bool getUsed )
 *
 * PreCondition:    None
 *
 * Input:           heapH       - handle of a heap
 *                  getUsed     - if true, only the currently used record slots 
 *                                will be returned otherwise the whole number of
 *                                slots will be returned.
 *
 * Output:          number of entries the heap has trace slots for
 *
 * Side Effects:    None
 *
 * Overview:        The function returns the number of slots that hold trace 
 *                  info. By using the "getUsed" parameter, only the number of
 *                  slots that hold recorded info will be returned.
 *
 * Note:            
 *                  Trace info is recorded only when
 *                  SYS_STACK_DRAM_DEBUG_ENABLE and SYS_STACK_DRAM_TRACE_ENABLE 
 *                  are enabled. Also, the number of defined slots
 *                  SYS_STACK_DRAM_TRACE_SLOTS has to be big enough to
 *                  accommodate all the modules.
 *
 *                  The moduleId is the one from SYS.h::SYS_STACK_MODULE
 *
 *                  If trace is enabled the function will return the value of
 *                  SYS_STACK_DRAM_TRACE_SLOTS
 ******************************************************************************/
int SYS_HEAP_TraceGetEntriesNo( SYS_MODULE_OBJ heapHandle, bool getUsed );

#if !defined(SYS_STACK_DRAM_DEBUG_ENABLE) || !defined(SYS_STACK_DRAM_TRACE_ENABLE)
// *****************************************************************************
/*
  Function:
    bool SYS_HEAP_TraceGetEntry
    (SYS_MODULE_OBJ heapHandle, int entryIx, SYS_HEAP_TRACE_ENTRY* tEntry)

  Summary:

  Description:

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    </code>

  Remarks:
    None.
*/
extern __inline__ bool __attribute__((always_inline)) SYS_HEAP_TraceGetEntry
(SYS_MODULE_OBJ heapHandle, int entryIx, SYS_HEAP_TRACE_ENTRY* tEntry)
{
    return false;
}

// *****************************************************************************
/*
  Function:
    void SYS_HEAP_TraceGetEntriesNo( SYS_MODULE_OBJ heapH, bool used )

  Summary:

  Description:

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>

    </code>

  Remarks:
    None.

*/
extern __inline__ int __attribute__((always_inline)) SYS_HEAP_TraceGetEntriesNo(
                                                        SYS_MODULE_OBJ heapH,
                                                        bool used )
{
    return 0;
}

#endif  // !defined(SYS_STACK_DRAM_DEBUG_ENABLE) ||
        //!defined(SYS_STACK_DRAM_TRACE_ENABLE)

#endif  // _SYS_HEAP_H_

