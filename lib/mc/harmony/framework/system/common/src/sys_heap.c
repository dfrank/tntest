/*******************************************************************************
  SYS Heap Allocation Manager

  Company:
    Microchip Technology Inc.

  File Name:
    SYS_heap.h

  Summary:

  Description:
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

#include <string.h>
#include <stdlib.h>

#include <sys/kmem.h>

#include "system/common/sys_module.h"
#include "system/common/sys_common.h"
#include "system/common/sys_heap.h"
#include "osal/osal.h"

// *****************************************************************************
/* Sytem heap alignement

  Summary:
    Defines the heap alignement.

  Description:
    The typedef defines the correct allignement based on the architecture.

  Remarks:
    This type changes depending upon which family of Microchip microcontrollers
    is chosen.
*/

#if defined(__PIC32MX__)

    typedef uint32_t _SYS_HEAP_ALIGN;

#elif defined(__C30__)

    typedef uint16_t _SYS_HEAP_ALIGN;

#elif defined(__PIC32MZ__)

    typedef struct __attribute__((aligned(16)))
    {
        uint64_t     pad[2];
        
    }_SYS_HEAP_ALIGN;

#endif  // defined(__PIC32MX__) || defined(__C30__) || defined(__PIC32MZ__)

// *****************************************************************************
/* System Heap Node

  Summary:
    It is a union defining the heap node.

  Description:
    The union defines a heap node. A Heap node is a minimum size of memory
  allocated during each allocation. It is accompanied to at the start of each
  allocation. It provides the information related to allocation.

  Remarks:
    None.

*/

typedef union __attribute__((aligned(16))) _HEAP_NODE
{
    _SYS_HEAP_ALIGN x;
    
    struct
    {
        union _HEAP_NODE*	next;
        size_t			units;
    };
    
} _SYS_HEAP_NODE;

// *****************************************************************************
/*
 
  Summary:

  Description:

  Precondition:
    None.

  Remarks:
    None.

*/
#define	_SYS_HEAP_MIN_BLK_USIZE_      2

// *****************************************************************************
/*

  Summary:

  Description:

  Precondition:
    None.

  Remarks:
    None.

*/
#define	_SYS_HEAP_MIN_BLKS_           20	


#if defined(SYS_STACK_DRAM_DEBUG_ENABLE) && defined(SYS_STACK_DRAM_TRACE_ENABLE)

// *****************************************************************************
/*
  Summary:

  Description:

  Precondition:
    None.

  Remarks:
    None.

*/

    #define _SYS_STACK_DRAM_TRACE_ENABLE

#else

    #undef  _SYS_STACK_DRAM_TRACE_ENABLE

#endif

// *****************************************************************************
/*
  Struct: System heap descriptor

  Summary:
     Describes system heap descriptor

  Description:
     Describes system heap descriptor

  Precondition:
    None.

  Example:
    <code>
    </code>

  Remarks:
    None.

*/
typedef struct
{
    /* head and tail pointers */
    _SYS_HEAP_NODE*         _heapHead;

    /* size of the heap, units */
    _SYS_HEAP_NODE*         _heapTail;

    /* how many units allocated out there */
    size_t                  _heapUnits;

    /**/
    size_t                  _heapAllocatedUnits;

    /* last error encountered */
    SYS_HEAP_RES            _lastHeapErr;

    /* heap flags */
    SYS_HEAP_FLAGS          _heapFlags;
    
#if defined(SYS_STACK_DRAM_DEBUG_ENABLE)

    /* out of memory handler */
    SYS_HEAP_NO_MEM_HANDLER _noMemHandler;

    /* debug checkpoint */
    _SYS_HEAP_NODE*         _heapStart;

    /* debug checkpoint */
    _SYS_HEAP_NODE*         _heapEnd;

#endif

#if defined(_SYS_STACK_DRAM_TRACE_ENABLE)

    /**/
    SYS_HEAP_TRACE_ENTRY        _heapTraceTbl[SYS_STACK_DRAM_TRACE_SLOTS];

#endif

    /**/
    OSAL_RESULT             _heapSemaphoreInit;

    /**/
    OSAL_SEM_HANDLE_TYPE    _heapSemaphore;

}SYS_HEAP_DESC;

// *****************************************************************************
/*
  
  Summary:
    

  Description:
    

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    </code>

  Remarks:
    None.

*/

#if defined(SYS_STACK_DRAM_DEBUG_ENABLE)

#if defined(SYS_DEBUG_ENABLE)

    static const char* _heapFailMessage = "Heap allocation of %d bytes failed \
                                           in module %d line %d\r\n";

#else

    #define             _heapFailMessage    0

#endif  // defined(SYS_DEBUG_ENABLE)

#endif  // defined(SYS_STACK_DRAM_DEBUG_ENABLE)

#if defined(_SYS_STACK_DRAM_TRACE_ENABLE)

// *****************************************************************************
/*
  Function:
    SYS_HEAP_TRACE_ENTRY* SYS_HEAP_FindEntry(SYS_HEAP_DESC* heapDesc,
                                                int moduleId, bool addNewSlot);

  Summary:
    

  Description:
    

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    </code>

  Remarks:
    None.

*/
static SYS_HEAP_TRACE_ENTRY* SYS_HEAP_FindEntry(SYS_HEAP_DESC* heapDesc,
                                                int moduleId, bool addNewSlot);

// *****************************************************************************
/*
  Function:
    void SYS_HEAP_AddToEntry(SYS_HEAP_DESC* heapDesc, int moduleId,
                                size_t nBytes, void* ptr);

  Summary:
    

  Description:
    

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    </code>

  Remarks:
    None.

*/
static void SYS_HEAP_AddToEntry(SYS_HEAP_DESC* heapDesc, int moduleId,
                                size_t nBytes, void* ptr);

// *****************************************************************************
/*
  Function:
     void SYS_HEAP_RemoveFromEntry(SYS_HEAP_DESC* heapDesc, int moduleId,
                                     size_t nUnits)
  Summary:


  Description:

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    </code>

  Remarks:
    None.

*/
static void SYS_HEAP_RemoveFromEntry(SYS_HEAP_DESC* heapDesc, int moduleId,
                                     size_t nUnits);

#endif  // defined(_SYS_STACK_DRAM_TRACE_ENABLE)

#if defined(__PIC32MZ__)

// *****************************************************************************
/*
  Function:
    void _SYS_HEAP_DataLineFlush(void* address)

  Summary:

  Description:

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    </code>

  Remarks:
    None.

*/

extern __inline__ void __attribute__((always_inline)) _SYS_HEAP_DataLineFlush(
                                                                  void* address)
{   // issue a hit-writeback invalidate order
    __asm__ __volatile__ ("cache 0x15, 0(%0)" ::"r"(address));
}


// *****************************************************************************
/*
  Function:
    void _SYS_HEAP_DataCacheInvalidate(void* address, size_t nBytes)

  Summary:


  Description:


  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    </code>

  Remarks:
    None.

*/

void _SYS_HEAP_DataCacheInvalidate(void* address, size_t nBytes)
{
    int ix;

    // start on an even cache line
    uint8_t*    pLine = (uint8_t*)((uint32_t)address & 0xfffffff0); 
    nBytes += (uint8_t*)address - pLine;
    
    // round up the number of taken cache lines
    int nLines = (nBytes + 15)/16;   

    for(ix = 0; ix < nLines; ix ++)
    {
        _SYS_HEAP_DataLineFlush(pLine);
        pLine += 16;
    }

    __asm__ __volatile__ ("sync");
}
#endif  // defined(__PIC32MZ__)

/*******************************************************************************
 * Function:        SYS_MODULE_OBJ SYS_HEAP_Initialize( uint8_t*   heapBuffer,
 *                                                   uint8_t size_t buffSize,
 *                                                   SYS_HEAP_FLAGS flags,
 *                                                   SYS_HEAP_RES*  pRes )
 *
 * PreCondition:    heapBuffer, buffSize    - valid buffer and size values
 *                  heapBuffer - properly aligned buffer
 *
 * Input:           heapBuffer  - buffer to contain the heap
 *                  buffSize    - size of the supplied buffer
 *                  flags       - heap creation/allocation flags.
 *                                Not used for now.
 *                  pRes        - Address to store the operation result.
 *                                Can be NULL if result not needed
 *
 * Output:          A valid handle if the creation succeeded,
 *                  NULL otherwise.
 *
 * Side Effects:    None
 *
 * Overview:        The function creates a heap of the maximum possible size in
 *                  the supplied buffer space. The size of the heap cannot be
 *                  changed afterwards.
 *
 * Note:            The current heap alignment unit is an unsigned long, i.e.
 *                  32 bits. The supplied heapBuffer value has to be correctly
 *                  32 bits aligned. Other wise a SYS_HEAP_RES_ALIGN_ERR error
 *                  code will be returned!
 *
 ******************************************************************************/

SYS_MODULE_OBJ SYS_HEAP_Create( uint8_t* heapBuffer, size_t buffSize,
                                 SYS_HEAP_FLAGS flags, SYS_HEAP_RES* pRes)
{
    SYS_HEAP_DESC*  heapDesc;
    size_t          heapSize, heapUnits, headerSize;
    uintptr_t       alignBuffer;
    uint8_t*        heapStart;
    SYS_HEAP_RES    res;
    

    while(true)
    {
        heapDesc =0;

        if( heapBuffer == 0 )
        {
            res = SYS_HEAP_RES_BUFF_ERR;
            break;
        }

        // align properly: round up and truncate
        alignBuffer = ((uintptr_t)heapBuffer + sizeof(_SYS_HEAP_ALIGN)-1 ) &
                        ~(sizeof(_SYS_HEAP_ALIGN)-1);
        buffSize -= (uint8_t*)alignBuffer - heapBuffer ;
        heapBuffer = (uint8_t*)alignBuffer;

#if defined(__PIC32MZ__)
        if(IS_KVA0(heapBuffer))
        {   // allocate in K1
            _SYS_HEAP_DataCacheInvalidate(heapBuffer, buffSize);
            heapBuffer = (uint8_t*)KVA0_TO_KVA1(heapBuffer);
        }
#endif  // defined(__PIC32MZ__)

        headerSize = ((sizeof(SYS_HEAP_DESC) + sizeof(_SYS_HEAP_NODE) - 1) /
                    sizeof(_SYS_HEAP_NODE)) * sizeof(_SYS_HEAP_NODE);

        heapSize = buffSize - headerSize;

        // adjust to multiple of heads
        heapUnits = heapSize / sizeof(_SYS_HEAP_NODE);

        if(heapUnits < _SYS_HEAP_MIN_BLKS_)
        {
            res = SYS_HEAP_RES_BUFF_SIZE_ERR;
            break;
        }

        heapStart = heapBuffer + headerSize; 
        heapDesc = (SYS_HEAP_DESC*)heapBuffer;
        heapDesc->_heapHead = (_SYS_HEAP_NODE*)heapStart;
        heapDesc->_heapHead->units = heapUnits;
        heapDesc->_heapHead->next = 0;
        heapDesc->_heapTail = heapDesc->_heapHead;
        heapDesc->_heapUnits = heapUnits;
        heapDesc->_heapAllocatedUnits = 0;
        heapDesc->_lastHeapErr = SYS_HEAP_RES_OK;
        heapDesc->_heapFlags = flags;
#ifdef SYS_STACK_DRAM_DEBUG_ENABLE
        heapDesc->_heapStart = heapDesc->_heapHead;
        heapDesc->_heapEnd = heapDesc->_heapHead + heapUnits;
        heapDesc->_noMemHandler = 0;
#endif

        heapDesc->_heapSemaphoreInit = OSAL_SEM_Create(&heapDesc->_heapSemaphore,
                                                    OSAL_SEM_TYPE_BINARY, 1, 1);


#if defined(_SYS_STACK_DRAM_TRACE_ENABLE)

        // clear entries
        memset(heapDesc->_heapTraceTbl, 0, sizeof(heapDesc->_heapTraceTbl));
#endif
        res = SYS_HEAP_RES_OK;
        break;
    }

    if(pRes)
    {
        *pRes = res;
    }

    return heapDesc;
    
}

/*******************************************************************************
 * Function:        SYS_HEAP_RES SYS_HEAP_Delete(SYS_MODULE_OBJ heapHandle);
 *
 * PreCondition:    heapHandle       - valid heap handle
 *
 * Input:           heapHandle       - handle to a heap that was created with
 *                                SYS_HEAP_Create()
 *
 * Output:          SYS_HEAP_RES_OK if deletion succeeded
 *                  SYS_HEAP_RES_IN_USE if the heap is still used
 *                  SYS_HEAP_RES_NO_HEAP if no such heap exists
 *
 * Side Effects:    None
 *
 * Overview:        The function deletes a previously created heap.
 *
 * Note:            The call will fail if blocks are still in use.
 ******************************************************************************/
SYS_HEAP_RES SYS_HEAP_Delete(SYS_MODULE_OBJ heapHandle)
{
    SYS_HEAP_DESC*   heapDesc;

    heapDesc = (SYS_HEAP_DESC *)heapHandle;

    if(heapDesc == 0)
    {
        return SYS_HEAP_RES_NO_HEAP;
    }
    
    if(heapDesc->_heapHead)
    {
    #ifdef SYS_STACK_DRAM_DEBUG_ENABLE

        if(heapDesc->_heapAllocatedUnits != 0          ||
           heapDesc->_heapHead != heapDesc->_heapStart    ||
           heapDesc->_heapHead->units != heapDesc->_heapUnits)

    #else

        if(heapDesc->_heapAllocatedUnits != 0)
            
    #endif
    
        {
            //  deallocating a heap not completely de-allocated or corrupted
            return (heapDesc->_lastHeapErr = SYS_HEAP_RES_IN_USE);
        }

        if (heapDesc->_heapSemaphoreInit == OSAL_RESULT_TRUE)
        {
            if (OSAL_SEM_Delete(heapDesc->_heapSemaphore) != OSAL_RESULT_TRUE)
            {
                //SYS_Debug message
            }
            heapDesc->_heapSemaphoreInit = OSAL_RESULT_FALSE;
        }

        return SYS_HEAP_RES_OK;
    }
    
    return (heapDesc->_lastHeapErr = SYS_HEAP_RES_IN_USE);
}

/*******************************************************************************
 * Function:        void* SYS_HEAP_Malloc(SYS_MODULE_OBJ heapHandle,
                                            size_t nBytes);
 *
 * PreCondition:    heapHandle       - valid heap handle
 *
 * Input:           heapHandle       - handle to a heap
 *                  nBytes      - size of the block to be allocated
 *
 * Output:          a valid pointer to an allocated block if allocation
 *                  succeeded,
 *                  NULL otherwise.
 *
 * Side Effects:    None
 *
 * Overview:        The function checks the heap for finding a block large
 *                  enough to accommodate the request. A first fit algorithm is
 *                  used.
 *
 * Note:            None
 ******************************************************************************/
void* _SYS_HEAP_Malloc(SYS_MODULE_OBJ heapHandle, size_t nBytes)
{
    _SYS_HEAP_NODE  *ptr, *prev;
    size_t          nunits;
    SYS_HEAP_DESC   *heapDesc;


    if(!nBytes)
    {
		return 0;
    }

    heapDesc = (SYS_HEAP_DESC *)heapHandle;

    // allocate units
    nunits=(nBytes+sizeof(_SYS_HEAP_NODE)-1)/sizeof(_SYS_HEAP_NODE) + 1;
    prev=0;

    if (heapDesc->_heapSemaphoreInit == OSAL_RESULT_TRUE)
    {
        if (OSAL_SEM_Pend(heapDesc->_heapSemaphore, OSAL_WAIT_FOREVER)
                != OSAL_RESULT_TRUE)
        {
            // SYS_DEBUG message
        }
    }

    for(ptr = heapDesc->_heapHead; ptr != 0; prev = ptr, ptr = ptr->next)
    {
	if(ptr->units >= nunits)
	{
            // found block
            if(ptr->units-nunits <= _SYS_HEAP_MIN_BLK_USIZE_)
            {
                // get the whole block
                nunits=ptr->units;
            }

                    if(ptr->units == nunits)
                    {
                        // exact match
                        if(prev)
                        {
                            prev->next = ptr->next;
                        }
                        else
                        {
                            heapDesc->_heapHead = ptr->next;
                            prev = heapDesc->_heapHead;
                        }

                    if(heapDesc->_heapTail == ptr)
                {
                    heapDesc->_heapTail = prev;
                }
			}
			else
			{   // larger than we need
				ptr->units -= nunits;
				ptr += ptr->units;
				ptr->units = nunits;
			}

            heapDesc->_heapAllocatedUnits += nunits;
            if (heapDesc->_heapSemaphoreInit == OSAL_RESULT_TRUE)
            {
                if (OSAL_SEM_Post(heapDesc->_heapSemaphore) != OSAL_RESULT_TRUE)
                {
                    // SYS_DEBUG message
                }
            }
            return ptr+1;
		}
	}

    heapDesc->_lastHeapErr = SYS_HEAP_RES_NO_MEM;
    if (heapDesc->_heapSemaphoreInit == OSAL_RESULT_TRUE)
    {
        if (OSAL_SEM_Post(heapDesc->_heapSemaphore) != OSAL_RESULT_TRUE)
        {
            // SYS_DEBUG message
        }
    }
    return 0;
}

/*******************************************************************************
 * Function:        void* SYS_HEAP_Calloc( SYS_MODULE_OBJ heapHandle,
                                           size_t nElems,
                                           size_t elemSize );
 *
 * PreCondition:    heapHandle       - valid heap handle
 *
 * Input:           heapHandle       - handle to a heap
 *                  nElems      - number of elements to be allocated
 *                  elemSize    - size of each element, in bytes
 *
 * Output:          a valid pointer to an allocated block if allocation
 *                  succeeded,
 *                  NULL otherwise.
 *
 * Side Effects:    None
 *
 * Overview:        The function checks the heap for finding a block large
 *                  enough to accommodate
 *                  nElems*elemSize request.
 *                  If the block is found, it is zero initialized and returned
 *                  to user. A first fit algorithm is used.
 *
 * Note:            None
 ******************************************************************************/
void* _SYS_HEAP_Calloc(SYS_MODULE_OBJ heapHandle, size_t nElems,
                        size_t elemSize)
{
    void* pBuff = _SYS_HEAP_Malloc(heapHandle, nElems * elemSize);
    if(pBuff)
    {
        memset(pBuff, 0, nElems * elemSize);
    }

    return pBuff;

}

/*******************************************************************************
 * Function:        int SYS_HEAP_Free(SYS_MODULE_OBJ heapHandle, void* pBuff);
 *
 * PreCondition:    heapHandle       - valid heap handle
 *
 * Input:           heapHandle       - handle to a heap
 *                  pBuff       - pointer to a buffer previously allocated from
 *                                the heap
 *
 * Output:          true if the operation succeeded,
 *                  false otherwise
 *
 * Side Effects:    None
 *
 * Overview:        The function returns the buffer to the heap.
 *                  Left and/or right defragment operations are performed if
 *                  possible.
 *
 * Note:            None
 ******************************************************************************/
int _SYS_HEAP_Free(SYS_MODULE_OBJ heapHandle, const void* pBuff)
{  
    SYS_HEAP_DESC   *heapDesc;
    _SYS_HEAP_NODE  *hdr, *ptr;
    int             fail;
    size_t          freedUnits;

    if(!pBuff)
    {
        return 0;
    }

    heapDesc = ( SYS_HEAP_DESC *)heapHandle;
    
    ptr = (_SYS_HEAP_NODE *)pBuff-1;

    if (heapDesc->_heapSemaphoreInit == OSAL_RESULT_TRUE)
    {
        if (OSAL_SEM_Pend(heapDesc->_heapSemaphore, OSAL_WAIT_FOREVER) !=
                                                            OSAL_RESULT_TRUE)
        {
            // SYS_DEBUG message
        }
    }

#ifdef SYS_STACK_DRAM_DEBUG_ENABLE
    
    if(ptr < heapDesc->_heapStart || ptr+ptr->units > heapDesc->_heapEnd)
    {
        // not one of our pointers!!!
        heapDesc->_lastHeapErr = SYS_HEAP_RES_PTR_ERR;
        if (heapDesc->_heapSemaphoreInit == OSAL_RESULT_TRUE)
        {
            if (OSAL_SEM_Post(heapDesc->_heapSemaphore) != OSAL_RESULT_TRUE)
            {
                // SYS_DEBUG message
            }
        }
        return 0;
    }
    
#endif
    
    freedUnits = ptr->units;
    
    fail = 0;
    
    if(!heapDesc->_heapHead)
    {
        ptr->next=0;
        heapDesc->_heapHead = heapDesc->_heapTail = ptr;
    }
    else if(ptr < heapDesc->_heapHead)
    {   // put it in front
        if(ptr+ptr->units == heapDesc->_heapHead)
        {   // compact
            ptr->units += heapDesc->_heapHead->units;
            ptr->next = heapDesc->_heapHead->next;
            if(heapDesc->_heapTail == heapDesc->_heapHead)
            {
                heapDesc->_heapTail = ptr;
            }
        }
        else
        {
            ptr->next = heapDesc->_heapHead;
        }
        heapDesc->_heapHead = ptr;    // new head
    }
    else if(ptr > heapDesc->_heapTail)
    {   // append tail
        if(heapDesc->_heapTail + heapDesc->_heapTail->units == ptr)
        {   // compact
            heapDesc->_heapTail->units += ptr->units;
        }
        else
        {
            heapDesc->_heapTail->next = ptr;
            ptr->next = 0;
            heapDesc->_heapTail = ptr;
        }
    }
    else
    {   // somewhere in the middle
        fail = 1;
        for(hdr = heapDesc->_heapHead; hdr != 0; hdr = hdr->next)
        {
            if(hdr<ptr && ptr<hdr->next)
            {   // found a place
                if(ptr+ptr->units == hdr->next)
                {   // compact right
                    ptr->units += hdr->next->units;
                    ptr->next = hdr->next->next;
                    if(heapDesc->_heapTail == hdr->next)
                    {
                        heapDesc->_heapTail = ptr;
                    }
                }
                else
                {
                #ifdef SYS_STACK_DRAM_DEBUG_ENABLE
                    if(ptr+ptr->units > hdr->next)
                    {
                        break;  // corrupted pointer!!!
                    }
                #endif
                    ptr->next = hdr->next;
                }
                
                // compact left
                if(hdr+hdr->units == ptr)
                {
                    hdr->units += ptr->units;
                    hdr->next = ptr->next;
                    if(heapDesc->_heapTail == ptr)
                    {
                        heapDesc->_heapTail = hdr;
                    }
                }
                else
                {
                #ifdef SYS_STACK_DRAM_DEBUG_ENABLE
                    if(hdr+hdr->units > ptr)
                    {
                        break;      // corrupted pointer!!!
                    }
                #endif
                    hdr->next = ptr;
                }

                fail = 0;   // everything OK
                break;                
            }
        }
        
    }

    if(fail)
    {
        heapDesc->_lastHeapErr = SYS_HEAP_RES_PTR_ERR;   // corrupted pointer!!!
        if (heapDesc->_heapSemaphoreInit == OSAL_RESULT_TRUE)
        {
            if (OSAL_SEM_Post(heapDesc->_heapSemaphore) != OSAL_RESULT_TRUE)
            {
                // SYS_DEBUG message
            }
        }
        return 0;
    }
    
    heapDesc->_heapAllocatedUnits -= freedUnits;
    if (heapDesc->_heapSemaphoreInit == OSAL_RESULT_TRUE)
    {
        if (OSAL_SEM_Post(heapDesc->_heapSemaphore) != OSAL_RESULT_TRUE)
        {
            // SYS_DEBUG message
        }
    }
    return freedUnits;
}

/*******************************************************************************
 * Function:        size_t SYS_HEAP_Size(SYS_MODULE_OBJ heapHandle);
 *
 * PreCondition:    heapHandle       - valid heap handle
 *
 * Input:           heapHandle       - handle to a heap
 *
 * Output:          the size of the heap as it was created
 *
 * Side Effects:    None
 *
 * Overview:        The function returns the size of the heap.
 *                  This is the size that was specified when the heap was
 *                  created.
 *
 * Note:            None
 ******************************************************************************/
size_t SYS_HEAP_Size(SYS_MODULE_OBJ heapHandle)
{
    SYS_HEAP_DESC*      heapDesc;

    heapDesc = (SYS_HEAP_DESC *)heapHandle;

    return heapDesc->_heapUnits * sizeof(_SYS_HEAP_NODE);
}

/*******************************************************************************
 * Function:        size_t SYS_HEAP_MaxSize(SYS_MODULE_OBJ heapHandle);
 *
 * PreCondition:    heapHandle       - valid heap handle
 *
 * Input:           heapHandle       - handle to a heap
 *
 * Output:          the max size of a block that can be allocated from the heap
 *
 * Side Effects:    None
 *
 * Overview:        The function returns the maximum size of a block that can be
 *                  currently allocated from this heap.
 *
 * Note:            This is info only.
 *                  It can change is the heap has multiple clients.
 *
 *                  The call is expensive.
 *                  The whole heap has to be traversed to find the maximum.
 *                  If the heap is really fragmented this might take some time.
 *
 ******************************************************************************/
size_t SYS_HEAP_MaxSize(SYS_MODULE_OBJ heapHandle)
{
    SYS_HEAP_DESC   *heapDesc;
    _SYS_HEAP_NODE  *ptr;
    size_t          max_nunits;

    max_nunits = 0;
    heapDesc = (SYS_HEAP_DESC*)heapHandle;


	for(ptr = heapDesc->_heapHead; ptr != 0; ptr = ptr->next)
    {
        if(ptr->units >= max_nunits)
        {   // found block
            max_nunits = ptr->units;
        }
    }

    return max_nunits * sizeof(_SYS_HEAP_NODE);

}

/*******************************************************************************
 * Function:        size_t SYS_HEAP_FreeSize(SYS_MODULE_OBJ heapHandle);
 *
 * PreCondition:    heapHandle       - valid heap handle
 *
 * Input:           heapHandle       - handle to a heap
 *
 * Output:          the size of the available space in the heap
 *
 * Side Effects:    None
 *
 * Overview:        The function returns the size of the space currently
 *                  available in the heap.
 *
 * Note:            This is a cumulative number, counting all the existent free
 *                  space.
 *                  It is not the maximum blocks size that could be allocated
 *                  from the heap.
 ******************************************************************************/
size_t SYS_HEAP_FreeSize(SYS_MODULE_OBJ heapHandle)
{
    SYS_HEAP_DESC*      heapDesc;

    heapDesc = ( SYS_HEAP_DESC *)heapHandle;

    return (heapDesc->_heapUnits - heapDesc->_heapAllocatedUnits) *
            sizeof(_SYS_HEAP_NODE);
}

/*******************************************************************************
 * Function:        SYS_HEAP_RES SYS_HEAP_LastError(SYS_MODULE_OBJ heapHandle)
 *
 * PreCondition:    heapHandle       - valid heap handle
 *
 * Input:           heapHandle       - handle of a heap
 *
 * Output:          The last error encountered in an operation
 *                  or SYS_HEAP_RES_OK if no error occurred
 *
 * Side Effects:    None
 *
 * Overview:        The function returns the last error encountered in a heap
 *                  operation. It clears the value of the last error variable.
 *
 * Note:            The heap holds an error variable storing the last error
 *                  encountered in an operation.
 *                  This should be consulted by the caller after each operation
 *                  that returns an invalid result for checking what the error
 *                  condition was.
 ******************************************************************************/
SYS_HEAP_RES SYS_HEAP_LastError(SYS_MODULE_OBJ heapHandle)
{
    SYS_HEAP_DESC   *heapDesc;
    SYS_HEAP_RES    res;

    heapDesc = (SYS_HEAP_DESC *)heapHandle;
    res = heapDesc->_lastHeapErr;
    heapDesc->_lastHeapErr = SYS_HEAP_RES_OK;

    return res;
}

#if defined(SYS_STACK_DRAM_DEBUG_ENABLE)

void* _SYS_HEAP_MallocDebug( SYS_MODULE_OBJ heapHandle, size_t nBytes,
                             int moduleId, int lineNo)
{
    void* ptr = _SYS_HEAP_Malloc(heapHandle, nBytes);
    if(ptr == 0)
    {
        SYS_ERROR_PRINT(SYS_ERROR_WARN, _heapFailMessage, nBytes, moduleId,
                        lineNo);
        if(((SYS_HEAP_DESC*)heapHandle)->_noMemHandler != 0)
        {
            (*((SYS_HEAP_DESC*)heapHandle)->_noMemHandler)( heapHandle, nBytes,
                                                            moduleId, lineNo);
        }
    }
#if defined(_SYS_STACK_DRAM_TRACE_ENABLE)
    SYS_HEAP_AddToEntry((SYS_HEAP_DESC*)heapHandle, moduleId, nBytes, ptr);
#endif  // defined(_SYS_STACK_DRAM_TRACE_ENABLE)
    return ptr;
}

void* _SYS_HEAP_CallocDebug(SYS_MODULE_OBJ heapHandle, size_t nElems,
                                size_t elemSize, int moduleId, int lineNo)
{
    size_t nBytes = nElems * elemSize;
    void* ptr = _SYS_HEAP_Calloc(heapHandle, nElems, elemSize);

    if(ptr == 0)
    {
        SYS_ERROR_PRINT(SYS_ERROR_WARN, _heapFailMessage, nBytes, moduleId,
                        lineNo);
        if(((SYS_HEAP_DESC*)heapHandle)->_noMemHandler != 0)
        {
            (*((SYS_HEAP_DESC*)heapHandle)->_noMemHandler)( heapHandle, nBytes,
                                                            moduleId, lineNo);
        }
    }
#if defined(_SYS_STACK_DRAM_TRACE_ENABLE)
    SYS_HEAP_AddToEntry((SYS_HEAP_DESC*)heapHandle, moduleId, nBytes, ptr);
#endif  // defined(_SYS_STACK_DRAM_TRACE_ENABLE)
    return ptr;
}

int _SYS_HEAP_FreeDebug(SYS_MODULE_OBJ heapHandle, const void* pBuff,
                                                                   int moduleId)
{
    int nUnits = _SYS_HEAP_Free(heapHandle, pBuff);

#if defined(_SYS_STACK_DRAM_TRACE_ENABLE)
    if(nUnits)
    {
        SYS_HEAP_RemoveFromEntry((SYS_HEAP_DESC*)heapHandle, moduleId, nUnits);
    }
#endif  // defined(_SYS_STACK_DRAM_TRACE_ENABLE)
    return nUnits;
}

/*******************************************************************************
 * Function:      SYS_HEAP_RES SYS_HEAP_SetNoMemHandler(
 *                                              SYS_MODULE_OBJ heapHandle,
 *                                              SYS_HEAP_NO_MEM_HANDLER handler)
 *
 * PreCondition:    heapHandle       - valid heap handle
 *
 * Input:           heapHandle       - handle of a heap
 *                  handler     - notification handler to be called when a
 *                                memory allocation failed use 0 to clear
 *
 * Output:          SYS_HEAP_RES_OK if no error occurred
 *
 * Side Effects:    None
 *
 * Overview:        The function sets the out of memory notification handler.
 *                  It is a very simple notification and only one client is
 *                  supported, basically it is meant for the creator of the
 *                  heap. When the debug is enabled and some allocation fails
 *                  some system warning message will be sent to the debug port.
 *                  Also if the handler is not null it will be called.
 *
 * Note:            The handler could be a signaling function, short
 *                  and just setting an event of some sort.
 *                  However since it is not called from ISR context this is not
 *                  really a requirement and some more extensive processing can
 *                  be done.
 *
 *                  This function is implemented only when the system debug and
 *                  heap debug (SYS_STACK_DRAM_DEBUG_ENABLE) are enabled
 ******************************************************************************/
SYS_HEAP_RES SYS_HEAP_SetNoMemHandler(SYS_MODULE_OBJ heapHandle,
                                        SYS_HEAP_NO_MEM_HANDLER handler)
{
    SYS_HEAP_DESC   *heapDesc = (SYS_HEAP_DESC*)heapHandle;

    heapDesc->_noMemHandler = handler;
    return SYS_HEAP_RES_OK;

}

#endif  // defined(SYS_STACK_DRAM_DEBUG_ENABLE)

#if defined(_SYS_STACK_DRAM_TRACE_ENABLE)
static SYS_HEAP_TRACE_ENTRY* SYS_HEAP_FindEntry(SYS_HEAP_DESC* heapDesc,
                                                int moduleId, bool addNewSlot)
{
    int ix;
    SYS_HEAP_TRACE_ENTRY    *freeEntry,*pEntry;

    freeEntry = 0;
    for(ix = 0, pEntry = heapDesc->_heapTraceTbl;
            ix < sizeof(heapDesc->_heapTraceTbl) /
            sizeof(*heapDesc->_heapTraceTbl);
            ix++, pEntry++)
    {
        if(pEntry->moduleId == moduleId)
        {
            return pEntry;
        }
        else if(addNewSlot && freeEntry == 0 && pEntry->moduleId == 0)
        {
            freeEntry = pEntry;
        }
    }

    if(freeEntry)
    {
        memset(freeEntry, 0x0, sizeof(*freeEntry));
        freeEntry->moduleId = moduleId;
    }

    return freeEntry;
}

// *****************************************************************************
/*
  Function:
    void SYS_HEAP_AddToEntry( SYS_HEAP_DESC* heapDesc, int moduleId,
                                size_t nBytes, void* ptr)

  Summary:

  Description:

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    </code>

  Remarks:
    None.

*/

static void SYS_HEAP_AddToEntry( SYS_HEAP_DESC* heapDesc, int moduleId,
                                size_t nBytes, void* ptr)
{
    SYS_HEAP_TRACE_ENTRY *pEntry = SYS_HEAP_FindEntry(heapDesc, moduleId, true);

    if(pEntry)
    {
        if(ptr)
        {   // successful
            _headNode* hPtr = (_headNode*)ptr -1;
            nBytes = hPtr->units * sizeof(_headNode);
            pEntry->totAllocated += nBytes;
            pEntry->currAllocated += nBytes;
        }
        else
        {
            // allocation units
            size_t units = (nBytes+sizeof(_headNode)-1)/sizeof(_headNode)+1;
            nBytes = units * sizeof(_headNode);
            pEntry->totFailed += nBytes;
            if(nBytes > pEntry->maxFailed)
            {
                pEntry->maxFailed = nBytes;
            }
        }
    }
}

// *****************************************************************************
/*
  Function:
    void SYS_HEAP_RemoveFromEntry( SYS_HEAP_DESC* heapDesc, int moduleId,
                                    size_t nUnits)

  Summary:


  Description:

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    </code>

  Remarks:
    None.

*/

static void SYS_HEAP_RemoveFromEntry( SYS_HEAP_DESC* heapDesc, int moduleId,
                                      size_t nUnits)
{
    SYS_HEAP_TRACE_ENTRY *pEntry = SYS_HEAP_FindEntry(heapDesc, moduleId,
                                                      false);

    if(pEntry)
    {
        pEntry->currAllocated -= nUnits * sizeof(_headNode);
    }
}

/*******************************************************************************
 * Function:      bool  SYS_HEAP_TraceGetEntry( SYS_MODULE_OBJ heapHandle,
 *                                              int entryIx,
 *                                              SYS_HEAP_TRACE_ENTRY* tEntry )
 *
 * PreCondition:    None
 *
 * Input:           heapHandle       - handle of a heap
 *                  entryIx     - index of the requested heap trace entry
 *                  tEntry      - address of a HEAP_TRACE_ENTRY that will be
 *                                updated with corresponding info
 *
 * Output:          true if tEntry was populated with the info
 *                  false if entryIx record does not exist or if tracing is not
 *                  enabled
 *
 * Side Effects:    None
 *
 * Overview:        The function returns the heap trace info for the requested
 *                  entry.
 *
 * Note:
 *                  Trace info is recorded only when
 *                  SYS_STACK_DRAM_DEBUG_ENABLE and SYS_STACK_DRAM_TRACE_ENABLE
 *                  are enabled Also, the number of defined slots
 *                  SYS_STACK_DRAM_TRACE_SLOTS has to be big enough to
 *                  accommodate all the modules.
 *
 *                  The moduleId is the one from SYS.h::SYS_STACK_MODULE
 ******************************************************************************/
bool SYS_HEAP_TraceGetEntry(SYS_MODULE_OBJ heapHandle, int entryIx,
                            SYS_HEAP_TRACE_ENTRY* tEntry)
{
    SYS_HEAP_TRACE_ENTRY *pEntry;
    SYS_HEAP_DESC   *heapDesc = (SYS_HEAP_DESC*)heapHandle;

    if(entryIx < sizeof(heapDesc->_heapTraceTbl) /
            sizeof(*heapDesc->_heapTraceTbl))
    {   // valid index
        pEntry = heapDesc->_heapTraceTbl + entryIx;
        if(pEntry->moduleId > 0)
        {
            *tEntry = *pEntry;
            return true;
        }
    }

    return false;
}

/*******************************************************************************
 * Function:      int  SYS_HEAP_TraceGetEntriesNo( SYS_MODULE_OBJ heapHandle,
 *                                                 bool getUsed )
 *
 * PreCondition:    None
 *
 * Input:           heapHandle       - handle of a heap
 *                  getUsed     - if true, only the currently used record slots
 *                                will be returned otherwise the whole number of
 *                                slots will be returned.
 *
 * Output:          number of entries the heap has trace slots for
 *
 * Side Effects:    None
 *
 * Overview:        The function returns the number of slots that hold trace
 *                  info. By using the "getUsed" parameter, only the number of
 *                  slots that hold recorded info will be returned.
 *
 * Note:
 *                  Trace info is recorded only when
 *                  SYS_STACK_DRAM_DEBUG_ENABLE and SYS_STACK_DRAM_TRACE_ENABLE
 *                  are enabled. Also, the number of defined slots
 *                  SYS_STACK_DRAM_TRACE_SLOTS has to be big enough to
 *                  accommodate all the modules.
 *
 *                  The moduleId is the one from SYS.h::SYS_STACK_MODULE
 *
 *                  If trace is enabled the function will return the value of
 *                  SYS_STACK_DRAM_TRACE_SLOTS
 ******************************************************************************/
int SYS_HEAP_TraceGetEntriesNo(SYS_MODULE_OBJ heapHandle, bool getUsed)
{
    SYS_HEAP_DESC   *heapDesc = (SYS_HEAP_DESC*)heapHandle;

    if(getUsed)
    {
        SYS_HEAP_TRACE_ENTRY *pEntry;
        int ix, nUsed;

        nUsed = 0;

        for(ix = 0, pEntry = heapDesc->_heapTraceTbl;
                ix < sizeof(heapDesc->_heapTraceTbl) /
                sizeof(*heapDesc->_heapTraceTbl);
                ix++, pEntry++)
        {
            if(pEntry->moduleId > 0)
            {
                nUsed++;
            }
        }

        return nUsed;
    }

    return sizeof(heapDesc->_heapTraceTbl)/sizeof(*heapDesc->_heapTraceTbl);
}


#endif  // defined(_SYS_STACK_DRAM_TRACE_ENABLE)


