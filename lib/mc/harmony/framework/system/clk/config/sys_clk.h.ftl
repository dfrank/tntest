<#--
/*******************************************************************************
  Clock System Service Freemarker Template File

  Company:
    Microchip Technology Inc.

  File Name:
    sys_clk.ftl

  Summary:
    Clock System Service Freemarker Template File

  Description:

*******************************************************************************/

/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS  WITHOUT  WARRANTY  OF  ANY  KIND,
EITHER EXPRESS  OR  IMPLIED,  INCLUDING  WITHOUT  LIMITATION,  ANY  WARRANTY  OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A  PARTICULAR  PURPOSE.
IN NO EVENT SHALL MICROCHIP OR  ITS  LICENSORS  BE  LIABLE  OR  OBLIGATED  UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,  BREACH  OF  WARRANTY,  OR
OTHER LEGAL  EQUITABLE  THEORY  ANY  DIRECT  OR  INDIRECT  DAMAGES  OR  EXPENSES
INCLUDING BUT NOT LIMITED TO ANY  INCIDENTAL,  SPECIAL,  INDIRECT,  PUNITIVE  OR
CONSEQUENTIAL DAMAGES, LOST  PROFITS  OR  LOST  DATA,  COST  OF  PROCUREMENT  OF
SUBSTITUTE  GOODS,  TECHNOLOGY,  SERVICES,  OR  ANY  CLAIMS  BY  THIRD   PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE  THEREOF),  OR  OTHER  SIMILAR  COSTS.
*******************************************************************************/
-->
<#if CONFIG_USE_SYS_CLK == true>

// *****************************************************************************
/* Clock System Service Configuration Options
*/

#define SYS_CLK_SOURCE                      ${CONFIG_SYS_CLK_SOURCE}
#define SYS_CLK_FREQ                        ${CONFIG_SYS_CLK_FREQ}ul
#define SYS_CLK_CONFIG_PRIMARY_XTAL         ${CONFIG_SYS_CLK_CONFIG_PRIMARY_XTAL}ul
#define SYS_CLK_CONFIG_SECONDARY_XTAL       ${CONFIG_SYS_CLK_CONFIG_SECONDARY_XTAL}ul
#define SYS_CLK_CONFIG_SYSPLL_INP_DIVISOR   ${CONFIG_SYS_CLK_CONFIG_SYSPLL_INP_DIVISOR}
#define SYS_CLK_CONFIG_FREQ_ERROR_LIMIT     ${CONFIG_SYS_CLK_CONFIG_FREQ_ERROR_LIMIT}
<#if CONFIG_SYS_CLK_CONFIGBIT_USBPLL_ENABLE?has_content>
<#if CONFIG_UPLLEN?has_content>
<#if CONFIG_UPLLEN == "ON">
#define SYS_CLK_CONFIGBIT_USBPLL_ENABLE     true
<#else>
#define SYS_CLK_CONFIGBIT_USBPLL_ENABLE     false
</#if></#if><#-- CONFIG_UPLLEN --></#if><#-- CONFIG_SYS_CLK_CONFIGBIT_USBPLL_ENABLE -->
<#if CONFIG_SYS_CLK_CONFIG_USB_CLOCK?has_content>
<#if CONFIG_UPLLFSEL?has_content>
<#if CONFIG_UPLLFSEL = "FREQ_24MHZ">
#define SYS_CLK_CONFIG_USB_CLOCK            24000000ul
</#if>
<#if CONFIG_UPLLFSEL = "FREQ_12MHZ">
#define SYS_CLK_CONFIG_USB_CLOCK            12000000ul
</#if></#if><#-- CONFIG_UPLLFSEL --></#if><#-- CONFIG_SYS_CLK_CONFIG_USB_CLOCK -->
<#if CONFIG_SYS_CLK_CONFIGBIT_USBPLL_DIVISOR?has_content>
<#if CONFIG_UPLLIDIV?has_content>
<#if CONFIG_UPLLIDIV = "DIV_1">
#define SYS_CLK_CONFIGBIT_USBPLL_DIVISOR    1
<#elseif CONFIG_UPLLIDIV = "DIV_2">
#define SYS_CLK_CONFIGBIT_USBPLL_DIVISOR    2
<#elseif CONFIG_UPLLIDIV = "DIV_3">
#define SYS_CLK_CONFIGBIT_USBPLL_DIVISOR    3
<#elseif CONFIG_UPLLIDIV = "DIV_4">
#define SYS_CLK_CONFIGBIT_USBPLL_DIVISOR    4
<#elseif CONFIG_UPLLIDIV = "DIV_5">
#define SYS_CLK_CONFIGBIT_USBPLL_DIVISOR    5
<#elseif CONFIG_UPLLIDIV = "DIV_6">
#define SYS_CLK_CONFIGBIT_USBPLL_DIVISOR    6
<#elseif CONFIG_UPLLIDIV = "DIV_10">
#define SYS_CLK_CONFIGBIT_USBPLL_DIVISOR    10
<#elseif CONFIG_UPLLIDIV = "DIV_12">
#define SYS_CLK_CONFIGBIT_USBPLL_DIVISOR    12
</#if></#if><#-- CONFIG_UPLLIDIV --></#if><#-- CONFIG_SYS_CLK_CONFIGBIT_USBPLL_DIVISOR -->
<#if CONFIG_SYS_CLK_WAIT_FOR_SWITCH == true>
#define SYS_CLK_WAIT_FOR_SWITCH             true
<#else>
#define SYS_CLK_WAIT_FOR_SWITCH             false
</#if>
<#if CONFIG_SYS_CLK_KEEP_SECONDARY_OSC_ENABLED == true>
#define SYS_CLK_KEEP_SECONDARY_OSC_ENABLED  true
<#else>
#define SYS_CLK_KEEP_SECONDARY_OSC_ENABLED  false
</#if>
#define SYS_CLK_ON_WAIT                     ${CONFIG_SYS_CLK_ON_WAIT}

</#if> <#-- CONFIG_USE_SYS_CLK -->
<#--
/*******************************************************************************
 End of File
*/
-->
