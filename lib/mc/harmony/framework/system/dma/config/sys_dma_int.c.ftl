<#--
/*******************************************************************************
  System DMA Service Interrupt Handler Template File

  File Name:
    sys_dma_int.c

  Summary:
    This file contains source code necessary to initialize the system.

  Description:
    This file contains source code necessary to run the system.  It
	generates code that is added to system_interrupt.c in order to handle
	all interrupts.
 *******************************************************************************/

/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->
<#if CONFIG_USE_SYS_DMA == true>
<#if CONFIG_SYS_DMA_INTERRUPT_MODE_CH0 == true>
void __ISR(${CONFIG_SYS_DMA_ISR_VECTOR_CH0}, ipl${CONFIG_SYS_DMA_INT_PRIO_NUM_CH0}) _IntHandlerSysDmaCh0(void)
{
	SYS_DMA_TasksISR(sysObj.sysDma, DMA_CHANNEL_0);
}
</#if>
<#if CONFIG_SYS_DMA_INTERRUPT_MODE_CH1 == true>
void __ISR(${CONFIG_SYS_DMA_ISR_VECTOR_CH1}, ipl${CONFIG_SYS_DMA_INT_PRIO_NUM_CH1}) _IntHandlerSysDmaCh1(void)
{
	SYS_DMA_TasksISR(sysObj.sysDma, DMA_CHANNEL_1);
}
</#if>
<#if CONFIG_SYS_DMA_INTERRUPT_MODE_CH2 == true>
void __ISR(${CONFIG_SYS_DMA_ISR_VECTOR_CH2}, ipl${CONFIG_SYS_DMA_INT_PRIO_NUM_CH2}) _IntHandlerSysDmaCh2(void)
{
	SYS_DMA_TasksISR(sysObj.sysDma, DMA_CHANNEL_2);
}
</#if>
<#if CONFIG_SYS_DMA_INTERRUPT_MODE_CH3 == true>
void __ISR(${CONFIG_SYS_DMA_ISR_VECTOR_CH3}, ipl${CONFIG_SYS_DMA_INT_PRIO_NUM_CH3}) _IntHandlerSysDmaCh3(void)
{
	SYS_DMA_TasksISR(sysObj.sysDma, DMA_CHANNEL_3);
}
</#if>
<#if CONFIG_SYS_DMA_INTERRUPT_MODE_CH4 == true>
void __ISR(${CONFIG_SYS_DMA_ISR_VECTOR_CH4}, ipl${CONFIG_SYS_DMA_INT_PRIO_NUM_CH4}) _IntHandlerSysDmaCh4(void)
{
	SYS_DMA_TasksISR(sysObj.sysDma, DMA_CHANNEL_4);
}
</#if>
<#if CONFIG_SYS_DMA_INTERRUPT_MODE_CH5 == true>
void __ISR(${CONFIG_SYS_DMA_ISR_VECTOR_CH5}, ipl${CONFIG_SYS_DMA_INT_PRIO_NUM_CH5}) _IntHandlerSysDmaCh5(void)
{
	SYS_DMA_TasksISR(sysObj.sysDma, DMA_CHANNEL_5);
}
</#if>
<#if CONFIG_SYS_DMA_INTERRUPT_MODE_CH6 == true>
void __ISR(${CONFIG_SYS_DMA_ISR_VECTOR_CH6}, ipl${CONFIG_SYS_DMA_INT_PRIO_NUM_CH6}) _IntHandlerSysDmaCh6(void)
{
	SYS_DMA_TasksISR(sysObj.sysDma, DMA_CHANNEL_6);
}
</#if>
<#if CONFIG_SYS_DMA_INTERRUPT_MODE_CH7 == true>
void __ISR(${CONFIG_SYS_DMA_ISR_VECTOR_CH7}, ipl${CONFIG_SYS_DMA_INT_PRIO_NUM_CH7}) _IntHandlerSysDmaCh7(void)
{
	SYS_DMA_TasksISR(sysObj.sysDma, DMA_CHANNEL_7);
}
</#if>
</#if>