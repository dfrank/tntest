/*******************************************************************************
  DMA System Service Library

  Company:
    Microchip Technology Inc.

  File Name:
    sys_dma.c

  Summary:
    DMA System Service.

  Description:
    This file implements the core functionality of the DMA System Service.
    It provides a way to interact with the DMA subsystem to
    manage the data transfer between different peripherals and/or memory
    without intervention from the CPU.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: File includes
// *****************************************************************************
// *****************************************************************************

#include "system_config.h"
#include "system/int/sys_int.h"
#include "system/dma/sys_dma.h"
#include "system/dma/src/sys_dma_local.h"



// *****************************************************************************
/* DMA Channel instance objects.

  Summary:
    Defines the DMA channel objects

  Description:
    This data type defines the DMA channel objects. It holds the information
    about the usage/availability of a channel for DMA data transfers

  Remarks:
    Not all modes are available on all micro-controllers.
 */

SYS_DMA_CHANNEL_OBJECT  gSysDMAChannelObj[SYS_DMA_CHANNEL_COUNT];


// *****************************************************************************
// *****************************************************************************
// Section: DMA System Service Interface Routines
// *****************************************************************************
// *****************************************************************************


// *****************************************************************************
// *****************************************************************************
// Section: Initialization
// *****************************************************************************
// *****************************************************************************
//******************************************************************************
/* Function:
    SYS_MODULE_OBJ SYS_DMA_Initialize ( const SYS_MODULE_INIT * const init)

  Summary:
    Initializes and Enables the DMA Controller.

  Description:
    This function Enables the DMA module. Enable/Disable stop in idle mode
    feature based on the passed parameter value.

    This routine initializes the DMA module making it ready for clients to
    open and use it. The initialization data is specified by the init parameter.


  Precondition:
    None.

  Parameters:
    init            - Pointer to the data structure containing any data
                      necessary to initialize the hardware. This pointer may
                      be null if no data is required and default
                      initialization is to be used.

  Returns:
    If successful, returns a valid handle to the DMA module object.
    Otherwise, it returns SYS_MODULE_OBJ_INVALID.

  Example:
  <code>
    SYS_MODULE_OBJ objectHandle;
    SYS_DMA_INIT initParam;

    initParam.sidl = SYS_DMA_SIDL_ENABLE;
    objectHandle = SYS_DMA_Initialize(DRV_I2S_INDEX_1,
                                      (SYS_MODULE_INIT*)initParam);
    if (SYS_MODULE_OBJ_INVALID == objectHandle)
    {
        // Handle error
    }
  </code>

  Remarks:
    This routine must be called before any other DMA systems service routines
    are called.

    Not all features are available on all micro-controllers.
*/
SYS_MODULE_OBJ SYS_DMA_Initialize (const SYS_MODULE_INIT * const init)
{
    SYS_DMA_INIT *initp;
    uint8_t chanIndex;
    SYS_DMA_CHANNEL_OBJECT *chanObj;


    /* Validate the init object */
    if ((SYS_MODULE_INIT *)NULL == init)
    {
        SYS_ASSERT(false, "Invalid Init Object");
        return SYS_MODULE_OBJ_INVALID;
    }

    initp = (SYS_DMA_INIT *)init;

    /* Enable/disable the stop in idle mode feature. */
    if(true == PLIB_DMA_ExistsStopInIdle(DMA_ID_0))
    {
        if(SYS_DMA_SIDL_DISABLE == initp->sidl)
        {
            PLIB_DMA_StopInIdleDisable(DMA_ID_0);
        }
        else if(SYS_DMA_SIDL_ENABLE == initp->sidl)
        {
            PLIB_DMA_StopInIdleEnable(DMA_ID_0);
        }
    }

    /* Enable the DMA module */
    if(true == PLIB_DMA_ExistsEnableControl(DMA_ID_0))
    {
        PLIB_DMA_Enable(DMA_ID_0);
    }

    /* Initialize the available channel objects */
    chanObj             =   (SYS_DMA_CHANNEL_OBJECT *)&gSysDMAChannelObj[0];
    for(chanIndex = 0; chanIndex < SYS_DMA_CHANNEL_COUNT; chanIndex++)
    {
        chanObj->inUse          =    false;
        chanObj                 =    chanObj + 1;
    }

    /* Return the object structure */
    return ((SYS_MODULE_OBJ) initp);
}


// *****************************************************************************
// *****************************************************************************
// Section:  Channel Setup and management routines
// *****************************************************************************
// *****************************************************************************

//******************************************************************************
/* Function:
    SYS_DMA_CHANNEL_HANDLE SYS_DMA_ChannelAllocate (DMA_CHANNEL channel)

  Summary:
    Allocates the specified DMA channel and returns a handle to it

  Description:
    This routine opens the specified DMA channel and provides a
    handle that must be provided to all other client-level operations to
    identify the caller and the DMA channel.

  Precondition:
    Function SYS_DMA_Initialize must have been called before calling this
    function.

  Parameters:
    channel     - Channel number requested for allocation.
                  When channel number sepcified is DMA_CHANNEL_ANY
                  a random channel is allocated for DMA transfers.

  Returns:
     The channel handle for the requested channel number.

     If an error occurs, the return value is SYS_DMA_CHANNEL_HANDLE_INVALID.
     Error can occur
     - if the requested channel number is invalid.
     - if the requested channel number is not free.

  Example:
  <code>
    DMA_CHANNEL channel;
    SYS_DMA_CHANNEL_HANDLE handle

    channel = DMA_CHANNEL_2;
    handle = SYS_DMA_ChannelAllocate(channel);
    if (SYS_DMA_CHANNEL_HANDLE_INVALID == handle)
    {
        // Failed to allocate the channel
    }
    else
    {
        // Proceed with setting up the channel and adding the transfer
    }
  </code>

  Remarks:
    The handle returned is valid until the SYS_DMA_ChannelRelease routine is called.
    This routine must be called before any other DMA channel Setup and management
    routines are called
*/
SYS_DMA_CHANNEL_HANDLE SYS_DMA_ChannelAllocate (DMA_CHANNEL channel)
{
    SYS_DMA_CHANNEL_HANDLE              channelHandle;
    SYS_DMA_CHANNEL_OBJECT              *chanObj;
    uint8_t                             chanIndex;
    bool                                chanAlloc;

    channelHandle       =   (SYS_DMA_CHANNEL_HANDLE)NULL;
    chanAlloc           =   false;
    chanObj             =   (SYS_DMA_CHANNEL_OBJECT *)&gSysDMAChannelObj[0];

    for(chanIndex = 0; chanIndex < SYS_DMA_CHANNEL_COUNT; chanIndex++)
    {
        /* Iterate to find a free channel */
        if(false == chanObj->inUse)
        {
            /* If user's choice is any,
             * Allocate the first available free channel */
            if(DMA_CHANNEL_ANY == channel)
            {
                chanObj->inUse          = true;
                chanAlloc               = true;
                break;
            }
            /* Check if the requested channel is free */
            else if(chanIndex == channel)
            {
                chanObj->inUse          = true;
                chanAlloc               = true;
                break;
            }

        }
        /* Check the next channel object */
        chanObj += 1;
    }
    if(true == chanAlloc )
    {
        /* Return the valid found handle */
        channelHandle  = (SYS_DMA_CHANNEL_HANDLE)chanObj;
    }
    else
    {
        /* Return an Invalid handle */
        channelHandle = SYS_DMA_CHANNEL_HANDLE_INVALID;
    }
    return channelHandle;
}


//******************************************************************************
/* Function:
    void SYS_DMA_ChannelRelease (SYS_DMA_CHANNEL_HANDLE handle)

  Summary:
    Deallocates and frees the channel specified by the handle.

  Description:
    This routine deallocates an allocated-channel of the DMA module,
    invalidating the handle.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.
    DMA channel should have been allocated by calling SYS_DMA_ChannelAllocate

  Parameters:
    handle         -  A valid aloocated-channel handle, returned from the service's
                      Allocate routine

  Returns:
    None.

  Example:
  <code>
    DMA_CHANNEL channel;
    SYS_DMA_CHANNEL_HANDLE handle;

    channel = DMA_CHANNEL_2;
    handle = SYS_DMA_ChannelAllocate(channel);
    SYS_DMA_ChannelRelease(handle);
  </code>

  Remarks:
    After calling this routine, the handle passed in "handle" must not be used
    with any of the remaining service's routines.  A new handle must be obtained by
    calling SYS_DMA_ChannelAllocate before the caller may use the service again
*/
 void SYS_DMA_ChannelRelease (SYS_DMA_CHANNEL_HANDLE handle)
 {
    uint8_t index;
    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL channelNumber;

    chanObj = (SYS_DMA_CHANNEL_OBJECT *) handle;
    channelNumber = (chanObj  - (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[0]);
    chanObj->inUse = false;
    /* Disable all interrupts on this channel */
    for(index = 0; index < 8 ;index++)
    {
        PLIB_DMA_ChannelXINTSourceDisable( DMA_ID_0, channelNumber, (1<<index));
    }
 }


//******************************************************************************
/* Function:
    void SYS_DMA_ChannelSetup ( SYS_DMA_CHANNEL_HANDLE handle,
                                DMA_CHANNEL_PRIORITY priority,
                                SYS_DMA_CHANNEL_OP_MODE modeEnable
                                DMA_TRIGGER_SOURCE eventSrc )

  Summary:
    Setup the DMA channel parameters.

  Description:
    This function sets up the DMA channel parameters.
    It sets the channel priority and enables the mode of operations for the
    current system design.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.
    DMA channel should have been allocated by calling SYS_DMA_ChannelAllocate.

  Parameters:
    handle          -   Handle of the DMA channel as returned by the
                        SYS_DMA_ChannelAllocate function.
    priority        -   The priorityvto be associated to the channel.
    modeEnable      -   The supported operating modes to be enabled.
                        This parameter can be logically ored to specify
                        multiple options.
    eventSrc        -   The event causing the cell transfer start.

  Returns:
    None.

  Example:
  <code>

     // 'handle' is a valid handle returned
     // by the SYS_DMA_ChannelAllocate function.

    DMA_CHANNEL_PRIORITY        priority;
    SYS_DMA_CHANNEL_OP_MODE     modeEnable;
    DMA_TRIGGER_SOURCE          eventSrc;

    channel         =   DMA_CHANNEL_2;
    priority        =   DMA_CHANNEL_PRIORITY_1;
    modeEnable      =   (SYS_DMA_CHANNEL_OP_MODE_BASIC | SYS_DMA_CHANNEL_OP_MODE_CRC);
    eventSrc        =   DMA_TRIGGER_USART_1_TRANSMIT;
     // Setup channel number, priority and enables basic and CRC mode
    SYS_DMA_ChannelSetup(handle, priority, modeEnable,eventSrc);
  </code>

  Remarks:
    If SYS_DMA_CHANNEL_OP_MODE_MATCH_ABORT, SYS_DMA_CHANNEL_OP_MODE_CHAIN or
    SYS_DMA_CHANNEL_OP_MODE_CRC mode of operation is enabled, then corresponding
    mode setup API's needs to be called to set the related parameters.

    If the parameter 'eventSrc' is specified as DMA_TRIGGER_SOURCE_NONE then
    SYS_DMA_ChannelForceStart must be called to start the DMA channel transfer.

    Not all features are available on all micro-controllers.
  */

void SYS_DMA_ChannelSetup ( SYS_DMA_CHANNEL_HANDLE handle,
                            DMA_CHANNEL_PRIORITY priority,
                            SYS_DMA_CHANNEL_OP_MODE modeEnable,
                            DMA_TRIGGER_SOURCE eventSrc )
{

    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL channelNumber;

    chanObj = (SYS_DMA_CHANNEL_OBJECT *) handle;
    channelNumber = (chanObj  - (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[0]);
    /* Setup the channel priority */
    if(true == PLIB_DMA_ExistsChannelXPriority(DMA_ID_0))
    {
        PLIB_DMA_ChannelXPrioritySelect(DMA_ID_0,
                                        channelNumber,
                                        priority);
    }

    /* Setup the channel modes */
    /* Basic mode, nothing specifically needs to be set */
    if(SYS_DMA_CHANNEL_OP_MODE_BASIC & modeEnable)
    {
        /* Do nothing */
        ;
    }

    /* Pattern match Abort */
    if(SYS_DMA_CHANNEL_OP_MODE_MATCH_ABORT & modeEnable)
    {
        if(true == PLIB_DMA_ExistsChannelXTrigger(DMA_ID_0))
        {
            PLIB_DMA_ChannelXTriggerEnable( DMA_ID_0,
                                            channelNumber,
                                            DMA_CHANNEL_TRIGGER_PATTERN_MATCH_ABORT);
        }
    }

    /* Channel chaining mode */
    if(SYS_DMA_CHANNEL_OP_MODE_CHAIN & modeEnable)
    {
        if(true == PLIB_DMA_ExistsChannelXChainEnbl(DMA_ID_0))
        {
            PLIB_DMA_ChannelXChainEnable( DMA_ID_0,
                                            channelNumber);
        }
    }

    /* Auto Enable mode */
    if(SYS_DMA_CHANNEL_OP_MODE_AUTO & modeEnable)
    {
        if(true == PLIB_DMA_ExistsChannelXAuto(DMA_ID_0))
        {
            PLIB_DMA_ChannelXAutoEnable( DMA_ID_0,
                                         channelNumber);
        }
    }

    /* CRC Enable */
    if(SYS_DMA_CHANNEL_OP_MODE_CRC & modeEnable)
    {
        if(true == PLIB_DMA_ExistsCRC(DMA_ID_0))
        {
            PLIB_DMA_CRCEnable( DMA_ID_0);
        }
    }


    /* Setup the DMA Trigger Source and Enable it */
    if(true == PLIB_DMA_ExistsChannelXStartIRQ(DMA_ID_0))
    {
        if(DMA_TRIGGER_SOURCE_NONE == eventSrc)
        {
            /* This is polling mode of Implementation */
            ;
            PLIB_DMA_ChannelXINTSourceEnable(DMA_ID_0, channelNumber,
                                            DMA_INT_BLOCK_TRANSFER_COMPLETE);
            PLIB_DMA_ChannelXINTSourceEnable(DMA_ID_0, channelNumber,
                                            DMA_INT_ADDRESS_ERROR);
        }
        else
        {
            PLIB_DMA_ChannelXStartIRQSet( DMA_ID_0,
                                          channelNumber,
                                          eventSrc);

            PLIB_DMA_ChannelXTriggerEnable( DMA_ID_0,
                                            channelNumber,
                                            DMA_CHANNEL_TRIGGER_TRANSFER_START);

            /* We need to notify the user on the completion of a
             * transfer request.
             * Setting the DMA block completion transfer interrupt. */
            PLIB_DMA_ChannelXINTSourceEnable(DMA_ID_0, channelNumber,
                                                DMA_INT_BLOCK_TRANSFER_COMPLETE);
            PLIB_DMA_ChannelXINTSourceEnable(DMA_ID_0, channelNumber,
                                            DMA_INT_ADDRESS_ERROR);
        }
    }


}


//******************************************************************************
/* Function:
    void SYS_DMA_ChannelSetupMatchAbortMode( SYS_DMA_CHANNEL_HANDLE handle,
                                             uint16_t pattern,
                                             DMA_PATTERN_LENGTH length,
                                             SYS_DMA_CHANNEL_IGNORE_MATCH ignore,
                                             uint8_t ignorePattern )

  Summary:
    Setup the pattern match abort mode.

  Description:
    This  function sets up the termination of DMA operation when the specified
    pattern is matched. Additionally on supported parts the function also
    sets up the ignoring of part of a pattern(8-bit) from match abort
    pattern(16-bit).

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.
    DMA channel should have been allocated by calling SYS_DMA_ChannelAllocate.
    The function SYS_DMA_ChannelSetup must be called to enable
    SYS_DMA_CHANNEL_OP_MODE_MATCH_ABORT before setting up pattern
    match mode features.

  Parameters:
    handle          -   Handle of the DMA channel as returned by the
                        SYS_DMA_ChannelAllocate function.
    pattern         -  The pattern that needs to be matched to abort a DMA transfer.
    length          -  Match pattern length can be 1-byte or 2-byte.
    ignore          -  Enable/Disable a byte between a 2-byte pattern match.
    ignorePattern   -  The part of the pattern(8-bit) that needs to be ignored
                       from the match abort pattern(16-bit)

  Returns:
    None.

  Example:
  <code>
    // Example 1
    // The below code is for a device with 8-bit pattern value and no
    // support for pattern match ignore feature

    // 'handle' is a valid handle returned
    // by the SYS_DMA_ChannelAllocate function.
    uint16_t                        pattern;
    DMA_PATTERN_LENGTH              length;
    SYS_DMA_CHANNEL_IGNORE_MATCH    ignore;
    uint8_t                         ignorePattern;

    pattern         = 0x00; //Stop transfer on detection of a NULL character
    length          = DMA_PATTERN_LENGTH_NONE;
    ignore          = SYS_DMA_CHANNEL_IGNORE_MATCH_DISABLE;
    ignorePattern   = 0;
    SYS_DMA_ChannelSetupMatchAbortMode(handle, pattern, length,
                                       ignoreEnable, ignorePattern);

    // Example 2
    // The below code is for a device with  16-bit pattern value and
    // support for pattern match ignore feature

    // 'handle' is a valid handle returned
    // by the SYS_DMA_ChannelAllocate function.
    uint16_t                        pattern;
    DMA_PATTERN_LENGTH              length;
    SYS_DMA_CHANNEL_IGNORE_MATCH    ignore;
    uint8_t                         ignorePattern;

    pattern         = 0x0D0A; //Stop transfer on detection of '\r\n'
    length          = DMA_PATTERN_MATCH_LENGTH_2BYTES;
    ignore          = SYS_DMA_CHANNEL_IGNORE_MATCH_ENABLE;
    ignorePattern   = 0x00; \\ Any null character between the termination pattern
                            \\ '\r' and '\n' is ignored.
    SYS_DMA_ChannelSetupMatchAbortMode(handle, pattern, length,
                                            ignore, ignorePattern);
  </code>

  Remarks:
    The parameter 'pattern' (8-bit or 16-bit) is part specific.
    (Refer the particular datasheet for details)

    Not all features are available on all micro-controllers.
    Refer the particular datasheet for details)
*/

void SYS_DMA_ChannelSetupMatchAbortMode( SYS_DMA_CHANNEL_HANDLE handle,
                                         uint16_t pattern,
                                         DMA_PATTERN_LENGTH length,
                                         SYS_DMA_CHANNEL_IGNORE_MATCH ignore,
                                         uint8_t ignorePattern )
{
    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL channelNumber;

    chanObj = (SYS_DMA_CHANNEL_OBJECT *) handle;
    channelNumber = (chanObj  - (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[0]);
    if(PLIB_DMA_ExistsChannelXPatternLength(DMA_ID_0))
    {
        PLIB_DMA_ChannelXPatternLengthSet( DMA_ID_0,
                                           channelNumber,
                                           length );
    }

    if(true == PLIB_DMA_ExistsChannelXPatternData(DMA_ID_0))
    {
        PLIB_DMA_ChannelXPatternDataSet(DMA_ID_0,
                                        channelNumber,
                                        pattern);
    }

    if(true == PLIB_DMA_ExistsChannelXPatternIgnoreByte(DMA_ID_0))
    {
        PLIB_DMA_ChannelXPatternIgnoreByteEnable(DMA_ID_0,
                                        channelNumber);
    }

    if(true == PLIB_DMA_ExistsChannelXPatternIgnore(DMA_ID_0))
    {
        PLIB_DMA_ChannelXPatternIgnoreSet( DMA_ID_0,
                                           channelNumber,
                                           ignorePattern );
    }

}


//******************************************************************************
/* Function:
    void SYS_DMA_ChannelSetupChainingMode( SYS_DMA_CHANNEL_HANDLE handle,
                                           SYS_DMA_CHANNEL_CHAIN_PRIO priority )

  Summary:
    Sets up the channel chaining mode.

  Description:
    This function sets up the priority of channel chaining DMA operation mode.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.
    DMA channel should have been allocated by calling SYS_DMA_ChannelAllocate.
    The function SYS_DMA_ChannelSetup must be called to enable
    SYS_DMA_CHANNEL_OP_MODE_CHAIN before setting up the priority of the
    chaining channel.

  Parameters:
    handle          -   Handle of the DMA channel as returned by the
                        SYS_DMA_ChannelAllocate function.
    priority        -  Chain to channel lower/hower in natural priority.

  Returns:
    None.

  Example:
  <code>
    // 'handle' is a valid handle returned
    // by the SYS_DMA_ChannelAllocate function.
    SYS_DMA_CHANNEL_CHAIN_PRIO  priority;

    priority        = SYS_DMA_CHANNEL_CHAIN_PRIO_HIGH;
    SYS_DMA_ChannelSetupChainingMode(handle, priority);
  </code>

  Remarks:
    None.
*/

void SYS_DMA_ChannelSetupChainingMode( SYS_DMA_CHANNEL_HANDLE handle,
                                       SYS_DMA_CHANNEL_CHAIN_PRIO priority )
{
    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL channelNumber;

    chanObj = (SYS_DMA_CHANNEL_OBJECT *) handle;
    channelNumber = (chanObj  - (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[0]);
    if(true == PLIB_DMA_ExistsChannelXChain(DMA_ID_0))
    {
        if(SYS_DMA_CHANNEL_CHAIN_PRIO_HIGH == priority)
        {
            PLIB_DMA_ChannelXChainToHigher(DMA_ID_0, channelNumber);
        }
        else if(SYS_DMA_CHANNEL_CHAIN_PRIO_LOW == priority)
        {
            PLIB_DMA_ChannelXChainToLower(DMA_ID_0, channelNumber);
        }
    }

}


//******************************************************************************
/* Function:
    void SYS_DMA_ChannelSetupCRCMode( SYS_DMA_CHANNEL_HANDLE handle,
                                      SYS_DMA_CHANNEL_OPERATION_MODE_CRC crc )

  Summary:
    Sets up the CRC operation mode

  Description:
    This function sets up the CRC computation features.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.
    DMA channel should have been allocated by calling SYS_DMA_ChannelAllocate.
    The function SYS_DMA_ChannelSetup must be called to enable
    SYS_DMA_CHANNEL_OP_MODE_CRC before setting up the CRC mode.

  Parameters:
    handle          -   Handle of the DMA channel as returned by the
                        SYS_DMA_ChannelAllocate function.
    crc.type        -   CRC will calculate an IP header checksum or an LFSR CRC.
    crc.mode        -   Compute the CRC in Background/Append mode.
    crc.polyLength  -   Denotes the length of the polynomial.
    crc.bitOrder    -   CRC is calculated LSb/MSb first.
    crc.byteOrder   -   Byte selection order input pre CRC Generator
    crc.writeOrder  -   Write byte order selection post CRC computation
    crc.data        -   Computed/Seed CRC
    crc.xorBitMask  -   Enable/Disable XOR bit mask on the corresponding bits
                        when mode is LFSR

  Returns:
    None.

  Example:
  <code>
    //Example 1
    // DMA calculation using the CRC background mode

    // 'handle' is a valid handle returned
    // by the SYS_DMA_ChannelAllocate function.
    SYS_DMA_CHANNEL_OPERATION_MODE_CRC  crc;

    crc.type            = DMA_CRC_LFSR;
    crc.mode            = SYS_DMA_CHANNEL_CRC_MODE_BACKGROUND;
    crc.polyLength      = 16;
    crc.bitOrder        = DMA_CRC_BIT_ORDER_LSB;
    crc.byteOrder       = DMA_CRC_BYTEORDER_NO_SWAPPING;
    crc.writeOrder      = SYS_DMA_CRC_WRITE_ORDER_MAINTAIN;
    crc.data            = 0xFFFF;
    crc.xorBitMask      = 0x1021;
    SYS_DMA_ChannelSetupCRCMode(handle, crc);

  </code>

  Remarks:
    Not all features are available on all micro-controllers.
*/

void SYS_DMA_ChannelSetupCRCMode( SYS_DMA_CHANNEL_HANDLE handle,
                                  SYS_DMA_CHANNEL_OPERATION_MODE_CRC crc )
{
    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL channelNumber;

    chanObj = (SYS_DMA_CHANNEL_OBJECT *) handle;
    channelNumber = (chanObj  - (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[0]);

    if(true == PLIB_DMA_ExistsCRCChannel(DMA_ID_0))
    {
        PLIB_DMA_CRCChannelSelect(DMA_ID_0, channelNumber);
    }

    if(true == PLIB_DMA_ExistsCRCType(DMA_ID_0))
    {
        PLIB_DMA_CRCTypeSet(DMA_ID_0, crc.type);
    }

    if(true == PLIB_DMA_ExistsCRCPolynomialLength(DMA_ID_0))
    {
        PLIB_DMA_CRCPolynomialLengthSet(DMA_ID_0, crc.polyLength);
    }

    if(true == PLIB_DMA_ExistsCRCBitOrder(DMA_ID_0))
    {
        PLIB_DMA_CRCBitOrderSelect(DMA_ID_0, crc.bitOrder);
    }

    if(true == PLIB_DMA_ExistsCRCByteOrder(DMA_ID_0))
    {
        PLIB_DMA_CRCByteOrderSelect(DMA_ID_0, crc.byteOrder);
    }

    if(true == PLIB_DMA_ExistsCRCWriteByteOrder(DMA_ID_0))
    {
        if(SYS_DMA_CRC_WRITE_ORDER_MAINTAIN == crc.writeOrder)
        {
            PLIB_DMA_CRCWriteByteOrderMaintain(DMA_ID_0);
        }
        else if(SYS_DMA_CRC_WRITE_ORDER_CHANGE == crc.writeOrder)
        {
            PLIB_DMA_CRCWriteByteOrderAlter(DMA_ID_0);
        }
    }

    if(true == PLIB_DMA_ExistsCRCData(DMA_ID_0))
    {
        PLIB_DMA_CRCDataWrite(DMA_ID_0, crc.data);
    }

    if(true == PLIB_DMA_ExistsCRCXOREnable(DMA_ID_0))
    {
        PLIB_DMA_CRCXOREnableSet(DMA_ID_0, crc.xorBitMask);
    }
}


//******************************************************************************
/* Function:
    uint32_t SYS_DMA_ChannelGetCRC(void)

  Summary:
    Returns the computed CRC

  Description:
    This  function returns the computed CRC

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.
    DMA channel should have been allocated by calling SYS_DMA_ChannelAllocate.
    The function SYS_DMA_ChannelSetup must be called to enable
    SYS_DMA_CHANNEL_OP_MODE_CRC before setting up the CRC mode.
    The CRC generator must have been previously setup using
    SYS_DMA_ChannelSetupCRCMode.

  Parameters:
    None

  Returns:
    uint32_t    - The Computed CRC.

  Example:
  <code>
    uint32_t                    computedCRC;

    computedCRC     = SYS_DMA_ChannelGetCrc();
  </code>

  Remarks:
    To get the computed CRC value this function must be called after the block
    transfer completion event. ie After getting and processing the callback
    registered with SYS_DMA_ChannelTransferEventHandlerSet.

    Not all features are available on all micro-controllers.
*/

uint32_t SYS_DMA_ChannelGetCRC(void)
{
    uint32_t crcData;

    if(true == PLIB_DMA_ExistsCRCData(DMA_ID_0))
    {
        crcData = PLIB_DMA_CRCDataRead(DMA_ID_0);
    }
    return crcData;
}


//******************************************************************************
/* Function:
    void SYS_DMA_ChannelEnable ( SYS_DMA_CHANNEL_HANDLE handle)

  Summary:
    Enables a channel.

  Description:
    This function enables a channel.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.
    DMA channel should have been allocated by calling SYS_DMA_ChannelAllocate.
    The function SYS_DMA_ChannelSetup must have been called to setup and
    enable the required features.

  Parameters:
    handle          -   Handle of the DMA channel as returned by the
                        SYS_DMA_ChannelAllocate function.

  Returns:
    None.

  Example:
  <code>
    // 'handle' is a valid handle returned
    // by the SYS_DMA_ChannelAllocate function.

    SYS_DMA_ChannelEnable(handle);
  </code>

  Remarks:
        This function may not required to be called when starting DMA setup
        (by SYS_DMA_ChannelSetup) and transfer Add (by SYS_DMA_ChannelTransferAdd).
        But may be needed to be called in the registered callback to enable the
        channel and continue the data transfer with the existing transfer parameters
        previously set with 'SYS_DMA_ChannelTransferAdd'.
        The DMA channel is by default disabled on the completion of block
        transfer(callback generated)
*/

void SYS_DMA_ChannelEnable ( SYS_DMA_CHANNEL_HANDLE handle)
{
    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL channelNumber;

    chanObj = (SYS_DMA_CHANNEL_OBJECT *) handle;
    channelNumber = (chanObj  - (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[0]);
    if(true == PLIB_DMA_ExistsChannelX(DMA_ID_0))
    {
        PLIB_DMA_ChannelXEnable(DMA_ID_0, channelNumber);
    }
}


//******************************************************************************
/* Function:
    void SYS_DMA_ChannelDisable ( SYS_DMA_CHANNEL_HANDLE handle)

  Summary:
    Disables a channel.

  Description:
    This function disables a channel.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.
    DMA channel should have been allocated by calling SYS_DMA_ChannelAllocate.
    The function SYS_DMA_ChannelSetup must have been called to setup and
    enable the required features.
    A DMA channel should have been enabled either by calling
    'SYS_DMA_ChannelTransferAdd' or 'SYS_DMA_ChannelEnable'

  Parameters:
    handle          -   Handle of the DMA channel as returned by the
                        SYS_DMA_ChannelAllocate function.
  Returns:
    None.

  Example:
  <code>
    // 'handle' is a valid handle returned
    // by the SYS_DMA_ChannelAllocate function.

    SYS_DMA_ChannelDisable(handle);
  </code>

  Remarks:
    None.
*/

void SYS_DMA_ChannelDisable ( SYS_DMA_CHANNEL_HANDLE handle)
{
    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL channelNumber;

    chanObj = (SYS_DMA_CHANNEL_OBJECT *) handle;
    channelNumber = (chanObj  - (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[0]);
    if(true == PLIB_DMA_ExistsChannelX(DMA_ID_0))
    {
        PLIB_DMA_ChannelXDisable(DMA_ID_0, channelNumber);
    }
}


//******************************************************************************
/* Function:
    void SYS_DMA_ChannelTransferAdd (   SYS_DMA_CHANNEL_HANDLE handle,
                                        const void *srcAddr, size_t srcSize
                                        const void *destAddr, size_t destSize,
                                        size_t cellSize)

  Summary:
    Adds a data transfer to a DMA channel and Enables the channel to start
    data transfer.

  Description:
    This function adds a data transfer characteristics for a DMA channel. The
    The source and the destination addresses, source and destination lengths,
    The number of bytes transferred per cell event are set. It also enables
    the channel to start data transfer.

    If the requesting client registered an event callback with the service,
    the service will issue a SYS_DMA_TRANSFER_EVENT_COMPLETE or
    SYS_DMA_TRANSFER_EVENT_ABORT  event if the transfered was processed
    successfully of SYS_DMA_TRANSFER_EVENT_ERROR event if the transfer was not
    processed successfully.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.
    DMA channel should have been allocated by calling SYS_DMA_ChannelAllocate.
    The function SYS_DMA_ChannelSetup must have been called to setup and
    enable the required features.

  Parameters:
     handle          - Handle of the DMA channel as returned by the
                       SYS_DMA_ChannelAllocate function.
     srcAddr         - Source of the DMA transfer
     srcSize         - Size of the source
     destAddr        - Destination of the DMA transfer
     destSize        - Size of the destination
     cellSize        - Size of the cell

  Returns:
    None.

  Example:
    // Add 10 bytes of data transfer to UART Tx

    // 'handle' is a valid handle returned
    // by the SYS_DMA_ChannelAllocate function.
    MY_APP_OBJ          myAppObj;
    uint8_t             buf[10];
    void                *srcAddr;
    void                *destAddr;
    size_t              srcSize;
    size_t              destSize;
    size_t              cellSize;

    srcAddr     = (uint8_t *) buf;
    srcSize     = 10;
    destAddr    = (uin8_t*) &U2TXREG;
    destSize    = 1;
    cellSize    = 1;

    // User registers an event handler with system service. This is done once

    SYS_DMA_ChannelTransferEventHandlerSet(handle, APP_DMASYSTransferEventHandler,
                                                            (uintptr_t)&myAppObj);

    SYS_DMA_ChannelTransferAdd(handle,srcAddr,srcSize,destAddr,destSize,cellSize);

    if(SYS_DMA_CHANNEL_HANDLE_INVALID == handle)
    {
        // Error handling here
    }

    // Event Processing Technique. Event is received when
    // the transfer is processed.

    void APP_DMASYSTransferEventHandler(SYS_DMA_TRANSFER_EVENT event,
            SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle)
    {
        switch(event)
        {
            case SYS_DMA_TRANSFER_EVENT_COMPLETE:

                // This means the data was transferred.
                break;

            case SYS_DMA_TRANSFER_EVENT_ERROR:

                // Error handling here.
                break;

            default:
                break;
        }
    }
    </code>

  Remarks:
    None.
*/

void SYS_DMA_ChannelTransferAdd (   SYS_DMA_CHANNEL_HANDLE handle,
                                    const void *srcAddr, size_t srcSize,
                                    const void *destAddr, size_t destSize,
                                    size_t cellSize)
{
    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL channelNumber;

    chanObj = (SYS_DMA_CHANNEL_OBJECT *) handle;
    channelNumber = (chanObj  - (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[0]);

    /* Set the transfer paramteters */
    PLIB_DMA_ChannelXSourceStartAddressSet(DMA_ID_0, channelNumber, (uint32_t) srcAddr);
    PLIB_DMA_ChannelXSourceSizeSet(DMA_ID_0, channelNumber, (uint16_t) srcSize);
    PLIB_DMA_ChannelXDestinationStartAddressSet(DMA_ID_0, channelNumber,(uint32_t) destAddr);
    PLIB_DMA_ChannelXDestinationSizeSet(DMA_ID_0, channelNumber, (uint16_t) destSize);
    PLIB_DMA_ChannelXCellSizeSet(DMA_ID_0, channelNumber,(uint16_t) cellSize);

    /* Enable the channel */
    PLIB_DMA_ChannelXEnable(DMA_ID_0, channelNumber);

}

/******************************************************************************
  Function:
	void SYS_DMA_ChannelTransferEventHandlerSet(SYS_DMA_CHANNEL_HANDLE handle,
			const SYS_DMA_CHANNEL_TRANSFER_EVENT_HANDLER eventHandler,
			const uintptr_t contextHandle)

  Summary:
    This function allows a DMA system service client to set an event handler.

  Description:
    This function allows a client to set an event handler. The client may want
    to receive transfer related events in cases when it uses non-blocking read and
    write functions. The event handler should be set before the client
    intends to perform operations that could generate events.

    This function accepts a contextHandle parameter. This parameter could be
    set by the client to contain (or point to) any client specific data object
    that should be associated with this DMA channel.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.
    DMA channel should have been allocated by calling SYS_DMA_ChannelAllocate.


  Parameters:
    handle       - A valid channel handle, returned from the system service's
                   Allocate routine

    eventHandler - Pointer to the event handler function.

  Returns:
    None.

  Example:

    // 'handle' is a valid handle returned
    // by the SYS_DMA_ChannelAllocate function.
    MY_APP_OBJ          myAppObj;
    uint8_t             buf[10];
    void                *srcAddr;
    void                *destAddr;
    size_t              srcSize;
    size_t              destSize;
    size_t              cellSize;

    srcAddr     = (uint8_t *) buf;
    srcSize     = 10;
    destAddr    = (uin8_t*) &U2TXREG;
    destSize    = 1;
    cellSize    = 1;
    channelHandle = SYS_DMA_ChannelAllocate(channel);
    // User registers an event handler with system service. This is done once

    SYS_DMA_ChannelTransferEventHandlerSet(handle, APP_DMASYSTransferEventHandler,
                                                            (uintptr_t)&myAppObj);

    SYS_DMA_ChannelTransferAdd(handle,srcAddr,srcSize,destAddr,destSize,cellSize);

    if(SYS_DMA_CHANNEL_HANDLE_INVALID == handle)
    {
        // Error handling here
    }

    // Event Processing Technique. Event is received when
    // the transfer is processed.

    void APP_DMASYSTransferEventHandler(SYS_DMA_TRANSFER_EVENT event,
            SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle)
    {
        switch(event)
        {
            case SYS_DMA_TRANSFER_EVENT_COMPLETE:

                // This means the data was transferred.
                break;

            case SYS_DMA_TRANSFER_EVENT_ERROR:

                // Error handling here.
                break;

            default:
                break;
        }
    }
    </code>

  Remarks:
    None.
 */
void SYS_DMA_ChannelTransferEventHandlerSet(SYS_DMA_CHANNEL_HANDLE handle,
        const SYS_DMA_CHANNEL_TRANSFER_EVENT_HANDLER eventHandler,
        const uintptr_t contextHandle)
{
    SYS_DMA_CHANNEL_OBJECT *chanObj;

    chanObj = (SYS_DMA_CHANNEL_OBJECT *)handle;
    if (chanObj == (SYS_DMA_CHANNEL_OBJECT *) NULL ||
        eventHandler == (SYS_DMA_CHANNEL_TRANSFER_EVENT_HANDLER) NULL ||
        contextHandle == (uintptr_t) NULL)
    {
        SYS_ASSERT(false, "Invalid handle");
        return;
    } else
    {
        /* Set the Event Handler and context */
        chanObj->pEventCallBack = eventHandler;
        chanObj->hClientArg = contextHandle;
    }
    return;
}


/*******************************************************************************
  Function:
    void SYS_DMA_Tasks(SYS_MODULE_OBJ object );

  Summary:
    Maintains the system service's state machine.

  Description:
    This routine is used to maintain the DMA system service's internal state machine.
    This function is specifically designed for non DMA interrupt trigger
    implementations(polling mode), and should be used only in polling mode.
    this function should be called from the SYS_Tasks() function.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.

  Parameters:
    object      - Object handle for the DMA module (returned from
                  SYS_DMA_Initialize)

  Returns:
    None.

  Example:
    <code>
    // 'object' Returned from SYS_DMA_Initialize

    while (true)
    {
        SYS_DMA_Tasks ((object) );

        // Do other tasks
    }
    </code>

  Remarks:
    This routine is normally not called directly by an application.  It is
    called by the system's Tasks routine (SYS_Tasks).

 */

void SYS_DMA_Tasks(SYS_MODULE_OBJ object)
{
    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL channels;
    DMA_CHANNEL_INT_SOURCE chanIntSrc;

    /* Go through each DMA channel and service it */
    for (channels = DMA_CHANNEL_0; channels < DMA_NUMBER_OF_CHANNELS; channels++)
    {
        chanIntSrc = PLIB_DMA_ChannelXTriggerSourceNumberGet(DMA_ID_0, channels);

        /* Check whether the DMA channel interrupt has occured */
        if(true == SYS_INT_SourceStatusGet(chanIntSrc))
        {
            if(true == PLIB_DMA_ChannelXINTSourceFlagGet(DMA_ID_0,channels,
                        DMA_INT_BLOCK_TRANSFER_COMPLETE))
            {
                /* Channel is by default disabled on completion of a block transfer */

                /* Clear the Block transfer complete flag */
                PLIB_DMA_ChannelXINTSourceFlagClear(DMA_ID_0,channels,
                        DMA_INT_BLOCK_TRANSFER_COMPLETE);

                SYS_INT_SourceStatusClear(chanIntSrc);

                /* Findout the channel object and give a callback */
                chanObj = (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[channels];
                chanObj->errorInfo = SYS_DMA_ERROR_NONE;
                chanObj->pEventCallBack(SYS_DMA_TRANSFER_EVENT_COMPLETE,
                                        (SYS_DMA_CHANNEL_HANDLE)chanObj,chanObj->hClientArg);
            }
            else if(true == PLIB_DMA_ChannelXINTSourceFlagGet(DMA_ID_0,channels,
                        DMA_INT_TRANSFER_ABORT))
            {

                /* Channel is by default disabled on Transfer Abortion */

                /* Clear the Abort transfer complete flag */
                PLIB_DMA_ChannelXINTSourceFlagClear(DMA_ID_0,channels,
                        DMA_INT_TRANSFER_ABORT);

                SYS_INT_SourceStatusClear(chanIntSrc);

                /* Findout the channel object and give a callback */
                chanObj = (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[channels];
                chanObj->errorInfo = SYS_DMA_ERROR_NONE;
                chanObj->pEventCallBack(SYS_DMA_TRANSFER_EVENT_ABORT,
                                        (SYS_DMA_CHANNEL_HANDLE)chanObj,chanObj->hClientArg);

            }
            else
            {
                ;
            }
        }
    }
    return;
}


/*******************************************************************************
  Function:
    void SYS_DMA_TasksISR(SYS_MODULE_OBJ object, DMA_CHANNEL activeChannel);

  Summary:
    Maintains the system service's state machine and implements its ISR

  Description:
    This routine is used to maintain the DMA system service's internal state machine
    and implement its ISR for DMA interrupt trigger implementations(interrupt mode).
    This function is specifically designed for DMA interrupt trigger
    implementations (interrupt mode).
    In interrupt mode, this function should be called in the interrupt
    service routine of the DMA channel that is associated with this transfer.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.

  Parameters:
    object      - Object handle for the DMA module (returned from
                  SYS_DMA_Initialize)
    activeChannel - DMA channel number of the ISR being serviced.

  Returns:
    None.

  Example:
    <code>
    // 'object' Returned from SYS_DMA_Initialize

	void __ISR(_DMA3_VECTOR,ipl5) _InterruptHandler_BT_USART_RX_DMA_CHANNEL(void)
	{
		SYS_DMA_TasksISR(object, DMA_CHANNEL_3);
	}


    </code>

  Remarks:
    It is called by appropriate raw ISR.

    This routine may execute in an ISR context and will never block or access any
    resources that may cause it to block.
 */
void SYS_DMA_TasksISR(SYS_MODULE_OBJ object, DMA_CHANNEL activeChannel)
{
    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL_INT_SOURCE chanIntSrc;

    /* Find out the interrupt source number for the active DMA channel */
     chanIntSrc = PLIB_DMA_ChannelXTriggerSourceNumberGet(DMA_ID_0, activeChannel);

    /* Check whether the active DMA channel interrupt has occured */
    if(true == SYS_INT_SourceStatusGet(chanIntSrc))
    {
        if(true == PLIB_DMA_ChannelXINTSourceFlagGet(DMA_ID_0,activeChannel,
                        DMA_INT_BLOCK_TRANSFER_COMPLETE))
        {
            /* Channel is by default disabled on completion of a block transfer */

            /* Clear the Block transfer complete flag */
            PLIB_DMA_ChannelXINTSourceFlagClear(DMA_ID_0,activeChannel,
                     DMA_INT_BLOCK_TRANSFER_COMPLETE);

            SYS_INT_SourceStatusClear(chanIntSrc);

            /* Findout the channel object and give a callback */
            chanObj = (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[activeChannel];
            chanObj->errorInfo = SYS_DMA_ERROR_NONE;
            chanObj->pEventCallBack(SYS_DMA_TRANSFER_EVENT_COMPLETE,
                                        (SYS_DMA_CHANNEL_HANDLE)chanObj,chanObj->hClientArg);

        }
        else if(true == PLIB_DMA_ChannelXINTSourceFlagGet(DMA_ID_0,activeChannel,
                        DMA_INT_TRANSFER_ABORT))
        {
            /* Channel is by default disabled on Transfer Abortion */

            /* Clear the Abort transfer complete flag */
            PLIB_DMA_ChannelXINTSourceFlagClear(DMA_ID_0,activeChannel,
                        DMA_INT_TRANSFER_ABORT);

            SYS_INT_SourceStatusClear(chanIntSrc);

            /* Findout the channel object and give a callback */
            chanObj = (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[activeChannel];
            chanObj->errorInfo = SYS_DMA_ERROR_NONE;
            chanObj->pEventCallBack(SYS_DMA_TRANSFER_EVENT_ABORT,
                        (SYS_DMA_CHANNEL_HANDLE)chanObj,chanObj->hClientArg);
        }
        else
        {
            ;
        }
    }
    return;
}

/*****************************************************************************
  Function:
    void SYS_DMA_TasksError (SYS_MODULE_OBJ object );

  Summary:
    Maintains the system service's error state machine.

  Description:
    This routine is used to maintain the DMA system service's error state machine.
	This function is specifically designed for non DMA interrupt trigger
    implementations(polling mode), and should be used only in polling mode.
    this function should be called from the SYS_Tasks() function.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.

Parameters:
    object      - Object handle for the DMA module (returned from
                  SYS_DMA_Initialize)

  Returns:
    None.

  Example:
    <code>
    // 'object' Returned from SYS_DMA_Initialize

    while (true)
    {
        SYS_DMA_TasksError (object);
        // Do other tasks
    }
    </code>

  Remarks:
    This routine is normally not called directly by an application.  It is
    called by the system's Tasks routine (SYS_Tasks).

 */

void SYS_DMA_TasksError(SYS_MODULE_OBJ object)
{
    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL channels;
    DMA_CHANNEL_INT_SOURCE chanIntSrc;

    /* Go through each DMA channel and service it */
    for (channels = DMA_CHANNEL_0; channels < DMA_NUMBER_OF_CHANNELS; channels++)
    {
		/* Find out the interrupt source number for the last active DMA channel */
		chanIntSrc = PLIB_DMA_ChannelXTriggerSourceNumberGet(DMA_ID_0, channels);

		/* Check whether the active DMA channel interrupt has occured */
		if(true == SYS_INT_SourceStatusGet(chanIntSrc))
		{
			if(true == PLIB_DMA_ChannelXINTSourceFlagGet(DMA_ID_0,channels,
						DMA_INT_ADDRESS_ERROR))
			{
				/* Channel is by default disabled on completion of a block transfer */

				/* Clear the Block transfer complete flag */
				PLIB_DMA_ChannelXINTSourceFlagClear(DMA_ID_0,channels,
						DMA_INT_ADDRESS_ERROR);

				SYS_INT_SourceStatusClear(chanIntSrc);

				/* Findout the channel object and give a callback */
				chanObj = (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[channels];
				chanObj->errorInfo = SYS_DMA_ERROR_ADDRESS_ERROR;
				chanObj->pEventCallBack(SYS_DMA_TRANSFER_EVENT_ERROR,
										(SYS_DMA_CHANNEL_HANDLE)chanObj,chanObj->hClientArg);
			}
		}
	}
    return;
}


/*******************************************************************************
  Function:
    void SYS_DMA_TasksErrorISR(SYS_MODULE_OBJ object, DMA_CHANNEL activeChannel);

  Summary:
    Maintains the system service's state machine and implements its ISR

  Description:
    This routine is used to maintain the DMA system service's internal error state machine
    and implement its ISR for DMA interrupt trigger implementations(interrupt mode).
    This function is specifically designed for DMA interrupt trigger
    implementations (interrupt mode).
    In interrupt mode, this function should be called in the interrupt
    service routine of the DMA channel that is associated with this transfer.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.

  Parameters:
    object      - Object handle for the DMA module (returned from
                  SYS_DMA_Initialize)
    activeChannel - DMA channel number of the ISR being serviced.

  Returns:
    None.

  Example:
    <code>
    // 'object' Returned from SYS_DMA_Initialize

	void __ISR(_DMA3_VECTOR,ipl5) _InterruptHandler_BT_USART_RX_DMA_CHANNEL(void)
	{
		// ....
		SYS_DMA_TasksErrorISR(object, DMA_CHANNEL_3);
	}


    </code>

  Remarks:
    It is called by appropriate raw ISR.

    This routine may execute in an ISR context and will never block or access any
    resources that may cause it to block.
 */
void SYS_DMA_TasksErrorISR(SYS_MODULE_OBJ object, DMA_CHANNEL activeChannel)
{
    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL_INT_SOURCE chanIntSrc;

    /* Find out the interrupt source number for the last active DMA channel */
    chanIntSrc = PLIB_DMA_ChannelXTriggerSourceNumberGet(DMA_ID_0, activeChannel);

    /* Check whether the active DMA channel interrupt has occured */
    if(true == SYS_INT_SourceStatusGet(chanIntSrc))
    {
        if(true == PLIB_DMA_ChannelXINTSourceFlagGet(DMA_ID_0,activeChannel,
                    DMA_INT_ADDRESS_ERROR))
        {
            /* Channel is by default disabled on completion of a block transfer */

            /* Clear the Block transfer complete flag */
            PLIB_DMA_ChannelXINTSourceFlagClear(DMA_ID_0,activeChannel,
                    DMA_INT_ADDRESS_ERROR);

            SYS_INT_SourceStatusClear(chanIntSrc);

            /* Findout the channel object and give a callback */
            chanObj = (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[activeChannel];
            chanObj->errorInfo = SYS_DMA_ERROR_ADDRESS_ERROR;
            chanObj->pEventCallBack(SYS_DMA_TRANSFER_EVENT_ERROR,
                                    (SYS_DMA_CHANNEL_HANDLE)chanObj,chanObj->hClientArg);
        }
    }
    return;
}

// *****************************************************************************
/* Function:
    SYS_DMA_ERROR SYS_DMA_ChannelErrorGet(SYS_DMA_CHANNEL_HANDLE handle)

  Summary:
    This function returns the error(if any) associated with the last client
    request.

  Description:
    This function returns the error(if any) associated with the last client
    request. If the service send a SYS_DMA_TRANSFER_EVENT_ERROR to the client,
    the client can call this function to know the error cause.
    The error status will be updated on every operation and should be read
    frequently (ideally immediately after the service operation has completed)
    to know the relevant error status.

  Precondition:
    The SYS_DMA_Initialize routine must have been called for the DMA sub system.
    SYS_DMA_ChannelAllocate must have been called to obtain a valid opened channel
    handle.

  Parameters:
     handle          - Handle of the DMA channel as returned by the
                       SYS_DMA_ChannelAllocate function.

  Returns:
    A SYS_DMA_ERROR type inidicating last known error status.

  Example:
    <code>

    // 'handle' is a valid handle returned
    // by the SYS_DMA_ChannelAllocate function.

    // pDmaSrc, pDmaDst is the  souce,destination address
    // txferSrcSize, txferDesSize is the souce,destination transfer size
    // cellSize is the cell size
    MY_APP_OBJ          myAppObj;

    // Client registers an event handler with service. This is done once

    SYS_DMA_ChannelTransferEventHandlerSet( handle, APP_DMATransferEventHandle,
                                     (uintptr_t)&myAppObj );

    SYS_DMA_ChannelSetup(handle, DMA_CHANNEL_PRIORITY_1,
                                 SYS_DMA_CHANNEL_OP_MODE_BASIC,
                                 DMA_TRIGGER_SOURCE_NONE);

    SYS_DMA_ChannelTransferAdd(handle,pDmaSrc,txferSrcSize,
                                pDmaDst,txferDesSize,cellSize);

    SYS_DMA_ChannelForceStart(handle);

    if(SYS_DMA_CHANNEL_HANDLE_INVALID == handle)
    {
        // Error handling here
    }

    // Event Processing Technique. Event is received when
    // the transfer is processed.

    void APP_DMATransferEventHandle( SYS_DMA_TRANSFER_EVENT event,
            SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle )
    {

        switch(event)
        {
            case SYS_DMA_TRANSFER_EVENT_COMPLETE:

                // This means the data was transferred.
                break;

            case SYS_DMA_TRANSFER_EVENT_ERROR:

                // Error handling here.

                if(SYS_DMA_ERROR_ADDRESS_ERROR == SYS_DMA_ChannelErrorGet(handle))
                {
                    // There was an address error.
                    // Do error handling here.
                }

                break;

            default:
                break;
        }
    }
    </code>

  Remarks:
    It is the client's reponsibility to make sure that the error status is
    obtained frequently. The service will update the error status
    irrespective of whether this has been examined by the client.
*/

SYS_DMA_ERROR SYS_DMA_ChannelErrorGet(SYS_DMA_CHANNEL_HANDLE handle)
{
    SYS_DMA_CHANNEL_OBJECT *chanObj;

    chanObj = (SYS_DMA_CHANNEL_OBJECT *) handle;
    return chanObj->errorInfo;
}


//******************************************************************************
/* Function:
    void SYS_DMA_ChannelForceStart ( SYS_DMA_CHANNEL_HANDLE handle )

  Summary:
    Force start of transfer on the selected channel

  Description:
    The function force start a DMA transfer to occur for the selected channel.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.
    DMA channel should have been allocated by calling SYS_DMA_ChannelAllocate.
    The function SYS_DMA_ChannelSetup must have been called to setup and
    enable the required features.
    The function 'SYS_DMA_ChannelTransferAdd' to add a transfer.

  Parameters:
     handle          - Handle of the DMA channel as returned by the
                       SYS_DMA_ChannelAllocate function.
  Returns:
    None.

  Example:
  <code>

    // 'handle' is a valid handle returned
    // by the SYS_DMA_ChannelAllocate function.

    // pDmaSrc, pDmaDst is the  souce,destination address
    // txferSrcSize, txferDesSize is the souce,destination transfer size
    // cellSize is the cell size
    MY_APP_OBJ          myAppObj;

    // Client registers an event handler with service. This is done once

    SYS_DMA_ChannelTransferEventHandlerSet( handle, APP_DMATransferEventHandle,
                                     (uintptr_t)&myAppObj );

    SYS_DMA_ChannelSetup(handle, DMA_CHANNEL_PRIORITY_1,
                                 SYS_DMA_CHANNEL_OP_MODE_BASIC,
                                 DMA_TRIGGER_SOURCE_NONE);

    SYS_DMA_ChannelTransferAdd(handle,pDmaSrc,txferSrcSize,
                                pDmaDst,txferDesSize,cellSize);

    SYS_DMA_ChannelForceStart(handle);

    if(SYS_DMA_CHANNEL_HANDLE_INVALID == handle)
    {
        // Error handling here
    }

    // Event Processing Technique. Event is received when
    // the transfer is processed.

    void APP_DMATransferEventHandle( SYS_DMA_TRANSFER_EVENT event,
            SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle )
    {

        switch(event)
        {
            case SYS_DMA_TRANSFER_EVENT_COMPLETE:

                // This means the data was transferred.
                break;

            case SYS_DMA_TRANSFER_EVENT_ERROR:

                // Error handling here.

                if(SYS_DMA_ERROR_ADDRESS_ERROR == DRV_I2S_ErrorGet(myI2SHandle))
                {
                    // There was an address error.
                    // Do error handling here.
                }

                break;

            default:
                break;
        }
    }
  </code>

  Remarks:
   This function must be used to start the DMA transfer when the channel has been
   setup(by calling SYS_DMA_ChannelSetup) with the eventSrc as DMA_TRIGGER_SOURCE_NONE.
*/

void SYS_DMA_ChannelForceStart ( SYS_DMA_CHANNEL_HANDLE handle)
{
    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL channelNumber;

    chanObj = (SYS_DMA_CHANNEL_OBJECT *) handle;
    channelNumber = (chanObj  - (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[0]);
    if(true == PLIB_DMA_ExistsStartTransfer(DMA_ID_0))
    {
        PLIB_DMA_StartTransferSet(DMA_ID_0, channelNumber);
    }
}

//******************************************************************************
/* Function:
    void SYS_DMA_ChannelForceAbort ( SYS_DMA_CHANNEL_HANDLE handle )

  Summary:
    Force abort of transfer on the selected channel

  Description:
    The function abort a DMA transfer to occur for the selected channel.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.
    DMA channel should have been allocated by calling SYS_DMA_ChannelAllocate.
    The function SYS_DMA_ChannelSetup must have been called to setup and
    enable the required features.
    The function 'SYS_DMA_ChannelTransferAdd' to add a transfer.

  Parameters:
     handle          - Handle of the DMA channel as returned by the
                       SYS_DMA_ChannelAllocate function.

  Returns:
    None.

  Example:
  <code>

    // 'handle' is a valid handle returned
    // by the SYS_DMA_ChannelAllocate function.

    // pDmaSrc, pDmaDst is the  souce,destination address
    // txferSrcSize, txferDesSize is the souce,destination transfer size
    // cellSize is the cell size
    MY_APP_OBJ          myAppObj;

    // Client registers an event handler with service. This is done once

    SYS_DMA_ChannelTransferEventHandlerSet( handle, APP_DMATransferEventHandle,
                                     (uintptr_t)&myAppObj );

    SYS_DMA_ChannelSetup(handle, DMA_CHANNEL_PRIORITY_1,
                                 SYS_DMA_CHANNEL_OP_MODE_BASIC,
                                 DMA_TRIGGER_SOURCE_NONE);

    SYS_DMA_ChannelTransferAdd(handle,pDmaSrc,txferSrcSize,
                                pDmaDst,txferDesSize,cellSize);

    SYS_DMA_ChannelForceStart(handle);

    if(SYS_DMA_CHANNEL_HANDLE_INVALID == handle)
    {
        // Error handling here
    }

    ....
    ....
    // Client may need to abort a transfer
    SYS_DMA_ChannelForceAbort(handle);

    // Event Processing Technique. Event is received when
    // the transfer is processed.

    void APP_DMATransferEventHandle( SYS_DMA_TRANSFER_EVENT event,
            SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle )
    {

        switch(event)
        {
            case SYS_DMA_TRANSFER_EVENT_ABORT:

                // This means the data was transferred.
                break;

            case SYS_DMA_TRANSFER_EVENT_ERROR:

                // Error handling here.

                if(SYS_DMA_ERROR_ADDRESS_ERROR == DRV_I2S_ErrorGet(myI2SHandle))
                {
                    // There was an address error.
                    // Do error handling here.
                }

                break;

            default:
                break;
        }
    }
  </code>

  Remarks:
   This function must be used to abort the DMA transfer when the channel has been
   setup(by calling SYS_DMA_ChannelSetup) with the eventSrc as DMA_TRIGGER_SOURCE_NONE.
   and SYS_DMA_ChannelAbortEventSet has not been called.
*/

void SYS_DMA_ChannelForceAbort ( SYS_DMA_CHANNEL_HANDLE handle )
{
    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL channelNumber;

    chanObj = (SYS_DMA_CHANNEL_OBJECT *) handle;
    channelNumber = (chanObj  - (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[0]);
    if(true == PLIB_DMA_ExistsAbortTransfer(DMA_ID_0))
    {
        PLIB_DMA_AbortTransferSet(DMA_ID_0, channelNumber);
    }
}


//******************************************************************************
/* Function:
    void SYS_DMA_ChannelAbortEventSet ( SYS_DMA_CHANNEL_HANDLE handle,
                                        DMA_TRIGGER_SOURCE eventSrc )

  Summary:
    Sets an event source and enables cell transfer abort event for the same
    for the selected channel.

  Description:
    This functions enables a cell transfer abort event for the selected source
    event.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.
    DMA channel should have been allocated by calling SYS_DMA_ChannelAllocate.
    The function SYS_DMA_ChannelSetup must have been called to setup and
    enable the required features.
    The function 'SYS_DMA_ChannelTransferAdd' to add a transfer.

  Parameters:
     handle          - Handle of the DMA channel as returned by the
                       SYS_DMA_ChannelAllocate function.
     eventSrc         - The event causing the cell transfer abort

  Returns:
    None.

  Example:
  <code>

    // 'handle' is a valid handle returned
    // by the SYS_DMA_ChannelAllocate function.

    // pDmaSrc, pDmaDst is the  souce,destination address
    // txferSrcSize, txferDesSize is the souce,destination transfer size
    // cellSize is the cell size
    MY_APP_OBJ          myAppObj;

    // Client registers an event handler with service. This is done once

    SYS_DMA_ChannelTransferEventHandlerSet( handle, APP_DMATransferEventHandle,
                                     (uintptr_t)&myAppObj );

    SYS_DMA_ChannelSetup(handle, DMA_CHANNEL_PRIORITY_1,
                                 SYS_DMA_CHANNEL_OP_MODE_BASIC,
                                 DMA_TRIGGER_SOURCE_NONE);

    SYS_DMA_ChannelTransferAdd(handle,pDmaSrc,txferSrcSize,
                                pDmaDst,txferDesSize,cellSize);

    SYS_DMA_ChannelAbortEventSet(handle, DMA_TRIGGER_CTMU);

    SYS_DMA_ChannelForceStart(handle);

    if(SYS_DMA_CHANNEL_HANDLE_INVALID == handle)
    {
        // Error handling here
    }

    // Event Processing Technique. Event is received when
    // the transfer is processed.

    void APP_DMATransferEventHandle( SYS_DMA_TRANSFER_EVENT event,
            SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle )
    {

        switch(event)
        {
            case SYS_DMA_TRANSFER_EVENT_ABORT:

                // This means the data was transferred.
                break;

            case SYS_DMA_TRANSFER_EVENT_ERROR:

                // Error handling here.

                if(SYS_DMA_ERROR_ADDRESS_ERROR == DRV_I2S_ErrorGet(myI2SHandle))
                {
                    // There was an address error.
                    // Do error handling here.
                }

                break;

            default:
                break;
        }
    }
  </code>

  Remarks:
    If the parameter 'eventSrc' is specified as DMA_TRIGGER_SOURCE_NONE the
    current DMA transfer will be aborted. The behaviour is a same as calling
    SYS_DMA_ChannelForceAbort.

*/

void SYS_DMA_ChannelAbortEventSet ( SYS_DMA_CHANNEL_HANDLE handle,
                                    DMA_TRIGGER_SOURCE eventSrc )
{
    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL channelNumber;

    chanObj = (SYS_DMA_CHANNEL_OBJECT *) handle;
    channelNumber = (chanObj  - (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[0]);
    /* Setup the DMA Trigger Aboort Source and Enable it */

    if(DMA_TRIGGER_SOURCE_NONE == eventSrc)
    {
        if(true == PLIB_DMA_ExistsAbortTransfer(DMA_ID_0))
        {
            PLIB_DMA_AbortTransferSet(DMA_ID_0, channelNumber);
        }
        return;
    }

    if(true == PLIB_DMA_ExistsChannelXAbortIRQ(DMA_ID_0))
    {
        PLIB_DMA_ChannelXAbortIRQSet( DMA_ID_0,
                                      channelNumber,
                                      eventSrc);

        PLIB_DMA_ChannelXTriggerEnable( DMA_ID_0,
                                        channelNumber,
                                        DMA_CHANNEL_TRIGGER_TRANSFER_ABORT);
    }
}


//******************************************************************************
/* Function:
    bool SYS_DMA_ChannelIsBusy(SYS_DMA_CHANNEL_HANDLE handle)

  Summary:
    Returns the busy status of the specified DMA channel

  Description:
    This function returns the busy status of the selected DMA channel

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.
    DMA channel should have been allocated by calling SYS_DMA_ChannelAllocate.

  Parameters:
    handle          - Handle of the DMA channel as returned by the
                       SYS_DMA_ChannelAllocate function.

  Returns:
    bool         - true,  if the selected DMA channel is active or enabled
                 - false, if the selected DMA channel is inactive or disabled
  Example:

  <code>
    bool                        busyStat;

    busyStat    = SYS_DMA_ChannelGetBusy(handle);
  </code>

  Remarks:
    Not all features are available on all micro-controllers.
*/

bool SYS_DMA_ChannelIsBusy(SYS_DMA_CHANNEL_HANDLE handle)
{
    bool isBusy;
    SYS_DMA_CHANNEL_OBJECT *chanObj;
    DMA_CHANNEL channelNumber;

    chanObj = (SYS_DMA_CHANNEL_OBJECT *) handle;
    channelNumber = (chanObj  - (SYS_DMA_CHANNEL_OBJECT *) &gSysDMAChannelObj[0]);
    isBusy = false;

    if(true == PLIB_DMA_ExistsChannelXBusy(DMA_ID_0))
    {
        if(true == PLIB_DMA_ChannelXBusyIsBusy( DMA_ID_0,
                                     channelNumber))
        {
            isBusy = true;
        }
    }
    return isBusy;
}


// *****************************************************************************
// *****************************************************************************
// Section: Global DMA Management Routines
// *****************************************************************************
// *****************************************************************************
//******************************************************************************
//******************************************************************************
/* Function:
    void SYS_DMA_Suspend(void)

  Summary:
    Suspend DMA transfers.

  Description:
    This function suspends DMA transfers to allow CPU uninterrupted access
    to data bus

  Precondition:
     DMA should have been initialized by calling SYS_DMA_Initialize.

  Parameters:
   None.

  Returns:
    None.

  Example:
  <code>
    SYS_DMA_Suspend();
  </code>

  Remarks:
    None
*/

void SYS_DMA_Suspend(void)
{
    if(true == PLIB_DMA_ExistsSuspend(DMA_ID_0))
    {
        PLIB_DMA_SuspendEnable(DMA_ID_0);
    }
}

//******************************************************************************
/* Function:
    void SYS_DMA_Resume(void)

  Summary:
    Resume DMA operations

  Description:
    This function disables DMA suspend. It resumes the DMA operation suspended
    by calling SYS_DMA_Suspend. The DMA operates normally.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.

  Parameters:
   None.

  Returns:
    None.

  Example:
  <code>
    SYS_DMA_Resume();
  </code>

  Remarks:
    None
*/
void SYS_DMA_Resume(void)
{
    if(true == PLIB_DMA_ExistsSuspend(DMA_ID_0))
    {
        PLIB_DMA_SuspendDisable(DMA_ID_0);
    }
}


//******************************************************************************
/* Function:
    bool SYS_DMA_IsBusy(void)

  Summary:
    Returns the busy status of the DMA module

  Description:
    This function returns the busy status of the DMA module

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.

  Parameters:
   None.

  Returns:
    bool         - true, if the DMA module is active
                 - false, if the DMA module is inactive and disabled

  <code>
    bool busyStat;
    busyStat = SYS_DMA_IsBusy();
  </code>

  Remarks:
    Not all features are available on all micro-controllers.
*/
bool SYS_DMA_IsBusy(void)
{
    if(true == PLIB_DMA_ExistsBusy(DMA_ID_0))
    {
        return PLIB_DMA_IsBusy(DMA_ID_0);
    }

    return false;  // added this line to avoid compiler warning
}


//******************************************************************************
/* Function:
    void SYS_DMA_StatusGet(SYS_DMA_STATUS *dmaStat)

  Summary:
    Gets the current DMA operation status

  Description:
    This function returns the information for the current DMA controller status.
    It updates the last DMA: operation, channel used and address.

  Precondition:
    DMA should have been initialized by calling SYS_DMA_Initialize.

  Parameters:
   None.

  Returns:
    dmaStat         - Last active DMA channel
                    - last DMA operation: Read if 1, Write if 0
                    - lastAddress -> Most recent DMA access address
  Example:
  <code>
    SYS_DMA_STATUS dmaStat;
    SYS_DMA_StatusGet(&dmaStat);
  </code>

  Remarks:
    Not all features are available on all micro-controllers.
*/
void SYS_DMA_StatusGet(SYS_DMA_STATUS *dmaStat)
{
    if(true==PLIB_DMA_ExistsChannelBits(DMA_ID_0))
    {
        dmaStat->lastAccess.channelNo = (0x7 & PLIB_DMA_ChannelBitsGet(DMA_ID_0));
    }

    if(true==PLIB_DMA_ExistsLastBusAccess(DMA_ID_0))
    {
        if(true == PLIB_DMA_LastBusAccessIsRead(DMA_ID_0))
        {
            dmaStat->lastAccess.operation = 1;
        }
        else
        {
            dmaStat->lastAccess.operation = 0;
        }
    }

    if(true == PLIB_DMA_ExistsRecentAddress(DMA_ID_0))
    {
        dmaStat->lastAddress = (void*) PLIB_DMA_RecentAddressAccessed(DMA_ID_0);
    }
}














/*******************************************************************************
 End of File
*/



