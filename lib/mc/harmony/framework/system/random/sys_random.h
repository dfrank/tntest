/*******************************************************************************
  Random Number Generator System Service Interface Definition

  Company:
    Microchip Technology Inc.

  File Name:
    sys_random.h

  Summary:
    Random Number Generator System Service interface definition.

  Description:
    This file contains the interface definition for the Random Number Generator
    System Service.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

#ifndef _SYS_RANDOM_H
#define _SYS_RANDOM_H



// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stdint.h>
#include "system/system.h"


// *****************************************************************************
// *****************************************************************************
// Section: SYS RANDOM Data Types
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Random Number Generator Service Initialization Data

  Summary:

  Description:

  Remarks:
*/

typedef struct
{
    uint32_t    seedPseudo;
    void        *seedCrypto;
    size_t      seedCryptoSize;


} SYS_RANDOM_INIT;


// *****************************************************************************
// *****************************************************************************
// Section: SYS RANDOM Module Routines
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Function:
    SYS_MODULE_OBJ SYS_RANDOM_Initialize( const SYS_RANDOM_INIT* const randomInit );

  Summary:
    Initializes the Random Number Generator system service.

  Description:

  PreCondition:
    None.

  Parameters:
    index - Module instance index.
    
    init  - initialization data for the random system service (cast of a pointer 
            to a SYS_RANDOM_INIT structure to a SYS_MODULE_INDEX structure 
            pointer).

  Returns:
    If successful, returns a valid handle to an object.  Otherwise, it
    returns SYS_MODULE_OBJ_INVALID.
    
  Example:

  Remarks:
    None.
*/

SYS_MODULE_OBJ SYS_RANDOM_Initialize( const SYS_MODULE_INDEX index, const SYS_MODULE_INIT * const init );


// *****************************************************************************
/* Function:
    void SYS_RANDOM_Deinitialize( SYS_MODULE_OBJ object );

  Summary:
    Deinitializes the Random Number Generator system service.

  Description:

  PreCondition:
    None.

  Parameters:
    object          - SYS RANDOM object handle, returned from SYS_RANDOM_Initialize

  Returns:
    None.
    
  Example:

  Remarks:
    None.
*/

void SYS_RANDOM_Deinitialize( SYS_MODULE_OBJ object );


// *****************************************************************************
// *****************************************************************************
// Section: SYS RANDOM Pseudo Random Number Generator Routines
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Function:
    void SYS_RANDOM_PseudoSeedSet( uint32_t seed );

  Summary:
    Reseeds the Pseudo-random Number Generator.

  Description:

  PreCondition:
    None.

  Parameters:
    None.

  Returns:
    None.
    
  Example:

  Remarks:
    None.
*/

void SYS_RANDOM_PseudoSeedSet( uint32_t seed );


// *****************************************************************************
/* Function:
    uint32_t SYS_RANDOM_PseudoGet( void );

  Summary:
    Returns a random value from the Pseudo-random Number Generator.

  Description:

  PreCondition:
    None.

  Parameters:
    None.

  Returns:
    None.
    
  Example:

  Remarks:
    None.
*/

uint32_t SYS_RANDOM_PseudoGet( void );


// *****************************************************************************
// *****************************************************************************
// Section: SYS RANDOM Cryptographic Random Number Generator Routines
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Function:
    void SYS_RANDOM_CryptoSeedSet( void *seed, size_t size);

  Summary:
    Reseeds the cryptographic quality Random Number Generator. 

  Description:

  PreCondition:
    None.

  Parameters:
    seed
    size    Must be less or equal to SYS_RANDOM_CRYPTO_SEED_SIZE

  Returns:
    None.
    
  Example:

  Remarks:
    None.
*/

void SYS_RANDOM_CryptoSeedSet( void *seed, size_t size);


// *****************************************************************************
/* Function:
    size_t SYS_RANDOM_CryptoSeedSizeGet( void );

  Summary:
    Reseeds the cryptographic quality Random Number Generator. 

  Description:

  PreCondition:
    None.

  Parameters:
    seed
    
    size    Must be less or equal to SYS_RANDOM_CRYPTO_SEED_SIZE

  Returns:
    None.
    
  Example:

  Remarks:
    Returns the size (in bytes) of the most recently set seed value for the 
    cryptographic strength random number generator.
*/

size_t SYS_RANDOM_CryptoSeedSizeGet( void );


// *****************************************************************************
/* Function:
    uint32_t SYS_RANDOM_CryptoGet( void );

  Summary:
    Returns a random 32 bit value from the cryptographic quality Random Number 
	Generator. 

  Description:

  PreCondition:
    None.

  Parameters:
    None.

  Returns:
    None.
    
  Example:

  Remarks:
    None.
*/

uint32_t SYS_RANDOM_CryptoGet( void );


// *****************************************************************************
/* Function:
    void SYS_RANDOM_CryptoBlockGet( uint8_t buffer, size_t bufferSize );

  Summary:
    Generates a sequence of random bytes using the cryptographic quality Random
	Number Generator. 

  Description:

  PreCondition:
    None.

  Parameters:
    None.

  Returns:
    None.
    
  Example:

  Remarks:
    None.
*/

void SYS_RANDOM_CryptoBlockGet( void *buffer, size_t size );


// *****************************************************************************
/* Function:
    uint8_t SYS_RANDOM_CryptoByteGet( void );

  Summary:
    Returns a random byte from the cryptographic quality Random Number Generator. 

  Description:

  PreCondition:
    None.

  Parameters:
    None.

  Returns:
    None.
    
  Example:

  Remarks:
    None.
*/

uint8_t SYS_RANDOM_CryptoByteGet( void );


// *****************************************************************************
/* Function:
    void SYS_RANDOM_CryptoEntropyAdd( uint8_t data );

  Summary:
    Adds randomness to the cryptographic quality Random Number Generator. 

  Description:

  PreCondition:
    None.

  Parameters:
    None.

  Returns:
    None.
    
  Example:

  Remarks:
    None.
*/

void SYS_RANDOM_CryptoEntropyAdd( uint8_t data );


#endif //_SYS_RANDOM_H

/*******************************************************************************
 End of File
*/

