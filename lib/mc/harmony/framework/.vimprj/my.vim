
" определяем путь к папке .vim
let s:sPath = expand('<sfile>:p:h')

let &tabstop = 4
let &shiftwidth = 4

let g:indexer_handlePath = 0
let g:indexer_indexerListFilename = s:sPath.'/.indexer_files'

" we don't want all the tags to be rebuilt every time we open file from linux
" kernel. But, if the file is saved and we want tags to update, we should
" manually call :IndexerRebuild command.
let g:indexer_dontUpdateTagsIfFileExists = 1 

call envcontrol#set_previous()

