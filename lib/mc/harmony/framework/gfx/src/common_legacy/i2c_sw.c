/***********************************************************************************/
/*  Copyright (c) 2002-2009, Silicon Image, Inc.  All rights reserved.             */
/*  No part of this work may be reproduced, modified, distributed, transmitted,    */
/*  transcribed, or translated into any language or computer format, in any form   */
/*  or by any means without written permission of: Silicon Image, Inc.,            */
/*  1060 East Arques Avenue, Sunnyvale, California 94085                           */
/***********************************************************************************/
#include "app.h"
#include "string.h"
#include "peripheral/i2c/plib_i2c.h"
#include <sys/attribs.h>
#include <sys/kmem.h>

uint8_t I2C_ReadBlock(uint8_t instance, uint8_t deviceID, uint8_t offset, uint8_t *buffer, uint16_t length);
uint8_t I2C_WriteBlock(uint8_t instance, uint8_t deviceID, uint8_t offset, uint8_t *buffer, uint16_t length);
bool I2CSendByte (uint8_t instance, uint8_t data );
bool I2CByteWasAcknowledged (void);

void I2C_Init(uint8_t instance)
{

    PLIB_I2C_Disable(instance);

    #if defined(__PIC32MZ__)
    I2C1BRG = 50; //200Mhz
    #else
    PLIB_I2C_BaudRateSet ( instance, (SYS_DEVCON_SYSTEM_CLOCK >> 1), 300000 ); //300kHz
    #endif

    PLIB_I2C_Enable(instance);

}

/*******************************************************************************
  Function:
    BOOL StartTransfer( BOOL restart )

  Summary:
    Starts (or restarts) a transfer to/from the EEPROM.

  Description:
    This routine starts (or restarts) a transfer to/from the EEPROM, waiting (in
    a blocking loop) until the start (or re-start) condition has completed.

  Precondition:
    The I2C module must have been initialized.

  Parameters:
    restart - If FALSE, send a "Start" condition
            - If TRUE, send a "Restart" condition

  Returns:
    TRUE    - If successful
    FALSE   - If a collision occured during Start signaling

  Example:
    <code>
    StartTransfer(FALSE);
    </code>

  Remarks:
    This is a blocking routine that waits for the bus to be idle and the Start
    (or Restart) signal to complete.
  *****************************************************************************/

bool StartTransfer(uint8_t instance, bool restart )
{

    // Wait for the bus to be idle, then start the transfer
    while(PLIB_I2C_BusIsIdle(instance) == false);   

     #if defined(__PIC32MZ__)
    while(PLIB_I2C_StartWasDetected(instance) == false)
    {
       PLIB_I2C_MasterStart(instance);
    }
    #else
    PLIB_I2C_MasterStart(instance);
    while(PLIB_I2C_StartWasDetected(instance) == false);
    #endif
    
    return true;
}

/*******************************************************************************
  Function:
    BOOL TransmitOneByte( UINT8 data )

  Summary:
    This transmits one byte to the EEPROM.

  Description:
    This transmits one byte to the EEPROM, and reports errors for any bus
    collisions.

  Precondition:
    The transfer must have been previously started.

  Parameters:
    data    - Data byte to transmit

  Returns:
    TRUE    - Data was sent successfully
    FALSE   - A bus collision occured

  Example:
    <code>
    TransmitOneByte(0xAA);
    </code>

  Remarks:
    This is a blocking routine that waits for the transmission to complete.
  *****************************************************************************/

bool TransmitOneByte(uint8_t instance, uint8_t data )
{
 
    // Wait for the transmitter to be ready
    while(PLIB_I2C_TransmitterIsReady(instance) == false);
  //  {
        
    // Transmit the byte
    if(I2CSendByte(instance, data) == false)
    {
        return(false);
    }

    // Wait for the transmission to finish
    while(PLIB_I2C_TransmitterByteHasCompleted(instance) == false);

    return true;
  //  }

    return false;
}

//------------------------------------------------------------------------------
// Function: I2C_WriteBlock
// Description: Write a block of bytes to the spacifed I2C slave address at the specified offset.
//              The offset address is one byte (8 bit offset only).
//              The return code is always 0.  There is no indication in case of error.
//------------------------------------------------------------------------------
uint8_t I2C_WriteBlock(uint8_t instance, uint8_t deviceID, uint8_t offset, uint8_t *buffer, uint16_t length)
{
    uint16_t count = 0;

    buffer[0] = deviceID;
    buffer[1] = offset;

    while(StartTransfer(instance, false) == false);

    while( (count < (length + 1)) )
    {
            // Transmit a byte
            if(TransmitOneByte(instance, buffer[count]) == false)
            {
                return(false);
            }
            // Advance to the next byte
            count++;

            // Verify that the byte was acknowledged
            while(PLIB_I2C_TransmitterByteWasAcknowledged(instance) == false);

    }

    // End the transfer (hang here if an error occured)
    PLIB_I2C_MasterStop(instance);

    return(true);
}

//------------------------------------------------------------------------------
// Function: I2C_ReadBlock
// Description: Read a block of bytes from the spacifed I2C slave address at the specified offset.
//              The offset address is one byte (8 bit offset only).
//              The return code is always 0.  There is no indication in case of error.
//------------------------------------------------------------------------------
uint8_t I2C_ReadBlock(uint8_t instance, uint8_t deviceID, uint8_t offset, uint8_t *buffer, uint16_t length)
{
  static uint8_t count = 0;

    // Start the transfer to write data to the EEPROM
    while(StartTransfer(instance, false) == false);

    if(TransmitOneByte(instance, deviceID|0x01) == false)
    {
        PLIB_I2C_MasterStop(instance);
        return(1);
    }

    // Verify that the byte was acknowledged
    while(PLIB_I2C_TransmitterByteWasAcknowledged(instance) == false);

    for(count=0;count<length;count++)
    {
        PLIB_I2C_MasterReceiverClock1Byte(instance);
        
        while(PLIB_I2C_ReceivedByteIsAvailable(instance) == false);

        #if defined(__PIC32MZ__)
        *buffer++  = I2C1RCV;
        #else
        *buffer++  = I2C2RCV;
        #endif

        if(count == (length-1))
        {
            PLIB_I2C_ReceivedByteAcknowledge(instance, false);
        }
        else
        {
           PLIB_I2C_ReceivedByteAcknowledge(instance, true);

        }

        while(PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(instance) == true);
    }

   // End the transfer (stop here if an error occured)
   PLIB_I2C_MasterStop(instance);
   
    return(0);

}

bool I2CSendByte (uint8_t instance, uint8_t data )
{
    // Send the byte
    PLIB_I2C_TransmitterByteSend(instance,data);
    
    // Check for collisions       
    if(PLIB_I2C_ArbitrationLossHasOccurred(instance) == true)
    {
         return(false);
    }
        
    if(PLIB_I2C_TransmitterOverflowHasOccurred(instance) == true)
    {

         return(false);
    }

    return(true);
    
}

//#endif //Used only when debug command handler is enabled
