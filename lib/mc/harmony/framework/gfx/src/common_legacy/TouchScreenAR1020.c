/*****************************************************************************
 *
 * Simple 4 wire touch screen driver
 *
 *****************************************************************************
 * File Name:        TouchScreen.c
 * Dependencies:    Beep.h, TouchScreen.h
 * Processor:       PIC24, PIC32, dsPIC, PIC24H
 * Compiler:       	MPLAB C30, MPLAB C32
 * Linker:          MPLAB LINK30, MPLAB LINK32
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * Copyright � 2008 Microchip Technology Inc.  All rights reserved.
 * Microchip licenses to you the right to use, modify, copy and distribute
 * Software only when embedded on a Microchip microcontroller or digital
 * signal controller, which is integrated into your product or third party
 * product (pursuant to the sublicense terms in the accompanying license
 * agreement).  
 *
 * You should refer to the license agreement accompanying this Software
 * for additional information regarding your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY
 * OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,
 * BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA,
 * COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY
 * CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF),
 * OR OTHER SIMILAR COSTS.
 *
 * Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 04/21/11	    ...
 * 10/03/11	    TouchDetectPosition() now returns SHORT for compatibility.
 *****************************************************************************/
#include "TouchScreenAR1020.h"
#include "SST25VF016.h"
#include "drv_spi.h"
#include "gfx/src/common_legacy/TouchScreenAR1020.h"
#include "gfx/src/common_legacy/TouchScreen.h"
#include "internal_resource.h"

//////////////////////// LOCAL PROTOTYPES ////////////////////////////
void    TouchGetCalPoints(uint16_t,uint16_t,uint16_t);

extern uint8_t CalibrateFlag;
uint8_t                CapButtonState = 0;
uint8_t                ARData[5];

// Default Calibration Inset Value (percentage of vertical or horizontal resolution)
// Calibration Inset = ( CALIBRATIONINSET / 2 ) % , Range of 0�20% with 0.5% resolution
// Example with CALIBRATIONINSET == 20, the calibration points are measured
// 10% from the corners.
#define CALIBRATIONINSET   25       

#define CAL_X_INSET    (((GFX_MaxXGet()+1)*(CALIBRATIONINSET>>1))/100)
#define CAL_Y_INSET    (((GFX_MaxYGet()+1)*(CALIBRATIONINSET>>1))/100)

#define TOUCH_DIAMETER	10
#define SAMPLE_POINTS   4

#define CALIBRATION_DELAY   300				// delay between calibration touch points

extern NVM_READ_FUNC           pCalDataRead;                  // function pointer to data read
extern NVM_WRITE_FUNC          pCalDataWrite;                // function pointer to data write
extern NVM_SECTORERASE_FUNC    pCalDataSectorErase;    // function pointer to data sector erase

volatile short xcor = -1;
volatile short ycor = -1;

#define TOUCH_AR1020_REG_TOUCH_THRESHOLD        0x02
#define TOUCH_AR1020_REG_SENSITIVITY_FILTER     0x03
#define TOUCH_AR1020_REG_SAMPLING_FAST          0x04
#define TOUCH_AR1020_REG_SAMPLING_SLOW          0x05
#define TOUCH_AR1020_REG_ACCURACY_FILTER_FAST   0x06
#define TOUCH_AR1020_REG_ACCURACY_FILTER_SLOW   0x07
#define TOUCH_AR1020_REG_SPEED_THRESHOLD        0x08
#define TOUCH_AR1020_REG_SLEEP_DELAY            0x0A
#define TOUCH_AR1020_REG_PEN_UP_DELAY           0x0B
#define TOUCH_AR1020_REG_TOUCH_MODE             0x0C
#define TOUCH_AR1020_REG_TOUCH_OPTIONS          0x0D
#define TOUCH_AR1020_REG_COLIBRATION_INSET      0x0E
#define TOUCH_AR1020_REG_PEN_STATE_REPORT_DELAY 0x0F
#define TOUCH_AR1020_REG_TOUCH_REPORT_DELAY     0x11

typedef uint8_t    TOUCH_AR1020_REG;

static void SendByte(uint8_t data);
static uint8_t GetByte(void);

typedef struct
{
    uint8_t                startRegOffset;
    uint8_t                size;
    TOUCH_AR1020_REG    values[8];
}TOUCH_AR1020_REG_WRITE_DATA;


const TOUCH_AR1020_REG_WRITE_DATA regInit[] =
{
    {   TOUCH_AR1020_REG_TOUCH_THRESHOLD,       0x07,   { 0xC5, 0xC0, 0x04, 0x10, 0x02, 0x08, 0x04 }    },
    {   TOUCH_AR1020_REG_SLEEP_DELAY,           0x06,   { 0x64, 0x03, 0x51, 0x00, 0x19, 0xC8 }          },
    {   TOUCH_AR1020_REG_TOUCH_REPORT_DELAY,    0x01,   { 0x00 }                                        }
};

#define TOUCH_AR1020_REG_INIT_SIZE          (sizeof(regInit) / sizeof(TOUCH_AR1020_REG_WRITE_DATA))

#define TOUCH_AR1020_CMD_HEADER             0x55

#define TOUCH_AR1020_CMD_ENABLE_TOUCH       0x12
#define TOUCH_AR1020_CMD_DISABLE_TOUCH      0x13
#define TOUCH_AR1020_CMD_CALIBRATE_MODE     0x14
#define TOUCH_AR1020_CMD_REG_READ           0x20
#define TOUCH_AR1020_CMD_REG_WRITE          0x21
#define TOUCH_AR1020_CMD_REG_START_ADDR     0x22
#define TOUCH_AR1020_CMD_REG_WRITE_EEPROM   0x23
#define TOUCH_AR1020_CMD_EEPROM_READ        0x28
#define TOUCH_AR1020_CMD_EEPROM_WRITE       0x29
#define TOUCH_AR1020_CMD_EEPROM_WRITE_REG   0x2B

#define TOUCHAR1020_RESP_SUCCESS            0x00
#define TOUCHAR1020_RESP_BAD_CMD            0x01
#define TOUCHAR1020_RESP_BAD_HDR            0x03
#define TOUCHAR1020_RESP_CMD_TO             0x04
#define TOUCHAR1020_RESP_CAL_CANCEL         0xFC
#define TOUCHAR1020_RESP_INVALID            0xFF

typedef union
{
    struct
    {
        uint8_t pen:       1;
        uint8_t reserved:  6;
        uint8_t startBit:  1;
        uint8_t lowX;
        uint8_t highX;
        uint8_t lowY;
        uint8_t highY;
    };
    uint8_t packet[5];
}AR1020_TOUCH_REPORT_PACKET;


uint8_t TouchAR1020SendCommandAndGetResponse(uint8_t command, uint8_t *data, uint8_t dataSize);
void TouchAR1020RegisterWrite(uint16_t regOffset, TOUCH_AR1020_REG *regData, uint8_t numReg);
void TouchAR1020SendCommand(uint8_t command, uint8_t *data, uint8_t dataSize);
uint8_t TouchAR1020GetResponceData(uint8_t command, uint8_t cmdDataSize, uint8_t *data);
uint8_t TouchAR1020GetResponceStatus(uint8_t command, uint8_t dataSize);

//////////////////////// Resistive Touch Driver Version ////////////////////////////
// The first three digit is version number and 0xF is assigned to this
// 4-wire resistive driver.
const uint16_t mchpTouchScreenVersion = 0xE100;

static uint8_t detectPosition;
extern DRV_SPI_INIT     SPI_Init_Data;
/*********************************************************************
* Function: AR1020 ISR
* PreCondition: none
* Input: none
* Output: Returns 1 if touch sampling is done.
*         Returns 0 if the touch sampling is not successful.  
* Side Effects: none
* Overview: AR1020 ISR
* Note: none
********************************************************************/
short TouchDetectPosition(void)            //Routine for touch messages including cap and resistive
{
    static uint32_t sysClk = SYS_CLK_PRIMARY_CLOCK;
    uint32_t t0;
    static long xc,yc;
    static AR1020_TOUCH_REPORT_PACKET touchReport;
    uint8_t i;

    if((TouchScreenPacketReady()) && (detectPosition))
    {
   
        TouchScreenChipEnable();

        for(i = 0; i < sizeof(AR1020_TOUCH_REPORT_PACKET); i++)
        {
            SPIPut(SPI_Init_Data.spiId, 0);
            t0 = _CP0_GET_COUNT();
            while (_CP0_GET_COUNT() - t0 < (sysClk/3333));
            touchReport.packet[i] = PLIB_SPI_BufferRead(SPI_Init_Data.spiId);

        }

        xcor = -1;
        ycor = -1;

        if(touchReport.startBit)
        {
            if(touchReport.pen)
            {
                xc = touchReport.highX;
                xc <<= 7;
                xc |= touchReport.lowX;
                xc *= GFX_MaxXGet();
                xc >>= 11;
                xc++;
                xc >>= 1;

                yc = touchReport.highY;
                yc <<= 7;
                yc |= touchReport.lowY;
                yc *= GFX_MaxYGet();
                yc >>= 11;
                yc++;
                yc >>= 1;

                xcor = xc;
                ycor = yc;
            }
        }        

        TouchScreenChipDisable();
        TouchGetMsg();
    }

    return 1;    
}

/*********************************************************************
 * TouchAR1020GetTouchReport
 *********************************************************************/
/*********************************************************************
* Function: void TouchInit(void)
* PreCondition: none
* Input: none
* Output: none
* Side Effects: none
* Overview: sets ADC 
* Note: none
********************************************************************/
void TouchHardwareInit(void *initValues)
{
    TouchScreenConfigPacketReady();
   
    // Set IOs directions for AR1020 SPI
    TouchScreenChipDisable();
    TouchScreenChipDirection();

    while(TouchAR1020SendCommandAndGetResponse(TOUCH_AR1020_CMD_ENABLE_TOUCH, NULL, 0) != TOUCHAR1020_RESP_SUCCESS);          //Enable Touch Messages

    TouchScreenChipEnable();

    detectPosition = 1;
}

/*********************************************************************
* Function: SHORT TouchGetX()
*
* PreCondition: none
*
* Input: none
*
* Output: x coordinate
*
* Side Effects: none
*
* Overview: returns x coordinate if touch screen is pressed
*           and -1 if not
*
* Note: none
*
********************************************************************/
short TouchGetX(uint8_t instance)
{
    return (short)xcor;
}
/*********************************************************************
* Function: SHORT TouchGetRawX()
*
* PreCondition: none
*
* Input: none
*
* Output: x raw value
*
* Side Effects: none
*
* Overview: returns x coordinate if touch screen is pressed
*           and -1 if not
*
* Note: none
*
********************************************************************/
short TouchGetRawX(void)
{
    return (short)xcor;
}
/*********************************************************************
* Function: SHORT TouchGetY()
*
* PreCondition: none
*
* Input: none
*
* Output: y coordinate
*
* Side Effects: none
*
* Overview: returns y coordinate if touch screen is pressed
*           and -1 if not
*
* Note: none
*
********************************************************************/
short TouchGetY(uint8_t instance)
{
    return (short)ycor;
}

/*********************************************************************
* Function: SHORT TouchGetRawY()
*
* PreCondition: none
*
* Input: none
*
* Output: raw y value
*
* Side Effects: none
*
* Overview: returns y coordinate if touch screen is pressed
*           and -1 if not
*
* Note: none
*
********************************************************************/
short TouchGetRawY(void)
{
    return (short)ycor;
}
/*********************************************************************
* Function: void TouchStoreCalibration(void)
*
* PreCondition: EEPROMInit() must be called before
*
* Input: none
*
* Output: none
*
* Side Effects: none
*
* Overview: stores calibration parameters into EEPROM
*
* Note: none
*
********************************************************************/
void TouchStoreCalibration(void)
{
 	if (pCalDataWrite != NULL)
	{
		// the coefficient A address is used since it is the first one
		// and this assumes that all stored values are located in one 
		// sector
		if (pCalDataSectorErase != NULL)
			pCalDataSectorErase(ADDRESS_RESISTIVE_TOUCH_VERSION);


    	pCalDataWrite(mchpTouchScreenVersion, ADDRESS_RESISTIVE_TOUCH_VERSION);
    	
 	}
}

/*********************************************************************
* Function: void TouchLoadCalibration(void)
*
* PreCondition: EEPROMInit() must be called before
*
* Input: none
*
* Output: none
*
* Side Effects: none
*
* Overview: loads calibration parameters from EEPROM
*
* Note: none
*
********************************************************************/
void TouchLoadCalibration(void)
{
    // Calibration data is loaded by the controller
}
/*********************************************************************
* Function: void TouchGetCalPoints(void)
* PreCondition: InitGraph() must be called before
* Input: none
* Output: none
* Side Effects: none
* Overview: gets values for 3 touches
* Note: none
********************************************************************/
void TouchCalHWGetPoints(void)
{
    uint8_t i;
    uint8_t type;

    detectPosition = 0;

    while(TouchAR1020SendCommandAndGetResponse(TOUCH_AR1020_CMD_DISABLE_TOUCH, NULL, 0) != TOUCHAR1020_RESP_SUCCESS);          //Disable Touch Messages

    SYS_TMR_DelayMS(100);                                                 //asked for in spec
    
    for(i = 0; i < TOUCH_AR1020_REG_INIT_SIZE; i++)
        TouchAR1020RegisterWrite(regInit[i].startRegOffset, (TOUCH_AR1020_REG *)regInit[i].values, regInit[i].size);
    
    type = 0x04;

    while(TouchAR1020SendCommandAndGetResponse(TOUCH_AR1020_CMD_CALIBRATE_MODE, (uint8_t *)&type, 1) != TOUCHAR1020_RESP_SUCCESS);          //Enable Touch Messages

    GFX_XCHAR  	calStr1[] = {'P','r','e','s','s',' ','&',' ','R','e','l','e','a','s','e',0};
    GFX_XCHAR   calStr2[] = {'o','n',' ','t','h','e',' ','f','i','l','l','e','d',0};
    GFX_XCHAR   calStr3[] = {'c','i','r','c','l','e',0};
    short   counter;
    uint16_t    dx[SAMPLE_POINTS], dy[SAMPLE_POINTS];
    uint16_t    textHeight, msgY;

    GFX_FontSet((void *) &GOLFontDefault);
    GFX_ColorSet(BRIGHTRED);

    textHeight = GFX_TextStringHeightGet((void *) &GOLFontDefault);

    while
    (
        GFX_TextStringDraw
            (
                (GFX_MaxXGet() - GFX_TextStringWidthGet((GFX_XCHAR *)calStr1, (void *) &GOLFontDefault)) >> 1,
                ((GFX_MaxYGet() >> 1) - textHeight),
                (GFX_XCHAR *)calStr1, 0
            ) == GFX_STATUS_FAILURE
    );

    while
    (
        GFX_TextStringDraw
            (
                (GFX_MaxXGet() - GFX_TextStringWidthGet((GFX_XCHAR *)calStr2, (void *) &GOLFontDefault)) >> 1,
                (GFX_MaxYGet() >> 1),
                (GFX_XCHAR *)calStr2, 0
            ) == GFX_STATUS_FAILURE
    );

    while
    (
        GFX_TextStringDraw
            (
                (GFX_MaxXGet() - GFX_TextStringWidthGet((GFX_XCHAR *)calStr3, (void *) &GOLFontDefault)) >> 1,
                ((GFX_MaxYGet() >> 1) + textHeight),
                (GFX_XCHAR *)calStr3, 0
            ) == GFX_STATUS_FAILURE
    );


    // calculate center points (locate them at 15% away from the corners)
	// draw the four touch points

    dy[0] = dy[1] = CAL_Y_INSET;
    dy[2] = dy[3] = (GFX_MaxYGet() - CAL_Y_INSET);
    dx[0] = dx[3] = CAL_X_INSET;
    dx[1] = dx[2] = (GFX_MaxXGet() - CAL_X_INSET);


    msgY = ((GFX_MaxYGet() >> 1) - textHeight);
	
    for(counter = 0; counter < SAMPLE_POINTS; counter++)
    {
        TouchGetCalPoints(dx[counter], dy[counter], TOUCH_DIAMETER);                            //Get Cal points from upper left
    }

    SYS_TMR_DelayMS(100);                                                 //asked for in spec

    while(TouchAR1020SendCommandAndGetResponse(TOUCH_AR1020_CMD_ENABLE_TOUCH, NULL, 0) != TOUCHAR1020_RESP_SUCCESS);          //Enable Touch Messages
    
    detectPosition = 1;

}

/*********************************************************************
* Function: void TouchGetCalPoints(void)
* PreCondition: InitGraph() must be called before
* Input: none
* Output: none
* Side Effects: none
* Overview: gets values for 3 touches
* Note: none
********************************************************************/
void TouchGetCalPoints(uint16_t xpos, uint16_t ypos, uint16_t radius)
{
    // draw the new filled circle (new calibration point)
    GFX_ColorSet(BRIGHTRED);
    while((GFX_CircleDraw(xpos, ypos, radius)) == GFX_STATUS_FAILURE);
    while((GFX_CircleFillDraw(xpos, ypos, radius-3)) == GFX_STATUS_FAILURE);

    // show points left message
    TouchScreenChipEnable();            //Issue Calibrate Mode Command
    
    while(TouchAR1020GetResponceStatus(TOUCH_AR1020_CMD_CALIBRATE_MODE, 0) != TOUCHAR1020_RESP_SUCCESS);

  //  TouchScreenChipDisable();

    // start the touch delay 
    SYS_TMR_DelayMS(CALIBRATION_DELAY<<1);                                                 //asked for in spec

    GFX_ColorSet(WHITE);
    while((GFX_CircleDraw(xpos, ypos, radius)) == GFX_STATUS_FAILURE);
    while((GFX_CircleFillDraw(xpos, ypos, radius-3)) == GFX_STATUS_FAILURE);
}

uint16_t TouchAR1020GetRegisterStartAddress(void)
{
    uint8_t responce;
    uint8_t startAddress;
    uint8_t size = 0;

    TouchScreenChipEnable();

    TouchAR1020SendCommand(TOUCH_AR1020_CMD_REG_START_ADDR, NULL, 0);

    responce = TouchAR1020GetResponceData(TOUCH_AR1020_CMD_REG_START_ADDR, size, &startAddress);

    TouchScreenChipDisable();

    if(responce != TOUCHAR1020_RESP_SUCCESS)
        return 0xFFFF;

    return (uint16_t)startAddress;
        
}

void TouchAR1020SendCommand(uint8_t command, uint8_t *data, uint8_t dataSize)
{
    SendByte(TOUCH_AR1020_CMD_HEADER);
    SendByte(1 + dataSize);
    SendByte(command);

    while(dataSize)
    {
        SendByte(*data);
        *data++;
        dataSize--;
    }
}

uint8_t TouchAR1020GetResponceData(uint8_t command, uint8_t cmdDataSize, uint8_t *data)
{
    uint8_t responce;

    while(!TouchScreenPacketReady());

    if(GetByte() != TOUCH_AR1020_CMD_HEADER)
        return TOUCHAR1020_RESP_INVALID;

    cmdDataSize = GetByte();

    if(!cmdDataSize)
        return TOUCHAR1020_RESP_INVALID;

    responce = GetByte();

    if(GetByte() != command)
        return TOUCHAR1020_RESP_INVALID;

    cmdDataSize -= 2;

    while(cmdDataSize)
    {
        if(data != NULL)
        {
            *data = GetByte();
            data++;
        }else
        {
            GetByte();
        }

        cmdDataSize--;
    }

    return responce;
}
uint8_t TouchAR1020GetResponceStatus(uint8_t command, uint8_t dataSize)
{
    return TouchAR1020GetResponceData(command, dataSize, NULL);
}

uint8_t TouchAR1020SendCommandAndGetResponse(uint8_t command, uint8_t *data, uint8_t dataSize)
{
    uint8_t responce;

    TouchScreenChipEnable();

    TouchAR1020SendCommand(command, data, dataSize);

    responce = TouchAR1020GetResponceStatus(command, dataSize);

    TouchScreenChipDisable();

    return responce;

}

void TouchAR1020RegisterWrite(uint16_t regOffset, TOUCH_AR1020_REG *regData, uint8_t numReg)
{
    uint8_t regPacket[12];
    uint16_t startAddress;

    startAddress = TouchAR1020GetRegisterStartAddress();

    while(numReg)
    {
         uint8_t i;
         uint8_t regDataSize = 8;

        if(regDataSize > numReg)
            regDataSize = numReg;

        startAddress += regOffset;

        regPacket[0] = (uint8_t)(startAddress >> 8);
        regPacket[1] = (uint8_t)(startAddress);
        regPacket[2] = regDataSize;
        
        for(i = 0; i < regDataSize; i++)
        {
            regPacket[i + 3] = *regData;
            *regData++;
        }

        if(TouchAR1020SendCommandAndGetResponse(TOUCH_AR1020_CMD_REG_WRITE, regPacket, regDataSize + 3) == TOUCHAR1020_RESP_SUCCESS)
        {
            numReg -= regDataSize;
            regOffset += regDataSize;
        }
    }
}

static void SendByte(uint8_t data)
{
    SPIPut(SPI_Init_Data.spiId, data);
    SYS_TMR_DelayMS(1);       //Asked for in AR1020 Spec
    PLIB_SPI_BufferRead(SPI_Init_Data.spiId);
}

static uint8_t GetByte(void)
{
    SPIPut(SPI_Init_Data.spiId, 0);
     SYS_TMR_DelayMS(1);       //Asked for in AR1020 Spec
    return PLIB_SPI_BufferRead(SPI_Init_Data.spiId);
}
