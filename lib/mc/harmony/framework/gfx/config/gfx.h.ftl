<#--
/*******************************************************************************
  GFX Freemarker Template File

  Company:
    Microchip Technology Inc.

  File Name:
    gfx.h.ftl

  Summary:
    GFX Freemarker Template File

  Description:

*******************************************************************************/

/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS  WITHOUT  WARRANTY  OF  ANY  KIND,
EITHER EXPRESS  OR  IMPLIED,  INCLUDING  WITHOUT  LIMITATION,  ANY  WARRANTY  OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A  PARTICULAR  PURPOSE.
IN NO EVENT SHALL MICROCHIP OR  ITS  LICENSORS  BE  LIABLE  OR  OBLIGATED  UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,  BREACH  OF  WARRANTY,  OR
OTHER LEGAL  EQUITABLE  THEORY  ANY  DIRECT  OR  INDIRECT  DAMAGES  OR  EXPENSES
INCLUDING BUT NOT LIMITED TO ANY  INCIDENTAL,  SPECIAL,  INDIRECT,  PUNITIVE  OR
CONSEQUENTIAL DAMAGES, LOST  PROFITS  OR  LOST  DATA,  COST  OF  PROCUREMENT  OF
SUBSTITUTE  GOODS,  TECHNOLOGY,  SERVICES,  OR  ANY  CLAIMS  BY  THIRD   PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE  THEREOF),  OR  OTHER  SIMILAR  COSTS.
*******************************************************************************/
-->


/*** GFX Library Configuration ***/

<#if CONFIG_USE_GFX_STACK == true>
#define GFX_INSTANCES_NUMBER                            ${CONFIG_GFX_NUMBER_OF_INSTANCES}
#define GFX_RUN_MODE                                    ${CONFIG_GFX_RUN_MODE}
#define GFX_SELF_PREEMPTION_LEVEL                       ${CONFIG_GFX_PREEMPTION_LEVEL}
<#if CONFIG_GFX_ALPHABLEND_DISABLE == true>
#define GFX_CONFIG_ALPHABLEND_DISABLE
</#if>
<#if CONFIG_GFX_GRADIENT_DISABLE == true>
#define GFX_CONFIG_GRADIENT_DISABLE
</#if>
<#if CONFIG_GFX_PALETTE_DISABLE == true>
#define GFX_CONFIG_PALETTE_DISABLE
</#if>
<#if CONFIG_GFX_FONT_ANTIALIASED_DISABLE == true>
#define GFX_CONFIG_FONT_ANTIALIASED_DISABLE
</#if>
<#if CONFIG_GFX_TRANSPARENT_COLOR_DISABLE == true>
#define GFX_CONFIG_TRANSPARENT_COLOR_DISABLE
</#if>
<#if CONFIG_GFX_PALETTE_EXTERNAL_DISABLE == true>
#define GFX_CONFIG_PALETTE_EXTERNAL_DISABLE
</#if>
<#if CONFIG_GFX_FOCUS_DISABLE == true>
#define GFX_CONFIG_FOCUS_DISABLE
</#if>
#define GFX_CONFIG_FONT_CHAR_SIZE                           ${CONFIG_GFX_FONT_CHAR_SIZE}
<#if CONFIG_GFX_FONT_FLASH_DISABLE == true>
#define GFX_CONFIG_FONT_FLASH_DISABLE
</#if>
<#if CONFIG_GFX_FONT_EXTERNAL_DISABLE == true>
#define GFX_CONFIG_FONT_EXTERNAL_DISABLE
</#if>
<#if CONFIG_GFX_FONT_RAM_DISABLE == true>
#define GFX_CONFIG_FONT_RAM_DISABLE
</#if>
<#if CONFIG_GFX_IMAGE_FLASH_DISABLE == true>
#define GFX_CONFIG_IMAGE_FLASH_DISABLE
</#if>
<#if CONFIG_GFX_IMAGE_EXTERNAL_DISABLE == true>
#define GFX_CONFIG_IMAGE_EXTERNAL_DISABLE
</#if>
<#if CONFIG_GFX_IMAGE_RAM_DISABLE == true>
#define GFX_CONFIG_IMAGE_RAM_DISABLE
</#if>
<#if CONFIG_GFX_RLE_DECODE_DISABLE == true>
#define GFX_CONFIG_RLE_DECODE_DISABLE
</#if>
#define GFX_CONFIG_COLOR_DEPTH                              ${CONFIG_GFX_COLOR_DEPTH}
<#if CONFIG_GFX_DOUBLE_BUFFERING_DISABLE == true>
#define GFX_CONFIG_DOUBLE_BUFFERING_DISABLE
</#if>
<#if CONFIG_GFX_IMAGE_PADDING_DISABLE == true>
#define GFX_CONFIG_IMAGE_PADDING_DISABLE
</#if>
<#if CONFIG_GFX_USE_KEYBOARD_DISABLE == true>
#define GFX_CONFIG_USE_KEYBOARD_DISABLE
</#if>
<#if CONFIG_GFX_USE_TOUCHSCREEN == false>
#define GFX_CONFIG_USE_TOUCHSCREEN_DISABLE
</#if>
#define GFX_malloc(size)                                    malloc(size)
#define GFX_free(pObj)                                      free(pObj)


<#-- Instance 0 -->
<#if CONFIG_GFX_INST_0 == true>
</#if>

<#-- Instance 1 -->
<#if CONFIG_GFX_INST_1 == true>
</#if>

</#if>

<#--
/*******************************************************************************
 End of File
*/
-->

