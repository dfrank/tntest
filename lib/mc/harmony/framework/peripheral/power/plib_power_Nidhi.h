/*******************************************************************************
  Power Peripheral Library Interface Header

  Company:
    Microchip Technology Inc.

  File Name:
    plib_power.h

  Summary:
    Defines the power peripheral library interface

  Description:
    This header file contains the function prototypes and definitions of
    the data types and constants that make up the interface to the power
    (POWER) peripheral library (PLIB) for Microchip microcontrollers.  The
    definitions in this file are for power controller module.
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

#ifndef _PLIB_POWER_H
#define _PLIB_POWER_H


// *****************************************************************************
// *****************************************************************************
// Section: Included Files (continued at end of file)
// *****************************************************************************
// *****************************************************************************
/*  This section lists the other files that are included in this file.  However,
    please see the bottom of the file for additional implementation header files
    that are also included
*/

#include "processor.h"

// *****************************************************************************
// *****************************************************************************
// Section: Constants & Data Types
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* POWER  module disable enumeration

  Summary:
    Lists the module disable selection.

  Description:
    This enumeration lists the module disable selection.

  Remarks:
    Not all modules may not exist in all the devices. Please refer to the
    specific device data sheet for availability.
 */

typedef enum {
    /* The ADC module is disabled */
    POWER_MODULE_DISABLE_ADC /*DOM-IGNORE-BEGIN*/ = 0 /* DOM-IGNORE-END*/,

    /* The Sigma Delta ADC module is disabled */
    POWER_MODULE_SIGMA_DELTA_ADC /*DOM-IGNORE-BEGIN*/ = 1 /* DOM-IGNORE-END*/,

    /* The DAC module is disabled */
    POWER_MODULE_DISABLE_DAC /*DOM-IGNORE-BEGIN*/ = 2 /* DOM-IGNORE-END*/,
	
	/* The SPI1 module is disabled */
    POWER_MODULE_DISABLE_SPI1 /*DOM-IGNORE-BEGIN*/ = 3 /* DOM-IGNORE-END*/,
	
	/* The SPI2 module is disabled */
    POWER_MODULE_DISABLE_SPI2 /*DOM-IGNORE-BEGIN*/ = 4 /* DOM-IGNORE-END*/,
	
	/* The SPI3 module is disabled */
    POWER_MODULE_DISABLE_SPI3 /*DOM-IGNORE-BEGIN*/ = 5 /* DOM-IGNORE-END*/,
	
	/* The UART1 module is disabled */
    POWER_MODULE_DISABLE_UART1 /*DOM-IGNORE-BEGIN*/ = 6 /* DOM-IGNORE-END*/,
	
	/* The UART2 module is disabled */
    POWER_MODULE_DISABLE_UART2 /*DOM-IGNORE-BEGIN*/ = 7 /* DOM-IGNORE-END*/,
		
	/* The UART3 module is disabled */
    POWER_MODULE_DISABLE_UART3 /*DOM-IGNORE-BEGIN*/ = 8 /* DOM-IGNORE-END*/,
	
	/* The UART4 module is disabled */
    POWER_MODULE_DISABLE_UART4 /*DOM-IGNORE-BEGIN*/ = 9 /* DOM-IGNORE-END*/,
	
	/* The I2C1 module is disabled */
    POWER_MODULE_DISABLE_I2C1 /*DOM-IGNORE-BEGIN*/ = 10 /* DOM-IGNORE-END*/,
	
	/* The I2C2 module is disabled */
    POWER_MODULE_DISABLE_I2C2 /*DOM-IGNORE-BEGIN*/ = 11 /* DOM-IGNORE-END*/,
	
	/* The I2C3 module is disabled */
    POWER_MODULE_DISABLE_I2C3 /*DOM-IGNORE-BEGIN*/ = 12 /* DOM-IGNORE-END*/,
	
	/* The Timer1 module is disabled */
    POWER_MODULE_DISABLE_TIMER1 /*DOM-IGNORE-BEGIN*/ = 13 /* DOM-IGNORE-END*/,

	/* The Timer2 module is disabled */
    POWER_MODULE_DISABLE_TIMER2 /*DOM-IGNORE-BEGIN*/ = 14 /* DOM-IGNORE-END*/,

	/* The Timer3 module is disabled */
    POWER_MODULE_DISABLE_TIMER3 /*DOM-IGNORE-BEGIN*/ = 15 /* DOM-IGNORE-END*/,

	/* The Timer4 module is disabled */
    POWER_MODULE_DISABLE_TIMER4 /*DOM-IGNORE-BEGIN*/ = 16 /* DOM-IGNORE-END*/,
	
	/* The Timer5 module is disabled */
    POWER_MODULE_DISABLE_TIMER5 /*DOM-IGNORE-BEGIN*/ = 17 /* DOM-IGNORE-END*/,
	
	/* The Input Capture 1 module is disabled */
    POWER_MODULE_DISABLE_INPUTCAPTURE1 /*DOM-IGNORE-BEGIN*/ = 18 /* DOM-IGNORE-END*/,

	/* The Input Capture 2 module is disabled */
    POWER_MODULE_DISABLE_INPUTCAPTURE2 /*DOM-IGNORE-BEGIN*/ = 19 /* DOM-IGNORE-END*/,
	
	/* The Input Capture 3 module is disabled */
    POWER_MODULE_DISABLE_INPUTCAPTURE3 /*DOM-IGNORE-BEGIN*/ = 20 /* DOM-IGNORE-END*/,
	
	/* The Input Capture 4 module is disabled */
    POWER_MODULE_DISABLE_INPUTCAPTURE4 /*DOM-IGNORE-BEGIN*/ = 21 /* DOM-IGNORE-END*/,
	
	/* The Input Capture 5 module is disabled */
    POWER_MODULE_DISABLE_INPUTCAPTURE5 /*DOM-IGNORE-BEGIN*/ = 22 /* DOM-IGNORE-END*/,
	
	/* The Input Capture 6 module is disabled */
    POWER_MODULE_DISABLE_INPUTCAPTURE6 /*DOM-IGNORE-BEGIN*/ = 23 /* DOM-IGNORE-END*/,
	
	/* The Input Capture 7 module is disabled */
    POWER_MODULE_DISABLE_INPUTCAPTURE7 /*DOM-IGNORE-BEGIN*/ = 24 /* DOM-IGNORE-END*/,
	
	/* The Input Capture 8 module is disabled */
    POWER_MODULE_DISABLE_INPUTCAPTURE8 /*DOM-IGNORE-BEGIN*/ = 25 /* DOM-IGNORE-END*/,
	
	/* The Input Capture 9 module is disabled */
    POWER_MODULE_DISABLE_INPUTCAPTURE9 /*DOM-IGNORE-BEGIN*/ = 26 /* DOM-IGNORE-END*/,

	/* The Output Compare 1 module is disabled */
    POWER_MODULE_DISABLE_OUTPUTCOMPARE1 /*DOM-IGNORE-BEGIN*/ = 27 /* DOM-IGNORE-END*/,

	/* The Output Compare 2 module is disabled */
    POWER_MODULE_DISABLE_OUTPUTCOMPARE2 /*DOM-IGNORE-BEGIN*/ = 28 /* DOM-IGNORE-END*/,

	/* The Output Compare 3 module is disabled */
    POWER_MODULE_DISABLE_OUTPUTCOMPARE3 /*DOM-IGNORE-BEGIN*/ = 29 /* DOM-IGNORE-END*/,

	/* The Output Compare 4 module is disabled */
    POWER_MODULE_DISABLE_OUTPUTCOMPARE4 /*DOM-IGNORE-BEGIN*/ = 30 /* DOM-IGNORE-END*/,

	/* The Output Compare 5 module is disabled */
    POWER_MODULE_DISABLE_OUTPUTCOMPARE5 /*DOM-IGNORE-BEGIN*/ = 31 /* DOM-IGNORE-END*/,

	/* The Output Compare 6 module is disabled */
    POWER_MODULE_DISABLE_OUTPUTCOMPARE6 /*DOM-IGNORE-BEGIN*/ = 32 /* DOM-IGNORE-END*/,

	/* The Output Compare 7 module is disabled */
    POWER_MODULE_DISABLE_OUTPUTCOMPARE7 /*DOM-IGNORE-BEGIN*/ = 33 /* DOM-IGNORE-END*/,

	/* The Output Compare 8 module is disabled */
    POWER_MODULE_DISABLE_OUTPUTCOMPARE8 /*DOM-IGNORE-BEGIN*/ = 34 /* DOM-IGNORE-END*/,

	/* The Output Compare 9 module is disabled */
    POWER_MODULE_DISABLE_OUTPUTCOMPARE9 /*DOM-IGNORE-BEGIN*/ = 35 /* DOM-IGNORE-END*/,
	
	/* The Parallel Master Port module is disabled */
    POWER_MODULE_DISABLE_PMP /*DOM-IGNORE-BEGIN*/ = 36 /* DOM-IGNORE-END*/,
	
	/* The Cyclic Redundancy Check module is disabled */
    POWER_MODULE_DISABLE_CRC /*DOM-IGNORE-BEGIN*/ = 37 /* DOM-IGNORE-END*/,
	
	/* The RealTime Clock and Calender module is disabled */
    POWER_MODULE_DISABLE_RTCC /*DOM-IGNORE-BEGIN*/ = 38 /* DOM-IGNORE-END*/,
	
	/* The Comparator module is disabled */
    POWER_MODULE_DISABLE_COMPARATOR /*DOM-IGNORE-BEGIN*/ = 39 /* DOM-IGNORE-END*/,
	
	/* The USB module is disabled */
    POWER_MODULE_DISABLE_USB /*DOM-IGNORE-BEGIN*/ = 40 /* DOM-IGNORE-END*/,
	
	/* The Low Voltage Detect module is disabled */
    POWER_MODULE_DISABLE_LVD /*DOM-IGNORE-BEGIN*/ = 41 /* DOM-IGNORE-END*/,
	
	/* The High Low Voltage Detect module is disabled */
    POWER_MODULE_DISABLE_HLVD /*DOM-IGNORE-BEGIN*/ = 42 /* DOM-IGNORE-END*/,
	
	/* The Charge Time Measurement Unit module is disabled */
    POWER_MODULE_DISABLE_CTMU /*DOM-IGNORE-BEGIN*/ = 43 /* DOM-IGNORE-END*/,
	
	/* The Reference Clock Output module is disabled */
    POWER_MODULE_DISABLE_REFO /*DOM-IGNORE-BEGIN*/ = 44 /* DOM-IGNORE-END*/,
	
	/* The LCD module is disabled */
    POWER_MODULE_DISABLE_LCD /*DOM-IGNORE-BEGIN*/ = 44 /* DOM-IGNORE-END*/,
	
	/* The Operational Amplifier 1 module is disabled */
    POWER_MODULE_DISABLE_OPAMP1 /*DOM-IGNORE-BEGIN*/ = 45 /* DOM-IGNORE-END*/,
	
	/* The Operational Amplifier 2 module is disabled */
    POWER_MODULE_DISABLE_OPAMP2 /*DOM-IGNORE-BEGIN*/ = 46 /* DOM-IGNORE-END*/,
	
	/* The Data Memory Access 1 module is disabled */
    POWER_MODULE_DISABLE_DMA1 /*DOM-IGNORE-BEGIN*/ = 47 /* DOM-IGNORE-END*/,
	
	/* The Data Memory Access 2 module is disabled */
    POWER_MODULE_DISABLE_DMA2 /*DOM-IGNORE-BEGIN*/ = 48 /* DOM-IGNORE-END*/,
	
	/* The Cryptographic module is disabled */
    POWER_MODULE_DISABLE_CRYPTO /*DOM-IGNORE-BEGIN*/ = 49 /* DOM-IGNORE-END*/,
	
	/* The Data Signal Modulator  module is disabled */
    POWER_MODULE_DISABLE_DSM /*DOM-IGNORE-BEGIN*/ = 50 /* DOM-IGNORE-END*/,
	
	/* The Enhanced Capture Compare PWM 1 module is disabled */
    POWER_MODULE_DISABLE_ECCP1 /*DOM-IGNORE-BEGIN*/ = 51 /* DOM-IGNORE-END*/,
	
	/* The Enhanced Capture Compare PWM 2 module is disabled */
    POWER_MODULE_DISABLE_ECCP2 /*DOM-IGNORE-BEGIN*/ = 52 /* DOM-IGNORE-END*/,
	
	/* The Enhanced Capture Compare PWM 3 module is disabled */
    POWER_MODULE_DISABLE_ECCP3 /*DOM-IGNORE-BEGIN*/ = 53 /* DOM-IGNORE-END*/,
	
	/* The Capture Compare PWM 4 module is disabled */
    POWER_MODULE_DISABLE_CCP4 /*DOM-IGNORE-BEGIN*/ = 54 /* DOM-IGNORE-END*/,
	
	/* The Capture Compare PWM 5 module is disabled */
    POWER_MODULE_DISABLE_CCP5 /*DOM-IGNORE-BEGIN*/ = 55 /* DOM-IGNORE-END*/,
	
	/* The Capture Compare PWM 6 module is disabled */
    POWER_MODULE_DISABLE_CCP6 /*DOM-IGNORE-BEGIN*/ = 56 /* DOM-IGNORE-END*/,
	
	/* The Capture Compare PWM 7 module is disabled */
    POWER_MODULE_DISABLE_CCP7 /*DOM-IGNORE-BEGIN*/ = 57 /* DOM-IGNORE-END*/,
	
	/* The Capture Compare PWM 8 module is disabled */
    POWER_MODULE_DISABLE_CCP8 /*DOM-IGNORE-BEGIN*/ = 58 /* DOM-IGNORE-END*/,
	
	/* The Capture Compare PWM 9 module is disabled */
    POWER_MODULE_DISABLE_CCP9 /*DOM-IGNORE-BEGIN*/ = 59 /* DOM-IGNORE-END*/,
	
	/* The Capture Compare PWM 10 module is disabled */
    POWER_MODULE_DISABLE_CCP10 /*DOM-IGNORE-BEGIN*/ = 60 /* DOM-IGNORE-END*/,
	
	/* The External Memory Bus module is disabled */
    POWER_MODULE_DISABLE_EMB /*DOM-IGNORE-BEGIN*/ = 61 /* DOM-IGNORE-END*/,
		
} POWER_MODULE_DISABLE;


// *****************************************************************************
// *****************************************************************************
// Section: Peripheral Library Interface Routines
// *****************************************************************************
// *****************************************************************************

//******************************************************************************
/* Function:
    void PLIB_POWER_ModuleDisable ( POWER_MODULE_ID index, POWER_MODULE_DISABLE source )

  Summary:
    Disable the power supply for the module selected in the source.

  Description:
    This API completely shuts down the selected peripheral, effectively powering down 
	all circuits and removing all clock sources. This has the additional effect of 
	making any of the module�s control and buffer registers, mapped in the SFR space, 
	unavailable for operations.
	
  Precondition:
    None.

  Parameters:
    index    - Identifier for the device instance to be configured
	source 	 - Select the module to be disabled from POWER_MODULE_DISABLE enum

  Returns:
    None.

  Example:
    <code>
    PLIB_POWER_ModuleDisable (POWER_ID_1, POWER_MODULE_DISABLE_ADC);
    </code>

  Remarks:
    This functionality is not available on all parts. Please refer to the data
   sheet for the part in use to identify supported features.
   
 */

void PLIB_POWER_ModuleDisable (POWER_MODULE_ID index, POWER_MODULE_DISABLE source);


/*******************************************************************************
/*  Function:
    void PLIB_POWER_VoltageRegulatorEnable (POWER_MODULE_ID index)

  Summary:
    Enable the Voltage regulator during the sleep mode.

  Description:
    This routine enables the voltage regulator during the sleep mode for the selected
    device.

  Precondition:
    None.

  Parameters:
    index    - Identifier for the device instance to be configured

  Returns:
    None.

  Example:
    <code>
    PLIB_POWER_VoltageRegulatorEnable(POWER_ID_1);
    </code>

  Remarks:
    This functionality is not available on all parts. Please refer to the data
    sheet for the part in use to identify supported features.
*/

void PLIB_POWER_VoltageRegulatorEnable (POWER_MODULE_ID index);


//******************************************************************************
/*  Function:
    void PLIB_POWER_VoltageRegulatorDisable (POWER_MODULE_ID index)

  Summary:
    Disable the Voltage regulator during the sleep mode.

  Description:
    This routine put the voltage regulator into standby mode during sleep for 
	the selected device.

  Precondition:
    None.

  Parameters:
    index    - Identifier for the device instance to be configured

  Returns:
    None.

  Example:
    <code>
    PLIB_POWER_VoltageRegulatorDisable(POWER_ID_1); 
    </code>

  Remarks:
    This functionality is not available on all parts. Please refer to the data
    sheet for the part in use to identify supported features.
*/

void PLIB_POWER_VoltageRegulatorDisable (POWER_MODULE_ID index);


//******************************************************************************
/*  Function:
    bool PLIB_POWER_DeviceSleepStatus (POWER_MODULE_ID index)

  Summary:
    Returns the sleep mode status of the device.

  Description:
    This routine returns the Sleep mode status of the device. The bit 
	will be set when the device wakes from sleep mode.
	
  Precondition:
    None.

  Parameters:
    index    - Identifier for the device instance to be configured
	
  Returns:
    true    - device is in Sleep mode
    false   - device was not in Sleep mode

  Example:
    <code>
    if(PLIB_POWER_DeviceSleepStatus(POWER_ID_1))
    {
	
    }
    </code>

  Remarks:
    None.
	
*/

bool PLIB_POWER_DeviceSleepStatus (POWER_MODULE_ID index);


//******************************************************************************
/*  Function:
    bool PLIB_POWER_DeviceIdleModeStatus (POWER_MODULE_ID index)

  Summary:
    Returns the idle mode status of the device.

  Description:
    This routine returns the idle mode status of the device. The bit will be set 
    when the device wakes from idle mode.
	
  Precondition:
    None.

  Parameters:
    index    - Identifier for the device instance to be configured
	
  Returns:
    true    - Device is in idle mode
    false   - Device was not in idle mode


  Example:
    <code>
    if(PLIB_POWER_DeviceIdleModeStatus(POWER_ID_1))
    {
	
    }
    </code>

  Remarks:
    This functionality is not available on all parts. Please refer to the data
    sheet for the part in use to identify supported features.
*/

bool PLIB_POWER_DeviceIdleModeStatus (POWER_MODULE_ID index);


//******************************************************************************
/*  Function:
    void PLIB_POWER_DeepSleepModeEnable (POWER_MODULE_ID index)

  Summary:
    Enables the Deep Sleep mode.

  Description:
    This API Enables the Deep Sleep mode. Deep Sleep mode is intended to provide 
	the lowest levels of power consumption available without requiring the use of 
	external switches to completely remove all power from the device.

  Precondition:
    None.

  Parameters:
    index    - Identifier for the device instance to be configured

  Returns:
    None.

  Example:
    <code>
    PLIB_POWER_DeepSleepModeEnable(POWER_ID_1);
    </code>

  Remarks:
   This functionality is not available on all parts. Please refer to the data
    sheet for the part in use to identify supported features.
*/

void PLIB_POWER_DeepSleepModeEnable (POWER_MODULE_ID index);


//******************************************************************************
/*  Function:
    void PLIB_POWER_DeepSleepModeDisable (POWER_MODULE_ID index)

  Summary:
    Disable the Deep Sleep mode.

  Description:
    This API Disable the Deep Sleep mode. Deep Sleep mode is intended to provide 
	the lowest levels of power consumption available without requiring the use 
	of external switches to completely remove all power from the device.

  Precondition:
    None.

  Parameters:
    index    - Identifier for the device instance to be configured

  Returns:
    None.

  Example:
    <code>
    PLIB_POWER_DeepSleepModeDisable(POWER_ID_1);
    </code>

  Remarks:
   This functionality is not available on all parts. Please refer to the data
    sheet for the part in use to identify supported features.
*/

void PLIB_POWER_DeepSleepModeDisable (POWER_MODULE_ID index);


//******************************************************************************
/*  Function:
    void PLIB_POWER_DeepSleepBOREnable (POWER_MODULE_ID index)

  Summary:
    Enables the Deep Sleep BOR mode.

  Description:
    This API Enables the Deep Sleep BOR. The DSBOR is independent of the standard
	BOR and a Deep Sleep BOR event will NOT cause a wake-up from Deep Sleep.

  Precondition:
    None.

  Parameters:
    index    - Identifier for the device instance to be configured

  Returns:
    None.

  Example:
    <code>
    PLIB_POWER_DeepSleepBOREnable(POWER_ID_1);
    </code>

  Remarks:
   This functionality is not available on all parts. Please refer to the data
    sheet for the part in use to identify supported features.
*/

void PLIB_POWER_DeepSleepBOREnable (POWER_MODULE_ID index);


//******************************************************************************
/*  Function:
    void PLIB_POWER_DeepSleepBORDisable (POWER_MODULE_ID index)

  Summary:
    Disable the Deep Sleep BOR.

  Description:
    This API Disables the Deep Sleep BOR. The DSBOR is independent of the standard 
	BOR and a Deep Sleep BOR event will NOT cause a wake-up from Deep Sleep.

	
  Precondition:
    None.

  Parameters:
    index    - Identifier for the device instance to be configured

  Returns:
    None.

  Example:
    <code>
    PLIB_POWER_DeepSleepBORDisable(POWER_ID_1);
    </code>

  Remarks:
   This functionality is not available on all parts. Please refer to the data
    sheet for the part in use to identify supported features.
*/

void PLIB_POWER_DeepSleepBORDisable (POWER_MODULE_ID index);


//******************************************************************************
/*  Function:
    bool PLIB_POWER_DeepSleepInterruptOnChangeStatus (POWER_MODULE_ID index)

  Summary:
    Returns the status of Deep Sleep Interrupt on Change bit.  
	
  Description:
    This API returns the status of the Deep Sleep Interrupt on Change bit.
	
  Precondition:
    None.

  Parameters:
    index    - Identifier for the device instance to be configured

  Returns:
    true    - Interrupt-on-change was asserted during Deep Sleep
    false   - Interrupt-on-change was not asserted during Deep Sleep

  Example:
    <code>
    if(PLIB_POWER_DeepSleepInterruptOnChangeStatus(POWER_ID_1))
    {

    }
    </code>

  Remarks:
    This functionality is not available on all parts. Please refer to the data
    sheet for the part in use to identify supported features.
*/

bool PLIB_POWER_DeepSleepInterruptOnChangeStatus (POWER_MODULE_ID index);


//******************************************************************************
/*  Function:
    bool PLIB_POWER_DeepSleepFaultDetectStatus (POWER_MODULE_ID index)

  Summary:
    Returns the status of Deep Sleep Fault Detect bit.  
	
  Description:
    This API returns the status of the Deep Sleep Fault Detect bit.
	
  Precondition:
    None.

  Parameters:
    index    - Identifier for the device instance to be configured

  Returns:
    true    - A Fault occurred during Deep Sleep, and some Deep Sleep configuration 
			  settings may have been corrupted.
    false   - No Fault was detected during Deep Sleep

  Example:
    <code>
    if(PLIB_POWER_DeepSleepFaultDetectStatus(POWER_ID_1))
    {

    }
    </code>

  Remarks:
    This functionality is not available on all parts. Please refer to the data
    sheet for the part in use to identify supported features.
*/

bool PLIB_POWER_DeepSleepFaultDetectStatus (POWER_MODULE_ID index);


//******************************************************************************
/*  Function:
    bool PLIB_POWER_DeepSleepMCLREventStatus (POWER_MODULE_ID index)

  Summary:
    Returns the status of Deep Sleep MCLR Event bit.  
	
  Description:
    This API returns the status of the Deep Sleep MCLR Event bit.
	
  Precondition:
    None.

  Parameters:
    index    - Identifier for the device instance to be configured

  Returns:
    true    - The MCLR pin was active and was asserted during Deep Sleep
    false   - The MCLR pin was not active, or was active, but not asserted 
			   during Deep Sleep

  Example:
    <code>
    if(PLIB_POWER_DeepSleepMCLREventStatus(POWER_ID_1))
    {

    }
    </code>

  Remarks:
    This functionality is not available on all parts. Please refer to the data
    sheet for the part in use to identify supported features.
*/

bool PLIB_POWER_DeepSleepMCLREventStatus (POWER_MODULE_ID index);


//******************************************************************************
/*  Function:
    bool PLIB_POWER_DeepSleepPowerOnResetStatus (POWER_MODULE_ID index)

  Summary:
    Returns the status of Deep Sleep Power-on Reset bit.  
	
  Description:
    This API returns the status of the Deep Sleep Power-on Reset bit.
	
  Precondition:
    None.

  Parameters:
    index    - Identifier for the device instance to be configured

  Returns:
    true    - The VDD supply POR circuit was active and a POR event was detected
    false   - The VDD supply POR circuit was not active, or was active but did 
			  not detect a POR event

  Example:
    <code>
    if(PLIB_POWER_DeepSleepPowerOnResetStatus(POWER_ID_1))
    {

    }
    </code>

  Remarks:
    This functionality is not available on all parts. Please refer to the data
    sheet for the part in use to identify supported features.
*/

bool PLIB_POWER_DeepSleepPowerOnResetStatus (POWER_MODULE_ID index);

#endif //#ifndef _PLIB_POWER_H
/*******************************************************************************
 End of File
*/

