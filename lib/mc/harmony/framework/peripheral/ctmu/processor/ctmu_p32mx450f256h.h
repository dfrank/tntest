/* Created by plibgen $Revision: 1.30 $ */

#ifndef _CTMU_P32MX450F256H_H
#define _CTMU_P32MX450F256H_H

/* Section 1 - Enumerate instances, define constants, VREGs */

#include <xc.h>
#include <stdbool.h>

#include "peripheral/peripheral_common_32bit.h"

/* Default definition used for all API dispatch functions */
#ifndef PLIB_INLINE_API
    #define PLIB_INLINE_API extern inline
#endif

/* Default definition used for all other functions */
#ifndef PLIB_INLINE
    #define PLIB_INLINE extern inline
#endif

typedef enum {

    CTMU_ID_0 = 0,
    CTMU_NUMBER_OF_MODULES

} CTMU_MODULE_ID;

typedef enum {

    CTMU_EDGE1 = 0x00,
    CTMU_EDGE2 = 0x01

} CTMU_EDGE_SELECT;

typedef enum {

    CTMU_EDGE_X_LEVEL_SENSITIVE = 0x00,
    CTMU_EDGE_X_EDGE_SENSITIVE = 0x01

} CTMU_EDGE_SENSITIVITY;

typedef enum {

    CTMU_EDGE_X_POSITIVE_EDGE = 0x01,
    CTMU_EDGE_X_NEGATIVE_EDGE = 0x00

} CTMU_EDGE_POLARITY;

typedef enum {

    CTMU_TRIGGERSOURCE_TMR1 = 0x00,
    CTMU_TRIGGERSOURCE_OC1 = 0x01,
    CTMU_TRIGGERSOURCE_IC1 = 0x0A,
    CTMU_TRIGGERSOURCE_IC2 = 0x0B,
    CTMU_TRIGGERSOURCE_IC3 = 0x0C,
    CTMU_TRIGGERSOURCE_CMP1 = 0x0D,
    CTMU_TRIGGERSOURCE_CMP2 = 0x0E,
    CTMU_TRIGGERSOURCE_CMP3 = 0x0F,
    CTMU_TRIGGERSOURCE_1 = 0x03,
    CTMU_TRIGGERSOURCE_2 = 0x02,
    CTMU_TRIGGERSOURCE_3 = 0x04,
    CTMU_TRIGGERSOURCE_4 = 0x05,
    CTMU_TRIGGERSOURCE_5 = 0x06,
    CTMU_TRIGGERSOURCE_6 = 0x07,
    CTMU_TRIGGERSOURCE_7 = 0x08,
    CTMU_TRIGGERSOURCE_8 = 0x09

} CTMU_TRIGGER_SOURCES;

typedef enum {

    CTMU_CURRENT_RANGE_1000XBASE = 0x00,
    CTMU_CURRENT_RANGE_BASE = 0x01,
    CTMU_CURRENT_RANGE_10XBASE = 0x02,
    CTMU_CURRENT_RANGE_100XBASE = 0x03

} CTMU_CURRENT_RANGE;

PLIB_INLINE SFR_TYPE* _CTMU_EDGE1_SENSITIVITY_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_EDGE1_POLARITY_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_EDGE1_SOURCE_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_EDGE2_STATUS_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_EDGE1_STATUS_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_EDGE2_SENSITIVITY_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_EDGE2_POLARITY_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_EDGE2_SOURCE_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_CTMU_ON_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_STOP_IN_IDLE_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_TIME_PULSE_GEN_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_ENABLE_EDGES_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_ENABLE_EDGE_SEQUENCING_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_CURRENT_DISCHARGE_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_ADC_TRIGGER_CONTROL_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_CURRENT_TRIM_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _CTMU_CURRENT_RANGE_VREG(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return &CTMUCON;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE1_SENSITIVITY_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG1MOD_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE1_POLARITY_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG1POL_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE1_SOURCE_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG1SEL_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE2_STATUS_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG2STAT_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE1_STATUS_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG1STAT_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE2_SENSITIVITY_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG2MOD_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE2_POLARITY_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG2POL_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE2_SOURCE_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG2SEL_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_CTMU_ON_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_ON_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_STOP_IN_IDLE_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_CTMUSIDL_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_TIME_PULSE_GEN_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_TGEN_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_ENABLE_EDGES_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDGEN_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_ENABLE_EDGE_SEQUENCING_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDGSEQEN_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_CURRENT_DISCHARGE_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_IDISSEN_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_ADC_TRIGGER_CONTROL_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_CTTRIG_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_CURRENT_TRIM_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_ITRIM_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_CURRENT_RANGE_MASK(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_IRNG_MASK;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE1_SENSITIVITY_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG1MOD_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE1_POLARITY_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG1POL_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE1_SOURCE_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG1SEL_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE2_STATUS_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG2STAT_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE1_STATUS_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG1STAT_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE2_SENSITIVITY_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG2MOD_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE2_POLARITY_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG2POL_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE2_SOURCE_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG2SEL_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_CTMU_ON_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_ON_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_STOP_IN_IDLE_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_CTMUSIDL_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_TIME_PULSE_GEN_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_TGEN_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_ENABLE_EDGES_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDGEN_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_ENABLE_EDGE_SEQUENCING_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDGSEQEN_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_CURRENT_DISCHARGE_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_IDISSEN_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_ADC_TRIGGER_CONTROL_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_CTTRIG_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_CURRENT_TRIM_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_ITRIM_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_CURRENT_RANGE_POS(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_IRNG_POSITION;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE1_SENSITIVITY_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG1MOD_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE1_POLARITY_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG1POL_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE1_SOURCE_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG1SEL_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE2_STATUS_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG2STAT_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE1_STATUS_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG1STAT_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE2_SENSITIVITY_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG2MOD_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE2_POLARITY_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG2POL_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_EDGE2_SOURCE_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDG2SEL_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_CTMU_ON_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_ON_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_STOP_IN_IDLE_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_CTMUSIDL_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_TIME_PULSE_GEN_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_TGEN_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_ENABLE_EDGES_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDGEN_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_ENABLE_EDGE_SEQUENCING_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_EDGSEQEN_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_CURRENT_DISCHARGE_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_IDISSEN_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_ADC_TRIGGER_CONTROL_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_CTTRIG_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_CURRENT_TRIM_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_ITRIM_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _CTMU_CURRENT_RANGE_LEN(CTMU_MODULE_ID i)
{
    switch (i) {
        case CTMU_ID_0 :
            return _CTMUCON_IRNG_LENGTH;
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

/* Section 2 - Feature variant inclusion */

#define PLIB_TEMPLATE PLIB_INLINE
#include "../templates/ctmu_EdgeSensitivity_Default.h"
#include "../templates/ctmu_EdgePolarity_Default.h"
#include "../templates/ctmu_EdgeTriggerSource_Default.h"
#include "../templates/ctmu_EdgeStatus_Default.h"
#include "../templates/ctmu_ModuleEnable_Default.h"
#include "../templates/ctmu_StopInIdle_Default.h"
#include "../templates/ctmu_TimePulseGeneration_Default.h"
#include "../templates/ctmu_EdgeEnable_Default.h"
#include "../templates/ctmu_EdgeSequencing_Default.h"
#include "../templates/ctmu_CurrentDischarge_Default.h"
#include "../templates/ctmu_AutomaticADCTrigger_Default.h"
#include "../templates/ctmu_CurrentTrim_Default.h"
#include "../templates/ctmu_CurrentRange_Default.h"

/* Section 3 - PLIB dispatch function definitions */

PLIB_INLINE_API void PLIB_CTMU_EdgeSensitivitySet(CTMU_MODULE_ID index, CTMU_EDGE_SELECT edgeNumber, CTMU_EDGE_SENSITIVITY edgeSense)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_EdgeSensitivitySet_Default(index, edgeNumber, edgeSense);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_CTMU_ExistsEdgeSensitivity(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            return CTMU_ExistsEdgeSensitivity_Default(index);
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_CTMU_EdgePolaritySet(CTMU_MODULE_ID index, CTMU_EDGE_SELECT edgeNumber, CTMU_EDGE_POLARITY edgePolarity)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_EdgePolaritySet_Default(index, edgeNumber, edgePolarity);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_CTMU_ExistsEdgePolarity(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            return CTMU_ExistsEdgePolarity_Default(index);
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_CTMU_EdgeTriggerSourceSelect(CTMU_MODULE_ID index, CTMU_EDGE_SELECT edgeNumber, CTMU_TRIGGER_SOURCES triggerSource)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_EdgeTriggerSourceSelect_Default(index, edgeNumber, triggerSource);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_CTMU_ExistsEdgeTriggerSource(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            return CTMU_ExistsEdgeTriggerSource_Default(index);
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API bool PLIB_CTMU_EdgeIsSet(CTMU_MODULE_ID index, CTMU_EDGE_SELECT edgeNumber)
{
    switch (index) {
        case CTMU_ID_0 :
            return CTMU_EdgeIsSet_Default(index, edgeNumber);
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_CTMU_EdgeSet(CTMU_MODULE_ID index, CTMU_EDGE_SELECT edgeNumber)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_EdgeSet_Default(index, edgeNumber);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_CTMU_ExistsEdgeStatus(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            return CTMU_ExistsEdgeStatus_Default(index);
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_CTMU_Disable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_Disable_Default(index);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void PLIB_CTMU_Enable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_Enable_Default(index);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_CTMU_ExistsModuleEnable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            return CTMU_ExistsModuleEnable_Default(index);
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_CTMU_StopInIdleDisable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_StopInIdleDisable_Default(index);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void PLIB_CTMU_StopInIdleEnable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_StopInIdleEnable_Default(index);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_CTMU_ExistsStopInIdle(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            return CTMU_ExistsStopInIdle_Default(index);
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_CTMU_TimePulseGenerationDisable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_TimePulseGenerationDisable_Default(index);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void PLIB_CTMU_TimePulseGenerationEnable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_TimePulseGenerationEnable_Default(index);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_CTMU_ExistsTimePulseGeneration(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            return CTMU_ExistsTimePulseGeneration_Default(index);
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_CTMU_EdgeDisable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_EdgeDisable_Default(index);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void PLIB_CTMU_EdgeEnable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_EdgeEnable_Default(index);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_CTMU_ExistsEdgeEnable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            return CTMU_ExistsEdgeEnable_Default(index);
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_CTMU_EdgeSequenceDisable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_EdgeSequenceDisable_Default(index);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void PLIB_CTMU_EdgeSequenceEnable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_EdgeSequenceEnable_Default(index);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_CTMU_ExistsEdgeSequencing(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            return CTMU_ExistsEdgeSequencing_Default(index);
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_CTMU_CurrentDischargeEnable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_CurrentDischargeEnable_Default(index);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void PLIB_CTMU_CurrentDischargeDisable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_CurrentDischargeDisable_Default(index);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_CTMU_ExistsCurrentDischarge(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            return CTMU_ExistsCurrentDischarge_Default(index);
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_CTMU_AutomaticADCTriggerDisable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_AutomaticADCTriggerDisable_Default(index);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void PLIB_CTMU_AutomaticADCTriggerEnable(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_AutomaticADCTriggerEnable_Default(index);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_CTMU_ExistsAutomaticADCTrigger(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            return CTMU_ExistsAutomaticADCTrigger_Default(index);
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_CTMU_CurrentTrimSet(CTMU_MODULE_ID index, int16_t currentTrim)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_CurrentTrimSet_Default(index, currentTrim);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_CTMU_ExistsCurrentTrim(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            return CTMU_ExistsCurrentTrim_Default(index);
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_CTMU_CurrentRangeSet(CTMU_MODULE_ID index, CTMU_CURRENT_RANGE currentRange)
{
    switch (index) {
        case CTMU_ID_0 :
            CTMU_CurrentRangeSet_Default(index, currentRange);
            break;
        case CTMU_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_CTMU_ExistsCurrentRange(CTMU_MODULE_ID index)
{
    switch (index) {
        case CTMU_ID_0 :
            return CTMU_ExistsCurrentRange_Default(index);
        case CTMU_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

#endif
