#ifndef _PLIB_CTMU_PROCESSOR_H
#define _PLIB_CTMU_PROCESSOR_H

#if defined(__PIC16F__)
    #include "ctmu_pic_other.h"

#elif defined(__18CXX)
    #include "ctmu_pic18.h"

#elif defined(_PIC18)
    #include "ctmu_pic18.h"

#elif defined(__PIC24F__)
    #include "ctmu_p24Fxxxx.h"

#elif defined(__PIC24H__)
    #include "ctmu_p24Hxxxx.h"

#elif defined(__dsPIC30F__)
    #include "ctmu_p30Fxxxx.h"

#elif defined(__dsPIC33E__)
    #include "ctmu_p33Exxxx.h"

#elif defined(__dsPIC33F__)
    #include "ctmu_p33Fxxxx.h"

#elif defined(__PIC32MX__)
    #include "ctmu_p32xxxx.h"

#elif defined(__PIC32MZ__)
    #include "ctmu_p32xxxx.h"


#else
    #error "Can't find header"

#endif

#endif//_PLIB_CTMU_PROCESSOR_H