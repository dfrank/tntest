/* Created by plibgen $Revision: 1.30 $ */

#ifndef _INT_P32MZ2048ECG064_H
#define _INT_P32MZ2048ECG064_H

/* Section 1 - Enumerate instances, define constants, VREGs */

#include <xc.h>
#include <stdbool.h>

#include "peripheral/peripheral_common_32bit.h"

/* Default definition used for all API dispatch functions */
#ifndef PLIB_INLINE_API
    #define PLIB_INLINE_API extern inline
#endif

/* Default definition used for all other functions */
#ifndef PLIB_INLINE
    #define PLIB_INLINE extern inline
#endif

typedef enum {

    INT_ID_0 = 0,
    INT_NUMBER_OF_MODULES

} INT_MODULE_ID;

typedef enum {

    INT_EXTERNAL_INT_SOURCE0 = 0x01,
    INT_EXTERNAL_INT_SOURCE1 = 0x02,
    INT_EXTERNAL_INT_SOURCE2 = 0x04,
    INT_EXTERNAL_INT_SOURCE3 = 0x08,
    INT_EXTERNAL_INT_SOURCE4 = 0x10

} INT_EXTERNAL_SOURCES;

typedef enum {

    INT_DISABLE_INTERRUPT = 0,
    INT_PRIORITY_LEVEL1 = 1,
    INT_PRIORITY_LEVEL2 = 2,
    INT_PRIORITY_LEVEL3 = 3,
    INT_PRIORITY_LEVEL4 = 4,
    INT_PRIORITY_LEVEL5 = 5,
    INT_PRIORITY_LEVEL6 = 6,
    INT_PRIORITY_LEVEL7 = 7

} INT_PRIORITY_LEVEL;

typedef enum {

    INT_SUBPRIORITY_LEVEL0 = 0x00,
    INT_SUBPRIORITY_LEVEL1 = 0x01,
    INT_SUBPRIORITY_LEVEL2 = 0x02,
    INT_SUBPRIORITY_LEVEL3 = 0x03

} INT_SUBPRIORITY_LEVEL;

typedef enum {

    INT_SOURCE_TIMER_CORE = 0,
    INT_SOURCE_SOFTWARE_0 = 1,
    INT_SOURCE_SOFTWARE_1 = 2,
    INT_SOURCE_EXTERNAL_0 = 3,
    INT_SOURCE_TIMER_1 = 4,
    INT_SOURCE_INPUT_CAPTURE_1_ERROR = 5,
    INT_SOURCE_INPUT_CAPTURE_1 = 6,
    INT_SOURCE_OUTPUT_COMPARE_1 = 7,
    INT_SOURCE_EXTERNAL_1 = 8,
    INT_SOURCE_TIMER_2 = 9,
    INT_SOURCE_INPUT_CAPTURE_2_ERROR = 10,
    INT_SOURCE_INPUT_CAPTURE_2 = 11,
    INT_SOURCE_OUTPUT_COMPARE_2 = 12,
    INT_SOURCE_EXTERNAL_2 = 13,
    INT_SOURCE_TIMER_3 = 14,
    INT_SOURCE_INPUT_CAPTURE_3_ERROR = 15,
    INT_SOURCE_INPUT_CAPTURE_3 = 16,
    INT_SOURCE_OUTPUT_COMPARE_3 = 17,
    INT_SOURCE_EXTERNAL_3 = 18,
    INT_SOURCE_TIMER_4 = 19,
    INT_SOURCE_INPUT_CAPTURE_4_ERROR = 20,
    INT_SOURCE_INPUT_CAPTURE_4 = 21,
    INT_SOURCE_OUTPUT_COMPARE_4 = 22,
    INT_SOURCE_EXTERNAL_4 = 23,
    INT_SOURCE_TIMER_5 = 24,
    INT_SOURCE_INPUT_CAPTURE_5_ERROR = 25,
    INT_SOURCE_INPUT_CAPTURE_5 = 26,
    INT_SOURCE_OUTPUT_COMPARE_5 = 27,
    INT_SOURCE_TIMER_6 = 28,
    INT_SOURCE_INPUT_CAPTURE_6_ERROR = 29,
    INT_SOURCE_INPUT_CAPTURE_6 = 30,
    INT_SOURCE_OUTPUT_COMPARE_6 = 31,
    INT_SOURCE_TIMER_7 = 32,
    INT_SOURCE_INPUT_CAPTURE_7_ERROR = 33,
    INT_SOURCE_INPUT_CAPTURE_7 = 34,
    INT_SOURCE_OUTPUT_COMPARE_7 = 35,
    INT_SOURCE_TIMER_8 = 36,
    INT_SOURCE_INPUT_CAPTURE_8_ERROR = 37,
    INT_SOURCE_INPUT_CAPTURE_8 = 38,
    INT_SOURCE_OUTPUT_COMPARE_8 = 39,
    INT_SOURCE_TIMER_9 = 40,
    INT_SOURCE_INPUT_CAPTURE_9_ERROR = 41,
    INT_SOURCE_INPUT_CAPTURE_9 = 42,
    INT_SOURCE_OUTPUT_COMPARE_9 = 43,
    INT_SOURCE_ADC_1 = 44,
    INT_SOURCE_ADC_1_DC1 = 46,
    INT_SOURCE_ADC_1_DC2 = 47,
    INT_SOURCE_ADC_1_DC3 = 48,
    INT_SOURCE_ADC_1_DC4 = 49,
    INT_SOURCE_ADC_1_DC5 = 50,
    INT_SOURCE_ADC_1_DC6 = 51,
    INT_SOURCE_ADC_1_DF1 = 52,
    INT_SOURCE_ADC_1_DF2 = 53,
    INT_SOURCE_ADC_1_DF3 = 54,
    INT_SOURCE_ADC_1_DF4 = 55,
    INT_SOURCE_ADC_1_DF5 = 56,
    INT_SOURCE_ADC_1_DF6 = 57,
    INT_SOURCE_ADC_1_DATA0 = 59,
    INT_SOURCE_ADC_1_DATA1 = 60,
    INT_SOURCE_ADC_1_DATA2 = 61,
    INT_SOURCE_ADC_1_DATA3 = 62,
    INT_SOURCE_ADC_1_DATA4 = 63,
    INT_SOURCE_ADC_1_DATA5 = 64,
    INT_SOURCE_ADC_1_DATA6 = 65,
    INT_SOURCE_ADC_1_DATA7 = 66,
    INT_SOURCE_ADC_1_DATA8 = 67,
    INT_SOURCE_ADC_1_DATA9 = 68,
    INT_SOURCE_ADC_1_DATA10 = 69,
    INT_SOURCE_ADC_1_DATA11 = 70,
    INT_SOURCE_ADC_1_DATA12 = 71,
    INT_SOURCE_ADC_1_DATA13 = 72,
    INT_SOURCE_ADC_1_DATA14 = 73,
    INT_SOURCE_ADC_1_DATA15 = 74,
    INT_SOURCE_ADC_1_DATA16 = 75,
    INT_SOURCE_ADC_1_DATA17 = 76,
    INT_SOURCE_ADC_1_DATA18 = 77,
    INT_SOURCE_ADC_1_DATA19 = 78,
    INT_SOURCE_ADC_1_DATA43 = 102,
    INT_SOURCE_ADC_1_DATA44 = 103,
    INT_SOURCE_CORE_PERF_COUNT = 104,
    INT_SOURCE_FAST_DEBUG = 105,
    INT_SOURCE_SYSTEM_BUS_PROTECTION = 106,
    INT_SOURCE_SPI_1_ERROR = 109,
    INT_SOURCE_SPI_1_RECEIVE = 110,
    INT_SOURCE_SPI_1_TRANSMIT = 111,
    INT_SOURCE_USART_1_ERROR = 112,
    INT_SOURCE_USART_1_RECEIVE = 113,
    INT_SOURCE_USART_1_TRANSMIT = 114,
    INT_SOURCE_I2C_1_BUS = 115,
    INT_SOURCE_I2C_1_SLAVE = 116,
    INT_SOURCE_I2C_1_MASTER = 117,
    INT_SOURCE_CHANGE_NOTICE_B = 119,
    INT_SOURCE_CHANGE_NOTICE_C = 120,
    INT_SOURCE_CHANGE_NOTICE_D = 121,
    INT_SOURCE_CHANGE_NOTICE_E = 122,
    INT_SOURCE_CHANGE_NOTICE_F = 123,
    INT_SOURCE_CHANGE_NOTICE_G = 124,
    INT_SOURCE_PARALLEL_PORT = 128,
    INT_SOURCE_PARALLEL_PORT_ERROR = 129,
    INT_SOURCE_COMPARATOR_1 = 130,
    INT_SOURCE_COMPARATOR_2 = 131,
    INT_SOURCE_USB_1 = 132,
    INT_SOURCE_USB_1_DMA = 133,
    INT_SOURCE_DMA_0 = 134,
    INT_SOURCE_DMA_1 = 135,
    INT_SOURCE_DMA_2 = 136,
    INT_SOURCE_DMA_3 = 137,
    INT_SOURCE_DMA_4 = 138,
    INT_SOURCE_DMA_5 = 139,
    INT_SOURCE_DMA_6 = 140,
    INT_SOURCE_DMA_7 = 141,
    INT_SOURCE_SPI_2_ERROR = 142,
    INT_SOURCE_SPI_2_RECEIVE = 143,
    INT_SOURCE_SPI_2_TRANSMIT = 144,
    INT_SOURCE_USART_2_ERROR = 145,
    INT_SOURCE_USART_2_RECEIVE = 146,
    INT_SOURCE_USART_2_TRANSMIT = 147,
    INT_SOURCE_ETH_1 = 153,
    INT_SOURCE_SPI_3_ERROR = 154,
    INT_SOURCE_SPI_3_RECEIVE = 155,
    INT_SOURCE_SPI_3_TRANSMIT = 156,
    INT_SOURCE_USART_3_ERROR = 157,
    INT_SOURCE_USART_3_RECEIVE = 158,
    INT_SOURCE_USART_3_TRANSMIT = 159,
    INT_SOURCE_I2C_3_BUS = 160,
    INT_SOURCE_I2C_3_SLAVE = 161,
    INT_SOURCE_I2C_3_MASTER = 162,
    INT_SOURCE_SPI_4_ERROR = 163,
    INT_SOURCE_SPI_4_RECEIVE = 164,
    INT_SOURCE_SPI_4_TRANSMIT = 165,
    INT_SOURCE_USART_4_ERROR = 170,
    INT_SOURCE_USART_4_RECEIVE = 171,
    INT_SOURCE_USART_4_TRANSMIT = 172,
    INT_SOURCE_I2C_4_BUS = 173,
    INT_SOURCE_I2C_4_SLAVE = 174,
    INT_SOURCE_I2C_5_MASTER = 184,
    INT_SOURCE_SPI_5_ERROR = 176,
    INT_SOURCE_SPI_5_RECEIVE = 177,
    INT_SOURCE_SPI_5_TRANSMIT = 178,
    INT_SOURCE_USART_5_ERROR = 179,
    INT_SOURCE_USART_5_RECEIVE = 180,
    INT_SOURCE_USART_5_TRANSMIT = 181,
    INT_SOURCE_I2C_5_BUS = 182,
    INT_SOURCE_I2C_5_SLAVE = 183,
    INT_SOURCE_USART_6_ERROR = 188,
    INT_SOURCE_USART_6_RECEIVE = 189,
    INT_SOURCE_USART_6_TRANSMIT = 190,
    INT_SOURCE_RTCC = 166,
    INT_SOURCE_FLASH_CONTROL = 167,
    INT_SOURCE_PREFETCH = 168,
    INT_SOURCE_SQI1 = 169,
    INT_SOURCE_I2C_4_MASTER = 175

} INT_SOURCE;

typedef enum {

    INT_VECTOR_CT = 0x00,
    INT_VECTOR_INT0 = 0x18,
    INT_VECTOR_T1 = 0x20,
    INT_VECTOR_IC1 = 0x30,
    INT_VECTOR_IC1_ERROR = 0x28,
    INT_VECTOR_OC1 = 0x38,
    INT_VECTOR_INT1 = 0x40,
    INT_VECTOR_T2 = 0x48,
    INT_VECTOR_IC2 = 0x58,
    INT_VECTOR_IC2_ERROR = 0x50,
    INT_VECTOR_OC2 = 0x60,
    INT_VECTOR_INT2 = 0x68,
    INT_VECTOR_T3 = 0x70,
    INT_VECTOR_IC3 = 0x80,
    INT_VECTOR_IC3_ERROR = 0x78,
    INT_VECTOR_OC3 = 0x88,
    INT_VECTOR_INT3 = 0x90,
    INT_VECTOR_T4 = 0x98,
    INT_VECTOR_IC4 = 0xA8,
    INT_VECTOR_IC4_ERROR = 0xA0,
    INT_VECTOR_OC4 = 0xB0,
    INT_VECTOR_INT4 = 0xB8,
    INT_VECTOR_T5 = 0xC0,
    INT_VECTOR_IC5 = 0xD0,
    INT_VECTOR_IC5_ERROR = 0xC8,
    INT_VECTOR_OC5 = 0xD8,
    INT_VECTOR_T6 = 0xE0,
    INT_VECTOR_IC6_ERROR = 0xE8,
    INT_VECTOR_IC6 = 0xF0,
    INT_VECTOR_OC6 = 0xF8,
    INT_VECTOR_T7 = 0x100,
    INT_VECTOR_IC7_ERROR = 0x108,
    INT_VECTOR_IC7 = 0x110,
    INT_VECTOR_OC7 = 0x118,
    INT_VECTOR_T8 = 0x120,
    INT_VECTOR_IC8_ERROR = 0x128,
    INT_VECTOR_IC8 = 0x130,
    INT_VECTOR_OC8 = 0X138,
    INT_VECTOR_T9 = 0x140,
    INT_VECTOR_IC9_ERROR = 0x148,
    INT_VECTOR_IC9 = 0x150,
    INT_VECTOR_OC9 = 0x158,
    INT_VECTOR_ADC1 = 0x160,
    INT_VECTOR_ADC1_DC1 = 0x170,
    INT_VECTOR_ADC1_DC2 = 0x178,
    INT_VECTOR_ADC1_DC3 = 0x180,
    INT_VECTOR_ADC1_DC4 = 0x188,
    INT_VECTOR_ADC1_DC5 = 0x190,
    INT_VECTOR_ADC1_DC6 = 0x198,
    INT_VECTOR_ADC1_DF1 = 0x1A0,
    INT_VECTOR_ADC1_DF2 = 0x1A8,
    INT_VECTOR_ADC1_DF3 = 0x1B0,
    INT_VECTOR_ADC1_DF4 = 0x1B8,
    INT_VECTOR_ADC1_DF5 = 0x1C0,
    INT_VECTOR_ADC1_DF6 = 0x1C8,
    INT_VECTOR_ADC1_DATA0 = 0x1D8,
    INT_VECTOR_ADC1_DATA1 = 0x1E0,
    INT_VECTOR_ADC1_DATA2 = 0x1E8,
    INT_VECTOR_ADC1_DATA3 = 0x1F0,
    INT_VECTOR_ADC1_DATA4 = 0x1F8,
    INT_VECTOR_ADC1_DATA5 = 0x200,
    INT_VECTOR_ADC1_DATA6 = 0x208,
    INT_VECTOR_ADC1_DATA7 = 0x210,
    INT_VECTOR_ADC1_DATA8 = 0x218,
    INT_VECTOR_ADC1_DATA9 = 0x220,
    INT_VECTOR_ADC1_DATA10 = 0x228,
    INT_VECTOR_ADC1_DATA11 = 0x230,
    INT_VECTOR_ADC1_DATA12 = 0x238,
    INT_VECTOR_ADC1_DATA13 = 0x240,
    INT_VECTOR_ADC1_DATA14 = 0x248,
    INT_VECTOR_ADC1_DATA15 = 0x250,
    INT_VECTOR_ADC1_DATA16 = 0x258,
    INT_VECTOR_ADC1_DATA17 = 0x260,
    INT_VECTOR_ADC1_DATA18 = 0x268,
    INT_VECTOR_ADC1_DATA19 = 0x270,
    INT_VECTOR_ADC1_DATA20 = 0x278,
    INT_VECTOR_ADC1_DATA21 = 0x280,
    INT_VECTOR_ADC1_DATA22 = 0X288,
    INT_VECTOR_ADC1_DATA23 = 0x290,
    INT_VECTOR_ADC1_DATA24 = 0x298,
    INT_VECTOR_ADC1_DATA25 = 0x2A0,
    INT_VECTOR_ADC1_DATA26 = 0x2A8,
    INT_VECTOR_ADC1_DATA27 = 0x2B0,
    INT_VECTOR_ADC1_DATA28 = 0x2B8,
    INT_VECTOR_ADC1_DATA29 = 0x2C0,
    INT_VECTOR_ADC1_DATA30 = 0x2C8,
    INT_VECTOR_ADC1_DATA31 = 0x2D0,
    INT_VECTOR_ADC1_DATA32 = 0x2D8,
    INT_VECTOR_ADC1_DATA33 = 0x2E0,
    INT_VECTOR_ADC1_DATA34 = 0x2E8,
    INT_VECTOR_ADC1_DATA35 = 0x2F0,
    INT_VECTOR_ADC1_DATA36 = 0x2F8,
    INT_VECTOR_ADC1_DATA37 = 0x300,
    INT_VECTOR_ADC1_DATA38 = 0x308,
    INT_VECTOR_ADC1_DATA39 = 0x310,
    INT_VECTOR_ADC1_DATA40 = 0x318,
    INT_VECTOR_ADC1_DATA41 = 0x320,
    INT_VECTOR_ADC1_DATA42 = 0x328,
    INT_VECTOR_ADC1_DATA43 = 0x330,
    INT_VECTOR_ADC1_DATA44 = 0x338,
    INT_VECTOR_CORE_PERF_COUNT = 0x340,
    INT_VECTOR_CORE_FAST_DEBUG_CHANNEL = 0x348,
    INT_VECTOR_CORE_SYSTEM_BUS_PROTECTION = 0x350,
    INT_VECTOR_CRYPTO = 0x358,
    INT_VECTOR_SPI1_FAULT = 0x368,
    INT_VECTOR_SPI1_RX = 0x370,
    INT_VECTOR_SPI1_TX = 0x378,
    INT_VECTOR_UART1_FAULT = 0x380,
    INT_VECTOR_UART1_RX = 0x388,
    INT_VECTOR_UART1_TX = 0x390,
    INT_VECTOR_I2C1_BUS = 0x398,
    INT_VECTOR_I2C1_SLAVE = 0x3A0,
    INT_VECTOR_I2C1_MASTER = 0x3A8,
    INT_VECTOR_SPI2_FAULT = 0x470,
    INT_VECTOR_SPI2_RX = 0x478,
    INT_VECTOR_SPI2_TX = 0x480,
    INT_VECTOR_UART2_FAULT = 0x488,
    INT_VECTOR_UART2_RX = 0x490,
    INT_VECTOR_UART2_TX = 0x498,
    INT_VECTOR_I2C2_BUS = 0x4A0,
    INT_VECTOR_I2C2_SLAVE = 0x4A8,
    INT_VECTOR_I2C2_MASTER = 0x4B0,
    INT_VECTOR_SPI3_FAULT = 0x4D0,
    INT_VECTOR_SPI3_RX = 0x4D8,
    INT_VECTOR_SPI3_TX = 0x4E0,
    INT_VECTOR_UART3_FAULT = 0x4E8,
    INT_VECTOR_UART3_RX = 0x4F0,
    INT_VECTOR_UART3_TX = 0x4F8,
    INT_VECTOR_I2C3_BUS = 0x500,
    INT_VECTOR_I2C3_SLAVE = 0x508,
    INT_VECTOR_I2C3_MASTER = 0x510,
    INT_VECTOR_SPI4_FAULT = 0x518,
    INT_VECTOR_SPI4_RX = 0x520,
    INT_VECTOR_SPI4_TX = 0x528,
    INT_VECTOR_UART4_FAULT = 0x550,
    INT_VECTOR_UART4_RX = 0x558,
    INT_VECTOR_I2C4_BUS = 0x568,
    INT_VECTOR_I2C4_SLAVE = 0x570,
    INT_VECTOR_I2C4_MASTER = 0x578,
    INT_VECTOR_SPI5_FAULT = 0x580,
    INT_VECTOR_SPI5_RX = 0x588,
    INT_VECTOR_SPI5_TX = 0x590,
    INT_VECTOR_UART5_FAULT = 0x598,
    INT_VECTOR_UART5_RX = 0x5A0,
    INT_VECTOR_UART5_TX = 0x5A8,
    INT_VECTOR_I2C5_BUS = 0x5B0,
    INT_VECTOR_I2C5_SLAVE = 0x5B8,
    INT_VECTOR_I2C5_MASTER = 0x5C0,
    INT_VECTOR_SPI6_FAULT = 0x5C8,
    INT_VECTOR_SPI6_RX = 0x5D0,
    INT_VECTOR_SPI6_TX = 0x5D8,
    INT_VECTOR_UART6_FAULT = 0x5E0,
    INT_VECTOR_UART6_RX = 0x5E8,
    INT_VECTOR_UART6_TX = 0x5F0,
    INT_VECTOR_CHANGE_NOTICE_A = 0x3B0,
    INT_VECTOR_CHANGE_NOTICE_B = 0x3B8,
    INT_VECTOR_CHANGE_NOTICE_C = 0x3C0,
    INT_VECTOR_CHANGE_NOTICE_D = 0X3C8,
    INT_VECTOR_CHANGE_NOTICE_E = 0x3D0,
    INT_VECTOR_CHANGE_NOTICE_F = 0x3D8,
    INT_VECTOR_CHANGE_NOTICE_G = 0x3E0,
    INT_VECTOR_CHANGE_NOTICE_H = 0x3E8,
    INT_VECTOR_CHANGE_NOTICE_J = 0x3F0,
    INT_VECTOR_CHANGE_NOTICE_K = 0x3F8,
    INT_VECTOR_PMP = 0x400,
    INT_VECTOR_PMP_ERROR = 0x408,
    INT_VECTOR_USB1 = 0x420,
    INT_VECTOR_USB1_DMA = 0x428,
    INT_VECTOR_RTCC = 0x530,
    INT_VECTOR_FLASH = 0x538,
    INT_VECTOR_SQI1 = 0x548,
    INT_VECTOR_CMP1 = 0x410,
    INT_VECTOR_CMP2 = 0x418,
    INT_VECTOR_DMA0 = 0x430,
    INT_VECTOR_DMA1 = 0x438,
    INT_VECTOR_DMA2 = 0x440,
    INT_VECTOR_DMA3 = 0x448,
    INT_VECTOR_DMA4 = 0x450,
    INT_VECTOR_DMA5 = 0x458,
    INT_VECTOR_DMA6 = 0x460,
    INT_VECTOR_DMA7 = 0x468,
    INT_VECTOR_CAN1 = 0x4B8,
    INT_VECTOR_CAN2 = 0x4C0,
    INT_VECTOR_ETH = 0x4C8

} INT_VECTOR;

typedef enum {

    INT_VECTOR_SPACING_0 = 0x00,
    INT_VECTOR_SPACING_8 = 0x01,
    INT_VECTOR_SPACING_16 = 0x02,
    INT_VECTOR_SPACING_32 = 0x04,
    INT_VECTOR_SPACING_64 = 0x08,
    INT_VECTOR_SPACING_128 = 0x10,
    INT_VECTOR_SPACING_256 = 0x20,
    INT_VECTOR_SPACING_512 = 0x40

} INT_VECTOR_SPACING;

typedef enum {

    INT_SHADOW_REGISTER_0 = 0x00,
    INT_SHADOW_REGISTER_1 = 0x01,
    INT_SHADOW_REGISTER_2 = 0x02,
    INT_SHADOW_REGISTER_3 = 0x03,
    INT_SHADOW_REGISTER_4 = 0x04,
    INT_SHADOW_REGISTER_5 = 0x05,
    INT_SHADOW_REGISTER_6 = 0x06,
    INT_SHADOW_REGISTER_7 = 0x07,
    INT_SHADOW_REGISTER_8 = 0x08,
    INT_SHADOW_REGISTER_9 = 0x09,
    INT_SHADOW_REGISTER_10 = 0x0A,
    INT_SHADOW_REGISTER_11 = 0x0B,
    INT_SHADOW_REGISTER_12 = 0x0C,
    INT_SHADOW_REGISTER_13 = 0x0D,
    INT_SHADOW_REGISTER_14 = 0x0E,
    INT_SHADOW_REGISTER_15 = 0x0F

} INT_SHADOW_REGISTER;

typedef enum {

    INT_TRAP_SOURCE_NONE

} INT_TRAP_SOURCE;

PLIB_INLINE SFR_TYPE* _INT_INT_EXT_INT0_VREG(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return &INTCON;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _INT_PROXIMITY_TIMER_ENABLE_VREG(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return &INTCON;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _INT_SINGLE_MULTI_VECTOR_VREG(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return &INTCON;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _INT_VECTOR_BASE_ADDRESS_VREG(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return &INTCON;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _INT_SHADOW_SET_VREG(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return &PRISS;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _INT_PRIORITY_SHADOW_REGISTER_SELECT_VREG(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return &PRISS;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _INT_LATEST_INT_VECTOR_VREG(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return &INTSTAT;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _INT_LATEST_INT_PRIORITY_VREG(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return &INTSTAT;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _INT_INT_FLAG_STATUS_0_VREG(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return &IFS0;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _INT_INT_FLAG_STATUS_2_VREG(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return &IFS0;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _INT_INT_FLAG_STATUS_1_VREG(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return &IFS1;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _INT_INT_ENABLE_CONTROL_0_VREG(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return &IEC0;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _INT_INT_ENABLE_CONTROL_1_VREG(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return &IEC1;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _INT_INT_PRIORITY_CONTROL_0_VREG(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return &IPC0;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_TYPE* _INT_VECTOR_ADDRESS_OFFSET_SET_VREG(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return &OFF000;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_TYPE*)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_EXT_INT0_MASK(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTCON_INT0EP_MASK;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_PROXIMITY_TIMER_ENABLE_MASK(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTCON_TPC_MASK;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_SINGLE_MULTI_VECTOR_MASK(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTCON_MVEC_MASK;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_VECTOR_BASE_ADDRESS_MASK(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTCON_MVEC_MASK;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_SHADOW_SET_MASK(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _PRISS_SS0_MASK;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_PRIORITY_SHADOW_REGISTER_SELECT_MASK(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _PRISS_PRI1SS_MASK;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_LATEST_INT_VECTOR_MASK(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTSTAT_SIRQ_MASK;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_LATEST_INT_PRIORITY_MASK(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTSTAT_SRIPL_MASK;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_FLAG_STATUS_0_MASK(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IFS0_CTIF_MASK;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_FLAG_STATUS_2_MASK(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IFS0_IC4EIF_MASK;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_FLAG_STATUS_1_MASK(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IFS1_T7IF_MASK;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_ENABLE_CONTROL_0_MASK(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IEC0_CTIE_MASK;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_ENABLE_CONTROL_1_MASK(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IEC1_T7IE_MASK;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_PRIORITY_CONTROL_0_MASK(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IPC0_CTIS_MASK;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_VECTOR_ADDRESS_OFFSET_SET_MASK(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _OFF000_VOFF_MASK;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_EXT_INT0_POS(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTCON_INT0EP_POSITION;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_PROXIMITY_TIMER_ENABLE_POS(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTCON_TPC_POSITION;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_SINGLE_MULTI_VECTOR_POS(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTCON_MVEC_POSITION;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_VECTOR_BASE_ADDRESS_POS(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTCON_MVEC_POSITION;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_SHADOW_SET_POS(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _PRISS_SS0_POSITION;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_PRIORITY_SHADOW_REGISTER_SELECT_POS(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _PRISS_PRI1SS_POSITION;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_LATEST_INT_VECTOR_POS(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTSTAT_SIRQ_POSITION;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_LATEST_INT_PRIORITY_POS(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTSTAT_SRIPL_POSITION;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_FLAG_STATUS_0_POS(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IFS0_CTIF_POSITION;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_FLAG_STATUS_2_POS(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IFS0_IC4EIF_POSITION;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_FLAG_STATUS_1_POS(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IFS1_T7IF_POSITION;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_ENABLE_CONTROL_0_POS(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IEC0_CTIE_POSITION;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_ENABLE_CONTROL_1_POS(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IEC1_T7IE_POSITION;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_PRIORITY_CONTROL_0_POS(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IPC0_CTIS_POSITION;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_VECTOR_ADDRESS_OFFSET_SET_POS(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _OFF000_VOFF_POSITION;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_EXT_INT0_LEN(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTCON_INT0EP_LENGTH;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_PROXIMITY_TIMER_ENABLE_LEN(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTCON_TPC_LENGTH;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_SINGLE_MULTI_VECTOR_LEN(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTCON_MVEC_LENGTH;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_VECTOR_BASE_ADDRESS_LEN(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTCON_MVEC_LENGTH;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_SHADOW_SET_LEN(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _PRISS_SS0_LENGTH;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_PRIORITY_SHADOW_REGISTER_SELECT_LEN(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _PRISS_PRI1SS_LENGTH;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_LATEST_INT_VECTOR_LEN(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTSTAT_SIRQ_LENGTH;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_LATEST_INT_PRIORITY_LEN(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _INTSTAT_SRIPL_LENGTH;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_FLAG_STATUS_0_LEN(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IFS0_CTIF_LENGTH;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_FLAG_STATUS_2_LEN(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IFS0_IC4EIF_LENGTH;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_FLAG_STATUS_1_LEN(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IFS1_T7IF_LENGTH;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_ENABLE_CONTROL_0_LEN(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IEC0_CTIE_LENGTH;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_ENABLE_CONTROL_1_LEN(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IEC1_T7IE_LENGTH;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_INT_PRIORITY_CONTROL_0_LEN(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _IPC0_CTIS_LENGTH;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

PLIB_INLINE SFR_DATA _INT_VECTOR_ADDRESS_OFFSET_SET_LEN(INT_MODULE_ID i)
{
    switch (i) {
        case INT_ID_0 :
            return _OFF000_VOFF_LENGTH;
        case INT_NUMBER_OF_MODULES :
        default :
            return (SFR_DATA)-1;
    }
}

/* Section 2 - Feature variant inclusion */

#define PLIB_TEMPLATE PLIB_INLINE
#include "../templates/int_SingleVectorShadowSet_Default.h"
#include "../templates/int_VectorSelect_Default.h"
#include "../templates/int_ProximityTimerEnable_Default.h"
#include "../templates/int_ProximityTimerControl_Default.h"
#include "../templates/int_ExternalINTEdgeSelect_Default.h"
#include "../templates/int_INTCPUPriority_Default.h"
#include "../templates/int_INTCPUVector_Default.h"
#include "../templates/int_SourceFlag_Default.h"
#include "../templates/int_SourceControl_Default.h"
#include "../templates/int_VectorPriority_Default.h"
#include "../templates/int_CPUCurrentPriorityLevel_Default.h"
#include "../templates/int_EnableControl_PIC32.h"
#include "../templates/int_INTNesting_Unsupported.h"
#include "../templates/int_TrapSource_Unsupported.h"
#include "../templates/int_AlternateVectorTable_Unsupported.h"
#include "../templates/int_PeripheralControl_Unsupported.h"
#include "../templates/int_HighPriority_Unsupported.h"
#include "../templates/int_LowPriority_Unsupported.h"
#include "../templates/int_Priority_Unsupported.h"
#include "../templates/int_ShadowRegisterAssign_Default.h"
#include "../templates/int_VariableOffset_Default.h"

/* Section 3 - PLIB dispatch function definitions */

PLIB_INLINE_API bool PLIB_INT_ExistsSingleVectorShadowSet(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsSingleVectorShadowSet_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_INT_SingleVectorShadowSetDisable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_SingleVectorShadowSetDisable_Default(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void PLIB_INT_SingleVectorShadowSetEnable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_SingleVectorShadowSetEnable_Default(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_INT_ExistsVectorSelect(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsVectorSelect_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_INT_MultiVectorSelect(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_MultiVectorSelect_Default(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void PLIB_INT_SingleVectorSelect(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_SingleVectorSelect_Default(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_INT_ExistsProximityTimerEnable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsProximityTimerEnable_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_INT_ProximityTimerEnable(INT_MODULE_ID index, INT_PRIORITY_LEVEL priority)
{
    switch (index) {
        case INT_ID_0 :
            INT_ProximityTimerEnable_Default(index, priority);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void PLIB_INT_ProximityTimerDisable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_ProximityTimerDisable_Default(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_INT_ExistsProximityTimerControl(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsProximityTimerControl_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_INT_ProximityTimerSet(INT_MODULE_ID index, uint32_t timerreloadvalue)
{
    switch (index) {
        case INT_ID_0 :
            INT_ProximityTimerSet_Default(index, timerreloadvalue);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API uint32_t PLIB_INT_ProximityTimerGet(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ProximityTimerGet_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (uint32_t)0;
    }
}

PLIB_INLINE_API bool PLIB_INT_ExistsExternalINTEdgeSelect(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsExternalINTEdgeSelect_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_INT_ExternalRisingEdgeSelect(INT_MODULE_ID index, INT_EXTERNAL_SOURCES source)
{
    switch (index) {
        case INT_ID_0 :
            INT_ExternalRisingEdgeSelect_Default(index, source);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void PLIB_INT_ExternalFallingEdgeSelect(INT_MODULE_ID index, INT_EXTERNAL_SOURCES source)
{
    switch (index) {
        case INT_ID_0 :
            INT_ExternalFallingEdgeSelect_Default(index, source);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_INT_ExistsINTCPUPriority(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsINTCPUPriority_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API INT_PRIORITY_LEVEL PLIB_INT_PriorityGet(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_PriorityGet_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (INT_PRIORITY_LEVEL)0;
    }
}

PLIB_INLINE_API bool PLIB_INT_ExistsINTCPUVector(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsINTCPUVector_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API INT_VECTOR PLIB_INT_VectorGet(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_VectorGet_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (INT_VECTOR)0;
    }
}

PLIB_INLINE_API bool PLIB_INT_ExistsSourceFlag(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsSourceFlag_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API bool PLIB_INT_SourceFlagGet(INT_MODULE_ID index, INT_SOURCE source)
{
    switch (index) {
        case INT_ID_0 :
            return INT_SourceFlagGet_Default(index, source);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_INT_SourceFlagSet(INT_MODULE_ID index, INT_SOURCE source)
{
    switch (index) {
        case INT_ID_0 :
            INT_SourceFlagSet_Default(index, source);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void PLIB_INT_SourceFlagClear(INT_MODULE_ID index, INT_SOURCE source)
{
    switch (index) {
        case INT_ID_0 :
            INT_SourceFlagClear_Default(index, source);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_INT_ExistsSourceControl(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsSourceControl_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_INT_SourceEnable(INT_MODULE_ID index, INT_SOURCE source)
{
    switch (index) {
        case INT_ID_0 :
            INT_SourceEnable_Default(index, source);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void PLIB_INT_SourceDisable(INT_MODULE_ID index, INT_SOURCE source)
{
    switch (index) {
        case INT_ID_0 :
            INT_SourceDisable_Default(index, source);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_INT_SourceIsEnabled(INT_MODULE_ID index, INT_SOURCE source)
{
    switch (index) {
        case INT_ID_0 :
            return INT_SourceIsEnabled_Default(index, source);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API bool PLIB_INT_ExistsVectorPriority(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsVectorPriority_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_INT_VectorPrioritySet(INT_MODULE_ID index, INT_VECTOR vector, INT_PRIORITY_LEVEL priority)
{
    switch (index) {
        case INT_ID_0 :
            INT_VectorPrioritySet_Default(index, vector, priority);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API INT_PRIORITY_LEVEL PLIB_INT_VectorPriorityGet(INT_MODULE_ID index, INT_VECTOR vector)
{
    switch (index) {
        case INT_ID_0 :
            return INT_VectorPriorityGet_Default(index, vector);
        case INT_NUMBER_OF_MODULES :
        default :
            return (INT_PRIORITY_LEVEL)0;
    }
}

PLIB_INLINE_API void PLIB_INT_VectorSubPrioritySet(INT_MODULE_ID index, INT_VECTOR vector, INT_SUBPRIORITY_LEVEL subPriority)
{
    switch (index) {
        case INT_ID_0 :
            INT_VectorSubPrioritySet_Default(index, vector, subPriority);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API INT_SUBPRIORITY_LEVEL PLIB_INT_VectorSubPriorityGet(INT_MODULE_ID index, INT_VECTOR vector)
{
    switch (index) {
        case INT_ID_0 :
            return INT_VectorSubPriorityGet_Default(index, vector);
        case INT_NUMBER_OF_MODULES :
        default :
            return (INT_SUBPRIORITY_LEVEL)0;
    }
}

PLIB_INLINE_API bool PLIB_INT_ExistsCPUCurrentPriorityLevel(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsCPUCurrentPriorityLevel_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API INT_PRIORITY_LEVEL PLIB_INT_CPUCurrentPriorityLevelGet(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_CPUCurrentPriorityLevelGet_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (INT_PRIORITY_LEVEL)0;
    }
}

PLIB_INLINE_API bool PLIB_INT_ExistsEnableControl(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsEnableControl_PIC32(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_INT_Enable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_Enable_PIC32(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void PLIB_INT_Disable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_Disable_PIC32(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_INT_IsEnabled(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_IsEnabled_PIC32(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_INT_SetState(INT_MODULE_ID index, INT_STATE_GLOBAL interrupt_state)
{
    switch (index) {
        case INT_ID_0 :
            INT_SetState_PIC32(index, interrupt_state);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API INT_STATE_GLOBAL PLIB_INT_GetStateAndDisable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_GetStateAndDisable_PIC32(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (INT_STATE_GLOBAL)0;
    }
}

PLIB_INLINE_API bool _PLIB_UNSUPPORTED PLIB_INT_ExistsINTNesting(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsINTNesting_Unsupported(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void _PLIB_UNSUPPORTED PLIB_INT_NestingEnable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_NestingEnable_Unsupported(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void _PLIB_UNSUPPORTED PLIB_INT_NestingDisable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_NestingDisable_Unsupported(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool _PLIB_UNSUPPORTED PLIB_INT_ExistsTrapSource(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsTrapSource_Unsupported(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API bool _PLIB_UNSUPPORTED PLIB_INT_TrapSourceFlagGet(INT_MODULE_ID index, INT_TRAP_SOURCE trapSource)
{
    switch (index) {
        case INT_ID_0 :
            return INT_TrapSourceFlagGet_Unsupported(index, trapSource);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void _PLIB_UNSUPPORTED PLIB_INT_TrapSourceFlagClear(INT_MODULE_ID index, INT_TRAP_SOURCE trapsource)
{
    switch (index) {
        case INT_ID_0 :
            INT_TrapSourceFlagClear_Unsupported(index, trapsource);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool _PLIB_UNSUPPORTED PLIB_INT_ExistsAlternateVectorTable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsAlternateVectorTable_Unsupported(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void _PLIB_UNSUPPORTED PLIB_INT_AlternateVectorTableSelect(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_AlternateVectorTableSelect_Unsupported(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void _PLIB_UNSUPPORTED PLIB_INT_StandardVectorTableSelect(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_StandardVectorTableSelect_Unsupported(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool _PLIB_UNSUPPORTED PLIB_INT_ExistsPeripheralControl(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsPeripheralControl_Unsupported(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void _PLIB_UNSUPPORTED PLIB_INT_PeripheralsEnable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_PeripheralsEnable_Unsupported(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void _PLIB_UNSUPPORTED PLIB_INT_PeripheralsDisable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_PeripheralsDisable_Unsupported(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool _PLIB_UNSUPPORTED PLIB_INT_ExistsHighPriority(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsHighPriority_Unsupported(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void _PLIB_UNSUPPORTED PLIB_INT_PriorityHighEnable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_PriorityHighEnable_Unsupported(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void _PLIB_UNSUPPORTED PLIB_INT_PriorityHighDisable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_PriorityHighDisable_Unsupported(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool _PLIB_UNSUPPORTED PLIB_INT_ExistsLowPriority(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsLowPriority_Unsupported(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void _PLIB_UNSUPPORTED PLIB_INT_PriorityLowEnable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_PriorityLowEnable_Unsupported(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void _PLIB_UNSUPPORTED PLIB_INT_PriorityLowDisable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_PriorityLowDisable_Unsupported(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool _PLIB_UNSUPPORTED PLIB_INT_ExistsPriority(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsPriority_Unsupported(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void _PLIB_UNSUPPORTED PLIB_INT_PriorityEnable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_PriorityEnable_Unsupported(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API void _PLIB_UNSUPPORTED PLIB_INT_PriorityDisable(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            INT_PriorityDisable_Unsupported(index);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API bool PLIB_INT_ExistsShadowRegisterAssign(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsShadowRegisterAssign_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_INT_ShadowRegisterAssign(INT_MODULE_ID index, INT_PRIORITY_LEVEL priority, INT_SHADOW_REGISTER shadowRegister)
{
    switch (index) {
        case INT_ID_0 :
            INT_ShadowRegisterAssign_Default(index, priority, shadowRegister);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API INT_SHADOW_REGISTER PLIB_INT_ShadowRegisterGet(INT_MODULE_ID index, INT_PRIORITY_LEVEL priority)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ShadowRegisterGet_Default(index, priority);
        case INT_NUMBER_OF_MODULES :
        default :
            return (INT_SHADOW_REGISTER)0;
    }
}

PLIB_INLINE_API bool PLIB_INT_ExistsVariableOffset(INT_MODULE_ID index)
{
    switch (index) {
        case INT_ID_0 :
            return INT_ExistsVariableOffset_Default(index);
        case INT_NUMBER_OF_MODULES :
        default :
            return (bool)0;
    }
}

PLIB_INLINE_API void PLIB_INT_VariableVectorOffsetSet(INT_MODULE_ID index, INT_VECTOR vector, uint32_t offset)
{
    switch (index) {
        case INT_ID_0 :
            INT_VariableVectorOffsetSet_Default(index, vector, offset);
            break;
        case INT_NUMBER_OF_MODULES :
        default :
            break;
    }
}

PLIB_INLINE_API uint32_t PLIB_INT_VariableVectorOffsetGet(INT_MODULE_ID index, INT_SOURCE source)
{
    switch (index) {
        case INT_ID_0 :
            return INT_VariableVectorOffsetGet_Default(index, source);
        case INT_NUMBER_OF_MODULES :
        default :
            return (uint32_t)0;
    }
}

#endif
