/*******************************************************************************
 Microchip Bluetooth Stack - Extra Files

  Company:
    Searan LLC.

  File Name:
    extra/patch.h

  Summary:
    Bluetooth API Library interface to the CSR driver.

  Description:
    This is a portion of the API interface to the Bluetooth stack.  Other header files are
	grouped in the section under the CDBT master directory. 
	
*******************************************************************************/
// DOM-IGNORE-BEGIN
/*******************************************************************************
* Source contains proprietary and confidential information of SEARAN LLC.
* May not be used or disclosed to any other party except in accordance
* with a license from SEARAN LLC or Microchip Technology Inc.
* Copyright (c) 2011, 2012 SEARAN LLC. All Rights Reserved.
*
*
*******************************************************************************/
// DOM-IGNORE-END

#ifndef __BTX_CSR_H_INCLUDED__
#define __BTX_CSR_H_INCLUDED__

// Select Variable definitions
#define CSR_VARID_CACHED_TEMPERATURE    0x2872
#define CSR_VARID_RSSI_ACL              0x301d
#define CSR_VARID_PIO                   0x681f
#define CSR_VARID_PIO_DIRECTION_MASK    0x681e
#define CSR_VARID_PIO_PROTECT_MASK      0x2823
#define CSR_VARID_STREAM_GET_SOURCE     0x505a
#define CSR_VARID_STREAM_GET_SINK       0x505b
#define CSR_VARID_ENABLE_SCO_STREAMS    0x4876
#define CSR_VARID_STREAM_CLOSE_SOURCE   0x486b
#define CSR_VARID_STREAM_CLOSE_SINK     0x486c
#define CSR_VARID_STREAM_CONFIGURE      0x505c
#define CSR_VARID_STREAM_CONNECT        0x505e
#define CSR_VARID_MAP_SCO_AUDIO         0x506a

// Select PS key definitions
#define PSKEY_BDADDR                          0x0001
#define PSKEY_ANA_FREQ                        0x01fe
#define PSKEY_VM_DISABLE                      0x025d
#define PSKEY_DEEP_SLEEP_STATE                0x0229
#define PSKEY_DEEP_SLEEP_USE_EXTERNAL_CLOCK   0x03c3
#define PSKEY_DEEP_SLEEP_CLEAR_RTS            0x0252
#define PSKEY_DEEP_SLEEP_WAKE_CTS             0x023c
#define PSKEY_CLOCK_REQUEST_ENABLE            0x0246
#define PSKEY_PIO_DEEP_SLEEP_EITHER_LEVEL     0x21bd
#define PSKEY_UART_BAUDRATE                   0x01be
#define PSKEY_UART_CONFIG_BCSP                0x01bf
#define PSKEY_UART_BITRATE                    0x01ea
#define PSKEY_HCI_NOP_DISABLE                 0x00f2
#define PSKEY_HOST_INTERFACE                  0x01f9
#define PSKEY_UART_CONFIG_H4                  0x01c0
#define PSKEY_UART_CONFIG_H5                  0x01c1
#define PSKEY_UART_TX_WINDOW_SIZE             0x01c6
#define PSKEY_HOSTIO_UART_RESET_TIMEOUT       0x01a4
#define PSKEY_HOSTIO_MAP_SCO_PCM              0x01ab
#define PSKEY_DIGITAL_AUDIO_CONFIG            0x01d9
#define PSKEY_DIGITAL_AUDIO_BITS_PER_SAMPLE   0x01db
#define PSKEY_PCM_PULL_CONTROL                0x01e2

// BCCMD GETRESP status codes
#define GETRESP_OK                   0x0000
#define GETRESP_NO_SUCH_VARID        0x0001
#define GETRESP_TOO_BIG              0x0002
#define GETRESP_NO_VALUE             0x0003
#define GETRESP_BAD_REQ              0x0004
#define GETRESP_NO_ACCESS            0x0005
#define GETRESP_READ_ONLY            0x0006
#define GETRESP_WRITE_ONLY           0x0007
#define GETRESP_ERROR                0x0008
#define GETRESP_PERMISSION_DENIED    0x0009

#define PS_DEFAULT                   0x0000
#define PS_RAM                       0x0008
#define PS_I                         0x0001
#define PS_F                         0x0002
#define PS_ROM                       0x0004

#define CSR_SRC_PCM                  1
#define CSR_SRC_I2S                  2
#define CSR_SRC_ADC                  3
#define CSR_SRC_FM                   4
#define CSR_SRC_SPDIF                5
#define CSR_SRC_MIC                  6
#define CSR_SRC_L2CAP                7
#define CSR_SRC_FASTPIPE             8
#define CSR_SRC_SCO                  9

#define CSR_SNK_PCM                  1
#define CSR_SNK_I2S                  2
#define CSR_SNK_ADC                  3
#define CSR_SNK_FM                   4
#define CSR_SNK_SPDIF                5
#define CSR_SNK_L2CAP                7
#define CSR_SNK_FASTPIPE             8
#define CSR_SNK_SCO                  9

// Macros for defining lists of PS values for use with btx_csr_set_ps_vars.
#define SET_PS_VALUE_UINT16(key, value) key, 1, value
#define SET_PS_VALUE_UINT32(key, value) key, 2, (uint16_t)(value >> 16), (uint16_t)(value &0xFFFF)
#define SET_PS_VALUE_BDADDR(key, m, l)  key, 4, (uint16_t)((l >> 16) & 0xFF), (uint16_t)(l &0xFFFF), (uint16_t)((l >> 24) & 0xFF), (uint16_t)(m &0xFFFF)


typedef struct _btx_csr_autobaud_buffer_t btx_csr_autobaud_buffer_t;

typedef void (*btx_csr_autobaud_callback_fp)(bt_bool success, btx_csr_autobaud_buffer_t* buffer);

struct _btx_csr_autobaud_buffer_t
{
	bt_byte* recv_buffer;
	bt_uint  recv_buffer_len;
	btx_csr_autobaud_callback_fp callback;
	void* callback_param;
};

typedef struct _btx_csr_script_t
{
	const bt_byte* const * packets;
	bt_int packet_count;
} btx_csr_script_t;

typedef struct _btx_csr_exec_script_buffer_t btx_csr_exec_script_buffer_t;

typedef void (*btx_csr_exec_script_callback_fp)(bt_bool success, btx_csr_exec_script_buffer_t* buffer);

struct _btx_csr_exec_script_buffer_t
{
	const btx_csr_script_t* script;
	btx_csr_exec_script_callback_fp callback;
	void* callback_param;
	bt_int current_packet;
};

typedef struct _btx_csr_bccmd_header_s
{
	bt_uint type;
	bt_uint len;
	bt_uint seq_no;
	bt_uint var_id;
	bt_uint status;
	bt_byte* payload;
} btx_csr_bccmd_header_t;

typedef struct _btx_csr_cached_temperature_s
{
	btx_csr_bccmd_header_t message;
	bt_uint                temperature;
} btx_csr_cached_temperature_t;

typedef struct _btx_csr_rssi_acl_s
{
	btx_csr_bccmd_header_t message;
	bt_hci_hconn_t         hconn;
	bt_uint                rssi;
} btx_csr_rssi_acl_t;

typedef struct _btx_csr_pio_s
{
	btx_csr_bccmd_header_t message;
	bt_uint                pio;
} btx_csr_pio_t;

typedef struct _btx_csr_pio_direction_mask_s
{
	btx_csr_bccmd_header_t message;
	bt_uint                direction;
} btx_csr_pio_direction_mask_t;

typedef struct _btx_csr_pio_protection_mask_s
{
	btx_csr_bccmd_header_t message;
	bt_uint                protection;
} btx_csr_pio_protection_mask_t;

typedef struct _btx_csr_strm_get_sink_s
{
	btx_csr_bccmd_header_t message;
	bt_uint                sink_id;
} btx_csr_strm_get_sink_t;

typedef struct _btx_csr_strm_get_source_s
{
	btx_csr_bccmd_header_t message;
	bt_uint                source_id;
} btx_csr_strm_get_source_t;

typedef union _btx_csr_var_u
{
	btx_csr_bccmd_header_t        message;
	btx_csr_cached_temperature_t  cached_temperature;
	btx_csr_rssi_acl_t            rssi_acl;
	btx_csr_pio_t                 pio;
	btx_csr_pio_direction_mask_t  pio_direction;
	btx_csr_pio_protection_mask_t pio_protection;
	btx_csr_strm_get_sink_t       strm_get_sink;
	btx_csr_strm_get_source_t     strm_get_source;
} btx_csr_var_t;

typedef struct _btx_csr_set_ps_vars_buffer_t btx_csr_set_ps_vars_buffer_t;

typedef void (*btx_csr_set_ps_vars_callback_fp)(bt_bool success, btx_csr_set_ps_vars_buffer_t* buffer);

typedef void (*btx_csr_get_var_callback_fp)(bt_uint status, bt_uint var_id, btx_csr_var_t* var_value, void* callback_param);

typedef void (*btx_csr_set_var_callback_fp)(bt_uint status, bt_uint var_id, btx_csr_var_t* var_value, void* callback_param);

typedef void (*btx_csr_get_ps_var_callback_fp)(bt_uint success, const bt_byte* value, bt_uint len, void* callback_param);

struct _btx_csr_set_ps_vars_buffer_t
{
	const bt_uint* ps_vars;
	btx_csr_set_ps_vars_callback_fp callback;
	void* callback_param;
	bt_uint current_var;
};


void btx_csr_autobaud(
		btx_csr_autobaud_buffer_t* buffer,
		btx_csr_autobaud_callback_fp callback,
		void* callback_param);

// Make controller auto-configure its UART speed and select H4 as host interface.
// PS_KEY_HOST_INTERFACE must not be set. PS_KEY_UART_BITRATE must be set to 0.
// This function works only with BC7 controller (tested on CSR 8810 and CSR 8811).
void btx_csr_bc7_sel_host_interface_h4(
		btx_csr_autobaud_buffer_t* buffer,
		bt_byte interval,
		btx_csr_autobaud_callback_fp callback,
		void* callback_param);

void btx_csr_exec_script(
		const btx_csr_script_t* script,
		btx_csr_exec_script_buffer_t* buffer,
		btx_csr_exec_script_callback_fp callback,
		void* callback_param);

bt_hci_command_t* btx_csr_alloc_bccmd_setreq(
		bt_uint var_id,
		bt_uint data_word_count,
		bt_hci_cmd_callback_fp callback,
		void* callback_param);

bt_hci_command_t* btx_csr_alloc_bccmd_getreq(
		bt_uint var_id,
		bt_uint data_word_count,
		bt_hci_cmd_callback_fp callback,
		void* callback_param);

bt_bool btx_csr_set_ps_var(
		bt_uint ps_key,
		const bt_uint* value,
		bt_uint value_word_count,
		bt_hci_cmd_callback_fp callback);

bt_bool btx_csr_set_ps_var_ex(
	   bt_uint ps_key,
	   const bt_uint* value,
	   bt_uint value_word_count,
	   bt_uint store,
	   bt_hci_cmd_callback_fp callback);

bt_bool btx_csr_get_ps_var(
		bt_uint ps_key,
		bt_uint value_word_count,
		btx_csr_get_ps_var_callback_fp callback,
		void* callback_param);

bt_bool btx_csr_get_ps_var_ex(
		bt_uint ps_key,
		bt_uint value_word_count,
		bt_uint store,
		btx_csr_get_ps_var_callback_fp callback,
		void* callback_param);

void btx_csr_set_ps_vars(
		const bt_uint* ps_vars,
		btx_csr_set_ps_vars_buffer_t* buffer,
		btx_csr_set_ps_vars_callback_fp callback,
		void* callback_param);

void btx_csr_set_ps_vars_ex(
		const bt_uint* ps_vars,
		btx_csr_set_ps_vars_buffer_t* buffer,
		bt_uint store,
		btx_csr_set_ps_vars_callback_fp callback,
		void* callback_param);

bt_bool btx_csr_warm_reset(void);

bt_bool btx_csr_warm_reset_ex(
		bt_hci_cmd_callback_fp callback,
		void* callback_param);

bt_bool btx_csr_enable_tx(
		bt_bool enable, 
		bt_hci_cmd_callback_fp callback,
		void* callback_param);

bt_bool btx_csr_get_var(
		bt_uint var_id,
		btx_csr_get_var_callback_fp callback,
		void* callback_param);

bt_bool btx_csr_set_var(
		bt_uint var_id,
		const bt_uint* value,
		bt_uint value_word_count,
		btx_csr_set_var_callback_fp callback,
		void* callback_param);

bt_bool btx_csr_get_cached_temperature(
		btx_csr_get_var_callback_fp callback,
		void* callback_param);

bt_bool btx_csr_get_rssi_acl(
		bt_hci_hconn_t hconn,
		btx_csr_get_var_callback_fp callback,
		void* callback_param);

// BlueCore 6		
const btx_csr_script_t* btx_csr_get_script__PB_27_R20_BC6ROM_A04(void);

// CSR8810
const btx_csr_script_t* btx_csr_get_script__PB_90_REV6(void);

#endif // __BTX_CSR_H_INCLUDED__
