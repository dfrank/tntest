/*******************************************************************************
 Microchip Bluetooth Stack - Host Controller Interface

  Company:
    Searan LLC.

  File Name:
    hci_eir.h

  Summary:
    Bluetooth API Library interface to the HCI Functions.

  Description:
    This is a portion of the API interface to the Bluetooth stack.  Other header files are
	grouped in the section under the CDBT master directory.  
	
*******************************************************************************/
// DOM-IGNORE-BEGIN
/*******************************************************************************
* Source contains proprietary and confidential information of SEARAN LLC.
* May not be used or disclosed to any other party except in accordance
* with a license from SEARAN LLC or Microchip Technology Inc.
* Copyright (c) 2011, 2012 SEARAN LLC. All Rights Reserved.
*
*
*******************************************************************************/
// DOM-IGNORE-END

#ifndef __HCI_EIR_H
#define __HCI_EIR_H

#define HCI_EIR_TYPE_FLAGS							0x01
#define HCI_EIR_TYPE_UUID16_LIST_MORE_AVAILABLE		0x02
#define HCI_EIR_TYPE_UUID16_LIST_COMPLETE			0x03
#define HCI_EIR_TYPE_UUID32_LIST_MORE_AVAILABLE		0x04
#define HCI_EIR_TYPE_UUID32_LIST_COMPLETE			0x05
#define HCI_EIR_TYPE_UUID128_LIST_MORE_AVAILABLE	0x06
#define HCI_EIR_TYPE_UUID128_LIST_COMPLETE			0x07
#define HCI_EIR_TYPE_LOCAL_NAME_SHORTENED			0x08
#define HCI_EIR_TYPE_LOCAL_NAME_COMPLETE			0x09
#define HCI_EIR_TYPE_TX_POWER_LEVEL					0x0a
#define HCI_EIR_TYPE_OOB_COD						0x0d
#define HCI_EIR_TYPE_OOB_HASH						0x0e
#define HCI_EIR_TYPE_OOB_RANDOMIZER					0x0f
#define HCI_EIR_TYPE_DEVICE_ID						0x10

#define HCI_EIR_TYPE_MANUFACTURER_SPECIFIC			0xFF

#define HCI_EIR_FEC_NOT_REQUIRED	0
#define HCI_EIR_FEC_REQUIRED		1

bt_hci_command_p bt_hci_allocate_write_eir_command(bt_byte fec_required);

bt_bool bt_hci_param_eir_local_name_add(const char* local_name, bt_hci_command_p pcmd);

bt_bool bt_hci_param_eir_uuid16_add(
	bt_byte data_type, const bt_uint* uuid_list, bt_byte uuid_list_size, bt_hci_command_p pcmd);

bt_bool bt_hci_param_eir_uuid32_add(
	bt_byte data_type, const bt_uuid32* uuid_list, bt_byte uuid_list_size, bt_hci_command_p pcmd);

bt_bool bt_hci_param_eir_uuid128_add(
	bt_byte data_type, const bt_uuid_t* uuid_list, bt_byte uuid_list_size, bt_hci_command_p pcmd);

bt_bool bt_hci_param_eir_vendor_add(
	bt_uint vendor_id, bt_byte* data, bt_byte data_len, bt_hci_command_p pcmd);

bt_bool bt_hci_param_eir_add(bt_byte data_type, const bt_byte* data, bt_byte length, bt_hci_command_p pcmd);

bt_bool bt_hci_param_tx_power_level_add(bt_byte tx_power_level, bt_hci_command_p pcmd);

bt_bool bt_hci_param_eir_device_id_add(
	bt_uint vendor_id_source, bt_uint vendor_id, bt_uint product_id, bt_uint version, bt_hci_command_p pcmd);

bt_bool bt_hci_write_eir(bt_hci_command_p pcmd, bt_hci_cmd_callback_fp cb);

#endif // __HCI_EIR_H
