/*******************************************************************************
 Microchip Bluetooth Stack - Host Controller Interface

  Company:
    Searan LLC.

  File Name:
    lm.h

  Summary:
    Bluetooth API Library interface to the HCI Functions.

  Description:
    This is a portion of the API interface to the Bluetooth stack.  Other header files are
	grouped in the section under the CDBT master directory.  
	
*******************************************************************************/
// DOM-IGNORE-BEGIN
/*******************************************************************************
* Source contains proprietary and confidential information of SEARAN LLC.
* May not be used or disclosed to any other party except in accordance
* with a license from SEARAN LLC or Microchip Technology Inc.
* Copyright (c) 2011, 2012 SEARAN LLC. All Rights Reserved.
*
*
*******************************************************************************/
// DOM-IGNORE-END

#ifndef __LM_H
#define __LM_H

#define LM_PACKET_TYPE_DM1	0x0008
#define LM_PACKET_TYPE_DH1	0x0010
#define LM_PACKET_TYPE_DM3	0x0400
#define LM_PACKET_TYPE_DH3	0x0800
#define LM_PACKET_TYPE_DM5	0x4000
#define LM_PACKET_TYPE_DH5	0x8000


#endif // __LM_H
