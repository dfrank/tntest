/*******************************************************************************
 Microchip Bluetooth Stack - Host Controller Interface

  Company:
    Searan LLC.

  File Name:
    hcitr_uart.h

  Summary:
    Bluetooth API Library interface to the HCI Functions.

  Description:
    This is a portion of the API interface to the Bluetooth stack.  Other header files are
	grouped in the section under the CDBT master directory.

*******************************************************************************/
// DOM-IGNORE-BEGIN
/*******************************************************************************
* Source contains proprietary and confidential information of SEARAN LLC.
* May not be used or disclosed to any other party except in accordance
* with a license from SEARAN LLC or Microchip Technology Inc.
* Copyright (c) 2011, 2012 SEARAN LLC. All Rights Reserved.
*
*
*******************************************************************************/
// DOM-IGNORE-END

#ifndef __HCITR_UART_H_INCLUDED__
#define __HCITR_UART_H_INCLUDED__


#define HCI_UART_PACKET_TYPE_COMMAND    1
#define HCI_UART_PACKET_TYPE_ACL_DATA   2
#define HCI_UART_PACKET_TYPE_SCO_DATA   3
#define HCI_UART_PACKET_TYPE_EVENT      4

// DOM-IGNORE-BEGIN
#ifdef __cplusplus
extern "C" {
#endif
// DOM-IGNORE-END

void bt_hcitr_uart_init(void);
void bt_hcitr_uart_reset(void);
void bt_hcitr_uart_start(void);

#ifdef __cplusplus
}
#endif

#endif // __HCITR_UART_H_INCLUDED__

