/*******************************************************************************
 Microchip Bluetooth Stack - Platform

  Company:
    Searan LLC.

  File Name:
    config.h

  Summary:
    Bluetooth API Library interface to the Platform.

  Description:
    This is a portion of the API interface to the Bluetooth stack.  Other header files are
	grouped in the section under the CDBT master directory.  This module describe functions 
	and data structures used for platform configuration.
    
*******************************************************************************/
// DOM-IGNORE-BEGIN
/*******************************************************************************
* Source contains proprietary and confidential information of SEARAN LLC.
* May not be used or disclosed to any other party except in accordance
* with a license from SEARAN LLC or Microchip Technology Inc.
* Copyright (c) 2011, 2012 SEARAN LLC. All Rights Reserved.
*
*
*******************************************************************************/
// DOM-IGNORE-END

#ifndef __PLAT_CONFIG_H_INCLUDED__
#define __PLAT_CONFIG_H_INCLUDED__


#endif // __PLAT_CONFIG_H_INCLUDED__
