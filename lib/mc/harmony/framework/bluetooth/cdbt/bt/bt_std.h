/*******************************************************************************
 Microchip Bluetooth Stack - Bluetooth General Functions

  Company:
    Searan LLC.

  File Name:
    bt_std.h

  Summary:
    Bluetooth API Library interface to Bluetooth General Functions.

  Description:
    This is a portion of the API interface to the Bluetooth stack.  Other header files are
	grouped in the section under the CDBT master directory.  
	
*******************************************************************************/
// DOM-IGNORE-BEGIN
/*******************************************************************************
* Source contains proprietary and confidential information of SEARAN LLC.
* May not be used or disclosed to any other party except in accordance
* with a license from SEARAN LLC or Microchip Technology Inc.
* Copyright (c) 2011, 2012 SEARAN LLC. All Rights Reserved.
*
*
*******************************************************************************/
// DOM-IGNORE-END

#ifndef __BT_STD_H_INCLUDED__
#define __BT_STD_H_INCLUDED__

#include "cdbt/bt/bt_config.h"
#define __BT_CONFIG_INCLUDED__

#include "cdbt/bt/bt_types.h"
#include "cdbt/bt/bt_bdaddr.h"

#include <stddef.h>
#include <string.h>

#endif // __BT_STD_H_INCLUDED__
