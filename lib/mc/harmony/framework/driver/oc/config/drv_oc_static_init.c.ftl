<#--
/*******************************************************************************
  Output Compare Driver Initialization File

  File Name:
    drv_ic_init.c.ftl

  Summary:
    This file contains source code necessary to initialize the OC driver.

  Description:
    This file contains source code necessary to initialize the system.  It
    implements the "SYS_Initialize" function, configuration bits, and allocates
    any necessary global system resources, such as the systemObjects structure
    that contains the object handles to all the MPLAB Harmony module objects in
    the system.
 *******************************************************************************/

/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTOCULAR PURPOSE.
IN NO EVENT SHALL MOCROCHIP OR ITS LOCENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STROCT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVOCES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->
<#if CONFIG_DRV_OC_DRIVER_MODE == "STATIC">
<#if CONFIG_DRV_OC_INST_IDX0 == true>

/*******************************************************************************
  Function:
    void DRV_OC0_Initialize(void)

  Summary:
    Initializes Output Compare Driver Instance 0

  Remarks:
 */
void DRV_OC0_Initialize(void)
{
    /* Setup OC0 Instance */
    PLIB_OC_ModeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_OC_COMPARE_MODES_IDX0});	
    PLIB_OC_BufferSizeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_OC_BUFFER_SIZE_IDX0});   
    PLIB_OC_TimerSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_OC_16BIT_TIMERS_IDX0});
    PLIB_OC_FaultInputSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_OC_FAULTS_IDX0});
    PLIB_OC_Buffer16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_OC_NONPWM_16BIT_PRI_COMPARE_IDX0});
    PLIB_OC_PulseWidth16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_OC_16BIT_PULSE_WIDTH_IDX0}); 
<#if CONFIG_DRV_OC_INTERRUPT_MODE == true>   

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX0});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX0}, ${CONFIG_DRV_OC_INT_PRIORITY_IDX0});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX0}, ${CONFIG_DRV_OC_INT_SUB_PRIORITY_IDX0});   
</#if>   
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX1 == true>

/*******************************************************************************
  Function:
    void DRV_OC1_Initialize(void)

  Summary:
    Initializes Output Compare Driver Instance 1

  Remarks:
 */
void DRV_OC1_Initialize(void)
{
    /* Setup OC1 Instance */	
    PLIB_OC_ModeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_OC_COMPARE_MODES_IDX1});	
    PLIB_OC_BufferSizeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_OC_BUFFER_SIZE_IDX1});   
    PLIB_OC_TimerSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_OC_16BIT_TIMERS_IDX1});
    PLIB_OC_FaultInputSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_OC_FAULTS_IDX1});
    PLIB_OC_Buffer16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_OC_NONPWM_16BIT_PRI_COMPARE_IDX1});
    PLIB_OC_PulseWidth16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_OC_16BIT_PULSE_WIDTH_IDX1});   
<#if CONFIG_DRV_OC_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX1});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX1}, ${CONFIG_DRV_OC_INT_PRIORITY_IDX1});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX1}, ${CONFIG_DRV_OC_INT_SUB_PRIORITY_IDX1});   
</#if>    
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX2 == true>

/*******************************************************************************
  Function:
    void DRV_OC2_Initialize(void)

  Summary:
    Initializes Output Compare Driver Instance 2

  Remarks:
 */
void DRV_OC2_Initialize(void)
{
    /* Setup OC2 Instance */	
    PLIB_OC_ModeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_OC_COMPARE_MODES_IDX2});	
    PLIB_OC_BufferSizeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_OC_BUFFER_SIZE_IDX2});   
    PLIB_OC_TimerSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_OC_16BIT_TIMERS_IDX2});
    PLIB_OC_FaultInputSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_OC_FAULTS_IDX2});
    PLIB_OC_Buffer16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_OC_NONPWM_16BIT_PRI_COMPARE_IDX2});
    PLIB_OC_PulseWidth16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_OC_16BIT_PULSE_WIDTH_IDX2}); 
<#if CONFIG_DRV_OC_INTERRUPT_MODE == true>    

    /* Setup Interrupt */  
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX2});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX2}, ${CONFIG_DRV_OC_INT_PRIORITY_IDX2});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX2}, ${CONFIG_DRV_OC_INT_SUB_PRIORITY_IDX2});      
</#if>    
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX3 == true>

/*******************************************************************************
  Function:
    void DRV_OC3_Initialize(void)

  Summary:
    Initializes Output Compare Driver Instance 3

  Remarks:
 */
void DRV_OC3_Initialize(void)
{
    /* Setup OC3 Instance */	
    PLIB_OC_ModeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_OC_COMPARE_MODES_IDX3});	
    PLIB_OC_BufferSizeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_OC_BUFFER_SIZE_IDX3});   
    PLIB_OC_TimerSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_OC_16BIT_TIMERS_IDX3});
    PLIB_OC_FaultInputSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_OC_FAULTS_IDX3});
    PLIB_OC_Buffer16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_OC_NONPWM_16BIT_PRI_COMPARE_IDX3});
    PLIB_OC_PulseWidth16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_OC_16BIT_PULSE_WIDTH_IDX3});  
<#if CONFIG_DRV_OC_INTERRUPT_MODE == true>

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX3});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX3}, ${CONFIG_DRV_OC_INT_PRIORITY_IDX3});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX3}, ${CONFIG_DRV_OC_INT_SUB_PRIORITY_IDX3});      
</#if> 
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX4 == true>

/*******************************************************************************
  Function:
    void DRV_OC4_Initialize(void)

  Summary:
    Initializes Output Compare Driver Instance 4

  Remarks:
 */
void DRV_OC4_Initialize(void)
{
    /* Setup OC4 Instance */
	
    PLIB_OC_ModeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_OC_COMPARE_MODES_IDX4});	
    PLIB_OC_BufferSizeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_OC_BUFFER_SIZE_IDX4});   
    PLIB_OC_TimerSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_OC_16BIT_TIMERS_IDX4});
    PLIB_OC_FaultInputSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_OC_FAULTS_IDX4});
    PLIB_OC_Buffer16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_OC_NONPWM_16BIT_PRI_COMPARE_IDX4});
    PLIB_OC_PulseWidth16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_OC_16BIT_PULSE_WIDTH_IDX4}); 
<#if CONFIG_DRV_OC_INTERRUPT_MODE == true>

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX4});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX4}, ${CONFIG_DRV_OC_INT_PRIORITY_IDX4});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX4}, ${CONFIG_DRV_OC_INT_SUB_PRIORITY_IDX4});      
</#if> 
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX5 == true>

/*******************************************************************************
  Function:
    void DRV_OC5_Initialize(void)

  Summary:
    Initializes Output Compare Driver Instance 5

  Remarks:
 */
void DRV_OC5_Initialize(void)
{
    /* Setup OC5 Instance */
	
    PLIB_OC_ModeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_OC_COMPARE_MODES_IDX5});	
    PLIB_OC_BufferSizeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_OC_BUFFER_SIZE_IDX5});   
    PLIB_OC_TimerSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_OC_16BIT_TIMERS_IDX5});
    PLIB_OC_FaultInputSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_OC_FAULTS_IDX5});
    PLIB_OC_Buffer16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_OC_NONPWM_16BIT_PRI_COMPARE_IDX5});
    PLIB_OC_PulseWidth16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_OC_16BIT_PULSE_WIDTH_IDX5});   
<#if CONFIG_DRV_OC_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX5});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX5}, ${CONFIG_DRV_OC_INT_PRIORITY_IDX5});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX5}, ${CONFIG_DRV_OC_INT_SUB_PRIORITY_IDX5});      
</#if> 
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX6 == true>

/*******************************************************************************
  Function:
    void DRV_OC6_Initialize(void)

  Summary:
    Initializes Output Compare Driver Instance 6

  Remarks:
 */
void DRV_OC6_Initialize(void)
{
    /* Setup OC6 Instance */	
    PLIB_OC_ModeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_OC_COMPARE_MODES_IDX6});	
    PLIB_OC_BufferSizeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_OC_BUFFER_SIZE_IDX6});   
    PLIB_OC_TimerSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_OC_16BIT_TIMERS_IDX6});
    PLIB_OC_FaultInputSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_OC_FAULTS_IDX6});
    PLIB_OC_Buffer16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_OC_NONPWM_16BIT_PRI_COMPARE_IDX6});
    PLIB_OC_PulseWidth16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_OC_16BIT_PULSE_WIDTH_IDX6}); 
<#if CONFIG_DRV_OC_INTERRUPT_MODE == true> 

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX6});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX6}, ${CONFIG_DRV_OC_INT_PRIORITY_IDX6});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX6}, ${CONFIG_DRV_OC_INT_SUB_PRIORITY_IDX6});      
</#if> 
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX7 == true>

/*******************************************************************************
  Function:
    void DRV_OC7_Initialize(void)

  Summary:
    Initializes Output Compare Driver Instance 7

  Remarks:
 */
void DRV_OC7_Initialize(void)
{
    /* Setup OC7 Instance */	
    PLIB_OC_ModeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_OC_COMPARE_MODES_IDX7});	
    PLIB_OC_BufferSizeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_OC_BUFFER_SIZE_IDX7});   
    PLIB_OC_TimerSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_OC_16BIT_TIMERS_IDX7});
    PLIB_OC_FaultInputSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_OC_FAULTS_IDX7});
    PLIB_OC_Buffer16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_OC_NONPWM_16BIT_PRI_COMPARE_IDX7});
    PLIB_OC_PulseWidth16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_OC_16BIT_PULSE_WIDTH_IDX7}); 
<#if CONFIG_DRV_OC_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX7});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX7}, ${CONFIG_DRV_OC_INT_PRIORITY_IDX7});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX7}, ${CONFIG_DRV_OC_INT_SUB_PRIORITY_IDX7});      
</#if> 
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX8 == true>

/*******************************************************************************
  Function:
    void DRV_OC8_Initialize(void)

  Summary:
    Initializes Output Compare Driver Instance 8

  Remarks:
 */
void DRV_OC8_Initialize(void)
{
    /* Setup OC8 Instance */	
    PLIB_OC_ModeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_OC_COMPARE_MODES_IDX8});	
    PLIB_OC_BufferSizeSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_OC_BUFFER_SIZE_IDX8});   
    PLIB_OC_TimerSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_OC_16BIT_TIMERS_IDX8});
    PLIB_OC_FaultInputSelect(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_OC_FAULTS_IDX8});
    PLIB_OC_Buffer16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_OC_NONPWM_16BIT_PRI_COMPARE_IDX8});
    PLIB_OC_PulseWidth16BitSet(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_OC_16BIT_PULSE_WIDTH_IDX8});  
<#if CONFIG_DRV_OC_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX8});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX8}, ${CONFIG_DRV_OC_INT_PRIORITY_IDX8});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_OC_INTERRUPT_VECTOR_IDX8}, ${CONFIG_DRV_OC_INT_SUB_PRIORITY_IDX8});      
</#if> 
}
</#if>
</#if>

<#--
/*******************************************************************************
 End of File
*/
-->
