<#--
/*******************************************************************************
  OC Driver Interrupt Handler Template File

  File Name:
    drv_oc_int.c.ftl

  Summary:
    This file contains source code necessary to initialize the system.

  Description:
    This file contains source code necessary to initialize the system.  It
    implements the "SYS_Initialize" function, configuration bits, and allocates
    any necessary global system resources, such as the systemObjects structure
    that contains the object handles to all the MPLAB Harmony module objects in
    the system.
 *******************************************************************************/

/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->
<#if CONFIG_DRV_OC_INTERRUPT_MODE == true>
<#if CONFIG_DRV_OC_INST_IDX0 == true>

void __ISR(${CONFIG_DRV_OC_ISR_VECTOR_IDX0}, ipl${CONFIG_DRV_OC_INT_PRIO_NUM_IDX0}) _IntHandlerInstanceOC0(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX0});
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX1 == true>

void __ISR(${CONFIG_DRV_OC_ISR_VECTOR_IDX1}, ipl${CONFIG_DRV_OC_INT_PRIO_NUM_IDX1}) _IntHandlerInstanceOC1(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX1});
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX2 == true>

void __ISR(${CONFIG_DRV_OC_ISR_VECTOR_IDX2}, ipl${CONFIG_DRV_OC_INT_PRIO_NUM_IDX2}) _IntHandlerInstanceOC2(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX2});
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX3 == true>

void __ISR(${CONFIG_DRV_OC_ISR_VECTOR_IDX3}, ipl${CONFIG_DRV_OC_INT_PRIO_NUM_IDX3}) _IntHandlerInstanceOC3(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX3});
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX4 == true>

void __ISR(${CONFIG_DRV_OC_ISR_VECTOR_IDX4}, ipl${CONFIG_DRV_OC_INT_PRIO_NUM_IDX4}) _IntHandlerInstanceOC4(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX4});
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX5 == true>

void __ISR(${CONFIG_DRV_OC_ISR_VECTOR_IDX5}, ipl${CONFIG_DRV_OC_INT_PRIO_NUM_IDX5}) _IntHandlerInstanceOC5(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX5});
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX6 == true>

void __ISR(${CONFIG_DRV_OC_ISR_VECTOR_IDX6}, ipl${CONFIG_DRV_OC_INT_PRIO_NUM_IDX6}) _IntHandlerInstanceOC6(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX6});
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX7 == true>

void __ISR(${CONFIG_DRV_OC_ISR_VECTOR_IDX7}, ipl${CONFIG_DRV_OC_INT_PRIO_NUM_IDX7}) _IntHandlerInstanceOC7(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX7});
}
</#if>
<#if CONFIG_DRV_OC_INST_IDX8 == true>

void __ISR(${CONFIG_DRV_OC_ISR_VECTOR_IDX8}, ipl${CONFIG_DRV_OC_INT_PRIO_NUM_IDX8}) _IntHandlerInstanceOC8(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_OC_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_OC_INTERRUPT_SOURCE_IDX8});
}
</#if>
</#if>

<#--
/*******************************************************************************
 End of File
*/
-->
