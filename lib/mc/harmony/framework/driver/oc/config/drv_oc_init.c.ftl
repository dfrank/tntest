<#--
/*******************************************************************************
  Output Compare Driver Initialization File

  File Name:
    drv_ic_init.c.ftl

  Summary:
    This file contains source code necessary to initialize the OC driver.

  Description:
    This file contains source code necessary to initialize the system.  It
    implements the "SYS_Initialize" function, configuration bits, and allocates
    any necessary global system resources, such as the systemObjects structure
    that contains the object handles to all the MPLAB Harmony module objects in
    the system.
 *******************************************************************************/

/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTOCULAR PURPOSE.
IN NO EVENT SHALL MOCROCHIP OR ITS LOCENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STROCT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVOCES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->
<#if CONFIG_DRV_OC_DRIVER_MODE == "STATIC">
<#if CONFIG_DRV_OC_INST_IDX0 == true>
    /* OC0 Initialization Call */ 
    DRV_OC0_Initialize();
</#if>
<#if CONFIG_DRV_OC_INST_IDX1 == true>

    /* OC1 Initialization Call */ 
    DRV_OC1_Initialize();
</#if>
<#if CONFIG_DRV_OC_INST_IDX2 == true>

    /* OC2 Initialization Call */ 
    DRV_OC2_Initialize();
</#if>
<#if CONFIG_DRV_OC_INST_IDX3 == true>

    /* OC3 Initialization Call */ 
    DRV_OC3_Initialize();
</#if>
<#if CONFIG_DRV_OC_INST_IDX4 == true>

    /* OC4 Initialization Call */ 
    DRV_OC4_Initialize();
</#if>
<#if CONFIG_DRV_OC_INST_IDX5 == true>

    /* OC5 Initialization Call */ 
    DRV_OC5_Initialize();
</#if>
<#if CONFIG_DRV_OC_INST_IDX6 == true>

    /* OC6 Initialization Call */ 
    DRV_OC6_Initialize();
</#if>
<#if CONFIG_DRV_OC_INST_IDX7 == true>

    /* OC7 Initialization Call */ 
    DRV_OC7_Initialize();
</#if>
<#if CONFIG_DRV_OC_INST_IDX8 == true>

    /* OC8 Initialization Call */ 
    DRV_OC8_Initialize();
</#if>
</#if>

<#--
/*******************************************************************************
 End of File
*/
-->
