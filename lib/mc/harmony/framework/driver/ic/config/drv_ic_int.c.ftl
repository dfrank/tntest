<#--
/*******************************************************************************
  IC Driver Interrupt Handler Template File

  File Name:
    drv_ic_int.c.ftl

  Summary:
    This file contains source code necessary to initialize the system.

  Description:
    This file contains source code necessary to initialize the system.  It
    implements the "SYS_Initialize" function, configuration bits, and allocates
    any necessary global system resources, such as the systemObjects structure
    that contains the object handles to all the MPLAB Harmony module objects in
    the system.
 *******************************************************************************/

/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DICUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICRICHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PRICUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->
<#if CONFIG_DRV_IC_INTERRUPT_MODE == true>
<#if CONFIG_DRV_IC_INST_IDX0 == true>

void __ISR(${CONFIG_DRV_IC_ISR_VECTOR_IDX0}, ipl${CONFIG_DRV_IC_INT_PRIO_NUM_IDX0}) _IntHandlerInstanceIC0(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX0});
}
<#if CONFIG_PIC32MZ == true>

void __ISR(${CONFIG_DRV_IC_ERR_ISR_VECTOR_IDX0}, ipl${CONFIG_DRV_IC_ERR_INT_PRIO_NUM_IDX0}) _IntErrorHandlerInstanceIC0(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX0});
}
</#if>
</#if>
<#if CONFIG_DRV_IC_INST_IDX1 == true>

void __ISR(${CONFIG_DRV_IC_ISR_VECTOR_IDX1}, ipl${CONFIG_DRV_IC_INT_PRIO_NUM_IDX1}) _IntHandlerInstanceIC1(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX1});
}
<#if CONFIG_PIC32MZ == true>

void __ISR(${CONFIG_DRV_IC_ERR_ISR_VECTOR_IDX1}, ipl${CONFIG_DRV_IC_ERR_INT_PRIO_NUM_IDX1}) _IntErrorHandlerInstanceIC1(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX1});
}
</#if>
</#if>
<#if CONFIG_DRV_IC_INST_IDX2 == true>

void __ISR(${CONFIG_DRV_IC_ISR_VECTOR_IDX2}, ipl${CONFIG_DRV_IC_INT_PRIO_NUM_IDX2}) _IntHandlerInstanceIC2(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX2});
}
<#if CONFIG_PIC32MZ == true>

void __ISR(${CONFIG_DRV_IC_ERR_ISR_VECTOR_IDX2}, ipl${CONFIG_DRV_IC_ERR_INT_PRIO_NUM_IDX2}) _IntErrorHandlerInstanceIC2(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX2});
}
</#if>
</#if>
<#if CONFIG_DRV_IC_INST_IDX3 == true>

void __ISR(${CONFIG_DRV_IC_ISR_VECTOR_IDX3}, ipl${CONFIG_DRV_IC_INT_PRIO_NUM_IDX3}) _IntHandlerInstanceIC3(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX3});
}
<#if CONFIG_PIC32MZ == true>

void __ISR(${CONFIG_DRV_IC_ERR_ISR_VECTOR_IDX3}, ipl${CONFIG_DRV_IC_ERR_INT_PRIO_NUM_IDX3}) _IntErrorHandlerInstanceIC3(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX3});
}
</#if>
</#if>
<#if CONFIG_DRV_IC_INST_IDX4 == true>

void __ISR(${CONFIG_DRV_IC_ISR_VECTOR_IDX4}, ipl${CONFIG_DRV_IC_INT_PRIO_NUM_IDX4}) _IntHandlerInstanceIC4(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX4});
}
<#if CONFIG_PIC32MZ == true>

void __ISR(${CONFIG_DRV_IC_ERR_ISR_VECTOR_IDX4}, ipl${CONFIG_DRV_IC_ERR_INT_PRIO_NUM_IDX4}) _IntErrorHandlerInstanceIC4(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX4});
}
</#if>
</#if>
<#if CONFIG_DRV_IC_INST_IDX5 == true>

void __ISR(${CONFIG_DRV_IC_ISR_VECTOR_IDX5}, ipl${CONFIG_DRV_IC_INT_PRIO_NUM_IDX5}) _IntHandlerInstanceIC5(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX5});
}
<#if CONFIG_PIC32MZ == true>

void __ISR(${CONFIG_DRV_IC_ERR_ISR_VECTOR_IDX5}, ipl${CONFIG_DRV_IC_ERR_INT_PRIO_NUM_IDX5}) _IntErrorHandlerInstanceIC5(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX5});
}
</#if>
</#if>
<#if CONFIG_DRV_IC_INST_IDX6 == true>

void __ISR(${CONFIG_DRV_IC_ISR_VECTOR_IDX6}, ipl${CONFIG_DRV_IC_INT_PRIO_NUM_IDX6}) _IntHandlerInstanceIC6(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX6});
}
<#if CONFIG_PIC32MZ == true>

void __ISR(${CONFIG_DRV_IC_ERR_ISR_VECTOR_IDX6}, ipl${CONFIG_DRV_IC_ERR_INT_PRIO_NUM_IDX6}) _IntErrorHandlerInstanceIC6(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX6});
}
</#if>
</#if>
<#if CONFIG_DRV_IC_INST_IDX7 == true>

void __ISR(${CONFIG_DRV_IC_ISR_VECTOR_IDX7}, ipl${CONFIG_DRV_IC_INT_PRIO_NUM_IDX7}) _IntHandlerInstanceIC7(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX7});
}
<#if CONFIG_PIC32MZ == true>

void __ISR(${CONFIG_DRV_IC_ERR_ISR_VECTOR_IDX7}, ipl${CONFIG_DRV_IC_ERR_INT_PRIO_NUM_IDX7}) _IntErrorHandlerInstanceIC7(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX7});
}
</#if>
</#if>
<#if CONFIG_DRV_IC_INST_IDX8 == true>

void __ISR(${CONFIG_DRV_IC_ISR_VECTOR_IDX8}, ipl${CONFIG_DRV_IC_INT_PRIO_NUM_IDX8}) _IntHandlerInstanceIC8(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX8});
}
<#if CONFIG_PIC32MZ == true>

void __ISR(${CONFIG_DRV_IC_ERR_ISR_VECTOR_IDX8}, ipl${CONFIG_DRV_IC_ERR_INT_PRIO_NUM_IDX8}) _IntErrorHandlerInstanceIC8(void)
{
    PLIB_INT_SourceFlagClear(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX8});
}
</#if>
</#if>
</#if>

<#--
/*******************************************************************************
 End of File
*/
-->
