<#--
/*******************************************************************************
  Input Capture Driver Initialization File

  File Name:
    drv_ic_init.c.ftl

  Summary:
    This file contains source code necessary to initialize the IC driver.

  Description:
    This file contains source code necessary to initialize the system.  It
    implements the "SYS_Initialize" function, configuration bits, and allocates
    any necessary global system resources, such as the systemObjects structure
    that contains the object handles to all the MPLAB Harmony module objects in
    the system.
 *******************************************************************************/

/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->
<#if CONFIG_DRV_IC_DRIVER_MODE == "STATIC">
<#if CONFIG_DRV_IC_INST_IDX0 == true>

/*******************************************************************************
  Function:
    void DRV_IC0_Initialize(void)

  Summary:
    Initializes Input Capture Driver Instance 0

  Remarks:
 */
void DRV_IC0_Initialize(void)
{
    /* Setup IC0 Instance */	
    PLIB_IC_ModeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_IC_INPUT_CAPTURE_MODES_IDX0});
    PLIB_IC_FirstCaptureEdgeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_IC_EDGE_TYPES_IDX0});
    PLIB_IC_TimerSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_IC_TIMERS_IDX0});
    PLIB_IC_BufferSizeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_IC_BUFFER_SIZE_IDX0});
    PLIB_IC_EventsPerInterruptSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_IC_EVENTS_PER_INTERRUPT_IDX0});   
<#if CONFIG_DRV_IC_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX0});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX0}, ${CONFIG_DRV_IC_INT_PRIORITY_IDX0});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX0}, ${CONFIG_DRV_IC_INT_SUB_PRIORITY_IDX0});          
<#if CONFIG_PIC32MZ == true>    

    /* Setup Error Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX0});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX0}, ${CONFIG_DRV_IC_ERR_INT_PRIORITY_IDX0});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX0}, ${CONFIG_DRV_IC_ERR_INT_SUB_PRIORITY_IDX0});      
</#if>
</#if>
}
</#if>
<#if CONFIG_DRV_IC_INST_IDX1 == true>

/*******************************************************************************
  Function:
    void DRV_IC1_Initialize(void)

  Summary:
    Initializes Input Capture Driver Instance 1

  Remarks:
 */
void DRV_IC1_Initialize(void)
{
    /* Setup IC1 Instance */	
    PLIB_IC_ModeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_IC_INPUT_CAPTURE_MODES_IDX1});
    PLIB_IC_FirstCaptureEdgeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_IC_EDGE_TYPES_IDX1});
    PLIB_IC_TimerSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_IC_TIMERS_IDX1});
    PLIB_IC_BufferSizeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_IC_BUFFER_SIZE_IDX1});
    PLIB_IC_EventsPerInterruptSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_IC_EVENTS_PER_INTERRUPT_IDX1});    
<#if CONFIG_DRV_IC_INTERRUPT_MODE == true>

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX1});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX1}, ${CONFIG_DRV_IC_INT_PRIORITY_IDX1});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX1}, ${CONFIG_DRV_IC_INT_SUB_PRIORITY_IDX1});      
<#if CONFIG_PIC32MZ == true>    

    /* Setup Error Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX1});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX1}, ${CONFIG_DRV_IC_ERR_INT_PRIORITY_IDX1});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX1}, ${CONFIG_DRV_IC_ERR_INT_SUB_PRIORITY_IDX1});      
</#if>
</#if>
}
</#if>
<#if CONFIG_DRV_IC_INST_IDX2 == true>

/*******************************************************************************
  Function:
    void DRV_IC2_Initialize(void)

  Summary:
    Initializes Input Capture Driver Instance 2

  Remarks:
 */
void DRV_IC2_Initialize(void)
{
    /* Setup IC2 Instance */	
    PLIB_IC_ModeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_IC_INPUT_CAPTURE_MODES_IDX2});
    PLIB_IC_FirstCaptureEdgeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_IC_EDGE_TYPES_IDX2});
    PLIB_IC_TimerSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_IC_TIMERS_IDX2});
    PLIB_IC_BufferSizeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_IC_BUFFER_SIZE_IDX2});
    PLIB_IC_EventsPerInterruptSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_IC_EVENTS_PER_INTERRUPT_IDX2});    
<#if CONFIG_DRV_IC_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX2});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX2}, ${CONFIG_DRV_IC_INT_PRIORITY_IDX2});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX2}, ${CONFIG_DRV_IC_INT_SUB_PRIORITY_IDX2});      
<#if CONFIG_PIC32MZ == true>    

    /* Setup Error Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX2});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX2}, ${CONFIG_DRV_IC_ERR_INT_PRIORITY_IDX2});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX2}, ${CONFIG_DRV_IC_ERR_INT_SUB_PRIORITY_IDX2});      
</#if>
</#if>
}
</#if>
<#if CONFIG_DRV_IC_INST_IDX3 == true>

/*******************************************************************************
  Function:
    void DRV_IC3_Initialize(void)

  Summary:
    Initializes Input Capture Driver Instance 3

  Remarks:
 */
void DRV_IC3_Initialize(void)
{
    /* Setup IC3 Instance */
    PLIB_IC_ModeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_IC_INPUT_CAPTURE_MODES_IDX3});
    PLIB_IC_FirstCaptureEdgeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_IC_EDGE_TYPES_IDX3});
    PLIB_IC_TimerSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_IC_TIMERS_IDX3});
    PLIB_IC_BufferSizeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_IC_BUFFER_SIZE_IDX3});
    PLIB_IC_EventsPerInterruptSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_IC_EVENTS_PER_INTERRUPT_IDX3});    
<#if CONFIG_DRV_IC_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX3});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX3}, ${CONFIG_DRV_IC_INT_PRIORITY_IDX3});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX3}, ${CONFIG_DRV_IC_INT_SUB_PRIORITY_IDX3});      
<#if CONFIG_PIC32MZ == true>    

    /* Setup Error Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX3});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX3}, ${CONFIG_DRV_IC_ERR_INT_PRIORITY_IDX3});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX3}, ${CONFIG_DRV_IC_ERR_INT_SUB_PRIORITY_IDX3});      
</#if>
</#if>
}
</#if>
<#if CONFIG_DRV_IC_INST_IDX4 == true>

/*******************************************************************************
  Function:
    void DRV_IC4_Initialize(void)

  Summary:
    Initializes Input Capture Driver Instance 4

  Remarks:
 */
void DRV_IC4_Initialize(void)
{
    /* Setup IC4 Instance */
    PLIB_IC_ModeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_IC_INPUT_CAPTURE_MODES_IDX4});
    PLIB_IC_FirstCaptureEdgeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_IC_EDGE_TYPES_IDX4});
    PLIB_IC_TimerSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_IC_TIMERS_IDX4});
    PLIB_IC_BufferSizeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_IC_BUFFER_SIZE_IDX4});
    PLIB_IC_EventsPerInterruptSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_IC_EVENTS_PER_INTERRUPT_IDX4});    
<#if CONFIG_DRV_IC_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX4});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX4}, ${CONFIG_DRV_IC_INT_PRIORITY_IDX4});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX4}, ${CONFIG_DRV_IC_INT_SUB_PRIORITY_IDX4});      
<#if CONFIG_PIC32MZ == true>    

    /* Setup Error Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX4});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX4}, ${CONFIG_DRV_IC_ERR_INT_PRIORITY_IDX4});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX4}, ${CONFIG_DRV_IC_ERR_INT_SUB_PRIORITY_IDX4});      
</#if>
</#if>
}
</#if>
<#if CONFIG_DRV_IC_INST_IDX5 == true>

/*******************************************************************************
  Function:
    void DRV_IC5_Initialize(void)

  Summary:
    Initializes Input Capture Driver Instance 5

  Remarks:
 */
void DRV_IC5_Initialize(void)
{
    /* Setup IC5 Instance */	
    PLIB_IC_ModeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_IC_INPUT_CAPTURE_MODES_IDX5});
    PLIB_IC_FirstCaptureEdgeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_IC_EDGE_TYPES_IDX5});
    PLIB_IC_TimerSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_IC_TIMERS_IDX5});
    PLIB_IC_BufferSizeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_IC_BUFFER_SIZE_IDX5});
    PLIB_IC_EventsPerInterruptSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_IC_EVENTS_PER_INTERRUPT_IDX5});    
<#if CONFIG_DRV_IC_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX5});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX5}, ${CONFIG_DRV_IC_INT_PRIORITY_IDX5});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX5}, ${CONFIG_DRV_IC_INT_SUB_PRIORITY_IDX5});      
<#if CONFIG_PIC32MZ == true>    

    /* Setup Error Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX5});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX5}, ${CONFIG_DRV_IC_ERR_INT_PRIORITY_IDX5});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX5}, ${CONFIG_DRV_IC_ERR_INT_SUB_PRIORITY_IDX5});      
</#if>
</#if>
}
</#if>
<#if CONFIG_DRV_IC_INST_IDX6 == true>

/*******************************************************************************
  Function:
    void DRV_IC6_Initialize(void)

  Summary:
    Initializes Input Capture Driver Instance 6

  Remarks:
 */
void DRV_IC6_Initialize(void)
{
    /* Setup IC6 Instance */	
    PLIB_IC_ModeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_IC_INPUT_CAPTURE_MODES_IDX6});
    PLIB_IC_FirstCaptureEdgeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_IC_EDGE_TYPES_IDX6});
    PLIB_IC_TimerSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_IC_TIMERS_IDX6});
    PLIB_IC_BufferSizeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_IC_BUFFER_SIZE_IDX6});
    PLIB_IC_EventsPerInterruptSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_IC_EVENTS_PER_INTERRUPT_IDX6});
<#if CONFIG_DRV_IC_INTERRUPT_MODE == true>    

    /* Setup Interrupt */
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX6});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX6}, ${CONFIG_DRV_IC_INT_PRIORITY_IDX6});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX6}, ${CONFIG_DRV_IC_INT_SUB_PRIORITY_IDX6});      
<#if CONFIG_PIC32MZ == true>    

    /* Setup Error Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX6});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX6}, ${CONFIG_DRV_IC_ERR_INT_PRIORITY_IDX6});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX6}, ${CONFIG_DRV_IC_ERR_INT_SUB_PRIORITY_IDX6});      
</#if>
</#if>
}
</#if>
<#if CONFIG_DRV_IC_INST_IDX7 == true>

/*******************************************************************************
  Function:
    void DRV_IC7_Initialize(void)

  Summary:
    Initializes Input Capture Driver Instance 7

  Remarks:
 */
void DRV_IC7_Initialize(void)
{
    /* Setup IC7 Instance */
    PLIB_IC_ModeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_IC_INPUT_CAPTURE_MODES_IDX7});
    PLIB_IC_FirstCaptureEdgeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_IC_EDGE_TYPES_IDX7});
    PLIB_IC_TimerSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_IC_TIMERS_IDX7});
    PLIB_IC_BufferSizeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_IC_BUFFER_SIZE_IDX7});
    PLIB_IC_EventsPerInterruptSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_IC_EVENTS_PER_INTERRUPT_IDX7});
<#if CONFIG_DRV_IC_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX7});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX7}, ${CONFIG_DRV_IC_INT_PRIORITY_IDX7});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX7}, ${CONFIG_DRV_IC_INT_SUB_PRIORITY_IDX7});      
<#if CONFIG_PIC32MZ == true>    

    /* Setup Error Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX7});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX7}, ${CONFIG_DRV_IC_ERR_INT_PRIORITY_IDX7});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX7}, ${CONFIG_DRV_IC_ERR_INT_SUB_PRIORITY_IDX7});      
</#if>
</#if>
}
</#if>
<#if CONFIG_DRV_IC_INST_IDX8 == true>

/*******************************************************************************
  Function:
    void DRV_IC8_Initialize(void)

  Summary:
    Initializes Input Capture Driver Instance 8

  Remarks:
 */
void DRV_IC8_Initialize(void)
{
    /* Setup IC8 Instance */
    PLIB_IC_ModeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_IC_INPUT_CAPTURE_MODES_IDX8});
    PLIB_IC_FirstCaptureEdgeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_IC_EDGE_TYPES_IDX8});
    PLIB_IC_TimerSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_IC_TIMERS_IDX8});
    PLIB_IC_BufferSizeSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_IC_BUFFER_SIZE_IDX8});
    PLIB_IC_EventsPerInterruptSelect(${CONFIG_DRV_IC_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_IC_EVENTS_PER_INTERRUPT_IDX8});    
<#if CONFIG_DRV_IC_INTERRUPT_MODE == true>    
   
    /* Setup Interrupt */
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_SOURCE_IDX8});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX8}, ${CONFIG_DRV_IC_INT_PRIORITY_IDX8});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_INTERRUPT_VECTOR_IDX8}, ${CONFIG_DRV_IC_INT_SUB_PRIORITY_IDX8});      
<#if CONFIG_PIC32MZ == true>    

    /* Setup Error Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_SOURCE_IDX8});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX8}, ${CONFIG_DRV_IC_ERR_INT_PRIORITY_IDX8});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_IC_ERR_INTERRUPT_VECTOR_IDX8}, ${CONFIG_DRV_IC_ERR_INT_SUB_PRIORITY_IDX8});      
</#if>
</#if>
}
</#if>
</#if>

<#--
/*******************************************************************************
 End of File
*/
-->
