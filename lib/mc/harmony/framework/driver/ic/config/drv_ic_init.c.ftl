<#--
/*******************************************************************************
  Input Capture Driver Initialization File

  File Name:
    drv_ic_init.c.ftl

  Summary:
    This file contains source code necessary to initialize the IC driver.

  Description:
    This file contains source code necessary to initialize the system.  It
    implements the "SYS_Initialize" function, configuration bits, and allocates
    any necessary global system resources, such as the systemObjects structure
    that contains the object handles to all the MPLAB Harmony module objects in
    the system.
 *******************************************************************************/

/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->
<#if CONFIG_DRV_IC_DRIVER_MODE == "STATIC">
<#if CONFIG_DRV_IC_INST_IDX0 == true>
    /* IC0 Initialization Call */
    DRV_IC0_Initialize();
</#if>
<#if CONFIG_DRV_IC_INST_IDX1 == true>

    /* IC1 Initialization Call */
    DRV_IC1_Initialize();
</#if>
<#if CONFIG_DRV_IC_INST_IDX2 == true>

    /* IC2 Initialization Call */
    DRV_IC2_Initialize();
</#if>
<#if CONFIG_DRV_IC_INST_IDX3 == true>

    /* IC3 Initialization Call */
    DRV_IC3_Initialize();
</#if>
<#if CONFIG_DRV_IC_INST_IDX4 == true>

    /* IC4 Initialization Call */
    DRV_IC4_Initialize();
</#if>
<#if CONFIG_DRV_IC_INST_IDX5 == true>

    /* IC5 Initialization Call */
    DRV_IC5_Initialize();
</#if>
<#if CONFIG_DRV_IC_INST_IDX6 == true>

    /* IC6 Initialization Call */
    DRV_IC6_Initialize();
</#if>
<#if CONFIG_DRV_IC_INST_IDX7 == true>

    /* IC7 Initialization Call */
    DRV_IC7_Initialize();
</#if>
<#if CONFIG_DRV_IC_INST_IDX8 == true>

    /* IC8 Initialization Call */
    DRV_IC8_Initialize();
</#if>
</#if>

<#--
/*******************************************************************************
 End of File
*/
-->
