<#--
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->

/*** SDCARD Driver Initialization Data ***/
<#if CONFIG_USE_DRV_SDCARD == true>
const DRV_SDCARD_INIT drvSDCardInit =
{
<#if CONFIG_DRV_SDCARD_SPI_ID?has_content>
    .spiId = ${CONFIG_DRV_SDCARD_SPI_ID},
</#if>
<#if CONFIG_DRV_SDCARD_SPEED?has_content>
    .sdcardSpeedHz = ${CONFIG_DRV_SDCARD_SPEED},
</#if>
<#if CONFIG_DRV_SDCARD_CD_PORT_CHANNEL?has_content>
    .cardDetectPort = ${CONFIG_DRV_SDCARD_CD_PORT_CHANNEL},
    .cardDetectBitPosition = ${CONFIG_DRV_SDCARD_CD_BIT_POSITION},
</#if>
<#if CONFIG_DRV_SDCARD_WP_PORT_CHANNEL?has_content>
    .writeProtectPort = ${CONFIG_DRV_SDCARD_WP_PORT_CHANNEL},
    .writeProtectBitPosition = ${CONFIG_DRV_SDCARD_WP_BIT_POSITION},
</#if>
<#if CONFIG_DRV_SDCARD_CS_PORT_CHANNEL?has_content>
    .chipSelectPort = ${CONFIG_DRV_SDCARD_CS_PORT_CHANNEL},
    .chipSelectBitPosition = ${CONFIG_DRV_SDCARD_CS_BIT_POSITION},
</#if>
};
</#if>
<#--
/*******************************************************************************
 End of File
*/
-->
