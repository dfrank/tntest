/*******************************************************************************
  MRF24W Driver Com Layer (specific to the MRF24WG)

  File Name:
    drv_wifi_client_table.c

  Summary:
    Module for Microchip TCP/IP Stack

  Description:
    -Provides access to MRF24W WiFi controller
    -Reference: MRF24W Data sheet, IEEE 802.11 Standard
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/


/*
*********************************************************************************************************
*                                           INCLUDES
*********************************************************************************************************
*/

#include "tcpip/src/tcpip_private.h"
#if defined(TCPIP_IF_MRF24W)
//#if (WF_DEFAULT_NETWORK_TYPE == DRV_WIFI_NETWORK_TYPE_SOFT_AP)
#define COUNTS_SOFTAP_CLIENT_TABLE 16
t_drv_wifi_softap_client_table  drv_wifi_softap_client_table[COUNTS_SOFTAP_CLIENT_TABLE] ;


static int SoftApClientTable_LookForMac(uint8_t *mac)
{
    int i;
    for(i=0; i<COUNTS_SOFTAP_CLIENT_TABLE; i++)
    {
        if(memcmp(mac, drv_wifi_softap_client_table[i].mac,6) == 0)
        {
            return i;
        }
    }
    return -1;
}
static int SoftApClientTable_Add_NewMac(uint8_t *NewMac)
{
    // look for a new slot
    uint8_t ZeroArray[6] = {0,0,0,0,0,0};
    int i;
    for(i=0; i<COUNTS_SOFTAP_CLIENT_TABLE; i++)
    {
        if(memcmp(ZeroArray, drv_wifi_softap_client_table[i].mac,6) == 0)
        {
            break;
        }
    }
    if(i<COUNTS_SOFTAP_CLIENT_TABLE)
    {
        memcpy(drv_wifi_softap_client_table[i].mac, NewMac,6);
        return i;
    }
    else
        return -1;
}
static void SoftApClientTable_Del_OldMac(uint8_t *NewMac)
{
   int i;
   uint8_t ZeroArray[6] = {0,0,0,0,0,0};
    for(i=0; i<COUNTS_SOFTAP_CLIENT_TABLE; i++)
    {
        if(memcmp(NewMac, drv_wifi_softap_client_table[i].mac,6) == 0)
        {
            break;
        }
    }
    if(i<COUNTS_SOFTAP_CLIENT_TABLE)
    {
        memcpy(drv_wifi_softap_client_table[i].mac, ZeroArray,6);
    }
}
bool DRV_WIFI_SoftAPClientTable_Refresh(DRV_WIFI_MGMT_INDICATE_SOFT_AP_EVENT *p_softApEvent)
{
    int position = 0;
    uint8_t *addr = p_softApEvent->address;

    if (p_softApEvent->event == DRV_WIFI_SOFTAP_EVENT_CONNECTED)
    {
        position = SoftApClientTable_LookForMac(addr);
        if(position == -1)
            SoftApClientTable_Add_NewMac(addr);
    }
    else if (p_softApEvent->event == DRV_WIFI_SOFTAP_EVENT_DISCONNECTED)
    {
         position = SoftApClientTable_LookForMac(addr);
        if(position != -1)
            SoftApClientTable_Del_OldMac(addr);
    }
    return true;
}
void DRV_WIFI_SoftAPClientTable_Init(void)
{
    int i;
    for(i=0; i<COUNTS_SOFTAP_CLIENT_TABLE; i++)
    {
        drv_wifi_softap_client_table[i].mac[0] = 0;
        drv_wifi_softap_client_table[i].mac[1] = 0;
        drv_wifi_softap_client_table[i].mac[2] = 0;
        drv_wifi_softap_client_table[i].mac[3] = 0;
        drv_wifi_softap_client_table[i].mac[4] = 0;
        drv_wifi_softap_client_table[i].mac[5] = 0;
    }
}
bool DRV_WIFI_SoftAPClientTable_IsClientInTable( uint8_t *MacAddr)
{
    int position;
    position = SoftApClientTable_LookForMac(MacAddr);
    if(position == -1) return false;
    return true;
}


void DRV_WIFI_SoftAPClientTable_print(void)
{
    int i;

    SYS_CONSOLE_MESSAGE("SoftAP Client Table: \r\n--------------------\r\n");
    for(i=0;i<COUNTS_SOFTAP_CLIENT_TABLE;i++)
    {
        SYS_CONSOLE_PRINT("%02x:%02x:%02x:%02x:%02x:%02x\r\n",
                drv_wifi_softap_client_table[i].mac[0],
                drv_wifi_softap_client_table[i].mac[1],
                drv_wifi_softap_client_table[i].mac[2],
                drv_wifi_softap_client_table[i].mac[3],
                drv_wifi_softap_client_table[i].mac[4],
                drv_wifi_softap_client_table[i].mac[5]);
    }
}
//#endif
#endif