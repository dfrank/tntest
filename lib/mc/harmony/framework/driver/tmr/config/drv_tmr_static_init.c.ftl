<#--
/*******************************************************************************
  Timer Driver Initialization File

  File Name:
    drv_tmr_static_init.c.ftl

  Summary:
    This file contains source code necessary to initialize the TMR driver.

  Description:
    This file contains source code necessary to initialize the system.  It
    implements the "SYS_Initialize" function, configuration bits, and allocates
    any necessary global system resources, such as the systemObjects structure
    that contains the object handles to all the MPLAB Harmony module objects in
    the system.
 *******************************************************************************/

/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

MTMRrochip lTMRenses to you the right to use, modify, copy and distribute
Software only when embedded on a MTMRrochip mTMRrocontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublTMRense terms in the accompanying lTMRense agreement).

You should refer to the lTMRense agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTTMRULAR PURPOSE.
IN NO EVENT SHALL MTMRROCHIP OR ITS LTMRENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRTMRT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVTMRES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->
<#if CONFIG_USE_DRV_TMR == true>
<#if CONFIG_DRV_TMR_DRIVER_MODE == "STATIC">
<#if CONFIG_DRV_TMR_INST_0 == true>
/*******************************************************************************
  Function:
    void DRV_TMR0_Initialize(void)

  Summary:
    Initializes Timer Driver Instance 0

  Remarks:
 */
void DRV_TMR0_Initialize(void)
{
    /* Setup TMR0 Instance */	
    PLIB_TMR_Stop(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX0}); /* Disable Timer */
    PLIB_TMR_ClockSourceSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_TMR_CLOCK_SOURCE_ST_IDX0}); /* Select clock source */
    PLIB_TMR_PrescaleSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_TMR_PRESCALE_IDX0}); /* Select prescalar value */
<#if CONFIG_DRV_TMR_OPERATION_MODE_IDX0 == "DRV_TMR_OPERATION_MODE_32_BIT">
    PLIB_TMR_Mode32BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX0}); /* Enable 32 bit mode */
    PLIB_TMR_Counter32BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX0}); /* Clear counter */
    PLIB_TMR_Period32BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_TMR_PERIOD_IDX0}); /*Set period */
<#else>
    PLIB_TMR_Mode16BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX0}); /* Enable 16 bit mode */
    PLIB_TMR_Counter16BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX0}); /* Clear counter */
    PLIB_TMR_Period16BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX0}, ${CONFIG_DRV_TMR_PERIOD_IDX0}); /*Set period */
</#if>
<#if CONFIG_DRV_TMR_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX0});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX0}, ${CONFIG_DRV_TMR_INTERRUPT_PRIORITY_IDX0});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX0}, ${CONFIG_DRV_TMR_INTERRUPT_SUB_PRIORITY_IDX0});          
</#if>
}
</#if>
<#if CONFIG_DRV_TMR_INST_1 == true>
/*******************************************************************************
  Function:
    void DRV_TMR1_Initialize(void)

  Summary:
    Initializes Timer Driver Instance 1

  Remarks:
 */
void DRV_TMR1_Initialize(void)
{
    /* Setup TMR1 Instance */	
    PLIB_TMR_Stop(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX1}); /* Disable Timer */
    PLIB_TMR_ClockSourceSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_TMR_CLOCK_SOURCE_ST_IDX1}); /* Select clock source */
    PLIB_TMR_PrescaleSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_TMR_PRESCALE_IDX1}); /* Select prescalar value */
<#if CONFIG_DRV_TMR_OPERATION_MODE_IDX1 == "DRV_TMR_OPERATION_MODE_32_BIT">
    PLIB_TMR_Mode32BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX1}); /* Enable 32 bit mode */
    PLIB_TMR_Counter32BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX1}); /* Clear counter */
    PLIB_TMR_Period32BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_TMR_PERIOD_IDX1}); /*Set period */
<#else>
    PLIB_TMR_Mode16BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX1}); /* Enable 16 bit mode */
    PLIB_TMR_Counter16BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX1}); /* Clear counter */
    PLIB_TMR_Period16BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX1}, ${CONFIG_DRV_TMR_PERIOD_IDX1}); /*Set period */
</#if>
<#if CONFIG_DRV_TMR_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX1});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX1}, ${CONFIG_DRV_TMR_INTERRUPT_PRIORITY_IDX1});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX1}, ${CONFIG_DRV_TMR_INTERRUPT_SUB_PRIORITY_IDX1});          
</#if>
}
</#if>
<#if CONFIG_DRV_TMR_INST_2 == true>
/*******************************************************************************
  Function:
    void DRV_TMR2_Initialize(void)

  Summary:
    Initializes Timer Driver Instance 2

  Remarks:
 */
void DRV_TMR2_Initialize(void)
{
    /* Setup TMR2 Instance */	
    PLIB_TMR_Stop(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX2}); /* Disable Timer */
    PLIB_TMR_ClockSourceSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_TMR_CLOCK_SOURCE_ST_IDX2}); /* Select clock source */
    PLIB_TMR_PrescaleSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_TMR_PRESCALE_IDX2}); /* Select prescalar value */
<#if CONFIG_DRV_TMR_OPERATION_MODE_IDX2 == "DRV_TMR_OPERATION_MODE_32_BIT">
    PLIB_TMR_Mode32BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX2}); /* Enable 32 bit mode */
    PLIB_TMR_Counter32BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX2}); /* Clear counter */
    PLIB_TMR_Period32BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_TMR_PERIOD_IDX2}); /*Set period */
<#else>
    PLIB_TMR_Mode16BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX2}); /* Enable 16 bit mode */
    PLIB_TMR_Counter16BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX2}); /* Clear counter */
    PLIB_TMR_Period16BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX2}, ${CONFIG_DRV_TMR_PERIOD_IDX2}); /*Set period */
</#if>
<#if CONFIG_DRV_TMR_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX2});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX2}, ${CONFIG_DRV_TMR_INTERRUPT_PRIORITY_IDX2});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX2}, ${CONFIG_DRV_TMR_INTERRUPT_SUB_PRIORITY_IDX2});          
</#if>
}
</#if>
<#if CONFIG_DRV_TMR_INST_3 == true>
/*******************************************************************************
  Function:
    void DRV_TMR3_Initialize(void)

  Summary:
    Initializes Timer Driver Instance 3

  Remarks:
 */
void DRV_TMR3_Initialize(void)
{
    /* Setup TMR3 Instance */	
    PLIB_TMR_Stop(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX3}); /* Disable Timer */
    PLIB_TMR_ClockSourceSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_TMR_CLOCK_SOURCE_ST_IDX3}); /* Select clock source */
    PLIB_TMR_PrescaleSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_TMR_PRESCALE_IDX3}); /* Select prescalar value */
<#if CONFIG_DRV_TMR_OPERATION_MODE_IDX3 == "DRV_TMR_OPERATION_MODE_32_BIT">
    PLIB_TMR_Mode32BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX3}); /* Enable 32 bit mode */
    PLIB_TMR_Counter32BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX3}); /* Clear counter */
    PLIB_TMR_Period32BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_TMR_PERIOD_IDX3}); /*Set period */
<#else>
    PLIB_TMR_Mode16BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX3}); /* Enable 16 bit mode */
    PLIB_TMR_Counter16BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX3}); /* Clear counter */
    PLIB_TMR_Period16BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX3}, ${CONFIG_DRV_TMR_PERIOD_IDX3}); /*Set period */
</#if>
<#if CONFIG_DRV_TMR_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX3});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX3}, ${CONFIG_DRV_TMR_INTERRUPT_PRIORITY_IDX3});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX3}, ${CONFIG_DRV_TMR_INTERRUPT_SUB_PRIORITY_IDX3});          
</#if>
}
</#if>
<#if CONFIG_DRV_TMR_INST_4 == true>
/*******************************************************************************
  Function:
    void DRV_TMR4_Initialize(void)

  Summary:
    Initializes Timer Driver Instance 4

  Remarks:
 */
void DRV_TMR4_Initialize(void)
{
    /* Setup TMR4 Instance */	
    PLIB_TMR_Stop(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX4}); /* Disable Timer */
    PLIB_TMR_ClockSourceSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_TMR_CLOCK_SOURCE_ST_IDX4}); /* Select clock source */
    PLIB_TMR_PrescaleSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_TMR_PRESCALE_IDX4}); /* Select prescalar value */
<#if CONFIG_DRV_TMR_OPERATION_MODE_IDX4 == "DRV_TMR_OPERATION_MODE_32_BIT">
    PLIB_TMR_Mode32BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX4}); /* Enable 32 bit mode */
    PLIB_TMR_Counter32BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX4}); /* Clear counter */
    PLIB_TMR_Period32BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_TMR_PERIOD_IDX4}); /*Set period */
<#else>
    PLIB_TMR_Mode16BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX4}); /* Enable 16 bit mode */
    PLIB_TMR_Counter16BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX4}); /* Clear counter */
    PLIB_TMR_Period16BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX4}, ${CONFIG_DRV_TMR_PERIOD_IDX4}); /*Set period */
</#if>
<#if CONFIG_DRV_TMR_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX4});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX4}, ${CONFIG_DRV_TMR_INTERRUPT_PRIORITY_IDX4});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX4}, ${CONFIG_DRV_TMR_INTERRUPT_SUB_PRIORITY_IDX4});          
</#if>
}
</#if>
<#if CONFIG_DRV_TMR_INST_5 == true>
/*******************************************************************************
  Function:
    void DRV_TMR5_Initialize(void)

  Summary:
    Initializes Timer Driver Instance 5

  Remarks:
 */
void DRV_TMR5_Initialize(void)
{
    /* Setup TMR5 Instance */	
    PLIB_TMR_Stop(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX5}); /* Disable Timer */
    PLIB_TMR_ClockSourceSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_TMR_CLOCK_SOURCE_ST_IDX5}); /* Select clock source */
    PLIB_TMR_PrescaleSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_TMR_PRESCALE_IDX5}); /* Select prescalar value */
<#if CONFIG_DRV_TMR_OPERATION_MODE_IDX5 == "DRV_TMR_OPERATION_MODE_32_BIT">
    PLIB_TMR_Mode32BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX5}); /* Enable 32 bit mode */
    PLIB_TMR_Counter32BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX5}); /* Clear counter */
    PLIB_TMR_Period32BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_TMR_PERIOD_IDX5}); /*Set period */
<#else>
    PLIB_TMR_Mode16BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX5}); /* Enable 16 bit mode */
    PLIB_TMR_Counter16BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX5}); /* Clear counter */
    PLIB_TMR_Period16BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX5}, ${CONFIG_DRV_TMR_PERIOD_IDX5}); /*Set period */
</#if>
<#if CONFIG_DRV_TMR_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX5});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX5}, ${CONFIG_DRV_TMR_INTERRUPT_PRIORITY_IDX5});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX5}, ${CONFIG_DRV_TMR_INTERRUPT_SUB_PRIORITY_IDX5});          
</#if>
}
</#if>
<#if CONFIG_DRV_TMR_INST_6 == true>
/*******************************************************************************
  Function:
    void DRV_TMR6_Initialize(void)

  Summary:
    Initializes Timer Driver Instance 6

  Remarks:
 */
void DRV_TMR6_Initialize(void)
{
    /* Setup TMR6 Instance */	
    PLIB_TMR_Stop(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX6}); /* Disable Timer */
    PLIB_TMR_ClockSourceSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_TMR_CLOCK_SOURCE_ST_IDX6}); /* Select clock source */
    PLIB_TMR_PrescaleSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_TMR_PRESCALE_IDX6}); /* Select prescalar value */
<#if CONFIG_DRV_TMR_OPERATION_MODE_IDX6 == "DRV_TMR_OPERATION_MODE_32_BIT">
    PLIB_TMR_Mode32BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX6}); /* Enable 32 bit mode */
    PLIB_TMR_Counter32BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX6}); /* Clear counter */
    PLIB_TMR_Period32BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_TMR_PERIOD_IDX6}); /*Set period */
<#else>
    PLIB_TMR_Mode16BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX6}); /* Enable 16 bit mode */
    PLIB_TMR_Counter16BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX6}); /* Clear counter */
    PLIB_TMR_Period16BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX6}, ${CONFIG_DRV_TMR_PERIOD_IDX6}); /*Set period */
</#if>
<#if CONFIG_DRV_TMR_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX6});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX6}, ${CONFIG_DRV_TMR_INTERRUPT_PRIORITY_IDX6});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX6}, ${CONFIG_DRV_TMR_INTERRUPT_SUB_PRIORITY_IDX6});          
</#if>
}
</#if>
<#if CONFIG_DRV_TMR_INST_7 == true>
/*******************************************************************************
  Function:
    void DRV_TMR7_Initialize(void)

  Summary:
    Initializes Timer Driver Instance 7

  Remarks:
 */
void DRV_TMR7_Initialize(void)
{
    /* Setup TMR7 Instance */	
    PLIB_TMR_Stop(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX7}); /* Disable Timer */
    PLIB_TMR_ClockSourceSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_TMR_CLOCK_SOURCE_ST_IDX7}); /* Select clock source */
    PLIB_TMR_PrescaleSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_TMR_PRESCALE_IDX7}); /* Select prescalar value */
<#if CONFIG_DRV_TMR_OPERATION_MODE_IDX7 == "DRV_TMR_OPERATION_MODE_32_BIT">
    PLIB_TMR_Mode32BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX7}); /* Enable 32 bit mode */
    PLIB_TMR_Counter32BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX7}); /* Clear counter */
    PLIB_TMR_Period32BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_TMR_PERIOD_IDX7}); /*Set period */
<#else>
    PLIB_TMR_Mode16BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX7}); /* Enable 16 bit mode */
    PLIB_TMR_Counter16BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX7}); /* Clear counter */
    PLIB_TMR_Period16BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX7}, ${CONFIG_DRV_TMR_PERIOD_IDX7}); /*Set period */
</#if>
<#if CONFIG_DRV_TMR_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX7});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX7}, ${CONFIG_DRV_TMR_INTERRUPT_PRIORITY_IDX7});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX7}, ${CONFIG_DRV_TMR_INTERRUPT_SUB_PRIORITY_IDX7});          
</#if>
}
</#if>
<#if CONFIG_DRV_TMR_INST_8 == true>
/*******************************************************************************
  Function:
    void DRV_TMR8_Initialize(void)

  Summary:
    Initializes Timer Driver Instance 8

  Remarks:
 */
void DRV_TMR8_Initialize(void)
{
    /* Setup TMR8 Instance */	
    PLIB_TMR_Stop(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX8}); /* Disable Timer */
    PLIB_TMR_ClockSourceSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_TMR_CLOCK_SOURCE_ST_IDX8}); /* Select clock source */
    PLIB_TMR_PrescaleSelect(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_TMR_PRESCALE_IDX8}); /* Select prescalar value */
<#if CONFIG_DRV_TMR_OPERATION_MODE_IDX8 == "DRV_TMR_OPERATION_MODE_32_BIT">
    PLIB_TMR_Mode32BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX8}); /* Enable 32 bit mode */
    PLIB_TMR_Counter32BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX8}); /* Clear counter */
    PLIB_TMR_Period32BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_TMR_PERIOD_IDX8}); /*Set period */
<#else>
    PLIB_TMR_Mode16BitEnable(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX8}); /* Enable 16 bit mode */
    PLIB_TMR_Counter16BitClear(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX8}); /* Clear counter */
    PLIB_TMR_Period16BitSet(${CONFIG_DRV_TMR_PERIPHERAL_ID_IDX8}, ${CONFIG_DRV_TMR_PERIOD_IDX8}); /*Set period */
</#if>
<#if CONFIG_DRV_TMR_INTERRUPT_MODE == true>    

    /* Setup Interrupt */   
    PLIB_INT_SourceEnable(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX8});
    PLIB_INT_VectorPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX8}, ${CONFIG_DRV_TMR_INTERRUPT_PRIORITY_IDX8});
    PLIB_INT_VectorSubPrioritySet(INT_ID_0, ${CONFIG_DRV_TMR_INTERRUPT_VECTOR_IDX8}, ${CONFIG_DRV_TMR_INTERRUPT_SUB_PRIORITY_IDX8});          
</#if>
}
</#if>
</#if>
</#if>
<#--
/*******************************************************************************
 End of File
*/
-->
