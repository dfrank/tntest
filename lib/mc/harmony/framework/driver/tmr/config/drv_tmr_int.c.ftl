<#--
/*******************************************************************************
  Timer Driver Interrupt Handler Template File

  File Name:
    drv_tmr_int.c

  Summary:
    This file contains source code necessary to initialize the system.

  Description:
    This file contains source code necessary to initialize the system.  It
    implements the "SYS_Initialize" function, configuration bits, and allocates
    any necessary global system resources, such as the systemObjects structure
    that contains the object handles to all the MPLAB Harmony module objects in
    the system.
 *******************************************************************************/

/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->
<#if CONFIG_DRV_TMR_INTERRUPT_MODE == true>
<#if CONFIG_DRV_TMR_INST_0 == true>
void __ISR(${CONFIG_DRV_TMR_ISR_VECTOR_IDX0}, ipl${CONFIG_DRV_TMR_INT_PRIO_NUM_IDX0}) _IntHandlerDrvTmrInstance0(void)
{
<#if CONFIG_DRV_TMR_DRIVER_MODE == "DYNAMIC">
    DRV_TMR_Tasks_ISR(sysObj.drvTmr0);
<#else>
    PLIB_INT_SourceFlagClear(INT_ID_0,${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX0});
</#if>    
}
</#if>
<#if CONFIG_DRV_TMR_INST_1 == true>
void __ISR(${CONFIG_DRV_TMR_ISR_VECTOR_IDX1}, ipl${CONFIG_DRV_TMR_INT_PRIO_NUM_IDX1}) _IntHandlerDrvTmrInstance1(void)
{
<#if CONFIG_DRV_TMR_DRIVER_MODE == "DYNAMIC">
    DRV_TMR_Tasks_ISR(sysObj.drvTmr0);
<#else>
    PLIB_INT_SourceFlagClear(INT_ID_0,${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX1});
</#if> 
}
</#if>
<#if CONFIG_DRV_TMR_INST_2 == true>
void __ISR(${CONFIG_DRV_TMR_ISR_VECTOR_IDX2}, ipl${CONFIG_DRV_TMR_INT_PRIO_NUM_IDX2}) _IntHandlerDrvTmrInstance2(void)
{
<#if CONFIG_DRV_TMR_DRIVER_MODE == "DYNAMIC">
    DRV_TMR_Tasks_ISR(sysObj.drvTmr0);
<#else>
    PLIB_INT_SourceFlagClear(INT_ID_0,${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX2});
</#if> 
}
</#if>
<#if CONFIG_DRV_TMR_INST_3 == true>
void __ISR(${CONFIG_DRV_TMR_ISR_VECTOR_IDX3}, ipl${CONFIG_DRV_TMR_INT_PRIO_NUM_IDX3}) _IntHandlerDrvTmrInstance3(void)
{
<#if CONFIG_DRV_TMR_DRIVER_MODE == "DYNAMIC">
    DRV_TMR_Tasks_ISR(sysObj.drvTmr0);
<#else>
    PLIB_INT_SourceFlagClear(INT_ID_0,${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX3});
</#if> 
}
</#if>
<#if CONFIG_DRV_TMR_INST_4 == true>
void __ISR(${CONFIG_DRV_TMR_ISR_VECTOR_IDX4}, ipl${CONFIG_DRV_TMR_INT_PRIO_NUM_IDX4}) _IntHandlerDrvTmrInstance4(void)
{
<#if CONFIG_DRV_TMR_DRIVER_MODE == "DYNAMIC">
    DRV_TMR_Tasks_ISR(sysObj.drvTmr0);
<#else>
    PLIB_INT_SourceFlagClear(INT_ID_0,${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX4});
</#if> 
}
</#if>
<#if CONFIG_DRV_TMR_INST_5 == true>
void __ISR(${CONFIG_DRV_TMR_ISR_VECTOR_IDX5}, ipl${CONFIG_DRV_TMR_INT_PRIO_NUM_IDX5}) _IntHandlerDrvTmrInstance5(void)
{
<#if CONFIG_DRV_TMR_DRIVER_MODE == "DYNAMIC">
    DRV_TMR_Tasks_ISR(sysObj.drvTmr0);
<#else>
    PLIB_INT_SourceFlagClear(INT_ID_0,${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX5});
</#if> 
}
</#if>
<#if CONFIG_DRV_TMR_INST_6 == true>
void __ISR(${CONFIG_DRV_TMR_ISR_VECTOR_IDX6}, ipl${CONFIG_DRV_TMR_INT_PRIO_NUM_IDX6}) _IntHandlerDrvTmrInstance6(void)
{
<#if CONFIG_DRV_TMR_DRIVER_MODE == "DYNAMIC">
    DRV_TMR_Tasks_ISR(sysObj.drvTmr0);
<#else>
    PLIB_INT_SourceFlagClear(INT_ID_0,${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX6});
</#if> 
}
</#if>
<#if CONFIG_DRV_TMR_INST_7 == true>
void __ISR(${CONFIG_DRV_TMR_ISR_VECTOR_IDX7}, ipl${CONFIG_DRV_TMR_INT_PRIO_NUM_IDX7}) _IntHandlerDrvTmrInstance7(void)
{
<#if CONFIG_DRV_TMR_DRIVER_MODE == "DYNAMIC">
    DRV_TMR_Tasks_ISR(sysObj.drvTmr0);
<#else>
    PLIB_INT_SourceFlagClear(INT_ID_0,${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX7});
</#if> 
}
</#if>
<#if CONFIG_DRV_TMR_INST_8 == true>
void __ISR(${CONFIG_DRV_TMR_ISR_VECTOR_IDX8}, ipl${CONFIG_DRV_TMR_INT_PRIO_NUM_IDX8}) _IntHandlerDrvTmrInstance8(void)
{
<#if CONFIG_DRV_TMR_DRIVER_MODE == "DYNAMIC">
    DRV_TMR_Tasks_ISR(sysObj.drvTmr0);
<#else>
    PLIB_INT_SourceFlagClear(INT_ID_0,${CONFIG_DRV_TMR_INTERRUPT_SOURCE_IDX8});
</#if> 
}
</#if>
</#if> <#--CONFIG_DRV_TMR_INTERRUPT_MODE == true-->

<#--
/*******************************************************************************
 End of File
*/
-->
