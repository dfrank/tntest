/*******************************************************************
  Company:
    Microchip Technology Inc.
    
  File Name:
    gfx_display_driver.h

  Summary:
    GFX Display Driver definitions
    
  Description:
    GFX Display Driver definitions
    
    This file describes the GFX Display Driver specific definitions.
  *******************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to  you  the  right  to  use,  modify,  copy  and  distribute
Software only when embedded on a Microchip  microcontroller  or  digital  signal
controller  that  is  integrated  into  your  product  or  third  party  product
(pursuant to the  sublicense  terms  in  the  accompanying  license  agreement).

You should refer  to  the  license  agreement  accompanying  this  Software  for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS  WITHOUT  WARRANTY  OF  ANY  KIND,
EITHER EXPRESS  OR  IMPLIED,  INCLUDING  WITHOUT  LIMITATION,  ANY  WARRANTY  OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A  PARTICULAR  PURPOSE.
IN NO EVENT SHALL MICROCHIP OR  ITS  LICENSORS  BE  LIABLE  OR  OBLIGATED  UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,  BREACH  OF  WARRANTY,  OR
OTHER LEGAL  EQUITABLE  THEORY  ANY  DIRECT  OR  INDIRECT  DAMAGES  OR  EXPENSES
INCLUDING BUT NOT LIMITED TO ANY  INCIDENTAL,  SPECIAL,  INDIRECT,  PUNITIVE  OR
CONSEQUENTIAL DAMAGES, LOST  PROFITS  OR  LOST  DATA,  COST  OF  PROCUREMENT  OF
SUBSTITUTE  GOODS,  TECHNOLOGY,  SERVICES,  OR  ANY  CLAIMS  BY  THIRD   PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE  THEREOF),  OR  OTHER  SIMILAR  COSTS.
*******************************************************************************/

#ifndef DRV_GFX_DISPLAYDRIVER_H
    #define DRV_GFX_DISPLAYDRIVER_H
// DOM-IGNORE-END

#include "system_config.h"
#include "gfx/gfx_common.h"
#include "driver/pmp/drv_pmp.h"
#include "system_definitions.h"

// DOM-IGNORE-BEGIN
#if !defined (GFX_CONFIG_DRIVER_COUNT)
    #define GFX_CONFIG_DRIVER_COUNT 1
#endif
// DOM-IGNORE-END

// *****************************************************************************
/*

  GFX Driver Module Index Numbers

  Summary:
    GFX display driver index definitions.

  Description:
    These constants provide the GFX display driver index definitions.

  Remarks:
    These constants should be used in place of hard-coded numeric literals.

    These values should be passed into the DRV_GFX_<driver>_Initialize and
    DRV_GFX_<driver>_Open functions to identify the driver instance in use.
*/

typedef enum {

    DRV_GFX_INDEX_0 = 0,
    DRV_GFX_INDEX_1,
    DRV_GFX_INDEX_2,
    DRV_GFX_INDEX_3,
    NUMBER_OF_DRV_INDEXES

} DRV_GFX_INDEX;


// *****************************************************************************
/*  Data type for GFX DRV handle.

  Summary:
    Data type for GFX DRV handle.

  Description:
    The data type of the handle that is returned from GFX_DRV_Open
    function.

  Remarks:
    None.
*/

typedef uintptr_t DRV_GFX_HANDLE;

// *****************************************************************************
/* GFX Device Layer Invalid Handle

  Summary:
    Constant that defines the value of an Invalid Device Handle.

  Description:
    This constant is returned by the DRV_GFX_Open() function when the
    function fails.

  Remarks:
    None.
*/

#define DRV_GFX_HANDLE_INVALID /*DOM-IGNORE-BEGIN*/((DRV_GFX_HANDLE)(-1))/*DOM-IGNORE-END*/

// *****************************************************************************
/*
  Driver GFX Supported Display Drivers

  Summary:
    GFX supported Graphics Display Drivers.

  Description:
    These constants provide the GFX display driver index definitions.

  Remarks:
    These constants should be used in place of hard-coded numeric literals.

    These values should be passed into the DRV_GFX_Open functions to
    identify the driver to use.
*/

typedef enum {
    DRV_GFX_SSD1926 = 0
} DRV_GFX_DISPLAY_DRIVER;

// *****************************************************************************
/* Driver State Machine States

   Summary
    Defines the various states that can be achieved by the driver operation.

   Description
    This enumeration defines the various states that can be achieved by the
    driver operation.

   Remarks:
    None.
*/

typedef enum
{
    /*  Driver state busy */
    DRV_GFX_STATE_BUSY,

    /*  Driver state init */
    DRV_GFX_STATE_INIT,

} DRV_GFX_STATES;


#define GFX_BUFFER1 (0)
#define GFX_BUFFER2 (1)

#ifndef GFX_USE_DISPLAY_CONTROLLER_LCC
// DOM-IGNORE-BEGIN
extern inline void __attribute__ ((always_inline)) DriverInterfaceInit(DRV_HANDLE *pmphandle, DRV_PMP_MODE_CONFIG *config)
{ 
    #if !defined(__PIC32MZ__)
    uint32_t pClockPeriod = (1000000000ul) / (SYS_DEVCON_PIC32MX_MAX_PB_FREQ / (1 << OSCCONbits.PBDIV));
    #else
    uint32_t pClockPeriod = (1000000000ul) / (SYS_DEVCON_SYSTEM_CLOCK / (1 << PB2DIVbits.PBDIV));
    #endif

    // Open the instance DRV_PMP_INDEX_0 with Non-blocking and Shared intent
    *pmphandle = DRV_PMP_Open(DRV_PMP_INDEX_0, DRV_IO_INTENT_SHARED | DRV_IO_INTENT_NONBLOCKING);

    if( *pmphandle == DRV_HANDLE_INVALID )
    {
        // Client cannot open the instance.
    }

    config->pmpMode = PMP_MASTER_READ_WRITE_STROBES_INDEPENDENT;
    #ifdef USE_16BIT_PMP
    config->portSize = PMP_DATA_SIZE_16_BITS;
    #else
    config->portSize = PMP_DATA_SIZE_8_BITS;
    #endif


    #if (PMP_DATA_SETUP_TIME == 0)
        config->waitStates.dataWait = 0;
    #else
        if (PMP_DATA_SETUP_TIME <= pClockPeriod)
            config->waitStates.dataWait = 0;
        else if (PMP_DATA_SETUP_TIME > pClockPeriod)
            config->waitStates.dataWait = (PMP_DATA_SETUP_TIME / pClockPeriod) + 1;
    #endif

    #if (PMP_DATA_WAIT_TIME == 0)
        config->waitStates.strobeWait = 0;
    #else
        if (PMP_DATA_WAIT_TIME <= pClockPeriod)
            config->waitStates.strobeWait = 1;
        else if (PMP_DATA_WAIT_TIME > pClockPeriod)
            config->waitStates.strobeWait = (PMP_DATA_WAIT_TIME / pClockPeriod) + 1;
    #endif

    #if (PMP_DATA_HOLD_TIME == 0)
        config->waitStates.dataHoldWait = 0;
    #else
        if (PMP_DATA_HOLD_TIME <= pClockPeriod)
            config->waitStates.dataHoldWait = 0;
        else if (PMP_DATA_HOLD_TIME > pClockPeriod)
            config->waitStates.dataHoldWait = (PMP_DATA_HOLD_TIME / pClockPeriod) + 1;
    #endif

    //config->intMode = PMP_INTERRUPT_EVERY_RW_CYCLE;

    // Configure the client
    DRV_PMP_ModeConfig ( *pmphandle, *config );

    _DRV_PMP_HardwareConfig ((DRV_HANDLE)*pmphandle);

}
#endif
// DOM-IGNORE-END

// DOM-IGNORE-BEGIN
/*********************************************************************
* Function: Alpha Macros
********************************************************************/
#define 	NUM_ALPHA_LEVELS 	0x20			// Specific to device
#define 	ALPHA_DELTA 		((NUM_ALPHA_LEVELS) << 5)
/*********************************************************************
* Function: BYTE Percentage2Alpha(BYTE alphaPercentage)
********************************************************************/
extern inline uint8_t __attribute__ ((always_inline)) Percentage2Alpha(uint8_t alphaPercentage)
{
    uint32_t percent = (uint32_t)(alphaPercentage);
    percent *= (ALPHA_DELTA);
    percent /= 100;
    percent >>= 5;

    return (uint8_t)percent;
}
// DOM-IGNORE-END

typedef struct
{
   SYS_MODULE_OBJ (*init) (const SYS_MODULE_INDEX   index,
                          const SYS_MODULE_INIT    * const init);

   DRV_HANDLE      (*open) ( const SYS_MODULE_INDEX index,
                                 const DRV_IO_INTENT intent );
   
   DRV_GFX_INTERFACE * (*interfaceGet)( DRV_HANDLE handle );

} DRV_CLIENT_INTERFACE;

// *****************************************************************************
/*  Driver Module Client Status

  Summary:
    Enumerated data type that identifies the  Driver Module Client Status.

  Description:
    This enumeration defines the possible status of the  Driver Module Client.
    It is returned by the () function.

  Remarks:
    None.
*/

typedef enum
{
     /* Client is closed or the specified handle is invalid */
    DRV_GFX_STATUS_CLOSED
            /*DOM-IGNORE-BEGIN*/ = 0 /*DOM-IGNORE-END*/,

    /* Client is ready */
    DRV_GFX_STATUS_READY
            /*DOM-IGNORE-BEGIN*/ = 1 /*DOM-IGNORE-END*/

} DRV_GFX_CLIENT_STATUS;

// *****************************************************************************
/*  GFX Driver client object structure

  Summary:
    GFX Driver client object structure.

  Description:
    GFX Driver client object structure.

  Remarks:
   None.
*/

typedef struct
{
    /* Client status */
    DRV_GFX_CLIENT_STATUS         clientState;

    /* Set to true if this object is in use */
    bool                          inUse;

    /* Object from which the client is derived */
    void *                        drvObj;

} DRV_GFX_CLIENT_OBJ;

// DOM-IGNORE-BEGIN
// *********************************************************************
/* 
  Enumeration: PAGE_TYPE

  Summary: types of pages supported by the GFX Library

  Description: 

*/
typedef enum
{
    ACTIVE_PAGE   = 0,
    VISUAL_PAGE,
    BACKGROUND_PAGE,
    FOREGROUND_PAGE,
    DESTINATION_PAGE,
    TRANSITION_PAGE 
} PAGE_TYPE;
// DOM-IGNORE-END

// DOM-IGNORE-BEGIN
// *********************************************************************
/* 
  Enumeration: LAYER_TYPE

  Summary: types of Layers supported by the GFX Library

  Description: 

*/
typedef enum
{
    PIP1 = 0,
    PIP2
} LAYER_TYPE;
// DOM-IGNORE-END


#if(GFX_CONFIG_DRIVER_COUNT > 1)
    #define DRV_GFX_BarFill(left,top,right,bottom)          gfxDrv->BarFill(left,top,right,bottom)
    #define DRV_GFX_PixelPut(x,y)                           gfxDrv->PixelPut(x,y)
#else
    #ifdef GFX_USE_DISPLAY_CONTROLLER_LCC
    #define DRV_GFX_BarFill(left,top,right,bottom)          DRV_GFX_LCC_BarFill(left,top,right,bottom)
    #define DRV_GFX_PixelPut(x,y)                           DRV_GFX_LCC_PixelPut(x,y)
    #define DRV_GFX_PixelArrayPut(array, left, top, width)  DRV_GFX_LCC_PixelArrayPut(array, left, top, width, 1)
    #endif
    #ifdef GFX_USE_DISPLAY_CONTROLLER_S1D13517
    #define DRV_GFX_BarFill(left,top,right,bottom)          DRV_GFX_S1D13517_BarFill(left,top,right,bottom)
    #define DRV_GFX_PixelPut(x,y)                           DRV_GFX_S1D13517_PixelPut(x,y)
    #define DRV_GFX_PixelArrayPut(array, left, top, width)  DRV_GFX_S1D13517_PixelArrayPut(array, left, top, width,1)
    #endif
    #ifdef  GFX_USE_DISPLAY_CONTROLLER_SSD1926
    #define DRV_GFX_BarFill(left,top,right,bottom)          DRV_GFX_SSD1926_BarFill(left,top,right,bottom)
    #define DRV_GFX_PixelPut(x,y)                           DRV_GFX_SSD1926_PixelPut(x,y)
    #define DRV_GFX_PixelArrayPut(array, left, top, width)  DRV_GFX_SSD1926_PixelArrayPut(array, left, top, width,1)
    #endif
    #ifdef  GFX_USE_DISPLAY_CONTROLLER_OTM2201A
    #define DRV_GFX_BarFill(left,top,right,bottom)          DRV_GFX_OTM2201A_BarFill(left,top,right,bottom)
    #define DRV_GFX_PixelPut(x,y)                           DRV_GFX_OTM2201A_PixelPut(x,y)
    #define DRV_GFX_PixelArrayPut(array, left, top, width)  DRV_GFX_OTM2201A_PixelArrayPut(array, left, top, width,1)
    #endif
#endif

#endif
