/*******************************************************************************
 New haven 5.0" 800 x 480 PCAP display board configuration.

 Company:
    Microchip Technology Inc.

 File Name:
    drv_gfx_newhaven_5.0_800x480_PCAP.h

 Summary:
    New haven 5.0" 800 x 480 PCAP display board configuration.

 Description:
    This contains all the configuration that is required to be done for the
    application running on PIC32 Graphics 5.0" Display Board
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/

#ifndef WVGA_BSP_CONFIG_H
#define WVGA_BSP_CONFIG_H

// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Data Types and Constants
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* New haven 5.0" 800 x 480 PCAP display orientation.

  Summary:
    Display orientation.

  Description:
    Defines the display rotation with respect to its native orientation. For
    example, if the display has a resolution specifications that says
    240x320 (QVGA), the display is natively in portrait mode. If the application
    uses the display in landscape mode (320x240), then the orientation musts be
    defined as 90 or 180 degree rotation. The Graphics Library will calculate
    the actual pixel location to rotate the contents of the screen. So when
    users view the display, the image on the screen will come out in the correct
    orientation.

    Valid values: 0, 90, 180 and 270
    Example: #define DISP_ORIENTATION 90

  Remarks:
    This constant should be used in place of hard-coded numeric literals.
    This value is device-specific.
*/

#define DISP_ORIENTATION		0

// *****************************************************************************
/* New haven 5.0" 800 x 480 PCAP display horizontal resolution.

  Summary:
    Display horizontal resolution.

  Description:
    Defines the horizontal dimension in pixels. This is the native horizontal
    dimension of the screen. In the example given in DISP_ORIENTATION, a 320x240
    display will have DISP_HOR_RESOLUTION of 320.

    Valid Values: dependent on the display glass resolution used.
    Example: #define DISP_HOR_RESOLUTION 320

  Remarks:
    This constant should be used in place of hard-coded numeric literals.
    This value is device-specific.
*/

#define DISP_HOR_RESOLUTION		400

// *****************************************************************************
/* New haven 5.0" 800 x 480 PCAP display vertical resolution.

  Summary:
    Display vertical resolution.

  Description:
    Defines the vertical dimension in pixels. This is the native vertical
    dimension of the screen. In the example given in DISP_ORIENTATION, a 320x240
    display will have DISP_VER_RESOLUTION of 240.

    Valid Values: dependent on the display glass resolution used.
    Example: #define DISP_VER_RESOLUTION 240

  Remarks:
    This constant should be used in place of hard-coded numeric literals.
    This value is device-specific.
*/

#define DISP_VER_RESOLUTION		240

// *****************************************************************************
/* New haven 5.0" 800 x 480 PCAP display data width.

  Summary:
    Display data width.

  Description:
    This defines the display controller physical interface to the display panel.

    Valid Values: 1, 4, 8, 16, 18, 24
                  1, 4, 8 are usually used in MSTN and CSTN displays
                  16, 18 and 24 are usually used in TFT displays.
    Example: #define DISP_DATA_WIDTH 18

  Remarks:
    This constant should be used in place of hard-coded numeric literals.
    This value is device-specific.
*/

#define DISP_DATA_WIDTH		24

// *****************************************************************************
/* New haven 5.0" 800 x 480 PCAP display horizontal pulse width.

  Summary:
    Display horizontal pulse width.

  Description:
    This defines the horizontal sync signal pulse width.

    Valid Values: See display panel data sheet
    Example: #define DISP_HOR_PULSE_WIDTH 25

  Remarks:
    This constant should be used in place of hard-coded numeric literals.
    This value is device-specific.
*/

#define DISP_HOR_PULSE_WIDTH    0

// *****************************************************************************
/* New haven 5.0" 800 x 480 PCAP display horizontal back porch.

  Summary:
    Display horizontal back porch.

  Description:
    This defines the horizontal back porch.
    DISP_HOR_BACK_PORCH + DISP_HOR_FRONT_PORCH + DISP_HOR_PULSE_WIDTH makes up
    the horizontal blanking period.

    Valid Values: See display panel data sheet
    Example: #define DISP_HOR_BACK_PORCH 5

  Remarks:
    This constant should be used in place of hard-coded numeric literals.
    This value is device-specific.
*/

#define DISP_HOR_BACK_PORCH     88

// *****************************************************************************
/* New haven 5.0" 800 x 480 PCAP display horizontal front porch.

  Summary:
    Display horizontal front porch.

  Description:
    This defines the horizontal front porch.
    DISP_HOR_BACK_PORCH + DISP_HOR_FRONT_PORCH + DISP_HOR_PULSE_WIDTH makes up
    the horizontal blanking period.

    Valid Values: See display panel data sheet
    Example: #define DISP_HOR_FRONT_PORCH 10

  Remarks:
    This constant should be used in place of hard-coded numeric literals.
    This value is device-specific.
*/

#define DISP_HOR_FRONT_PORCH    40

// *****************************************************************************
/* New haven 5.0" 800 x 480 PCAP display vertical pulse width.

  Summary:
    Display vertical pulse width.

  Description:
    This defines the vertical sync signal pulse width.

    Valid Values: See display panel data sheet
    Example: #define DISP_VER_PULSE_WIDTH 4

  Remarks:
    This constant should be used in place of hard-coded numeric literals.
    This value is device-specific.
*/

#define DISP_VER_PULSE_WIDTH    0

// *****************************************************************************
/* New haven 5.0" 800 x 480 PCAP display vertical back porch.

  Summary:
    Display vertical back porch.

  Description:
    This defines the vertical back porch.
    DISP_VER_BACK_PORCH + DISP_VER_FRONT_PORCH + DISP_VER_PULSE_WIDTH makes up
    the horizontal blanking period.

    Valid Values: See display panel data sheet
    Example: #define DISP_VER_BACK_PORCH 0

  Remarks:
    This constant should be used in place of hard-coded numeric literals.
    This value is device-specific.
*/

#define DISP_VER_BACK_PORCH     34

// *****************************************************************************
/* New haven 5.0" 800 x 480 PCAP display vertical front porch.

  Summary:
    Display vertical front porch.

  Description:
    This defines the horizontal front porch.
    DISP_VER_BACK_PORCH + DISP_VER_FRONT_PORCH + DISP_VER_PULSE_WIDTH makes up
    the horizontal blanking period.

    Valid Values: See display panel data sheet
    Example: #define DISP_VER_FRONT_PORCH 2

  Remarks:
    This constant should be used in place of hard-coded numeric literals.
    This value is device-specific.
*/

#define DISP_VER_FRONT_PORCH    0

// *****************************************************************************
/* New haven 5.0" 800 x 480 PCAP display inverting level shift.

  Summary:
    Display inverting level shift.

  Description:
    This indicates that the color data is sampled in the falling edge of the
    pixel clock.
    Example: #define DISP_INV_LSHIFT - define this to sample the color data in
             the falling edge of the pixel clock.

  Remarks:
    This constant should be used in place of hard-coded numeric literals.
    This value is device-specific.
*/

#define DISP_INV_LSHIFT         0

// *****************************************************************************
/* New haven 5.0" 800 x 480 PCAP LCD display type.

  Summary:
    LCD display type.

  Description:
    This defines the type of display glass used.

    Valid Values: Dependent on the display controller supported LCD types.
                  GFX_LCD_TFT  // Type TFT Display
                  GFX_LCD_CSTN // Type Color STN Display
                  GFX_LCD_MSTN // Type Mono STN Display
                  GFX_LCD_OLED // Type OLED Display

    Example: #define GFX_LCD_TYPE GFX_LCD_TFT

  Remarks:
    This constant should be used in place of hard-coded numeric literals.
    This value is device-specific.
*/

#define GFX_LCD_TYPE            GFX_LCD_TFT

// *****************************************************************************
/* New haven 5.0" 800 x 480 PCAP TCON module interface.

  Summary:
    TCON module interface.

  Description:
    TCON module interface if applicable.

  Remarks:
    This constant should be used in place of hard-coded numeric literals.
    This value is device-specific.
*/

#define TCON_MODULE     NULL

// *****************************************************************************
/* New haven 5.0" 800 x 480 PCAP display backlight enable level.

  Summary:
    Display backlight enable level.

  Description:
    Display backlight enable level.

  Remarks:
    This constant should be used in place of hard-coded numeric literals.
    This value is device-specific.
*/

#define BACKLIGHT_ENABLE_LEVEL      1

// *****************************************************************************
/* New haven 5.0" 800 x 480 PCAP display backlight disable level.

  Summary:
    Display backlight disable level.

  Description:

  Remarks:
    This constant should be used in place of hard-coded numeric literals.
    This value is device-specific.
*/

#define BACKLIGHT_DISABLE_LEVEL     0

// *****************************************************************************
// *****************************************************************************
// Section: Interface Routines
// *****************************************************************************
// *****************************************************************************

// DOM-IGNORE-BEGIN

// *****************************************************************************
/* Function: void BSP_Initialize(void)

  Summary:
    Performs the neccassary actions to initialize a board

  Description:
    This routine performs the neccassary actions to initialize a board
*/

void BSP_Initialize(void);


#endif //QVGA_BSP_CONFIG_H

// DOM-IGNORE-END

/*******************************************************************************
 End of File
*/
