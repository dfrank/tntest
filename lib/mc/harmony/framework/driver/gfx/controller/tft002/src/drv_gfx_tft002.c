/*******************************************************************************
  Company:
    Microchip Technology Incorporated

  File Name:
    drv_gfx_TFT002.c
    
  Summary:
    Interface for the graphics library where the primitives are renderred and sent to the graphics controller
    either external or internal

  Description:
    None
*******************************************************************************/
//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute Software
only when embedded on a Microchip microcontroller or digital  signal  controller
that is integrated into your product or third party  product  (pursuant  to  the
sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS  WITHOUT  WARRANTY  OF  ANY  KIND,
EITHER EXPRESS  OR  IMPLIED,  INCLUDING  WITHOUT  LIMITATION,  ANY  WARRANTY  OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A  PARTICULAR  PURPOSE.
IN NO EVENT SHALL MICROCHIP OR  ITS  LICENSORS  BE  LIABLE  OR  OBLIGATED  UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,  BREACH  OF  WARRANTY,  OR
OTHER LEGAL  EQUITABLE  THEORY  ANY  DIRECT  OR  INDIRECT  DAMAGES  OR  EXPENSES
INCLUDING BUT NOT LIMITED TO ANY  INCIDENTAL,  SPECIAL,  INDIRECT,  PUNITIVE  OR
CONSEQUENTIAL DAMAGES, LOST  PROFITS  OR  LOST  DATA,  COST  OF  PROCUREMENT  OF
SUBSTITUTE  GOODS,  TECHNOLOGY,  SERVICES,  OR  ANY  CLAIMS  BY  THIRD   PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE  THEREOF),  OR  OTHER  SIMILAR  COSTS.
*******************************************************************************/
//DOM-IGNORE-END
#include "../drv_gfx_tft002.h"

static DRV_GFX_INTERFACE lccInterface;
DRV_HANDLE       pmphandle;
DRV_PMP_MODE_CONFIG config;
DRV_GFX_TFT002_COMMAND  commandBuffer[DRV_GFX_TFT002_COMMANDQUEUESIZE];  //This contains the commands to send to the graphics driver
DRV_GFX_TFT002_COMMAND* queueIndex = &commandBuffer[0];                             //pointer to where the command Buffer is currently
volatile uint16_t queueCount = 0;
volatile uint16_t driverBusy = 0;  
volatile uint8_t driverTaskBusy = 0;  /*Flag to state driver is busy*/
uint8_t taskReady = 0;
uint8_t instances[GFX_CONFIG_DRIVER_COUNT] = {0};
uint8_t   instance = 0;  /*global instance for the driver*/
uint8_t   dataWidth;     /*PMP Data Bus Width*/
#define DELAY 0xFFFF


// *****************************************************************************
/* TFT002 Driver task states

  Summary
    Lists the different states that TFT002 task routine can have.

  Description
    This enumeration lists the different states that TFT002 task routine can have.

  Remarks:
    None.
*/

typedef enum
{
    /* Process queue */
    DRV_GFX_TFT002_INITIALIZE_START,

    DRV_GFX_TFT002_INITIALIZE_PWR_UP,

    DRV_GFX_TFT002_INITIALIZE_SET_REGISTERS,

    /* GFX TFT002 task initialization done */
    DRV_GFX_TFT002_INITIALIZE_DONE,

} DRV_GFX_TFT002_OBJECT_TASK;

// *****************************************************************************
/* GFX TFT002 Driver Instance Object

  Summary:
    Defines the object required for the maintenance of the hardware instance.

  Description:
    This defines the object required for the maintenance of the hardware
    instance. This object exists once per hardware instance of the peripheral.

  Remarks:
    None.
*/

typedef struct _DRV_GFX_TFT002_OBJ
{
    /* Flag to indicate in use  */
    bool                                        inUse;

    /* Save the index of the driver */
    SYS_MODULE_INDEX                            drvIndex;

    /* TFT002 machine state */
    DRV_GFX_STATES                              state;

    /* Status of this driver instance */
    SYS_STATUS                                  status;

    /* Number of clients */
    uint32_t                                    nClients;

    /* Client of this driver */
    DRV_GFX_CLIENT_OBJ *                        pDrvTFT002ClientObj;

    /* State of the task */
    DRV_GFX_TFT002_OBJECT_TASK    		task;

    DRV_GFX_INIT *                              initData;

} DRV_GFX_TFT002_OBJ;

static DRV_GFX_TFT002_OBJ        drvTFT002Obj[DRV_GFX_TFT002_NUMBER_OF_MODULES];

static DRV_GFX_CLIENT_OBJ drvTFT002Clients[DRV_GFX_TFT002_NUMBER_OF_MODULES];

#if defined (GFX_USE_DISPLAY_CONTROLLER_SSD1289)
    /*TFT002 Registers to be initialized*/
    const uint8_t regCount = 42;
    uint16_t registers[42][2] = {
        0x00, 0x0001, 0x03, 0xAAAC,
        0x0C, 0x0002, DELAY, 15,
        0x0D, 0x000A, 0x0E, 0x2D00,
        0x1E, 0x00BC, 0x01, 0x1A0C,
        DELAY, 15,
        #if (DISP_ORIENTATION == 0)
        0x01, 0x2B3F,
        #else
        0x01, 0x293F,
        #endif
        0x02, 0x0600,0x10, 0x0000,
        #if (DISP_ORIENTATION == 0)
        0x11, 0x60B0,
        #else
        0x11, 0x60B8,
        #endif
        0x05, 0x0000,0x06, 0x0000,
        DELAY, 100,0x16, 0xEF1C,
        0x17, 0x0003,0x07, 0x0233,
        0x0B, 0x0000,0x0F, 0x0000,
        0x41, 0x0000,0x42, 0x0000,
        0x48, 0x0000,0x49, 0x013F,
        0x44, 0xEF00,0x45, 0x0000,
        0x46, 0x013F,0x4A, 0x0000,
        0x4B, 0x0000,0x30, 0x0707,
        0x31, 0x0704,0x32, 0x0204,
        0x33, 0x0502,0x34, 0x0507,
        0x35, 0x0204,0x36, 0x0204,
        0x37, 0x0502,0x3A, 0x0302,
        0x3B, 0x1f00,0x23, 0x0000,
        0x24, 0x0000
    };
    #elif defined (GFX_USE_DISPLAY_CONTROLLER_SSD2119)
    const uint8_t regCount = 29;
    uint16_t registers[29][2] = {
        0x0028, 0x0006,0x0000, 0x0001,
        0x0010, 0x0000,
        #if (DISP_ORIENTATION == 0)
        0x0001, 0x72EF,
        #else
        0x0001, 0x32EF,
        #endif
        0x0002, 0x0600,0x0003, 0x6A38,
        #if (DISP_ORIENTATION == 0)
        0x0011, 0x6870,
        #else
        0x0011, 0x6878,
        #endif
        0x000F, 0x0000,0x000B, 0x5308,
        0x000C, 0x0003,0x000D, 0x000A,
        0x000E, 0x2E00,0x001E, 0x00BE,
        0x0025, 0x8000,0x0026, 0x7800,
        0x004E, 0x0000,0x004F, 0x0000,
        0x0012, 0x08D9,0x0030, 0x0000,
        0x0031, 0x0104,0x0032, 0x0100,
        0x0033, 0x0305,0x0034, 0x0505,
        0x0035, 0x0305,0x0036, 0x0707,
        0x0037, 0x0300,0x003A, 0x1200,
        0x003B, 0x0800,0x0007, 0x0033
    };
    #endif

// *****************************************************************************
/*
  Function: uint16_t DRV_GFX_TFT002_Initialize(uint8_t instance)

  Summary:
    resets LCD, initializes PMP

  Description:
    none

  Input:
        instance - driver instance
  Output:
    1 - call not successful (PMP driver busy)
    0 - call successful
*/
SYS_MODULE_OBJ DRV_GFX_TFT002_Initialize(const SYS_MODULE_INDEX   index,
                                          const SYS_MODULE_INIT    * const init)
{
    DRV_GFX_INIT * drvInit;

    /* Validate the driver index */
    if ( index >= DRV_GFX_TFT002_INDEX_COUNT )
    {
        return SYS_MODULE_OBJ_INVALID;
    }

    DRV_GFX_TFT002_OBJ *dObj = &drvTFT002Obj[index];

    /* Object is valid, set it in use */
    dObj->inUse = true;
    dObj->state = SYS_STATUS_BUSY;
    dObj->initData = (DRV_GFX_INIT *) init;

    /* Save the index of the driver. Important to know this
    as we are using reference based accessing */
    dObj->drvIndex = index;

    dObj->task = DRV_GFX_TFT002_INITIALIZE_SET_REGISTERS;

    DRV_GFX_TFT002_OBJ *dObj = (DRV_GFX_TFT002_OBJ*)object;

    static uint8_t   state =0;
    static uint16_t  horizontalSize, verticalSize, rotation;

    switch ( dObj->task )
    {
        case DRV_GFX_TFT002_INITIALIZE_SET_REGISTERS:

           while(state < regCount)
           {
               switch(state)
               {

                    case 0:
                        DriverInterfaceInit(&pmphandle, &config);

                        DisplayResetDisable();
                        DisplayEnable();


                        if((drvTFT002Obj[0].initData->orientation == 90) || (drvTFT002Obj[0].initData->orientation == 270))
                        {
                            horizontalSize = drvTFT002Obj[0].initData->verticalResolution;
                            verticalSize   = drvTFT002Obj[0].initData->horizontalResolution;
                        }
                        else
                        {
                            horizontalSize = drvTFT002Obj[0].initData->horizontalResolution;
                            verticalSize   = drvTFT002Obj[0].initData->verticalResolution;
                        }
                        break;

                    default:
                        break;
                }

                if(*registers[state-1] == DELAY)
                {
                    GFX_TMR_DelayMS(registers[state-1][1]);
                    state++;
//TODO                    return(1);
                }

                if(DRV_GFX_TFT002_SetReg(*registers[state-1], registers[state-1][1]))
                {
//TODO                    return(1);
                }

                state++;
            }

            drvTFT002Obj[0].initData->ready = 1;
            drvTFT002Obj[0].initData->driverBusy = 0;
            drvTFT002Obj[0].initData->horizontalResolution = horizontalSize;
            drvTFT002Obj[0].initData->verticalResolution = verticalSize;
            DisplayBacklightOn();

            dObj->state = SYS_STATUS_READY;
            dObj->task = DRV_GFX_TFT002_INITIALIZE_DONE;
            break;
    }

    dObj->nClients = 0;
    dObj->status = SYS_STATUS_READY;

    /* Return the driver handle */
    return (SYS_MODULE_OBJ)dObj;

}

// *****************************************************************************
/*
  Function: DRV_GFX_TFT002_Open(uint8_t instance)

  Summary:
    opens an instance of the graphics controller

  Description:
    none

  Input:
    instance of the driver

  Output:
    1 - driver not initialied
    2 - instance doesn't exist
    3 - instance already open
    instance to driver when successful
*/
DRV_GFX_HANDLE DRV_GFX_TFT002_Open( const SYS_MODULE_INDEX index,
                             const DRV_IO_INTENT intent )
{
    DRV_GFX_CLIENT_OBJ * client = (DRV_GFX_CLIENT_OBJ *)DRV_HANDLE_INVALID;

    /* Check if the specified driver index is in valid range */
    if(index >= DRV_GFX_TFT002_NUMBER_OF_MODULES)
    {
        //SYS_DEBUG(0, "GFX_TFT002 Driver: Bad Driver Index");
    }
    /* Check if instance object is ready*/
    else if(drvTFT002Obj[index].status != SYS_STATUS_READY)
    {
        /* The TFT002 module should be ready */
//        SYS_DEBUG(0, "GFX_TFT002 Driver: Was the driver initialized?");
    }
    else if(intent != DRV_IO_INTENT_EXCLUSIVE)
    {
        /* The driver only supports this mode */
//        SYS_DEBUG(0, "GFX_TFT002 Driver: IO intent mode not supported");
    }
    else if(drvTFT002Obj[index].nClients > 0)
    {
        /* Driver supports exclusive open only */
//        SYS_DEBUG(0, "GFX_TFT002 already opened once. Cannot open again");
    }
    else
    {
        client = &drvTFT002Clients[index];

        client->inUse = true;
        client->drvObj = &drvTFT002Obj[index];

        /* Increment the client number for the specific driver instance*/
        drvTFT002Obj[index].nClients++;
    }

    /* Return invalid handle */
    return ((DRV_HANDLE)client);
}

// *****************************************************************************
/* Function:
    void DRV_GFX_TFT002_Close( DRV_HANDLE handle )

  Summary:
    closes an instance of the graphics controller

  Description:
    This is closes the instance of the driver specified by handle.
*/
void DRV_GFX_TFT002_Close( DRV_HANDLE handle )
{
    /* Start of local variable */
    DRV_GFX_CLIENT_OBJ * client = (DRV_GFX_CLIENT_OBJ *)NULL;
    DRV_GFX_TFT002_OBJ * drvObj = (DRV_GFX_TFT002_OBJ *)NULL;
    /* End of local variable */

    /* Check if the handle is valid */
    if(handle == DRV_HANDLE_INVALID)
    {
//        SYS_DEBUG(0, "Bad Client Handle");
    }
    else
    {
        client = (DRV_GFX_CLIENT_OBJ *)handle;

        if(client->inUse)
        {
            client->inUse = false;
            drvObj = (DRV_GFX_TFT002_OBJ *)client->drvObj;

            /* Remove this client from the driver client table */
            drvObj->nClients--;
        }
        else
        {
//            SYS_DEBUG(0, "Client Handle no inuse");
        }

    }
    return;
}

// *****************************************************************************
/*
  Function:
     void DRV_GFX_TFT002_MaxXGet()

  Summary:
     Returns x extent of the display.

  Description:

  Precondition:

  Parameters:

  Returns:

  Example:
    <code>
    <code>

  Remarks:
*/
uint16_t DRV_GFX_TFT002_MaxXGet( DRV_HANDLE handle )
{
    DRV_GFX_CLIENT_OBJ * client = (DRV_GFX_CLIENT_OBJ *)handle;
    DRV_GFX_TFT002_OBJ * driver = (DRV_GFX_TFT002_OBJ*)client->drvObj;

    return driver->initData->verticalResolution;
}

// *****************************************************************************
/*
  Function:
     void GFX_MaxYGet()

  Summary:
     Returns y extent of the display.

  Description:

  Precondition:

  Parameters:

  Returns:

  Example:
    <code>
    <code>

  Remarks:
*/
uint16_t DRV_GFX_TFT002_MaxYGet( DRV_HANDLE handle )
{
    DRV_GFX_CLIENT_OBJ * client = (DRV_GFX_CLIENT_OBJ *)handle;
    DRV_GFX_TFT002_OBJ * driver = (DRV_GFX_TFT002_OBJ*)client->drvObj;

    return driver->initData->horizontalResolution;
}

/*********************************************************************
  Function:
     DRV_GFX_INTEFACE DRV_GFX_TFT002_InterfaceGet( DRV_HANDLE handle )

  Summary:
    Returns the API of the graphics controller

  Description:
    none

  Return:

  *********************************************************************/
DRV_GFX_INTERFACE * DRV_GFX_TFT002_InterfaceGet( DRV_HANDLE handle )
{
    DRV_GFX_INTERFACE * interface = &lccInterface;

    interface->PixelsPut = DRV_GFX_TFT002_PixelsPut;
    interface->BarFill = NULL;
    interface->PixelArrayPut = DRV_GFX_TFT002_PixelArrayPut;
    interface->PixelArrayGet = DRV_GFX_TFT002_PixelArrayGet;
    interface->PixelPut = DRV_GFX_TFT002_PixelPut;
    interface->ColorSet = DRV_GFX_TFT002_SetColor;
    interface->InstanceSet = DRV_GFX_TFT002_SetInstance;
    interface->PageSet = NULL;
    interface->Layer = NULL;
    interface->PixelGet = NULL;
    interface->AlphaBlendWindow = NULL;

    return interface;
}

// *****************************************************************************
/*
  Function: uint16_t DRV_GFX_TFT002_SetAddress(uint16_t x, uint16_t y)

  Summary:
    sets the pmp address for read/write operations

  Description:
    none

  Input:
    address - address

  Output:
    0 - when call was passed
*/
uint16_t DRV_GFX_TFT002_SetAddress(uint16_t x, uint16_t y)
{
    static uint32_t temp;
    static uint8_t state;
    
    switch(state)
    {    
        case 0:
    #if (DISP_ORIENTATION == 0)
    temp = 0x004;
    #else
    temp = 0x004f;
    #endif

    if(DRV_PMP_Write ( &pmphandle, true, &temp, 2, 0) == NULL)
    {
        return(1);
    }
    
    temp = x;
    state = 1;
        case 1:
    if(DRV_PMP_Write ( &pmphandle, false, &temp, 2, 0) == NULL)
    {
        return(1);
    }
    
    #if (DISP_ORIENTATION == 0)
    temp = 0x004f;
    #else
    temp = 0x004e;
    #endif
    state = 2;
        case 2:
    if(DRV_PMP_Write ( &pmphandle, true, &temp, 2, 0) == NULL)
    {
        return(1);
    }

    temp = y;
    state = 3;
        case 3:
    if(DRV_PMP_Write ( &pmphandle, false, &temp, 2, 0) == NULL)
    {
        return(1);
    }

    temp = 0x0022;

    case 4:
    if(DRV_PMP_Write ( &pmphandle, true, &temp, 2, 0) == NULL)
    {
        return(1);
    }

    }
    return(0);

}

// *****************************************************************************
/*
  Function: uint8_t DRV_GFX_TFT002_SetReg(uint16_t index, uint8_t value)

  Summary:
    updates graphics controller register value (byte access)

  Description:
    none

  Input:
    index - register number 
    value - value to write to register

  Output:
    1 - call was not passed
    0 - call was passed
*/
uint16_t DRV_GFX_TFT002_SetReg(uint16_t index, uint16_t value)
{
    static uint32_t myWriteBuffer;
    static uint8_t state = 0;

    switch(state)
    {
        case 0:
        myWriteBuffer = index;

        if(DRV_PMP_Write ( &pmphandle, true, &myWriteBuffer, 2, 0) == NULL)
        {
            return(1);
        }

        state = 1;

        case 1:
        myWriteBuffer = value;

        if(DRV_PMP_Write (&pmphandle, false, &myWriteBuffer, 2, 0) == NULL)
        {
            return(1);
        }

        state = 0;
    }

    return(0);
}

// *****************************************************************************
/*
  Function: uint8_t DRV_GFX_TFT002_GetReg(uint16_t index, uint8_t *data)

  Summary:
    returns graphics controller register value (byte access)

  Description:
    none

  Input:
    index - register number 
    *data - array to store data

  Output:
    0 - when call was passed
*/
uint8_t  DRV_GFX_TFT002_GetReg(uint16_t  index, uint8_t *data)
{  
    return(0);   
}

// *****************************************************************************
/*
  Function: uint8_t DRV_GFX_TFT002_BrightnessSet(uint8_t instance, uint16_t level)

  Summary:
    Sets the brightness of the display backlight.

  Description:
    none

  Input:
        level - Brightness level. Valid values are 0 to 100.
			             -   0: brightness level is zero or display is turned off
			             - 100: brightness level is maximum 
  Output:
    none
*/
void DRV_GFX_TFT002_BrightnessSet(uint8_t instance, uint16_t  level)
{
    if (level > 0)
    {
        DisplayBacklightOn();           
    }    
    else if (level == 0)
    {
        DisplayBacklightOff();
    }           
}    

//// *****************************************************************************
///*
//  Function: uint16_t DRV_GFX_TFT002_Initialize(uint8_t instance)
//
//  Summary:
//    resets LCD, initializes PMP
//
//  Description:
//    none
//
//  Input:
//        instance - driver instance
//  Output:
//    1 - call not successful (PMP driver busy)
//    0 - call successful
//*/
//uint16_t DRV_GFX_TFT002_Initialize(uint8_t instance, const SYS_MODULE_INIT    * const init )
//{
//    static uint8_t   state =0;
//    static uint16_t  horizontalSize, verticalSize, rotation;
//
//    if(driverTaskBusy == 0)
//    {
//        if(queueCount >= (DRV_GFX_TFT002_COMMANDQUEUESIZE-1))
//        {
//             return(1); //queue is full
//        }
//
//	driverBusy = 1;
//        queueIndex->task = INITIALIZE; //Start the state machine in the init function
//        queueIndex->instance = instance; //Start the state machine in the init function
//
//        if(queueIndex == &commandBuffer[DRV_GFX_TFT002_COMMANDQUEUESIZE-1])
//        {
//            queueIndex = &commandBuffer[0];
//        }
//        else
//        {
//            queueIndex++;
//        }
//
//         queueCount++;              //Add 1 to the queue count
//         driverBusy = 0;
//         return(0); //In the queue
//   }
//
//   while(state < regCount)
//   {
//       switch(state)
//       {
//
//            case 0:
//                DriverInterfaceInit(&pmphandle, &config);
//
//                DisplayResetDisable();
//                DisplayEnable();
//
//
//                if((drvTFT002Obj[0].initData->orientation == 90) || (drvTFT002Obj[0].initData->orientation == 270))
//                {
//                    horizontalSize = drvTFT002Obj[0].initData->verticalResolution;
//                    verticalSize   = drvTFT002Obj[0].initData->horizontalResolution;
//                }
//                else
//                {
//                    horizontalSize = drvTFT002Obj[0].initData->horizontalResolution;
//                    verticalSize   = drvTFT002Obj[0].initData->verticalResolution;
//                }
//                break;
//
//            default:
//                break;
//        }
//
//        if(*registers[state-1] == DELAY)
//        {
//            SYS_TMR_DelayMS(registers[state-1][1]);
//            state++;
//            return(1);
//        }
//
//        if(DRV_GFX_TFT002_SetReg(*registers[state-1], registers[state-1][1]))
//        {
//            return(1);
//        }
//
//        state++;
//    }
//
//    drvTFT002Obj[0].initData->ready = 1;
//    drvTFT002Obj[0].initData->driverBusy = 0;
//    drvTFT002Obj[0].initData->horizontalResolution = horizontalSize;
//    drvTFT002Obj[0].initData->verticalResolution = verticalSize;
//    DisplayBacklightOn();
//    return (0); //Driver initialized successfully
//}

// *****************************************************************************
/*
  Function: void DRV_GFX_TFT002_SetColor(GFX_COLOR color)

  Summary: Sets the color for the driver instance

  Description:
  
  Output: none

*/

void DRV_GFX_TFT002_SetColor(GFX_COLOR color)
{
    drvTFT002Obj[0].initData->color = color;
}

// *****************************************************************************
/*
  Function: void DRV_GFX_TFT002_SetInstance(uint8_t instance)

  Summary: Sets the instance for the driver

  Description:
  
  Output: none

*/

void DRV_GFX_TFT002_SetInstance(uint8_t instance)
{
    instance = instance;
}

// *****************************************************************************
/*
  Function: uint16_t DRV_GFX_TFT002_PixelPut(short x, short y)

  Summary:
    outputs one pixel into the frame buffer at the x,y coordinate given

  Description:
    none

  Input:
        instance - driver instance
        color - color to output
        x,y - pixel coordinates
  Output:
    1 - call not successful (TFT002 driver busy)
    0 - call successful
*/
uint16_t DRV_GFX_TFT002_PixelPut(short x, short y)
{

    if(queueCount >= (DRV_GFX_TFT002_COMMANDQUEUESIZE-1))
    {
        return (1); //queue is full
    }

    driverBusy = 1; 
    queueIndex->task = PUT_PIXELS;
    queueIndex->address = (uint32_t)((x << 16) + y);
    queueIndex->data = drvTFT002Obj[0].initData->color;
    queueIndex->instance = instance;
    queueIndex->count = 1;
    queueIndex->lineCount = 1;

    if(queueIndex == &commandBuffer[DRV_GFX_TFT002_COMMANDQUEUESIZE-1])
    {
        queueIndex = &commandBuffer[0];
    }
    else
    {
        queueIndex++;
    } 

    queueCount++;
    driverBusy = 0;
    return(0);

}  

// *****************************************************************************
/*
  Function: uint16_t DRV_GFX_TFT002_PixelsPut(short x, short y, uint16_t count)

  Summary:
    outputs one pixel into the frame buffer at the x,y coordinate given

  Description:
    none

  Input:
        instance - driver instance
        color - color to output
        x,y - pixel coordinates

  Output:
          1 - call not successful (TFT002 driver busy)
          0 - call successful
*/
uint16_t  DRV_GFX_TFT002_PixelsPut(short x, short y, uint16_t count, uint16_t lineCount)
{

    if(queueCount >= (DRV_GFX_TFT002_COMMANDQUEUESIZE-1))
    {
        return (1); //queue is full
    }

    driverBusy = 1;
    queueIndex->instance = instance;
    queueIndex->task = PUT_PIXELS;
    queueIndex->address = (uint32_t)((x << 16) + y);
    queueIndex->data = drvTFT002Obj[0].initData->color;
    queueIndex->count = count;
    queueIndex->lineCount = lineCount;

    if(queueIndex == &commandBuffer[DRV_GFX_TFT002_COMMANDQUEUESIZE-1])
    {
        queueIndex = &commandBuffer[0];
    }
    else
    {
        queueIndex++;
    } 

    queueCount++;
    driverBusy = 0;
    return(0);
}  

// *****************************************************************************
/*
  Function: uint16_t*  DRV_GFX_TFT002_PixelArrayPut(uint16_t *color, short x, short y, uint16_t count)

  Summary:
    outputs an array of pixels of length count starting at *color 

  Description:
    none

  Input:
          instance - driver instance
          *color - start of the array
		  x - x coordinate of the start point.
		  y - y coordinate of the end point.
		  count - number of pixels
  Output:
         handle to the number of pixels remaining
*/
uint16_t*  DRV_GFX_TFT002_PixelArrayPut(uint16_t *color, short x, short y, uint16_t count, uint16_t lineCount)
{
    static DRV_GFX_TFT002_COMMAND  *imageIndex;

    if(queueCount >= (DRV_GFX_TFT002_COMMANDQUEUESIZE-1))
    {
        return (NULL); //queue is full
    }

    driverBusy = 1;
   
    imageIndex = queueIndex;      //Inrement the queue pointer and index the value in case we are interrupted 

    queueIndex->task = PUT_ARRAY;
    queueIndex->address = (uint32_t)((x << 16) + y);
    queueIndex->array = color;   //Starting address of the array
    queueIndex->count = count;
    queueIndex->lineCount = lineCount;

    if(queueIndex == &commandBuffer[DRV_GFX_TFT002_COMMANDQUEUESIZE-1])
    {
        queueIndex = &commandBuffer[0];
    }
    else
    {
        queueIndex++;
    } 

    queueCount++;
    driverBusy = 0;
    return(&imageIndex->count);
} 

// *****************************************************************************
/*
  Function: uint16_t  DRV_GFX_TFT002_PixelArrayGet(uint16_t *color, short x, short y, uint16_t count)

  Summary:
    gets an array of pixels of length count starting at *color 

  Description:
    none

  Input:
          instance - driver instance
          *color - start of the array
		  x - x coordinate of the start point.
		  y - y coordinate of the end point.
		  count - number of pixels
  Output:
         1 - call not successful (TFT002 driver busy)
         0 - call successful
*/ 
uint16_t*  DRV_GFX_TFT002_PixelArrayGet(uint16_t *color, short x, short y, uint16_t count)
{
    /*Not yet supported*/
    return(NULL);
}  

// *****************************************************************************
/*
  Function: uint16_t  DRV_GFX_TFT002_Busy(uint8_t instance)

  Summary:
    Returns non-zero if LCD controller is busy 
          (previous drawing operation is not completed).

  Description:
    none

  Input:
          instance - driver instance
  Output:
         1 - busy
         0 - not busy
*/ 
uint16_t  DRV_GFX_TFT002_Busy(uint8_t instance)
{  
   return(1);
}
// *****************************************************************************
/* Function:
    SYS_STATUS DRV_GFX_TFT002_Status ( SYS_MODULE_OBJ object )

  Summary:
    Returns status of the specific module instance of the Driver module.

  Description:
    This function returns the status of the specific module instance disabling its
    operation (and any hardware for driver modules).

  PreCondition:
    The DRV_GFX_TFT002_Initialize function should have been called before calling
    this function.

  Parameters:
    object          - DRV_GFX_TFT002 object handle, returned from DRV_GFX_TFT002_Initialize

  Returns:
    SYS_STATUS_READY    Indicates that any previous module operation for the
                        specified module has completed

    SYS_STATUS_BUSY     Indicates that a previous module operation for the
                        specified module has not yet completed

    SYS_STATUS_ERROR    Indicates that the specified module is in an error state
*/

SYS_STATUS DRV_GFX_TFT002_Status ( SYS_MODULE_OBJ object )
{
    DRV_GFX_TFT002_OBJ *dObj = (DRV_GFX_TFT002_OBJ*)object;
    return ( dObj->state );

} /* SYS_TMR_Status */

void GFX_TMR_DelayMS ( unsigned int delayMs )
{
#if (true == SYS_TMR_INTERRUPT_MODE)

#else      //Primitive Blocking Mode
    if(delayMs)
    {
        uint32_t sysClk = SYS_CLK_CONFIG_EXTERNAL_CLOCK;
        uint32_t t0;
        #if   defined (__C32__)
        t0 = _CP0_GET_COUNT();
        while (_CP0_GET_COUNT() - t0 < (sysClk/2000)*delayMs);
        #elif defined (__C30__)
        t0 = ReadTimer23();
        while (ReadTimer23() -  t0 < (sysClk/2000)*mSec);
        #else
        #error "Neither __C32__ nor __C30__ is defined!"
        #endif
    }
#endif
}