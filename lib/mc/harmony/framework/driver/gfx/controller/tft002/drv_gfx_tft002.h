/*******************************************************************************
  Company:
    Microchip Technology Inc.
  File Name:
    drv_gfx_tft002.h

  Summary:
    Interface for the graphics library where the primitives are rendered and sent to the graphics controller either external or internal

  Description:
    This header file contains the function prototypes and definitions of
    the data types and constants that make up the interface to the tft002
    Graphics Controller.
*******************************************************************************/
//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute Software
only when embedded on a Microchip microcontroller or digital  signal  controller
that is integrated into your product or third party  product  (pursuant  to  the
sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS  WITHOUT  WARRANTY  OF  ANY  KIND,
EITHER EXPRESS  OR  IMPLIED,  INCLUDING  WITHOUT  LIMITATION,  ANY  WARRANTY  OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A  PARTICULAR  PURPOSE.
IN NO EVENT SHALL MICROCHIP OR  ITS  LICENSORS  BE  LIABLE  OR  OBLIGATED  UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,  BREACH  OF  WARRANTY,  OR
OTHER LEGAL  EQUITABLE  THEORY  ANY  DIRECT  OR  INDIRECT  DAMAGES  OR  EXPENSES
INCLUDING BUT NOT LIMITED TO ANY  INCIDENTAL,  SPECIAL,  INDIRECT,  PUNITIVE  OR
CONSEQUENTIAL DAMAGES, LOST  PROFITS  OR  LOST  DATA,  COST  OF  PROCUREMENT  OF
SUBSTITUTE  GOODS,  TECHNOLOGY,  SERVICES,  OR  ANY  CLAIMS  BY  THIRD   PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE  THEREOF),  OR  OTHER  SIMILAR  COSTS.
*******************************************************************************/
//DOM-IGNORE-END
 
#ifndef _TFT002_H
    #define _TFT002_H

#include "driver/gfx/display/drv_gfx_display.h"

//DOM-IGNORE-BEGIN

#define DRV_GFX_TFT002_COMMANDQUEUESIZE 480

/*********************************************************************
* Overview: Display orientation.
*********************************************************************/
//#define DISP_ORIENTATION 0

/*********************************************************************
* Overview: Horizontal and vertical screen size.
*********************************************************************/
    #if defined (GFX_USE_DISPLAY_CONTROLLER_SSD1289)
//        #define DISP_HOR_RESOLUTION 240
//        #define DISP_VER_RESOLUTION 320
    #elif defined (GFX_USE_DISPLAY_CONTROLLER_SSD2119)
        #define DISP_HOR_RESOLUTION 320
        #define DISP_VER_RESOLUTION 240
    #endif

typedef    enum 
{
    INITIALIZE = 0,
    BUSY,
    PUT_ARRAY,
    PUT_PIXELS,
} TFT002_TASK;

//DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Data Types and Constants
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* TFT002 Driver Module Index Count

  Summary:
    Number of valid TFT002 driver indices.

  Description:
    This constant identifies TFT002 driver index definitions.

  Remarks:
    This constant should be used in place of hard-coded numeric literals.

    This value is device-specific.
*/

#define DRV_GFX_TFT002_INDEX_COUNT     DRV_GFX_TFT002_NUMBER_OF_MODULES

// *********************************************************************
/* 
  Structure: DRV_GFX_TFT002_COMMAND

  Summary: Structure for the commands in the driver queue.

  Description:
        Structure for the commands in the driver queue.

  Input:
        instance    - instance of the driver
        address     - pixel address
        array       - pointer to array of pixel data
        data        - pixel color
        count       - count number of pixels in one line
        lineCount   - lineCount number of lines of display
        task        - Type of task (TFT002_TASK enum)
*/
// 
typedef struct
{
   uint8_t                     instance;
   uint32_t                     address;
   uint16_t                      *array;
   uint16_t                        data;
   uint16_t                       count;
   uint16_t                   lineCount;
   TFT002_TASK                    task;
} DRV_GFX_TFT002_COMMAND;

//DOM-IGNORE-BEGIN

// *********************************************************************
/* 
  Structure: DRV_GFX_TFT002_TASK

  Summary: Structure for the task machine

  Description: None
*/
// 
typedef struct
{
   uint8_t                         instance;
   uint32_t                        address;                    
   uint16_t                        color;
   uint8_t                         state;
} DRV_GFX_TFT002_TASK;

//DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Functions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/*
  Function: uint8_t DRV_GFX_TFT002_SetReg(uint16_t index, uint8_t value)

  Summary:
    updates graphics controller register value (byte access)

  Description:
    This call can set "value" of the register accessed by its "index".

  Input:
    index - register number 
    value - value to write to register

  Output:
    1 - call was not passed
    0 - call was passed
*/
uint16_t DRV_GFX_TFT002_SetReg(uint16_t index, uint16_t value);

// *****************************************************************************
/*
  Function: uint8_t DRV_GFX_TFT002_GetReg(uint16_t index, uint8_t *data)

  Summary:
    Returns graphics controller register value (byte access)

  Description:
    Returns graphics controller register value (byte access).

  Input:
    index - register number 
    *data - array to store data

  Output:
    0 - when call was passed
*/
uint8_t  DRV_GFX_TFT002_GetReg(uint16_t  index, uint8_t *data);

// *****************************************************************************
/*
  Function: SYS_MODULE_OBJ DRV_GFX_TFT002_Initialize(const SYS_MODULE_INDEX   moduleIndex,
                                          const SYS_MODULE_INIT    * const moduleInit)
  Summary:
    resets LCD, initializes PMP

  Description:
    Initializes driver instance having index moduleIndex. Initialization
    parameters are set by moduleInit structure. It also calls initialization
    routines of LCD and PMP modules.

  Input:
        instance - driver instance
  Output:
    1 - call not successful (PMP driver busy)
    0 - call successful
*/
SYS_MODULE_OBJ DRV_GFX_TFT002_Initialize(const SYS_MODULE_INDEX   moduleIndex,
                                          const SYS_MODULE_INIT    * const moduleInit);

/*********************************************************************
  Function:
     DRV_GFX_TFT002_Open(uint8_t instance)

  Summary:
    opens an instance of the graphics controller

  Description:
    Opens the tft002 driver instance with index given by index parameter.

  Return:
    Returns the handle of the driver instance.

  *********************************************************************/
DRV_HANDLE DRV_GFX_TFT002_Open( const SYS_MODULE_INDEX index,
                                 const DRV_IO_INTENT intent );

// *****************************************************************************
/*
  Function: void DRV_GFX_TFT002_Close( DRV_HANDLE handle )

  Summary:
    closes an instance of the graphics controller

  Description:
    Closes the tft002 driver instance, handle of which is given by handle
  variable.

  Input:
    instance of the driver

*/
void DRV_GFX_TFT002_Close( DRV_HANDLE handle );

/*********************************************************************
  Function:
     DRV_GFX_INTEFACE DRV_GFX_TFT002_InterfaceGet( DRV_HANDLE handle )

  Summary:
    Returns the API of the graphics controller

  Description:
    It returns the driver interfaces to be called by graphics library.

  Return:
    Returns the driver interfaces to be called by graphics library.

  *********************************************************************/
DRV_GFX_INTERFACE * DRV_GFX_TFT002_InterfaceGet( DRV_HANDLE handle );

// *****************************************************************************
/*
  Function:
     void DRV_GFX_TFT002_MaxXGet()

  Summary:
    Returns x extent of the display.

  Description:
    Returns x extent of the display.

*/
uint16_t DRV_GFX_TFT002_MaxXGet( DRV_HANDLE handle );

// *****************************************************************************
/*
  Function:
     void GFX_MaxYGet()

  Summary:
    Returns y extent of the display.

  Description:
    Returns y extent of the display.

*/
uint16_t DRV_GFX_TFT002_MaxYGet( DRV_HANDLE handle );

// *****************************************************************************
/*
  Function: void DRV_GFX_TFT002_SetColor(GFX_COLOR color)

  Summary:
    Sets the color for the driver instance.

  Description:
    Sets the color for the driver instance.

*/

void DRV_GFX_TFT002_SetColor(GFX_COLOR color);

// *****************************************************************************
/*
  Function: void DRV_GFX_TFT002_SetInstance(uint8_t instance)

  Summary:
    Sets the instance for the driver

  Description:
    Sets the instance of the driver to be referred.
  

*/

void DRV_GFX_TFT002_SetInstance(uint8_t instance);

// *****************************************************************************
/*
  Function: uint16_t DRV_GFX_TFT002_PixelsPut(short x, short y, uint16_t count, uint16_t lineCount)

  Summary:
    Outputs pixels into the frame buffer starting at the x,y coordinate given.

  Description:
    Outputs pixesl into the frame buffer starting at the x,y coordinate. Number
    of pixels are given by count and number of lines is given by lineCount.

  Input:
        x,y - pixel coordinates
      count - # of pixels to put
  lineCount - # of lines

  Output:
          NULL - call not successful
         !NULL - address of the display driver queue command
*/
uint16_t DRV_GFX_TFT002_PixelsPut(short x, short y, uint16_t count, uint16_t lineCount);

// *****************************************************************************
/*
  Function: uint16_t DRV_GFX_TFT002_PixelPut(short x, short y)

  Summary:
    Outputs one pixel into the frame buffer at the x,y coordinate given.

  Description:
    Outputs one pixel into the frame buffer at the x,y coordinate given.

  Input:
        x,y - pixel coordinates
  Output:
    NULL - call not successful
    !NULL - address of the display driver queue command
*/
uint16_t DRV_GFX_TFT002_PixelPut(short x, short y);

// *****************************************************************************
/*
  Function: uint16_t*  DRV_GFX_TFT002_PixelArrayPut(uint16_t *color, short x, short y, uint16_t count, uint16_t lineCount)

  Summary:
    Outputs an array of pixels of length count starting at *color

  Description:
    Outputs an array of pixels of length count starting at *color.

  Input:
          *color - start of the array
		  x - x coordinate of the start point.
		  y - y coordinate of the end point.
		  count - number of pixels
              lineCount - number of lines
  Output:
        NULL - call not successful
        !NULL - handle to the number of pixels remaining
*/
uint16_t* DRV_GFX_TFT002_PixelArrayPut(uint16_t *color, short x, short y, uint16_t count, uint16_t lineCount);

// *****************************************************************************
/*
  Function: uint16_t  DRV_GFX_TFT002_PixelArrayGet(uint16_t *color, short x, short y, uint16_t count)

  Summary:
    Gets an array of pixels of length count starting at *color.

  Description:
    Gets an array of pixels of length count starting at *color.

  Input:
          instance - driver instance
          *color - start of the array
		  x - x coordinate of the start point.
		  y - y coordinate of the end point.
		  count - number of pixels
  Output:
         NULL - call not successful
         !NULL - address of the display driver queue command
*/ 
uint16_t* DRV_GFX_TFT002_PixelArrayGet(uint16_t *color,short x, short y, uint16_t count);

// *****************************************************************************
/*
  Function: uint16_t DRV_GFX_TFT002_Busy(uint8_t instance)

  Summary:
    Returns non-zero if LCD controller is busy 
          (previous drawing operation is not completed).

  Description:
    Returns non-zero if LCD controller is busy
          (previous drawing operation is not completed).

  Input:
          instance - driver instance
  Output:
         1 - busy
         0 - not busy
*/ 
uint16_t DRV_GFX_TFT002_Busy(uint8_t instance);

/*************************************************************************

  Function:
      void DRV_GFX_TFT002_Tasks(SYS_MODULE_OBJ object)

  Summary:
    Task machine that renders the driver calls for the graphics library it
    must be called periodically to output the contents of its circular
    buffer

  Description:
    Task machine that renders the driver calls for the graphics library it
    must be called periodically to output the contents of its circular
    buffer

  Input:
    object: driver object.

  *************************************************************************/
void DRV_GFX_TFT002_Tasks(SYS_MODULE_OBJ object);

// *****************************************************************************
/*
  Function: uint8_t DRV_GFX_TFT002_BrightnessSet(uint8_t instance, uint16_t level)

  Summary:
    Sets the brightness of the display backlight.

  Description:
    Sets the brightness of the display backlight to the level given by level
    variable.

  Input:
        level - Brightness level. Valid values are 0 to 100.
			             -   0: brightness level is zero or display is turned off
			             - 100: brightness level is maximum 
  
*/
void DRV_GFX_TFT002_BrightnessSet(uint8_t instance, uint16_t  level);

// *****************************************************************************
/* Function:
    SYS_STATUS DRV_GFX_TFT002_Status ( SYS_MODULE_OBJ object )

  Summary:
    Returns status of the specific module instance of the Driver module.

  Description:
    This function returns the status of the specific module instance disabling its
    operation (and any hardware for driver modules).

  PreCondition:
    The DRV_GFX_TFT002_Initialize function should have been called before calling
    this function.

  Parameters:
    object          - DRV_GFX_TFT002 object handle, returned from DRV_GFX_TFT002_Initialize

  Returns:
    SYS_STATUS_READY    Indicates that any previous module operation for the
                        specified module has completed

    SYS_STATUS_BUSY     Indicates that a previous module operation for the
                        specified module has not yet completed

    SYS_STATUS_ERROR    Indicates that the specified module is in an error state
*/

SYS_STATUS DRV_GFX_TFT002_Status ( SYS_MODULE_OBJ object );
      
#endif // _TFT002_H


