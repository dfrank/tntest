/*******************************************************************************
  Company:
    Microchip Technology Incorporated

  File Name:
    drv_gfx_lcc.c

  Summary:
    Interface for the graphics library where the primitives are renderred and sent to the graphics controller
    either external or internal

  Description:
    None
*******************************************************************************/
//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute Software
only when embedded on a Microchip microcontroller or digital  signal  controller
that is integrated into your product or third party  product  (pursuant  to  the
sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS  WITHOUT  WARRANTY  OF  ANY  KIND,
EITHER EXPRESS  OR  IMPLIED,  INCLUDING  WITHOUT  LIMITATION,  ANY  WARRANTY  OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A  PARTICULAR  PURPOSE.
IN NO EVENT SHALL MICROCHIP OR  ITS  LICENSORS  BE  LIABLE  OR  OBLIGATED  UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,  BREACH  OF  WARRANTY,  OR
OTHER LEGAL  EQUITABLE  THEORY  ANY  DIRECT  OR  INDIRECT  DAMAGES  OR  EXPENSES
INCLUDING BUT NOT LIMITED TO ANY  INCIDENTAL,  SPECIAL,  INDIRECT,  PUNITIVE  OR
CONSEQUENTIAL DAMAGES, LOST  PROFITS  OR  LOST  DATA,  COST  OF  PROCUREMENT  OF
SUBSTITUTE  GOODS,  TECHNOLOGY,  SERVICES,  OR  ANY  CLAIMS  BY  THIRD   PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE  THEREOF),  OR  OTHER  SIMILAR  COSTS.
*******************************************************************************/
//DOM-IGNORE-END
#include "driver/gfx/gfx_lcc/drv_gfx_lcc.h"
#include <xc.h>
#include <sys/attribs.h>
#include "system/int/sys_int.h"
#include "system/dma/sys_dma.h"
#include "peripheral/pmp/plib_pmp.h"
#include "peripheral/dma/plib_dma.h"
#include "peripheral/tmr/plib_tmr.h"


#define VER_BLANK                 (DISP_VER_PULSE_WIDTH+DISP_VER_BACK_PORCH+DISP_VER_FRONT_PORCH-1)

volatile DRV_GFX_LCC_COMMAND commandBuffer[DRV_GFX_LCC_COMMANDQUEUESIZE];  //This contains the commands to send to the graphics driver
volatile DRV_GFX_LCC_COMMAND *queueIndex = &commandBuffer[0];                             //pointer to where the command Buffer is currently
volatile DRV_GFX_LCC_COMMAND  *taskIndex = &commandBuffer[0];

volatile uint16_t queueCount = 0;
uint8_t instances[1] = {0};

volatile uint16_t driverBusy = 0;                              //Flag to state driver is busy

#define SRAM_ADDR_CS0  0xE0000000

    volatile DRV_GFX_DATA DRV_GFX_instance[1] =
    {
        DISP_ORIENTATION, 
        DISP_HOR_RESOLUTION,
        DISP_VER_RESOLUTION,
        DISP_DATA_WIDTH,
        DISP_HOR_PULSE_WIDTH,
        DISP_HOR_BACK_PORCH,
        DISP_HOR_FRONT_PORCH,
        DISP_VER_PULSE_WIDTH,
        DISP_VER_BACK_PORCH,
        DISP_VER_FRONT_PORCH,
    	DISP_INV_LSHIFT,
        LCD_TFT,
        0,
        TCON_MODULE,
        {DRV_GFX_LCC_PixelsPut, NULL, DRV_GFX_LCC_PixelArrayPut, DRV_GFX_LCC_PixelArrayGet,
         DRV_GFX_LCC_PixelPut, DRV_GFX_LCC_SetColor, DRV_GFX_LCC_SetInstance,
         DRV_GFX_LCC_SetPage, DRV_GFX_LCC_Layer, NULL, DRV_GFX_LCC_AlphaBlendWindow},
        1
    };

volatile uint8_t DrawCount = 0;                          /* The current status of how many pixels have been drawn inbetween a DMA IR*/
volatile uint8_t overflowcount;                      /* The count for the amount of overflows that have happened in the PMP Adress*/

static SYS_DMA_CHANNEL_HANDLE dmaHandle = SYS_DMA_CHANNEL_HANDLE_INVALID;

volatile uint8_t driverTaskActive = 0;

volatile GFX_COLOR GraphicsFrame[(DISP_VER_RESOLUTION+1)][(DISP_HOR_RESOLUTION)];

uint16_t HBackPorch = (DISP_HOR_PULSE_WIDTH+DISP_HOR_BACK_PORCH)-1;
uint16_t HFrontPorch = 1;

//PIP Variables (NULL at start)
uint16_t PipStartT = 0;
uint16_t PipStartL = 0;
uint16_t PipVLength = 0;
uint16_t PipHLength = 0;
static uint32_t PipX,PipY;
uint8_t GFXPIPPage=0;
uint16_t scroll,scrollLine,scrollPage = 0;
volatile uint16_t* _ebiBaseAddress = (uint16_t*)SRAM_ADDR_CS0;
// *****************************************************************************
/*
  Function: DRV_GFX_LCC_Open(uint8_t instance)

  Summary:
    opens an instance of the graphics controller

  Description:
    none

  Input:
    instance of the driver

  Output:
    1 - driver not initialied
    2 - instance doesn't exist
    3 - instance already open
    instance to driver when successful
*/
uint16_t DRV_GFX_LCC_Open(uint8_t instance)
{

   if(DRV_GFX_instance[instance].ready == 0)
   {
       return(1);
   }

   if(instance >= 1)
   {
      return(2); //instance doesn't exist
   }

   if(instances[instance] == 0)
   {
       instances[instance] = 1; //Flag to let driver know instance is open
       return(instance);
   }
   else
   {
       return(3); //Instance already open
   }
}

// *****************************************************************************
/*
  Function: DRV_GFX_LCC_Close(uint8_t instance)

  Summary:
    closes an instance of the graphics controller

  Description:
    none

  Input:
    instance of the driver

  Output:
    0 - instance closed
    2 - instance doesn't exist
    3 - instance already closed
*/
uint16_t DRV_GFX_LCC_Close(uint8_t instance)
{
      
   if(instance >= 1)
   {
       return(2); //instance doesn't exist
   }

   if(instances[instance] == 1)
   {
       instances[instance] = 0; //Flag to let driver know instance is closed
       return(0);
   }
   else
   {
       return(3); //Instance already closed
   }
}

uint16_t DRV_GFX_LCC_SetPage(uint8_t pageType,uint8_t page)
{

    if(driverTaskActive)
    {
        switch(pageType)
        {
            case ACTIVE_PAGE:
                DRV_GFX_instance[0].activePage = page;
                _ebiBaseAddress = (uint16_t*)SRAM_ADDR_CS0 + (DRV_GFX_instance[0].activePage << 17);
                break;
            case VISUAL_PAGE:
                DRV_GFX_instance[0].visualPage = page;
                break;
        }

        return(0);
    }

    if(queueCount >= (DRV_GFX_LCC_COMMANDQUEUESIZE-1))
    {
        return (1); //queue is full
    }
     driverBusy = 1;
     queueIndex->task = PAGE;
     queueIndex->data = pageType;
     queueIndex->instance = 0;
     queueIndex->count = page;

      if(queueIndex == &commandBuffer[DRV_GFX_LCC_COMMANDQUEUESIZE-1])
      {
          queueIndex = &commandBuffer[0];
      }
      else
      {
          queueIndex++;
      } 

     queueCount++;
     driverBusy = 0;
     return(0);
}
// *****************************************************************************
/*
  Function: void DRV_GFX_LCC_SetColor(uint8_t instance, GFX_COLOR color)

  Summary: Sets the color for the driver instance

  Description:
  
  Output: none

*/

void DRV_GFX_LCC_SetColor(GFX_COLOR color)
{
  DRV_GFX_instance[0].color = color;
}

// *****************************************************************************
/*
  Function: void DRV_GFX_LCC_SetInstance(uint8_t instance)

  Summary: Sets the instance for the driver

  Description:
  
  Output: none

*/

void DRV_GFX_LCC_SetInstance(uint8_t instance)
{
  //_instance = instance;
}

// *****************************************************************************
/*
  Function: uint16_t DRV_GFX_LCC_Initialize(uint8_t instance)

  Summary:
    resets LCD, initializes PMP

  Description:
    none

  Input:
        instance - driver instance
  Output:
    1 - call not successful (PMP driver busy)
    0 - call successful
*/
uint16_t DRV_GFX_LCC_Initialize(uint8_t instance)
{
    static uint16_t  horizontalSize, verticalSize;

    if((DRV_GFX_instance[instance].orientation == 90) || (DRV_GFX_instance[instance].orientation == 270))
    {
        horizontalSize = DRV_GFX_instance[instance].verticalResolution;
        verticalSize   = DRV_GFX_instance[instance].horizontalResolution;
    }
    else
    {
        horizontalSize = DRV_GFX_instance[instance].horizontalResolution;
        verticalSize   = DRV_GFX_instance[instance].verticalResolution;
    }

    /*Suspend DMA Module*/
    SYS_DMA_Suspend();

    HSYNC_TRIS =0;
    LCD_CS_TRIS =0;
    VSYNC_TRIS =0;
    LCD_RESET_TRIS =0;

    BACKLIGHT_TRIS = 0;
    DATA_ENABLE_TRIS = 0;

    LCD_RESET = 1;
    LCD_CS    = 1;

    PIXELCLOCK_TRIS = 0;
    PIXELCLOCK = DRV_GFX_instance[0].logicShift;

    if(DRV_GFX_instance[instance].TCON_Init != NULL)
    {
        DRV_GFX_instance[instance].TCON_Init();
    }

    DRV_GFX_instance[0].activePage = 0;
    DRV_GFX_instance[0].visualPage = 0;

    #if defined(MEB_2_BOARD)
    TRISJbits.TRISJ7 = 0; //Camera
    LATJbits.LATJ7 = 1; //Camera

    TRISJbits.TRISJ12 = 0; //UB
    LATJbits.LATJ12 = 0; //UB

    TRISJbits.TRISJ10 = 0; //LB
    LATJbits.LATJ10 = 0; //LB

    CFGEBIA = 0x0007ffff;
    CFGEBIC = 0x00003003;

    EBICS0  = 0x20000000;
    EBIMSK0 = 0x00000026;
    EBISMT0 = 0x00001341;
    EBISMCON = 0x00000000;      //SFR control for Static Memory

    ADDR19_TRIS = 0;
    ADDR19 = 0;
    #endif

    SRAM_TRIS = 0;
    SRAM_CS   = 0;

    /*Turn Backlight on*/
    BACKLIGHT = BACKLIGHT_ENABLE_LEVEL;

    /* Allocate DMA channel */
    dmaHandle = SYS_DMA_ChannelAllocate (DMA_CHANNEL_INDEX);

    if(SYS_DMA_CHANNEL_HANDLE_INVALID == dmaHandle)
    {
        return (1);
    }

    // set the transfer parameters: source & destination address, source & destination size, number of bytes per event
    
    // set the transfer event control: what event is to start the DMA transfer
    SYS_DMA_ChannelSetup (  dmaHandle,
                            DMA_CHANNEL_PRIORITY_0,
                            SYS_DMA_CHANNEL_OP_MODE_BASIC,
                            DMA_TRIGGER_SOURCE_NONE);

    // set the transfer parameters: source & destination address,
    // source & destination size, number of bytes per event
    SYS_DMA_ChannelTransferAdd(dmaHandle, (void *)&PMDIN, (HBackPorch<<1), &GraphicsFrame[DISP_VER_RESOLUTION][0],
                               2, (HBackPorch<<1));

    DCH1SSA = 0x20000000;                         /*DMA destination address*/

    /* Enable the transfer done interrupt, when all buffer transferred */
    PLIB_DMA_ChannelXINTSourceEnable (dmaHandle,DMA_CHANNEL_1 ,
                                    DMA_INT_BLOCK_TRANSFER_COMPLETE );

    SYS_INT_SourceEnable(INT_SOURCE_DMA_1);

    // once we configured the DMA channel we can enable it
    SYS_DMA_ChannelEnable(dmaHandle);
   
    /*Unsuspend DMA Module*/
    SYS_DMA_Resume();

    SYS_DMA_ChannelForceStart(dmaHandle);

    DRV_GFX_instance[0].horizontalResolution = horizontalSize;
    DRV_GFX_instance[0].verticalResolution = verticalSize;
    DRV_GFX_instance[0].ready = 1;
    DRV_GFX_instance[0].driverBusy =0;

    return (0); //Driver initialized successfully
}

#ifdef LCC_EXTERNAL_MEMORY

volatile short pixelX;
volatile GFX_COLOR pixelColor;
volatile uint16_t pixelCount = 0;
volatile uint8_t pixelUpdate = 0;

void DRV_GFX_LCC_DisplayRefresh()
{
    static uint8_t GraphicsState = ACTIVE_PERIOD;
    static uint16_t remaining = 0;
    static uint16_t remainingPixels = 0;
    static short line = 0;
    static uint32_t pixelAddress = 0;
    static uint8_t prevGraphicsState = BLANKING_PERIOD;
    static uint16_t pipLine = 0;
    static uint32_t* EBIStart = (uint32_t*)(SRAM_ADDR_CS0);
    static uint8_t _page;

    switch(GraphicsState)
    {

        case ACTIVE_PERIOD:
            remaining = DISP_HOR_RESOLUTION;
            GraphicsState = BLANKING_PERIOD;

            if(line >= 0)
            {

                DCH1SSA = 0x20000000;
                DCH1SSA += ((line) * DISP_HOR_RESOLUTION) << 1;
                DCH1SSA += (DRV_GFX_instance[0].visualPage << 18);

                if((line) == (DISP_VER_RESOLUTION))
                {         
                    VSYNC = 0;
                    line = (-VER_BLANK);
                    overflowcount = 0;
                    pipLine = 0;
               }
               else
               {
                   DATA_ENABLE = 1;
                   VSYNC =1;
                   
                   if((line >= PipStartT)&&(line <= (PipStartT + PipVLength))&&(PipVLength != 0))
                   {
                       GraphicsState = PIP;

                       if(!PipStartL)
                       {
                           //Draw PIP Line
      case PIP:           
                           pixelAddress = (uint32_t)(((PipY+pipLine++)*(DISP_HOR_RESOLUTION))+(PipX));

                            DCH1SSA = 0x20000000;
                            DCH1SSA += (pixelAddress) << 1;
                            DCH1SSA += (GFXPIPPage << 18);

                           remaining = PipHLength;

                           remainingPixels = DISP_HOR_RESOLUTION - remaining - PipStartL;
                           pixelAddress = ((line*DISP_HOR_RESOLUTION) + remaining + PipStartL);
 
                           GraphicsState = FINISH_LINE;
                        }
                       else
                       {
                           remaining = PipStartL;
                       }
                 }

                 if(scroll > 0)
                 {

                     switch(scroll)
                     {
                         case 1:             //Up
                         case 2:             //Down
                            if(line < scrollLine)
                            {
                                pixelAddress = ((DRV_GFX_instance[0].verticalResolution-1)-(scrollLine-line)) * DISP_HOR_RESOLUTION;
                                _page = DRV_GFX_instance[0].visualPage;
                            }
                            else
                            {
                                pixelAddress = (line-scrollLine) * DISP_HOR_RESOLUTION;
                                _page = scrollPage;
                            }

                            DCH1SSA = 0x20000000;
                            DCH1SSA += (pixelAddress) << 1;
                            DCH1SSA += (_page << 18);
                            break;

                        case 3://Left
                        case 4://Right
                            pixelAddress = ((line+1) * DISP_HOR_RESOLUTION) + ((DRV_GFX_instance[0].horizontalResolution-1)-scrollLine);
                   
                            DCH1SSA = 0x20000000;
                            DCH1SSA += (pixelAddress) << 1;

                            if(scroll == 3)
                            {
                                _page = DRV_GFX_instance[0].visualPage;
                            }
                            else
                            {
                                _page = scrollPage;
                            }

                            remaining = scrollLine;
                            remainingPixels = DISP_HOR_RESOLUTION - scrollLine;
                            pixelAddress = (line) * DISP_HOR_RESOLUTION;
                            GraphicsState = FINISH_LINE;
                            break;
                    }

                  if((scroll>2) && (GraphicsState != FINISH_LINE))
                  {
        case FINISH_LINE:            //Finish Line Render
                      remaining = remainingPixels;
                   
                      DCH1SSA = 0x20000000;
                      DCH1SSA += (pixelAddress) << 1;

                      if(scroll == 3)
                      {
                          _page = scrollPage;
                      }
                      else
                      {
                         _page =  DRV_GFX_instance[0].visualPage;
                      }
                      DCH1SSA += (_page << 18);
                      GraphicsState = BLANKING_PERIOD;
                  }

              }

              prevGraphicsState = GraphicsState;
          }
      }
        break;

        case BLANKING_PERIOD:   //Front Porch then Back Porch Start 
            HSYNC = 0;
            DATA_ENABLE = 0;
            remaining = *EBIStart;
            HSYNC = 1;

            //Setup DMA Back Porch
            remaining = HBackPorch;
            GraphicsState = ACTIVE_PERIOD;   
            line++;      

        default:
            break;
    }

    DCH1SSIZ =  remaining << 1;
    DCH1CSIZ = DCH1SSIZ;


    PLIB_DMA_ChannelXINTSourceFlagClear(0, DMA_CHANNEL_INDEX, DMA_INT_BLOCK_TRANSFER_COMPLETE);
    PLIB_DMA_ChannelXEnable(0,DMA_CHANNEL_INDEX);
    SYS_DMA_ChannelForceStart(DMA_CHANNEL_INDEX);
}
#endif

// *****************************************************************************
/*
  Function: uint16_t DRV_GFX_LCC_PixelPut(short x, short y)

  Summary:
    outputs one pixel into the frame buffer at the x,y coordinate given

  Description:
    none

  Input:
        x,y - pixel coordinates
  Output:
    1 - call not successful (lcc driver busy)
    0 - call successful
*/
uint16_t DRV_GFX_LCC_PixelPut(short x, short y)
{

   if(queueCount >= (DRV_GFX_LCC_COMMANDQUEUESIZE-1))
    {
        return (1); //queue is full
    }

    driverBusy = 1;
    queueCount++;
    queueIndex->task = PUT_PIXEL;
    queueIndex->address.Val = (uint32_t)(((y)*(DISP_HOR_RESOLUTION))+(x));
    queueIndex->data = DRV_GFX_instance[0].color;
    queueIndex->count = x;
    queueIndex->lineCount = y;

    if(queueIndex == &commandBuffer[DRV_GFX_LCC_COMMANDQUEUESIZE-1])
    {
        queueIndex = &commandBuffer[0];
    }
    else
    {
        queueIndex++;
    }

    driverBusy = 0;
    return(0);
}  

// *****************************************************************************
/*
  Function: uint16_t DRV_GFX_LCC_PixelsPut(short x, short y, uint16_t count, uint16_t lineCount)

  Summary:
    outputs one pixel into the frame buffer at the x,y coordinate given

  Description:
    none

  Input:
        x,y - pixel coordinates
        count - pixel count per line
        lineCount - line count

  Output:
          1 - call not successful (lcc driver busy)
          0 - call successful
*/
uint16_t  DRV_GFX_LCC_PixelsPut(short x, short y, uint16_t count, uint16_t lineCount)
{

      if(queueCount >= (DRV_GFX_LCC_COMMANDQUEUESIZE-1))
    {
        return (1); //queue is full
    }

    driverBusy = 1;
    queueCount++;
    queueIndex->task = PUT_PIXELS;
    queueIndex->address.Val = (uint32_t)(((y)*(DISP_HOR_RESOLUTION))+(x));
    queueIndex->data = DRV_GFX_instance[0].color;
    queueIndex->count = count;
    queueIndex->lineCount = lineCount;

    if(queueIndex == &commandBuffer[DRV_GFX_LCC_COMMANDQUEUESIZE-1])
    {
        queueIndex = &commandBuffer[0];
    }
    else
    {
        queueIndex++;
    }
    driverBusy = 0;
    return(0);
}  

// *****************************************************************************
/*
  Function: uint16_t*  DRV_GFX_LCC_PixelArrayPut(uint16_t *color, short x, short y, uint16_t count)

  Summary:
    outputs an array of pixels of length count starting at *color 

  Description:
    none

  Input:
          instance - driver instance
          *color - start of the array
		  x - x coordinate of the start point.
		  y - y coordinate of the end point.
		  count - number of pixels
  Output:
         handle to the number of pixels remaining
*/
uint16_t*  DRV_GFX_LCC_PixelArrayPut(uint16_t *color, short x, short y, uint16_t count, uint16_t lineCount)
{

 static uint16_t  *index1;

    if(queueCount >= (DRV_GFX_LCC_COMMANDQUEUESIZE-1))
    {
     return (NULL); //queue is full
    }

        driverBusy = 1;
        queueCount++;
        queueIndex->instance = 0;
        queueIndex->task = PUT_ARRAY;
        queueIndex->address.Val = (uint32_t)(((y)*(DISP_HOR_RESOLUTION))+(x));
        queueIndex->array = color;   //Starting address of the array
        queueIndex->count = count;
        queueIndex->lineCount = lineCount;

     index1 = (uint16_t*)&queueIndex->count;

      if(queueIndex == &commandBuffer[DRV_GFX_LCC_COMMANDQUEUESIZE-1])
      {
          queueIndex = &commandBuffer[0];
      }
      else
      {
          queueIndex++;
      }
     driverBusy = 0;
     return(index1);
   
} 

// *****************************************************************************
/*
  Function: uint16_t*  DRV_GFX_LCC_PixelArrayGet(uint16_t *color, short x, short y, uint16_t count)

  Summary:
    outputs an array of pixels of length count starting at *color 

  Description:
    none

  Input:
          instance - driver instance
          *color - start of the array
		  x - x coordinate of the start point.
		  y - y coordinate of the end point.
		  count - number of pixels
  Output:
         handle to the number of pixels remaining
*/
uint16_t*  DRV_GFX_LCC_PixelArrayGet(uint16_t *color, short x, short y, uint16_t count)
{

    static uint16_t  *qindex;

    if(queueCount >= (DRV_GFX_LCC_COMMANDQUEUESIZE-1))
    {
     return (NULL); //queue is full
    }
     driverBusy = 1;
     queueCount++;

     queueIndex->instance = 0;
     queueIndex->task = GET_PIXELS;
     queueIndex->address.Val =  (uint32_t)(((y)*(DISP_HOR_RESOLUTION))+(x));
     queueIndex->array = color;   //Starting address of the array
     queueIndex->count = count;

     qindex = (uint16_t*)&queueIndex->count;      //Inrement the queue pointer and index the value in case we are interrupted


      if(queueIndex == &commandBuffer[DRV_GFX_LCC_COMMANDQUEUESIZE-1])
      {
          queueIndex = &commandBuffer[0];
      }
      else
      {
          queueIndex++;
      }
     driverBusy = 0;
     return(qindex);
   
}

volatile uint16_t _pixelRendercount;
volatile uint16_t* _framePoint;

// *****************************************************************************
/*
  Function:
	void DRV_GFX_LCC_Tasks(void)

  Summary:
	Task machine that renders the driver calls for the graphics library
    it must be called peridically to output the contents of its circular buffer
*/
//
void DRV_GFX_LCC_Tasks(void)
{

   uint16_t x, y, temp, i;
   static GFX_ALPHA_PARAMS alphaParams;
   GFX_COLOR *point, *pixelArray;
   
   if(driverBusy == 1)
   {
       return;
   }

   while(queueCount != 0)
   {

        switch(taskIndex->task)  //State Machine for Putpixel ONLY
        {

            case PUT_PIXEL:

                point = (uint16_t*)KVA0_TO_KVA1(&GraphicsFrame[taskIndex->lineCount][taskIndex->count]);
                *point = taskIndex->data;

                DCH2SSIZ = 2;
                DCH2DSIZ = DCH2SSIZ;
                DCH2CSIZ = DCH2DSIZ;

                PLIB_DMA_ChannelXSourceStartAddressSet(DMA_ID_0, 2, (uint16_t*)(&taskIndex->data));

                DCH2DSA = 0x20000000;
                DCH2DSA += (taskIndex->address.Val) << 1;
                DCH2DSA += (DRV_GFX_instance[0].activePage << 18);
                DCH2CONSET = 0x80;
                DCH2ECONbits.CFORCE = 1;
                while(DCH2CONbits.CHBUSY == 1);
                break;

            case PUT_PIXELS:
                 x = ((taskIndex->address.Val) % (DISP_HOR_RESOLUTION));
                 y = (taskIndex->address.Val) / DISP_HOR_RESOLUTION;
                 temp = y;

                 for(y = 0; y < taskIndex->lineCount; y++)
                 {

                    point = (uint16_t*)KVA0_TO_KVA1(&GraphicsFrame[temp][x]);

                     for(i = 0; i < taskIndex->count; i++)
                    {
                        *point++ = taskIndex->data;
                    }
                    temp++;
                 }

                DCH2SSIZ = 2;
                DCH2DSIZ = (taskIndex->count) << 1;
                DCH2CSIZ = DCH2DSIZ;
                temp = 1;
                PLIB_DMA_ChannelXSourceStartAddressSet(DMA_ID_0, 2, (uint16_t*)(&taskIndex->data));
  
                while(taskIndex->lineCount)
                {

                    DCH2DSA = 0x20000000;
                    DCH2DSA += (taskIndex->address.Val) << 1;
                    DCH2DSA += (DRV_GFX_instance[0].activePage << 18);
                    DCH2CONSET = 0x80;
                    DCH2ECONbits.CFORCE = 1;
                    taskIndex->address.Val += (DISP_HOR_RESOLUTION*temp);
                    taskIndex->lineCount -= temp;

                    if((taskIndex->lineCount > 65) && (taskIndex->count == DISP_HOR_RESOLUTION))
                    {
                        temp = 65;
                    }
                    else
                    {
                      temp = 1;
                    }

                    while(DCH2CONbits.CHBUSY == 1);
                    DCH2DSIZ = (temp*taskIndex->count) << 1;
                    DCH2CSIZ = DCH2DSIZ;
                }
                break;

            case PUT_ARRAY:

                 x = (taskIndex->address.Val) % DISP_HOR_RESOLUTION;
                 y = (taskIndex->address.Val) / DISP_HOR_RESOLUTION;
                 temp = y;
                 pixelArray = (uint16_t*)(taskIndex->array);

                 for(y = 0; y < taskIndex->lineCount; y++)
                 {
                     point = (uint16_t*)KVA0_TO_KVA1(&GraphicsFrame[temp][x]);

                     for(i = 0; i < taskIndex->count; i++)
                    {
                        *point++ = *pixelArray++;
                    }
                     temp++;
                 }
  
                  DCH2SSIZ = (taskIndex->count) << 1;
                  DCH2DSIZ = DCH2SSIZ;
                  DCH2CSIZ = DCH2SSIZ;

                while(taskIndex->lineCount--)
                {
                    while(DCH2CONbits.CHBUSY == 1);
                    PLIB_DMA_ChannelXSourceStartAddressSet(DMA_ID_0, 2, (uint16_t*)(taskIndex->array));
                    DCH2DSA = 0x20000000;//(_framePoint);
                    DCH2DSA += (taskIndex->address.Val) << 1;
                    DCH2DSA += (DRV_GFX_instance[0].activePage << 18);
                    taskIndex->array += taskIndex->count;
                    taskIndex->address.Val += DISP_HOR_RESOLUTION;
                    DCH2CONSET = 0x80;
                    DCH2ECONbits.CFORCE = 1;       
                }

                break;
        
            case PAGE:
              driverTaskActive = 1;
              DRV_GFX_LCC_SetPage(taskIndex->data,taskIndex->count);
              driverTaskActive = 0;
              break;
        
            case LAYERS:
              driverTaskActive = 1;
              DRV_GFX_LCC_Layer(0,(GFX_LAYER_PARAMS*)taskIndex->array);
              driverTaskActive = 0;
              break;
              
             case ALPHA_BLEND: //AlphaBlend

                if(taskIndex->instance == 2)
                {
                    alphaParams.foregroundPage = taskIndex->data;
                    alphaParams.foregroundLeft = taskIndex->count;
                    alphaParams.foregroundTop = taskIndex->lineCount;
                }
                else if(taskIndex->instance == 1)
                {
                    alphaParams.destinationPage = taskIndex->data;
                    alphaParams.destinationLeft = taskIndex->count;
                    alphaParams.destinationTop = taskIndex->lineCount;
                }
                else
                {
                    driverTaskActive = 1;

                    DRV_GFX_LCC_AlphaBlendWindow(&alphaParams, taskIndex->count, taskIndex->lineCount, taskIndex->data);
     
                    driverTaskActive = 0;
                }
                break;

            case GET_PIXELS:

              x = (taskIndex->address.Val) % DISP_HOR_RESOLUTION;
              y = (taskIndex->address.Val) / DISP_HOR_RESOLUTION;

              GFX_COLOR *point = (uint16_t*)KVA0_TO_KVA1(&GraphicsFrame[y][x]);

                  while(taskIndex->count--)
                  {
                    *taskIndex->array = *point;
                    taskIndex->array++;
                    point++;
                  }
                  break;


            default: //Do nothing
            break;
           }

        queueCount--;      //Subtract 1 from the queue count
        taskIndex->array = NULL;
        taskIndex->count = 0;

        if(taskIndex++ == &commandBuffer[DRV_GFX_LCC_COMMANDQUEUESIZE-1])
        {
            taskIndex = &commandBuffer[0];     
        }
    }
}

uint16_t* DRV_GFX_LCC_Layer(uint8_t instance, GFX_LAYER_PARAMS* layer)
{
   static uint16_t* LayerIndex = NULL;

    if(driverTaskActive)
    {

    GFXPIPPage = layer->page;

   if(layer->on == 0)
   {    
       PipVLength = 0;
       return((uint16_t*)&queueIndex->count);
   }

   #if (DISP_ORIENTATION == 90)
     PipStartL = (layer->top);
     PipStartT = (DISP_VER_RESOLUTION-1)-(layer->left + layer->width);
     PipVLength = layer->width;
     PipHLength = layer->height;
     PipY = (DISP_VER_RESOLUTION-1) - (layer->layerLeft+layer->width);
     PipX = (layer->layerTop);
   #elif (DISP_ORIENTATION == 0)
    PipStartL = layer->left;
    PipStartT = layer->top;   
    PipVLength = layer->height;
    PipHLength = layer->width;
    PipX = layer->layerLeft;
    PipY = layer->layerTop;
   #endif
   return((uint16_t*)&queueIndex->count);
   }

    if(queueCount >= (DRV_GFX_LCC_COMMANDQUEUESIZE-1))
    {
        return (NULL); //queue is full
    }
    driverBusy = 1;
    queueCount++;
     
     queueIndex->task = LAYERS;
     queueIndex->instance = 0;
     queueIndex->array = (uint16_t*)layer; 
     queueIndex->count = 1;
     LayerIndex = (uint16_t*)&queueIndex->count;
 
      if(queueIndex == &commandBuffer[DRV_GFX_LCC_COMMANDQUEUESIZE-1])
      {
          queueIndex = &commandBuffer[0];
      }
      else
      {
          queueIndex++;
      } 
     driverBusy = 0;
     return(LayerIndex);
}

uint16_t* DRV_GFX_LCC_AlphaBlendWindow(GFX_ALPHA_PARAMS* alphaParams, uint16_t width, uint16_t height, uint8_t alpha)
{
    static uint8_t state = 0;
    static uint32_t address;
    static DRV_GFX_LCC_COMMAND *index = NULL;

    if(driverTaskActive == 1)
    {
         GFX_COLOR *point = (uint16_t*)KVA0_TO_KVA1(&GraphicsFrame[alphaParams->foregroundTop]
                                                                [alphaParams->foregroundLeft]);

         address = (alphaParams->destinationTop * DISP_HOR_RESOLUTION) + alphaParams->destinationLeft;

         DCH2SSIZ = (width) << 1;
         DCH2DSIZ = DCH2SSIZ;
         DCH2CSIZ = DCH2SSIZ;

           while(height--)
           {       
               PLIB_DMA_ChannelXSourceStartAddressSet(DMA_ID_0, 2, (uint16_t*)(point));

               DCH2DSA = 0x20000000;
               DCH2DSA += (address) << 1;
               DCH2DSA += (alphaParams->destinationPage << 18);
               point += DISP_HOR_RESOLUTION;
               address += DISP_HOR_RESOLUTION;
               DCH2CONSET = 0x80;
               DCH2ECONbits.CFORCE = 1;
               while(DCH2CONbits.CHBUSY == 1);
            }

    state = 0;
    return((uint16_t*)&state);
    }

    static uint8_t commandCount = 3;

    if(queueCount >= (DRV_GFX_LCC_COMMANDQUEUESIZE-commandCount))
    {
        return (NULL); //queue is full
    }

    driverBusy = 1;

    while(commandCount--)
    {
        queueIndex->instance = commandCount;
        queueIndex->task = ALPHA_BLEND; //AlphaBlend

        if(commandCount == 2)
        {
            queueIndex->data = alphaParams->foregroundPage;
            queueIndex->count = alphaParams->foregroundLeft;
            queueIndex->lineCount = alphaParams->foregroundTop;
        }
        else if(commandCount == 1)
        {
            queueIndex->data = alphaParams->destinationPage;
            queueIndex->count = alphaParams->destinationLeft;
            queueIndex->lineCount = alphaParams->destinationTop;
        }
        else
        {
            queueIndex->count = width;
            queueIndex->lineCount = height;
            queueIndex->data = alpha;
            index = (uint16_t*)&queueIndex->count;
        }

        if(queueIndex == &commandBuffer[DRV_GFX_LCC_COMMANDQUEUESIZE-1])
        {
            queueIndex = &commandBuffer[0];
        }
        else
        {
            queueIndex++;
        }

        queueCount++;
     }

     commandCount = 3;
     driverBusy = 0;
     return(index);
}
