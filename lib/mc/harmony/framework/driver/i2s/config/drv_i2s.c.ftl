<#--
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->

/*** I2S Driver Initialization Data ***/
<#-- Instance 0 -->
<#if CONFIG_DRV_I2S_INST_0 == true>
const DRV_I2S_INIT drvI2S0InitData =
{
<#if CONFIG_DRV_I2S_POWER_STATE_IDX0?has_content>
    .moduleInit.value = DRV_I2S_POWER_STATE_IDX0,
</#if>
<#if CONFIG_DRV_I2S_PERIPHERAL_ID_IDX0?has_content>
    .spiID = DRV_I2S_PERIPHERAL_ID_IDX0, 
</#if>
<#if CONFIG_DRV_I2S_MODE_IDX0?has_content>
    .usageMode = DRV_I2S_USAGE_MODE_IDX0,
</#if>
<#if CONFIG_SPI_BAUD_RATE_CLK_IDX0?has_content>
    .baudClock = SPI_BAUD_RATE_CLK_IDX0,
</#if>
<#if CONFIG_BAUD_RATE_IDX0?has_content>
    .baud = BAUD_RATE_IDX0,
</#if>
<#if CONFIG_DRV_I2S_CLK_MODE_IDX0?has_content>
    .clockMode = DRV_I2S_CLK_MODE_IDX0,
</#if>
<#if CONFIG_SPI_AUDIO_COMM_WIDTH_IDX0?has_content>
    .moduleInit.value = SPI_AUDIO_COMM_WIDTH_IDX0,
</#if>
<#if CONFIG_SPI_AUDIO_TRANSMIT_MODE_IDX0?has_content>
	.audioTransmitMode = SPI_AUDIO_TRANSMIT_MODE_IDX0,
</#if>
<#if CONFIG_SPI_INPUT_SAMPLING_PHASE_IDX0?has_content>
	.inputSamplePhase = SPI_INPUT_SAMPLING_PHASE_IDX0,
</#if>
<#if CONFIG_DRV_I2S_AUDIO_PROTOCOL_MODE_IDX0?has_content>
	.protocolMode = DRV_I2S_AUDIO_PROTOCOL_MODE_IDX0,
</#if>
<#if CONFIG_DRV_I2S_INTERRUPT_MODE == true>
<#if CONFIG_DRV_I2S_TX_INT_SRC_IDX0?has_content>
    .txInterruptSource = DRV_I2S_TX_INT_SRC_IDX0,
</#if>
<#if CONFIG_DRV_I2S_RX_INT_SRC_IDX0?has_content>
    .rxInterruptSource = DRV_I2S_RX_INT_SRC_IDX0,
</#if>
<#if CONFIG_DRV_I2S_ERR_INT_SRC_IDX0?has_content>
    .errorInterruptSource = DRV_I2S_ERR_INT_SRC_IDX0,
</#if>
</#if>
<#if CONFIG_QUEUE_SIZE_TX_IDX0?has_content>
    .queueSizeTransmit = QUEUE_SIZE_TX_IDX0,
</#if>
<#if CONFIG_QUEUE_SIZE_RX_IDX0?has_content>
    .queueSizeReceive = QUEUE_SIZE_RX_IDX0,
</#if>
<#if CONFIG_DRV_I2S_TX_DMA_CHANNEL_IDX0?has_content>
    .dmaChannelTransmit = DRV_I2S_TX_DMA_CHANNEL_IDX0,
</#if>
<#if CONFIG_DRV_I2S_RX_DMA_CHANNEL_IDX0?has_content>
    .dmaChannelReceive = DRV_I2S_RX_DMA_CHANNEL_IDX0
</#if>
};
</#if>

<#-- Instance 1 -->
<#if CONFIG_DRV_I2S_INST_1 == true>
const DRV_I2S_INIT drvI2S1InitData =
{
<#if CONFIG_DRV_I2S_POWER_STATE_IDX1?has_content>
    .moduleInit.value = SYS_MODULE_POWER_STATE_IDX1,
</#if>
<#if CONFIG_DRV_I2S_PERIPHERAL_ID_IDX1?has_content>
    .spiID = DRV_I2S_PERIPHERAL_ID_IDX1, 
</#if>
<#if CONFIG_DRV_I2S_MODE_IDX1?has_content>
    .usageMode = DRV_I2S_USAGE_MODE_IDX1,
</#if>
<#if CONFIG_SPI_BAUD_RATE_CLK_IDX1?has_content>
    .baudClock = SPI_BAUD_RATE_CLK_IDX1,
</#if>
<#if CONFIG_BAUD_RATE_IDX1?has_content>
    .baud = BAUD_RATE_IDX1,
</#if>
<#if CONFIG_DRV_I2S_CLK_MODE_IDX1?has_content>
    .clockMode = DRV_I2S_CLK_MODE_IDX1,
</#if>
<#if CONFIG_SPI_AUDIO_COMM_WIDTH_IDX1?has_content>
    .moduleInit.value = SPI_AUDIO_COMM_WIDTH_IDX1,
</#if>
<#if CONFIG_SPI_AUDIO_TRANSMIT_MODE_IDX1?has_content>
	.audioTransmitMode = SPI_AUDIO_TRANSMIT_MODE_IDX1,
</#if>
<#if CONFIG_SPI_INPUT_SAMPLING_PHASE_IDX1?has_content>
	.inputSamplePhase = SPI_INPUT_SAMPLING_PHASE_IDX1,
</#if>
<#if CONFIG_DRV_I2S_AUDIO_PROTOCOL_MODE_IDX1?has_content>
	.protocolMode = DRV_I2S_AUDIO_PROTOCOL_MODE_IDX1,
</#if>
<#if CONFIG_DRV_I2S_INTERRUPT_MODE == true>
<#if CONFIG_DRV_I2S_TX_INT_SRC_IDX1?has_content>
    .txInterruptSource = DRV_I2S_TX_INT_SRC_IDX1,
</#if>
<#if CONFIG_DRV_I2S_RX_INT_SRC_IDX1?has_content>
    .rxInterruptSource = DRV_I2S_RX_INT_SRC_IDX1,
</#if>
<#if CONFIG_DRV_I2S_ERR_INT_SRC_IDX1?has_content>
    .errorInterruptSource = DRV_I2S_ERR_INT_SRC_IDX1,
</#if>
</#if>
<#if CONFIG_QUEUE_SIZE_TX_IDX1?has_content>
    .queueSizeTransmit = QUEUE_SIZE_TX_IDX1,
</#if>
<#if CONFIG_QUEUE_SIZE_RX_IDX1?has_content>
    .queueSizeReceive = QUEUE_SIZE_RX_IDX1,
</#if>
<#if CONFIG_DRV_I2S_TX_DMA_CHANNEL_IDX1?has_content>
    .dmaChannelTransmit = DRV_I2S_TX_DMA_CHANNEL_IDX1,
</#if>
<#if CONFIG_DRV_I2S_RX_DMA_CHANNEL_IDX1?has_content>
    .dmaChannelReceive = DRV_I2S_RX_DMA_CHANNEL_IDX1,
</#if>
};
</#if>

<#-- Instance 0 -->
<#if CONFIG_DRV_I2S_INST_2 == true>
const DRV_I2S_INIT drvI2S2InitData =
{
<#if CONFIG_DRV_I2S_POWER_STATE_IDX2?has_content>
    .moduleInit.value = SYS_MODULE_POWER_STATE_IDX2,
</#if>
<#if CONFIG_DRV_I2S_PERIPHERAL_ID_IDX2?has_content>
    .spiID = DRV_I2S_PERIPHERAL_ID_IDX2, 
</#if>
<#if CONFIG_DRV_I2S_MODE_IDX2?has_content>
    .usageMode = DRV_I2S_USAGE_MODE_IDX2,
</#if>
<#if CONFIG_SPI_BAUD_RATE_CLK_IDX2?has_content>
    .baudClock = SPI_BAUD_RATE_CLK_IDX2,
</#if>
<#if CONFIG_BAUD_RATE_IDX2?has_content>
    .baud = BAUD_RATE_IDX2,
</#if>
<#if CONFIG_DRV_I2S_CLK_MODE_IDX2?has_content>
    .clockMode = DRV_I2S_CLK_MODE_IDX2,
</#if>
<#if CONFIG_SPI_AUDIO_COMM_WIDTH_IDX2?has_content>
    .moduleInit.value = SPI_AUDIO_COMM_WIDTH_IDX2,
</#if>
<#if CONFIG_SPI_AUDIO_TRANSMIT_MODE_IDX2?has_content>
	.audioTransmitMode = SPI_AUDIO_TRANSMIT_MODE_IDX2,
</#if>
<#if CONFIG_SPI_INPUT_SAMPLING_PHASE_IDX2?has_content>
	.inputSamplePhase = SPI_INPUT_SAMPLING_PHASE_IDX2,
</#if>
<#if CONFIG_DRV_I2S_AUDIO_PROTOCOL_MODE_IDX2?has_content>
	.protocolMode = DRV_I2S_AUDIO_PROTOCOL_MODE_IDX2,
</#if>
<#if CONFIG_DRV_I2S_INTERRUPT_MODE == true>
<#if CONFIG_DRV_I2S_TX_INT_SRC_IDX2?has_content>
    .txInterruptSource = DRV_I2S_TX_INT_SRC_IDX2,
</#if>
<#if CONFIG_DRV_I2S_RX_INT_SRC_IDX2?has_content>
    .rxInterruptSource = DRV_I2S_RX_INT_SRC_IDX2,
</#if>
<#if CONFIG_DRV_I2S_ERR_INT_SRC_IDX2?has_content>
    .errorInterruptSource = DRV_I2S_ERR_INT_SRC_IDX2,
</#if>
</#if>
<#if CONFIG_QUEUE_SIZE_TX_IDX2?has_content>
    .queueSizeTransmit = QUEUE_SIZE_TX_IDX2,
</#if>
<#if CONFIG_QUEUE_SIZE_RX_IDX2?has_content>
    .queueSizeReceive = QUEUE_SIZE_RX_IDX2,
</#if>
<#if CONFIG_DRV_I2S_TX_DMA_CHANNEL_IDX2?has_content>
    .dmaChannelTransmit = DRV_I2S_TX_DMA_CHANNEL_IDX2,
</#if>
<#if CONFIG_DRV_I2S_RX_DMA_CHANNEL_IDX2?has_content>
    .dmaChannelReceive = DRV_I2S_RX_DMA_CHANNEL_IDX2,
</#if>
};
</#if>

<#-- Instance 3 -->
<#if CONFIG_DRV_I2S_INST_3 == true>
const DRV_I2S_INIT drvI2S3InitData =
{
<#if CONFIG_DRV_I2S_POWER_STATE_IDX3?has_content>
    .moduleInit.value = SYS_MODULE_POWER_STATE_IDX3,
</#if>
<#if CONFIG_DRV_I2S_PERIPHERAL_ID_IDX3?has_content>
    .spiID = DRV_I2S_PERIPHERAL_ID_IDX3, 
</#if>
<#if CONFIG_DRV_I2S_MODE_IDX3?has_content>
    .usageMode = DRV_I2S_USAGE_MODE_IDX3,
</#if>
<#if CONFIG_SPI_BAUD_RATE_CLK_IDX3?has_content>
    .baudClock = SPI_BAUD_RATE_CLK_IDX3,
</#if>
<#if CONFIG_BAUD_RATE_IDX3?has_content>
    .baud = BAUD_RATE_IDX3,
</#if>
<#if CONFIG_DRV_I2S_CLK_MODE_IDX3?has_content>
    .clockMode = DRV_I2S_CLK_MODE_IDX3,
</#if>
<#if CONFIG_SPI_AUDIO_COMM_WIDTH_IDX3?has_content>
    .moduleInit.value = SPI_AUDIO_COMM_WIDTH_IDX3,
</#if>
<#if CONFIG_SPI_AUDIO_TRANSMIT_MODE_IDX3?has_content>
	.audioTransmitMode = SPI_AUDIO_TRANSMIT_MODE_IDX3,
</#if>
<#if CONFIG_SPI_INPUT_SAMPLING_PHASE_IDX3?has_content>
	.inputSamplePhase = SPI_INPUT_SAMPLING_PHASE_IDX3,
</#if>
<#if CONFIG_DRV_I2S_AUDIO_PROTOCOL_MODE_IDX3?has_content>
	.protocolMode = DRV_I2S_AUDIO_PROTOCOL_MODE_IDX3,
</#if>
<#if CONFIG_DRV_I2S_INTERRUPT_MODE == true>
<#if CONFIG_DRV_I2S_TX_INT_SRC_IDX3?has_content>
    .txInterruptSource = DRV_I2S_TX_INT_SRC_IDX3,
</#if>
<#if CONFIG_DRV_I2S_RX_INT_SRC_IDX3?has_content>
    .rxInterruptSource = DRV_I2S_RX_INT_SRC_IDX3,
</#if>
<#if CONFIG_DRV_I2S_ERR_INT_SRC_IDX3?has_content>
    .errorInterruptSource = DRV_I2S_ERR_INT_SRC_IDX3,
</#if>
</#if>
<#if CONFIG_QUEUE_SIZE_TX_IDX3?has_content>
    .queueSizeTransmit = QUEUE_SIZE_TX_IDX3,
</#if>
<#if CONFIG_QUEUE_SIZE_RX_IDX3?has_content>
    .queueSizeReceive = QUEUE_SIZE_RX_IDX3,
</#if>
<#if CONFIG_DRV_I2S_TX_DMA_CHANNEL_IDX3?has_content>
    .dmaChannelTransmit = DRV_I2S_TX_DMA_CHANNEL_IDX3,
</#if>
<#if CONFIG_DRV_I2S_RX_DMA_CHANNEL_IDX3?has_content>
    .dmaChannelReceive = DRV_I2S_RX_DMA_CHANNEL_IDX3,
</#if>
};
</#if>

<#-- Instance 4 -->
<#if CONFIG_DRV_I2S_INST_4 == true>
const DRV_I2S_INIT drvI2S4InitData =
{
<#if CONFIG_DRV_I2S_POWER_STATE_IDX4?has_content>
    .moduleInit.value = SYS_MODULE_POWER_STATE_IDX4,
</#if>
<#if CONFIG_DRV_I2S_PERIPHERAL_ID_IDX4?has_content>
    .spiID = DRV_I2S_PERIPHERAL_ID_IDX4, 
</#if>
<#if CONFIG_DRV_I2S_MODE_IDX4?has_content>
    .usageMode = DRV_I2S_USAGE_MODE_IDX4,
</#if>
<#if CONFIG_SPI_BAUD_RATE_CLK_IDX4?has_content>
    .baudClock = SPI_BAUD_RATE_CLK_IDX4,
</#if>
<#if CONFIG_BAUD_RATE_IDX4?has_content>
    .baud = BAUD_RATE_IDX4,
</#if>
<#if CONFIG_DRV_I2S_CLK_MODE_IDX4?has_content>
    .clockMode = DRV_I2S_CLK_MODE_IDX4,
</#if>
<#if CONFIG_SPI_AUDIO_COMM_WIDTH_IDX4?has_content>
    .moduleInit.value = SPI_AUDIO_COMM_WIDTH_IDX4,
</#if>
<#if CONFIG_SPI_AUDIO_TRANSMIT_MODE_IDX4?has_content>
	.audioTransmitMode = SPI_AUDIO_TRANSMIT_MODE_IDX4,
</#if>
<#if CONFIG_SPI_INPUT_SAMPLING_PHASE_IDX4?has_content>
	.inputSamplePhase = SPI_INPUT_SAMPLING_PHASE_IDX4,
</#if>
<#if CONFIG_DRV_I2S_AUDIO_PROTOCOL_MODE_IDX4?has_content>
	.protocolMode = DRV_I2S_AUDIO_PROTOCOL_MODE_IDX4,
</#if>
<#if CONFIG_DRV_I2S_INTERRUPT_MODE == true>
<#if CONFIG_DRV_I2S_TX_INT_SRC_IDX4?has_content>
    .txInterruptSource = DRV_I2S_TX_INT_SRC_IDX4,
</#if>
<#if CONFIG_DRV_I2S_RX_INT_SRC_IDX4?has_content>
    .rxInterruptSource = DRV_I2S_RX_INT_SRC_IDX4,
</#if>
<#if CONFIG_DRV_I2S_ERR_INT_SRC_IDX4?has_content>
    .errorInterruptSource = DRV_I2S_ERR_INT_SRC_IDX4,
</#if>
</#if>
<#if CONFIG_QUEUE_SIZE_TX_IDX4?has_content>
    .queueSizeTransmit = QUEUE_SIZE_TX_IDX4,
</#if>
<#if CONFIG_QUEUE_SIZE_RX_IDX4?has_content>
    .queueSizeReceive = QUEUE_SIZE_RX_IDX4,
</#if>
<#if CONFIG_DRV_I2S_TX_DMA_CHANNEL_IDX4?has_content>
    .dmaChannelTransmit = DRV_I2S_TX_DMA_CHANNEL_IDX4,
</#if>
<#if CONFIG_DRV_I2S_RX_DMA_CHANNEL_IDX4?has_content>
    .dmaChannelReceive = DRV_I2S_RX_DMA_CHANNEL_IDX4,
</#if>
};
</#if>

<#-- Instance 5 -->
<#if CONFIG_DRV_I2S_INST_5 == true>
const DRV_I2S_INIT drvI2S5InitData =
{
<#if CONFIG_DRV_I2S_POWER_STATE_IDX5?has_content>
    .moduleInit.value = SYS_MODULE_POWER_STATE_IDX5,
</#if>
<#if CONFIG_DRV_I2S_PERIPHERAL_ID_IDX5?has_content>
    .spiID = DRV_I2S_PERIPHERAL_ID_IDX5, 
</#if>
<#if CONFIG_DRV_I2S_MODE_IDX5?has_content>
    .usageMode = DRV_I2S_USAGE_MODE_IDX5,
    .usageMode = DRV_I2S_USAGE_MODE_IDX5,
</#if>
<#if CONFIG_SPI_BAUD_RATE_CLK_IDX5?has_content>
    .baudClock = SPI_BAUD_RATE_CLK_IDX5,
</#if>
<#if CONFIG_BAUD_RATE_IDX5?has_content>
    .baud = BAUD_RATE_IDX5,
</#if>
<#if CONFIG_DRV_I2S_CLK_MODE_IDX0?has_content>
    .clockMode = DRV_I2S_CLK_MODE_IDX5,
</#if>
<#if CONFIG_SPI_AUDIO_COMM_WIDTH_IDX5?has_content>
    .moduleInit.value = SPI_AUDIO_COMM_WIDTH_IDX5,
</#if>
<#if CONFIG_SPI_AUDIO_TRANSMIT_MODE_IDX5?has_content>
	.audioTransmitMode = SPI_AUDIO_TRANSMIT_MODE_IDX5,
</#if>
<#if CONFIG_SPI_INPUT_SAMPLING_PHASE_IDX5?has_content>
	.inputSamplePhase = SPI_INPUT_SAMPLING_PHASE_IDX5,
</#if>
<#if CONFIG_DRV_I2S_AUDIO_PROTOCOL_MODE_IDX5?has_content>
	.protocolMode = DRV_I2S_AUDIO_PROTOCOL_MODE_IDX5,
</#if>
<#if CONFIG_DRV_I2S_INTERRUPT_MODE == true>
<#if CONFIG_DRV_I2S_TX_INT_SRC_IDX5?has_content>
    .txInterruptSource = DRV_I2S_TX_INT_SRC_IDX5,
</#if>
<#if CONFIG_DRV_I2S_RX_INT_SRC_IDX5?has_content>
    .rxInterruptSource = DRV_I2S_RX_INT_SRC_IDX5,
</#if>
<#if CONFIG_DRV_I2S_ERR_INT_SRC_IDX5?has_content>
    .errorInterruptSource = DRV_I2S_ERR_INT_SRC_IDX5,
</#if>
</#if>
<#if CONFIG_QUEUE_SIZE_TX_IDX5?has_content>
    .queueSizeTransmit = QUEUE_SIZE_TX_IDX5,
</#if>
<#if CONFIG_QUEUE_SIZE_RX_IDX5?has_content>
    .queueSizeReceive = QUEUE_SIZE_RX_IDX5,
</#if>
<#if CONFIG_DRV_I2S_TX_DMA_CHANNEL_IDX5?has_content>
    .dmaChannelTransmit = DRV_I2S_TX_DMA_CHANNEL_IDX5,
</#if>
<#if CONFIG_DRV_I2S_RX_DMA_CHANNEL_IDX5?has_content>
    .dmaChannelReceive = DRV_I2S_RX_DMA_CHANNEL_IDX5,
</#if>
};
</#if>

<#--
/*******************************************************************************
 End of File
*/
-->
