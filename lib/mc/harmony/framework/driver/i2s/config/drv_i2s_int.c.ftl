<#--
/*******************************************************************************
  I2S Driver Interrupt Handler Template File

  File Name:
    drv_i2s_int.c

  Summary:
    This file contains source code necessary to initialize the system.

  Description:
    This file contains source code necessary to initialize the system.  It
    implements the "SYS_Initialize" function, configuration bits, and allocates
    any necessary global system resources, such as the systemObjects structure
    that contains the object handles to all the MPLAB Harmony module objects in
    the system.
 *******************************************************************************/

/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->
<#if CONFIG_DRV_I2S_INTERRUPT_MODE == true>
<#if CONFIG_DRV_I2S_INST_0 == true>
void __ISR(${CONFIG_DRV_I2S_TX_ISR_VECTOR_IDX0}, ipl${CONFIG_DRV_I2S_TX_INT_PRIO_NUM_IDX0}) _IntHandlerDrvI2STransmitInstance0(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S0);
}
void __ISR(${CONFIG_DRV_I2S_RX_ISR_VECTOR_IDX0}, ipl${CONFIG_DRV_I2S_RX_INT_PRIO_NUM_IDX0}) _IntHandlerDrvI2SReceiveInstance0(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S0);
}
void __ISR(${CONFIG_DRV_I2S_ERR_ISR_VECTOR_IDX0}, ipl${CONFIG_DRV_I2S_ERR_INT_PRIO_NUM_IDX0}) _IntHandlerDrvI2SErrorInstance0(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S0);
}
</#if>

<#if CONFIG_DRV_I2S_INST_1 == true>
void __ISR(${CONFIG_DRV_I2S_TX_ISR_VECTOR_IDX1}, ipl${CONFIG_DRV_I2S_TX_INT_PRIO_NUM_IDX1}) _IntHandlerDrvI2STransmitInstance1(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S1);
}
void __ISR(${CONFIG_DRV_I2S_RX_ISR_VECTOR_IDX1}, ipl${CONFIG_DRV_I2S_RX_INT_PRIO_NUM_IDX1}) _IntHandlerDrvI2SReceiveInstance1(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S1);
}
void __ISR(${CONFIG_DRV_I2S_ERR_ISR_VECTOR_IDX1}, ipl${CONFIG_DRV_I2S_ERR_INT_PRIO_NUM_IDX1}) _IntHandlerDrvI2SErrorInstance1(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S1);
}
</#if>

<#if CONFIG_DRV_I2S_INST_2 == true>
void __ISR(${CONFIG_DRV_I2S_TX_ISR_VECTOR_IDX2}, ipl${CONFIG_DRV_I2S_TX_INT_PRIO_NUM_IDX2}) _IntHandlerDrvI2STransmitInstance2(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S2);
}
void __ISR(${CONFIG_DRV_I2S_RX_ISR_VECTOR_IDX2}, ipl${CONFIG_DRV_I2S_RX_INT_PRIO_NUM_IDX2}) _IntHandlerDrvI2SReceiveInstance2(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S2);
}
void __ISR(${CONFIG_DRV_I2S_ERR_ISR_VECTOR_IDX2}, ipl${CONFIG_DRV_I2S_ERR_INT_PRIO_NUM_IDX2}) _IntHandlerDrvI2SErrorInstance2(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S2);
}
</#if>

<#if CONFIG_DRV_I2S_INST_3 == true>
void __ISR(${CONFIG_DRV_I2S_TX_ISR_VECTOR_IDX3}, ipl${CONFIG_DRV_I2S_TX_INT_PRIO_NUM_IDX3}) _IntHandlerDrvI2STransmitInstance3(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S3);
}
void __ISR(${CONFIG_DRV_I2S_RX_ISR_VECTOR_IDX3}, ipl${CONFIG_DRV_I2S_RX_INT_PRIO_NUM_IDX3}) _IntHandlerDrvI2SReceiveInstance3(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S3);
}
void __ISR(${CONFIG_DRV_I2S_ERR_ISR_VECTOR_IDX3}, ipl${CONFIG_DRV_I2S_ERR_INT_PRIO_NUM_IDX3}) _IntHandlerDrvI2SErrorInstance3(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S3);
}
</#if>

<#if CONFIG_DRV_I2S_INST_4 == true>
void __ISR(${CONFIG_DRV_I2S_TX_ISR_VECTOR_IDX4}, ipl${CONFIG_DRV_I2S_TX_INT_PRIO_NUM_IDX4}) _IntHandlerDrvI2STransmitInstance4(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S4);
}
void __ISR(${CONFIG_DRV_I2S_RX_ISR_VECTOR_IDX4}, ipl${CONFIG_DRV_I2S_RX_INT_PRIO_NUM_IDX4}) _IntHandlerDrvI2SReceiveInstance4(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S4);
}
void __ISR(${CONFIG_DRV_I2S_ERR_ISR_VECTOR_IDX4}, ipl${CONFIG_DRV_I2S_ERR_INT_PRIO_NUM_IDX4}) _IntHandlerDrvI2SErrorInstance4(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S4);
}
</#if>

<#if CONFIG_DRV_I2S_INST_5 == true>
void __ISR(${CONFIG_DRV_I2S_TX_ISR_VECTOR_IDX5}, ipl${CONFIG_DRV_I2S_TX_INT_PRIO_NUM_IDX5}) _IntHandlerDrvI2STransmitInstance5(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S5);
}
void __ISR(${CONFIG_DRV_I2S_RX_ISR_VECTOR_IDX5}, ipl${CONFIG_DRV_I2S_RX_INT_PRIO_NUM_IDX5}) _IntHandlerDrvI2SReceiveInstance5(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S5);
}
void __ISR(${CONFIG_DRV_I2S_ERR_ISR_VECTOR_IDX5}, ipl${CONFIG_DRV_I2S_ERR_INT_PRIO_NUM_IDX5}) _IntHandlerDrvI2SErrorInstance5(void)
{
    DRV_I2S_Tasks(sysObj.drvI2S5);
}
</#if>

<#--
/*******************************************************************************
 End of File
*/
-->
</#if>