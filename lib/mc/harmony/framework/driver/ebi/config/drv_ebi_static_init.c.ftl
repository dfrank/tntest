<#--
/*******************************************************************************
  EBI Driver Initialization File

  File Name:
    drv_ebi_static_init.c.ftl

  Summary:
    This file contains source code necessary to initialize the IC driver.

  Description:
    This file contains source code necessary to initialize the system.  It
    implements the "SYS_Initialize" function, configuration bits, and allocates
    any necessary global system resources, such as the systemObjects structure
    that contains the object handles to all the MPLAB Harmony module objects in
    the system.
 *******************************************************************************/

/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->
<#if CONFIG_USE_DRV_EBI == true>
<#if CONFIG_DRV_EBI_DRIVER_MODE == "STATIC">
/*******************************************************************************
  Function:
    void DRV_EBI_Initialize(void)

  Summary:
    Initializes EBI Driver Instance

  Remarks:
 */
void DRV_EBI_Initialize(void)
{
    /* Configure EBI I/O */
    PLIB_EBI_ControlEnableSet(EBI_ID_0, true); /* Shared I/O Controlled by EBI */
    PLIB_EBI_AddressPinEnableBitsSet(EBI_ID_0, ${CONFIG_DRV_EBI_ADDRESS_PORT_CSX0}); /* Enable Address Pins */
    PLIB_EBI_DataEnableSet(EBI_ID_0, ${CONFIG_DRV_EBI_UPPER_BYTE_ENABLE_CSX0}, ${CONFIG_DRV_EBI_LOWER_BYTE_ENABLE_CSX0}); /* Data Byte Enables */ 
<#if CONFIG_DRV_EBI_USE_RDY_PINS == true>
<#if CONFIG_DRV_EBI_USE_RDY1_PIN == true>
<#if CONFIG_DRV_EBI_USE_RDY1_PIN_INV == true>
    PLIB_EBI_ReadyPin1ConfigSet (EBI_ID_0, true, true); /* EBIRDY1 Enabled and Inverted */
<#else>
    PLIB_EBI_ReadyPin1ConfigSet (EBI_ID_0, true, false);	/* EBIRDY1 Enabled but not Inverted */
</#if>
</#if>
<#if CONFIG_DRV_EBI_USE_RDY2_PIN == true>
<#if CONFIG_DRV_EBI_USE_RDY2_PIN_INV == true>
    PLIB_EBI_ReadyPin2ConfigSet (EBI_ID_0, true, true); /* EBIRDY2 Enabled and Inverted */
<#else>   
    PLIB_EBI_ReadyPin2ConfigSet (EBI_ID_0, true, false); /* EBIRDY2 Enabled but not Inverted */
</#if>
</#if>
<#if CONFIG_DRV_EBI_USE_RDY3_PIN == true>
<#if CONFIG_DRV_EBI_USE_RDY3_PIN_INV == true>
    PLIB_EBI_ReadyPin3ConfigSet (EBI_ID_0, true, true); /* EBIRDY3 Enabled and Inverted */
<#else>
    PLIB_EBI_ReadyPin3ConfigSet (EBI_ID_0, true, false); /* EBIRDY3 Enabled but not Inverted */
</#if>
</#if>
</#if>
<#if CONFIG_DRV_EBI_RDY_PIN_SENSITIVITY == "EDGE">
    PLIB_EBI_ReadyPinSensSet (EBI_ID_0, false); /* EBIRDYx is edge sensitive */  
</#if>
<#if CONFIG_DRV_EBI_RDY_PIN_SENSITIVITY == "LEVEL">
    PLIB_EBI_ReadyPinSensSet (EBI_ID_0, true); /* EBIRDYx is level sensitive */
</#if>
<#if CONFIG_DRV_EBI_ENABLE_WE == true>
<#if CONFIG_DRV_EBI_ENABLE_OE == true>
    PLIB_EBI_WriteOutputControlSet (EBI_ID_0, true, true); /* /EBIWE and /EBIOE are enabled */ 
<#else>   
    PLIB_EBI_WriteOutputControlSet (EBI_ID_0, true, false); /* /EBIWE is enabled /EBIOE is disabled */
</#if>
<#else>
    PLIB_EBI_WriteOutputControlSet (EBI_ID_0, false, false); /* /EBIWE and /EBIOE are disabled */    
</#if>
<#if CONFIG_DRV_EBI_CSX0 == true && CONFIG_DRV_EBI_CSX1 == false && CONFIG_DRV_EBI_CSX2 == false && CONFIG_DRV_EBI_CSX3 == false>
    PLIB_EBI_ChipSelectEnableSet (EBI_ID_0, true, false, false, false); /* /EBICSx Pin Controls */
</#if>   
<#if CONFIG_DRV_EBI_CSX0 == true && CONFIG_DRV_EBI_CSX1 == true && CONFIG_DRV_EBI_CSX2 == false && CONFIG_DRV_EBI_CSX3 == false>
    PLIB_EBI_ChipSelectEnableSet (EBI_ID_0, true, true, false, false); /* /EBICSx Pin Controls */   
</#if>   
<#if CONFIG_DRV_EBI_CSX0 == true && CONFIG_DRV_EBI_CSX1 == true && CONFIG_DRV_EBI_CSX2 == true && CONFIG_DRV_EBI_CSX3 == false>
    PLIB_EBI_ChipSelectEnableSet (EBI_ID_0, true, true, true, false); /* /EBICSx Pin Controls */
</#if>
<#if CONFIG_DRV_EBI_CSX0 == true && CONFIG_DRV_EBI_CSX1 == true && CONFIG_DRV_EBI_CSX2 == true && CONFIG_DRV_EBI_CSX3 == true>
    PLIB_EBI_ChipSelectEnableSet (EBI_ID_0, true, true, true, true); /* /EBICSx Pin Controls */   
</#if>
<#if (CONFIG_DRV_EBI_BYTE_SELECT_PIN0 && CONFIG_DRV_EBI_BYTE_SELECT_PIN1) == true>
    PLIB_EBI_ByteSelectPinSet(EBI_ID_0, true, true); /* /EBIBS0 and /EBIBS1 enabled */
</#if> 
<#if CONFIG_DRV_EBI_BYTE_SELECT_PIN0 == true && CONFIG_DRV_EBI_BYTE_SELECT_PIN1 == false>
    PLIB_EBI_ByteSelectPinSet(EBI_ID_0, true, false); /* /EBIBS0 enabled and /EBIBS1 disabled */
</#if> 
<#if CONFIG_DRV_EBI_BYTE_SELECT_PIN0 == false && CONFIG_DRV_EBI_BYTE_SELECT_PIN1 == true>
    PLIB_EBI_ByteSelectPinSet(EBI_ID_0, false, true); /* /EBIBS0 disabled and /EBIBS1 enabled */
</#if>   
<#if CONFIG_DRV_EBI_BYTE_SELECT_PIN0 == false && CONFIG_DRV_EBI_BYTE_SELECT_PIN1 == false>
    PLIB_EBI_ByteSelectPinSet(EBI_ID_0, false, false); /* /EBIBS0 and /EBIBS1 disabled */
</#if> 
</#if>
</#if>
<#if CONFIG_DRV_EBI_CSX0 == true> 

    /* Initiialize EBI for Memory on EBICS0 */
    PLIB_EBI_BaseAddressSet(EBI_ID_0, 0, ${CONFIG_DRV_EBI_BASE_ADDR_CSX0}); /* Setup EBICS0*/
    PLIB_EBI_MemoryCharacteristicsSet(EBI_ID_0, 0, ${CONFIG_DRV_EBI_MEMORY_TYPE_CSX0}, ${CONFIG_DRV_EBI_MEMORY_SIZE_CSX0}, ${CONFIG_DRV_EBI_CS_TIMING_CSX0}); /* Setup EBIMSK0 */
<#if CONFIG_DRV_EBI_CS_TIMING_CSX0 == "CS_TIMING_0">
<#if CONFIG_DRV_EBI_USE_RDY_PINS == true>
    PLIB_EBI_ReadyModeSet(EBI_ID_0, true, false, false); /* Setup EBISMT0->RDYMODE */
</#if>
<#if CONFIG_DRV_EBI_PAGE_MODE_CSX0 == true>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, true, ${CONFIG_DRV_EBI_PAGE_SIZE_CSX0});
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 0, ${CONFIG_DRV_EBI_MEM_PAGE_MODE_TRC_CSX0}, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX0}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX0}); /* Setup EBISMT0 */
<#else>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, false, PAGE_WORD4);
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 0, 0, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX0}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX0}); /* Setup EBISMT0 */
</#if>
</#if>
<#if CONFIG_DRV_EBI_CS_TIMING_CSX0 == "CS_TIMING_1">
<#if CONFIG_DRV_EBI_USE_RDY_PINS == true>
    PLIB_EBI_ReadyModeSet(EBI_ID_0, true, false, false); /* Setup EBISMT0->RDYMODE */
</#if>
<#if CONFIG_DRV_EBI_PAGE_MODE_CSX0 == true>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, true, ${CONFIG_DRV_EBI_PAGE_SIZE_CSX0});
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 1, ${CONFIG_DRV_EBI_MEM_PAGE_MODE_TRC_CSX0}, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX0}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX0}); /* Setup EBISMT1 */
<#else>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, false, PAGE_WORD4);
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 1, 0, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX0}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX0}); /* Setup EBISMT1 */
</#if>
</#if>
<#if CONFIG_DRV_EBI_CS_TIMING_CSX0 == "CS_TIMING_2">
<#if CONFIG_DRV_EBI_USE_RDY_PINS == true>
    PLIB_EBI_ReadyModeSet(EBI_ID_0, false, true, false); /* Setup EBISMT2->RDYMODE */
</#if>
<#if CONFIG_DRV_EBI_PAGE_MODE_CSX0 == true>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, true, ${CONFIG_DRV_EBI_PAGE_SIZE_CSX0}); /* Memory Page Setup */
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 2, ${CONFIG_DRV_EBI_MEM_PAGE_MODE_TRC_CSX0}, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX0}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX0}); /* Setup EBISMT2 */
<#else>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, false, PAGE_WORD4); /* Memory Page Setup */
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 2, 0, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX0}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX0}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX0}); /* Setup EBISMT2 */
</#if>
</#if>
    PLIB_EBI_StaticMemoryWidthRegisterSet (EBI_ID_0, 0, ${CONFIG_DRV_EBI_STATIC_MEMORY_WIDTH_CSX0}); /* Setup EBISMCON->SMWIDTH0 */
</#if>
<#if CONFIG_DRV_EBI_CSX1 == true> 

    /* Initiialize EBI for Memory on EBICS1 */
    PLIB_EBI_BaseAddressSet(EBI_ID_0, 1, ${CONFIG_DRV_EBI_BASE_ADDR_CSX1}); /* Setup EBICS1*/
    PLIB_EBI_MemoryCharacteristicsSet(EBI_ID_0, 1, ${CONFIG_DRV_EBI_MEMORY_TYPE_CSX1}, ${CONFIG_DRV_EBI_MEMORY_SIZE_CSX1}, ${CONFIG_DRV_EBI_CS_TIMING_CSX1}); /* Setup EBIMSK1 */
<#if CONFIG_DRV_EBI_CS_TIMING_CSX1 == "CS_TIMING_0">
<#if CONFIG_DRV_EBI_USE_RDY_PINS == true>
    PLIB_EBI_ReadyModeSet(EBI_ID_0, true, false, false); /* Setup EBISMT0->RDYMODE */
</#if>
<#if CONFIG_DRV_EBI_PAGE_MODE_CSX1 == true>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, true, ${CONFIG_DRV_EBI_PAGE_SIZE_CSX1});
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 0, ${CONFIG_DRV_EBI_MEM_PAGE_MODE_TRC_CSX1}, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX1}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX1}); /* Setup EBISMT0 */
<#else>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, false, PAGE_WORD4);
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 0, 0, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX1}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX1}); /* Setup EBISMT0 */
</#if>
</#if>
<#if CONFIG_DRV_EBI_CS_TIMING_CSX1 == "CS_TIMING_1">
<#if CONFIG_DRV_EBI_USE_RDY_PINS == true>
    PLIB_EBI_ReadyModeSet(EBI_ID_0, true, false, false); /* Setup EBISMT0->RDYMODE */
</#if>
<#if CONFIG_DRV_EBI_PAGE_MODE_CSX1 == true>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, true, ${CONFIG_DRV_EBI_PAGE_SIZE_CSX1}); /* Memory Page Setup */
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 1, ${CONFIG_DRV_EBI_MEM_PAGE_MODE_TRC_CSX1}, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX1}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX1}); /* Setup EBISMT1 */
<#else>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, false, PAGE_WORD4); /* Memory Page Setup */
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 1, 0, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX1}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX1}); /* Setup EBISMT1 */
</#if>
</#if>
<#if CONFIG_DRV_EBI_CS_TIMING_CSX1 == "CS_TIMING_2">
<#if CONFIG_DRV_EBI_USE_RDY_PINS == true>
    PLIB_EBI_ReadyModeSet(EBI_ID_0, false, true, false); /* Setup EBISMT2->RDYMODE */
</#if>
<#if CONFIG_DRV_EBI_PAGE_MODE_CSX1 == true>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, true, ${CONFIG_DRV_EBI_PAGE_SIZE_CSX1}); /* Memory Page Setup */
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 2, ${CONFIG_DRV_EBI_MEM_PAGE_MODE_TRC_CSX1}, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX1}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX1}); /* Setup EBISMT2 */
<#else>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, false, PAGE_WORD4); /* Memory Page Setup */
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 2, 0, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX1}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX1}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX1}); /* Setup EBISMT2 */
</#if>
</#if>
    PLIB_EBI_StaticMemoryWidthRegisterSet (EBI_ID_0, 1, ${CONFIG_DRV_EBI_STATIC_MEMORY_WIDTH_CSX1}); /* Setup EBISMCON->SMWIDTH0 */
</#if>
<#if CONFIG_DRV_EBI_CSX2 == true> 

    /* Initiialize EBI for Memory on EBICS2 */
    PLIB_EBI_BaseAddressSet(EBI_ID_0, 2, ${CONFIG_DRV_EBI_BASE_ADDR_CSX2}); /* Setup EBICS2*/
    PLIB_EBI_MemoryCharacteristicsSet(EBI_ID_0, 2, ${CONFIG_DRV_EBI_MEMORY_TYPE_CSX2}, ${CONFIG_DRV_EBI_MEMORY_SIZE_CSX2}, ${CONFIG_DRV_EBI_CS_TIMING_CSX2}); /* Setup EBIMSK2 */
<#if CONFIG_DRV_EBI_CS_TIMING_CSX2 == "CS_TIMING_0">
<#if CONFIG_DRV_EBI_USE_RDY_PINS == true>
    PLIB_EBI_ReadyModeSet(EBI_ID_0, true, false, false); /* Setup EBISMT0->RDYMODE */
</#if>
<#if CONFIG_DRV_EBI_PAGE_MODE_CSX2 == true>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, true, ${CONFIG_DRV_EBI_PAGE_SIZE_CSX2});
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 0, ${CONFIG_DRV_EBI_MEM_PAGE_MODE_TRC_CSX2}, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX2}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX2}); /* Setup EBISMT0 */
<#else>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, false, PAGE_WORD4);
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 0, 0, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX2}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX2}); /* Setup EBISMT0 */
</#if>
</#if>
<#if CONFIG_DRV_EBI_CS_TIMING_CSX2 == "CS_TIMING_1">
<#if CONFIG_DRV_EBI_USE_RDY_PINS == true>
    PLIB_EBI_ReadyModeSet(EBI_ID_0, true, false, false); /* Setup EBISMT0->RDYMODE */
</#if>
<#if CONFIG_DRV_EBI_PAGE_MODE_CSX2 == true>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, true, ${CONFIG_DRV_EBI_PAGE_SIZE_CSX2}); /* Memory Page Setup */
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 1, ${CONFIG_DRV_EBI_MEM_PAGE_MODE_TRC_CSX2}, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX2}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX2}); /* Setup EBISMT1 */
<#else>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, false, PAGE_WORD4); /* Memory Page Setup */
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 1, 0, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX2}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX2}); /* Setup EBISMT1 */
</#if>
</#if>
<#if CONFIG_DRV_EBI_CS_TIMING_CSX2 == "CS_TIMING_2">
<#if CONFIG_DRV_EBI_USE_RDY_PINS == true>
    PLIB_EBI_ReadyModeSet(EBI_ID_0, false, true, false); /* Setup EBISMT2->RDYMODE */
</#if>
<#if CONFIG_DRV_EBI_PAGE_MODE_CSX2 == true>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, true, ${CONFIG_DRV_EBI_PAGE_SIZE_CSX2}); /* Memory Page Setup */
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 2, ${CONFIG_DRV_EBI_MEM_PAGE_MODE_TRC_CSX2}, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX2}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX2}); /* Setup EBISMT2 */
<#else>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, false, PAGE_WORD4); /* Memory Page Setup */
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 2, 0, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX2}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX2}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX2}); /* Setup EBISMT2 */
</#if>
</#if>
    PLIB_EBI_StaticMemoryWidthRegisterSet (EBI_ID_0, 2, ${CONFIG_DRV_EBI_STATIC_MEMORY_WIDTH_CSX2}); /* Setup EBISMCON->SMWIDTH2 */
</#if>
<#if CONFIG_DRV_EBI_CSX3 == true> 

    /* Initiialize EBI for Memory on EBICS3 */
    PLIB_EBI_BaseAddressSet(EBI_ID_0, 3, ${CONFIG_DRV_EBI_BASE_ADDR_CSX3}); /* Setup EBICS3*/
    PLIB_EBI_MemoryCharacteristicsSet(EBI_ID_0, 3, ${CONFIG_DRV_EBI_MEMORY_TYPE_CSX3}, ${CONFIG_DRV_EBI_MEMORY_SIZE_CSX3}, ${CONFIG_DRV_EBI_CS_TIMING_CSX3}); /* Setup EBIMSK3 */
<#if CONFIG_DRV_EBI_CS_TIMING_CSX3 == "CS_TIMING_0">
<#if CONFIG_DRV_EBI_USE_RDY_PINS == true>
    PLIB_EBI_ReadyModeSet(EBI_ID_0, true, false, false); /* Setup EBISMT0->RDYMODE */
</#if>
<#if CONFIG_DRV_EBI_PAGE_MODE_CSX3 == true>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, true, ${CONFIG_DRV_EBI_PAGE_SIZE_CSX3});
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 0, ${CONFIG_DRV_EBI_MEM_PAGE_MODE_TRC_CSX3}, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX3}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX3}); /* Setup EBISMT0 */
<#else>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, false, PAGE_WORD4);
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 0, 0, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX3}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX3}); /* Setup EBISMT0 */
</#if>
</#if>
<#if CONFIG_DRV_EBI_CS_TIMING_CSX3 == "CS_TIMING_1">
<#if CONFIG_DRV_EBI_USE_RDY_PINS == true>
    PLIB_EBI_ReadyModeSet(EBI_ID_0, true, false, false); /* Setup EBISMT0->RDYMODE */
</#if>
<#if CONFIG_DRV_EBI_PAGE_MODE_CSX3 == true>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, true, ${CONFIG_DRV_EBI_PAGE_SIZE_CSX3}); /* Memory Page Setup */
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 1, ${CONFIG_DRV_EBI_MEM_PAGE_MODE_TRC_CSX3}, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX3}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX3}); /* Setup EBISMT1 */
<#else>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, false, PAGE_WORD4); /* Memory Page Setup */
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 1, 0, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX3}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX3}); /* Setup EBISMT1 */
</#if>
</#if>
<#if CONFIG_DRV_EBI_CS_TIMING_CSX3 == "CS_TIMING_2">
<#if CONFIG_DRV_EBI_USE_RDY_PINS == true>
    PLIB_EBI_ReadyModeSet(EBI_ID_0, false, true, false); /* Setup EBISMT2->RDYMODE */
</#if>
<#if CONFIG_DRV_EBI_PAGE_MODE_CSX3 == true>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, true, ${CONFIG_DRV_EBI_PAGE_SIZE_CSX3}); /* Memory Page Setup */
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 2, ${CONFIG_DRV_EBI_MEM_PAGE_MODE_TRC_CSX3}, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX3}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX3}); /* Setup EBISMT2 */
<#else>
    PLIB_EBI_MemoryPagingSet(EBI_ID_0, 0, false, PAGE_WORD4); /* Memory Page Setup */
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 2, 0, ${CONFIG_DRV_EBI_MEM_DATABUS_TURNAROUND_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_WRITE_PULSE_WIDTH_CSX3}, ${CONFIG_DRV_EBI_MEM_ADDRESS_DATA_HOLD_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_ADDRESS_SETUP_TIME_CSX3}, ${CONFIG_DRV_EBI_MEM_READ_CYCLE_TIME_CSX3}); /* Setup EBISMT2 */
</#if>
</#if>
    PLIB_EBI_StaticMemoryWidthRegisterSet (EBI_ID_0, 0, ${CONFIG_DRV_EBI_STATIC_MEMORY_WIDTH_CSX3}); /* Setup EBISMCON->SMWIDTH0 */
</#if>
}

<#--
/*******************************************************************************
 End of File
*/
-->
