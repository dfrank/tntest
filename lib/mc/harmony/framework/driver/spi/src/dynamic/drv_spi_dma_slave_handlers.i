/*******************************************************************************
  SPI Driver Interface Implementation

  Company:
    Microchip Technology Inc.

  File Name:
   drv_spi_dma_slave_handlers.i

  Summary:
 SPI Driver implementation for the dma call back handler variations.

  Description:
    The SPI Driver provides a interface to access the SPI hardware on the PIC32
    microcontroller.  This file implements the SPI Driver. This file
    should be included in the project if SPI driver functionality is needed.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

#include "drv_spi_internal.h"

#if DRV_SPI_DMA
    #if defined(DRV_SPI_USE_ISR)
        #if defined(DRV_SPI_USE_8BIT)
            void DRV_SPI_ISRDMASlaveSendEventHandler8bit(SYS_DMA_TRANSFER_EVENT event, SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle)
        #elif defined(DRV_SPI_USE_16BIT)
            void DRV_SPI_ISRDMASlaveSendEventHandler16bit(SYS_DMA_TRANSFER_EVENT event, SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle)
        #elif defined(DRV_SPI_USE_32BIT)
            void DRV_SPI_ISRDMASlaveSendEventHandler32bit(SYS_DMA_TRANSFER_EVENT event, SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle)
        #endif
    #elif defined(DRV_SPI_USE_POLLED)
        #if defined(DRV_SPI_USE_8BIT)
            void DRV_SPI_PolledDMASlaveSendEventHandler8bit(SYS_DMA_TRANSFER_EVENT event, SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle)
        #elif defined(DRV_SPI_USE_16BIT)
            void DRV_SPI_PolledDMASlaveSendEventHandler16bit(SYS_DMA_TRANSFER_EVENT event, SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle)
        #elif defined(DRV_SPI_USE_32BIT)
            void DRV_SPI_PolledDMASlaveSendEventHandler32bit(SYS_DMA_TRANSFER_EVENT event, SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle)
        #endif
    #endif
{
    if (event != SYS_DMA_TRANSFER_EVENT_COMPLETE)
    {
        //handle Error
        return;
    }
    struct DRV_SPI_DRIVER_OBJECT * pDrvInstance = (struct DRV_SPI_DRIVER_OBJECT * )contextHandle;
    register DRV_SPI_JOB_OBJECT * currentJob = pDrvInstance->currentJob;
    register SPI_MODULE_ID spiId = pDrvInstance->spiId;

    if (currentJob == NULL)
    {
        return;
    }
    if (pDrvInstance->txDmaThreshold != 0 && currentJob->dataLeftToTx > pDrvInstance->txDmaThreshold)
    {
        // Start a DMA transfer
        size_t txSize = MIN(PLIB_DMA_MAX_TRF_SIZE, currentJob->dataLeftToTx);
        if (txSize != 0)
        {
#if defined(DRV_SPI_USE_8BIT)
            SYS_DMA_ChannelTransferAdd(pDrvInstance->txDmaChannelHandle, currentJob->txBuffer, txSize, PLIB_SPI_BufferAddressGet(spiId), 1, 1);
#elif defined(DRV_SPI_USE_16BIT)
            SYS_DMA_ChannelTransferAdd(pDrvInstance->txDmaChannelHandle, currentJob->txBuffer, txSize, PLIB_SPI_BufferAddressGet(spiId), 2, 2);
#elif defined(DRV_SPI_USE_32BIT)
            SYS_DMA_ChannelTransferAdd(pDrvInstance->txDmaChannelHandle, currentJob->txBuffer, txSize, PLIB_SPI_BufferAddressGet(spiId), 4, 4);
#endif
            currentJob->dataLeftToTx -= txSize;
            currentJob->dataTxed += txSize;
        }
        currentJob->txDMAProgressStage = DRV_SPI_DMA_INPROGRESS;
        SYS_DMA_ChannelEnable(pDrvInstance->txDmaChannelHandle);
        SYS_DMA_ChannelForceStart(pDrvInstance->txDmaChannelHandle);
    }
        else
    {
        currentJob->txDMAProgressStage = DRV_SPI_DMA_COMPLETE;
    }
    if ((currentJob->txDMAProgressStage == DRV_SPI_DMA_COMPLETE) &&
        (currentJob->rxDMAProgressStage != DRV_SPI_DMA_INPROGRESS))
    {
        currentJob->txDMAProgressStage = DRV_SPI_DMA_NONE;
        currentJob->rxDMAProgressStage = DRV_SPI_DMA_NONE;
#if defined(DRV_SPI_USE_ISR)
        SYS_INT_SourceEnable(pDrvInstance->txInterruptSource);
#endif
    }
}

    #if defined(DRV_SPI_USE_ISR)
        #if defined(DRV_SPI_USE_8BIT)
            void DRV_SPI_ISRDMASlaveReceiveEventHandler8bit(SYS_DMA_TRANSFER_EVENT event, SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle)
        #elif defined(DRV_SPI_USE_16BIT)
            void DRV_SPI_ISRDMASlaveReceiveEventHandler16bit(SYS_DMA_TRANSFER_EVENT event, SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle)
        #elif defined(DRV_SPI_USE_32BIT)
            void DRV_SPI_ISRDMASlaveReceiveEventHandler32bit(SYS_DMA_TRANSFER_EVENT event, SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle)
        #endif
    #elif defined(DRV_SPI_USE_POLLED)
        #if defined(DRV_SPI_USE_8BIT)
            void DRV_SPI_PolledDMASlaveReceiveEventHandler8bit(SYS_DMA_TRANSFER_EVENT event, SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle)
        #elif defined(DRV_SPI_USE_16BIT)
            void DRV_SPI_PolledDMASlaveReceiveEventHandler16bit(SYS_DMA_TRANSFER_EVENT event, SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle)
        #elif defined(DRV_SPI_USE_32BIT)
        void DRV_SPI_PolledDMASlaveReceiveEventHandler32bit(SYS_DMA_TRANSFER_EVENT event, SYS_DMA_CHANNEL_HANDLE handle, uintptr_t contextHandle)
        #endif
    #endif
{
    if (event != SYS_DMA_TRANSFER_EVENT_COMPLETE)
    {
        //handle Error
        return;
    }
    struct DRV_SPI_DRIVER_OBJECT * pDrvInstance = (struct DRV_SPI_DRIVER_OBJECT * )contextHandle;
    register DRV_SPI_JOB_OBJECT * currentJob = pDrvInstance->currentJob;
    register SPI_MODULE_ID spiId = pDrvInstance->spiId;
    if (currentJob == NULL)
    {
        return;
    }
    if (pDrvInstance->rxDmaThreshold != 0 && currentJob->dataLeftToRx + currentJob->dummyLeftToRx > pDrvInstance->rxDmaThreshold)
    {
        // Start a DMA transfer
        size_t rxSize = MIN(PLIB_DMA_MAX_TRF_SIZE, currentJob->dataLeftToRx);
        if (rxSize != 0)
        {
#if defined(DRV_SPI_USE_8BIT)
            SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 1, currentJob->rxBuffer, rxSize, 1);
#elif defined(DRV_SPI_USE_16BIT)
            SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 2, currentJob->rxBuffer, rxSize, 2);
#elif defined(DRV_SPI_USE_32BIT)
            SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 4, currentJob->rxBuffer, rxSize, 4);
#endif
            currentJob->dataLeftToRx -= rxSize;
            currentJob->dataRxed += rxSize;
        }
        else
        {
            rxSize = MIN(PLIB_DMA_MAX_TRF_SIZE, currentJob->dummyLeftToRx);
            rxSize = MIN(rxSize, DRV_SPI_DMA_DUMMY_BUFFER_SIZE);
#if defined(DRV_SPI_USE_8BIT)
            SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 1, sSPIRxDummyBuffer, rxSize, 1);
#elif defined(DRV_SPI_USE_16BIT)
            SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 2, sSPIRxDummyBuffer, rxSize, 2);
#elif defined(DRV_SPI_USE_32BIT)
            SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 4, sSPIRxDummyBuffer, rxSize, 4);
#endif
            currentJob->dummyLeftToTx -= rxSize;
        }
        currentJob->rxDMAProgressStage = DRV_SPI_DMA_INPROGRESS;
        SYS_DMA_ChannelEnable(pDrvInstance->rxDmaChannelHandle);
        SYS_DMA_ChannelForceStart(pDrvInstance->rxDmaChannelHandle);
    }
    else
    {
        currentJob->rxDMAProgressStage = DRV_SPI_DMA_COMPLETE;
    }
    if ((currentJob->rxDMAProgressStage == DRV_SPI_DMA_COMPLETE) &&
        (currentJob->txDMAProgressStage != DRV_SPI_DMA_INPROGRESS))
    {
        currentJob->txDMAProgressStage = DRV_SPI_DMA_NONE;
        currentJob->rxDMAProgressStage = DRV_SPI_DMA_NONE;
#if defined(DRV_SPI_USE_ISR)
        SYS_INT_SourceEnable(pDrvInstance->txInterruptSource);
#endif
    }

}
#endif