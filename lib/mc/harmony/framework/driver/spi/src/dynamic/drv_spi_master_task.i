/*******************************************************************************
  SPI Driver Interface Implementation

  Company:
    Microchip Technology Inc.

  File Name:
   drv_spi_master_tasks.i

  Summary:
 SPI Driver implementation for the master task functions.

  Description:
    The SPI Driver provides a interface to access the SPI hardware on the PIC32
    microcontroller.  This file implements the SPI Driver. This file
    should be included in the project if SPI driver functionality is needed.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

#include "drv_spi_internal.h"
#if defined(DRV_SPI_USE_EBM)
    #if defined(DRV_SPI_USE_ISR)
    int32_t DRV_SPI_ISRMasterEBMTasks ( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_POLLED)
    int32_t DRV_SPI_PolledMasterEBMTasks ( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #endif
#elif defined(DRV_SPI_USE_RM)
    #if defined(DRV_SPI_USE_ISR)
    int32_t DRV_SPI_ISRMasterRMTasks ( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_POLLED)
    int32_t DRV_SPI_PolledMasterRMTasks ( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #endif
#endif
{
    bool continueLoop;
    int32_t result;
#if defined (DRV_SPI_USE_POLLED) && defined (DRV_SPI_USE_RM)
    uint8_t counter = 0;
    uint8_t numPolled = pDrvInstance->numTrfsSmPolled;
#endif
#if defined(DRV_SPI_USE_ISR)
    SYS_INT_SourceDisable(pDrvInstance->rxInterruptSource);
    SYS_INT_SourceDisable(pDrvInstance->txInterruptSource);
#endif
    do {
        continueLoop = false;
        result = (*pDrvInstance->vfReceiveTask)(pDrvInstance);
        result += (*pDrvInstance->vfSendTask)(pDrvInstance);
        result += (*pDrvInstance->vfReceiveTask)(pDrvInstance);
        (*pDrvInstance->vfErrorTask)(pDrvInstance);

#if defined(DRV_SPI_USE_ISR)

        if (pDrvInstance->currentJob != NULL)
        {
            register SPI_MODULE_ID spiId = pDrvInstance->spiId;
            register DRV_SPI_JOB_OBJECT * currentJob = pDrvInstance->currentJob;

#if defined(DRV_SPI_USE_EBM)
            uint8_t txBuff = PLIB_SPI_FIFOCountGet(spiId, SPI_FIFO_TYPE_TRANSMIT);
            uint8_t rxBuff = PLIB_SPI_FIFOCountGet(spiId, SPI_FIFO_TYPE_RECEIVE);
#elif defined(DRV_SPI_USE_RM)
            uint8_t txBuff = PLIB_SPI_TransmitBufferIsEmpty(spiId) ? 1 : 0;
            uint8_t rxBuff = PLIB_SPI_ReceiverBufferIsFull(spiId) ? 1 : 0;
#endif
            size_t txLeft = currentJob->dataLeftToTx + currentJob->dummyLeftToTx;
            size_t rxLeft = currentJob->dataLeftToRx + currentJob->dummyLeftToRx;
            switch (pDrvInstance->commWidth)
            {
            case SPI_COMMUNICATION_WIDTH_8BITS:
                txBuff = PLIB_SPI_TX_8BIT_FIFO_SIZE(pDrvInstance->spiId) - txBuff;
                break;
            case SPI_COMMUNICATION_WIDTH_16BITS:
                txBuff = PLIB_SPI_TX_8BIT_FIFO_SIZE(pDrvInstance->spiId) - (txBuff<<1);
                rxBuff <<=1;
                break;
            case SPI_COMMUNICATION_WIDTH_32BITS:
                txBuff = PLIB_SPI_TX_8BIT_FIFO_SIZE(pDrvInstance->spiId) - (txBuff<<2);
                rxBuff <<=2;
                break;
            }

            if (((txLeft != 0) && (txBuff > PLIB_SPI_TX_8BIT_LW_MARK(pDrvInstance->spiId))) ||
                ((rxLeft != 0) && (rxBuff > PLIB_SPI_RX_8BIT_HW_MARK(pDrvInstance->spiId))))
            {
                //SYS_CONSOLE_MESSAGE("R");
                continueLoop = true;
                continue;
            }
            //SYS_CONSOLE_PRINT("I %d %d %d %d\r\n", txLeft, txBuff, rxLeft, rxBuff);
            SYS_INT_SourceStatusClear(pDrvInstance->rxInterruptSource);
            SYS_INT_SourceStatusClear(pDrvInstance->txInterruptSource);
            if (pDrvInstance->rxEnabled)
            {
                SYS_INT_SourceEnable(pDrvInstance->rxInterruptSource);
            }
            if (pDrvInstance->txEnabled)
            {
                SYS_INT_SourceEnable(pDrvInstance->txInterruptSource);
            }
            return 0;
        }
        if (!DRV_SPI_SYS_QUEUE_IsEmpty(pDrvInstance->queue))
        {
            //SYS_CONSOLE_MESSAGE("J");
            //SYS_INT_SourceEnable(pDrvInstance->txInterruptSource);
            pDrvInstance->txInterruptSource = true;
            continueLoop = true;
            continue;
        }
#elif defined(DRV_SPI_USE_RM)
        counter ++;
        if ((counter < numPolled) && (result != 0))
        {
            continueLoop = true;
        }
#endif

    } while(continueLoop);
    //SYS_CONSOLE_MESSAGE("N");
#if defined(DRV_SPI_USE_ISR)
    SYS_INT_SourceStatusClear(pDrvInstance->rxInterruptSource);
    SYS_INT_SourceStatusClear(pDrvInstance->txInterruptSource);
#endif
    return 0;
}