/*******************************************************************************
  SPI Driver Interface Implementation

  Company:
    Microchip Technology Inc.

  File Name:
   drv_spi_slave_ebm_tasks.i

  Summary:
 SPI Driver implementation for the enhanced buffer mode slave functions.

  Description:
    The SPI Driver provides a interface to access the SPI hardware on the PIC32
    microcontroller.  This file implements the SPI Driver. This file
    should be included in the project if SPI driver functionality is needed.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

#include "drv_spi_internal.h"

#if defined(DRV_SPI_USE_ISR)
    #if defined(DRV_SPI_USE_8BIT)
        int32_t DRV_SPI_SlaveEBMSend8BitISR( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_16BIT)
        int32_t DRV_SPI_SlaveEBMSend16BitISR( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_32BIT)
        int32_t DRV_SPI_SlaveEBMSend32BitISR( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #endif
#elif defined(DRV_SPI_USE_POLLED)
    #if defined(DRV_SPI_USE_8BIT)
        int32_t DRV_SPI_SlaveEBMSend8BitPolled( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_16BIT)
        int32_t DRV_SPI_SlaveEBMSend16BitPolled( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_32BIT)
        int32_t DRV_SPI_SlaveEBMSend32BitPolled( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #endif
#endif
{
    register SPI_MODULE_ID spiId = pDrvInstance->spiId;
    register DRV_SPI_JOB_OBJECT * currentJob = pDrvInstance->currentJob;
    if (currentJob == NULL)
    {
        return 0;
    }
#if DRV_SPI_DMA
    if (currentJob->txDMAProgressStage != DRV_SPI_DMA_NONE)
    {
#if defined(DRV_SPI_USE_ISR)
        SYS_INT_SourceStatusClear(pDrvInstance->txInterruptSource);
#endif
        return 0;
    }
#endif

    if (currentJob->dataLeftToTx == 0)
    {
        if (!PLIB_SPI_TransmitBufferIsEmpty(spiId))
        {
            return 0;
        }
#if defined(DRV_SPI_USE_ISR)
        SYS_INT_SourceDisable(pDrvInstance->txInterruptSource);
#endif
        if (currentJob->dataLeftToRx != 0 || currentJob->dummyLeftToRx != 0)
        {
            return 0;
        }
        currentJob->status = DRV_SPI_BUFFER_EVENT_COMPLETE;
        if (currentJob->completeCB != NULL)
        {
            (*currentJob->completeCB)(DRV_SPI_BUFFER_EVENT_COMPLETE, (DRV_SPI_BUFFER_HANDLE)currentJob, currentJob->context);
        }
        if (pDrvInstance->operationEnded != NULL)
        {
            (*pDrvInstance->operationEnded)(DRV_SPI_BUFFER_EVENT_COMPLETE, (DRV_SPI_BUFFER_HANDLE)currentJob, currentJob->context);
        }
#if defined(DRV_SPI_USE_ISR)
        if (DRV_SPI_SYS_QUEUE_FreeElement(pDrvInstance->queue, currentJob) != DRV_SPI_SYS_QUEUE_SUCCESS)
#elif defined(DRV_SPI_USE_POLLED)
        if (DRV_SPI_SYS_QUEUE_FreeElementLock(pDrvInstance->queue, currentJob) != DRV_SPI_SYS_QUEUE_SUCCESS)
#endif
        {
            SYS_ASSERT(false, "SPI Driver: Queue free element error");
            return 0;
        }
        pDrvInstance->currentJob = NULL;
        return 0;
    }

#if defined(DRV_SPI_USE_8BIT)
    uint8_t bufferBytes = PLIB_SPI_TX_8BIT_FIFO_SIZE(spiId) - PLIB_SPI_FIFOCountGet(spiId, SPI_FIFO_TYPE_TRANSMIT);
#elif defined(DRV_SPI_USE_16BIT)
    uint8_t bufferBytes = PLIB_SPI_TX_8BIT_FIFO_SIZE(spiId) - (PLIB_SPI_FIFOCountGet(spiId, SPI_FIFO_TYPE_TRANSMIT) << 1);
#elif defined(DRV_SPI_USE_32BIT)
    uint8_t bufferBytes = PLIB_SPI_TX_8BIT_FIFO_SIZE(spiId) - (PLIB_SPI_FIFOCountGet(spiId, SPI_FIFO_TYPE_TRANSMIT) << 2);
#endif
    size_t dataUnits = MIN(currentJob->dataLeftToTx, bufferBytes);
    currentJob->dataLeftToTx -= dataUnits;


#if defined(DRV_SPI_USE_ISR)
    if (currentJob->dataLeftToTx < bufferBytes)
    {
        PLIB_SPI_FIFOInterruptModeSelect(spiId, SPI_FIFO_INTERRUPT_WHEN_TRANSMIT_BUFFER_IS_1HALF_EMPTY_OR_MORE);
        SYS_INT_SourceDisable(pDrvInstance->txInterruptSource);
        SYS_INT_SourceEnable(pDrvInstance->rxInterruptSource);
    }
#endif

#if defined(DRV_SPI_USE_8BIT)
    uint8_t *bufferLoc = &(currentJob->txBuffer[currentJob->dataTxed]);
#elif defined(DRV_SPI_USE_16BIT)
    uint16_t *bufferLoc = (uint16_t*)&(currentJob->txBuffer[currentJob->dataTxed]);
    dataUnits >>=1;
#elif defined(DRV_SPI_USE_32BIT)
    uint32_t *bufferLoc = (uint32_t*)&(currentJob->txBuffer[currentJob->dataTxed]);
    dataUnits >>=2;
#endif
    size_t counter;
    for (counter = 0; counter < dataUnits; counter++)
    {
#if defined(DRV_SPI_USE_8BIT)
        PLIB_SPI_BufferWrite(spiId, bufferLoc[counter]);
#elif defined(DRV_SPI_USE_16BIT)
        PLIB_SPI_BufferWrite16bit(spiId, bufferLoc[counter]);
#elif defined(DRV_SPI_USE_32BIT)
        PLIB_SPI_BufferWrite32bit(spiId, bufferLoc[counter]);
#endif
    }

#if defined(DRV_SPI_USE_8BIT)
    currentJob->dataTxed += dataUnits;
#elif defined(DRV_SPI_USE_16BIT)
    currentJob->dataTxed += dataUnits<<1;
#elif defined(DRV_SPI_USE_32BIT)
    currentJob->dataTxed += dataUnits<<2;
#endif
    
#if defined(DRV_SPI_USE_ISR)
    SYS_INT_SourceStatusClear(pDrvInstance->txInterruptSource);
#endif
    return 0;
}


#if defined(DRV_SPI_USE_ISR)
    #if defined(DRV_SPI_USE_8BIT)
        int32_t DRV_SPI_SlaveEBMReceive8BitISR( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_16BIT)
        int32_t DRV_SPI_SlaveEBMReceive16BitISR( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_32BIT)
        int32_t DRV_SPI_SlaveEBMReceive32BitISR( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #endif
#elif defined(DRV_SPI_USE_POLLED)
    #if defined(DRV_SPI_USE_8BIT)
        int32_t DRV_SPI_SlaveEBMReceive8BitPolled( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_16BIT)
        int32_t DRV_SPI_SlaveEBMReceive16BitPolled( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_32BIT)
        int32_t DRV_SPI_SlaveEBMReceive32BitPolled( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #endif
#endif
{
    register SPI_MODULE_ID spiId = pDrvInstance->spiId;
    register DRV_SPI_JOB_OBJECT * currentJob = pDrvInstance->currentJob;
    if (pDrvInstance->currentJob == NULL)
    {
#if defined(DRV_SPI_USE_ISR)
        if (DRV_SPI_SYS_QUEUE_Dequeue(pDrvInstance->queue, (void *)&(pDrvInstance->currentJob)) != DRV_SPI_SYS_QUEUE_SUCCESS)
#elif defined(DRV_SPI_USE_POLLED)
        if (DRV_SPI_SYS_QUEUE_DequeueLock(pDrvInstance->queue, (void *)&(pDrvInstance->currentJob)) != DRV_SPI_SYS_QUEUE_SUCCESS)
#endif
        {
            SYS_ASSERT(false, "SPI Driver: Error in dequeing");
            return 0;
        }
        if (pDrvInstance->currentJob == NULL)
        {
#if defined(DRV_SPI_USE_ISR)
            SYS_INT_SourceDisable(pDrvInstance->txInterruptSource);
            SYS_INT_SourceStatusClear(pDrvInstance->txInterruptSource);
#endif
            return 0;
        }
#if defined(DRV_SPI_USE_ISR)
        SYS_INT_SourceEnable(pDrvInstance->rxInterruptSource);
#endif
        currentJob = pDrvInstance->currentJob;
        if (pDrvInstance->operationStarting != NULL)
        {
            (*pDrvInstance->operationStarting)(DRV_SPI_BUFFER_EVENT_PROCESSING, (DRV_SPI_BUFFER_HANDLE)currentJob, pDrvInstance->currentJob->context);
        }
        pDrvInstance->currentJob->status = DRV_SPI_BUFFER_EVENT_PROCESSING;
#if defined(DRV_SPI_USE_ISR)
        if (currentJob->dataLeftToRx + currentJob->dummyLeftToRx > PLIB_SPI_RX_8BIT_HW_MARK(spiId))
        {
            PLIB_SPI_FIFOInterruptModeSelect(spiId, SPI_FIFO_INTERRUPT_WHEN_TRANSMIT_BUFFER_IS_1HALF_EMPTY_OR_MORE);
            PLIB_SPI_FIFOInterruptModeSelect(spiId, SPI_FIFO_INTERRUPT_WHEN_RECEIVE_BUFFER_IS_1HALF_FULL_OR_MORE);
        }
#endif
#if DRV_SPI_DMA
        if (pDrvInstance->txDmaThreshold != 0 && currentJob->dataLeftToTx > pDrvInstance->txDmaThreshold)
        {
            // Start a DMA transfer
            size_t txSize = MIN(PLIB_DMA_MAX_TRF_SIZE, currentJob->dataLeftToTx);
            if (txSize != 0)
            {
    #if defined(DRV_SPI_USE_8BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->txDmaChannelHandle, currentJob->txBuffer, txSize, PLIB_SPI_BufferAddressGet(spiId), 1, 1);
    #elif defined(DRV_SPI_USE_16BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->txDmaChannelHandle, currentJob->txBuffer, txSize, PLIB_SPI_BufferAddressGet(spiId), 2, 2);
    #elif defined(DRV_SPI_USE_32BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->txDmaChannelHandle, currentJob->txBuffer, txSize, PLIB_SPI_BufferAddressGet(spiId), 4, 4);
    #endif
                currentJob->dataLeftToTx -= txSize;
                currentJob->dataTxed += txSize;
            }
            currentJob->txDMAProgressStage = DRV_SPI_DMA_INPROGRESS;
            SYS_DMA_ChannelEnable(pDrvInstance->txDmaChannelHandle);

            SYS_DMA_ChannelForceStart(pDrvInstance->txDmaChannelHandle);
#if defined(DRV_SPI_USE_ISR)
            SYS_INT_SourceDisable(pDrvInstance->txInterruptSource);
            PLIB_SPI_FIFOInterruptModeSelect(spiId, SPI_FIFO_INTERRUPT_WHEN_TRANSMIT_BUFFER_IS_COMPLETELY_EMPTY);
            SYS_INT_SourceEnable(pDrvInstance->rxInterruptSource);
#endif
        }
        if (pDrvInstance->rxDmaThreshold != 0 && currentJob->dataLeftToRx + currentJob->dummyLeftToRx > pDrvInstance->rxDmaThreshold)
        {
            // Start a DMA transfer
            size_t rxSize = MIN(PLIB_DMA_MAX_TRF_SIZE, currentJob->dataLeftToRx);
            if (rxSize != 0)
            {
    #if defined(DRV_SPI_USE_8BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 1, currentJob->rxBuffer, rxSize, 1);
    #elif defined(DRV_SPI_USE_16BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 2, currentJob->rxBuffer, rxSize, 2);
    #elif defined(DRV_SPI_USE_32BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 4, currentJob->rxBuffer, rxSize, 4);
    #endif
                currentJob->dataLeftToRx -= rxSize;
                currentJob->dataRxed += rxSize;
            }
            else
            {
                rxSize = MIN(PLIB_DMA_MAX_TRF_SIZE, currentJob->dummyLeftToRx);
                rxSize = MIN(rxSize, DRV_SPI_DMA_DUMMY_BUFFER_SIZE);
    #if defined(DRV_SPI_USE_8BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 1, sSPIRxDummyBuffer, rxSize, 1);
    #elif defined(DRV_SPI_USE_16BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 2, sSPIRxDummyBuffer, rxSize, 2);
    #elif defined(DRV_SPI_USE_32BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 4, sSPIRxDummyBuffer, rxSize, 4);
    #endif
                currentJob->dummyLeftToTx -= rxSize;
            }
            currentJob->rxDMAProgressStage = DRV_SPI_DMA_INPROGRESS;
            SYS_DMA_ChannelEnable(pDrvInstance->rxDmaChannelHandle);
            SYS_DMA_ChannelForceStart(pDrvInstance->rxDmaChannelHandle);
#if defined(DRV_SPI_USE_ISR)
            PLIB_SPI_FIFOInterruptModeSelect(spiId, SPI_FIFO_INTERRUPT_WHEN_RECEIVE_BUFFER_IS_NOT_EMPTY);
            SYS_INT_SourceDisable(pDrvInstance->rxInterruptSource);
#endif
        }
#endif
        if (currentJob->dataLeftToTx != 0)
        {
            return 0;//  This will only affect new tasks, we want to get to transmit asap on new tasks
        }
    }
#if DRV_SPI_DMA
    if (currentJob->rxDMAProgressStage != DRV_SPI_DMA_NONE)
    {
#if defined(DRV_SPI_USE_ISR)
        SYS_INT_SourceStatusClear(pDrvInstance->rxInterruptSource);
#endif
        return 0;
    }
#endif
    if (currentJob->dataLeftToRx + currentJob->dummyLeftToRx == 0)
    {
        return 0;
    }

#if defined(DRV_SPI_USE_8BIT)
    uint8_t bufferBytes = PLIB_SPI_FIFOCountGet(spiId, SPI_FIFO_TYPE_RECEIVE);
#elif defined(DRV_SPI_USE_16BIT)
    uint8_t bufferBytes = PLIB_SPI_FIFOCountGet(spiId, SPI_FIFO_TYPE_RECEIVE) << 1;
#elif defined(DRV_SPI_USE_32BIT)
    uint8_t bufferBytes = PLIB_SPI_FIFOCountGet(spiId, SPI_FIFO_TYPE_RECEIVE) << 2;
#endif
    size_t dataUnits = MIN(currentJob->dataLeftToRx, bufferBytes);
    bufferBytes -= dataUnits;
    currentJob->dataLeftToRx -= dataUnits;
    size_t dummyUnits = MIN(currentJob->dummyLeftToRx, bufferBytes);
    currentJob->dummyLeftToRx -= dummyUnits;

#if defined(DRV_SPI_USE_8BIT)
    uint8_t *bufferLoc = &(currentJob->rxBuffer[currentJob->dataRxed]);
#elif defined(DRV_SPI_USE_16BIT)
    uint16_t *bufferLoc = (uint16_t*)&(currentJob->rxBuffer[currentJob->dataRxed]);
    dataUnits >>=1;
#elif defined(DRV_SPI_USE_32BIT)
    uint32_t *bufferLoc = (uint32_t*)&(currentJob->rxBuffer[currentJob->dataRxed]);
    dataUnits >>=2;
#endif
    size_t counter;
    for (counter = 0; counter < dataUnits; counter++)
    {
#if defined(DRV_SPI_USE_8BIT)
        bufferLoc[counter] = PLIB_SPI_BufferRead(spiId);
#elif defined(DRV_SPI_USE_16BIT)
        bufferLoc[counter] = PLIB_SPI_BufferRead16bit(spiId);
#elif defined(DRV_SPI_USE_32BIT)
        bufferLoc[counter] = PLIB_SPI_BufferRead32bit(spiId);
#endif
    }

    for (counter = 0; counter < dummyUnits; counter++)
    {
#if defined(DRV_SPI_USE_8BIT)
        PLIB_SPI_BufferRead(spiId);
#elif defined(DRV_SPI_USE_16BIT)
        PLIB_SPI_BufferRead16bit(spiId);
#elif defined(DRV_SPI_USE_32BIT)
        PLIB_SPI_BufferRead32bit(spiId);
#endif
    }
#if defined(DRV_SPI_USE_8BIT)
    currentJob->dataRxed += dataUnits;
#elif defined(DRV_SPI_USE_16BIT)
    currentJob->dataRxed += dataUnits<<1;
#elif defined(DRV_SPI_USE_32BIT)
    currentJob->dataRxed += dataUnits<<2;
#endif

#if defined(DRV_SPI_USE_ISR)
    if (currentJob->dataLeftToRx + currentJob->dummyLeftToRx < PLIB_SPI_RX_8BIT_HW_MARK(spiId))
    {
        PLIB_SPI_FIFOInterruptModeSelect(spiId, SPI_FIFO_INTERRUPT_WHEN_RECEIVE_BUFFER_IS_NOT_EMPTY);
    }
    if (currentJob->dataLeftToRx + currentJob->dummyLeftToRx == 0)
    {
        SYS_INT_SourceDisable(pDrvInstance->rxInterruptSource);
        SYS_INT_SourceEnable(pDrvInstance->txInterruptSource);
    }
    SYS_INT_SourceStatusClear(pDrvInstance->rxInterruptSource);
#endif
    return 0;

}