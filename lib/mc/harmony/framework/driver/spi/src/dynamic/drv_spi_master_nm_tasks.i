/*******************************************************************************
  SPI Driver Interface Implementation

  Company:
    Microchip Technology Inc.

  File Name:
   drv_spi_master_nm_tasks.i

  Summary:
 SPI Driver implementation for the normal buffer mode master functions.

  Description:
    The SPI Driver provides a interface to access the SPI hardware on the PIC32
    microcontroller.  This file implements the SPI Driver. This file
    should be included in the project if SPI driver functionality is needed.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

#include "../drv_spi_sys_queue.h"

#if defined(DRV_SPI_USE_ISR)


#include "drv_spi_internal.h"
    #if defined(DRV_SPI_USE_8BIT)
        int32_t DRV_SPI_MasterRMSend8BitISR( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_16BIT)
        int32_t DRV_SPI_MasterRMSend16BitISR( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_32BIT)
        int32_t DRV_SPI_MasterRMSend32BitISR( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #endif
#elif defined(DRV_SPI_USE_POLLED)
    #if defined(DRV_SPI_USE_8BIT)
        int32_t DRV_SPI_MasterRMSend8BitPolled( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_16BIT)
        int32_t DRV_SPI_MasterRMSend16BitPolled( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_32BIT)
        int32_t DRV_SPI_MasterRMSend32BitPolled( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #endif
#endif
{
    register SPI_MODULE_ID spiId = pDrvInstance->spiId;
    register DRV_SPI_JOB_OBJECT * currentJob = pDrvInstance->currentJob;
    if (pDrvInstance->currentJob == NULL)
    {
#if defined(DRV_SPI_USE_ISR)
        if (DRV_SPI_SYS_QUEUE_Dequeue(pDrvInstance->queue, (void *)&(pDrvInstance->currentJob)) != DRV_SPI_SYS_QUEUE_SUCCESS)
#elif defined(DRV_SPI_USE_POLLED)
        if (DRV_SPI_SYS_QUEUE_DequeueLock(pDrvInstance->queue, (void *)&(pDrvInstance->currentJob)) != DRV_SPI_SYS_QUEUE_SUCCESS)
#endif
        {
            SYS_ASSERT(false, "SPI Driver: Error in dequeing");
            return 0;
        }
        if (pDrvInstance->currentJob == NULL)
        {
#if defined(DRV_SPI_USE_ISR)
            SYS_INT_SourceDisable(pDrvInstance->txInterruptSource);
            SYS_INT_SourceStatusClear(pDrvInstance->txInterruptSource);
#endif
            return 0;
        }
        currentJob = pDrvInstance->currentJob;
        if (pDrvInstance->operationStarting != NULL)
        {
            (*pDrvInstance->operationStarting)(DRV_SPI_BUFFER_EVENT_PROCESSING, (DRV_SPI_BUFFER_HANDLE)currentJob, pDrvInstance->currentJob->context);
        }
        pDrvInstance->currentJob->status = DRV_SPI_BUFFER_EVENT_PROCESSING;

#if DRV_SPI_DMA
        if (pDrvInstance->txDmaThreshold != 0 && currentJob->dataLeftToTx + currentJob->dummyLeftToTx > pDrvInstance->txDmaThreshold)
        {
            // Start a DMA transfer
            size_t txSize = MIN(PLIB_DMA_MAX_TRF_SIZE, currentJob->dataLeftToTx);
            if (txSize != 0)
            {
    #if defined(DRV_SPI_USE_8BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->txDmaChannelHandle, currentJob->txBuffer, txSize, PLIB_SPI_BufferAddressGet(spiId), 1, 1);
    #elif defined(DRV_SPI_USE_16BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->txDmaChannelHandle, currentJob->txBuffer, txSize, PLIB_SPI_BufferAddressGet(spiId), 2, 2);
    #elif defined(DRV_SPI_USE_32BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->txDmaChannelHandle, currentJob->txBuffer, txSize, PLIB_SPI_BufferAddressGet(spiId), 4, 4);
    #endif
                currentJob->dataLeftToTx -= txSize;
                currentJob->dataTxed += txSize;
            }
            else
            {
                txSize = MIN(PLIB_DMA_MAX_TRF_SIZE, currentJob->dummyLeftToTx);
                txSize = MIN(txSize, DRV_SPI_DMA_DUMMY_BUFFER_SIZE);
    #if defined(DRV_SPI_USE_8BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->txDmaChannelHandle, sSPITxDummyBuffer, txSize, PLIB_SPI_BufferAddressGet(spiId), 1, 1);
    #elif defined(DRV_SPI_USE_16BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->txDmaChannelHandle, sSPITxDummyBuffer, txSize, PLIB_SPI_BufferAddressGet(spiId), 2, 2);
    #elif defined(DRV_SPI_USE_32BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->txDmaChannelHandle, sSPITxDummyBuffer, txSize, PLIB_SPI_BufferAddressGet(spiId), 4, 4);
    #endif
                currentJob->dummyLeftToTx -= txSize;
            }
            currentJob->txDMAProgressStage = DRV_SPI_DMA_INPROGRESS;
            SYS_DMA_ChannelEnable(pDrvInstance->txDmaChannelHandle);
            SYS_DMA_ChannelForceStart(pDrvInstance->txDmaChannelHandle);
#if defined(DRV_SPI_USE_ISR)
            SYS_INT_SourceDisable(pDrvInstance->txInterruptSource);
            SYS_INT_SourceEnable(pDrvInstance->rxInterruptSource);
#endif
        }
        if (pDrvInstance->rxDmaThreshold != 0 && currentJob->dataLeftToRx + currentJob->dummyLeftToRx > pDrvInstance->rxDmaThreshold)
        {
            // Start a DMA transfer
            size_t rxSize = MIN(PLIB_DMA_MAX_TRF_SIZE, currentJob->dataLeftToRx);
            if (rxSize != 0)
            {
    #if defined(DRV_SPI_USE_8BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 1, currentJob->rxBuffer, rxSize, 1);
    #elif defined(DRV_SPI_USE_16BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 2, currentJob->rxBuffer, rxSize, 2);
    #elif defined(DRV_SPI_USE_32BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 4, currentJob->rxBuffer, rxSize, 4);
    #endif
                currentJob->dataLeftToRx -= rxSize;
                currentJob->dataRxed += rxSize;
            }
            else
            {
                rxSize = MIN(PLIB_DMA_MAX_TRF_SIZE, currentJob->dummyLeftToRx);
                rxSize = MIN(rxSize, DRV_SPI_DMA_DUMMY_BUFFER_SIZE);
    #if defined(DRV_SPI_USE_8BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 1, sSPIRxDummyBuffer, rxSize, 1);
    #elif defined(DRV_SPI_USE_16BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 2, sSPIRxDummyBuffer, rxSize, 2);
    #elif defined(DRV_SPI_USE_32BIT)
                SYS_DMA_ChannelTransferAdd(pDrvInstance->rxDmaChannelHandle, PLIB_SPI_BufferAddressGet(spiId), 4, sSPIRxDummyBuffer, rxSize, 4);
    #endif
                currentJob->dummyLeftToTx -= rxSize;
            }
            currentJob->rxDMAProgressStage = DRV_SPI_DMA_INPROGRESS;
            SYS_DMA_ChannelEnable(pDrvInstance->rxDmaChannelHandle);
            SYS_DMA_ChannelForceStart(pDrvInstance->rxDmaChannelHandle);
#if defined(DRV_SPI_USE_ISR)
            SYS_INT_SourceDisable(pDrvInstance->rxInterruptSource);
#endif
        }

#endif
    }
#if DRV_SPI_DMA
    if (currentJob->txDMAProgressStage != DRV_SPI_DMA_NONE)
    {
#if defined(DRV_SPI_USE_ISR)
        SYS_INT_SourceStatusClear(pDrvInstance->txInterruptSource);
#endif
        return 0;
    }
#endif
    if (currentJob->dataLeftToTx + currentJob->dummyLeftToTx == 0)
    {
        return 0;
    }
    if (!PLIB_SPI_TransmitBufferIsEmpty(spiId))
    {
        return 0;
    }


    if (currentJob->dataLeftToTx != 0)
    {
#if defined(DRV_SPI_USE_8BIT)
        //SYS_CONSOLE_PRINT("T%02x ", currentJob->txBuffer[currentJob->dataTxed]);
        PLIB_SPI_BufferWrite(spiId, currentJob->txBuffer[currentJob->dataTxed]);
        currentJob->dataTxed++;
        currentJob->dataLeftToTx--;
#elif defined(DRV_SPI_USE_16BIT)
        PLIB_SPI_BufferWrite16bit(spiId, ((uint16_t*)currentJob->txBuffer)[currentJob->dataTxed>>1]);
        currentJob->dataTxed+=2;
        currentJob->dataLeftToTx-=2;
#elif defined(DRV_SPI_USE_32BIT)
        PLIB_SPI_BufferWrite32bit(spiId, ((uint32_t*)currentJob->txBuffer)[currentJob->dataTxed>>2]);
        currentJob->dataTxed+=4;
        currentJob->dataLeftToTx-=4;
#endif
    }
    else
    {
#if defined(DRV_SPI_USE_8BIT)
        //SYS_CONSOLE_MESSAGE("Td ");
        PLIB_SPI_BufferWrite(spiId, 0xff);
        currentJob->dummyLeftToTx--;
#elif defined(DRV_SPI_USE_16BIT)
        PLIB_SPI_BufferWrite16bit(spiId, 0xffff);
        currentJob->dummyLeftToTx-=2;
#elif defined(DRV_SPI_USE_32BIT)
        PLIB_SPI_BufferWrite32bit(spiId, 0xffffffff);
        currentJob->dummyLeftToTx-=4;
#endif
    }
#if defined(DRV_SPI_USE_ISR)
    if (currentJob->dataLeftToTx + currentJob->dummyLeftToTx == 0)
    {
        SYS_INT_SourceDisable(pDrvInstance->txInterruptSource);
        SYS_INT_SourceEnable(pDrvInstance->rxInterruptSource);
    }
    SYS_INT_SourceStatusClear(pDrvInstance->txInterruptSource);
#endif
    return 1;
}



#if defined(DRV_SPI_USE_ISR)
    #if defined(DRV_SPI_USE_8BIT)
        int32_t DRV_SPI_MasterRMReceive8BitISR( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_16BIT)
        int32_t DRV_SPI_MasterRMReceive16BitISR( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_32BIT)
        int32_t DRV_SPI_MasterRMReceive32BitISR( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #endif
#elif defined(DRV_SPI_USE_POLLED)
    #if defined(DRV_SPI_USE_8BIT)
        int32_t DRV_SPI_MasterRMReceive8BitPolled( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_16BIT)
        int32_t DRV_SPI_MasterRMReceive16BitPolled( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_USE_32BIT)
        int32_t DRV_SPI_MasterRMReceive32BitPolled( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #endif
#endif
{
    register SPI_MODULE_ID spiId = pDrvInstance->spiId;
    register DRV_SPI_JOB_OBJECT * currentJob = pDrvInstance->currentJob;

    if (currentJob == NULL)
    {
        return 0;
    }

    if (!PLIB_SPI_ReceiverBufferIsFull(spiId))
    {
        return 0;
    }
#if DRV_SPI_DMA
    if (currentJob->rxDMAProgressStage != DRV_SPI_DMA_NONE)
    {
#if defined(DRV_SPI_USE_ISR)
        SYS_INT_SourceStatusClear(pDrvInstance->rxInterruptSource);
#endif
        return 0;
    }
#endif

    if (currentJob->dataLeftToRx != 0)
    {
#if defined(DRV_SPI_USE_8BIT)
        currentJob->rxBuffer[currentJob->dataRxed] = PLIB_SPI_BufferRead(spiId);
        //SYS_CONSOLE_PRINT("R%02x ", currentJob->rxBuffer[currentJob->dataRxed]);
        currentJob->dataRxed++;
        currentJob->dataLeftToRx --;
#elif defined(DRV_SPI_USE_16BIT)
        ((uint16_t*)(currentJob->rxBuffer))[currentJob->dataRxed>>1] = PLIB_SPI_BufferRead16bit(spiId);
        currentJob->dataRxed+=2;
        currentJob->dataLeftToRx -=2;
#elif defined(DRV_SPI_USE_32BIT)
        ((uint32_t*)(currentJob->rxBuffer))[currentJob->dataRxed>>2] = PLIB_SPI_BufferRead32bit(spiId);
        currentJob->dataRxed+=2;
        currentJob->dataLeftToRx -=4;
#endif
    }
    else
    {
#if defined(DRV_SPI_USE_8BIT)
        PLIB_SPI_BufferRead(spiId);
        //SYS_CONSOLE_MESSAGE("Rd ");
        currentJob->dummyLeftToRx--;
#elif defined(DRV_SPI_USE_16BIT)
        PLIB_SPI_BufferRead16bit(spiId);
        currentJob->dummyLeftToRx-=2;
#elif defined(DRV_SPI_USE_32BIT)
        PLIB_SPI_BufferRead32bit(spiId);
        currentJob->dummyLeftToRx-=4;
#endif
    }
    
    if (currentJob->dataLeftToRx + currentJob->dummyLeftToRx == 0)
    {
#if defined(DRV_SPI_USE_ISR)
        SYS_INT_SourceDisable(pDrvInstance->rxInterruptSource);
#endif
        currentJob->status = DRV_SPI_BUFFER_EVENT_COMPLETE;
        //SYS_CONSOLE_MESSAGE("c\r\n");

        if (currentJob->completeCB != NULL)
        {
            (*currentJob->completeCB)(DRV_SPI_BUFFER_EVENT_COMPLETE, (DRV_SPI_BUFFER_HANDLE)currentJob, currentJob->context);
        }
        if (pDrvInstance->operationEnded != NULL)
        {
            (*pDrvInstance->operationEnded)(DRV_SPI_BUFFER_EVENT_COMPLETE, (DRV_SPI_BUFFER_HANDLE)currentJob, currentJob->context);
        }
#if defined(DRV_SPI_USE_ISR)
        if (DRV_SPI_SYS_QUEUE_FreeElement(pDrvInstance->queue, currentJob) != DRV_SPI_SYS_QUEUE_SUCCESS)
#elif defined(DRV_SPI_USE_POLLED)
        if (DRV_SPI_SYS_QUEUE_FreeElementLock(pDrvInstance->queue, currentJob) != DRV_SPI_SYS_QUEUE_SUCCESS)
#endif
        {
            SYS_ASSERT(false, "SPI Driver: Queue free element error");
            return 0;
        }
        pDrvInstance->currentJob = NULL;
    }
#if defined(DRV_SPI_USE_ISR)
    SYS_INT_SourceStatusClear(pDrvInstance->rxInterruptSource);
#endif

    return 1;
}


