/*******************************************************************************
  SPI Driver Interface Implementation

  Company:
    Microchip Technology Inc.

  File Name:
   drv_spi_slave_task.i

  Summary:
 SPI Driver implementation for the enhanced buffer mode master functions.

  Description:
    The SPI Driver provides a interface to access the SPI hardware on the PIC32
    microcontroller.  This file implements the SPI Driver. This file
    should be included in the project if SPI driver functionality is needed.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

#if defined(DRV_SPI_USE_EBM)
    #if defined(DRV_SPI_USE_ISR)
    int32_t DRV_SPI_ISRSlaveEBMTasks ( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_POLLED)
    int32_t DRV_SPI_PolledSlaveEBMTasks ( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #endif
#elif defined(DRV_SPI_USE_RM)
    #if defined(DRV_SPI_USE_ISR)
    int32_t DRV_SPI_ISRSlaveRMTasks ( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #elif defined(DRV_SPI_POLLED)
    int32_t DRV_SPI_PolledSlaveRMTasks ( struct DRV_SPI_DRIVER_OBJECT * pDrvInstance )
    #endif
#endif
{
#if defined(DRV_SPI_USE_ISR)
    bool continueLoop;
    do {
        continueLoop = false;
#endif
        (*pDrvInstance->vfReceiveTask)(pDrvInstance);
        (*pDrvInstance->vfSendTask)(pDrvInstance);
        (*pDrvInstance->vfErrorTask)(pDrvInstance);

#if defined(DRV_SPI_USE_ISR)
        if ((PLIB_INT_SourceIsEnabled (INT_ID_0 , pDrvInstance->txInterruptSource) && SYS_INT_SourceStatusGet(pDrvInstance->txInterruptSource)) ||
            (PLIB_INT_SourceIsEnabled (INT_ID_0 , pDrvInstance->rxInterruptSource) && SYS_INT_SourceStatusGet(pDrvInstance->rxInterruptSource)) ||
            (PLIB_INT_SourceIsEnabled (INT_ID_0 , pDrvInstance->errInterruptSource) && SYS_INT_SourceStatusGet(pDrvInstance->errInterruptSource)))
        {
            continueLoop = true;
            continue;
        }
        if (pDrvInstance->currentJob != NULL)
        {
            return 0;
        }
        if (!DRV_SPI_SYS_QUEUE_IsEmpty(pDrvInstance->queue))
        {
            SYS_INT_SourceEnable(pDrvInstance->txInterruptSource);
            continueLoop = true;
            continue;
        }

    } while(continueLoop);
#endif
    return 0;
}