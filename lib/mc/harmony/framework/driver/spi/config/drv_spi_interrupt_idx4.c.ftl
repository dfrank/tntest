<#--
/*******************************************************************************
  SPI Driver Interrupt Handler Template File

  File Name:
    drv_spi_interrupt_IDX4.c.flt

  Summary:
    This file contains source code necessary to initialize the system.

  Description:
    This file contains source code necessary to initialize the system.  It
    implements the "SYS_Initialize" function, configuration bits, and allocates
    any necessary global system resources, such as the systemObjects structure
    that contains the object handles to all the MPLAB Harmony module objects in
    the system.
 *******************************************************************************/

/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->
<#if CONFIG_DRV_SPI_TASK_MODE_ISR_IDX4 == true>
<#if CONFIG_DRV_SPI_IDX4 == true>
<#if CONFIG_PIC32MX == true>
void __ISR(${CONFIG_DRV_SPI_ISR_VECTOR_IDX4}, ipl${CONFIG_DRV_SPI_INT_PRIO_NUM_IDX4}) _IntHandlerSPIInstance4(void)
{
<#if CONFIG_DRV_SPI_DRIVER_MODE == "DYNAMIC">
    DRV_SPI_Tasks(sysObj.spiObjectIdx4);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_SPI_TX_INT_SOURCE_IDX4});
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_SPI_RX_INT_SOURCE_IDX4});
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_SPI_ERROR_INT_SOURCE_IDX4});
</#if>
}
</#if>
<#if CONFIG_PIC32MZ == true>
void __ISR(${CONFIG_DRV_SPI_RX_ISR_VECTOR_IDX4}, ipl${CONFIG_DRV_SPI_RX_INT_PRIO_NUM_IDX4}) _IntHandlerSPIRxInstance4(void)
{
<#if CONFIG_DRV_SPI_DRIVER_MODE == "DYNAMIC">
    DRV_SPI_Tasks(sysObj.spiObjectIdx4);
</#if>
<#if CONFIG_DRV_SPI_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_SPI_TX_INT_SOURCE_IDX4});
</#if>
}
void __ISR(${CONFIG_DRV_SPI_TX_ISR_VECTOR_IDX4}, ipl${CONFIG_DRV_SPI_TX_INT_PRIO_NUM_IDX4}) _IntHandlerSPITxInstance4(void)
{
<#if CONFIG_DRV_SPI_DRIVER_MODE == "DYNAMIC">
    DRV_SPI_Tasks(sysObj.spiObjectIdx4);
</#if>
<#if CONFIG_DRV_SPI_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_SPI_RX_INT_SOURCE_IDX4});
</#if>
}
void __ISR(${CONFIG_DRV_SPI_ERROR_ISR_VECTOR_IDX4}, ipl${CONFIG_DRV_SPI_ERROR_INT_PRIO_NUM_IDX4}) _IntHandlerSPIFAultInstance4(void)
{
<#if CONFIG_DRV_SPI_DRIVER_MODE == "DYNAMIC">
    DRV_SPI_Tasks(sysObj.spiObjectIdx4);
</#if>
<#if CONFIG_DRV_SPI_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_SPI_ERROR_INT_SOURCE_IDX4});
</#if>
}
</#if> <#-- CONFIG_PIC32MZ -->
</#if> <#-- CONFIG_DRV_SPI_INST_IDX4 -->
</#if> <#-- CONFIG_DRV_SPI_TASK_MODE_ISR_IDX4 -->