<#--
/*******************************************************************************
  SPI Driver Freemarker Template File

  Company:
    Microchip Technology Inc.

  File Name:
    drv_spi.ftl

  Summary:
    SPI Driver Freemarker Template File

  Description:

*******************************************************************************/

/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS  WITHOUT  WARRANTY  OF  ANY  KIND,
EITHER EXPRESS  OR  IMPLIED,  INCLUDING  WITHOUT  LIMITATION,  ANY  WARRANTY  OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A  PARTICULAR  PURPOSE.
IN NO EVENT SHALL MICROCHIP OR  ITS  LICENSORS  BE  LIABLE  OR  OBLIGATED  UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,  BREACH  OF  WARRANTY,  OR
OTHER LEGAL  EQUITABLE  THEORY  ANY  DIRECT  OR  INDIRECT  DAMAGES  OR  EXPENSES
INCLUDING BUT NOT LIMITED TO ANY  INCIDENTAL,  SPECIAL,  INDIRECT,  PUNITIVE  OR
CONSEQUENTIAL DAMAGES, LOST  PROFITS  OR  LOST  DATA,  COST  OF  PROCUREMENT  OF
SUBSTITUTE  GOODS,  TECHNOLOGY,  SERVICES,  OR  ANY  CLAIMS  BY  THIRD   PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE  THEREOF),  OR  OTHER  SIMILAR  COSTS.
*******************************************************************************/
-->

<#if CONFIG_DRV_SPI_USE_DRIVER == true>
/*** SPI Driver Configuration ***/
/*** Driver Compilation and static configuration options. ***/
/*** Select SPI compilation units.***/
<#if CONFIG_DRV_SPI_USE_ISR_MODE != true && CONFIG_DRV_SPI_USE_POLLED_MODE != true>
#define DRV_SPI_POLLED 				1
#define DRV_SPI_ISR 				0
	<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX0 = true>
	<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX1 = true>
	<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX2 = true>
	<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX3 = true>
	<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX4 = true>
	<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX5 = true>
	<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX0 = false>
	<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX1 = false>
	<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX2 = false>
	<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX3 = false>
	<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX4 = false>
	<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX5 = false>
<#else>
	<#if CONFIG_DRV_SPI_USE_ISR_MODE == true>
#define DRV_SPI_ISR 				1
	<#else>
#define DRV_SPI_ISR 				0
		<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX0 = true>
		<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX1 = true>
		<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX2 = true>
		<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX3 = true>
		<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX4 = true>
		<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX5 = true>
		<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX0 = false>
		<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX1 = false>
		<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX2 = false>
		<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX3 = false>
		<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX4 = false>
		<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX5 = false>
	</#if>
	<#if CONFIG_DRV_SPI_USE_POLLED_MODE == true>
#define DRV_SPI_POLLED 				1
	<#else>
#define DRV_SPI_POLLED 				0
		<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX0 = false>
		<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX1 = false>
		<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX2 = false>
		<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX3 = false>
		<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX4 = false>
		<#assign CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX5 = false>
		<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX0 = true>
		<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX1 = true>
		<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX2 = true>
		<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX3 = true>
		<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX4 = true>
		<#assign CONFIG_DRV_SPI_TASK_MODE_ISR_IDX5 = true>
	</#if>	
</#if>
<#if CONFIG_DRV_SPI_USE_MASTER_MODE != true && CONFIG_DRV_SPI_USE_SLAVE_MODE != true>
#define DRV_SPI_MASTER 				1
#define DRV_SPI_SLAVE 				0
	<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX0 = true>
	<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX1 = true>
	<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX2 = true>
	<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX3 = true>
	<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX4 = true>
	<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX5 = true>
	<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX0 = false>
	<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX1 = false>
	<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX2 = false>
	<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX3 = false>
	<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX4 = false>
	<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX5 = false>
<#else>
	<#if CONFIG_DRV_SPI_USE_MASTER_MODE == true>
#define DRV_SPI_MASTER 				1
	<#else>
#define DRV_SPI_MASTER 				0
		<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX0 = false>
		<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX1 = false>
		<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX2 = false>
		<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX3 = false>
		<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX4 = false>
		<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX5 = false>
		<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX0 = true>
		<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX1 = true>
		<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX2 = true>
		<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX3 = true>
		<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX4 = true>
		<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX5 = true>
	</#if>	
	<#if CONFIG_DRV_SPI_USE_SLAVE_MODE == true>
#define DRV_SPI_SLAVE 				1
	<#else>
#define DRV_SPI_SLAVE 				0
		<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX0 = true>
		<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX1 = true>
		<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX2 = true>
		<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX3 = true>
		<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX4 = true>
		<#assign CONFIG_DRV_SPI_SPI_MODE_MASTER_IDX5 = true>
		<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX0 = false>
		<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX1 = false>
		<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX2 = false>
		<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX3 = false>
		<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX4 = false>
		<#assign CONFIG_DRV_SPI_SPI_MODE_SLAVE_IDX5 = false>
	</#if>
</#if>
<#if CONFIG_DRV_SPI_USE_STANDARD_BUFFER_MODE != true && CONFIG_DRV_SPI_USE_ENHANCED_BUFFER_MODE != true>
#define DRV_SPI_RM 				1
#define DRV_SPI_EBM 				0
	<#assign DRV_SPI_BUFFER_STANDARD_IDX0 = true>
	<#assign DRV_SPI_BUFFER_STANDARD_IDX1 = true>
	<#assign DRV_SPI_BUFFER_STANDARD_IDX2 = true>
	<#assign DRV_SPI_BUFFER_STANDARD_IDX3 = true>
	<#assign DRV_SPI_BUFFER_STANDARD_IDX4 = true>
	<#assign DRV_SPI_BUFFER_STANDARD_IDX5 = true>
	<#assign DRV_SPI_BUFFER_ENHANCED_IDX0 = false>
	<#assign DRV_SPI_BUFFER_ENHANCED_IDX1 = false>
	<#assign DRV_SPI_BUFFER_ENHANCED_IDX2 = false>
	<#assign DRV_SPI_BUFFER_ENHANCED_IDX3 = false>
	<#assign DRV_SPI_BUFFER_ENHANCED_IDX4 = false>
	<#assign DRV_SPI_BUFFER_ENHANCED_IDX5 = false>
<#else>
	<#if CONFIG_DRV_SPI_USE_STANDARD_BUFFER_MODE == true>
#define DRV_SPI_RM 				1
	<#else>
#define DRV_SPI_RM 				0
		<#assign DRV_SPI_BUFFER_STANDARD_IDX0 = false>
		<#assign DRV_SPI_BUFFER_STANDARD_IDX1 = false>
		<#assign DRV_SPI_BUFFER_STANDARD_IDX2 = false>
		<#assign DRV_SPI_BUFFER_STANDARD_IDX3 = false>
		<#assign DRV_SPI_BUFFER_STANDARD_IDX4 = false>
		<#assign DRV_SPI_BUFFER_STANDARD_IDX5 = false>
		<#assign DRV_SPI_BUFFER_ENHANCED_IDX0 = true>
		<#assign DRV_SPI_BUFFER_ENHANCED_IDX1 = true>
		<#assign DRV_SPI_BUFFER_ENHANCED_IDX2 = true>
		<#assign DRV_SPI_BUFFER_ENHANCED_IDX3 = true>
		<#assign DRV_SPI_BUFFER_ENHANCED_IDX4 = true>
		<#assign DRV_SPI_BUFFER_ENHANCED_IDX5 = true>
	</#if>
	<#if CONFIG_DRV_SPI_USE_ENHANCED_BUFFER_MODE == true>
#define DRV_SPI_EBM 				1
	<#else>
#define DRV_SPI_EBM 				0
		<#assign DRV_SPI_BUFFER_STANDARD_IDX0 = true>
		<#assign DRV_SPI_BUFFER_STANDARD_IDX1 = true>
		<#assign DRV_SPI_BUFFER_STANDARD_IDX2 = true>
		<#assign DRV_SPI_BUFFER_STANDARD_IDX3 = true>
		<#assign DRV_SPI_BUFFER_STANDARD_IDX4 = true>
		<#assign DRV_SPI_BUFFER_STANDARD_IDX5 = true>
		<#assign DRV_SPI_BUFFER_ENHANCED_IDX0 = false>
		<#assign DRV_SPI_BUFFER_ENHANCED_IDX1 = false>
		<#assign DRV_SPI_BUFFER_ENHANCED_IDX2 = false>
		<#assign DRV_SPI_BUFFER_ENHANCED_IDX3 = false>
		<#assign DRV_SPI_BUFFER_ENHANCED_IDX4 = false>
		<#assign DRV_SPI_BUFFER_ENHANCED_IDX5 = false>
	</#if>
</#if>
<#if CONFIG_DRV_SPI_USE_8BIT_MODE != true && CONFIG_DRV_SPI_USE_16BIT_MODE != true && CONFIG_DRV_SPI_USE_32BIT_MODE != true>
#define DRV_SPI_8BIT 				1
#define DRV_SPI_16BIT 				0 
#define DRV_SPI_32BIT 				0 
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_8_BIT_IDX0 = true>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_8_BIT_IDX1 = true>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_8_BIT_IDX2 = true>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_8_BIT_IDX3 = true>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_8_BIT_IDX4 = true>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_8_BIT_IDX5 = true>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_16_BIT_IDX0 = false>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_16_BIT_IDX1 = false>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_16_BIT_IDX2 = false>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_16_BIT_IDX3 = false>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_16_BIT_IDX4 = false>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_16_BIT_IDX5 = false>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_32_BIT_IDX0 = false>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_32_BIT_IDX1 = false>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_32_BIT_IDX2 = false>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_32_BIT_IDX3 = false>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_32_BIT_IDX4 = false>
	<#assign CONFIG_DRV_SPI_COMM_WIDTH_32_BIT_IDX5 = false>
<#else>
	<#if CONFIG_DRV_SPI_USE_8BIT_MODE == true>
#define DRV_SPI_8BIT 				1
    <#else>
#define DRV_SPI_8BIT 				0
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_8_BIT_IDX0 = false>
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_8_BIT_IDX1 = false>
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_8_BIT_IDX2 = false>
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_8_BIT_IDX3 = false>
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_8_BIT_IDX4 = false>
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_8_BIT_IDX5 = false>
    </#if>
	<#if CONFIG_DRV_SPI_USE_16BIT_MODE == true>
#define DRV_SPI_16BIT 				1
    <#else>
#define DRV_SPI_16BIT				0
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_16_BIT_IDX0 = false>
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_16_BIT_IDX1 = false>
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_16_BIT_IDX2 = false>
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_16_BIT_IDX3 = false>
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_16_BIT_IDX4 = false>
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_16_BIT_IDX5 = false>
    </#if>
	<#if CONFIG_DRV_SPI_USE_32BIT_MODE == true>
#define DRV_SPI_32BIT 				1
    <#else>
#define DRV_SPI_32BIT 				0
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_32_BIT_IDX0 = false>
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_32_BIT_IDX1 = false>
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_32_BIT_IDX2 = false>
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_32_BIT_IDX3 = false>
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_32_BIT_IDX4 = false>
		<#assign CONFIG_DRV_SPI_COMM_WIDTH_32_BIT_IDX5 = false>
    </#if>
</#if>
<#if CONFIG_DRV_SPI_USE_DMA == true>
#define DRV_SPI_DMA 				1
<#else>
#define DRV_SPI_DMA 				0
</#if>
</#if>

/*** SPI Driver Static Allocation Options ***/
<#if CONFIG_DRV_SPI_INSTANCES_NUMBER?has_content>
#define DRV_SPI_INSTANCES_NUMBER 		${CONFIG_DRV_SPI_INSTANCES_NUMBER}
<#else>
#define DRV_SPI_INSTANCES_NUMBER 		1
</#if>
<#if CONFIG_DRV_SPI_CLIENT_NUMBER?has_content>
#define DRV_SPI_CLIENTS_NUMBER 			${CONFIG_DRV_SPI_CLIENT_NUMBER}
<#else>
#define DRV_SPI_CLIENTS_NUMBER 			1
</#if>
<#if CONFIG_DRV_SPI_NUM_ELEMENTS_PER_INSTANCE?has_content>
#define DRV_SPI_ELEMENTS_PER_QUEUE 		${CONFIG_DRV_SPI_NUM_ELEMENTS_PER_INSTANCE}
<#else>
#define DRV_SPI_ELEMENTS_PER_QUEUE 		10
</#if>
<#if CONFIG_DRV_SPI_USE_DMA == true>
/*** SPI Driver DMA Options ***/
	<#if CONFIG_DRV_SPI_DMA_TXFER_SIZE?has_content>
#define DRV_SPI_DMA_TXFER_SIZE 			${CONFIG_DRV_SPI_DMA_TXFER_SIZE}
	<#else>
#define DRV_SPI_DMA_TXFER_SIZE 			256
	</#if>
	<#if CONFIG_DRV_SPI_DMA_DUMMY_BUFFER_SIZE?has_content>
#define DRV_SPI_DMA_DUMMY_BUFFER_SIZE 		${CONFIG_DRV_SPI_DMA_DUMMY_BUFFER_SIZE}
	<#else>
#define DRV_SPI_DMA_DUMMY_BUFFER_SIZE 		256
	</#if>
</#if>
<#if CONFIG_DRV_SPI_IDX0 == true>
<#include "/framework/driver/spi/config/drv_spi_idx0.h.ftl">
</#if>
<#if CONFIG_DRV_SPI_IDX1 == true>
<#include "/framework/driver/spi/config/drv_spi_idx1.h.ftl">
</#if>
<#if CONFIG_DRV_SPI_IDX2 == true>
<#include "/framework/driver/spi/config/drv_spi_idx2.h.ftl">
</#if>
<#if CONFIG_DRV_SPI_IDX3 == true>
<#include "/framework/driver/spi/config/drv_spi_idx3.h.ftl">
</#if>
<#if CONFIG_DRV_SPI_IDX4 == true>
<#include "/framework/driver/spi/config/drv_spi_idx4.h.ftl">
</#if>
<#if CONFIG_DRV_SPI_IDX5 == true>
<#include "/framework/driver/spi/config/drv_spi_idx5.h.ftl">
</#if>

