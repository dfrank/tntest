<#--
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->
 
 /*** SPI Driver Initialization Data ***/
 <#if CONFIG_DRV_SPI_IDX0 == true>
 /*** Index 0  ***/
 DRV_SPI_INIT drvSpi0InitData =
 {
    .spiId = DRV_SPI_SPI_ID_IDX0,
    .taskMode = DRV_SPI_TASK_MODE_IDX0,
    .spiMode = DRV_SPI_SPI_MODE_IDX0,
    .allowIdleRun = DRV_SPI_ALLOW_IDLE_RUN_IDX0,
    .spiProtocolType = DRV_SPI_SPI_PROTOCOL_TYPE_IDX0,
<#if CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX0?has_content>
    <#if CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX0 == "DRV_SPI_PROTOCOL_TYPE_FRAMED">
    .frameSyncPulse = DRV_SPI_FRAME_SYNC_PULSE_IDX0,
    .framePulsePolarity = DRV_SPI_FRAME_PULSE_POLARITY_IDX0,
    .framePulseDirection = DRV_SPI_FRAME_PULSE_DIRECTION_IDX0,
    .framePulseEdge = DRV_SPI_FRAME_PULSE_EDGE_IDX0,
    .framePulseWidth = DRV_SPI_FRAME_PULSE_WIDTH_IDX0,
    <#elseif CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX0 == "DRV_SPI_PROTOCOL_TYPE_AUDIO">
    .audioTransmitMode = DRV_SPI_AUDIO_TRANSMIT_MODE_IDX0,
    .audioProtocolMode = DRV_SPI_AUDIO_PROTOCOL_MODE_IDX0,
    </#if>
 </#if>
    .commWidth = DRV_SPI_COMM_WIDTH_IDX0,
    .spiClk = DRV_SPI_SPI_CLOCK_IDX0,
    .baudRate = DRV_SPI_BAUD_RATE_IDX0,
    .bufferType = DRV_SPI_BUFFER_TYPE_IDX0,
    .clockMode = DRV_SPI_CLOCK_MODE_IDX0,
    .inputSamplePhase = DRV_SPI_INPUT_PHASE_IDX0,
 <#if CONFIG_DRV_SPI_TASK_MODE_ISR_IDX0 == true>
    .txInterruptSource = DRV_SPI_TX_INT_SOURCE_IDX0,
    .rxInterruptSource = DRV_SPI_RX_INT_SOURCE_IDX0,
    .errInterruptSource = DRV_SPI_ERROR_INT_SOURCE_IDX0,
 </#if>
 <#if CONFIG_DRV_SPI_USE_DMA_IDX0 == true>
    .txDmaThreshold = DRV_SPI_TX_DMA_THRESHOLD_IDX0,
    .rxDmaThreshold = DRV_SPI_RX_DMA_THRESHOLD_IDX0,
 </#if>
    .queueSize = DRV_SPI_QUEUE_SIZE_IDX0,
    .jobQueueReserveSize = DRV_SPI_RESERVED_JOB_IDX0,
 <#if CONFIG_DRV_SPI_BUFFER_STANDARD_IDX0 == true && CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX0 == true>
    .numTrfsSmPolled = DRV_SPI_TRANS_PER_SM_RUN_IDX0,
 </#if>
 };
 </#if>
 
  <#if CONFIG_DRV_SPI_IDX1 == true>
 /*** Index 1  ***/
 DRV_SPI_INIT drvSpi1InitData =
 {
    .spiId = DRV_SPI_SPI_ID_IDX1,
    .taskMode = DRV_SPI_TASK_MODE_IDX1,
    .spiMode = DRV_SPI_SPI_MODE_IDX1,
    .allowIdleRun = DRV_SPI_ALLOW_IDLE_RUN_IDX1,
    .spiProtocolType = DRV_SPI_SPI_PROTOCOL_TYPE_IDX1,
<#if CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX1?has_content>
    <#if CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX1 == "DRV_SPI_PROTOCOL_TYPE_FRAMED">
    .frameSyncPulse = DRV_SPI_FRAME_SYNC_PULSE_IDX1,
    .framePulsePolarity = DRV_SPI_FRAME_PULSE_POLARITY_IDX1,
    .framePulseDirection = DRV_SPI_FRAME_PULSE_DIRECTION_IDX1,
    .framePulseEdge = DRV_SPI_FRAME_PULSE_EDGE_IDX1,
    .framePulseWidth = DRV_SPI_FRAME_PULSE_WIDTH_IDX1,
    <#elseif CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX1 == "DRV_SPI_PROTOCOL_TYPE_AUDIO">
    .audioTransmitMode = DRV_SPI_AUDIO_TRANSMIT_MODE_IDX1,
    .audioProtocolMode = DRV_SPI_AUDIO_PROTOCOL_MODE_IDX1,
    </#if>
 </#if>
    .commWidth = DRV_SPI_COMM_WIDTH_IDX1,
    .spiClk = DRV_SPI_SPI_CLOCK_IDX1,
    .baudRate = DRV_SPI_BAUD_RATE_IDX1,
    .bufferType = DRV_SPI_BUFFER_TYPE_IDX1,
    .clockMode = DRV_SPI_CLOCK_MODE_IDX1,
    .inputSamplePhase = DRV_SPI_INPUT_PHASE_IDX1,
 <#if CONFIG_DRV_SPI_TASK_MODE_ISR_IDX1 == true>
    .txInterruptSource = DRV_SPI_TX_INT_SOURCE_IDX1,
    .rxInterruptSource = DRV_SPI_RX_INT_SOURCE_IDX1,
    .errInterruptSource = DRV_SPI_ERROR_INT_SOURCE_IDX1,
 </#if>
 <#if CONFIG_DRV_SPI_USE_DMA_IDX1 == true>
    .txDmaThreshold = DRV_SPI_TX_DMA_THRESHOLD_IDX1,
    .rxDmaThreshold = DRV_SPI_RX_DMA_THRESHOLD_IDX1,
 </#if>
    .queueSize = DRV_SPI_QUEUE_SIZE_IDX1,
    .jobQueueReserveSize = DRV_SPI_RESERVED_JOB_IDX1,
 <#if CONFIG_DRV_SPI_BUFFER_STANDARD_IDX1 == true && CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX1 == true>
    .numTrfsSmPolled = DRV_SPI_TRANS_PER_SM_RUN_IDX1,
 </#if>
 };
 </#if>
 
 <#if CONFIG_DRV_SPI_IDX2 == true>
 /*** Index 2  ***/
 DRV_SPI_INIT drvSpi2InitData =
 {
    .spiId = DRV_SPI_SPI_ID_IDX2,
    .taskMode = DRV_SPI_TASK_MODE_IDX2,
    .spiMode = DRV_SPI_SPI_MODE_IDX2,
    .allowIdleRun = DRV_SPI_ALLOW_IDLE_RUN_IDX2,
    .spiProtocolType = DRV_SPI_SPI_PROTOCOL_TYPE_IDX2,
<#if CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX2?has_content>
    <#if CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX2 == "DRV_SPI_PROTOCOL_TYPE_FRAMED">
    .frameSyncPulse = DRV_SPI_FRAME_SYNC_PULSE_IDX2,
    .framePulsePolarity = DRV_SPI_FRAME_PULSE_POLARITY_IDX2,
    .framePulseDirection = DRV_SPI_FRAME_PULSE_DIRECTION_IDX2,
    .framePulseEdge = DRV_SPI_FRAME_PULSE_EDGE_IDX2,
    .framePulseWidth = DRV_SPI_FRAME_PULSE_WIDTH_IDX2,
    <#elseif CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX2 == "DRV_SPI_PROTOCOL_TYPE_AUDIO">
    .audioTransmitMode = DRV_SPI_AUDIO_TRANSMIT_MODE_IDX2,
    .audioProtocolMode = DRV_SPI_AUDIO_PROTOCOL_MODE_IDX2,
    </#if>
 </#if>
    .commWidth = DRV_SPI_COMM_WIDTH_IDX2,
    .spiClk = DRV_SPI_SPI_CLOCK_IDX2,
    .baudRate = DRV_SPI_BAUD_RATE_IDX2,
    .bufferType = DRV_SPI_BUFFER_TYPE_IDX2,
    .clockMode = DRV_SPI_CLOCK_MODE_IDX2,
    .inputSamplePhase = DRV_SPI_INPUT_PHASE_IDX2,
 <#if CONFIG_DRV_SPI_TASK_MODE_ISR_IDX2 == true>
    .txInterruptSource = DRV_SPI_TX_INT_SOURCE_IDX2,
    .rxInterruptSource = DRV_SPI_RX_INT_SOURCE_IDX2,
    .errInterruptSource = DRV_SPI_ERROR_INT_SOURCE_IDX2,
 </#if>
 <#if CONFIG_DRV_SPI_USE_DMA_IDX2 == true>
    .txDmaThreshold = DRV_SPI_TX_DMA_THRESHOLD_IDX2,
    .rxDmaThreshold = DRV_SPI_RX_DMA_THRESHOLD_IDX2,
 </#if>
    .queueSize = DRV_SPI_QUEUE_SIZE_IDX2,
    .jobQueueReserveSize = DRV_SPI_RESERVED_JOB_IDX2,
 <#if CONFIG_DRV_SPI_BUFFER_STANDARD_IDX2 == true && CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX2 == true>
    .numTrfsSmPolled = DRV_SPI_TRANS_PER_SM_RUN_IDX2,
 </#if>
 };
 </#if>
 
  
 <#if CONFIG_DRV_SPI_IDX3 == true>
 /*** Index 3  ***/
 DRV_SPI_INIT drvSpi3InitData =
 {
    .spiId = DRV_SPI_SPI_ID_IDX3,
    .taskMode = DRV_SPI_TASK_MODE_IDX3,
    .spiMode = DRV_SPI_SPI_MODE_IDX3,
    .allowIdleRun = DRV_SPI_ALLOW_IDLE_RUN_IDX3,
    .spiProtocolType = DRV_SPI_SPI_PROTOCOL_TYPE_IDX3,
<#if CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX3?has_content>
    <#if CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX3 == "DRV_SPI_PROTOCOL_TYPE_FRAMED">
    .frameSyncPulse = DRV_SPI_FRAME_SYNC_PULSE_IDX3,
    .framePulsePolarity = DRV_SPI_FRAME_PULSE_POLARITY_IDX3,
    .framePulseDirection = DRV_SPI_FRAME_PULSE_DIRECTION_IDX3,
    .framePulseEdge = DRV_SPI_FRAME_PULSE_EDGE_IDX3,
    .framePulseWidth = DRV_SPI_FRAME_PULSE_WIDTH_IDX3,
    <#elseif CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX3 == "DRV_SPI_PROTOCOL_TYPE_AUDIO">
    .audioTransmitMode = DRV_SPI_AUDIO_TRANSMIT_MODE_IDX3,
    .audioProtocolMode = DRV_SPI_AUDIO_PROTOCOL_MODE_IDX3,
    </#if>
 </#if>
    .commWidth = DRV_SPI_COMM_WIDTH_IDX3,
    .spiClk = DRV_SPI_SPI_CLOCK_IDX3,
    .baudRate = DRV_SPI_BAUD_RATE_IDX3,
    .bufferType = DRV_SPI_BUFFER_TYPE_IDX3,
    .clockMode = DRV_SPI_CLOCK_MODE_IDX3,
    .inputSamplePhase = DRV_SPI_INPUT_PHASE_IDX3,
 <#if CONFIG_DRV_SPI_TASK_MODE_ISR_IDX3 == true>
    .txInterruptSource = DRV_SPI_TX_INT_SOURCE_IDX3,
    .rxInterruptSource = DRV_SPI_RX_INT_SOURCE_IDX3,
    .errInterruptSource = DRV_SPI_ERROR_INT_SOURCE_IDX3,
 </#if>
 <#if CONFIG_DRV_SPI_USE_DMA_IDX3 == true>
    .txDmaThreshold = DRV_SPI_TX_DMA_THRESHOLD_IDX3,
    .rxDmaThreshold = DRV_SPI_RX_DMA_THRESHOLD_IDX3,
 </#if>
    .queueSize = DRV_SPI_QUEUE_SIZE_IDX3,
    .jobQueueReserveSize = DRV_SPI_RESERVED_JOB_IDX3,
 <#if CONFIG_DRV_SPI_BUFFER_STANDARD_IDX3 == true && CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX3 == true>
    .numTrfsSmPolled = DRV_SPI_TRANS_PER_SM_RUN_IDX3,
 </#if>
 };
 </#if>
 
  
 <#if CONFIG_DRV_SPI_IDX2 == true>
 /*** Index 4  ***/
 DRV_SPI_INIT drvSpi4InitData =
 {
    .spiId = DRV_SPI_SPI_ID_IDX4,
    .taskMode = DRV_SPI_TASK_MODE_IDX4,
    .spiMode = DRV_SPI_SPI_MODE_IDX4,
    .allowIdleRun = DRV_SPI_ALLOW_IDLE_RUN_IDX4,
    .spiProtocolType = DRV_SPI_SPI_PROTOCOL_TYPE_IDX4,
<#if CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX4?has_content>
    <#if CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX4 == "DRV_SPI_PROTOCOL_TYPE_FRAMED">
    .frameSyncPulse = DRV_SPI_FRAME_SYNC_PULSE_IDX4,
    .framePulsePolarity = DRV_SPI_FRAME_PULSE_POLARITY_IDX4,
    .framePulseDirection = DRV_SPI_FRAME_PULSE_DIRECTION_IDX4,
    .framePulseEdge = DRV_SPI_FRAME_PULSE_EDGE_IDX4,
    .framePulseWidth = DRV_SPI_FRAME_PULSE_WIDTH_IDX4,
    <#elseif CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX4 == "DRV_SPI_PROTOCOL_TYPE_AUDIO">
    .audioTransmitMode = DRV_SPI_AUDIO_TRANSMIT_MODE_IDX4,
    .audioProtocolMode = DRV_SPI_AUDIO_PROTOCOL_MODE_IDX4,
    </#if>
 </#if>
    .commWidth = DRV_SPI_COMM_WIDTH_IDX4,
    .spiClk = DRV_SPI_SPI_CLOCK_IDX4,
    .baudRate = DRV_SPI_BAUD_RATE_IDX4,
    .bufferType = DRV_SPI_BUFFER_TYPE_IDX4,
    .clockMode = DRV_SPI_CLOCK_MODE_IDX4,
    .inputSamplePhase = DRV_SPI_INPUT_PHASE_IDX4,
 <#if CONFIG_DRV_SPI_TASK_MODE_ISR_IDX4 == true>
    .txInterruptSource = DRV_SPI_TX_INT_SOURCE_IDX4,
    .rxInterruptSource = DRV_SPI_RX_INT_SOURCE_IDX4,
    .errInterruptSource = DRV_SPI_ERROR_INT_SOURCE_IDX4,
 </#if>
 <#if CONFIG_DRV_SPI_USE_DMA_IDX4 == true>
    .txDmaThreshold = DRV_SPI_TX_DMA_THRESHOLD_IDX4,
    .rxDmaThreshold = DRV_SPI_RX_DMA_THRESHOLD_IDX4,
 </#if>
    .queueSize = DRV_SPI_QUEUE_SIZE_IDX4,
    .jobQueueReserveSize = DRV_SPI_RESERVED_JOB_IDX4,
 <#if CONFIG_DRV_SPI_BUFFER_STANDARD_IDX4 == true && CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX4 == true>
    .numTrfsSmPolled = DRV_SPI_TRANS_PER_SM_RUN_IDX4,
 </#if>
 };
 </#if>
 
  
 <#if CONFIG_DRV_SPI_IDX5 == true>
 /*** Index 5  ***/
 DRV_SPI_INIT drvSpi5InitData =
 {
    .spiId = DRV_SPI_SPI_ID_IDX5,
    .taskMode = DRV_SPI_TASK_MODE_IDX5,
    .spiMode = DRV_SPI_SPI_MODE_IDX5,
    .allowIdleRun = DRV_SPI_ALLOW_IDLE_RUN_IDX5,
    .spiProtocolType = DRV_SPI_SPI_PROTOCOL_TYPE_IDX5,
<#if CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX5?has_content>
    <#if CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX5 == "DRV_SPI_PROTOCOL_TYPE_FRAMED">
    .frameSyncPulse = DRV_SPI_FRAME_SYNC_PULSE_IDX5,
    .framePulsePolarity = DRV_SPI_FRAME_PULSE_POLARITY_IDX5,
    .framePulseDirection = DRV_SPI_FRAME_PULSE_DIRECTION_IDX5,
    .framePulseEdge = DRV_SPI_FRAME_PULSE_EDGE_IDX5,
    .framePulseWidth = DRV_SPI_FRAME_PULSE_WIDTH_IDX5,
    <#elseif CONFIG_DRV_SPI_SPI_PROTOCOL_TYPE_IDX5 == "DRV_SPI_PROTOCOL_TYPE_AUDIO">
    .audioTransmitMode = DRV_SPI_AUDIO_TRANSMIT_MODE_IDX5,
    .audioProtocolMode = DRV_SPI_AUDIO_PROTOCOL_MODE_IDX5,
    </#if>
 </#if>
    .commWidth = DRV_SPI_COMM_WIDTH_IDX5,
    .spiClk = DRV_SPI_SPI_CLOCK_IDX5,
    .baudRate = DRV_SPI_BAUD_RATE_IDX5,
    .bufferType = DRV_SPI_BUFFER_TYPE_IDX5,
    .clockMode = DRV_SPI_CLOCK_MODE_IDX5,
    .inputSamplePhase = DRV_SPI_INPUT_PHASE_IDX5,
 <#if CONFIG_DRV_SPI_TASK_MODE_ISR_IDX5 == true>
    .txInterruptSource = DRV_SPI_TX_INT_SOURCE_IDX5,
    .rxInterruptSource = DRV_SPI_RX_INT_SOURCE_IDX5,
    .errInterruptSource = DRV_SPI_ERROR_INT_SOURCE_IDX5,
 </#if>
 <#if CONFIG_DRV_SPI_USE_DMA_IDX5 == true>
    .txDmaThreshold = DRV_SPI_TX_DMA_THRESHOLD_IDX5,
    .rxDmaThreshold = DRV_SPI_RX_DMA_THRESHOLD_IDX5,
 </#if>
    .queueSize = DRV_SPI_QUEUE_SIZE_IDX5,
    .jobQueueReserveSize = DRV_SPI_RESERVED_JOB_IDX5,
 <#if CONFIG_DRV_SPI_BUFFER_STANDARD_IDX5 == true && CONFIG_DRV_SPI_TASK_MODE_POLLED_IDX5 == true>
    .numTrfsSmPolled = DRV_SPI_TRANS_PER_SM_RUN_IDX5,
 </#if>
 };
 </#if>