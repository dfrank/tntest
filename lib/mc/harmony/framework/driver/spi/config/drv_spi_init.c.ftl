<#--
/*******************************************************************************
  SPI Driver Freemarker Template File

  Company:
    Microchip Technology Inc.

  File Name:
    drv_spi_sys_init.c.ftl

  Summary:
    SPI Driver Freemarker Template File

  Description:

*******************************************************************************/

/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS  WITHOUT  WARRANTY  OF  ANY  KIND,
EITHER EXPRESS  OR  IMPLIED,  INCLUDING  WITHOUT  LIMITATION,  ANY  WARRANTY  OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A  PARTICULAR  PURPOSE.
IN NO EVENT SHALL MICROCHIP OR  ITS  LICENSORS  BE  LIABLE  OR  OBLIGATED  UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,  BREACH  OF  WARRANTY,  OR
OTHER LEGAL  EQUITABLE  THEORY  ANY  DIRECT  OR  INDIRECT  DAMAGES  OR  EXPENSES
INCLUDING BUT NOT LIMITED TO ANY  INCIDENTAL,  SPECIAL,  INDIRECT,  PUNITIVE  OR
CONSEQUENTIAL DAMAGES, LOST  PROFITS  OR  LOST  DATA,  COST  OF  PROCUREMENT  OF
SUBSTITUTE  GOODS,  TECHNOLOGY,  SERVICES,  OR  ANY  CLAIMS  BY  THIRD   PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE  THEREOF),  OR  OTHER  SIMILAR  COSTS.
*******************************************************************************/
-->
<#if CONFIG_DRV_SPI_IDX0 == true>

    /*** SPI Driver Index 0 initialization***/
<#if CONFIG_DRV_SPI_USE_DMA_IDX0 == true>
    sysObj.spiTxDmaIdx0 = SYS_DMA_ChannelAllocate(DRV_SPI_TX_DMA_CHANNEL_IDX0);
    sysObj.spiRxDmaIdx0 = SYS_DMA_ChannelAllocate(DRV_SPI_RX_DMA_CHANNEL_IDX0);
    SYS_DMA_ChannelSetup(sysObj.spiTxDmaIdx0, DRV_SPI_TX_DMA_CHANNEL_PRIORITY_IDX0, DRV_SPI_TX_DMA_CHANNEL_OP_MODE_IDX0 , DRV_SPI_TX_DMA_TRIGGER_IDX0);
    SYS_DMA_ChannelSetup(sysObj.spiRxDmaIdx0, DRV_SPI_RX_DMA_CHANNEL_PRIORITY_IDX0, DRV_SPI_RX_DMA_CHANNEL_OP_MODE_IDX0 , DRV_SPI_RX_DMA_TRIGGER_IDX0);
    drvSpi0InitData.txDmaChannelHandle = sysObj.spiTxDmaIdx0;
    drvSpi0InitData.rxDmaChannelHandle = sysObj.spiRxDmaIdx0;
</#if>
<#if CONFIG_DRV_SPI_TASK_MODE_ISR_IDX0 == true>

<#if CONFIG_PIC32MX == true>
    SYS_INT_VectorPrioritySet(DRV_SPI_INT_VECTOR_IDX0, DRV_SPI_INT_PRIORITY_IDX0);
    SYS_INT_VectorSubprioritySet(DRV_SPI_INT_VECTOR_IDX0, DRV_SPI_INT_SUB_PRIORITY_IDX0);
<#else>
    SYS_INT_VectorPrioritySet(DRV_SPI_TX_INT_VECTOR_IDX0, DRV_SPI_TX_INT_PRIORITY_IDX0);
    SYS_INT_VectorSubprioritySet(DRV_SPI_TX_INT_VECTOR_IDX0, DRV_SPI_TX_INT_SUB_PRIORITY_IDX0);
    SYS_INT_VectorPrioritySet(DRV_SPI_RX_INT_VECTOR_IDX0, DRV_SPI_RX_INT_PRIORITY_IDX0);
    SYS_INT_VectorSubprioritySet(DRV_SPI_RX_INT_VECTOR_IDX0, DRV_SPI_RX_INT_SUB_PRIORITY_IDX0);
    SYS_INT_VectorPrioritySet(DRV_DRV_SPI_ERROR_INT_VECTOR_IDX0, DRV_SPI_ERROR_INT_PRIORITY_IDX0);
    SYS_INT_VectorSubprioritySet(DRV_DRV_SPI_ERROR_INT_VECTOR_IDX0, DRV_SPI_ERROR_INT_SUB_PRIORITY_IDX0);
</#if>
</#if>    
 
    sysObj.spiObjectIdx0 = DRV_SPI_Initialize(0, (const SYS_MODULE_INIT  * const)&drvSpi0InitData);
</#if>
<#if CONFIG_DRV_SPI_IDX1 == true>

    /*** SPI Driver Index 1 initialization***/
<#if CONFIG_DRV_SPI_USE_DMA_IDX1 == true>
    sysObj.spiTxDmaIdx1 = SYS_DMA_ChannelAllocate(DRV_SPI_TX_DMA_CHANNEL_IDX1);
    sysObj.spiRxDmaIdx1 = SYS_DMA_ChannelAllocate(DRV_SPI_RX_DMA_CHANNEL_IDX1);
    SYS_DMA_ChannelSetup(sysObj.spiTxDmaIdx1, DRV_SPI_TX_DMA_CHANNEL_PRIORITY_IDX1, DRV_SPI_TX_DMA_CHANNEL_OP_MODE_IDX1 , DRV_SPI_TX_DMA_TRIGGER_IDX1);
    SYS_DMA_ChannelSetup(sysObj.spiRxDmaIdx1, DRV_SPI_RX_DMA_CHANNEL_PRIORITY_IDX1, DRV_SPI_RX_DMA_CHANNEL_OP_MODE_IDX1 , DRV_SPI_RX_DMA_TRIGGER_IDX1);
    drvSpi0InitData.txDmaChannelHandle = sysObj.spiTxDmaIdx1;
    drvSpi0InitData.rxDmaChannelHandle = sysObj.spiRxDmaIdx1;
</#if>
<#if CONFIG_DRV_SPI_TASK_MODE_ISR_IDX1 == true>

<#if CONFIG_PIC32MX == true>
    SYS_INT_VectorPrioritySet(DRV_SPI_INT_VECTOR_IDX1, DRV_SPI_INT_PRIORITY_IDX1);
    SYS_INT_VectorSubprioritySet(DRV_SPI_INT_VECTOR_IDX1, DRV_SPI_INT_SUB_PRIORITY_IDX1);
<#else>
    SYS_INT_VectorPrioritySet(DRV_SPI_TX_INT_VECTOR_IDX1, DRV_SPI_TX_INT_PRIORITY_IDX1);
    SYS_INT_VectorSubprioritySet(DRV_SPI_TX_INT_VECTOR_IDX1, DRV_SPI_TX_INT_SUB_PRIORITY_IDX1);
    SYS_INT_VectorPrioritySet(DRV_SPI_RX_INT_VECTOR_IDX1, DRV_SPI_RX_INT_PRIORITY_IDX1);
    SYS_INT_VectorSubprioritySet(DRV_SPI_RX_INT_VECTOR_IDX1, DRV_SPI_RX_INT_SUB_PRIORITY_IDX1);
    SYS_INT_VectorPrioritySet(DRV_DRV_SPI_ERROR_INT_VECTOR_IDX1, DRV_SPI_ERROR_INT_PRIORITY_IDX1);
    SYS_INT_VectorSubprioritySet(DRV_DRV_SPI_ERROR_INT_VECTOR_IDX1, DRV_SPI_ERROR_INT_SUB_PRIORITY_IDX1);
</#if>
</#if>
    sysObj.spiObjectIdx1 = DRV_SPI_Initialize(1, (const SYS_MODULE_INIT  * const)&drvSpi1InitData);
</#if>
<#if CONFIG_DRV_SPI_IDX2 == true>

    /*** SPI Driver Index 2 initialization***/
<#if CONFIG_DRV_SPI_USE_DMA_IDX2 == true>
    sysObj.spiTxDmaIdx2 = SYS_DMA_ChannelAllocate(DRV_SPI_TX_DMA_CHANNEL_IDX2);
    sysObj.spiRxDmaIdx2 = SYS_DMA_ChannelAllocate(DRV_SPI_RX_DMA_CHANNEL_IDX2);
    SYS_DMA_ChannelSetup(sysObj.spiTxDmaIdx2, DRV_SPI_TX_DMA_CHANNEL_PRIORITY_IDX2, DRV_SPI_TX_DMA_CHANNEL_OP_MODE_IDX2, DRV_SPI_TX_DMA_TRIGGER_IDX2);
    SYS_DMA_ChannelSetup(sysObj.spiRxDmaIdx2, DRV_SPI_RX_DMA_CHANNEL_PRIORITY_IDX2, DRV_SPI_RX_DMA_CHANNEL_OP_MODE_IDX2, DRV_SPI_RX_DMA_TRIGGER_IDX2);
    drvSpi2InitData.txDmaChannelHandle = sysObj.spiTxDmaIdx2;
    drvSpi2InitData.rxDmaChannelHandle = sysObj.spiRxDmaIdx2;
</#if>
<#if CONFIG_DRV_SPI_TASK_MODE_ISR_IDX2 == true>

<#if CONFIG_PIC32MX == true>
    SYS_INT_VectorPrioritySet(DRV_SPI_INT_VECTOR_IDX2, DRV_SPI_INT_PRIORITY_IDX2);
    SYS_INT_VectorSubprioritySet(DRV_SPI_INT_VECTOR_IDX2, DRV_SPI_INT_SUB_PRIORITY_IDX2);
<#else>
    SYS_INT_VectorPrioritySet(DRV_SPI_TX_INT_VECTOR_IDX2, DRV_SPI_TX_INT_PRIORITY_IDX2);
    SYS_INT_VectorSubprioritySet(DRV_SPI_TX_INT_VECTOR_IDX2, DRV_SPI_TX_INT_SUB_PRIORITY_IDX2);
    SYS_INT_VectorPrioritySet(DRV_SPI_RX_INT_VECTOR_IDX2, DRV_SPI_RX_INT_PRIORITY_IDX2);
    SYS_INT_VectorSubprioritySet(DRV_SPI_RX_INT_VECTOR_IDX2, DRV_SPI_RX_INT_SUB_PRIORITY_IDX2);
    SYS_INT_VectorPrioritySet(DRV_DRV_SPI_ERROR_INT_VECTOR_IDX2, DRV_SPI_ERROR_INT_PRIORITY_IDX2);
    SYS_INT_VectorSubprioritySet(DRV_DRV_SPI_ERROR_INT_VECTOR_IDX2, DRV_SPI_ERROR_INT_SUB_PRIORITY_IDX2);
</#if>
</#if>
    sysObj.spiObjectIdx2 = DRV_SPI_Initialize(2, (const SYS_MODULE_INIT  * const)&drvSpi2InitData);
</#if>
<#if CONFIG_DRV_SPI_IDX3 == true>

    /*** SPI Driver Index 3 initialization***/
<#if CONFIG_DRV_SPI_USE_DMA_IDX3 == true>
    sysObj.spiTxDmaIdx3 = SYS_DMA_ChannelAllocate(DRV_SPI_TX_DMA_CHANNEL_IDX3);
    sysObj.spiRxDmaIdx3 = SYS_DMA_ChannelAllocate(DRV_SPI_RX_DMA_CHANNEL_IDX3);
    SYS_DMA_ChannelSetup(sysObj.spiTxDmaIdx3, DRV_SPI_TX_DMA_CHANNEL_PRIORITY_IDX3, DRV_SPI_TX_DMA_CHANNEL_OP_MODE_IDX3 , DRV_SPI_TX_DMA_TRIGGER_IDX3);
    SYS_DMA_ChannelSetup(sysObj.spiRxDmaIdx3, DRV_SPI_RX_DMA_CHANNEL_PRIORITY_IDX3, DRV_SPI_RX_DMA_CHANNEL_OP_MODE_IDX3 , DRV_SPI_RX_DMA_TRIGGER_IDX3);
    drvSpi3InitData.txDmaChannelHandle = sysObj.spiTxDmaIdx3;
    drvSpi3InitData.rxDmaChannelHandle = sysObj.spiRxDmaIdx3;
</#if>
<#if CONFIG_DRV_SPI_TASK_MODE_ISR_IDX3 == true>

<#if CONFIG_PIC32MX == true>
    SYS_INT_VectorPrioritySet(DRV_SPI_INT_VECTOR_IDX3, DRV_SPI_INT_PRIORITY_IDX3);
    SYS_INT_VectorSubprioritySet(DRV_SPI_INT_VECTOR_IDX3, DRV_SPI_INT_SUB_PRIORITY_IDX3);
<#else>    
    SYS_INT_VectorPrioritySet(DRV_SPI_TX_INT_VECTOR_IDX3, DRV_SPI_TX_INT_PRIORITY_IDX3);
    SYS_INT_VectorSubprioritySet(DRV_SPI_TX_INT_VECTOR_IDX3, DRV_SPI_TX_INT_SUB_PRIORITY_IDX3);
    SYS_INT_VectorPrioritySet(DRV_SPI_RX_INT_VECTOR_IDX3, DRV_SPI_RX_INT_PRIORITY_IDX3);
    SYS_INT_VectorSubprioritySet(DRV_SPI_RX_INT_VECTOR_IDX3, DRV_SPI_RX_INT_SUB_PRIORITY_IDX3);
    SYS_INT_VectorPrioritySet(DRV_DRV_SPI_ERROR_INT_VECTOR_IDX3, DRV_SPI_ERROR_INT_PRIORITY_IDX3);
    SYS_INT_VectorSubprioritySet(DRV_DRV_SPI_ERROR_INT_VECTOR_IDX3, DRV_SPI_ERROR_INT_SUB_PRIORITY_IDX3);
</#if>
</#if>
    sysObj.spiObjectIdx3 = DRV_SPI_Initialize(3, (const SYS_MODULE_INIT  * const)&drvSpi3InitData);
</#if>
<#if CONFIG_DRV_SPI_IDX4 == true>

    /*** SPI Driver Index 4 initialization***/
<#if CONFIG_DRV_SPI_USE_DMA_IDX4 == true>
    sysObj.spiTxDmaIdx4 = SYS_DMA_ChannelAllocate(DRV_SPI_TX_DMA_CHANNEL_IDX4);
    sysObj.spiRxDmaIdx4 = SYS_DMA_ChannelAllocate(DRV_SPI_RX_DMA_CHANNEL_IDX4);
    SYS_DMA_ChannelSetup(sysObj.spiTxDmaIdx4, DRV_SPI_TX_DMA_CHANNEL_PRIORITY_IDX4, DRV_SPI_TX_DMA_CHANNEL_OP_MODE_IDX4 , DRV_SPI_TX_DMA_TRIGGER_IDX4);
    SYS_DMA_ChannelSetup(sysObj.spiRxDmaIdx4, DRV_SPI_RX_DMA_CHANNEL_PRIORITY_IDX4, DRV_SPI_RX_DMA_CHANNEL_OP_MODE_IDX4 , DRV_SPI_RX_DMA_TRIGGER_IDX4);
    drvSpi4InitData.txDmaChannelHandle = sysObj.spiTxDmaIdx4;
    drvSpi4InitData.rxDmaChannelHandle = sysObj.spiRxDmaIdx4;
</#if>
<#if CONFIG_DRV_SPI_TASK_MODE_ISR_IDX4 == true>
<#if CONFIG_PIC32MX == true>
    SYS_INT_VectorPrioritySet(DRV_SPI_INT_VECTOR_IDX4, DRV_SPI_INT_PRIORITY_IDX4);
    SYS_INT_VectorSubprioritySet(DRV_SPI_INT_VECTOR_IDX4, DRV_SPI_INT_SUB_PRIORITY_IDX4);
<#else>
    SYS_INT_VectorPrioritySet(DRV_SPI_TX_INT_VECTOR_IDX4, DRV_SPI_TX_INT_PRIORITY_IDX4);
    SYS_INT_VectorSubprioritySet(DRV_SPI_TX_INT_VECTOR_IDX4, DRV_SPI_TX_INT_SUB_PRIORITY_IDX4);
    SYS_INT_VectorPrioritySet(DRV_SPI_RX_INT_VECTOR_IDX4, DRV_SPI_RX_INT_PRIORITY_IDX4);
    SYS_INT_VectorSubprioritySet(DRV_SPI_RX_INT_VECTOR_IDX4, DRV_SPI_RX_INT_SUB_PRIORITY_IDX4);
    SYS_INT_VectorPrioritySet(DRV_DRV_SPI_ERROR_INT_VECTOR_IDX4, DRV_SPI_ERROR_INT_PRIORITY_IDX4);
    SYS_INT_VectorSubprioritySet(DRV_DRV_SPI_ERROR_INT_VECTOR_IDX4, DRV_SPI_ERROR_INT_SUB_PRIORITY_IDX4);
</#if>
</#if>
    sysObj.spiObjectIdx4 = DRV_SPI_Initialize(4, (const SYS_MODULE_INIT  * const)&drvSpi4InitData);
</#if>
<#if CONFIG_DRV_SPI_IDX5 == true>

    /*** SPI Driver Index 5 initialization***/
<#if CONFIG_DRV_SPI_USE_DMA_IDX5 == true>
    sysObj.spiTxDmaIdx5 = SYS_DMA_ChannelAllocate(DRV_SPI_TX_DMA_CHANNEL_IDX5);
    sysObj.spiRxDmaIdx5 = SYS_DMA_ChannelAllocate(DRV_SPI_RX_DMA_CHANNEL_IDX5);
    SYS_DMA_ChannelSetup(sysObj.spiTxDmaIdx5, DRV_SPI_TX_DMA_CHANNEL_PRIORITY_IDX5, DRV_SPI_TX_DMA_CHANNEL_OP_MODE_IDX5 , DRV_SPI_TX_DMA_TRIGGER_IDX5);
    SYS_DMA_ChannelSetup(sysObj.spiRxDmaIdx5, DRV_SPI_RX_DMA_CHANNEL_PRIORITY_IDX5, DRV_SPI_RX_DMA_CHANNEL_OP_MODE_IDX5 , DRV_SPI_RX_DMA_TRIGGER_IDX5);
    drvSpi5InitData.txDmaChannelHandle = sysObj.spiTxDmaIdx5;
    drvSpi5InitData.rxDmaChannelHandle = sysObj.spiRxDmaIdx5;
</#if>
<#if CONFIG_DRV_SPI_TASK_MODE_ISR_IDX5 == true>

<#if CONFIG_PIC32MX == true>
    SYS_INT_VectorPrioritySet(DRV_SPI_INT_VECTOR_IDX5, DRV_SPI_INT_PRIORITY_IDX5);
    SYS_INT_VectorSubprioritySet(DRV_SPI_INT_VECTOR_IDX5, DRV_SPI_INT_SUB_PRIORITY_IDX5);
<#else>
    SYS_INT_VectorPrioritySet(DRV_SPI_TX_INT_VECTOR_IDX5, DRV_SPI_TX_INT_PRIORITY_IDX5);
    SYS_INT_VectorSubprioritySet(DRV_SPI_TX_INT_VECTOR_IDX5, DRV_SPI_TX_INT_SUB_PRIORITY_IDX5);
    SYS_INT_VectorPrioritySet(DRV_SPI_RX_INT_VECTOR_IDX5, DRV_SPI_RX_INT_PRIORITY_IDX5);
    SYS_INT_VectorSubprioritySet(DRV_SPI_RX_INT_VECTOR_IDX5, DRV_SPI_RX_INT_SUB_PRIORITY_IDX5);
    SYS_INT_VectorPrioritySet(DRV_DRV_SPI_ERROR_INT_VECTOR_IDX5, DRV_SPI_ERROR_INT_PRIORITY_IDX5);
    SYS_INT_VectorSubprioritySet(DRV_DRV_SPI_ERROR_INT_VECTOR_IDX5, DRV_SPI_ERROR_INT_SUB_PRIORITY_IDX5);
</#if>
</#if>
    sysObj.spiObjectIdx5 = DRV_SPI_Initialize(5, (const SYS_MODULE_INIT  * const)&drvSpi5InitData);
</#if>