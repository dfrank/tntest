/*******************************************************************************
  USART Driver Functions for Static Single Instance Driver

  Company:
    Microchip Technology Inc.

  File Name:
    drv_usart_static.c

  Summary:
    USART driver impementation for the static single instance driver.

  Description:
    The USART device driver provides a simple interface to manage the USART
    modules on Microchip microcontrollers. This file contains implemenation
    for the USART driver.
    
  Remarks:
    Static interfaces incorporate the driver instance number within the names
    of the routines, eliminating the need for an object ID or object handle.
    
    Static single-open interfaces also eliminate the need for the open handle.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

#ifndef _DRV_USART_STATIC_H
#define _DRV_USART_STATIC_H

#include "peripheral/usart/plib_usart.h"

<#macro DRV_USART_STATIC_FUNCTIONS DRV_INSTANCE USART_INSTANCE>
// *****************************************************************************
// *****************************************************************************
// Section: Instance ${DRV_INSTANCE} static driver functions
// *****************************************************************************
// *****************************************************************************

bool DRV_USART${DRV_INSTANCE}_ReceiverBufferIsEmpty(void)
{
   return (!PLIB_USART_ReceiverDataIsAvailable(${USART_INSTANCE}));
}

uint8_t DRV_USART${DRV_INSTANCE}_ReadByte(void)
{
   if(PLIB_USART_ReceiverOverrunHasOccurred(${USART_INSTANCE}))
   {
      PLIB_USART_ReceiverOverrunErrorClear(${USART_INSTANCE});
   }

   return (PLIB_USART_ReceiverByteReceive(${USART_INSTANCE}));
}

void DRV_USART${DRV_INSTANCE}_WriteByte(const uint8_t byte)
{
   while(PLIB_USART_TransmitterBufferIsFull(${USART_INSTANCE}))
   {
   }

   PLIB_USART_TransmitterByteSend(${USART_INSTANCE}, byte);
}

</#macro>

<#if CONFIG_DRV_USART_INST_IDX0 == true>
<@DRV_USART_STATIC_FUNCTIONS DRV_INSTANCE="0" USART_INSTANCE=CONFIG_DRV_USART_PERIPHERAL_ID_IDX0 />
</#if>
<#if CONFIG_DRV_USART_INST_IDX1 == true>
<@DRV_USART_STATIC_FUNCTIONS DRV_INSTANCE="1" USART_INSTANCE=CONFIG_DRV_USART_PERIPHERAL_ID_IDX1 />
</#if>
<#if CONFIG_DRV_USART_INST_IDX2 == true>
<@DRV_USART_STATIC_FUNCTIONS DRV_INSTANCE="2" USART_INSTANCE=CONFIG_DRV_USART_PERIPHERAL_ID_IDX2 />
</#if>
<#if CONFIG_DRV_USART_INST_IDX3 == true>
<@DRV_USART_STATIC_FUNCTIONS DRV_INSTANCE="3" USART_INSTANCE=CONFIG_DRV_USART_PERIPHERAL_ID_IDX3 />
</#if>
<#if CONFIG_DRV_USART_INST_IDX4 == true>
<@DRV_USART_STATIC_FUNCTIONS DRV_INSTANCE="4" USART_INSTANCE=CONFIG_DRV_USART_PERIPHERAL_ID_IDX4 />
</#if>
<#if CONFIG_DRV_USART_INST_IDX5 == true>
<@DRV_USART_STATIC_FUNCTIONS DRV_INSTANCE="5" USART_INSTANCE=CONFIG_DRV_USART_PERIPHERAL_ID_IDX5 />
</#if>
#endif // #ifndef _DRV_USART_STATIC_H

/*******************************************************************************
 End of File
*/
