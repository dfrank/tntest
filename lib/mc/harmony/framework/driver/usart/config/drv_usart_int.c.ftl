<#--
/*******************************************************************************
  USART Driver Interrupt Handler Template File

  File Name:
    drv_usart_int.c

  Summary:
    This file contains source code necessary to initialize the system.

  Description:
    This file contains source code necessary to initialize the system.  It
    implements the "SYS_Initialize" function, configuration bits, and allocates
    any necessary global system resources, such as the systemObjects structure
    that contains the object handles to all the MPLAB Harmony module objects in
    the system.
 *******************************************************************************/

/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
 -->
<#if CONFIG_DRV_USART_INTERRUPT_MODE == true>
<#if CONFIG_DRV_USART_INST_IDX0 == true>
<#if CONFIG_PIC32MX == true>
void __ISR(${CONFIG_DRV_USART_ISR_VECTOR_IDX0}, ipl${CONFIG_DRV_USART_INT_PRIO_NUM_IDX0}) _IntHandlerDrvUsartInstance0(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksTransmit(sysObj.drvUsart0);
    DRV_USART_TasksReceive(sysObj.drvUsart0);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_XMIT_INT_SRC_IDX0});
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_RCV_INT_SRC_IDX0});
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_ERR_INT_SRC_IDX0});
</#if>
}
</#if> <#-- CONFIG_PIC32MX -->
<#if CONFIG_PIC32MZ == true>
void __ISR(${CONFIG_DRV_USART_XMIT_ISR_VECTOR_IDX0}, ipl${CONFIG_DRV_USART_XMIT_INT_PRIO_NUM_IDX0}) _IntHandlerDrvUsartTransmitInstance0(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksTransmit(sysObj.drvUsart0);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_XMIT_INT_SRC_IDX0});
</#if>
}
void __ISR(${CONFIG_DRV_USART_RCV_ISR_VECTOR_IDX0}, ipl${CONFIG_DRV_USART_RCV_INT_PRIO_NUM_IDX0}) _IntHandlerDrvUsartReceiveInstance0(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksReceive(sysObj.drvUsart0);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_RCV_INT_SRC_IDX0});
</#if>
}
void __ISR(${CONFIG_DRV_USART_ERR_ISR_VECTOR_IDX0}, ipl${CONFIG_DRV_USART_ERR_INT_PRIO_NUM_IDX0}) _IntHandlerDrvUsartErrorInstance0(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    SYS_ASSERT(false, "USART Driver Instance 0 Error");
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_ERR_INT_SRC_IDX0});
</#if>
}
</#if> <#-- CONFIG_PIC32MZ -->
</#if> <#-- CONFIG_DRV_USART_INST_IDX0 -->

<#if CONFIG_DRV_USART_INST_IDX1 == true>
<#if CONFIG_PIC32MX == true>
void __ISR(${CONFIG_DRV_USART_ISR_VECTOR_IDX1}, ipl${CONFIG_DRV_USART_INT_PRIO_NUM_IDX1}) _IntHandlerDrvUsartInstance1(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksTransmit(sysObj.drvUsart1);
    DRV_USART_TasksReceive(sysObj.drvUsart1);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_XMIT_INT_SRC_IDX1});
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_RCV_INT_SRC_IDX1});
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_ERR_INT_SRC_IDX1});
</#if>
}
</#if> <#-- CONFIG_PIC32MX -->
<#if CONFIG_PIC32MZ == true>
void __ISR(${CONFIG_DRV_USART_XMIT_ISR_VECTOR_IDX1}, ipl${CONFIG_DRV_USART_XMIT_INT_PRIO_NUM_IDX1}) _IntHandlerDrvUsartTransmitInstance1(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksTransmit(sysObj.drvUsart1);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_XMIT_INT_SRC_IDX1});
</#if>
}
void __ISR(${CONFIG_DRV_USART_RCV_ISR_VECTOR_IDX1}, ipl${CONFIG_DRV_USART_RCV_INT_PRIO_NUM_IDX1}) _IntHandlerDrvUsartReceiveInstance1(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksReceive(sysObj.drvUsart1);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_RCV_INT_SRC_IDX1});
</#if>
}
void __ISR(${CONFIG_DRV_USART_ERR_ISR_VECTOR_IDX1}, ipl${CONFIG_DRV_USART_ERR_INT_PRIO_NUM_IDX1}) _IntHandlerDrvUsartErrorInstance1(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    SYS_ASSERT(false, "USART Driver Instance 1 Error");
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_ERR_INT_SRC_IDX1});
</#if>
}
</#if> <#-- CONFIG_PIC32MZ -->
</#if> <#-- CONFIG_DRV_USART_INST_IDX1 -->
<#if CONFIG_DRV_USART_INST_IDX2 == true>
<#if CONFIG_PIC32MX == true>
void __ISR(${CONFIG_DRV_USART_ISR_VECTOR_IDX2}, ipl${CONFIG_DRV_USART_INT_PRIO_NUM_IDX2}) _IntHandlerDrvUsartInstance2(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksTransmit(sysObj.drvUsart2);
    DRV_USART_TasksReceive(sysObj.drvUsart2);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_XMIT_INT_SRC_IDX2});
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_RCV_INT_SRC_IDX2});
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_ERR_INT_SRC_IDX2});
</#if>
}
</#if> <#-- CONFIG_PIC32MX -->
<#if CONFIG_PIC32MZ == true>
void __ISR(${CONFIG_DRV_USART_XMIT_ISR_VECTOR_IDX2}, ipl${CONFIG_DRV_USART_XMIT_INT_PRIO_NUM_IDX2}) _IntHandlerDrvUsartTransmitInstance2(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksTransmit(sysObj.drvUsart2);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_XMIT_INT_SRC_IDX2});
</#if>
}
void __ISR(${CONFIG_DRV_USART_RCV_ISR_VECTOR_IDX2}, ipl${CONFIG_DRV_USART_RCV_INT_PRIO_NUM_IDX2}) _IntHandlerDrvUsartReceiveInstance2(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksReceive(sysObj.drvUsart2);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_RCV_INT_SRC_IDX2});
</#if>
}
void __ISR(${CONFIG_DRV_USART_ERR_ISR_VECTOR_IDX2}, ipl${CONFIG_DRV_USART_ERR_INT_PRIO_NUM_IDX2}) _IntHandlerDrvUsartErrorInstance2(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    SYS_ASSERT(false, "USART Driver Instance 0 Error");
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_ERR_INT_SRC_IDX2});
</#if>
}
</#if> <#-- CONFIG_PIC32MZ -->
</#if> <#-- CONFIG_DRV_USART_INST_IDX2 -->

<#if CONFIG_DRV_USART_INST_IDX3 == true>
<#if CONFIG_PIC32MX == true>
void __ISR(${CONFIG_DRV_USART_ISR_VECTOR_IDX3}, ipl${CONFIG_DRV_USART_INT_PRIO_NUM_IDX3}) _IntHandlerDrvUsartInstance3(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksTransmit(sysObj.drvUsart3);
    DRV_USART_TasksReceive(sysObj.drvUsart3);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_XMIT_INT_SRC_IDX3});
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_RCV_INT_SRC_IDX3});
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_ERR_INT_SRC_IDX3});
</#if>
}
</#if> <#-- CONFIG_PIC32MX -->
<#if CONFIG_PIC32MZ == true>
void __ISR(${CONFIG_DRV_USART_XMIT_ISR_VECTOR_IDX3}, ipl${CONFIG_DRV_USART_XMIT_INT_PRIO_NUM_IDX3}) _IntHandlerDrvUsartTransmitInstance3(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksTransmit(sysObj.drvUsart3);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_XMIT_INT_SRC_IDX3});
</#if>
}
void __ISR(${CONFIG_DRV_USART_RCV_ISR_VECTOR_IDX3}, ipl${CONFIG_DRV_USART_RCV_INT_PRIO_NUM_IDX3}) _IntHandlerDrvUsartReceiveInstance3(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksReceive(sysObj.drvUsart3);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_RCV_INT_SRC_IDX3});
</#if>
}
void __ISR(${CONFIG_DRV_USART_ERR_ISR_VECTOR_IDX3}, ipl${CONFIG_DRV_USART_ERR_INT_PRIO_NUM_IDX3}) _IntHandlerDrvUsartErrorInstance3(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    SYS_ASSERT(false, "USART Driver Instance 0 Error");
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_ERR_INT_SRC_IDX3});
</#if>
}
</#if> <#-- CONFIG_PIC32MZ -->
</#if> <#-- CONFIG_DRV_USART_INST_IDX3 -->

<#if CONFIG_DRV_USART_INST_IDX4 == true>
<#if CONFIG_PIC32MX == true>
void __ISR(${CONFIG_DRV_USART_ISR_VECTOR_IDX4}, ipl${CONFIG_DRV_USART_INT_PRIO_NUM_IDX4}) _IntHandlerDrvUsartInstance4(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksTransmit(sysObj.drvUsart4);
    DRV_USART_TasksReceive(sysObj.drvUsart4);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_XMIT_INT_SRC_IDX4});
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_RCV_INT_SRC_IDX4});
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_ERR_INT_SRC_IDX4});
</#if>
}
</#if> <#-- CONFIG_PIC32MX -->
<#if CONFIG_PIC32MZ == true>
void __ISR(${CONFIG_DRV_USART_XMIT_ISR_VECTOR_IDX4}, ipl${CONFIG_DRV_USART_XMIT_INT_PRIO_NUM_IDX4}) _IntHandlerDrvUsartTransmitInstance4(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksTransmit(sysObj.drvUsart4);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_XMIT_INT_SRC_IDX4});
</#if>
}
void __ISR(${CONFIG_DRV_USART_RCV_ISR_VECTOR_IDX4}, ipl${CONFIG_DRV_USART_RCV_INT_PRIO_NUM_IDX4}) _IntHandlerDrvUsartReceiveInstance4(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksReceive(sysObj.drvUsart4);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_RCV_INT_SRC_IDX4});
</#if>
}
void __ISR(${CONFIG_DRV_USART_ERR_ISR_VECTOR_IDX4}, ipl${CONFIG_DRV_USART_ERR_INT_PRIO_NUM_IDX4}) _IntHandlerDrvUsartErrorInstance4(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    SYS_ASSERT(false, "USART Driver Instance 0 Error");
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_ERR_INT_SRC_IDX4});
</#if>
}
</#if> <#-- CONFIG_PIC32MZ -->
</#if> <#-- CONFIG_DRV_USART_INST_IDX4 -->
<#if CONFIG_DRV_USART_INST_IDX5 == true>
<#if CONFIG_PIC32MX == true>
void __ISR(${CONFIG_DRV_USART_ISR_VECTOR_IDX5}, ipl${CONFIG_DRV_USART_INT_PRIO_NUM_IDX5}) _IntHandlerDrvUsartInstance5(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksTransmit(sysObj.drvUsart5);
    DRV_USART_TasksReceive(sysObj.drvUsart5);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_XMIT_INT_SRC_IDX5});
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_RCV_INT_SRC_IDX5});
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_ERR_INT_SRC_IDX5});
</#if>
}
</#if> <#-- CONFIG_PIC32MX -->
<#if CONFIG_PIC32MZ == true>
void __ISR(${CONFIG_DRV_USART_XMIT_ISR_VECTOR_IDX5}, ipl${CONFIG_DRV_USART_XMIT_INT_PRIO_NUM_IDX5}) _IntHandlerDrvUsartTransmitInstance5(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksTransmit(sysObj.drvUsart5);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_XMIT_INT_SRC_IDX5});
</#if>
}
void __ISR(${CONFIG_DRV_USART_RCV_ISR_VECTOR_IDX5}, ipl${CONFIG_DRV_USART_RCV_INT_PRIO_NUM_IDX5}) _IntHandlerDrvUsartReceiveInstance5(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    DRV_USART_TasksReceive(sysObj.drvUsart5);
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_RCV_INT_SRC_IDX5});
</#if>
}
void __ISR(${CONFIG_DRV_USART_ERR_ISR_VECTOR_IDX5}, ipl${CONFIG_DRV_USART_ERR_INT_PRIO_NUM_IDX5}) _IntHandlerDrvUsartErrorInstance5(void)
{
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
    SYS_ASSERT(false, "USART Driver Instance 0 Error");
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">

    /* TODO: Add code to process interrupt here */

    /* Clear pending interrupt */
    PLIB_INT_SourceFlagClear(INT_ID_0, ${CONFIG_DRV_USART_ERR_INT_SRC_IDX5});
</#if>
}
</#if> <#-- CONFIG_PIC32MZ -->
</#if> <#-- CONFIG_DRV_USART_INST_IDX5 -->
</#if> <#-- CONFIG_DRV_USART_INTERRUPT_MODE -->
<#--
/*******************************************************************************
 End of File
*/
-->
