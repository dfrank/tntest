/*******************************************************************************
  System Initialization File

  File Name:
    system_init.c

  Summary:
    This file contains source code necessary to initialize the system.

  Description:
    This file contains source code necessary to initialize the system.  It
    implements the "SYS_Initialize" function, configuration bits, and allocates
    any necessary global system resources, such as the systemObjects structure
    that contains the object handles to all the MPLAB Harmony module objects in
    the system.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "system_config.h"
#include "app.h"
#include "system_definitions.h"


// ****************************************************************************
// ****************************************************************************
// Section: Configuration Bits
// ****************************************************************************
// ****************************************************************************

/*** DEVCFG0 ***/

<#if CONFIG_DEBUG?has_content>
#pragma config DEBUG =      ${CONFIG_DEBUG}
</#if>
<#if CONFIG_JTAGEN?has_content>
#pragma config JTAGEN =     ${CONFIG_JTAGEN}
</#if>
<#if CONFIG_ICESEL?has_content>
#pragma config ICESEL =     ${CONFIG_ICESEL}
</#if>
<#if CONFIG_TRCEN?has_content>
#pragma config TRCEN =      ${CONFIG_TRCEN}
</#if>
<#if CONFIG_BOOTISA?has_content>
#pragma config BOOTISA =    ${CONFIG_BOOTISA}
</#if>
<#if CONFIG_FECCCON?has_content>
#pragma config FECCCON =    ${CONFIG_FECCCON}
</#if>
<#if CONFIG_FSLEEP?has_content>
#pragma config FSLEEP =     ${CONFIG_FSLEEP}
</#if>
<#if CONFIG_DBGPER?has_content>
#pragma config DBGPER =     ${CONFIG_DBGPER}
</#if>
<#if CONFIG_EJTAGBEN?has_content>
#pragma config EJTAGBEN =   ${CONFIG_EJTAGBEN}
</#if>
<#if CONFIG_PWP?has_content>
#pragma config PWP =        ${CONFIG_PWP}
</#if>
<#if CONFIG_BWP?has_content>
#pragma config BWP =        ${CONFIG_BWP}
</#if>
<#if CONFIG_CP?has_content>
#pragma config CP =         ${CONFIG_CP}
</#if>

/*** DEVCFG1 ***/

<#if CONFIG_FNOSC?has_content>
#pragma config FNOSC =      ${CONFIG_FNOSC}
</#if>
<#if CONFIG_DMTINTV?has_content>
#pragma config DMTINTV =    ${CONFIG_DMTINTV}
</#if>
<#if CONFIG_FSOSCEN?has_content>
#pragma config FSOSCEN =    ${CONFIG_FSOSCEN}
</#if>
<#if CONFIG_IESO?has_content>
#pragma config IESO =       ${CONFIG_IESO}
</#if>
<#if CONFIG_POSCMOD?has_content>
#pragma config POSCMOD =    ${CONFIG_POSCMOD}
</#if>
<#if CONFIG_OSCIOFNC?has_content>
#pragma config OSCIOFNC =   ${CONFIG_OSCIOFNC}
</#if>
<#if CONFIG_FPBDIV?has_content>
#pragma config FPBDIV =     ${CONFIG_FPBDIV}
</#if>
<#if CONFIG_FCKSM?has_content>
#pragma config FCKSM =      ${CONFIG_FCKSM}
</#if>
<#if CONFIG_WDTPS?has_content>
#pragma config WDTPS =      ${CONFIG_WDTPS}
</#if>
<#if CONFIG_WDTSPGM?has_content>
#pragma config WDTSPGM =    ${CONFIG_WDTSPGM}
</#if>
<#if CONFIG_FWDTEN?has_content>
#pragma config FWDTEN =     ${CONFIG_FWDTEN}
</#if>
<#if CONFIG_WINDIS?has_content>
#pragma config WINDIS =     ${CONFIG_WINDIS}
</#if>
<#if CONFIG_FWDTWINSZ?has_content>
#pragma config FWDTWINSZ =  ${CONFIG_FWDTWINSZ}
</#if>
<#if CONFIG_DMTCNT?has_content>
#pragma config DMTCNT =     ${CONFIG_DMTCNT}
</#if>
<#if CONFIG_FDMTEN?has_content>
#pragma config FDMTEN =     ${CONFIG_FDMTEN}
</#if>

/*** DEVCFG2 ***/

<#if CONFIG_FPLLIDIV?has_content>
#pragma config FPLLIDIV =   ${CONFIG_FPLLIDIV}
</#if>
<#if CONFIG_FPLLRNG?has_content>
#pragma config FPLLRNG =    ${CONFIG_FPLLRNG}
</#if>
<#if CONFIG_FPLLICLK?has_content>
#pragma config FPLLICLK =   ${CONFIG_FPLLICLK}
</#if>
<#if CONFIG_FPLLMULT?has_content>
#pragma config FPLLMULT =   ${CONFIG_FPLLMULT}
</#if>
<#if CONFIG_FPLLMUL?has_content>
#pragma config FPLLMUL =    ${CONFIG_FPLLMUL}
</#if>
<#if CONFIG_FPLLODIV?has_content>
#pragma config FPLLODIV =   ${CONFIG_FPLLODIV}
</#if>
<#if CONFIG_UPLLFSEL?has_content>
#pragma config UPLLFSEL =   ${CONFIG_UPLLFSEL}
</#if>
<#if CONFIG_UPLLIDIV?has_content>
#pragma config UPLLIDIV =   ${CONFIG_UPLLIDIV}
</#if>
<#if CONFIG_UPLLEN?has_content>
#pragma config UPLLEN =     ${CONFIG_UPLLEN}
</#if>

/*** DEVCFG3 ***/

<#if CONFIG_USERID?has_content>
#pragma config USERID =     ${CONFIG_USERID}
</#if>
<#if CONFIG_FSRSSEL?has_content>
#pragma config FSRSSEL =    ${CONFIG_FSRSSEL}
</#if>
<#if CONFIG_FMIIE?has_content>
#pragma config FMIIE =      ${CONFIG_FMIIE}
</#if>
<#if CONFIG_FMIIEN?has_content>
#pragma config FMIIEN =     ${CONFIG_FMIIEN}
</#if>
<#if CONFIG_FETHIO?has_content>
#pragma config FETHIO =     ${CONFIG_FETHIO}
</#if>
<#if CONFIG_PGL1WAY?has_content>
#pragma config PGL1WAY =    ${CONFIG_PGL1WAY}
</#if>
<#if CONFIG_PMDL1WAY?has_content>
#pragma config PMDL1WAY =   ${CONFIG_PMDL1WAY}
</#if>
<#if CONFIG_IOL1WAY?has_content>
#pragma config IOL1WAY =    ${CONFIG_IOL1WAY}
</#if>
<#if CONFIG_FCANIO?has_content>
#pragma config FCANIO =     ${CONFIG_FCANIO}
</#if>
<#if CONFIG_FUSBIDIO?has_content>
#pragma config FUSBIDIO =   ${CONFIG_FUSBIDIO}
</#if>
<#if CONFIG_FVBUSONIO?has_content>
#pragma config FVBUSONIO =  ${CONFIG_FVBUSONIO}
</#if>
<#if CONFIG_TSEQ?has_content>

/*** BF1SEQ0 ***/

#pragma config TSEQ =       ${CONFIG_TSEQ}
</#if>
<#if CONFIG_CSEQ?has_content>
#pragma config CSEQ =       ${CONFIG_CSEQ}
</#if>


// *****************************************************************************
// *****************************************************************************
// Section: Library/Stack Initialization Data
// *****************************************************************************
// *****************************************************************************/

<#if CONFIG_USE_USB_STACK == true>
<#include "/framework/usb/config/usb.c.ftl">
</#if>

<#if CONFIG_DRV_USB_HOST_SUPPORT == true>
<#include "/framework/usb/config/usb_host.c.ftl">
</#if>

// *****************************************************************************
// *****************************************************************************
// Section: Driver Initialization Data
// *****************************************************************************
// *****************************************************************************

<#if CONFIG_USE_DRV_TMR == true>
<#include "/framework/driver/tmr/config/drv_tmr.c.ftl">
</#if>
<#if CONFIG_USE_DRV_USART == true>
<#include "/framework/driver/usart/config/drv_usart.c.ftl">
</#if>
<#if CONFIG_USE_DRV_NVM == true>
<#if CONFIG_USE_TCPIP_STACK == true>
extern const uint8_t NVM_MEDIA_DATA[];
</#if>
<#include "/framework/driver/nvm/config/drv_nvm.c.ftl">
</#if>
<#if CONFIG_DRV_SPI_USE_DRIVER == true>
<#include "/framework/driver/spi/config/drv_spi.c.ftl">
</#if>
<#if CONFIG_USE_DRV_I2S == true>
<#include "framework/driver/i2s/config/drv_i2s.c.ftl">
</#if>
<#if CONFIG_USE_SYS_TMR == true>
<#include "/framework/system/tmr/config/sys_tmr.c.ftl">
</#if>
<#if CONFIG_USE_DRV_SDCARD == true>
<#include "/framework/driver/sdcard/config/drv_sdcard.c.ftl">
</#if>
<#if CONFIG_USE_DRV_PMP == true>
<#include "/framework/driver/pmp/config/pmp_init_data.c.ftl">
</#if>
<#if CONFIG_USE_DRV_GFX_LCC == true>
<#include "/framework/driver/gfx/controller/lcc/config/drv_gfx_lcc_init_data.c.ftl">
</#if>
<#if CONFIG_USE_DRV_GFX_SSD1926 == true>
<#include "/framework/driver/gfx/controller/ssd1926/config/drv_gfx_ssd1926_init_data.c.ftl">
</#if>
<#if CONFIG_USE_DRV_GFX_S1D13517 == true>
<#include "/framework/driver/gfx/controller/s1d13517/config/drv_gfx_s1d13517_init_data.c.ftl">
</#if>
<#if CONFIG_USE_DRV_GFX_TFT002 == true>
<#include "/framework/driver/gfx/controller/tft002/config/drv_gfx_tft002_init_data.c.ftl">
</#if>
<#if CONFIG_USE_DRV_GFX_OTM2201A == true>
<#include "/framework/driver/gfx/controller/otm2201a/config/drv_gfx_otm2201a_init_data.c.ftl">
</#if>

<#if CONFIG_USE_TCPIP_STACK == true>

// *****************************************************************************
// *****************************************************************************
// Section: TCPIP Data
// *****************************************************************************
// *****************************************************************************

<#include "/framework/tcpip/config/tcpip_stack.c.ftl">
</#if>

// *****************************************************************************
// *****************************************************************************
// Section: System Data
// *****************************************************************************
// *****************************************************************************

/* Structure to hold the object handles for the modules in the system. */
SYSTEM_OBJECTS sysObj;

// *****************************************************************************
// *****************************************************************************
// Section: Module Initialization Data
// *****************************************************************************
// *****************************************************************************

<#if CONFIG_USE_SYS_CLK == true>
<#include "/framework/system/clk/config/sys_clk.c.ftl">
</#if>
<#if CONFIG_USE_SYS_DEVCON == true>
<#include "/framework/system/devcon/config/sys_devcon.c.ftl">
</#if>
<#if CONFIG_USE_SYS_DMA == true>
<#include "/framework/system/dma/config/sys_dma.c.ftl">
</#if>
<#if CONFIG_USE_SYS_FS == true>
<#include "/framework/system/fs/config/sys_fs.c.ftl">
</#if>
<#if CONFIG_USE_SYS_MSG == true>
<#include "/framework/system/msg/config/sys_msg.c.ftl">
</#if>
<#if CONFIG_USE_SYS_DEBUG == true>
<#include "/framework/system/debug/config/sys_debug.c.ftl">
</#if>
<#if CONFIG_USE_SYS_CONSOLE == true>
<#include "/framework/system/console/config/sys_console.c.ftl">
</#if>

// *****************************************************************************
// *****************************************************************************
// Section: Static Initialization Functions
// *****************************************************************************
// *****************************************************************************

<#if CONFIG_USE_DRV_USART == true>
<#include "/framework/driver/usart/config/drv_usart_proto.c.ftl">
</#if>
<#if CONFIG_USE_DRV_IC == true>
<#include "/framework/driver/ic/config/drv_ic_static_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_OC == true>
<#include "/framework/driver/oc/config/drv_oc_static_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_EBI == true>
<#include "/framework/driver/ebi/config/drv_ebi_static_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_RTCC == true>
<#include "/framework/driver/rtcc/config/drv_rtcc_static_init.c.ftl">
</#if>
<#include "/framework/driver/tmr/config/drv_tmr_static_init.c.ftl">

// *****************************************************************************
// *****************************************************************************
// Section: System Initialization
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void SYS_Initialize ( SYS_INIT_DATA *data )

  Summary:
    Initializes the board, services, drivers, application and other modules.

  Remarks:
    See prototype in system/common/sys_module.h.
 */

void SYS_Initialize ( void* data )
{
    /* Core Processor Initialization */
<#if CONFIG_USE_SYS_CLK == true>
    SYS_CLK_Initialize(&sysClkInit);
</#if>
<#if CONFIG_USE_SYS_DEVCON == true>
    sysObj.sysDevcon = SYS_DEVCON_Initialize(SYS_DEVCON_INDEX_0, (SYS_MODULE_INIT*)&sysDevconInit);
    SYS_DEVCON_PerformanceConfig(SYS_DEVCON_SYSTEM_CLOCK);
</#if>
<#if CONFIG_USE_BSP == true>

    /* Board Support Package Initialization */
    BSP_Initialize();
</#if>

    /* System Services Initialization */    
<#if CONFIG_USE_SYS_INT == true>
    SYS_INT_Initialize();  
</#if>

    /* Initialize Drivers */
<#if CONFIG_USE_DRV_TMR == true>
<#include "/framework/driver/tmr/config/drv_tmr_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_USART == true>
<#include "/framework/driver/usart/config/drv_usart_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_EBI == true>
<#include "/framework/driver/ebi/config/drv_ebi_init.c.ftl">
</#if>
<#if CONFIG_DRV_SPI_USE_DRIVER == true>
<#include "/framework/driver/spi/config/drv_spi_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_NVM == true>
<#include "/framework/driver/nvm/config/drv_nvm_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_SDCARD == true>
    sysObj.drvSDCard = DRV_SDCARD_Initialize(DRV_SDCARD_INDEX_0,(SYS_MODULE_INIT *)&drvSDCardInit);
</#if>
<#if CONFIG_USE_DRV_I2S == true>
<#include "/framework/driver/i2s/config/drv_i2s_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_RTCC == true>
<#include "/framework/driver/rtcc/config/drv_rtcc_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_IC == true>
<#include "/framework/driver/ic/config/drv_ic_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_OC == true>
<#include "/framework/driver/oc/config/drv_oc_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_PMP == true>
<#include "/framework/driver/pmp/config/drv_pmp_init.c.ftl">
</#if>
<#if CONFIG_USE_GFX_STACK == false>
<#if CONFIG_USE_DRV_GFX_LCC == true>
<#include "/framework/driver/gfx/controller/lcc/config/drv_gfx_lcc_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_GFX_SSD1926 == true>
<#include "/framework/driver/gfx/controller/ssd1926/config/drv_gfx_ssd1926_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_GFX_S1D13517 == true>
<#include "/framework/driver/gfx/controller/s1d13517/config/drv_gfx_s1d13517_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_GFX_TFT002 == true>
<#include "/framework/driver/gfx/controller/tft002/config/drv_gfx_tft002_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_GFX_OTM2201A == true>
<#include "/framework/driver/gfx/controller/otm2201a/config/drv_gfx_otm2201a_init.c.ftl">
</#if>
</#if>

    /* Initialize System Services */
<#if CONFIG_SYS_MSG_INST_IDX0 == true>
    msg0Init.nQSizes = queuePriorities0;
    sysObj.sysMsg0 = SYS_MSG_Initialize(SYS_MSG_0, (SYS_OBJ_HANDLE)&msg0Init);
</#if>
<#if CONFIG_SYS_MSG_INST_IDX1 == true>
    msg1Init.nQSizes = queuePriorities1;
    sysObj.sysMsg1 = SYS_MSG_Initialize(SYS_MSG_1, (SYS_OBJ_HANDLE)&msg1Init);
</#if>
<#if CONFIG_SYS_MSG_INST_IDX2 == true>
    msg2Init.nQSizes = queuePriorities2;
    sysObj.sysMsg2 = SYS_MSG_Initialize(SYS_MSG_2, (SYS_OBJ_HANDLE)&msg2Init);
</#if>
<#if CONFIG_SYS_MSG_INST_IDX3 == true>
    msg3Init.nQSizes = queuePriorities3;
    sysObj.sysMsg3 = SYS_MSG_Initialize(SYS_MSG_3, (SYS_OBJ_HANDLE)&msg3Init);
</#if>
<#if CONFIG_SYS_MSG_INST_IDX4 == true>
    msg4Init.nQSizes = queuePriorities4;
    sysObj.sysMsg4 = SYS_MSG_Initialize(SYS_MSG_4, (SYS_OBJ_HANDLE)&msg4Init);
</#if>
<#if CONFIG_USE_SYS_CONSOLE == true>
<#include "/framework/system/console/config/sys_console_init.c.ftl">
</#if>
<#if CONFIG_USE_SYS_DEBUG == true>
    sysObj.sysDebug = SYS_DEBUG_Initialize(SYS_DEBUG_INDEX_0, (SYS_MODULE_INIT*)&debugInit);
</#if>
<#if CONFIG_USE_SYS_DMA == true>
<#include "/framework/system/dma/config/sys_dma_init.c.ftl">
</#if>
<#if CONFIG_USE_SYS_TMR == true>
<#include "/framework/system/tmr/config/sys_tmr_init.c.ftl">
</#if>
<#if CONFIG_USE_SYS_COMMAND == true>
    SYS_COMMAND_INIT();
</#if>
<#if CONFIG_USE_SYS_FS == true>
    SYS_FS_Initialize( (const void *) sysFSInit );
</#if>
<#if CONFIG_USE_SYS_RANDOM == true>
    SYS_RANDOM_Initialize(0, 0);
</#if>

    /* Initialize Middleware */
<#if CONFIG_USE_USB_STACK == true>
<#include "/framework/usb/config/usb_init.c.ftl">
</#if>
<#if CONFIG_DRV_USB_HOST_SUPPORT == true>
<#include "/framework/usb/config/usb_host_init.c.ftl">
</#if>
<#if CONFIG_USE_DRV_GFX_LCC == true>
           /* Setting priority for LCC Refresh ISR */
    SYS_INT_VectorPrioritySet(INT_VECTOR_DMA1,    INT_PRIORITY_LEVEL6);
</#if>
<#if CONFIG_USE_GFX_STACK == true>
<#include "/framework/gfx/config/gfx_init.c.ftl">
</#if>
<#if CONFIG_USE_TCPIP_STACK == true>
<#include "/framework/tcpip/config/tcpip_stack_init.c.ftl"> 
</#if>
<#if CONFIG_USE_SYS_INT == true>

    /* Enable Global Interrupts */
    SYS_INT_Enable();
</#if>

    /* Initialize the Application */
    APP_Initialize();
}

/*******************************************************************************
 End of File
*/

