/*******************************************************************************
 System Interrupts File

  File Name:
    system_int.c

  Summary:
    Raw ISR definitions.

  Description:
    This file contains a definitions of the raw ISRs required to support the
    interrupt sub-system.

  Summary:
    This file contains source code for the interrupt vector functions in the
    system.

  Description:
    This file contains source code for the interrupt vector functions in the
    system.  It implements the system and part specific vector "stub" functions
    from which the individual "Tasks" functions are called for any modules
    executing interrupt-driven in the MPLAB Harmony system.

  Remarks:
    This file requires access to the systemObjects global data structure that
    contains the object handles to all MPLAB Harmony module objects executing
    interrupt-driven in the system.  These handles are passed into the individual
    module "Tasks" functions to identify the instance of the module to maintain.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2011-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <xc.h>
#include <sys/attribs.h>
#include "app.h"
#include "system_definitions.h"


// *****************************************************************************
// *****************************************************************************
// Section: System Interrupt Vector Functions
// *****************************************************************************
// *****************************************************************************
<#if CONFIG_USE_DRV_TMR == true>
<#include "/framework/driver/tmr/config/drv_tmr_int.c.ftl">
</#if>
<#if CONFIG_USE_DRV_USART == true>
<#include "/framework/driver/usart/config/drv_usart_int.c.ftl">
</#if>
<#if CONFIG_DRV_SPI_USE_DRIVER == true>
<#include "/framework/driver/spi/config/drv_spi_int.c.ftl"> 
</#if>
<#if CONFIG_USE_DRV_NVM == true>
<#include "/framework/driver/nvm/config/drv_nvm_int.c.ftl">
</#if>
<#if CONFIG_USE_SYS_DMA == true> 
<#include "/framework/system/dma/config/sys_dma_int.c.ftl">
</#if>
<#if CONFIG_USE_DRV_I2S == true>
<#include "/framework/driver/i2s/config/drv_i2s_int.c.ftl">
</#if>
<#if CONFIG_USE_DRV_IC == true>
<#include "framework/driver/ic/config/drv_ic_int.c.ftl">
</#if> 
<#if CONFIG_USE_DRV_OC == true>
<#include "framework/driver/oc/config/drv_oc_int.c.ftl">
</#if>
<#if CONFIG_GFX_USE_TOUCHSCREEN == true>
<#if CONFIG_GFX_TOUCHSCREEN_TYPE == "Analog Resistive Touch">
<#include "framework/gfx/config/gfx_int.c.ftl">
</#if>
</#if>
<#if CONFIG_USE_DRV_GFX_LCC == true>
<#include "framework/driver/gfx/controller/lcc/config/drv_gfx_lcc_int.c.ftl">
</#if>
<#if CONFIG_USE_DRV_RTCC == true>
<#include "framework/driver/rtcc/config/drv_rtcc_int.c.ftl">
</#if>
<#if CONFIG_USE_USB_STACK == true>
<#include "framework/usb/config/usb_interrupt.c.ftl">
</#if>
 

<#if CONFIG_USE_TCPIP_STACK == true>
<#include "/framework/tcpip/config/tcpip_mac_int.c.ftl">
</#if>
 
/*******************************************************************************
 End of File
*/

