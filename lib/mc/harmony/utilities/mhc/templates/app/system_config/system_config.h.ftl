/*******************************************************************************
  MPLAB Harmony System Configuration Header

  File Name:
    system_config.h

  Summary:
    Build-time configuration header for the system defined by this MPLAB Harmony
    project.

  Description:
    An MPLAB Project may have multiple configurations.  This file defines the
    build-time options for a single configuration.

  Remarks:
    This configuration header must not define any prototypes or data
    definitions (or include any files that do).  It only provides macro
    definitions for build-time configuration options that are not instantiated
    until used by another MPLAB Harmony module or application.
    
    Created with MPLAB Harmony Version ${CONFIG_MPLAB_HARMONY_VERSION_STRING}
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

#ifndef _SYSTEM_CONFIG_H
#define _SYSTEM_CONFIG_H


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
/*  This section Includes other configuration headers necessary to completely
    define this configuration.
*/

<#if CONFIG_USE_BSP == true>
#include "bsp_config.h"
</#if>
<#if CONFIG_USE_SYS_CONSOLE == true>
#include "system/console/sys_console.h"
</#if>

// *****************************************************************************
// *****************************************************************************
// Section: System Service Configuration
// *****************************************************************************
// *****************************************************************************

<#if CONFIG_USE_SYS_CLK == true>
<#include "/framework/system/clk/config/sys_clk.h.ftl">
</#if>
<#if CONFIG_USE_SYS_COMMON == true>
<#include "/framework/system/common/config/sys_common.h.ftl">
</#if>
<#if CONFIG_USE_SYS_DEVCON == true>
<#include "/framework/system/devcon/config/sys_devcon.h.ftl">
</#if>
<#if CONFIG_USE_SYS_INT == true>
<#include "/framework/system/int/config/sys_int.h.ftl">
</#if>
<#if CONFIG_USE_SYS_PORTS == true>
<#include "/framework/system/ports/config/sys_ports.h.ftl">
</#if>
<#if CONFIG_USE_SYS_TMR == true>
<#include "/framework/system/tmr/config/sys_tmr.h.ftl">
</#if>
<#if CONFIG_USE_SYS_DMA == true>
<#include "/framework/system/dma/config/sys_dma.h.ftl">
</#if>
<#if CONFIG_USE_SYS_CONSOLE == true>
<#include "/framework/system/console/config/sys_console.h.ftl">
</#if>
<#if CONFIG_USE_SYS_COMMAND == true>
<#include "/framework/system/command/config/sys_command.h.ftl">
</#if>
<#if CONFIG_USE_SYS_MSG == true>
<#include "/framework/system/msg/config/sys_msg.h.ftl">
</#if>
<#if CONFIG_USE_SYS_DEBUG == true>
<#include "/framework/system/debug/config/sys_debug.h.ftl">
</#if>
<#if CONFIG_USE_SYS_WDT == true>
<#include "/framework/system/wdt/config/sys_wdt.h.ftl">
</#if>
<#if CONFIG_USE_SYS_FS == true>
<#include "/framework/system/fs/config/sys_fs.h.ftl">
</#if>
<#if CONFIG_USE_SYS_RANDOM == true>
<#include "/framework/system/random/config/sys_random.h.ftl">
</#if>

// *****************************************************************************
// *****************************************************************************
// Section: Driver Configuration
// *****************************************************************************
// *****************************************************************************

<#if CONFIG_USE_DRV_TMR == true>
<#include "/framework/driver/tmr/config/drv_tmr.h.ftl">
</#if>
<#if CONFIG_USE_DRV_USART == true>
<#include "/framework/driver/usart/config/drv_usart.h.ftl">
</#if>
<#if CONFIG_USE_DRV_NVM == true>
<#include "/framework/driver/nvm/config/drv_nvm.h.ftl">
</#if>
<#if CONFIG_USE_DRV_GFX_LCC == true>
<#include "/framework/driver/gfx/controller/lcc/config/drv_gfx_lcc_config.h.ftl">
</#if>
<#if CONFIG_USE_DRV_GFX_SSD1926 == true>
<#include "/framework/driver/gfx/controller/ssd1926/config/drv_gfx_ssd1926_config.h.ftl">
</#if>
<#if CONFIG_USE_DRV_GFX_S1D13517 == true>
<#include "/framework/driver/gfx/controller/s1d13517/config/drv_gfx_s1d13517_config.h.ftl">
</#if>
<#if CONFIG_USE_DRV_GFX_TFT002 == true>
<#include "/framework/driver/gfx/controller/tft002/config/drv_gfx_tft002_config.h.ftl">
</#if>
<#if CONFIG_USE_DRV_GFX_OTM2201A == true>
<#include "/framework/driver/gfx/controller/otm2201a/config/drv_gfx_otm2201a_config.h.ftl">
</#if>
<#if CONFIG_USE_DRV_PMP == true>
<#include "/framework/driver/pmp/config/drv_pmp.h.ftl">
</#if>
<#if CONFIG_USE_DRV_I2S == true>
<#include "/framework/driver/i2s/config/drv_i2s.h.ftl">
</#if>
<#if CONFIG_USE_DRV_SDCARD == true>
<#include "/framework/driver/sdcard/config/drv_sdcard.h.ftl">
</#if>
<#if CONFIG_DRV_SPI_USE_DRIVER == true>
<#include "/framework/driver/spi/config/drv_spi.h.ftl">
</#if>

// *****************************************************************************
// *****************************************************************************
// Section: Middleware & Other Library Configuration
// *****************************************************************************
// *****************************************************************************

<#if CONFIG_USE_GFX_STACK == true>
<#include "/framework/gfx/config/gfx.h.ftl">
</#if>
<#if CONFIG_USE_CRYPTO_LIB == true>
<#include "/framework/crypto/config/crypto.h.ftl">
</#if>
<#if CONFIG_USE_USB_STACK == true>
<#include "/framework/usb/config/usb.h.ftl">
</#if>
<#if CONFIG_USE_TCPIP_STACK == true>
<#include "/framework/tcpip/config/tcpip_stack.h.ftl">
</#if>
<#if CONFIG_USE_OSAL == "Yes (Required)">
<#include "/framework/osal/config/osal.ftl">
</#if>


#endif // _SYSTEM_CONFIG_H
/*******************************************************************************
 End of File
*/

