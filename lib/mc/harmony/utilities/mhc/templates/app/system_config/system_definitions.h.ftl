/*******************************************************************************
  System Definitions

  File Name:
    sys_definitions.h

  Summary:
    MPLAB Harmony project system definitions.

  Description:
    This file contains the system-wide prototypes and definitions for an MPLAB
    Harmony project.
 *******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
//DOM-IGNORE-END

#ifndef _SYS_DEFINITIONS_H
#define _SYS_DEFINITIONS_H


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>
<#if CONFIG_USE_SYS_CLK == true> 
#include "system/clk/sys_clk.h"
</#if>
<#if CONFIG_USE_SYS_DEVCON == true> 
#include "system/devcon/sys_devcon.h"
</#if>
<#if CONFIG_USE_SYS_INT == true>
#include "system/int/sys_int.h"
</#if>
<#if CONFIG_USE_SYS_DMA == true>
#include "system/dma/sys_dma.h"
</#if>
<#if CONFIG_USE_SYS_FS == true> 
#include "system/fs/sys_fs.h"
</#if>
<#if CONFIG_USE_SYS_CONSOLE == true>
#include "system/console/sys_console.h"
</#if>
<#if CONFIG_USE_SYS_RANDOM == true> 
#include "system/random/sys_random.h"
</#if>
<#if CONFIG_SYS_FS_MPFS == true> 
#include "system/fs/mpfs/mpfs.h"
</#if>
<#if CONFIG_SYS_FS_FAT == true> 
#include "system/fs/fat_fs/src/file_system/ff.h"
#include "system/fs/fat_fs/src/file_system/ffconf.h"
#include "system/fs/fat_fs/src/hardware_access/diskio.h"
</#if>
<#if CONFIG_USE_SYS_TMR == true> 
#include "system/tmr/sys_tmr.h"
</#if>
<#if CONFIG_USE_DRV_TMR == true>
<#if CONFIG_DRV_TMR_DRIVER_MODE == "DYNAMIC">
#include "driver/tmr/drv_tmr.h"
<#else>
#include "peripheral/tmr/plib_tmr.h"
<#if CONFIG_DRV_TMR_INTERRUPT_MODE == true>
#include "peripheral/int/plib_int.h"
</#if>
</#if>
</#if>
<#if CONFIG_USE_DRV_PMP == true>
#include "driver/pmp/drv_pmp.h"
</#if>
<#if CONFIG_USE_DRV_I2S == true>
#include "driver/i2s/drv_i2s.h"
</#if>
<#if CONFIG_USE_DRV_USART == true>
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
#include "driver/usart/drv_usart.h"
</#if>
<#if CONFIG_DRV_USART_DRIVER_MODE == "STATIC">
#include "peripheral/usart/plib_usart.h"
<#if CONFIG_DRV_USART_INTERRUPT_MODE == true>
#include "peripheral/int/plib_int.h"
</#if>
</#if>
</#if>
<#if CONFIG_USE_DRV_NVM == true>
#include "driver/nvm/drv_nvm.h"
<#if CONFIG_USE_DRV_NVM_MEDIA == true>
#include "driver/nvm/drv_nvm_media.h"
</#if>
</#if>
<#if CONFIG_USE_SYS_PORTS == true>
#include "system/ports/sys_ports.h"
</#if>
<#if CONFIG_USE_DRV_SDCARD == true>
#include "driver/sdcard/drv_sdcard.h"
</#if>
<#if CONFIG_USE_DRV_GFX_LCC == true>
#include "driver/gfx/controller/lcc/drv_gfx_lcc.h"
</#if>
<#if CONFIG_USE_DRV_GFX_SSD1926 == true>
#include "driver/gfx/controller/ssd1926/drv_gfx_ssd1926.h"
</#if>
<#if CONFIG_USE_DRV_GFX_S1D13517 == true>
#include "driver/gfx/controller/s1d13517/drv_gfx_s1d13517.h"
</#if>
<#if CONFIG_USE_DRV_GFX_TFT002 == true>
#include "driver/gfx/controller/tft002/drv_gfx_tft002.h"
</#if>
<#if CONFIG_USE_DRV_GFX_OTM2201A == true>
#include "driver/gfx/controller/otm2201a/drv_gfx_otm2201a.h"
</#if>
<#if CONFIG_USE_GFX_TRULY_32_240X320 == true>
    #include "driver/gfx/display/truly_3.2_240x320/drv_gfx_truly_3.2_240x320.h"
</#if>
<#if CONFIG_USE_GFX_POWERTIP_43_480X272 == true>
    #include "driver/gfx/display/powertip_4.3_480x272/drv_gfx_powertip_4.3_480x272.h"
</#if>
<#if CONFIG_USE_GFX_TRULY_57_640X480 == true>
    #include "driver/gfx/display/truly_5.7_640x480/drv_gfx_truly_5.7_640x480.h"
</#if>
<#if CONFIG_USE_GFX_TRULY_7_800X480 == true>
    #include "driver/gfx/display/truly_7_800x480/drv_gfx_truly_7_800x480.h"
</#if>
<#if CONFIG_USE_GFX_NEWHAVEN_43_480X272_PCAP == true>
    #include "driver/gfx/display/newhaven_4.3_480x272_PCAP/drv_gfx_newhaven_4.3_480x272_PCAP.h"
</#if>
<#if CONFIG_USE_GFX_NEWHAVEN_50_800X480_PCAP == true>
    #include "driver/gfx/display/newhaven_5.0_800x480_PCAP/drv_gfx_newhaven_5.0_800x480_PCAP.h"
</#if>
<#if CONFIG_DRV_SPI_USE_DRIVER>
#include "driver/spi/drv_spi.h"
</#if>
<#if CONFIG_USE_SYS_DEBUG == true> 
#include "system/debug/sys_debug.h"
</#if>
<#if CONFIG_DRV_USB_DEVICE_SUPPORT == true>
#include "usb/usb_device.h"
</#if>
<#if CONFIG_USB_DEVICE_USE_MSD == true>
#include "usb/usb_device_msd.h"
#include "usb/drv_nvm_block.h"
</#if>
<#if CONFIG_USB_DEVICE_USE_CDC == true>
#include "usb/usb_device_cdc.h"
</#if>
<#if CONFIG_USB_DEVICE_USE_HID == true>
#include "usb/usb_device_hid.h"
</#if>
<#if CONFIG_USB_DEVICE_USE_AUDIO == true>
#include "usb/usb_device_audio_v1_0.h"
</#if>

<#if CONFIG_DRV_USB_HOST_SUPPORT == true>
#include "usb/usb_host.h"
</#if>
<#if CONFIG_USB_HOST_USE_MSD == true>
#include "usb/usb_host_msd.h"
#include "usb/usb_host_scsi.h"
</#if>
<#if CONFIG_USB_HOST_USE_CDC == true>
#include "usb/usb_host_cdc.h"
</#if>

<#if CONFIG_USE_DRV_IC == true>
<#if CONFIG_DRV_IC_DRIVER_MODE == "STATIC">
#include "peripheral/ic/plib_ic.h"
<#if CONFIG_DRV_IC_INTERRUPT_MODE == true>
#include "peripheral/int/plib_int.h"
</#if>
</#if>
</#if>
<#if CONFIG_USE_DRV_OC == true>
<#if CONFIG_DRV_OC_DRIVER_MODE == "STATIC">
#include "peripheral/oc/plib_oc.h"
<#if CONFIG_DRV_OC_INTERRUPT_MODE == true>
#include "peripheral/int/plib_int.h"
</#if>
</#if>
</#if>
<#if CONFIG_USE_DRV_EBI == true>
<#if CONFIG_DRV_EBI_DRIVER_MODE == "STATIC">
#include "peripheral/ebi/plib_ebi.h"
</#if>
</#if>
<#if CONFIG_USE_DRV_RTCC == true>
<#if CONFIG_DRV_RTCC_DRIVER_MODE == "STATIC">
#include "peripheral/rtcc/plib_rtcc.h"
<#if CONFIG_DRV_RTCC_INTERRUPT_MODE == true>
#include "peripheral/int/plib_int.h"
</#if>
#include "peripheral/devcon/plib_devcon.h"
</#if>
</#if>
<#if CONFIG_USE_TCPIP_STACK == true>
#include "tcpip/tcpip.h"
#include "driver/ethmac/drv_ethmac.h"
</#if>
<#if CONFIG_USE_SYS_MSG == true>
#include "system/msg/sys_msg.h"
</#if>
// *****************************************************************************
// *****************************************************************************
// Section: Type Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* System Objects

  Summary:
    Structure holding the system's object handles

  Description:
    This structure contains the object handles for all objects in the
    MPLAB Harmony project's system configuration.

  Remarks:
    These handles are returned from the "Initialize" functions for each module
    and must be passed into the "Tasks" function for each module.
*/

typedef struct
{
<#if CONFIG_USE_SYS_DEVCON = true > 
    SYS_MODULE_OBJ  sysDevcon;
</#if>
<#if CONFIG_USE_SYS_TMR = true > 
    SYS_MODULE_OBJ  sysTmr;
</#if>
<#if CONFIG_USE_SYS_DMA = true >
    SYS_MODULE_OBJ  sysDma;
</#if>
<#if CONFIG_USE_DRV_TMR = true >
<#if CONFIG_DRV_TMR_INST_0 == true>
    SYS_MODULE_OBJ  drvTmr0;
</#if>
<#if CONFIG_DRV_TMR_INST_1 == true>
    SYS_MODULE_OBJ  drvTmr1;
</#if>
<#if CONFIG_DRV_TMR_INST_2 == true>
    SYS_MODULE_OBJ  drvTmr2;
</#if>
<#if CONFIG_DRV_TMR_INST_3 == true>
    SYS_MODULE_OBJ  drvTmr3;
</#if>
<#if CONFIG_DRV_TMR_INST_4 == true>
    SYS_MODULE_OBJ  drvTmr4;
</#if>
<#if CONFIG_DRV_TMR_INST_5 == true>
    SYS_MODULE_OBJ  drvTmr5;
</#if>
<#if CONFIG_DRV_TMR_INST_6 == true>
    SYS_MODULE_OBJ  drvTmr6;
</#if>
<#if CONFIG_DRV_TMR_INST_7 == true>
    SYS_MODULE_OBJ  drvTmr7;
</#if>
<#if CONFIG_DRV_TMR_INST_8 == true>
    SYS_MODULE_OBJ  drvTmr8;
</#if>
</#if>
<#if CONFIG_USE_DRV_I2S = true >
<#if CONFIG_DRV_I2S_INST_0 == true>
    SYS_MODULE_OBJ  drvI2S0;
</#if>
<#if CONFIG_DRV_I2S_INST_1 == true>
    SYS_MODULE_OBJ  drvI2S1;
</#if>
<#if CONFIG_DRV_I2S_INST_2 == true>
    SYS_MODULE_OBJ  drvI2S2;
</#if>
<#if CONFIG_DRV_I2S_INST_3 == true>
    SYS_MODULE_OBJ  drvI2S3;
</#if>
<#if CONFIG_DRV_I2S_INST_4 == true>
    SYS_MODULE_OBJ  drvI2S4;
</#if>
<#if CONFIG_DRV_I2S_INST_5 == true>
    SYS_MODULE_OBJ  drvI2S5;
</#if>
</#if>
<#if CONFIG_USE_DRV_USART == true>
<#if CONFIG_DRV_USART_DRIVER_MODE == "DYNAMIC">
<#if CONFIG_DRV_USART_INST_IDX0 == true>
    SYS_MODULE_OBJ  drvUsart0;
</#if>
<#if CONFIG_DRV_USART_INST_IDX1 == true>
    SYS_MODULE_OBJ  drvUsart1;
</#if>
<#if CONFIG_DRV_USART_INST_IDX2 == true>
    SYS_MODULE_OBJ  drvUsart2;
</#if>
<#if CONFIG_DRV_USART_INST_IDX3 == true>
    SYS_MODULE_OBJ  drvUsart3;
</#if>
<#if CONFIG_DRV_USART_INST_IDX4 == true>
    SYS_MODULE_OBJ  drvUsart4;
</#if>
<#if CONFIG_DRV_USART_INST_IDX5 == true>
    SYS_MODULE_OBJ  drvUsart5;
</#if>
</#if>
</#if>
<#if CONFIG_USE_DRV_NVM = true >
    SYS_MODULE_OBJ  drvNvm;
    SYS_MODULE_OBJ  drvNvmMedia;
</#if>
<#if CONFIG_USE_DRV_SDCARD = true >
    SYS_MODULE_OBJ  drvSDCard;
</#if>
<#if CONFIG_USE_SYS_DEBUG == true>
    SYS_MODULE_OBJ  sysDebug;
</#if>
<#if CONFIG_SYS_MSG_INST_IDX0 == true>
    SYS_MODULE_OBJ  sysMsg0;
</#if>
<#if CONFIG_SYS_MSG_INST_IDX1 == true>
    SYS_MODULE_OBJ  sysMsg1;
</#if>
<#if CONFIG_SYS_MSG_INST_IDX2 == true>
    SYS_MODULE_OBJ  sysMsg2;
</#if>
<#if CONFIG_SYS_MSG_INST_IDX3 == true>
    SYS_MODULE_OBJ  sysMsg3;
</#if>
<#if CONFIG_SYS_MSG_INST_IDX4 == true>
    SYS_MODULE_OBJ  sysMsg4;
</#if>
<#if CONFIG_SYS_CONSOLE_INST_IDX0 == true>
    SYS_MODULE_OBJ  sysConsole0;
</#if>
<#if CONFIG_SYS_CONSOLE_INST_IDX1 == true>
    SYS_MODULE_OBJ  sysConsole1;
</#if>
<#if CONFIG_DRV_SPI_USE_DRIVER == true>
<#include "/framework/driver/spi/config/drv_spi_sys_defs.h.ftl">
</#if>	 
<#if CONFIG_USE_USB_STACK == true>
<#include "/framework/usb/config/usb_sys_defs.h.ftl">
</#if>
<#if CONFIG_USE_GFX_STACK == true>
<#include "/framework/gfx/config/gfx_definitions.h.ftl">
</#if>
<#if CONFIG_USE_TCPIP_STACK == true>
    SYS_MODULE_OBJ  tcpip;
</#if>

} SYSTEM_OBJECTS;


// *****************************************************************************
// *****************************************************************************
// Section: extern declarations
// *****************************************************************************
// *****************************************************************************

extern SYSTEM_OBJECTS sysObj;
<#if CONFIG_USE_USB_STACK == true>
<#include "/framework/usb/config/usb_app.h.ftl">
</#if>

#endif /* _SYS_DEFINITIONS_H */
/*******************************************************************************
 End of File
*/

