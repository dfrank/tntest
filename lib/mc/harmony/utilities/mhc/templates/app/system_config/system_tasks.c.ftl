/*******************************************************************************
 System Tasks File

  File Name:
    system_tasks.c

  Summary:
    This file contains source code necessary to maintain system's polled state
    machines.

  Description:
    This file contains source code necessary to maintain system's polled state
    machines.  It implements the "SYS_Tasks" function that calls the individual
    "Tasks" functions for all the MPLAB Harmony modules in the system.

  Remarks:
    This file requires access to the systemObjects global data structure that
    contains the object handles to all MPLAB Harmony module objects executing
    polled in the system.  These handles are passed into the individual module
    "Tasks" functions to identify the instance of the module to maintain.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "system_config.h"
#include "system_definitions.h"
#include "app.h"


// *****************************************************************************
// *****************************************************************************
// Section: System "Tasks" Routine
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void SYS_Tasks ( void )

  Remarks:
    See prototype in system/common/sys_module.h.
*/

void SYS_Tasks ( void )
{
    /* Maintain the state machines of all library modules executing polled in
    the system. */
    
    /* Maintain system services */
    <#if CONFIG_USE_SYS_DEVCON == true>
    SYS_DEVCON_Tasks(sysObj.sysDevcon);
    </#if>
    <#if CONFIG_USE_SYS_DMA == true>
    <#if CONFIG_SYS_DMA_INTERRUPT_MODE_CH0 == false ||
         CONFIG_SYS_DMA_INTERRUPT_MODE_CH1 == false ||
         CONFIG_SYS_DMA_INTERRUPT_MODE_CH2 == false ||
         CONFIG_SYS_DMA_INTERRUPT_MODE_CH3 == false ||
         CONFIG_SYS_DMA_INTERRUPT_MODE_CH4 == false ||
         CONFIG_SYS_DMA_INTERRUPT_MODE_CH5 == false ||
         CONFIG_SYS_DMA_INTERRUPT_MODE_CH6 == false ||
         CONFIG_SYS_DMA_INTERRUPT_MODE_CH7 == false>
    SYS_DMA_Tasks(sysObj.sysDma);
    </#if>
    </#if>
    <#if CONFIG_USE_SYS_FS == true>
    SYS_FS_Tasks();
    </#if>
    <#if CONFIG_SYS_MSG_INST_IDX0 == true>
    SYS_MSG_Tasks( (SYS_OBJ_HANDLE) sysObj.sysMsg0 );
    </#if>
    <#if CONFIG_SYS_MSG_INST_IDX1 == true>
    SYS_MSG_Tasks( (SYS_OBJ_HANDLE) sysObj.sysMsg1 );
    </#if>
    <#if CONFIG_SYS_MSG_INST_IDX2 == true>
    SYS_MSG_Tasks( (SYS_OBJ_HANDLE) sysObj.sysMsg2 );
    </#if>
    <#if CONFIG_SYS_MSG_INST_IDX3 == true>
    SYS_MSG_Tasks( (SYS_OBJ_HANDLE) sysObj.sysMsg3 );
    </#if>
    <#if CONFIG_SYS_MSG_INST_IDX4 == true>
    SYS_MSG_Tasks( (SYS_OBJ_HANDLE) sysObj.sysMsg4 );
    </#if>
    <#if CONFIG_SYS_CONSOLE_INST_IDX0 == true>
    SYS_CONSOLE_Tasks(sysObj.sysConsole0);
    </#if>
    <#if CONFIG_SYS_CONSOLE_INST_IDX1 == true>
    SYS_CONSOLE_Tasks(sysObj.sysConsole1);
    </#if>
    <#if CONFIG_USE_SYS_COMMAND == true>
    SYS_COMMAND_TASK();
    </#if>    
    <#if CONFIG_USE_SYS_TMR == true>
    SYS_TMR_Tasks(sysObj.sysTmr);
    </#if>
    
    /* Maintain Device Drivers */
    <#if CONFIG_USE_DRV_TMR == true>
    <#include "/framework/driver/tmr/config/drv_tmr_tasks.c.ftl">
    </#if>
    <#if CONFIG_USE_DRV_USART == true>
    <#include "/framework/driver/usart/config/drv_usart_tasks.c.ftl">
    </#if>
    <#if CONFIG_DRV_SPI_USE_DRIVER == true>
    <#include "/framework/driver/spi/config/drv_spi_tasks.c.ftl">
    </#if>   
    <#if CONFIG_USE_DRV_SDCARD == true>
    <#include "/framework/driver/sdcard/config/drv_sdcard_tasks.c.ftl">    
    </#if>
    <#if CONFIG_USE_GFX_STACK == true>

    /* Maintain Graphics Library
    <#include "/framework/gfx/config/gfx_tasks.c.ftl">
    </#if>
    <#if CONFIG_USE_USB_STACK == true>
    
    /* Maintain USB Stack */
    <#include "/framework/usb/config/usb_tasks.c.ftl">
    </#if>
	
	<#if CONFIG_DRV_USB_HOST_SUPPORT == true>
    <#include "/framework/usb/config/usb_host_tasks.c.ftl">
	</#if>
	
    <#if CONFIG_USE_TCPIP_STACK == true>
    <#include "/framework/tcpip/config/tcpip_stack_tasks.c.ftl"> 
    </#if>
    
    /* Maintain the application's state machine. */
    APP_Tasks();
}


/*******************************************************************************
 End of File
 */

