/*******************************************************************************
  Board Support Package Implementation.

  Company:      
    Microchip Technology Inc.

  File Name:    
    bsp_sys_init.c

  Summary:      
    Board Support Package Implementation for Explorer 16 Development Board when
    used with a PIC32MX470F512L Plug In Module (PIM).

  Description:
    This file contains the implementation of the Board Support Package for the
    Explorer 16 Development Board when used with a PIC32MX470F512L PIM.
*******************************************************************************/
// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "bsp_config.h"


// *****************************************************************************
// *****************************************************************************
// *****************************************************************************
// Section: Interface Routines
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function: void BSP_Initialize(void)

  Summary:
    Performs the neccassary actions to initialize a board

  Description:
    This routine performs the neccassary actions to initialize a board

  Remarks:
    This routine performs direct register accesses, when the PORTS PLIB and
    system service become available, these register accesses will be be replaced
    by the PLIB\system service interfaces.

*/

void BSP_Initialize(void )
{

 // Start of code comment of old PIC32MX795F512L
    /* set the switch pins as input */
 /* PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 ,
                                      PORT_CHANNEL_D ,
                                      SWITCH_0 );

    PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 ,
                                      PORT_CHANNEL_D ,
                                      SWITCH_1 );

    PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 ,
                                      PORT_CHANNEL_A ,
                                      SWITCH_2 );

    PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_A, LED_1 );
    PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_A, LED_2 );
    PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_A, LED_3 );

    PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_A, LED_1 );
    PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_A, LED_2 );
    PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_A, LED_3 );

    // Enable pullups on the Switch ports

    PLIB_PORTS_ChangeNoticePullUpEnable(PORTS_ID_0, CN15);
    PLIB_PORTS_ChangeNoticePullUpEnable(PORTS_ID_0, CN16);
    PLIB_PORTS_ChangeNoticePullUpEnable(PORTS_ID_0, CN19);  */

  // End of code comment of old PIC32MX795F512L

            /*remap SPI pins */
    PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 ,                      //PIC32MX470F512L   Set SDI-1 as I/P
                                     PORT_CHANNEL_C ,                   //PIC32MX470F512L   RF5
                                     BSP_SDI_1 );

    PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 ,                      //PIC32MX470F512L   Set RB12 connected to U6 as I/P - floats HI on start-up
                                     PORT_CHANNEL_B ,
                                     BSP_SWITCH_12 );

    PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 ,                      //PIC32MX470F512L   Set RB13 connected to U6 as I/P - pulled HI externally
                                     PORT_CHANNEL_B ,
                                     BSP_SWITCH_13 );

    PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 ,                      //PIC32MX470F512L   Set RF0 Card Detect signal
                                     PORT_CHANNEL_F ,
                                     BSP_SWITCH_CD );

    PLIB_PORTS_PinDirectionOutputSet ( PORTS_ID_0 ,                      //PIC32MX470F512L  Set RF1 Write Protect signal
                                     PORT_CHANNEL_F ,
                                     BSP_SWITCH_WP );

    PLIB_PORTS_RemapInput(PORTS_ID_0,INPUT_FUNC_SDI1,INPUT_PIN_RPC4);         //PIC32MX470F512L
    PLIB_PORTS_RemapOutput(PORTS_ID_0,OUTPUT_FUNC_SS1,OUTPUT_PIN_RPE3);       //PIC32MX470F512L
    PLIB_PORTS_RemapOutput(PORTS_ID_0,OUTPUT_FUNC_SDO1,OUTPUT_PIN_RPD0);      //PIC32MX470F512L

    PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_A, LED_1 );
    PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_A, LED_2 );
    PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_A, LED_3 );

    PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_A, LED_1 );
    PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_A, LED_2 );
    PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_A, LED_3 );

}

// *****************************************************************************
/* Function: 
    void BSP_LEDStateSet(BSP_LED led, BSP_LED_STATE state);

  Summary:
    Controls the state of the LED.
  
  Description:
    This function allows the application to specify the state of the LED.

  Remarks:
    Refer to bsp_config.h for usage information.
*/

void BSP_LEDStateSet(BSP_LED led, BSP_LED_STATE state)
{
    /* Switch ON the LED */
    PLIB_PORTS_PinWrite ( PORTS_ID_0 , PORT_CHANNEL_A , led, state );
}

// *****************************************************************************
/* Function: 
    void BSP_LEDOn(BSP_LED led);

  Summary:
    Switches ON the specified LED.
  
  Description:
    This function switches ON the specified LED.

  Remarks:
    Refer to bsp_config.h for usage information.
*/

void BSP_LEDOn(BSP_LED led)
{
    PLIB_PORTS_PinSet( PORTS_ID_0, PORT_CHANNEL_A, led);
}

// *****************************************************************************
/* Function: 
    void BSP_LEDOff(BSP_LED led);

  Summary:
    Switches OFF the specified LED.
  
  Description:
    This function switches OFF the specified LED.

  Remarks:
    Refer to bsp_config.h for usage information.
*/

void BSP_LEDOff(BSP_LED led)
{
    PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_A, led);
}

// *****************************************************************************
/* Function: 
    BSP_LED_STATE BSP_LEDStateGet(BSP_LED led);

  Summary:
    Returns the present state of the LED.
  
  Description:
    This function returns the present state of the LED.

  Remarks:
    Refer to bsp_config.h for usage information.
*/

BSP_LED_STATE BSP_LEDStateGet(BSP_LED led)
{
    return(PLIB_PORTS_PinGet(PORTS_ID_0, PORT_CHANNEL_A, led));
}

// *****************************************************************************
/* Function: 
    void BSP_LEDToggle(BSP_LED led);

  Summary:
    Toggles the state of the LED between BSP_LED_STATE_ON and BSP_LED_STATE_OFF.
  
  Description:
    This function toggles the state of the LED between BSP_LED_STATE_ON and
    BSP_LED_STATE_OFF.

  Remarks:
    Refer to bsp_config.h for usage information.
*/

void BSP_LEDToggle(BSP_LED led)
{
    PLIB_PORTS_PinToggle(PORTS_ID_0, PORT_CHANNEL_A,led );
}

// *****************************************************************************
/* Function: 
    void BSP_SwitchStateGet(BSP_SWITCH switch);

  Summary:
    Returns the present state (pressed or not pressed) of the specified switch.
  
  Description:
    This function returns the present state (pressed or not pressed) of the
    specified switch.

  Remarks:
    Refer to bsp_config.h for usage information.
*/

BSP_SWITCH_STATE BSP_SwitchStateGet( BSP_SWITCH bspSwitch )
{
    if(bspSwitch == BSP_SWITCH_5)
    {
       /* Switch 5 is connected to RA7! */
       return ( PLIB_PORTS_PinGet(PORTS_ID_0, PORT_CHANNEL_A, 7) ) ;
    }
    
    return ( PLIB_PORTS_PinGet(PORTS_ID_0, PORT_CHANNEL_D, 7) );
}


/*******************************************************************************
 End of File
*/
