/*******************************************************************************
  Board Support Package Implementation.

  Company:
    Microchip Technology Inc.

  File Name:
    bsp_sys_init.c

  Summary:
    Board Support Package Implementation for the PIC32MZ Embedded Connectivity
    (EC) Starter Kit in combination with the Multimedia Expansion Board II and a
    WVGA display.

  Description:
    This file contains routines that implement the Board Support Package for the
    PIC32MZ Embedded Connectivity (EC) Starter Kit in combination with the
    Multimedia Expansion Board II and a WVGA display.
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "bsp_config.h"
#include "peripheral/ebi/plib_ebi.h"

// *****************************************************************************
// *****************************************************************************
// *****************************************************************************
// Section: Interface Routines
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Function: 
    void BSP_Initialize(void)

  Summary:
    Performs the necessary actions to initialize a board
  
  Description:
    This function initializes the LED, Switch and other ports on the board.
    This function must be called by the user before using any APIs present in
    this BSP.  

  Remarks:
    Refer to bsp_config.h for usage information.
*/

void BSP_Initialize(void)
{
    /* Required for camera */
    PLIB_PORTS_PinDirectionOutputSet(PORTS_ID_0, PORT_CHANNEL_J, PORTS_BIT_POS_7);
    PLIB_PORTS_PinSet(PORTS_ID_0, PORT_CHANNEL_J, PORTS_BIT_POS_7);

    PLIB_PORTS_PinDirectionOutputSet(PORTS_ID_0, PORT_CHANNEL_J, PORTS_BIT_POS_12);
    PLIB_PORTS_PinClear(PORTS_ID_0, PORT_CHANNEL_J, PORTS_BIT_POS_12);

    PLIB_PORTS_PinDirectionOutputSet(PORTS_ID_0, PORT_CHANNEL_J, PORTS_BIT_POS_10);
    PLIB_PORTS_PinClear(PORTS_ID_0, PORT_CHANNEL_J, PORTS_BIT_POS_10);

    PLIB_EBI_BaseAddressSet(EBI_ID_0, 0, 0x20000000);
    PLIB_EBI_MemoryCharacteristicsSet(EBI_ID_0, 0 , SRAM, MEMORY_SIZE_8MB, CS_TIMING_0);
    PLIB_EBI_MemoryTimingConfigSet(EBI_ID_0, 0, 0, 0, 0, 0, 0, 1);
    PLIB_EBI_StaticMemoryWidthRegisterSet(EBI_ID_0, 0,  MEMORY_WIDTH_16BIT);
    PLIB_EBI_StaticMemoryWidthRegisterSet(EBI_ID_0, 1,  MEMORY_WIDTH_16BIT);
    PLIB_EBI_StaticMemoryWidthRegisterSet(EBI_ID_0, 2,  MEMORY_WIDTH_16BIT);
    PLIB_EBI_FlashPowerDownModeSet (EBI_ID_0, true);

    PLIB_PORTS_PinDirectionOutputSet(PORTS_ID_0, PORT_CHANNEL_K, PORTS_BIT_POS_5);
    PLIB_PORTS_PinClear(PORTS_ID_0, PORT_CHANNEL_K, PORTS_BIT_POS_5);

    /* Make the Switch pins digital */
    PLIB_PORTS_PinModePerPortSelect (PORTS_ID_0, PORT_CHANNEL_B, BSP_SWITCH_1, PORTS_PIN_MODE_DIGITAL);
    PLIB_PORTS_PinModePerPortSelect (PORTS_ID_0, PORT_CHANNEL_B, BSP_SWITCH_2, PORTS_PIN_MODE_DIGITAL);
    PLIB_PORTS_PinModePerPortSelect (PORTS_ID_0, PORT_CHANNEL_B, BSP_SWITCH_3, PORTS_PIN_MODE_DIGITAL);

    /* Set the Switch pins as input */
    PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 , PORT_CHANNEL_B , BSP_SWITCH_1 );
    PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 , PORT_CHANNEL_B , BSP_SWITCH_2 );
    PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 , PORT_CHANNEL_B , BSP_SWITCH_3 );

    /* Enable pull up resistors on switch ports */
    PLIB_PORTS_ChangeNoticePullUpPerPortEnable (PORTS_ID_0, PORT_CHANNEL_B, BSP_SWITCH_1);
    PLIB_PORTS_ChangeNoticePullUpPerPortEnable (PORTS_ID_0, PORT_CHANNEL_B, BSP_SWITCH_2);
    PLIB_PORTS_ChangeNoticePullUpPerPortEnable (PORTS_ID_0, PORT_CHANNEL_B, BSP_SWITCH_3);

    /* Setup the USB VBUS Switch Control Pin */
    PLIB_PORTS_PinModePerPortSelect (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_5, PORTS_PIN_MODE_DIGITAL );
    PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_5 );

    /* Setup the LED port pins as digital */
    PLIB_PORTS_PinModePerPortSelect(PORTS_ID_0, PORT_CHANNEL_H, BSP_LED_1, PORTS_PIN_MODE_DIGITAL);  
    PLIB_PORTS_PinModePerPortSelect(PORTS_ID_0, PORT_CHANNEL_H, BSP_LED_2, PORTS_PIN_MODE_DIGITAL);  

    /* Set the LED pins as output */
    PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_H, BSP_LED_1 );
    PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_H, BSP_LED_2 );
    PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_H, BSP_LED_3 );

    /* Switch off LEDs */
    PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_H, BSP_LED_1 );
    PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_H, BSP_LED_2 );
    PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_H, BSP_LED_3 );

}

// *****************************************************************************
/* Function: 
    void BSP_LEDStateSet(BSP_LED led, BSP_LED_STATE state);

  Summary:
    Controls the state of the LED.
  
  Description:
    This function allows the application to specify the state of the LED.

  Remarks:
    Refer to bsp_config.h for usage information.
*/

void BSP_LEDStateSet(BSP_LED led, BSP_LED_STATE state)
{
    /* Set the state of the LED */
    PLIB_PORTS_PinWrite (PORTS_ID_0 , PORT_CHANNEL_H , led, state );
}

// *****************************************************************************
/* Function: 
    void BSP_LEDToggle(BSP_LED led);

  Summary:
    Toggles the state of the LED between BSP_LED_STATE_ON and BSP_LED_STATE_OFF.
  
  Description:
    This function toggles the state of the LED between BSP_LED_STATE_ON and
    BSP_LED_STATE_OFF.

  Remarks:
    Refer to bsp_config.h for usage information.
*/    

void BSP_LEDToggle(BSP_LED led)
{
    PLIB_PORTS_PinToggle(PORTS_ID_0, PORT_CHANNEL_H, led );
}

// *****************************************************************************
/* Function: 
    BSP_LED_STATE BSP_LEDStateGet(BSP_LED led);

  Summary:
    Returns the present state of the LED.
  
  Description:
    This function returns the present state of the LED.

  Remarks:
    Refer to bsp_config.h for usage information.
*/    

BSP_LED_STATE BSP_LEDStateGet (BSP_LED led)
{
    /* Get LED Status */
    return PLIB_PORTS_PinGet (PORTS_ID_0, PORT_CHANNEL_H, led);
}

// *****************************************************************************
/* Function: 
    void BSP_LEDOn(BSP_LED led);

  Summary:
    Switches ON the specified LED.
  
  Description:
    This function switches ON the specified LED.

  Remarks:
    Refer to bsp_config.h for usage information.
*/

void BSP_LEDOn(BSP_LED led)
{
    PLIB_PORTS_PinSet( PORTS_ID_0, PORT_CHANNEL_H, led );
}

// *****************************************************************************
/* Function: 
    void BSP_LEDOff(BSP_LED led);

  Summary:
    Switches Off the specified LED.
  
  Description:
    This function switches Off the specified LED.

  Remarks:
    Refer to bsp_config.h for usage information.
*/

void BSP_LEDOff(BSP_LED led)
{
    PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_H, led );
}

// *****************************************************************************
/* Function: 
    void BSP_SwitchStateGet(BSP_SWITCH switch);

  Summary:
    Returns the present state (pressed or not pressed) of the specified switch.
  
  Description:
    This function returns the present state (pressed or not pressed) of the
    specified switch.

  Remarks:
    Refer to bsp_config.h for usage information.
*/

BSP_SWITCH_STATE BSP_SwitchStateGet( BSP_SWITCH bspSwitch )
{
    return ( PLIB_PORTS_PinGet(PORTS_ID_0, PORT_CHANNEL_B, bspSwitch) );
}

// *****************************************************************************
/* Function: 
    void BSP_USBVBUSSwitchStateSet(BSP_USB_VBUS_SWITCH_STATE state);

  Summary:
    This function enables or disables the USB VBUS switch on the board.
  
  Description:
    This function enables or disables the VBUS switch on the board.

  Remarks:
    Refer to bsp_config.h for usage information.
*/

void BSP_USBVBUSSwitchStateSet(BSP_USB_VBUS_SWITCH_STATE state)
{
    /* Enable the VBUS switch */

    PLIB_PORTS_PinWrite( PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_5, state );
}


/*******************************************************************************
 End of File
*/
