/*******************************************************************************
 Board initialization file for PIC32 USB starter kit

 Company:
    Microchip Technology Inc.

 File Name:
    bsp_sys_init.c

 Summary:
    Board initialization file for PIC32 Explorer 16 board.

 Description:
    This file contains the initialization of board specific I/O.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "bsp_config.h"


// *****************************************************************************
// *****************************************************************************
// *****************************************************************************
// Section: Interface Routines
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function: void BSP_Initialize(void)

  Summary:
    Performs the neccassary actions to initialize a board

  Description:
    This routine performs the neccassary actions to initialize a board

  Remarks:
    This routine performs direct register accesses, when the PORTS PLIB and
    system service become available, these register accesses will be be replaced
    by the PLIB\system service interfaces.

*/

void BSP_Initialize(void )
{
    // Start of code comment of old PIC32MX795F512L
    /* set the switch pins as input */
/*  PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 ,              //PIC32MX795F512L no portD present
                                      PORT_CHANNEL_D ,
                                      SWITCH_0 );

    PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 ,
                                      PORT_CHANNEL_D ,
                                      SWITCH_1 );       */

//  PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 ,
//                                    PORT_CHANNEL_A ,
//                                    SWITCH_2 );

//  PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_A, LED_1 );
//  PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_A, LED_2 );      //PIC32MX795F512L
//  PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_A, LED_3 );

//  PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_A, LED_1 );
//  PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_A, LED_2 );          //PIC32MX795F512L
//  PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_A, LED_3 );


    /* Enable pullups on the Switch ports*/

 /* PLIB_PORTS_ChangeNoticePullUpEnable(PORTS_ID_0, CN15);              //PIC32MX270F256D
    PLIB_PORTS_ChangeNoticePullUpEnable(PORTS_ID_0, CN16);
    PLIB_PORTS_ChangeNoticePullUpEnable(PORTS_ID_0, CN19);      */

      // End of code comment of old PIC32MX795F512L

//  ANSELCCLR = 0x000F;
//  TRISCCLR  = 0x0002;     //RC1 to o/p
        /*remap SPI pins */
    PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 ,                      //PIC32MX270F256D   Set SDI-1 as I/P
                                     PORT_CHANNEL_B ,                   //PIC32MX270F256D
                                     SWITCH_1 );

    PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 ,                      //PIC32MX270F256D   Set RB12 connected to U6 as I/P - floats HI on start-up
                                     PORT_CHANNEL_B ,
                                     SWITCH_12 );

    PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 ,                      //PIC32MX270F256D   Set RB13 connected to U6 as I/P - pulled HI externally
                                     PORT_CHANNEL_B ,
                                     SWITCH_13 );

    PLIB_PORTS_PinDirectionInputSet ( PORTS_ID_0 ,                      //PIC32MX270F256D   Set RB3 Card Detect signal
                                     PORT_CHANNEL_B ,
                                     SWITCH_CD );

    PLIB_PORTS_PinDirectionOutputSet ( PORTS_ID_0 ,                      //PIC32MX270F256D   Set RA9 Write Protect signal
                                     PORT_CHANNEL_A ,
                                     SWITCH_WP );

    PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_C, LED_5 );      //PIC32MX270F256D
    PLIB_PORTS_PinDirectionOutputSet( PORTS_ID_0, PORT_CHANNEL_B, LED_6 );      //PIC32MX270F256D

    PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_C, LED_5 );                   //PIC32MX270F256D
    PLIB_PORTS_PinClear( PORTS_ID_0, PORT_CHANNEL_A, LED_6 );                   //PIC32MX270F256D

    PLIB_PORTS_RemapInput(PORTS_ID_0,INPUT_FUNC_SDI1,INPUT_PIN_RPB1);          //PIC32MX270F256D
    PLIB_PORTS_RemapOutput(PORTS_ID_0,OUTPUT_FUNC_SS1,OUTPUT_PIN_RPB15);       //PIC32MX270F256D
    PLIB_PORTS_RemapOutput(PORTS_ID_0,OUTPUT_FUNC_SDO1,OUTPUT_PIN_RPC1);       //PIC32MX270F256D

}

void BSP_LEDOn(BSP_LED led)
{

    /* switch ON the LED */
//  PLIB_PORTS_PinWrite ( PORTS_ID_0 ,
//                       PORT_CHANNEL_A ,
//                       led,
//                       1 );
/*    PLIB_PORTS_PinWrite ( PORTS_ID_0 ,
                         PORT_CHANNEL_B ,
                         led,
                         1 ); */
     PLIB_PORTS_PinWrite ( PORTS_ID_0 ,
                         PORT_CHANNEL_C ,
                         led,
                         1 );

}

void BSP_SwitchON_FailureLED(BSP_LED led)       //PIC32MX270F256D Lighting up LED-D6 on Explorer 16
{

  PLIB_PORTS_PinWrite ( PORTS_ID_0 ,
                         PORT_CHANNEL_B ,
                         led,
                         1 );

}



void BSP_SwitchOFFLED(BSP_LED led)
{

    /* switch OFF the LED */
    PLIB_PORTS_PinWrite ( PORTS_ID_0 ,
                         PORT_CHANNEL_A ,
                         led,
                         0 );
}

void BSP_ToggleLED(BSP_LED led)
{

//  PLIB_PORTS_PinToggle(PORTS_ID_0, PORT_CHANNEL_A,led );        //PIC32MX795F512L
    PLIB_PORTS_PinToggle(PORTS_ID_0, PORT_CHANNEL_B,led );        //PIC32MX270F256D
}

void BSP_PortB_SetAnlgPortstoDgtl(void)
{
    ANSELBCLR = 0xFFFF;           //PIC32MX270F256D - Set all PORTB to Digital I/O
}


/*
BSP_SWITCH_STATE BSP_ReadSwitch( BSP_SWITCH bspSwitch )                     //PIC32MX270F256D no portD present
{
    return ( PLIB_PORTS_PinGet(PORTS_ID_0, PORT_CHANNEL_D, bspSwitch) );
}   */

uint32_t __attribute__((nomips16)) BSP_ReadCoreTimer()
{
    uint32_t timer;

    // get the count reg
    asm volatile("mfc0   %0, $9" : "=r"(timer));

    return(timer);
}

void __attribute__((nomips16)) BSP_StartTimer(uint32_t period)
{
    /* Reset the coutner */

    uint32_t loadZero = 0;

    asm volatile("mtc0   %0, $9" : "+r"(loadZero));
    asm volatile("mtc0   %0, $11" : "+r" (period));

}

/******************************************************************************/
/******************************************************************************/


/*******************************************************************************
 End of File
*/
