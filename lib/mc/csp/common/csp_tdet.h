/***************************************************************************************************
 *   Project:       
 *   Author:        Alex Borisov
 ***************************************************************************************************
 *   Distribution:
 *
 ***************************************************************************************************
 *   MCU Family:    
 *   Compiler:      
 ***************************************************************************************************
 *   File:          tdet.h
 *   Description:   Target Detect (Compiler and CPU)
 *
 ***************************************************************************************************
 *   History:       15.09.2010 - [Alex Borisov] - file created
 *
 **************************************************************************************************/

#ifndef __TDET_H
#define __TDET_H

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

/* DEFINE FOLLOWING FOR EVERY CPU:

    CPU_ARCH    - cpu architecture (for string reference)
    CPU_ID      - cpu ID (for string reference)

    COMPILER_XX - compiler
    CPU_XX      - cpu family
*/

#if defined(__GNUC__)
#define COMPILER_GCC                1
#endif

    /* Microchip C30 */

#if defined(__C30__)
#define COMPILER_MCHP_C30           1

#if defined(__PIC24F__)
    #define CPU_ARCH                MCHP_16BIT
    #define CPU_MCHP_PIC24F         1
    #define CPU_ID                  MCHP_PIC24F                     /* PIC24F       */
#elif defined(__PIC24FK__)
    #define CPU_ARCH                MCHP_16BIT
    #define CPU_MCHP_PIC24FK        1
    #define CPU_ID                  MCHP_PIC24FK                    /* PIC24FK      */
#elif defined(__PIC24H__)
    #define CPU_ARCH                MCHP_16BIT
    #define CPU_MCHP_PIC24H         1
    #define CPU_ID                  MCHP_PIC24H                     /* PIC24H       */
#elif defined(__dsPIC33F__)
    #define CPU_ARCH                MCHP_16BIT
    #define CPU_MCHP_DSPIC33        1
    #define CPU_ID                  MCHP_DSPIC33                    /* dsPIC33      */
#elif defined(__dsPIC30F__)
    #define CPU_ARCH                MCHP_16BIT
    #define CPU_MCHP_DSPIC30        1
    #define CPU_ID                  MCHP_DSPIC30                    /* dsPIC30      */
#else
    #error Unknown CPU under Microchip C30 compiler
#endif
    
#endif


    /* Microchip C32 */

#if defined(__C32_VERSION__)
#define COMPILER_MCHP_C32           1

#if defined(__PIC32MX__)
    #define CPU_ARCH                MCHP_32BIT
    #define CPU_MCHP_PIC32          1
    #define CPU_ID                  MCHP_PIC32                      /* PIC32        */
#else
    #error Unknown CPU under Microchip C32 compiler
#endif
#endif


    /* Keil RealView */

#if defined(__ARMCC_VERSION)
#define COMPILER_KEIL_REALVIEW      1

#if ((__TARGET_ARCH_ARM == 0) && (__TARGET_ARCH_THUMB == 4))
    #define CPU_ARCH                CORTEX_M3

    /* Check for processor type */
#if defined(__LPC17XX__)
    #define CPU_NXP_LPC17XX         1
    #define CPU_ID                  NXP_LPC17XX                     /* LPC17XX      */


/*  Tip: To avoid modifying this file each time you need to switch between these
        devices, you can define the device in your toolchain compiler preprocessor.

 STM32F10X_LD       - Low density devices are STM32F101xx, STM32F102xx and STM32F103xx
   microcontrollers where the Flash memory density ranges between 16 and 32 Kbytes.
 STM32F10X_LD_VL    - Low-density value line devices are STM32F100xx microcontrollers
   where the Flash memory density ranges between 16 and 32 Kbytes.
 STM32F10X_MD       - Medium density devices are STM32F101xx, STM32F102xx and STM32F103xx
   microcontrollers where the Flash memory density ranges between 64 and 128 Kbytes.
 STM32F10X_MD_VL    - Medium-density value line devices are STM32F100xx microcontrollers
   where the Flash memory density ranges between 64 and 128 Kbytes
 STM32F10X_HD       - High density devices are STM32F101xx and STM32F103xx microcontrollers
   where the Flash memory density ranges between 256 and 512 Kbytes.
 STM32F10X_XL       - XL-density devices are STM32F101xx and STM32F103xx microcontrollers where
   the Flash memory density ranges between 512 and 1024 Kbytes.
 STM32F10X_CL       - Connectivity line devices are STM32F105xx and STM32F107xx microcontrollers.
  */

#elif defined (STM32F10X_LD) 
    #define CPU_STM_STM32F10X_LD    1
    #define CPU_ID                  STM_STM32F10X_LD
#elif defined (STM32F10X_LD_VL)
    #define CPU_STM_STM32F10X_LD_VL 1
    #define CPU_ID                  STM_STM32F10X_LD_VL
#elif defined (STM32F10X_MD) 
    #define CPU_STM_STM32F10X_MD    1
    #define CPU_ID                  STM_STM32F10X_MD
#elif defined (STM32F10X_MD_VL) 
    #define CPU_STM_STM32F10X_MD_VL 1
    #define CPU_ID                  STM_STM32F10X_MD_VL
#elif defined (STM32F10X_HD) 
    #define CPU_STM_STM32F10X_HD    1
    #define CPU_ID                  STM_STM32F10X_HD
#elif defined (STM32F10X_XL) 
    #define CPU_STM_STM32F10X_XL    1
    #define CPU_ID                  STM_STM32F10X_XL
#elif defined (STM32F10X_CL) 
    #define CPU_STM_STM32F10X_CL    1
    #define CPU_ID                  STM_STM32F10X_CL

#elif defined (STM32L1XX_MD)
    #define CPU_STM_STM32L1XX_MD    1
    #define CPU_ID                  STM_STM32L1XX_MD
#else
    //#error Please Define CPU type in project options
#endif

#elif ((__TARGET_ARCH_ARM == 0) && (__TARGET_ARCH_THUMB == 3))
#   define  CPU_ARCH                CORTEX_M0
#else
    #error Unknown CPU architecture under Keil RealView Compiler
#endif
#endif


    /* COSMIC STM8 */

#if defined(__CSMC__)
#define COMPILER_COSMIC_STM8        1
#define CPU_ARCH                    STM8

#if defined(STM8L15X)
    #define CPU_STM_STM8L15X        1
    #define CPU_ID                  STM_STM8L15X
#elif defined(STM8S208)
    #define CPU_STM_STM8S208        1
    #define CPU_ID                  STM_STM8S208
#elif defined(STM8S207)
    #define CPU_STM_STM8S207        1
    #define CPU_ID                  STM_STM8S207
#elif defined(STM8S105)
    #define CPU_STM_STM8S105        1
    #define CPU_ID                  STM_STM8S105
#elif defined(STM8S103)
    #define CPU_STM_STM8S103        1
    #define CPU_ID                  STM_STM8S103
#elif defined(STM8S903)
    #define CPU_STM_STM8S903        1
    #define CPU_ID                  STM_STM8S903
#else
    #error Unknown CPU under COSMIC compiler
#endif

#endif


#if defined(__PICC__)
#define COMPILER_HITECH_PICC        1

#if defined(_PIC12)
    #define CPU_ARCH                MCHP_BASELINE
    #define CPU_MCHP_PIC12          1
    #define CPU_ID                  MCHP_PIC12
#elif defined (_PIC14)
    #define CPU_ARCH                MCHP_MIDRANGE
    #define CPU_MCHP_PIC16          1
    #define CPU_ID                  MCHP_PIC16
#elif defined (_PIC16)
    #define CPU_ARCH                MCHP_HIGHEND
    #define CPU_MCHP_PIC16          1
    #define CPU_ID                  MCHP_PIC16
#else
    #error Unknown CPU under Hi-Tech PICC compiler
#endif

#endif



/* Self-check for the detection: only one compiler must be detected */

#if 0
#if COMPILER_MCHP_C30 + COMPILER_MCHP_C32 + COMPILER_KEIL_REALVIEW + COMPILER_COSMIC_STM8 + COMPILER_HITECH_PICC/* + COMPILER_GCC*/ == 0
    #error Unknown compiler
#elif COMPILER_MCHP_C30 + COMPILER_MCHP_C32 + COMPILER_KEIL_REALVIEW + COMPILER_COSMIC_STM8 + COMPILER_HITECH_PICC/*  + COMPILER_GCC*/!= 1
    #error Internal compiler configuration error
#endif
#endif



#endif /* __TDET_H */
/***************************************************************************************************
    end of file: tdet.h
 **************************************************************************************************/
