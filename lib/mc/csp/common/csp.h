/***************************************************************************************************
 *   Project:       P_0161 - 
 *   Author:        Alex Borisov
 ***************************************************************************************************
 *   Distribution:
 *
 ***************************************************************************************************
 *   MCU Family:    
 *   Compiler:      
 ***************************************************************************************************
 *   File:          csp.h
 *   Description:   
 *
 ***************************************************************************************************
 *   History:       16.09.2010 - [Alex Borisov] - file created
 *
 **************************************************************************************************/

#ifndef _CSP_DEF_H
#define _CSP_DEF_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include "csp_tdet.h"
#include "csp_types.h"

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

#if defined(CPU_MCHP_PIC24F) || defined(CPU_MCHP_PIC24FK) || defined(__PIC24H__) || defined(__dsPIC33F__) || defined(__dsPIC30F__)
    #if defined(COMPILER_MCHP_C30)
        #include "../mchp/pic24_dspic/cspi.h"
    #else
        #error Undefined compiler for PIC24 or dsPIC CPU
    #endif

#elif defined(__PIC32MX__)
    #if defined(COMPILER_MCHP_C32)
        #include "../mchp/pic32/cspi.h"
    #else
        #error Undefined compiler for PIC24 or dsPIC CPU
    #endif

#elif defined(CPU_MCHP_PIC12) || defined(CPU_MCHP_PIC16)
    #if defined(COMPILER_HITECH_PICC)
        #include "../mchp/pic12_16/cspi.h"
    #else
        #error Undefined compiler for PIC12 or PIC16 CPU
    #endif

#elif defined(CPU_NXP_LPC17XX)
    #if defined(COMPILER_KEIL_REALVIEW)
        #include <LPC17xx.h>
    #else
        #error Undefined compiler for NXP LPC17XX CPU
    #endif

#elif defined(CPU_STM_STM32F10X_LD) || defined(CPU_STM_STM32F10X_LD_VL) || defined(CPU_STM_STM32F10X_MD) || defined(CPU_STM_STM32F10X_MD_VL) || defined(CPU_STM_STM32F10X_HD) || defined(CPU_STM_STM32F10X_XL) || defined(CPU_STM_STM32F10X_CL)
    #if defined(COMPILER_KEIL_REALVIEW)
        #include "../stm/stm32f/CMSIS/CM3/device/ST/STM32F10x/stm32f10x.h"
    #else
        #error Undefined compiler for STM STM32F CPU
    #endif

#elif defined(CPU_STM_STM32L1XX_MD)
    #if defined(COMPILER_KEIL_REALVIEW)
        #include "../stm/stm32l/CMSIS/CM3/device/ST/STM32L1xx/stm32l1xx.h"
    #else
        #error Undefined compiler for STM STM32L CPU
    #endif

#elif defined(CPU_STM_STM8L15X)
    #if defined(COMPILER_COSMIC_STM8)
        #include "../stm/stm8l/stm8l15x/inc/stm8l15x.h"
    #else
        #error Undefined compiler for STM STM8L CPU
    #endif

#elif defined(CPU_STM_STM8S208) || defined(CPU_STM_STM8S207) || defined(CPU_STM_STM8S105) || defined(CPU_STM_STM8S103) || defined(CPU_STM_STM8S903)
    #if defined(COMPILER_COSMIC_STM8)
        #include "../stm/stm8s/inc/stm8s.h"
    #else
        #error Undefined compiler for STM STM8S CPU
    #endif

#else
    #error Please define path to CSP or peripheral support library
#endif

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/


#endif /* _CSP_DEF_H */
/***************************************************************************************************
    end of file: csp.h
 **************************************************************************************************/
