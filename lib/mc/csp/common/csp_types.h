/***************************************************************************************************
 *   Project:       
 *   Author:        Alex Borisov
 ***************************************************************************************************
 *   Distribution:
 *
 ***************************************************************************************************
 *   MCU Family:    
 *   Compiler:      
 ***************************************************************************************************
 *   File:          types.h
 *   Description:   Types definition
 *
 ***************************************************************************************************
 *   History:       15.09.2010 - [Alex Borisov] - file created
 *
 **************************************************************************************************/

#ifndef __TYPES_H
#define __TYPES_H

#include "csp_tdet.h"


/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

#if COMPILER_MCHP_C30

    #include <stddef.h>
    #include <stdint.h>
    #include <stdbool.h>

    #ifndef __STDINT_DECLS
    #define __STDINT_DECLS

    /*
     * 'signed' is redundant below, except for 'signed char' and if
     * the typedef is used to declare a bitfield.
     * '__int64' is used instead of 'long long' so that this header
     * can be used in --strict mode.
     */
        /* 7.18.1.1 */
    
        /* exact-width signed integer types */
    typedef   signed               char int8_t;
    typedef   signed                int int16_t;
    typedef   signed long           int int32_t;
    typedef   signed long long      int int64_t;
    
        /* exact-width unsigned integer types */
    typedef unsigned               char uint8_t;
    typedef unsigned                int uint16_t;
    typedef unsigned long           int uint32_t;
    typedef unsigned long long      int uint64_t;
    
        /* 7.18.1.2 */
    
        /* smallest type of at least n bits */
        /* minimum-width signed integer types */
    typedef   signed               char int_least8_t;
    typedef   signed                int int_least16_t;
    typedef   signed long           int int_least32_t;
    typedef   signed long long      int int_least64_t;
    
        /* minimum-width unsigned integer types */
    typedef unsigned               char uint_least8_t;
    typedef unsigned                int uint_least16_t;
    typedef unsigned long           int uint_least32_t;
    typedef unsigned long long      int uint_least64_t;
    
        /* 7.18.1.3 */
    
        /* fastest minimum-width signed integer types */
    typedef   signed                int int_fast8_t;
    typedef   signed                int int_fast16_t;
    typedef   signed long           int int_fast32_t;
    typedef   signed long long      int int_fast64_t;
    
        /* fastest minimum-width unsigned integer types */
    typedef unsigned                int uint_fast8_t;
    typedef unsigned                int uint_fast16_t;
    typedef unsigned long           int uint_fast32_t;
    typedef unsigned long long      int uint_fast64_t;
    
        /* 7.18.1.4 integer types capable of holding object pointers */
    typedef   signed                int intptr_t;
    typedef unsigned                int uintptr_t;
    
        /* 7.18.1.5 greatest-width integer types */
    typedef   signed long long      int intmax_t;
    typedef unsigned long long      int uintmax_t;

    #if !defined(__cplusplus) || defined(__STDC_LIMIT_MACROS)
#if 0

    /* 7.18.2.1 */

    /* minimum values of exact-width signed integer types */
    #define INT8_MIN                (-128)
    #define INT16_MIN               (-32768)
    #define INT32_MIN               (~0x7fffffffL)               /* -2147483648 is unsigned */
    #define INT64_MIN               (~0x7fffffffffffffffLL)      /* -9223372036854775808 is unsigned */

    /* maximum values of exact-width signed integer types */
    #define INT8_MAX                (127)
    #define INT16_MAX               (32767)
    #define INT32_MAX               (2147483647L)
    #define INT64_MAX               (9223372036854775807LL)

    /* maximum values of exact-width unsigned integer types */
    #define UINT8_MAX               (255)
    #define UINT16_MAX              (65535)
    #define UINT32_MAX              (4294967295UL)
    #define UINT64_MAX              (18446744073709551615ULL)

    /* 7.18.2.2 */

    /* minimum values of minimum-width signed integer types */
    #define INT_LEAST8_MIN          (-128)
    #define INT_LEAST16_MIN         (-32768)
    #define INT_LEAST32_MIN         (~0x7fffffffL)
    #define INT_LEAST64_MIN         (~0x7fffffffffffffffLL)

    /* maximum values of minimum-width signed integer types */
    #define INT_LEAST8_MAX          (127)
    #define INT_LEAST16_MAX         (32767)
    #define INT_LEAST32_MAX         (2147483647L)
    #define INT_LEAST64_MAX         (9223372036854775807LL)

    /* maximum values of minimum-width unsigned integer types */
    #define UINT_LEAST8_MAX         (255)
    #define UINT_LEAST16_MAX        (65535)
    #define UINT_LEAST32_MAX        (4294967295UL)
    #define UINT_LEAST64_MAX        (18446744073709551615ULL)

    /* 7.18.2.3 */

    /* minimum values of fastest minimum-width signed integer types */
    #define INT_FAST8_MIN           (-32768)
    #define INT_FAST16_MIN          (-32768)
    #define INT_FAST32_MIN          (~0x7fffffffL)
    #define INT_FAST64_MIN          (~0x7fffffffffffffffLL)

    /* maximum values of fastest minimum-width signed integer types */
    #define INT_FAST8_MAX           (32767)
    #define INT_FAST16_MAX          (32767)
    #define INT_FAST32_MAX          (2147483647L)
    #define INT_FAST64_MAX          (9223372036854775807LL)

    /* maximum values of fastest minimum-width unsigned integer types */
    #define UINT_FAST8_MAX          (65535)
    #define UINT_FAST16_MAX         (65535)
    #define UINT_FAST32_MAX         (4294967295UL)
    #define UINT_FAST64_MAX         (18446744073709551615ULL)

    /* 7.18.2.4 */

    /* minimum value of pointer-holding signed integer type */
    #define INTPTR_MIN              (-32768)

    /* maximum value of pointer-holding signed integer type */
    #define INTPTR_MAX              (32767)

    /* maximum value of pointer-holding unsigned integer type */
    #define UINTPTR_MAX             (65535)

    /* 7.18.2.5 */

    /* minimum value of greatest-width signed integer type */
    #define INTMAX_MIN              (~0x7fffffffffffffffLL)

    /* maximum value of greatest-width signed integer type */
    #define INTMAX_MAX              (9223372036854775807LL)

    /* maximum value of greatest-width unsigned integer type */
    #define UINTMAX_MAX             (18446744073709551615ULL)

    /* 7.18.3 */

    /* limits of ptrdiff_t */
    #define PTRDIFF_MIN             (-32768)
    #define PTRDIFF_MAX             (32767)

    /* limits of sig_atomic_t */
    #define SIG_ATOMIC_MIN          (-32768)
    #define SIG_ATOMIC_MAX          (32767) 

    /* limit of size_t */
    #define SIZE_MAX                (65535)

    /* limits of wchar_t */
    /* NB we have to undef and redef because they're defined in both
     * stdint.h and wchar.h */
    #undef WCHAR_MIN
    #undef WCHAR_MAX

    #if defined(__WCHAR32)
        #define WCHAR_MIN           (0)
        #define WCHAR_MAX           (0xffffffffUL)
    #else
        #define WCHAR_MIN           (0)
        #define WCHAR_MAX           (65535)
    #endif

    /* limits of wint_t */
    #define WINT_MIN                (~0x7fffffffL)
    #define WINT_MAX                (2147483647L)

#endif
    #endif /* __STDC_LIMIT_MACROS */


    #if !defined(__cplusplus) || defined(__STDC_CONSTANT_MACROS)

    /* 7.18.4.1 macros for minimum-width integer constants */
    #define INT8_C(x)   (x)
    #define INT16_C(x)  (x)
    #define INT32_C(x)  (x ## L)
    #define INT64_C(x)  (x ## LL)

    #define UINT8_C(x)  (x ## U)
    #define UINT16_C(x) (x ## U)
    #define UINT32_C(x) (x ## UL)
    #define UINT64_C(x) (x ## ULL)

    /* 7.18.4.2 macros for greatest-width integer constants */
    #define INTMAX_C(x)  (x ## LL)
    #define UINTMAX_C(x) (x ## ULL)

    #endif /* __STDC_CONSTANT_MACROS */    
    #endif /* __STDINT_DECLS */

#if 0
    #ifndef __cplusplus
        #define true            (1==1)
        #define false           (1!=1)
        typedef unsigned int    bool;
    #endif /* __cplusplus */
#endif


#elif COMPILER_COSMIC_STM8

    #include <stddef.h>

    #ifndef __STDINT_DECLS
    #define __STDINT_DECLS

    /*
     * 'signed' is redundant below, except for 'signed char' and if
     * the typedef is used to declare a bitfield.
     * '__int64' is used instead of 'long long' so that this header
     * can be used in --strict mode.
     */
        /* 7.18.1.1 */
    
        /* exact-width signed integer types */
    typedef   signed               char int8_t;
    typedef   signed                int int16_t;
    typedef   signed long           int int32_t;
    //typedef   signed long long int int64_t;
    
        /* exact-width unsigned integer types */
    typedef unsigned               char uint8_t;
    typedef unsigned                int uint16_t;
    typedef unsigned long           int uint32_t;
    //typedef unsigned long long int uint64_t;
    
        /* 7.18.1.2 */
    
        /* smallest type of at least n bits */
        /* minimum-width signed integer types */
    typedef   signed               char int_least8_t;
    typedef   signed                int int_least16_t;
    typedef   signed long           int int_least32_t;
    //typedef   signed long long int int_least64_t;
    
        /* minimum-width unsigned integer types */
    typedef unsigned               char uint_least8_t;
    typedef unsigned                int uint_least16_t;
    typedef unsigned long           int uint_least32_t;
    //typedef unsigned long long int uint_least64_t;
    
        /* 7.18.1.3 */
    
        /* fastest minimum-width signed integer types */
    typedef   signed               char int_fast8_t;
    typedef   signed                int int_fast16_t;
    typedef   signed long           int int_fast32_t;
    //typedef   signed long long int int_fast64_t;
    
        /* fastest minimum-width unsigned integer types */
    typedef unsigned               char uint_fast8_t;
    typedef unsigned                int uint_fast16_t;
    typedef unsigned long           int uint_fast32_t;
    //typedef unsigned long long int uint_fast64_t;
    
        /* 7.18.1.4 integer types capable of holding object pointers */
    typedef   signed                int intptr_t;
    typedef unsigned                int uintptr_t;
    
        /* 7.18.1.5 greatest-width integer types */
    typedef   signed long           int intmax_t;
    typedef unsigned long           int uintmax_t;

    #if !defined(__cplusplus) || defined(__STDC_LIMIT_MACROS)

    /* 7.18.2.1 */

    /* minimum values of exact-width signed integer types */
    #define INT8_MIN                (-128)
    #define INT16_MIN               (-32768)
    #define INT32_MIN               (~0x7fffffffL)               /* -2147483648 is unsigned */
    //#define INT64_MIN               (~0x7fffffffffffffffLL)      /* -9223372036854775808 is unsigned */

    /* maximum values of exact-width signed integer types */
    #define INT8_MAX                (127)
    #define INT16_MAX               (32767)
    #define INT32_MAX               (2147483647L)
    //#define INT64_MAX               (9223372036854775807LL)

    /* maximum values of exact-width unsigned integer types */
    #define UINT8_MAX               (255)
    #define UINT16_MAX              (65535)
    #define UINT32_MAX              (4294967295UL)
    //#define UINT64_MAX              (18446744073709551615ULL)

    /* 7.18.2.2 */

    /* minimum values of minimum-width signed integer types */
    #define INT_LEAST8_MIN          (-128)
    #define INT_LEAST16_MIN         (-32768)
    #define INT_LEAST32_MIN         (~0x7fffffffL)
    //#define INT_LEAST64_MIN         (~0x7fffffffffffffffLL)

    /* maximum values of minimum-width signed integer types */
    #define INT_LEAST8_MAX          (127)
    #define INT_LEAST16_MAX         (32767)
    #define INT_LEAST32_MAX         (2147483647L)
    //#define INT_LEAST64_MAX         (9223372036854775807LL)

    /* maximum values of minimum-width unsigned integer types */
    #define UINT_LEAST8_MAX         (255)
    #define UINT_LEAST16_MAX        (65535)
    #define UINT_LEAST32_MAX        (4294967295UL)
    //#define UINT_LEAST64_MAX        (18446744073709551615ULL)

    /* 7.18.2.3 */

    /* minimum values of fastest minimum-width signed integer types */
    #define INT_FAST8_MIN           (-128)
    #define INT_FAST16_MIN          (-32768)
    #define INT_FAST32_MIN          (~0x7fffffffL)
    //#define INT_FAST64_MIN          (~0x7fffffffffffffffLL)

    /* maximum values of fastest minimum-width signed integer types */
    #define INT_FAST8_MAX           (127)
    #define INT_FAST16_MAX          (32767)
    #define INT_FAST32_MAX          (2147483647L)
    //#define INT_FAST64_MAX          (9223372036854775807LL)

    /* maximum values of fastest minimum-width unsigned integer types */
    #define UINT_FAST8_MAX          (255)
    #define UINT_FAST16_MAX         (65535)
    #define UINT_FAST32_MAX         (4294967295UL)
    //#define UINT_FAST64_MAX         (18446744073709551615ULL)

    /* 7.18.2.4 */

    /* minimum value of pointer-holding signed integer type */
    #define INTPTR_MIN              (-32768)

    /* maximum value of pointer-holding signed integer type */
    #define INTPTR_MAX              (32767)

    /* maximum value of pointer-holding unsigned integer type */
    #define UINTPTR_MAX             (65535)

    /* 7.18.2.5 */

    /* minimum value of greatest-width signed integer type */
    #define INTMAX_MIN              (~0x7fffffffL)

    /* maximum value of greatest-width signed integer type */
    #define INTMAX_MAX              (2147483647L)

    /* maximum value of greatest-width unsigned integer type */
    #define UINTMAX_MAX             (4294967295UL)

    /* 7.18.3 */

    /* limits of ptrdiff_t */
    #define PTRDIFF_MIN             (-32768)
    #define PTRDIFF_MAX             (32767)

    /* limits of sig_atomic_t */
    #define SIG_ATOMIC_MIN          (-32768)
    #define SIG_ATOMIC_MAX          (32767) 

    /* limit of size_t */
    #define SIZE_MAX                (65535)

    /* limits of wchar_t */
    /* NB we have to undef and redef because they're defined in both
     * stdint.h and wchar.h */
    #undef WCHAR_MIN
    #undef WCHAR_MAX

    #if defined(__WCHAR32)
        #define WCHAR_MIN           (0)
        #define WCHAR_MAX           (0xffffffffUL)
    #else
        #define WCHAR_MIN           (0)
        #define WCHAR_MAX           (65535)
    #endif

    /* limits of wint_t */
    #define WINT_MIN                (~0x7fffffffL)
    #define WINT_MAX                (2147483647L)

    #endif /* __STDC_LIMIT_MACROS */


    #if !defined(__cplusplus) || defined(__STDC_CONSTANT_MACROS)

    /* 7.18.4.1 macros for minimum-width integer constants */
    #define INT8_C(x)   (x)
    #define INT16_C(x)  (x)
    #define INT32_C(x)  (x ## L)
    //#define INT64_C(x)  (x ## LL)

    #define UINT8_C(x)  (x ## U)
    #define UINT16_C(x) (x ## U)
    #define UINT32_C(x) (x ## UL)
    //#define UINT64_C(x) (x ## ULL)

    /* 7.18.4.2 macros for greatest-width integer constants */
    #define INTMAX_C(x)  (x ## L)
    #define UINTMAX_C(x) (x ## UL)

    #endif /* __STDC_CONSTANT_MACROS */    
    #endif /* __STDINT_DECLS */


    #ifndef __cplusplus
        #define true            (1==1)
        #define false           (1!=1)
        typedef unsigned char   bool;
    #endif /* __cplusplus */


#elif COMPILER_HITECH_PICC

    #include <stddef.h>

    #ifndef __STDINT_DECLS
    #define __STDINT_DECLS
    
    /*
    * 'signed' is redundant below, except for 'signed char' and if
    * the typedef is used to declare a bitfield.
    * '__int64' is used instead of 'long long' so that this header
    * can be used in --strict mode.
    */
    /* 7.18.1.1 */
    
    /* exact-width signed integer types */
    typedef   signed               char int8_t;
    typedef   signed                int int16_t;
    typedef   signed long           int int32_t;
    //typedef   signed long long int int64_t;
    
    /* exact-width unsigned integer types */
    typedef unsigned               char uint8_t;
    typedef unsigned                int uint16_t;
    typedef unsigned long           int uint32_t;
    //typedef unsigned long long int uint64_t;
    
    /* 7.18.1.2 */
    
    /* smallest type of at least n bits */
    /* minimum-width signed integer types */
    typedef   signed               char int_least8_t;
    typedef   signed                int int_least16_t;
    typedef   signed long           int int_least32_t;
    //typedef   signed long long int int_least64_t;
    
    /* minimum-width unsigned integer types */
    typedef unsigned               char uint_least8_t;
    typedef unsigned                int uint_least16_t;
    typedef unsigned long           int uint_least32_t;
    //typedef unsigned long long int uint_least64_t;
    
    /* 7.18.1.3 */
    
    /* fastest minimum-width signed integer types */
    typedef   signed               char int_fast8_t;
    typedef   signed                int int_fast16_t;
    typedef   signed long           int int_fast32_t;
    //typedef   signed long long int int_fast64_t;
    
    /* fastest minimum-width unsigned integer types */
    typedef unsigned               char uint_fast8_t;
    typedef unsigned                int uint_fast16_t;
    typedef unsigned long           int uint_fast32_t;
    //typedef unsigned long long int uint_fast64_t;
    
    /* 7.18.1.4 integer types capable of holding object pointers */
    typedef   signed                int intptr_t;
    typedef unsigned                int uintptr_t;
    
    /* 7.18.1.5 greatest-width integer types */
    typedef   signed long           int intmax_t;
    typedef unsigned long           int uintmax_t;
    
    #if !defined(__cplusplus) || defined(__STDC_LIMIT_MACROS)
    
    /* 7.18.2.1 */
    
    /* minimum values of exact-width signed integer types */
    #define INT8_MIN                (-128)
    #define INT16_MIN               (-32768)
    #define INT32_MIN               (~0x7fffffffL)               /* -2147483648 is unsigned */
    //#define INT64_MIN               (~0x7fffffffffffffffLL)      /* -9223372036854775808 is unsigned */
    
    /* maximum values of exact-width signed integer types */
    #define INT8_MAX                (127)
    #define INT16_MAX               (32767)
    #define INT32_MAX               (2147483647L)
    //#define INT64_MAX               (9223372036854775807LL)
    
    /* maximum values of exact-width unsigned integer types */
    #define UINT8_MAX               (255)
    #define UINT16_MAX              (65535)
    #define UINT32_MAX              (4294967295UL)
    //#define UINT64_MAX              (18446744073709551615ULL)
    
    /* 7.18.2.2 */
    
    /* minimum values of minimum-width signed integer types */
    #define INT_LEAST8_MIN          (-128)
    #define INT_LEAST16_MIN         (-32768)
    #define INT_LEAST32_MIN         (~0x7fffffffL)
    //#define INT_LEAST64_MIN         (~0x7fffffffffffffffLL)
    
    /* maximum values of minimum-width signed integer types */
    #define INT_LEAST8_MAX          (127)
    #define INT_LEAST16_MAX         (32767)
    #define INT_LEAST32_MAX         (2147483647L)
    //#define INT_LEAST64_MAX         (9223372036854775807LL)
    
    /* maximum values of minimum-width unsigned integer types */
    #define UINT_LEAST8_MAX         (255)
    #define UINT_LEAST16_MAX        (65535)
    #define UINT_LEAST32_MAX        (4294967295UL)
    //#define UINT_LEAST64_MAX        (18446744073709551615ULL)
    
    /* 7.18.2.3 */
    
    /* minimum values of fastest minimum-width signed integer types */
    #define INT_FAST8_MIN           (-128)
    #define INT_FAST16_MIN          (-32768)
    #define INT_FAST32_MIN          (~0x7fffffffL)
    //#define INT_FAST64_MIN          (~0x7fffffffffffffffLL)
    
    /* maximum values of fastest minimum-width signed integer types */
    #define INT_FAST8_MAX           (127)
    #define INT_FAST16_MAX          (32767)
    #define INT_FAST32_MAX          (2147483647L)
    //#define INT_FAST64_MAX          (9223372036854775807LL)
    
    /* maximum values of fastest minimum-width unsigned integer types */
    #define UINT_FAST8_MAX          (255)
    #define UINT_FAST16_MAX         (65535)
    #define UINT_FAST32_MAX         (4294967295UL)
    //#define UINT_FAST64_MAX         (18446744073709551615ULL)
    
    /* 7.18.2.4 */
    
    /* minimum value of pointer-holding signed integer type */
    #define INTPTR_MIN              (-32768)
    
    /* maximum value of pointer-holding signed integer type */
    #define INTPTR_MAX              (32767)
    
    /* maximum value of pointer-holding unsigned integer type */
    #define UINTPTR_MAX             (65535)
    
    /* 7.18.2.5 */
    
    /* minimum value of greatest-width signed integer type */
    #define INTMAX_MIN              (~0x7fffffffL)
    
    /* maximum value of greatest-width signed integer type */
    #define INTMAX_MAX              (2147483647L)
    
    /* maximum value of greatest-width unsigned integer type */
    #define UINTMAX_MAX             (4294967295UL)
    
    /* 7.18.3 */
    
    /* limits of ptrdiff_t */
    #define PTRDIFF_MIN             (-32768)
    #define PTRDIFF_MAX             (32767)
    
    /* limits of sig_atomic_t */
    #define SIG_ATOMIC_MIN          (-32768)
    #define SIG_ATOMIC_MAX          (32767) 
    
    /* limit of size_t */
    #define SIZE_MAX                (65535)
    
    /* limits of wchar_t */
    /* NB we have to undef and redef because they're defined in both
    * stdint.h and wchar.h */
    #undef WCHAR_MIN
    #undef WCHAR_MAX
    
    #if defined(__WCHAR32)
    #define WCHAR_MIN               (0)
    #define WCHAR_MAX               (0xffffffffUL)
    #else
    #define WCHAR_MIN               (0)
    #define WCHAR_MAX               (65535)
    #endif
    
    /* limits of wint_t */
    #define WINT_MIN                (~0x7fffffffL)
    #define WINT_MAX                (2147483647L)
    
    #endif /* __STDC_LIMIT_MACROS */
    
    
    #if !defined(__cplusplus) || defined(__STDC_CONSTANT_MACROS)
    
    /* 7.18.4.1 macros for minimum-width integer constants */
    #define INT8_C(x)   (x)
    #define INT16_C(x)  (x)
    #define INT32_C(x)  (x ## L)
    //#define INT64_C(x)  (x ## LL)
    
    #define UINT8_C(x)  (x ## U)
    #define UINT16_C(x) (x ## U)
    #define UINT32_C(x) (x ## UL)
    //#define UINT64_C(x) (x ## ULL)
    
    /* 7.18.4.2 macros for greatest-width integer constants */
    #define INTMAX_C(x)  (x ## L)
    #define UINTMAX_C(x) (x ## UL)
    
    #endif /* __STDC_CONSTANT_MACROS */    
    #endif /* __STDINT_DECLS */

    #ifndef __cplusplus
        #define true            (1==1)
        #define false           (1!=1)
        typedef unsigned char   bool;
    #endif /* __cplusplus */


#elif COMPILER_MCHP_C32
    #include <stddef.h>
    #include <stdint.h>
    #include <stdbool.h>

    #ifndef __cplusplus
        #ifndef true
            #define true            (1==1)
        #endif
        #ifndef false
            #define false           (1!=1)
        #endif
        #ifndef bool
            #ifdef _Bool
                #define bool	_Bool
            #else
                typedef int    bool;
            #endif
        #endif
    #endif /* __cplusplus */

#elif COMPILER_KEIL_REALVIEW
    #include <stddef.h>
    #include <stdint.h>
    #include <stdbool.h>
#elif COMPILER_GCC
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#else
#   include <stddef.h>
#   include <stdint.h>
#   include <stdbool.h>
#endif


/***************************************************************************************************
    Common user defined types 
 **************************************************************************************************/


#ifndef _U08_DEFINED
typedef uint8_t         U08;
#define _U08_DEFINED
#endif

#ifndef _U08_FAST_DEFINED
typedef uint_fast8_t    U08_FAST;
#define _U08_FAST_DEFINED
#endif

#ifndef _U16_DEFINED
typedef uint16_t        U16;
#define _U16_DEFINED
#endif

#ifndef _U16_FAST_DEFINED
typedef uint_fast16_t    U16_FAST;
#define _U16_FAST_DEFINED
#endif

#ifndef _U32_DEFINED
typedef uint32_t        U32;
#define _U32_DEFINED
#endif

#ifndef _U64_DEFINED
typedef uint64_t        U64;
#define _U64_DEFINED
#endif

#ifndef _S08_DEFINED
typedef int8_t          S08;
#define _S08_DEFINED
#endif

#ifndef _S08_FAST_DEFINED
typedef int_fast8_t     S08_FAST;
#define _S08_FAST_DEFINED
#endif

#ifndef _S16_DEFINED
typedef int16_t         S16;
#define _S16_DEFINED
#endif

#ifndef _S16_FAST_DEFINED
typedef int_fast16_t    S16_FAST;
#define _S16_FAST_DEFINED
#endif

#ifndef _S32_DEFINED
typedef int32_t         S32;
#define _S32_DEFINED
#endif

#ifndef _S64_DEFINED
typedef int64_t         S64;
#define _S64_DEFINED
#endif


//#ifndef _BOOL_DEFINED
//typedef int_fast8_t     BOOL;
//#define _BOOL_DEFINED
//#endif

//#ifndef TRUE
//#define TRUE    1
//#endif

//#ifndef FALSE
//#define FALSE   0
//#endif

#ifndef NULL
#define NULL  (void *)0
#endif

/***************************************************************************************************
    Useful definitions
 **************************************************************************************************/

#ifndef sizeofmember
#   define sizeofmember(s,m)        sizeof(((s *)0)->m)
#endif

#ifndef offsetof
    #define offsetof(s,m)           (size_t)&(((s *)0)->m)
#endif

#ifndef countof
    #define countof(a)              (sizeof(a) / sizeof(*(a)))
#endif

#ifndef alignof
    #define alignof(type)           offsetof(struct { char c; type member; }, member)
#endif

#define CSP_INT_DIV(a, b)           ((a) >= 0 ? (((a) + ((b) >> 1)) / (b)) : (((a) - ((b) >> 1)) / (b)) )
#define CSP_INT_DIV__POS(a, b)      (((a) + ((b) >> 1)) / (b))
#define CSP_INT_DIV__NEG(a, b)      (((a) - ((b) >> 1)) / (b))

#define CSP_UINT_DIV(a, b)          CSP_INT_DIV__POS(a, b)


#define _JOIN_2_H(A,B)              A##B
#define _JOIN_2(A,B)                _JOIN_2_H(A,B)
#define _STATIC_ASSERT_H(A)         typedef int _JOIN_2(compile_time_error_in_line_,__LINE__) [(A) ? 1 : -1]
#define STATIC_ASSERT(A)            _STATIC_ASSERT_H(A)




#endif /* __TYPES_H */
/***************************************************************************************************
    end of file: types.h
 **************************************************************************************************/
