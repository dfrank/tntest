/***************************************************************************************************
    Project:         PIC16F Family CPU Support Library
    Author:          [US] Ultrastar (�) 2008, http://www.ultrastar.ru

 ***************************************************************************************************
    Distribution:

    PIC16F Family Peripheral Support Library
    ----------------------------------------
    Copyright � 2007-2008 Ultrastar

 ***************************************************************************************************
    Compiled Using:  Hi-Tech PICC 9.60
    Processor:       PIC16F

 ***************************************************************************************************
    File:            csp_core.h
    Description:

 ***************************************************************************************************
    History:         10/06/2008 [US] File created

 **************************************************************************************************/

#ifndef __CSP_CORE_H
#define __CSP_CORE_H

/***************************************************************************************************
    Included Files
 **************************************************************************************************/
/***************************************************************************************************
    Definitions
 **************************************************************************************************/

/* STATUS register bits */

#if defined (__CSP_BASELINE)
    #define STAT_PB_WAKEUP      (1 << 7)
#endif

#define STAT_C      (1 << 0)
#define STAT_DC     (1 << 1)
#define STAT_Z      (1 << 2)

#define STAT_TO     (1 << 4)        /* 0 = A WDT time-out occurred               */
#define STAT_PD     (1 << 3)        /* 0 = By execution of the SLEEP instruction */

/* OPTION register bits */

#define CORE_TMR0_PS_1_2        (0 << 0)
#define CORE_TMR0_PS_1_4        (1 << 0)
#define CORE_TMR0_PS_1_8        (2 << 0)
#define CORE_TMR0_PS_1_16       (3 << 0)
#define CORE_TMR0_PS_1_32       (4 << 0)
#define CORE_TMR0_PS_1_64       (5 << 0)
#define CORE_TMR0_PS_1_128      (6 << 0)
#define CORE_TMR0_PS_1_256      (7 << 0)

#define CORE_WDT_PS_1_1         (0 << 0)
#define CORE_WDT_PS_1_2         (1 << 0)
#define CORE_WDT_PS_1_4         (2 << 0)
#define CORE_WDT_PS_1_8         (3 << 0)
#define CORE_WDT_PS_1_16        (4 << 0)
#define CORE_WDT_PS_1_32        (5 << 0)
#define CORE_WDT_PS_1_64        (6 << 0)
#define CORE_WDT_PS_1_128       (7 << 0)

#define CORE_WDT_PS_MASK        (7 << 0)

#define CORE_PS_WDT             (1 << 3)
#define CORE_PS_TMR0            (0 << 3)

#define CORE_TMR0_INC_FALL      (1 << 4)
#define CORE_TMR0_INC_RISE      (0 << 4)

#define CORE_TMR0_SOURCE_EXT    (1 << 5)
#define CORE_TMR0_SOURCE_INT    (0 << 5)


#if defined(__CSP_MIDRANGE)
    #define CORE_INT_FALL           (0 << 6)
    #define CORE_INT_RISE           (1 << 6)
#elif defined(__CSP_BASELINE)
    #if defined(_16F505)
        #define CORE_PULLUP_EN      (0 << 6)
        #define CORE_PULLUP_DIS     (1 << 6)
    #endif
#endif

#if defined(__CSP_MIDRANGE)
    #if defined(_16F689) || defined(_16F690)
        #define CORE_PORTAB_PULLUP_DIS  (1 << 7)
        #define CORE_PORTAB_PULLUP_EN   (0 << 7)
    #else
        #define CORE_PORTB_PULLUP_DIS   (1 << 7)
        #define CORE_PORTB_PULLUP_EN    (0 << 7)
    #endif 
#elif defined(__CSP_BASELINE)
    #define CORE_WAKEUP_DIS             (1 << 7)
    #define CORE_WAKEUP_EN              (0 << 7)
#endif


#if defined(__CSP_MIDRANGE)

    /* PCON register bits */
    
    #define CORE_POR_NOT            (1 << 1)
    #define CORE_BOD_NOT            (1 << 0)
    
    #define CORE_RESET_CAUSE_MASK   (CORE_POR_NOT | CORE_BOD_NOT)
    
    #if defined(_16F916) || defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)
        #define CORE_BOR_EN         (1 << 4)
        #define CORE_BOR_DIS        (0 << 4)
    #endif
    
    #if defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)
        #define CORE_ULPWU_EN       (1 << 5)
        #define CORE_ULPWU_DIS      (0 << 5)
    #endif

    /* WDTCON register bits */
    
    #if defined(_16F916) || defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)
    
    #define WDT_PS_1_32             ( 0 << 1)
    #define WDT_PS_1_64             ( 1 << 1)
    #define WDT_PS_1_128            ( 2 << 1)
    #define WDT_PS_1_256            ( 3 << 1)
    #define WDT_PS_1_512            ( 4 << 1)
    #define WDT_PS_1_1024           ( 5 << 1)
    #define WDT_PS_1_2048           ( 6 << 1)
    #define WDT_PS_1_4096           ( 7 << 1)
    #define WDT_PS_1_8192           ( 8 << 1)
    #define WDT_PS_1_16384          ( 9 << 1)
    #define WDT_PS_1_32768          (10 << 1)
    #define WDT_PS_1_65536          (11 << 1)
    
    #define WDT_SW_EN               (1 << 0)
    #define WDT_SW_DIS              (0 << 0)
    
    #endif

#endif


/* CONFIGURATION BITS */

#if   defined(_16F505)

    #define CONFIG_ADDR 0xFFF

    /* Protection of program code */

    #define CODE_PROTECT_DIS        0xFFF   // do not protect code
    #define CODE_PROTECT_EN         0xFEF   // protect code
    
    /* Oscillator configurations */

    #define OSC_EXTRC               0xFFF   // external resistor/capacitor oscillator, CLKOUT function on RB4/OSC2/CLKOUT pin
    #define OSC_EXTRC_IO            0xFFE   // external resistor capacitor oscillator, RB4 function on RB4/OSC2/CLKOUT pin
    #define OSC_INT                 0xFFD   // internal 4Mhz oscillator, CLKOUT function on RB4/OSC2/CLKOUT pin
    #define OSC_INT_IO              0xFFC   // internal 4MHz oscillator, RB4 function on RB4/OSC2/CLKOUT pin
    #define OSC_HS                  0xFFA   // high speed crystal/resonator
    #define OSC_XT                  0xFF9   // crystal/resonator
    #define OSC_LP                  0xFF8   // low power crystal/resonator
    
    /* WDT mode */

    #define WDT_EN                  0xFFF   // enable watchdog timer
    #define WDT_DIS                 0xFF7   // disable watchdog timer
    
    /* MCLR mode */

    #define MCLR_EN                 0xFFF   // enable master clear
    #define MCLR_DIS                0xFDF   // disbale master clear


#elif defined(_16F818) || defined(_16F819)

    #define CONFIG_ADDR 0x2007

    /* Protection of program code */

    #define CODE_READ_PROTECT_EN    0x1FFF
    #define CODE_READ_PROTECT_DIS   0x3FFF

    #define CODE_WRITE_PROTECT_DIS  0x3FFF
    #define CODE_WRITE_PROTECT_EN0  0x3DFF      /* Protect 000h to 01FFh */
    #define CODE_WRITE_PROTECT_EN1  0x3BFF      /* Protect 000h to 03FFh */
    #define CODE_WRITE_PROTECT_EN2  0x39FF      /* Protect 000h to 05FFh */

    /* Protection of data block */

    #define DATA_PROTECT_EN         0x3EFF
    #define DATA_PROTECT_DIS        0x3FFF

    /* CCP multiplex pin selection */

    #define CCP_MUX_RB2             0x3FFF
    #define CCP_MUX_RB3             0x2FFF

    /* In-Circuit Debugger Mode */

    #define DEBUG_EN                0x37FF
    #define DEBUG_DIS               0x3FFF

    /* Low voltage programming */

    #define LVP_EN                  0x3FFF
    #define LVP_DIS                 0x3F7F

    /* Brown-out detect modes */

    #define BOR_EN                  0x3FFF
    #define BOR_DIS                 0x3FBF

    /* MCLR mode */

    #define MCLR_EN                 0x3FFF
    #define MCLR_DIS                0x3FDF

    /* Power up timer mode */

    #define POWER_UP_TMR_EN         0x3FF7
    #define POWER_UP_TMR_DIS        0x3FFF

    /* WDT mode */

    #define WDT_EN                  0x3FFF
    #define WDT_DIS                 0x3FFB

    /* Oscillator configurations */

    #define OSC_RC                  0x3FFF
    #define OSC_RC_IO               0x3FFE
    #define OSC_INT                 0x3FFD
    #define OSC_INT_IO              0x3FFC
    #define OSC_EC                  0x3FEF
    #define OSC_HS                  0x3FEE
    #define OSC_XT                  0x3FED
    #define OSC_LP                  0x3FEC

#elif defined(_16F916)

    #define CONFIG_ADDR 0x2007

    /* Configuration bit definitions */

    /* Protection of program code */

    #define CODE_PROTECT_DIS        0x3FFF
    #define CODE_PROTECT_EN         0x3FBF

    /* Protection of data block */

    #define DATA_PROTECT_DIS        0x3FFF
    #define DATA_PROTECT_EN         0x3F7F

    /* In-Circuit Debugger Mode */

    #define DEBUG_EN                0x2FFF
    #define DEBUG_DIS               0x3FFF

    /* Brown-out detect modes */

    #define BOR_DIS                 0x3CFF  /* BOD and SBOREN disabled */
    #define BOR_SW_EN               0x3DFF  /* SBOREN controls BOR function (Software control) */
    #define BOR_RUN_EN              0x3EFF  /* BOD enabled in run, disabled in sleep, SBOREN disabled */
    #define BOR_EN                  0x3FFF  /* BOD Enabled, SBOREN Disabled */

    /* MCLR mode */

    #define MCLR_EN                 0x3FFF
    #define MCLR_DIS                0x3FDF

    /* Power up timer mode */

    #define POWER_UP_TMR_DIS        0x3FFF
    #define POWER_UP_TMR_EN         0x3FEF

    /* WDT mode */

    #define WDT_EN                  0x3FFF
    #define WDT_DIS                 0x3FF7

    /* Internal External Switch Over Mode */

    #define IESO_EN                 0x3FFF
    #define IESO_DIS                0x3BFF

    /* Monitor Clock Fail-safe */

    #define FSCM_EN                 0x3FFF
    #define FSCM_DIS                0x37FF

    /* Oscillator configurations */

    #define OSC_RC                  0x3FFF
    #define OSC_RC_IO               0x3FFE
    #define OSC_INT                 0x3FFD
    #define OSC_INT_IO              0x3FFC
    #define OSC_EC                  0x3FFB
    #define OSC_HS                  0x3FFA
    #define OSC_XT                  0x3FF9
    #define OSC_LP                  0x3FF8

#elif defined(_16F689) || defined(_16F690)

    #define CONFIG_ADDR 0x2007

    /* Configuration bit definitions */

    /* Protection of program code */

    #define CODE_PROTECT_DIS        0x3FFF
    #define CODE_PROTECT_EN         0x3FBF

    /* Protection of data block */

    #define DATA_PROTECT_DIS        0x3FFF
    #define DATA_PROTECT_EN         0x3F7F

    /* Brown-out detect modes */

    #define BOR_DIS                 0x3CFF  /* BOD and SBOREN disabled */
    #define BOR_SW_EN               0x3DFF  /* SBOREN controls BOR function (Software control) */
    #define BOR_RUN_EN              0x3EFF  /* BOD enabled in run, disabled in sleep, SBOREN disabled */
    #define BOR_EN                  0x3FFF  /* BOD Enabled, SBOREN Disabled */// 

    /* MCLR mode */

    #define MCLR_EN                 0x3FFF
    #define MCLR_DIS                0x3FDF

    /* Power up timer mode */

    #define POWER_UP_TMR_DIS        0x3FFF
    #define POWER_UP_TMR_EN         0x3FEF

    /* WDT mode */

    #define WDT_EN                  0x3FFF
    #define WDT_DIS                 0x3FF7

    /* Internal External Switch Over Mode */

    #define IESO_EN                 0x3FFF
    #define IESO_DIS                0x3BFF

    /* Monitor Clock Fail-safe */

    #define FSCM_EN                 0x3FFF
    #define FSCM_DIS                0x37FF

    /* Oscillator configurations */

    #define OSC_RC                  0x3FFF
    #define OSC_RC_IO               0x3FFE
    #define OSC_INT                 0x3FFD
    #define OSC_INT_IO              0x3FFC
    #define OSC_EC                  0x3FFB
    #define OSC_HS                  0x3FFA
    #define OSC_XT                  0x3FF9
    #define OSC_LP                  0x3FF8

#elif defined(_16F883) || defined(_16F886)

    /* Configuration bit definitions */

    #define CONFIG_ADDR_1           0x2007

    /* Protection of program code */

    #define CODE_PROTECT_DIS        0x3FFF
    #define CODE_PROTECT_EN         0x3FBF    

    /* Protection of data block */

    #define DATA_PROTECT_DIS        0x3FFF
    #define DATA_PROTECT_EN         0x3F7F

    /* Brown-out detect modes */

    #define BOR_DIS                 0x3CFF  /* BOD and SBOREN disabled */
    #define BOR_SW_EN               0x3DFF  /* SBOREN controls BOR function (Software control) */
    #define BOR_RUN_EN              0x3EFF  /* BOD enabled in run, disabled in sleep, SBOREN disabled */
    #define BOR_EN                  0x3FFF  /* BOD Enabled, SBOREN Disabled */// 

    /* MCLR mode */

    #define MCLR_EN                 0x3FFF
    #define MCLR_DIS                0x3FDF

    /* Power up timer mode */

    #define POWER_UP_TMR_DIS        0x3FFF
    #define POWER_UP_TMR_EN         0x3FEF

    /* WDT mode */

    #define WDT_EN                  0x3FFF
    #define WDT_DIS                 0x3FF7

    /* Internal External Switch Over Mode */

    #define IESO_EN                 0x3FFF
    #define IESO_DIS                0x3BFF

    /* Monitor Clock Fail-safe */

    #define FSCM_EN                 0x3FFF
    #define FSCM_DIS                0x37FF

    /* Low voltage programmig */

    #define LVP_DIS                 0x2FFF
    #define LVP_EN                  0x3FFF

    /* Debug module config */

    #define DEBUG_EN                0x1FFF
    #define DEBUG_DIS               0x3FFF

    /* Oscillator configurations */

    #define OSC_RC                  0x3FFF
    #define OSC_RC_IO               0x3FFE
    #define OSC_INT                 0x3FFD
    #define OSC_INT_IO              0x3FFC
    #define OSC_EC                  0x3FFB
    #define OSC_HS                  0x3FFA
    #define OSC_XT                  0x3FF9
    #define OSC_LP                  0x3FF8


    #define CONFIG_ADDR2                0x2008

    /* Brown-out reset voltage */

    #define BORV_21                         0x3EFF      /* 2.1 Volts */
    #define BORV_40                         0x3FFF      /* 4.0 Volts */

    /* Self-write flash protect */

    #define WR_PROTECT_EN0          0x3DFF      /* Protect 0x0000-0x00FF        */
    #define WR_PROTECT_EN1          0x3BFF      /* Protect 0x0000-0x03FF        */
    #define WR_PROTECT_EN2          0x39FF      /* Protect 0x0000-0x07FF        */
    #define WR_PROTECT_DIS          0x3FFF      /* Protect disabled             */

#else
#endif

/***************************************************************************************************
    Public Types
 **************************************************************************************************/

/***************************************************************************************************
    Global Variables
 **************************************************************************************************/

/***************************************************************************************************
    Public Function Prototypes
 **************************************************************************************************/

#define CSP_CONFIG(x)                               \
    asm("\tpsect config,class=CONFIG,delta=2");     \
    asm("\tdw "___mkstr(x))

#define NOP()                       asm("nop")

/**
 * Clear watch-dog timer
 */
#define csp_wdt_clear()             asm("clrwdt")
#define csp_wdt_check()             !(STATUS & STAT_TO)
#define csp_wdt_ps_set(a)           OPTION = ((OPTION & ~CORE_WDT_PS_MASK) | a)

#define csp_powerdw_check()         !(STATUS & STAT_PD)

#define csp_core_sleep()            asm("sleep")

#define csp_core_conf_set(a)        OPTION = (a)
#define csp_core_conf_get()         (OPTION)

#define csp_core_status_get()       (STATUS)

#if defined(__CSP_MIDRANGE)

    #define csp_core_reset_cond_get()   (PCON)
    #define csp_core_reset_cond_set(a)  PCON |= (a)
    
    #if defined(_16F916) || defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)
        #define csp_core_bor_en()       (PCON |=  CORE_BOR_EN)
        #define csp_core_bor_dis()      (PCON &= ~CORE_BOR_EN)
    
        #define csp_wdt_conf_set(a)     WDTCON = (a)
        #define csp_wdt_conf_get()      (WDTCON)
    #endif
    
    #define csp_core_eint_rise()        (OPTION |=  CORE_INT_RISE)
    #define csp_core_eint_fall()        (OPTION &= ~CORE_INT_RISE)
#endif


/***************************************************************************************************
    Included Files
 **************************************************************************************************/


#endif /* __CSP_CORE_H */
/***************************************************************************************************
    end of file: csp_core.h
 **************************************************************************************************/

