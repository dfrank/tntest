/***************************************************************************************************
    Project:         PIC16F Family CPU Support Library
    Author:          [US] Ultrastar (�) 2008, http://www.ultrastar.ru

 ***************************************************************************************************
    Distribution:

    PIC16F Family Peripheral Support Library
    ----------------------------------------
    Copyright � 2007-2008 Ultrastar

 ***************************************************************************************************
    Compiled Using:  Hi-Tech PICC 9.60
    Processor:       PIC16F

 ***************************************************************************************************
    File:            csp_gpio.h
    Description:

 ***************************************************************************************************
    History:         11/06/2008 [US] File created

 **************************************************************************************************/

#ifndef __CSP_GPIO_H
#define __CSP_GPIO_H

/***************************************************************************************************
    Included Files
 **************************************************************************************************/

/***************************************************************************************************
    Definitions
 **************************************************************************************************/

/* All PORTs and TRIS registers */

#define PIN_0           (1 << 0)
#define PIN_1           (1 << 1)
#define PIN_2           (1 << 2)
#define PIN_3           (1 << 3)
#define PIN_4           (1 << 4)
#define PIN_5           (1 << 5)
#define PIN_6           (1 << 6)
#define PIN_7           (1 << 7)

#define PIN_ALL         (0xFF)

/***************************************************************************************************
    Public Types
 **************************************************************************************************/

/***************************************************************************************************
    Global Variables
 **************************************************************************************************/

/***************************************************************************************************
    Public Function Prototypes
 **************************************************************************************************/

#if defined (__CSP_MIDRANGE)
    #define csp_gpio_dir_out(port, pin)     (port) &= ~(pin)
    #define csp_gpio_dir_in(port, pin)      (port) |= (pin)
#elif defined (__CSP_BASELINE)
    #define csp_gpio_dir_put(port, pin)     (port) = (pin)
#endif


#if defined(_16F916) || defined(_16F883) || defined(_16F886)
    #define csp_gpio_pullup_en(a)   WPUB |=  (a)
    #define csp_gpio_pullup_dis(a)  WPUB &= ~(a)

    #define csp_gpio_int_en(a)      IOCB |=  (a)
    #define csp_gpio_int_dis(a)     IOCB &= ~(a)
#endif

#if defined(_16F689) || defined(_16F690)
    #define csp_gpio_pullup_a_en(a)   WPUA |=  (a)
    #define csp_gpio_pullup_a_dis(a)  WPUA &= ~(a)

    #define csp_gpio_pullup_b_en(a)   WPUB |=  (a)
    #define csp_gpio_pullup_b_dis(a)  WPUB &= ~(a)

    #define csp_gpio_int_a_en(a)      IOCA |=  (a)
    #define csp_gpio_int_a_dis(a)     IOCA &= ~(a)

    #define csp_gpio_int_b_en(a)      IOCB |=  (a)
    #define csp_gpio_int_b_dis(a)     IOCB &= ~(a)
#endif

#define csp_gpio_set(port, pin)         (port) |=  (pin)
#define csp_gpio_clr(port, pin)         (port) &= ~(pin)
#define csp_gpio_put(port, pin)         (port)  = (pin)
#define csp_gpio_get(port, pin)         ((port) & (pin))

#define csp_gpio_port_get(port)         (port)
#define csp_gpio_port_set(port, val)    (port) = (val)

/***************************************************************************************************
    Included Files
 **************************************************************************************************/


#endif /* __CSP_GPIO_H */
/***************************************************************************************************
    end of file: csp_gpio.h
 **************************************************************************************************/
