/***************************************************************************************************
 *   Project:       P-0148
 *   Author:        auto_25
 ***************************************************************************************************
 *   Distribution:
 *
 ***************************************************************************************************
 *   MCU Family:    PIC16
 *   Compiler:      HT PICC
 ***************************************************************************************************
 *   File:          csp_comp.h
 *   Description:   Comparator module
 *
 ***************************************************************************************************
 *   History:       12.01.2009 - [auto_25] - file created
 *
 **************************************************************************************************/

#ifndef __CSP_COMP_H
#define __CSP_COMP_H

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/
/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

#if defined(_16F916)

    /* CMCON0 register bits */

    #define COMP_C2_OUT_BIT     (1 << 7)
    #define COMP_C1_OUT_BIT     (1 << 6)

    #define COMP_C2_INV_EN      (1 << 5)
    #define COMP_C2_INV_DIS     (0 << 5)

    #define COMP_C1_INV_EN      (1 << 4)
    #define COMP_C1_INV_DIS     (0 << 4)

    #define COMP_INP_MODE_0     (0 << 3)
    #define COMP_INP_MODE_1     (1 << 3)

    #define COMP_MODE_0         (0 << 0)
    #define COMP_MODE_1         (1 << 0)
    #define COMP_MODE_2         (2 << 0)
    #define COMP_MODE_3         (3 << 0)
    #define COMP_MODE_4         (4 << 0)
    #define COMP_MODE_5         (5 << 0)
    #define COMP_MODE_6         (6 << 0)
    #define COMP_MODE_OFF       (7 << 0)

    /* CMCON1 register bits */

    #define COMP_TMR1_C2_GATE_EN   (0 << 1)
    #define COMP_TMR1_C2_GATE_DIS  (1 << 1)

    #define COMP_TMR1_C2_SYNC_EN   (1 << 0)
    #define COMP_TMR1_C2_SYNC_DIS  (0 << 0)

    /* VRCON register bits */

    #define VR_EN               (1 << 7)
    #define VR_DIS              (0 << 7)

    #define VR_RANGE_LOW        (1 << 5)
    #define VR_RANGE_HIGH       (0 << 5)

    #define VR_VAL_MASK         (0x0F)

#elif defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)

    /* CM1CON0 and CM2CON0 registers bits */

    #define COMP_EN             (1 << 7)
    #define COMP_DIS            (0 << 7)

    #define COMP_OUT_BIT        (1 << 6)

    #define COMP_OUT_EN         (1 << 5)
    #define COMP_OUT_DIS        (0 << 5)

    #define COMP_INV_EN         (1 << 4)
    #define COMP_INV_DIS        (0 << 4)

    #define COMP_IN_P_VREF      (1 << 2)
    #define COMP_IN_P_CINP      (0 << 2)

    #define COMP_IN_M_CIN0      (0 << 0)
    #define COMP_IN_M_CIN1      (1 << 0)
    #define COMP_IN_M_CIN2      (2 << 0)
    #define COMP_IN_M_CIN3      (3 << 0)

    /* CM2CON1 register bits */

    #define COMP_C1_OUT_BIT     (1 << 7)
    #define COMP_C2_OUT_BIT     (1 << 6)

    #if defined(_16F883) || defined(_16F886)

        #define VR_C1_CVREF         (1 << 5)
        #define VR_C1_V06           (0 << 5)
    
        #define VR_C2_CVREF         (1 << 4)
        #define VR_C2_V06           (0 << 4)

    #endif

    #define COMP_TMR1_C2_GATE_EN   (0 << 1)
    #define COMP_TMR1_C2_GATE_DIS  (1 << 1)

    #define COMP_TMR1_C2_SYNC_EN   (1 << 0)
    #define COMP_TMR1_C2_SYNC_DIS  (0 << 0)

    /* SRCON register bits */

    #if defined(_16F883) || defined(_16F886)
        #define VR_V06_EN           (1 << 0)
        #define VR_V06_DIS          (0 << 0)
    #endif

    /* VRCON register bits */

    #if defined(_16F883) || defined(_16F886)
        #define VR_EN               (1 << 7)
        #define VR_DIS              (0 << 7)

        #define VR_OUT_EN           (1 << 6)
        #define VR_OUT_DIS          (0 << 6)

        #define VR_REF_VREFP_VREFM  (1 << 4)
        #define VR_REF_VDD_VSS      (0 << 4)

    #else
        #define VR_C1_CVREF         (1 << 7)
        #define VR_C1_V06           (0 << 7)

        #define VR_C2_CVREF         (1 << 6)
        #define VR_C2_V06           (0 << 6)

        #define VR_V06_EN           (1 << 4)
        #define VR_V06_DIS          (0 << 4)
    #endif

    #define VR_RANGE_LOW            (1 << 5)
    #define VR_RANGE_HIGH           (0 << 5)

    #define VR_VAL_MASK             (0x0F)

#endif


/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

#if defined(_16F916)

    #define csp_comp_conf0_set(a)        CMCON0 = (a)
    #define csp_comp_conf0_get()         (CMCON0)

    #define csp_comp_conf1_set(a)        CMCON1 = (a)
    #define csp_comp_conf1_get()         (CMCON1)

    #define csp_comp_get()               (CMCON0)

    #define csp_comp_ref_set(a)          VRCON = (a)
    #define csp_comp_ref_get()           (VRCON)


#elif (defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886))

    #define csp_comp1_conf_set(a)       CM1CON0 = (a)
    #define csp_comp1_conf_get()        (CM1CON0)

    #define csp_comp2_conf_set(a)       CM2CON0 = (a)
    #define csp_comp2_conf_get()        (CM2CON0)

    #define csp_comp_conf1_set(a)       CM2CON1 = (a)
    #define csp_comp_conf1_get()        (CM2CON1)
    
    #define csp_comp_get()              (CM2CON1)

    #define csp_comp_ref_set(a)         VRCON = (a)
    #define csp_comp_ref_get()          (VRCON)

    #define csp_comp_latch_set(a)       SRCON = (a)
    #define csp_comp_latch_get()        (SRCON)

#endif

#endif /* __CSP_COMP_H */
/***************************************************************************************************
 *  end of file: csp_comp.h
 **************************************************************************************************/


