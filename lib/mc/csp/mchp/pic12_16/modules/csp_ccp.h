/***************************************************************************************************
    Project:         PIC16F Family CPU Support Library
    Author:          [US] Ultrastar (�) 2008, http://www.ultrastar.ru

 ***************************************************************************************************
    Distribution:

    PIC16F Family Peripheral Support Library
    ----------------------------------------
    Copyright � 2007-2008 Ultrastar

 ***************************************************************************************************
    Compiled Using:  Hi-Tech PICC 9.60
    Processor:       PIC16F

 ***************************************************************************************************
    File:            csp_ccp.h
    Description:

 ***************************************************************************************************
    History:         11/06/2008 [US] File created

 **************************************************************************************************/

#ifndef __CSP_CCP_H
#define __CSP_CCP_H

/***************************************************************************************************
    Included Files
 **************************************************************************************************/
/***************************************************************************************************
    Definitions
 **************************************************************************************************/

/* CCP1CON and CCP2CON register bits */

#if defined(_16F690) || defined(_16F883) || defined(_16F886)
    #define CCP_SINGLE          ( 0 << 6)
    #define CCP_FBRIDGE_FRW     ( 1 << 6)
    #define CCP_HBRIDGE         ( 2 << 6)
    #define CCP_FBRIDGE_REV     ( 3 << 6)
#endif

#define CCP_MODE_DIS            ( 0 << 0)
#if defined(_16F690) || defined(_16F883) || defined(_16F886)
#define CCP_MODE_COMP_TGL       ( 2 << 0)
#endif
#define CCP_MODE_CAPT_FALL_1    ( 4 << 0)
#define CCP_MODE_CAPT_RISE_1    ( 5 << 0)
#define CCP_MODE_CAPT_RISE_4    ( 6 << 0)
#define CCP_MODE_CAPT_RISE_16   ( 7 << 0)
#define CCP_MODE_COMP_SET       ( 8 << 0)
#define CCP_MODE_COMP_CLR       ( 9 << 0)
#define CCP_MODE_COMP_INT       (10 << 0)
#define CCP_MODE_COMP_TRIG      (11 << 0)
#if defined(_16F690) || defined(_16F883) || defined(_16F886)
#define CCP_MODE_PWM_A_C_1_B_D_1  (12 << 0)
#define CCP_MODE_PWM_A_C_1_B_D_0  (13 << 0)
#define CCP_MODE_PWM_A_C_0_B_D_1  (14 << 0)
#define CCP_MODE_PWM_A_C_0_B_D_0  (15 << 0)
#endif

#define CCP_MODE_PWM            (12 << 0)   /* For all non Eccp modules */

#define CCP_MODE_MASK           (15 << 0)   /* Mask for checking current mode */

#define CCP_PR_LOW_MASK         (3 << 4)
#define CCP_PR_LOW_L            (1 << 4)
#define CCP_PR_LOW_H            (1 << 5)


/* ECCPAS register bits */

#if defined(_16F690) || defined(_16F883) || defined(_16F886)

    #define CCP_P1B_P1D_0       (0 << 0)
    #define CCP_P1B_P1D_1       (1 << 0)
    #define CCP_P1B_P1D_Z       (2 << 0)

    #define CCP_P1A_P1C_0       (0 << 2)
    #define CCP_P1A_P1C_1       (1 << 2)
    #define CCP_P1A_P1C_Z       (2 << 2)

    #define CCP_SHDN_DIS        (0 << 4)
    #define CCP_SHDN_COMP1      (1 << 4)
    #define CCP_SHDN_COMP2      (2 << 4)
    #define CCP_SHDN_COMP1_2    (3 << 4)
    #define CCP_SHDN_INT        (4 << 4)
    #define CCP_SHDN_INT_COMP1  (5 << 4)
    #define CCP_SHDN_INT_COMP2  (6 << 4)
    #define CCP_SHDN_INT_COMP1_2 (7 << 4)

    #define CCP_SHDN_TRUE       (1 << 7)

#endif

/* PWM1CON register bits */

#if defined(_16F690) || defined(_16F883) || defined(_16F886)

    #define CCP_PWM_DELAY_MASK  (0x7F)

    #define CCP_PWM_RESTART_EN  (1 << 7)
    #define CCP_PWM_RESTART_DIS (0 << 7)

#endif

/* PSTRCON register bits */

#if defined(_16F690) || defined(_16F883) || defined(_16F886)

    #define CCP_P1A_PWM         (1 << 0)
    #define CCP_P1A_GPIO        (0 << 0)

    #define CCP_P1B_PWM         (1 << 1)
    #define CCP_P1B_GPIO        (0 << 1)

    #define CCP_P1C_PWM         (1 << 2)
    #define CCP_P1C_GPIO        (0 << 2)

    #define CCP_P1D_PWM         (1 << 3)
    #define CCP_P1D_GPIO        (0 << 3)

    #define CCP_STEERING_PWM    (1 << 4)
    #define CCP_STEERING_CLK    (0 << 4)
#endif


/***************************************************************************************************
    Public Types
 **************************************************************************************************/
/***************************************************************************************************
    Global Variables
 **************************************************************************************************/
/***************************************************************************************************
    Public Function Prototypes
 **************************************************************************************************/

#define csp_ccp1_conf_set(a)        CCP1CON = (a)
#define csp_ccp1_conf_get()         (CCP1CON)

#define csp_ccp1_mode_set(a)        CCP1CON = ((CCP1CON & 0xF0) | (a))
#define csp_ccp1_mode_get()         (CCP1CON & 0x0F)

#define csp_ccp1_H_set(a)           CCPR1H = (a)
#define csp_ccp1_H_get()            (CCPR1H)

#define csp_ccp1_L_set(a)           CCPR1L = (a)
#define csp_ccp1_L_get()            (CCPR1L)

#define csp_ccp1_pwm_duty_set(a)    CCPR1L = ((a) >> 2);                            \
                                    if ((a) & (1 << 0)) CCP1CON |=  CCP_PR_LOW_L;   \
                                    else                CCP1CON &= ~CCP_PR_LOW_L;   \
                                    if ((a) & (1 << 1)) CCP1CON |=  CCP_PR_LOW_H;   \
                                    else                CCP1CON &= ~CCP_PR_LOW_H

#if defined(_16F883) || defined(_16F886)

#define csp_ccp2_conf_set(a)        CCP2CON = (a)
#define csp_ccp2_conf_get()         (CCP2CON)

#define csp_ccp2_mode_set(a)        CCP2CON = ((CCP2CON & 0xF0) | (a))
#define csp_ccp2_mode_get()         (CCP2CON & 0x0F)

#define csp_ccp2_H_set(a)           CCPR2H = (a)
#define csp_ccp2_H_get()            (CCPR2H)

#define csp_ccp2_L_set(a)           CCPR2L = (a)
#define csp_ccp2_L_get()            (CCPR2L)

#define csp_ccp2_pwm_duty_set(a)    CCPR2L = ((a) >> 2);                            \
                                    if ((a) & (1 << 0)) CCP2CON |=  CCP_PR_LOW_L;   \
                                    else                CCP2CON &= ~CCP_PR_LOW_L;   \
                                    if ((a) & (1 << 1)) CCP2CON |=  CCP_PR_LOW_H;   \
                                    else                CCP2CON &= ~CCP_PR_LOW_H
#endif


#if defined(_16F690) || defined(_16F883) || defined(_16F886)
#define csp_ccp1_pwm_conf(a)        PSTRCON = (a)
#endif

/***************************************************************************************************
    Included Files
 **************************************************************************************************/

#endif /* __CSP_CCP_H */
/***************************************************************************************************
    end of file: csp_ccp.h
 **************************************************************************************************/
