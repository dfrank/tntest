/***************************************************************************************************
    Project:         PIC16F Family CPU Support Library
    Author:          [US] Ultrastar (�) 2008, http://www.ultrastar.ru

 ***************************************************************************************************
    Distribution:

    PIC16F Family Peripheral Support Library
    ----------------------------------------
    Copyright � 2007-2008 Ultrastar

 ***************************************************************************************************
    Compiled Using:  Hi-Tech PICC 9.60
    Processor:       PIC16F

 ***************************************************************************************************
    File:            csp_ssp.h
    Description:

 ***************************************************************************************************
    History:         11/06/2008 [US] File created

 **************************************************************************************************/

#ifndef __CSP_SSP_H
#define __CSP_SSP_H

/***************************************************************************************************
    Included Files
 **************************************************************************************************/
/***************************************************************************************************
    Definitions
 **************************************************************************************************/

/* SSPSTAT register bits */

#define SSP_INP_SAMP_END            ( 1 << 7)
#define SSP_INP_SAMP_MID            ( 0 << 7)
                                    
#define SSP_OUT_RISE                ( 1 << 6)        /* CKE */
#define SSP_OUT_FALL                ( 0 << 6)
                                    
#define SSP_STAT_REC_DATA           ( 1 << 5)
#define SSP_STAT_STOP               ( 1 << 4)
#define SSP_STAT_START              ( 1 << 3)
#define SSP_STAT_READ               ( 1 << 2)
#define SSP_STAT_UPDATE_ADDR        ( 1 << 1)
#define SSP_STAT_BUFF_FULL          ( 1 << 0)
                                    
/* SSPCON register bits */          
                                    
#define SSP_STAT_WRITE_COLL         ( 1 << 7)
#define SSP_STAT_OVFL               ( 1 << 6)
                                    
#define SSP_EN                      ( 1 << 5)
#define SSP_DIS                     ( 0 << 5)
                                    
#define SSP_POLAR_0                 ( 1 << 4)        /* CKP */
#define SSP_POLAR_1                 ( 0 << 4)

#define SSP_MODE_SPI_MS_FOSC_4      ( 0 << 0)   /* SPI Master mode, clock = FOSC/4 */
#define SSP_MODE_SPI_MS_FOSC_16     ( 1 << 0)   /* SPI Master mode, clock = FOSC/16 */
#define SSP_MODE_SPI_MS_FOSC_64     ( 2 << 0)   /* SPI Master mode, clock = FOSC/64 */
#define SSP_MODE_SPI_MS_TMR2_2      ( 3 << 0)   /* SPI Master mode, clock = TMR2 output/2 */
#define SSP_MODE_SPI_SL_SS_EN       ( 4 << 0)   /* SPI Slave mode, clock = SCK pin. SS pin control enabled */
#define SSP_MODE_SPI_SL_SS_DIS      ( 5 << 0)   /* SPI Slave mode, clock = SCK pin. SS pin control disabled. SS can be used as I/O pin */
#define SSP_MODE_I2C_SL_7_ADD_0     ( 6 << 0)   /* I2C Slave mode, 7-bit address */
#define SSP_MODE_I2C_SL_10_ADD_0    ( 7 << 0)   /* I2C Slave mode, 10-bit address */
#if defined(_16F883) || defined(_16F886)
#define SSP_MODE_I2C_MS_HW          ( 8 << 0)   /* I2C Master mode, clock = FOSC / (4 * (SSPADD+1)) */
#endif
#if defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)
#define SSP_MODE_LOAD_SSPMSK        ( 9 << 0)   /* Load SSPMSK register at SSPADD SFR address */
#endif
#define SSP_MODE_I2C_MS_FW          (11 << 0)   /* I2C Firmware Controlled Master mode (slave IDLE) */
#define SSP_MODE_I2C_SL_7_ADD_1     (14 << 0)   /* I2C Slave mode, 7-bit address with Start and Stop bit interrupts enabled */
#define SSP_MODE_I2C_SL_10_ADD_1    (15 << 0)   /* I2C Slave mode, 10-bit address with Start and Stop bit interrupts enabled */


#if defined(_16F883) || defined(_16F886)

    /* SSPCON2 register bits */

    #define SSP_I2C_GEN_CALL_EN     (1 << 7)    /* Enable interrupt when a general call address (0000h) is received in the SSPSR */
    #define SSP_I2C_GEN_CALL_DIS    (0 << 7)    /* General call address disabled */

    #define SSP_I2C_ACK_STAT        (1 << 6)

    #define SSP_I2C_ACK_EN          (0 << 5)    /* Acknowledge      */
    #define SSP_I2C_ACK_DIS         (1 << 5)    /* Not Acknowledge  */

    #define SSP_I2C_CMD_ACK         (1 << 4)    /* Initiate Acknowledge sequence on SDA and SCL pins, and transmit ACKDT data bit. */
    #define SSP_I2C_CMD_REC         (1 << 3)    /* Enables Receive mode for I2C */
    #define SSP_I2C_CMD_STOP        (1 << 2)    /* Initiate Stop condition on SDA and SCL pins. Automatically cleared by hardware */
    #define SSP_I2C_CMD_RESTART     (1 << 1)    /* Initiate Repeated Start condition on SDA and SCL pins. Automatically cleared by hardware. */
    #define SSP_I2C_CMD_START       (1 << 0)    /* Initiate Start condition on SDA and SCL pins. Automatically cleared by hardware */


#endif


/***************************************************************************************************
    Public Types
 **************************************************************************************************/
/***************************************************************************************************
    Global Variables
 **************************************************************************************************/
/***************************************************************************************************
    Public Function Prototypes
 **************************************************************************************************/

#define csp_ssp_stat0_get()         (SSPSTAT)
#define csp_ssp_stat1_get()         (SSPCON)

#define csp_ssp_conf0_set(a)        SSPSTAT = (a)
#define csp_ssp_conf0_get()         (SSPSTAT)

#define csp_ssp_conf1_set(a)        SSPCON  = (a)
#define csp_ssp_conf1_get()         (SSPCON)

#if defined(_16F883) || defined(_16F886)
    #define csp_ssp_conf2_set(a)    SSPCON2  = (a)
    #define csp_ssp_conf2_get()     (SSPCON2)

    #define csp_ssp_cmd(a)          SSPCON2 |= (a)
#endif


#define csp_ssp_addr_set(a)         SSPADD = (a)

#define csp_ssp_put(a)              SSPBUF = (a)
#define csp_ssp_get()               (SSPBUF)

#define csp_ssp_en()                SSPCON |=  SSP_EN
#define csp_ssp_dis()               SSPCON &= ~SSP_EN


/***************************************************************************************************
    Included Files
 **************************************************************************************************/

#endif /* __CSP_SSP_H */
/***************************************************************************************************
    end of file: csp_ssp.h
 **************************************************************************************************/
