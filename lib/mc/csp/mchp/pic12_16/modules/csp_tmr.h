/***************************************************************************************************
    Project:         PIC16F Family CPU Support Library
    Author:          [US] Ultrastar (�) 2008, http://www.ultrastar.ru

 ***************************************************************************************************
    Distribution:

    PIC16F Family Peripheral Support Library
    ----------------------------------------
    Copyright � 2007-2008 Ultrastar

 ***************************************************************************************************
    Compiled Using:  Hi-Tech PICC 9.60
    Processor:       PIC16F

 ***************************************************************************************************
    File:            csp_tmr.h
    Description:

 ***************************************************************************************************
    History:         11/06/2008 [US] File created

 **************************************************************************************************/

#ifndef __CSP_TMR_H
#define __CSP_TMR_H

/***************************************************************************************************
    Included Files
 **************************************************************************************************/

/***************************************************************************************************
    Definitions
 **************************************************************************************************/

#if defined(__CSP_MIDRANGE)

    /* T1CON register bits */
    
    
    #if defined(_16F916) || defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)
        #define TMR1_GATE_INV_EN    (1 << 7)
        #define TMR1_GATE_INV_DIS   (0 << 7)
    
        #define TMR1_GATE_EN        (1 << 6)
        #define TMR1_GATE_DIS       (0 << 6)
    #endif
    
    #define TMR1_PS_1_1         (0 << 4)
    #define TMR1_PS_1_2         (1 << 4)
    #define TMR1_PS_1_4         (2 << 4)
    #define TMR1_PS_1_8         (3 << 4)
    
    #define TMR1_EXT_OSC_EN     (1 << 3)
    #define TMR1_EXT_OSC_DIS    (0 << 3)
    
    #define TMR1_EXT_SYNC_EN    (0 << 2)
    #define TMR1_EXT_SYNC_DIS   (1 << 2)
    
    #define TMR1_SOURCE_INT     (0 << 1)
    #define TMR1_SOURCE_EXT     (1 << 1)
    
    #define TMR1_EN             (1 << 0)
    #define TMR1_DIS            (0 << 0)
    
    /* T2CON register bits */
    
    #if !defined(_16F689)
        #define TMR2_OUTPS_1_1      ( 0 << 3)
        #define TMR2_OUTPS_1_2      ( 1 << 3)
        #define TMR2_OUTPS_1_3      ( 2 << 3)
        #define TMR2_OUTPS_1_4      ( 3 << 3)
        #define TMR2_OUTPS_1_5      ( 4 << 3)
        #define TMR2_OUTPS_1_6      ( 5 << 3)
        #define TMR2_OUTPS_1_7      ( 6 << 3)
        #define TMR2_OUTPS_1_8      ( 7 << 3)
        #define TMR2_OUTPS_1_9      ( 8 << 3)
        #define TMR2_OUTPS_1_10     ( 9 << 3)
        #define TMR2_OUTPS_1_11     (10 << 3)
        #define TMR2_OUTPS_1_12     (11 << 3)
        #define TMR2_OUTPS_1_13     (12 << 3)
        #define TMR2_OUTPS_1_14     (13 << 3)
        #define TMR2_OUTPS_1_15     (14 << 3)
        #define TMR2_OUTPS_1_16     (15 << 3)
        
        #define TMR2_EN             (1 << 2)
        #define TMR2_DIS            (0 << 2)
        
        #define TMR2_PS_1_1         (0 << 0)
        #define TMR2_PS_1_4         (1 << 0)
        #define TMR2_PS_1_16        (2 << 0)
    #endif

#endif

/***************************************************************************************************
    Public Types
 **************************************************************************************************/

/***************************************************************************************************
    Global Variables
 **************************************************************************************************/

/***************************************************************************************************
    Public Function Prototypes
 **************************************************************************************************/

#define csp_tmr0_set(a)         TMR0 = (a)
#define csp_tmr0_get()          (TMR0)

#if defined (__CSP_MIDRANGE)

    #define csp_tmr1_conf_set(a)    T1CON = (a)
    #define csp_tmr1_conf_get()     (T1CON)
    
    #define csp_tmr1l_get()         (TMR1L)
    #define csp_tmr1h_get()         (TMR1H)
    
    #define csp_tmr1l_set(a)        TMR1L = (a)
    #define csp_tmr1h_set(a)        TMR1H = (a)
    
    #define csp_tmr1_en()           T1CON |=  TMR1_EN
    #define csp_tmr1_dis()          T1CON &= ~TMR1_EN
    
    #if !defined(_16F689)    
    
        #define csp_tmr2_conf_set(a)    T2CON = (a)
        #define csp_tmr2_conf_get()     (T2CON)
        
        #define csp_tmr2_set(a)         TMR2 = (a)
        #define csp_tmr2_get()          (TMR2)
        
        #define csp_tmr2_period_set(a)  PR2 = (a)
        #define csp_tmr2_period_get()   (PR2)
        
        #define csp_tmr2_en()           T2CON |=  TMR2_EN
        #define csp_tmr2_dis()          T2CON &= ~TMR2_EN
    #endif

#endif

/***************************************************************************************************
    Included Files
 **************************************************************************************************/

#endif /* __CSP_TMR_H */
/***************************************************************************************************
    end of file: csp_tmr.h
 **************************************************************************************************/
