/***************************************************************************************************
    Project:         PIC16F Family CPU Support Library
    Author:          [US] Ultrastar (�) 2008, http://www.ultrastar.ru

 ***************************************************************************************************
    Distribution:

    PIC16F Family Peripheral Support Library
    ----------------------------------------
    Copyright � 2007-2008 Ultrastar

 ***************************************************************************************************
    Compiled Using:  Hi-Tech PICC 9.60
    Processor:       PIC16F

 ***************************************************************************************************
    File:            csp_int.h
    Description:

 ***************************************************************************************************
    History:         11/06/2008 [US] File created

 **************************************************************************************************/

#ifndef __CSP_INT_H
#define __CSP_INT_H

/***************************************************************************************************
    Included Files
 **************************************************************************************************/

/***************************************************************************************************
    Definitions
 **************************************************************************************************/

/* INTCON register bits */

#define INT_GLOBAL          (1 << 7)
#define INT_PERIPH          (1 << 6)
#define INT_TMR0            (1 << 5)
#define INT_EINT            (1 << 4)

#if defined(_16F689) || defined(_16F690)
    #define INT_PORTAB      (1 << 3)
#else
    #define INT_PORTB       (1 << 3)
#endif

#define INT_TMR0_FLAG       (INT_TMR0  >> 3)
#define INT_EINT_FLAG       (INT_EINT  >> 3)

#if defined(_16F689) || defined(_16F690)
    #define INT_PORTAB_FLAG (INT_PORTAB >> 3)
#else
    #define INT_PORTB_FLAG  (INT_PORTB >> 3)
#endif

/* PIE1 and PIR1 register bits */

#if defined(_16F916)
     #define INT_EEPROM     (1 << 7)
#endif

#if defined(_16F916) || defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)

    #define INT_UART_RX     (1 << 5)
    #define INT_UART_TX     (1 << 4)

#endif

#define INT_ADC             (1 << 6)
#define INT_SSP             (1 << 3)
#define INT_CCP1            (1 << 2)
#define INT_TMR2            (1 << 1)
#define INT_TMR1            (1 << 0)

/* PIE2 and PIR2 register bits */

#if defined(_16F916)
    #define INT_LCD         (1 << 4)
    #define INT_LVD         (1 << 2)
#endif

#if defined(_16F883) || defined(_16F886)
    #define INT_BCOLL       (1 << 3)
    #define INT_ULPWU       (1 << 2)
    #define INT_CCP2        (1 << 0)
#endif

#if defined(_16F916) || defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)
    #define INT_OSCF        (1 << 7)
    #define INT_COMP2       (1 << 6)
    #define INT_COMP1       (1 << 5)
#endif

#if defined (_16F818) || defined (_16F819) || defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)
    #define INT_EEPROM      (1 << 4)
#endif

static bit GIE @ ((unsigned)&INTCON*8)+7;

/***************************************************************************************************
    Public Types
 **************************************************************************************************/

/***************************************************************************************************
    Global Variables
 **************************************************************************************************/

/***************************************************************************************************
    Public Function Prototypes
 **************************************************************************************************/

#define csp_int_core_en(a)          INTCON |= (a)
#define csp_int_core_dis(a)         INTCON &= ~(a)
#define csp_int_core_check(a)       (INTCON & (a))
#define csp_int_core_clr(a)         INTCON &= ~(a)

#define csp_int_per_en(pie, a)      (pie) |= (a)
#define csp_int_per_dis(pie, a)     (pie) &= ~(a)
#define csp_int_per_check(pir, a)   ((pir) & (a))
#define csp_int_per_clr(pir, a)     (pir) &= ~(a)

#define csp_int_en()                INTCON |= INT_GLOBAL
#define csp_int_dis()               INTCON &= ~INT_GLOBAL

/***************************************************************************************************
    Included Files
 **************************************************************************************************/


#endif /* __CSP_INT_H */
/***************************************************************************************************
    end of file: csp_int.h
 **************************************************************************************************/

