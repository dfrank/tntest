/***************************************************************************************************
    Project:         PIC16F Family CPU Support Library
    Author:          [US] Ultrastar (�) 2008, http://www.ultrastar.ru

 ***************************************************************************************************
    Distribution:

    PIC16F Family Peripheral Support Library
    ----------------------------------------
    Copyright � 2007-2008 Ultrastar

 ***************************************************************************************************
    Compiled Using:  Hi-Tech PICC 9.60
    Processor:       PIC16F

 ***************************************************************************************************
    File:            csp_adc.h
    Description:

 ***************************************************************************************************
    History:         11/06/2008 [US] File created

 **************************************************************************************************/

#ifndef __CSP_ADC_H
#define __CSP_ADC_H

/***************************************************************************************************
    Included Files
 **************************************************************************************************/

/***************************************************************************************************
    Definitions
 **************************************************************************************************/

/* ADCON0 register bits */

#if   defined(_16F818) || defined(_16F819)

    #define ADC_CLOCK_FOSC_2        (0 << 14)
    #define ADC_CLOCK_FOSC_8        (1 << 14)
    #define ADC_CLOCK_FOSC_32       (2 << 14)
    #define ADC_CLOCK_INT_RC        (3 << 14)

    #define ADC_CHAN_AN0            (0 << 11)
    #define ADC_CHAN_AN1            (1 << 11)
    #define ADC_CHAN_AN2            (2 << 11)
    #define ADC_CHAN_AN3            (3 << 11)
    #define ADC_CHAN_AN4            (4 << 11)

    #define ADC_CHAN_SEL_MASK       (0x07 << 11)

    #define ADC_GO                  (1 << 10)

#elif defined(_16F916)

    #define ADC_RESULT_RIGHT        (1 << 15)
    #define ADC_RESULT_LEFT         (0 << 15)

    #define ADC_REF_VREF_MIN        (1 << 14)
    #define ADC_REF_VSS             (0 << 14)

    #define ADC_REF_VREF_PL         (1 << 13)
    #define ADC_REF_VDD             (0 << 13)

    #define ADC_CHAN_AN0            (0 << 10)
    #define ADC_CHAN_AN1            (1 << 10)
    #define ADC_CHAN_AN2            (2 << 10)
    #define ADC_CHAN_AN3            (3 << 10)
    #define ADC_CHAN_AN4            (4 << 10)
    #define ADC_CHAN_AN5            (5 << 10)
    #define ADC_CHAN_AN6            (6 << 10)
    #define ADC_CHAN_AN7            (7 << 10)

    #define ADC_CHAN_SEL_MASK       (0x07 << 10)

    #define ADC_GO                  (1 << 9)

#elif defined(_16F689) || defined(_16F690)

    #define ADC_RESULT_RIGHT        (1 << 15)
    #define ADC_RESULT_LEFT         (0 << 15)
    
    #define ADC_REF_VREF            (1 << 14)
    #define ADC_REF_VDD             (0 << 14)    

    #define ADC_CHAN_AN0            ( 0 << 10)
    #define ADC_CHAN_AN1            ( 1 << 10)
    #define ADC_CHAN_AN2            ( 2 << 10)
    #define ADC_CHAN_AN3            ( 3 << 10)
    #define ADC_CHAN_AN4            ( 4 << 10)
    #define ADC_CHAN_AN5            ( 5 << 10)
    #define ADC_CHAN_AN6            ( 6 << 10)
    #define ADC_CHAN_AN7            ( 7 << 10)
    #define ADC_CHAN_AN8            ( 8 << 10)
    #define ADC_CHAN_AN9            ( 9 << 10)
    #define ADC_CHAN_AN10           (10 << 10)
    #define ADC_CHAN_AN11           (11 << 10)
    #define ADC_CHAN_CVREF          (12 << 10)
    #define ADC_CHAN_0_6V           (13 << 10)

    #define ADC_CHAN_SEL_MASK       (0x0F << 10)

    #define ADC_GO                  (1 << 9)

#elif defined(_16F883) || defined(_16F886)

    #define ADC_CLOCK_FOSC_2        (0 << 14)
    #define ADC_CLOCK_FOSC_8        (1 << 14)
    #define ADC_CLOCK_FOSC_32       (2 << 14)
    #define ADC_CLOCK_INT_RC        (3 << 14)

    #define ADC_CHAN_AN0            ( 0 << 10)
    #define ADC_CHAN_AN1            ( 1 << 10)
    #define ADC_CHAN_AN2            ( 2 << 10)
    #define ADC_CHAN_AN3            ( 3 << 10)
    #define ADC_CHAN_AN4            ( 4 << 10)
    #define ADC_CHAN_AN5            ( 5 << 10)
    #define ADC_CHAN_AN6            ( 6 << 10)
    #define ADC_CHAN_AN7            ( 7 << 10)
    #define ADC_CHAN_AN8            ( 8 << 10)
    #define ADC_CHAN_AN9            ( 9 << 10)
    #define ADC_CHAN_AN10           (10 << 10)
    #define ADC_CHAN_AN11           (11 << 10)
    #define ADC_CHAN_AN12           (12 << 10)
    #define ADC_CHAN_AN13           (13 << 10)
    #define ADC_CHAN_CVREF          (14 << 10)
    #define ADC_CHAN_0_6V           (15 << 10)

    #define ADC_CHAN_SEL_MASK       (0x0F << 10)

    #define ADC_GO                  (1 << 9)
#endif


#define ADC_EN                      (1 << 8)
#define ADC_DIS                     (0 << 8)

/* ADCON1 register bits */

#if   defined(_16F818) || defined(_16F819)

    #define ADC_RESULT_RIGHT        (1 << 7)
    #define ADC_RESULT_LEFT         (0 << 7)

    #define ADC_CLOCK_1_2           (1 << 6)
    #define ADC_CLOCK_1_1           (0 << 6)

    #define ADC_PORT_CONF_0         ( 0 << 0)
    #define ADC_PORT_CONF_1         ( 1 << 0)
    #define ADC_PORT_CONF_2         ( 2 << 0)
    #define ADC_PORT_CONF_3         ( 3 << 0)
    #define ADC_PORT_CONF_4         ( 4 << 0)
    #define ADC_PORT_CONF_5         ( 5 << 0)
    #define ADC_PORT_CONF_6         ( 6 << 0)
    #define ADC_PORT_CONF_7         ( 7 << 0)
    #define ADC_PORT_CONF_8         ( 8 << 0)
    #define ADC_PORT_CONF_9         ( 9 << 0)
    #define ADC_PORT_CONF_10        (10 << 0)
    #define ADC_PORT_CONF_11        (11 << 0)
    #define ADC_PORT_CONF_12        (12 << 0)
    #define ADC_PORT_CONF_13        (13 << 0)
    #define ADC_PORT_CONF_14        (14 << 0)
    #define ADC_PORT_CONF_15        (15 << 0)

#elif defined(_16F916) || defined(_16F689) || defined(_16F690)

    #define ADC_CLOCK_FOSC_2        (0 << 4)
    #define ADC_CLOCK_FOSC_4        (4 << 4)
    #define ADC_CLOCK_FOSC_8        (1 << 4)
    #define ADC_CLOCK_FOSC_16       (5 << 4)
    #define ADC_CLOCK_FOSC_32       (2 << 4)
    #define ADC_CLOCK_FOSC_64       (6 << 4)
    #define ADC_CLOCK_INT_RC        (7 << 4)

#elif defined(_16F883) || defined(_16F886)

    #define ADC_RESULT_RIGHT        (1 << 7)
    #define ADC_RESULT_LEFT         (0 << 7)

    #define ADC_VREF_M_VREF         (1 << 5)
    #define ADC_VREF_M_VSS          (0 << 5)

    #define ADC_VREF_P_VREF         (1 << 4)
    #define ADC_VREF_P_VDD          (0 << 4)

#endif

/* ANSEL register bits */

#if defined(_16F916)

    #define ADC_AN0                 (0 << 0)
    #define ADC_AN1                 (1 << 0)
    #define ADC_AN2                 (2 << 0)
    #define ADC_AN3                 (3 << 0)
    #define ADC_AN4                 (4 << 0)
    #define ADC_AN5                 (5 << 0)
    #define ADC_AN6                 (6 << 0)
    #define ADC_AN7                 (7 << 0)

    #define ADC_AN_ALL              (0xFF)

#elif defined(_16F689) || defined(_16F690)

    #define ADC_AN0                 ( 0 << 0)
    #define ADC_AN1                 ( 1 << 0)
    #define ADC_AN2                 ( 2 << 0)
    #define ADC_AN3                 ( 3 << 0)
    #define ADC_AN4                 ( 4 << 0)
    #define ADC_AN5                 ( 5 << 0)
    #define ADC_AN6                 ( 6 << 0)
    #define ADC_AN7                 ( 7 << 0)
    #define ADC_AN8                 ( 8 << 0)
    #define ADC_AN9                 ( 9 << 0)
    #define ADC_AN10                (10 << 0)
    #define ADC_AN11                (11 << 0)
      
    #define ADC_AN_ALL              (0x0FFF)

#elif defined(_16F883) || defined(_16F886)

    #define ADC_AN0                 ( 0 << 0)
    #define ADC_AN1                 ( 1 << 0)
    #define ADC_AN2                 ( 2 << 0)
    #define ADC_AN3                 ( 3 << 0)
    #define ADC_AN4                 ( 4 << 0)
    #define ADC_AN5                 ( 5 << 0)
    #define ADC_AN6                 ( 6 << 0)
    #define ADC_AN7                 ( 7 << 0)
    #define ADC_AN8                 ( 8 << 0)
    #define ADC_AN9                 ( 9 << 0)
    #define ADC_AN10                (10 << 0)
    #define ADC_AN11                (11 << 0)
    #define ADC_AN12                (12 << 0)
    #define ADC_AN13                (13 << 0)
      
    #define ADC_AN_ALL              (0xFFFF)
#endif


/***************************************************************************************************
    Public Types
 **************************************************************************************************/

/***************************************************************************************************
    Global Variables
 **************************************************************************************************/

/***************************************************************************************************
    Public Function Prototypes
 **************************************************************************************************/

#define csp_adc_conf_set(a)     ADCON0 = (a) >> 8; ADCON1 = ((a) & 0xFF)

#if defined(_16F916)
    #define csp_adc_an_en(a)    ANSEL |=  (a)
    #define csp_adc_an_dis(a)   ANSEL &= ~(a)
#elif defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)
    #define csp_adc_an_en(a)    ANSEL |=  ((a) & 0xFF); ANSELH |=  ((a) >> 8)
    #define csp_adc_an_dis(a)   ANSEL &= ~((a) & 0xFF); ANSELH &= ~((a) >> 8)
#endif

#define csp_adc_en()            ADCON0 |=  (ADC_EN >> 8)
#define csp_adc_dis()           ADCON0 &= ~(ADC_EN >> 8)

#define csp_adc_chan_set(a)     ADCON0 = (ADCON0 & ~(ADC_CHAN_SEL_MASK >> 8)) | ((a) >> 8)
#define csp_adc_chan_get()      (ADCON0 & (ADC_CHAN_SEL_MASK >> 8))

#define csp_adc_start()         ADCON0 |= (ADC_GO >> 8)
#define csp_adc_process_check() (ADCON0 & (ADC_GO >> 8))

#define csp_adc_get()           ((U16)ADRESL | (U16)(ADRESH << 8))


/***************************************************************************************************
    Included Files
 **************************************************************************************************/

#endif /* __CSP_ADC_H */
/***************************************************************************************************
    end of file: csp_adc.h
 **************************************************************************************************/
