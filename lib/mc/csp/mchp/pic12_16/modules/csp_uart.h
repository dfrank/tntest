/***************************************************************************************************
    Project:         PIC16F Family CPU Support Library
    Author:          [US] Ultrastar (�) 2008, http://www.ultrastar.ru

 ***************************************************************************************************
    Distribution:

    PIC16F Family Peripheral Support Library
    ----------------------------------------
    Copyright � 2007-2008 Ultrastar

 ***************************************************************************************************
    Compiled Using:  Hi-Tech PICC 9.60
    Processor:       PIC16F

 ***************************************************************************************************
    File:            csp_uart.h
    Description:

 ***************************************************************************************************
    History:         11/06/2008 [US] File created

 **************************************************************************************************/

#ifndef __CSP_UART_H
#define __CSP_UART_H

/***************************************************************************************************
    Included Files
 **************************************************************************************************/
/***************************************************************************************************
    Definitions
 **************************************************************************************************/

#if defined(_16F916) || defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)

    /* TXSTA register bits */

    #define UART_MODE_MASTER        (1 << 7)
    #define UART_MODE_SLAVE         (0 << 7)

    #define UART_TX_8_BIT           (0 << 6)
    #define UART_TX_9_BIT           (1 << 6)

    #define UART_TX_EN              (1 << 5)
    #define UART_TX_DIS             (0 << 5)

    #define UART_SYNC_EN            (1 << 4)
    #define UART_SYNC_DIS           (0 << 4)

    #if defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)
    #define UART_CMD_BREAK          (1 << 3)
    #endif

    #define UART_SPEED_HIGH         (1 << 2)
    #define UART_SPEED_LOW          (0 << 2)

    #define UART_SR_EMPTY           (1 << 1)
    #define UART_TX_9_TH            (1 << 0)

    /* RCSTA register bits */

    #define UART_EN                 (1 << 7)
    #define UART_DIS                (0 << 7)

    #define UART_RX_8_BIT           (0 << 6)
    #define UART_RX_9_BIT           (1 << 6)

    #define UART_SINGL_RX_EN        (1 << 5)
    #define UART_SINGL_RX_DIS       (0 << 5)

    #define UART_RX_EN              (1 << 4)
    #define UART_RX_DIS             (0 << 4)

    #define UART_ADDR_DET_EN        (1 << 3)
    #define UART_ADDR_DET_DIS       (0 << 3)

    #define UART_STAT_FERR          (1 << 2)
    #define UART_STAT_OERR          (1 << 1)

    #define UART_RX_9_TH            (1 << 0)

#endif

#if defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)

    /* BAUDCTL register bits */

    #define UART_ABAUD_TIMER_OVF    (1 << 7)
    #define UART_RX_IDLE            (1 << 6)

    #define UART_TX_INV_EN          (1 << 4)
    #define UART_TX_INV_DIS         (0 << 4)

    #define UART_BRG_16             (1 << 3)
    #define UART_BRG_8              (0 << 3)

    #define UART_CMD_WAKE_UP        (1 << 1)
    #define UART_CMD_ABAUD          (1 << 0)


#endif

/***************************************************************************************************
    Public Types
 **************************************************************************************************/
/***************************************************************************************************
    Global Variables
 **************************************************************************************************/
/***************************************************************************************************
    Public Function Prototypes
 **************************************************************************************************/

#if defined(_16F916) || defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)

    #define csp_uart_conf0_set(a)   TXSTA = (a)
    #define csp_uart_conf0_get()    (TXSTA)

    #define csp_uart_conf1_set(a)   RCSTA = (a)
    #define csp_uart_conf1_get()    (RCSTA)

    #define csp_uart_err_get()      (RCSTA)


    #if defined(_16F689) || defined(_16F690) || defined(_16F883) || defined(_16F886)
    #define csp_uart_conf2_set(a)   BAUDCTL = (a)
    #define csp_uart_conf2_get()    (BAUDCTL)
    #endif


    #define csp_uart_put(a)         TXREG = (a)
    #define csp_uart_get()          (RCREG)

    #if defined(_16F916)
    #define csp_uart_baud_set(a)    SPBRG = (a)
    #else
    #define csp_uart_baud_set(a)    SPBRG = (a); SPBRGH = ((a) >> 8)
    #endif

    #define csp_uart_en()           RCSTA |=  UART_EN
    #define csp_uart_dis()          RCSTA &= ~UART_EN

    #define csp_uart_tx_en()        TXSTA |=   UART_TX_EN
    #define csp_uart_tx_dis()       TXSTA &=  ~UART_TX_EN

    #define csp_uart_rx_en()        RCSTA |=   UART_RX_EN
    #define csp_uart_rx_dis()       RCSTA &=  ~UART_RX_EN

#endif

/***************************************************************************************************
    Included Files
 **************************************************************************************************/

#endif /* __CSP_UART_H */
/***************************************************************************************************
    end of file: csp_uart.h
 **************************************************************************************************/
