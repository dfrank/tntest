/***************************************************************************************************
    Project:         PIC16F Family CPU Support Library
    Author:          [US] Ultrastar (�) 2008, http://www.ultrastar.ru

 ***************************************************************************************************
    Distribution:

    PIC16F Family Peripheral Support Library
    ----------------------------------------
    Copyright � 2007-2008 Ultrastar

 ***************************************************************************************************
    Compiled Using:  Hi-Tech PICC 9.60
    Processor:       PIC16F

 ***************************************************************************************************
    File:            P16F818_inf.h
    Description:

 ***************************************************************************************************
    History:         10/06/2008 [US] File created

 **************************************************************************************************/

#ifndef __P16F818_INF_H
#define __P16F818_INF_H

/***************************************************************************************************
    Included Files
 **************************************************************************************************/
/***************************************************************************************************
    Definitions
 **************************************************************************************************/

#define __CSP_MIDRANGE

static  volatile       U08  TMR0        __CSP_SFR(0x01);    /* */
static  volatile       U08  PCL         __CSP_SFR(0x02);    /* */
static  volatile       U08  STATUS      __CSP_SFR(0x03);    /* */
static                 U08  FSR         __CSP_SFR(0x04);    /* */
static  volatile       U08  PORTA       __CSP_SFR(0x05);    /* */
static  volatile       U08  PORTB       __CSP_SFR(0x06);    /* */
static  volatile       U08  PCLATH      __CSP_SFR(0x0A);    /* */
static  volatile       U08  INTCON      __CSP_SFR(0x0B);    /* */
static  volatile       U08  PIR1        __CSP_SFR(0x0C);    /* */
static  volatile       U08  PIR2        __CSP_SFR(0x0D);    /* */
static  volatile       U08  TMR1L       __CSP_SFR(0x0E);    /* */
static  volatile       U08  TMR1H       __CSP_SFR(0x0F);    /* */
static  volatile       U08  T1CON       __CSP_SFR(0x10);    /* */
static  volatile       U08  TMR2        __CSP_SFR(0x11);    /* */
static  volatile       U08  T2CON       __CSP_SFR(0x12);    /* */
static  volatile       U08  SSPBUF      __CSP_SFR(0x13);    /* */
static  volatile       U08  SSPCON      __CSP_SFR(0x14);    /* */
static  volatile       U08  CCPR1L      __CSP_SFR(0x15);    /* */
static  volatile       U08  CCPR1H      __CSP_SFR(0x16);    /* */
static  volatile       U08  CCP1CON     __CSP_SFR(0x17);    /* */
static  volatile       U08  ADRESH      __CSP_SFR(0x1E);    /* */
static  volatile       U08  ADCON0      __CSP_SFR(0x1F);    /* */
static           bank1 U08  OPTION      __CSP_SFR(0x81);    /* */
static  volatile bank1 U08  TRISA       __CSP_SFR(0x85);    /* */
static  volatile bank1 U08  TRISB       __CSP_SFR(0x86);    /* */
static           bank1 U08  PIE1        __CSP_SFR(0x8C);    /* */
static           bank1 U08  PIE2        __CSP_SFR(0x8D);    /* */
static  volatile bank1 U08  PCON        __CSP_SFR(0x8E);    /* */
static  volatile bank1 U08  OSCCON      __CSP_SFR(0x8F);    /* */
static  volatile bank1 U08  OSCTUNE     __CSP_SFR(0x90);    /* */
static  volatile bank1 U08  PR2         __CSP_SFR(0x92);    /* */
static  volatile bank1 U08  SSPADD      __CSP_SFR(0x93);    /* */
static  volatile bank1 U08  SSPSTAT     __CSP_SFR(0x94);    /* */
static  volatile bank1 U08  ADRESL      __CSP_SFR(0x9E);    /* */
static  volatile bank1 U08  ADCON1      __CSP_SFR(0x9F);    /* */
static  volatile bank2 U08  EEDATL      __CSP_SFR(0x10C);   /* */
static  volatile bank2 U08  EEADRL      __CSP_SFR(0x10D);   /* */
static  volatile bank2 U08  EEDATH      __CSP_SFR(0x10E);   /* */
static  volatile bank2 U08  EEADRH      __CSP_SFR(0x10F);   /* */
static  volatile bank3 U08  EECON1      __CSP_SFR(0x18C);   /* */
static  volatile bank3 U08  EECON2      __CSP_SFR(0x18D);   /* */

/***************************************************************************************************
    Public Types
 **************************************************************************************************/

/***************************************************************************************************
    Global Variables
 **************************************************************************************************/

/***************************************************************************************************
    Public Function Prototypes
 **************************************************************************************************/

/***************************************************************************************************
    Included Files
 **************************************************************************************************/

#include "../modules/csp_core.h"
#include "../modules/csp_gpio.h"
#include "../modules/csp_int.h"
#include "../modules/csp_osc.h"
#include "../modules/csp_tmr.h"
#include "../modules/csp_ssp.h"
#include "../modules/csp_ccp.h"
#include "../modules/csp_adc.h"
#include "../modules/csp_eepr.h"

#endif /* __P16F818_INF_H */
/***************************************************************************************************
    end of file: P16F818_inf.h
 **************************************************************************************************/

