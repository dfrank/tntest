
/***************************************************************************************************
    Project:         PIC16F Family CPU Support Library
    Author:          [US] Ultrastar (�) 2008, http://www.ultrastar.ru

 ***************************************************************************************************
    Distribution:

    PIC16F Family Peripheral Support Library
    ----------------------------------------
    Copyright � 2007-2008 Ultrastar

 ***************************************************************************************************
    Compiled Using:  Hi-Tech PICC 9.60
    Processor:       PIC16F

 ***************************************************************************************************
    File:            P16F886_inf.h
    Description:

 ***************************************************************************************************
    History:         10/11/2008 [US] File created

 **************************************************************************************************/
#ifndef __P16F886_INF_H
#define __P16F886_INF_H

/***************************************************************************************************
    Included Files
 **************************************************************************************************/
/***************************************************************************************************
    Definitions
 **************************************************************************************************/

#define __CSP_MIDRANGE

    /* BANK0 */

static  volatile       U08  INDF        __CSP_SFR(0x00);    /* */
static  volatile       U08  TMR0        __CSP_SFR(0x01);    /* */
static  volatile       U08  PCL         __CSP_SFR(0x02);    /* */
static  volatile       U08  STATUS      __CSP_SFR(0x03);    /* */
static                 U08  FSR         __CSP_SFR(0x04);    /* */
static  volatile       U08  PORTA       __CSP_SFR(0x05);    /* */
static  volatile       U08  PORTB       __CSP_SFR(0x06);    /* */
static  volatile       U08  PORTC       __CSP_SFR(0x07);    /* */
static  volatile       U08  PORTE       __CSP_SFR(0x09);    /* */
static  volatile       U08  PCLATH      __CSP_SFR(0x0A);    /* */
static  volatile       U08  INTCON      __CSP_SFR(0x0B);    /* */
static  volatile       U08  PIR1        __CSP_SFR(0x0C);    /* */
static  volatile       U08  PIR2        __CSP_SFR(0x0D);    /* */
static  volatile       U08  TMR1L       __CSP_SFR(0x0E);    /* */
static  volatile       U08  TMR1H       __CSP_SFR(0x0F);    /* */
static                 U08  T1CON       __CSP_SFR(0x10);    /* */
static  volatile       U08  TMR2        __CSP_SFR(0x11);    /* */
static                 U08  T2CON       __CSP_SFR(0x12);    /* */
static  volatile       U08  SSPBUF      __CSP_SFR(0x13);    /* */
static  volatile       U08  SSPCON      __CSP_SFR(0x14);    /* */
static  volatile       U08  CCPR1L      __CSP_SFR(0x15);    /* */
static  volatile       U08  CCPR1H      __CSP_SFR(0x16);    /* */
static  volatile       U08  CCP1CON     __CSP_SFR(0x17);    /* */
static  volatile       U08  RCSTA       __CSP_SFR(0x18);    /* */
static  volatile       U08  TXREG       __CSP_SFR(0x19);    /* */
static  volatile       U08  RCREG       __CSP_SFR(0x1A);    /* */
static  volatile       U08  CCPR2L      __CSP_SFR(0x1B);    /* */
static  volatile       U08  CCPR2H      __CSP_SFR(0x1C);    /* */
static  volatile       U08  CCP2CON     __CSP_SFR(0x1D);    /* */
static  volatile       U08  ADRESH      __CSP_SFR(0x1E);    /* */
static  volatile       U08  ADCON0      __CSP_SFR(0x1F);    /* */

    /* BANK1 */

static           bank1 U08  OPTION      __CSP_SFR(0x81);    /* */
static  volatile bank1 U08  TRISA       __CSP_SFR(0x85);    /* */
static  volatile bank1 U08  TRISB       __CSP_SFR(0x86);    /* */
static  volatile bank1 U08  TRISC       __CSP_SFR(0x87);    /* */
static  volatile bank1 U08  TRISE       __CSP_SFR(0x89);    /* */
static           bank1 U08  PIE1        __CSP_SFR(0x8C);    /* */
static           bank1 U08  PIE2        __CSP_SFR(0x8D);    /* */
static  volatile bank1 U08  PCON        __CSP_SFR(0x8E);    /* */
static  volatile bank1 U08  OSCCON      __CSP_SFR(0x8F);    /* */
static           bank1 U08  OSCTUNE     __CSP_SFR(0x90);    /* */
static  volatile bank1 U08  SSPCON2     __CSP_SFR(0x91);    /* */
static           bank1 U08  PR2         __CSP_SFR(0x92);    /* */
static           bank1 U08  SSPADD      __CSP_SFR(0x93);    /* */
static  volatile bank1 U08  SSPSTAT     __CSP_SFR(0x94);    /* */
static           bank1 U08  WPUB        __CSP_SFR(0x95);    /* */
static           bank1 U08  IOCB        __CSP_SFR(0x96);    /* */
static           bank1 U08  VRCON       __CSP_SFR(0x97);
static  volatile bank1 U08  TXSTA       __CSP_SFR(0x98);    /* */
static           bank1 U08  SPBRG       __CSP_SFR(0x99);    /* */
static           bank1 U08  SPBRGH      __CSP_SFR(0x9A);    /* */
static  volatile bank1 U08  PWM1CON     __CSP_SFR(0x9B);    /* */
static  volatile bank1 U08  ECCPAS      __CSP_SFR(0x9C);    /* */
static           bank1 U08  PSTRCON     __CSP_SFR(0x9D);    /* */
static  volatile bank1 U08  ADRESL      __CSP_SFR(0x9E);    /* */
static           bank1 U08  ADCON1      __CSP_SFR(0x9F);    /* */

    /* BANK2 */

static  volatile bank2 U08  WDTCON      __CSP_SFR(0x105);    /* */ 
static  volatile bank2 U08  CM1CON0     __CSP_SFR(0x107);    /* */ 
static  volatile bank2 U08  CM2CON0     __CSP_SFR(0x108);    /* */ 
static  volatile bank2 U08  CM2CON1     __CSP_SFR(0x109);    /* */ 
static  volatile bank2 U08  EEDATL      __CSP_SFR(0x10C);    /* */ 
static           bank2 U08  EEADRL      __CSP_SFR(0x10D);    /* */ 
static  volatile bank2 U08  EEDATH      __CSP_SFR(0x10E);    /* */ 
static           bank2 U08  EEADRH      __CSP_SFR(0x10F);    /* */ 

    /* BANK3 */

static  volatile bank3 U08  SRCON       __CSP_SFR(0x185);    /* */ 
static  volatile bank3 U08  BAUDCTL     __CSP_SFR(0x187);    /* */ 
static           bank3 U08  ANSEL       __CSP_SFR(0x188);    /* */ 
static           bank3 U08  ANSELH      __CSP_SFR(0x189);    /* */ 
static  volatile bank3 U08  EECON1      __CSP_SFR(0x18C);    /* */ 
static  volatile bank3 U08  EECON2      __CSP_SFR(0x18D);    /* */ 

/***************************************************************************************************
    Public Types
 **************************************************************************************************/
/***************************************************************************************************
    Global Variables
 **************************************************************************************************/
/***************************************************************************************************
    Public Function Prototypes
 **************************************************************************************************/
/***************************************************************************************************
    Included Files
 **************************************************************************************************/

#include "../modules/csp_core.h"  /* */
#include "../modules/csp_gpio.h"  /* */
#include "../modules/csp_int.h"   /* */
#include "../modules/csp_osc.h"   /* */
#include "../modules/csp_tmr.h"   /* */
#include "../modules/csp_ssp.h"   /* */
#include "../modules/csp_ccp.h"   /* */
#include "../modules/csp_adc.h"   /* */
#include "../modules/csp_comp.h"  /* */
#include "../modules/csp_uart.h"
#include "../modules/csp_eepr.h"  /* */

#endif /* __P16F886_INF_H */
/***************************************************************************************************
    end of file: P16F886_inf.h
 **************************************************************************************************/


