/***************************************************************************************************
    Project:         PIC16F Family CPU Support Library
    Author:          [US] Ultrastar (�) 2008, http://www.ultrastar.ru

 ***************************************************************************************************
    Distribution:

    PIC16F Family Peripheral Support Library
    ----------------------------------------
    Copyright � 2007-2008 Ultrastar

 ***************************************************************************************************
    Compiled Using:  Hi-Tech PICC 9.60
    Processor:       PIC16F

 **************************************************************************************************
    File:            csp.h
    Description:

 ***************************************************************************************************
    History:         10/06/2008 [US] File created

 **************************************************************************************************/

#ifndef __CSP_H
#define __CSP_H

/***************************************************************************************************
    Included Files
 **************************************************************************************************/

/***************************************************************************************************
    Definitions
 **************************************************************************************************/

#ifndef ENABLE
#define ENABLE  1
#endif

#ifndef DISABLE
#define DISABLE 0
#endif

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#ifndef NULL
#define NULL    0
#endif

#define     ___mkstr1(x)        #x
#define     ___mkstr(x)         ___mkstr1(x)

/***************************************************************************************************
    Public Types
 **************************************************************************************************/

typedef unsigned char       U08;
typedef unsigned int        U16;
typedef unsigned long       U32;

typedef signed   char       S08;
typedef signed   int        S16;
typedef signed   long       S32;

typedef          float      FLOAT;
typedef          double     DOUBLE;

typedef          U08        BOOL;

#ifndef __cplusplus
    #ifndef true
    #define true            (1==1)
    #endif
    #ifndef false
    #define false           (1!=1)
    #endif
    #ifndef _BOOL_CPP_DEFINED
    typedef unsigned char   bool;
    #define _BOOL_CPP_DEFINED
    #endif
#endif /* !__cplusplus */

/***************************************************************************************************
    Global Variables
 **************************************************************************************************/

/***************************************************************************************************
    Public Function Prototypes
 **************************************************************************************************/

/***************************************************************************************************
    Included Files
 **************************************************************************************************/

#define ___mkstr1(x)                #x
#define ___mkstr(x)                 ___mkstr1(x)

#define __CSP_SFR(addr)             @ addr
#define __CSP_BASELINE_CON          control

#define __EE_DAT(addr, val) asm("\tpsect ee_dt, delta=2 ,abs, ovrld"); \
                            asm("\torg     2100h + " ___mkstr(addr)); \
                            asm("\tdb\t" ___mkstr(val))

#define __B0(x)             (*((U08*)(&x) + 0))
#define __B1(x)             (*((U08*)(&x) + 1))
#define __B2(x)             (*((U08*)(&x) + 2))
#define __B3(x)             (*((U08*)(&x) + 3))


#if   defined(_16F505)
    #include "cpu_inf/P16F505_inf.h"
#elif defined(_16F818)
    #include "cpu_inf/P16F818_inf.h"
#elif defined(_16F819)
    #include "cpu_inf/P16F819_inf.h"
#elif defined(_16F916)
    #include "cpu_inf/P16F916_inf.h"
#elif defined(_16F689)
    #include "cpu_inf/P16F689_inf.h"
#elif defined(_16F690)
    #include "cpu_inf/P16F690_inf.h"
#elif defined(_16F883)
    #include "cpu_inf/P16F883_inf.h"
#elif defined(_16F886)
    #include "cpu_inf/P16F886_inf.h"
#else
    #error "PIC16 Peripheral library dosn't support selected CPU"
#endif


#endif /* __CSP_H */
/***************************************************************************************************
    end of file: csp.h
 **************************************************************************************************/

