/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_pps.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_PPS_H
#define __CSP_PPS_H

#if  ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  || \
      (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
      (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
     )

    /* PPS programming model */
    /* --------------------- */

    typedef struct __PPS_TYPE
    {
        volatile UWORD RPINR0;
        __CSP_SKIP_BYTE(62);
        volatile UWORD RP0R0;

    } PPS_TYPE;

    /* PPS base address */
    /* ---------------- */

    #define PPS_BASE_ADDR       0x0680

    /* Declare PPS model */
    /* ----------------- */

    extern  PPS_TYPE PPS __CSP_SFR(PPS_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    #define __PPS_LOCK        (1 << 6)    /* OSCCON<6> */

    #define __PPS_INP_MASK    (0x3F)
    #define __PPS_OUT_MASK    (0x3F)

    /* Definitions for PPS API */
    /* ----------------------- */

    #if  (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)

        /* csp_pps_inp_set() and csp_pps_inp_get() parameters type */

        typedef enum __PPS_INP
        {
            PPS_INP_INT1       =  1,
            PPS_INP_INT2       =  2,
            PPS_INP_TMR2_CLK   =  6,
            PPS_INP_TMR3_CLK   =  7,
            PPS_INP_TMR4_CLK   =  8,
            PPS_INP_TMR5_CLK   =  9,
            PPS_INP_IC1        = 14,
            PPS_INP_IC2        = 15,
            PPS_INP_IC3        = 16,
            PPS_INP_IC4        = 17,
            PPS_INP_IC5        = 18,
            PPS_INP_OC_FAULT_A = 22,
            PPS_INP_OC_FAULT_B = 23,
            PPS_INP_UART1_RX   = 36,
            PPS_INP_UART1_CTS  = 37,
            PPS_INP_UART2_RX   = 38,
            PPS_INP_UART2_CTS  = 39,
            PPS_INP_SPI1_SDI   = 40,
            PPS_INP_SPI1_SCK   = 41,
            PPS_INP_SPI1_SS    = 42,
            PPS_INP_SPI2_SDI   = 44,
            PPS_INP_SPI2_SCK   = 45,
            PPS_INP_SPI2_SS    = 46
        } PPS_INP;

        /* csp_pps_out_set() and csp_pps_out_get() parameters type */

        typedef enum __PPS_OUT
        {
            PPS_OUT_NULL      =  0,
            PPS_OUT_COMP_1    =  1,
            PPS_OUT_COMP_2    =  2,
            PPS_OUT_UART1_TX  =  3,
            PPS_OUT_UART1_RTS =  4,
            PPS_OUT_UART2_TX  =  5,
            PPS_OUT_UART2_RTS =  6,
            PPS_OUT_SPI1_SDO  =  7,
            PPS_OUT_SPI1_SCK  =  8,
            PPS_OUT_SPI1_SS   =  9,
            PPS_OUT_SPI2_SDO  = 10,
            PPS_OUT_SPI2_SCK  = 11,
            PPS_OUT_SPI2_SS   = 12,
            PPS_OUT_OC1       = 18,
            PPS_OUT_OC2       = 19,
            PPS_OUT_OC3       = 20,
            PPS_OUT_OC4       = 21,
            PPS_OUT_OC5       = 22
        } PPS_OUT;

        #if   (__CSP_PIN_COUNT == __CSP_PIN_COUNT_28)

            /* Most PPS API functions parameters type */

            typedef enum __PPS_PIN
            {
                PPS_PIN_0,
                PPS_PIN_1,
                PPS_PIN_2,
                PPS_PIN_3,
                PPS_PIN_4,
                PPS_PIN_5,
                PPS_PIN_6,
                PPS_PIN_7,
                PPS_PIN_8,
                PPS_PIN_9,
                PPS_PIN_10,
                PPS_PIN_11,
                PPS_PIN_12,
                PPS_PIN_13,
                PPS_PIN_14,
                PPS_PIN_15,
            } PPS_PIN;


        #elif (__CSP_PIN_COUNT == __CSP_PIN_COUNT_44)

            /* Most PPS API functions parameters type */

            typedef enum __PPS_PIN
            {
                PPS_PIN_0,
                PPS_PIN_1,
                PPS_PIN_2,
                PPS_PIN_3,
                PPS_PIN_4,
                PPS_PIN_5,
                PPS_PIN_6,
                PPS_PIN_7,
                PPS_PIN_8,
                PPS_PIN_9,
                PPS_PIN_10,
                PPS_PIN_11,
                PPS_PIN_12,
                PPS_PIN_13,
                PPS_PIN_14,
                PPS_PIN_15,
                PPS_PIN_16,
                PPS_PIN_17,
                PPS_PIN_18,
                PPS_PIN_19,
                PPS_PIN_20,
                PPS_PIN_21,
                PPS_PIN_22,
                PPS_PIN_23,
                PPS_PIN_24,
                PPS_PIN_25
            } PPS_PIN;

        #endif

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
          )

        /* csp_pps_inp_set() and csp_pps_inp_get() parameters type */

        typedef enum __PPS_INP
        {
            PPS_INP_INT1       =  1,
            PPS_INP_INT2       =  2,
            PPS_INP_INT3       =  3,
            PPS_INP_INT4       =  4,
            PPS_INP_TMR1_CLK   =  5,
            PPS_INP_TMR2_CLK   =  6,
            PPS_INP_TMR3_CLK   =  7,
            PPS_INP_TMR4_CLK   =  8,
            PPS_INP_TMR5_CLK   =  9,

            PPS_INP_IC1        = 14,
            PPS_INP_IC2        = 15,
            PPS_INP_IC3        = 16,
            PPS_INP_IC4        = 17,
            PPS_INP_IC5        = 18,
            PPS_INP_IC6        = 19,
            PPS_INP_IC7        = 20,
            PPS_INP_IC8        = 21,
            PPS_INP_OC_FAULT_A = 22,
            PPS_INP_OC_FAULT_B = 23,
            PPS_INP_IC9        = 31,

            PPS_INP_UART3_RX   = 35,
            PPS_INP_UART1_RX   = 36,
            PPS_INP_UART1_CTS  = 37,
            PPS_INP_UART2_RX   = 38,
            PPS_INP_UART2_CTS  = 39,

            PPS_INP_SPI1_SDI   = 40,
            PPS_INP_SPI1_SCK   = 41,
            PPS_INP_SPI1_SS    = 42,
            PPS_INP_UART3_CTS  = 43,

            PPS_INP_SPI2_SDI   = 44,
            PPS_INP_SPI2_SCK   = 45,
            PPS_INP_SPI2_SS    = 46,

            PPS_INP_UART4_RX   = 54,
            PPS_INP_UART4_CTS  = 55,
            PPS_INP_SPI3_SDI   = 56,
            PPS_INP_SPI3_SCK   = 57,
            PPS_INP_SPI3_SS    = 58,

        } PPS_INP;

        /* csp_pps_out_set() and csp_pps_out_get() parameters type */

        typedef enum __PPS_OUT
        {
            PPS_OUT_NULL      =  0,
            PPS_OUT_COMP_1    =  1,
            PPS_OUT_COMP_2    =  2,
            PPS_OUT_UART1_TX  =  3,
            PPS_OUT_UART1_RTS =  4,
            PPS_OUT_UART2_TX  =  5,
            PPS_OUT_UART2_RTS =  6,
            PPS_OUT_SPI1_SDO  =  7,
            PPS_OUT_SPI1_SCK  =  8,
            PPS_OUT_SPI1_SS   =  9,
            PPS_OUT_SPI2_SDO  = 10,
            PPS_OUT_SPI2_SCK  = 11,
            PPS_OUT_SPI2_SS   = 12,
            PPS_OUT_OC1       = 18,
            PPS_OUT_OC2       = 19,
            PPS_OUT_OC3       = 20,
            PPS_OUT_OC4       = 21,
            PPS_OUT_OC5       = 22,
            PPS_OUT_OC6       = 23,
            PPS_OUT_OC7       = 24,
            PPS_OUT_OC8       = 25,
            PPS_OUT_UART3_TX  = 28,
            PPS_OUT_UART3_RTS = 29,
            PPS_OUT_UART4_TX  = 30,
            PPS_OUT_UART4_RTS = 31,
            PPS_OUT_SPI3_SDO  = 32,
            PPS_OUT_SPI3_SCK  = 33,
            PPS_OUT_SPI3_SS   = 34,
            PPS_OUT_OC9       = 35

        } PPS_OUT;

        #if   (__CSP_PIN_COUNT == __CSP_PIN_COUNT_64)

            /* Most PPS API functions parameters type */

            typedef enum __PPS_PIN
            {
                PPS_PIN_0,
                PPS_PIN_1,
                PPS_PIN_2,
                PPS_PIN_3,
                PPS_PIN_4,

                PPS_PIN_6 = 6,
                PPS_PIN_7,
                PPS_PIN_8,
                PPS_PIN_9,
                PPS_PIN_10,
                PPS_PIN_11,
                PPS_PIN_12,
                PPS_PIN_13,
                PPS_PIN_14,

                PPS_PIN_16 = 16,
                PPS_PIN_17,
                PPS_PIN_18,
                PPS_PIN_19,
                PPS_PIN_20,
                PPS_PIN_21,
                PPS_PIN_22,
                PPS_PIN_23,
                PPS_PIN_24,
                PPS_PIN_25,
                PPS_PIN_26,
                PPS_PIN_27,
                PPS_PIN_28,
                PPS_PIN_29,
                PPS_PIN_30,

                PPS_PINI_37 = 37,
                PPS_PINI_45 = 45

            } PPS_PIN;

        #elif (__CSP_PIN_COUNT == __CSP_PIN_COUNT_80)

            typedef enum __PPS_PIN
            {
                PPS_PIN_0,
                PPS_PIN_1,
                PPS_PIN_2,
                PPS_PIN_3,
                PPS_PIN_4,
                PPS_PIN_5,
                PPS_PIN_6,
                PPS_PIN_7,
                PPS_PIN_8,
                PPS_PIN_9,
                PPS_PIN_10,
                PPS_PIN_11,
                PPS_PIN_12,
                PPS_PIN_13,
                PPS_PIN_14,
                PPS_PIN_15,
                PPS_PIN_16,
                PPS_PIN_17,
                PPS_PIN_18,
                PPS_PIN_19,
                PPS_PIN_20,
                PPS_PIN_21,
                PPS_PIN_22,
                PPS_PIN_23,
                PPS_PIN_24,
                PPS_PIN_25,
                PPS_PIN_26,
                PPS_PIN_27,
                PPS_PIN_28,
                PPS_PIN_29,
                PPS_PIN_30,
                PPS_PINI_33 = 33,
                PPS_PINI_34,
                PPS_PINI_35,
                PPS_PINI_36,
                PPS_PINI_37,
                PPS_PINI_38,
                PPS_PINI_40 = 40,
                PPS_PINI_42 = 42,
                PPS_PINI_43,
                PPS_PINI_44,
                PPS_PINI_45
            } PPS_PIN;

        #elif (__CSP_PIN_COUNT == __CSP_PIN_COUNT_100)

            typedef enum __PPS_PIN
            {
                PPS_PIN_0,
                PPS_PIN_1,
                PPS_PIN_2,
                PPS_PIN_3,
                PPS_PIN_4,
                PPS_PIN_5,
                PPS_PIN_6,
                PPS_PIN_7,
                PPS_PIN_8,
                PPS_PIN_9,
                PPS_PIN_10,
                PPS_PIN_11,
                PPS_PIN_12,
                PPS_PIN_13,
                PPS_PIN_14,
                PPS_PIN_15,
                PPS_PIN_16,
                PPS_PIN_17,
                PPS_PIN_18,
                PPS_PIN_19,
                PPS_PIN_20,
                PPS_PIN_21,
                PPS_PIN_22,
                PPS_PIN_23,
                PPS_PIN_24,
                PPS_PIN_25,
                PPS_PIN_26,
                PPS_PIN_27,
                PPS_PIN_28,
                PPS_PIN_29,
                PPS_PIN_30,
                PPS_PIN_31,
                PPS_PINI_32,
                PPS_PINI_33,
                PPS_PINI_34,
                PPS_PINI_35,
                PPS_PINI_36,
                PPS_PINI_37,
                PPS_PINI_38,
                PPS_PINI_39,
                PPS_PINI_40,
                PPS_PINI_41,
                PPS_PINI_42,
                PPS_PINI_43,
                PPS_PINI_44,
                PPS_PINI_45
            } PPS_PIN;

        #endif

    #endif

#endif


/* External functions prototypes */
/* ----------------------------- */

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)      \
    )

    #if !defined(__CSP_BUILD_LIB)

        /* Module API definition */

        #if defined(__CSP_DEBUG_PPS)

                void    csp_pps_inp_set(PPS_INP val, PPS_PIN pin);
                PPS_PIN csp_pps_inp_get(PPS_INP val);

                void    csp_pps_out_set(PPS_OUT val, PPS_PIN pin);
                PPS_OUT csp_pps_out_get(PPS_PIN pin);

                void    csp_pps_lock(void);
                void    csp_pps_unlock(void);

        #else

            #include "csp_pps.c"

        #endif

    #endif

#endif


#endif /* #ifndef __CSP_PPS_H */
/* ********************************************************************************************** */
/* end of file: csp_pps.h */
