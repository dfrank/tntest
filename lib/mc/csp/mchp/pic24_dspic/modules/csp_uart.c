/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_uart_bget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_uart_baud_get (UART_TYPE *uart)
{
    return (uart->UBRG);
}

/* ********************************************************************************************** */
/* end of file: csp_uart_bget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_uart_bset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_uart_baud_set (UART_TYPE *uart, UWORD val)
{
    uart->UBRG = val;
}

/* ********************************************************************************************** */
/* end of file: csp_uart_bset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_uart_cget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC U32  csp_uart_conf_get (UART_TYPE *uart)
{
    AL_EXT_32 ret;

    ret.word_0 = uart->USTA;
    ret.word_1 = uart->UMODE;

    return ret.full;
}

__CSP_FUNC void  csp_uart_baud_set_auto (UART_TYPE *uart, unsigned long periph_bus_clk_freq, unsigned long baudrate)
{
   if (csp_uart_conf_get(uart) & UART_HIGH_BAUD_EN){
      uart->UBRG = (CSP_INT_DIV((periph_bus_clk_freq >> 2), baudrate)) - 1;
   } else {
      uart->UBRG = (CSP_INT_DIV((periph_bus_clk_freq >> 4), baudrate)) - 1;
   }
}



/* ********************************************************************************************** */
/* end of file: csp_uart_cget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_uart_comm.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_uart_comm (UART_TYPE *uart, UART_COMM comm)
{
    switch (comm)
    {
        case UART_COMM_WAKEUP:
            __CSP_BIT_VAL_SET(uart->UMODE, __UART_WAKEUP_BIT);
            break;
        case UART_COMM_AUTOBAUD:
            __CSP_BIT_VAL_SET(uart->UMODE, __UART_AUTOBAUD_BIT);
            break;
        case UART_COMM_BREAK:
            __CSP_BIT_VAL_SET(uart->USTA, __UART_BREAK_BIT);
            uart->UTXREG = 0;
            break;
    }
}

/* ********************************************************************************************** */
/* end of file: csp_uart_comm.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_uart_cset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_uart_conf_set (UART_TYPE *uart, U32 val)
{
    uart->UMODE = ((AL_EXT_32)val).word_1;
    uart->USTA  = ((AL_EXT_32)val).word_0;
}

/* ********************************************************************************************** */
/* end of file: csp_uart_cset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_uart_get.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC U16  csp_uart_get (UART_TYPE *uart)
{
    return (uart->URXREG);
}

/* ********************************************************************************************** */
/* end of file: csp_uart_get.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_uart_put.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_uart_put (UART_TYPE *uart, UWORD val)
{
    uart->UTXREG = val;
}

/* ********************************************************************************************** */
/* end of file: csp_uart_put.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_uart_sclr.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_uart_stat_clr (UART_TYPE *uart, UART_STAT mask)
{
    __CSP_ACC_MASK_VAL(BFA_CLR, uart->USTA, __UART_STAT_MASK, mask);
}

/* ********************************************************************************************** */
/* end of file: csp_uart_sclr.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_uart_sget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UART_STAT  csp_uart_stat_get (UART_TYPE *uart)
{
    return (UART_STAT)__CSP_ACC_MASK_VAL(BFA_RD, uart->USTA, __UART_STAT_MASK);
}

/* ********************************************************************************************** */
/* end of file: csp_uart_sget.c */
