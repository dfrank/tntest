/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_pmd.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_PMD_H
#define __CSP_PMD_H


#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    /* PMD programming model */
    /* --------------------- */

    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
        )

        typedef struct __PMD_TYPE
        {
            volatile UWORD PMD1;
            volatile UWORD PMD2;
            volatile UWORD PMD3;
        } PMD_TYPE;

    #elif (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

        typedef struct __PMD_TYPE
        {
            volatile UWORD PMD1;
            volatile UWORD PMD2;
            volatile UWORD PMD3;
            volatile UWORD PMD4;
        } PMD_TYPE;

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
          )

        typedef struct __PMD_TYPE
        {
            volatile UWORD PMD1;
            volatile UWORD PMD2;
            volatile UWORD PMD3;
            volatile UWORD PMD4;
            volatile UWORD PMD5;
            volatile UWORD PMD6;
        } PMD_TYPE;
    #endif

    /* PMD base addresses */
    /* ------------------ */

    #define PMD_BASE_ADDR       0x0770

    /* Declare PMD model */
    /* ----------------- */

    extern  PMD_TYPE PMD __CSP_SFR(PMD_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    /* Definitions for PMD API */
    /* ----------------------- */

    /* csp_pmd_en(), csp_pmd_dis() and csp_pmd_get() parameters type */

    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
        )

        typedef enum __PMD_PER
        {
            PMD_ADC   = (0),
            PMD_SPI1  = (3),
            PMD_SPI2  = (4),
            PMD_UART1 = (5),
            PMD_UART2 = (6),
            PMD_I2C1  = (7),
            PMD_TMR1  = (11),
            PMD_TMR2  = (12),
            PMD_TMR3  = (13),
            PMD_TMR4  = (14),
            PMD_TMR5  = (15),
            PMD_OC1   = (16),
            PMD_OC2   = (17),
            PMD_OC3   = (18),
            PMD_OC4   = (19),
            PMD_OC5   = (20),
            PMD_IC1   = (24),
            PMD_IC2   = (25),
            PMD_IC3   = (26),
            PMD_IC4   = (27),
            PMD_IC5   = (28),
            PMD_I2C2  = (33),
            PMD_CRC   = (39),
            PMD_PMP   = (40),
            PMD_RTC   = (41),
            PMD_COMP  = (42)
        } PMD_PER;

    #elif (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

        typedef enum __PMD_PER
        {
            PMD_ADC   = (0),
            PMD_SPI1  = (3),
            PMD_UART1 = (5),
            PMD_UART2 = (6),
            PMD_I2C1  = (7),
            PMD_TMR1  = (11),
            PMD_TMR2  = (12),
            PMD_TMR3  = (13),
            PMD_OC1   = (16),
            PMD_IC1   = (24),           
            //PMD_I2C2  = (33),
            PMD_CRC   = (39),
            PMD_RTC   = (41),
            PMD_COMP  = (42),
            PMD_LVD   = (49),
            PMD_CTMU  = (50),
            PMD_REFO  = (51),
            PMD_EE    = (52),
        } PMD_PER;

    #elif (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110)

        typedef enum __PMD_PER
        {
            PMD_ADC   = (0),
            PMD_SPI1  = (3),
            PMD_SPI2  = (4),
            PMD_UART1 = (5),
            PMD_UART2 = (6),
            PMD_I2C1  = (7),
            PMD_TMR1  = (11),
            PMD_TMR2  = (12),
            PMD_TMR3  = (13),
            PMD_TMR4  = (14),
            PMD_TMR5  = (15),
            PMD_OC1   = (16),
            PMD_OC2   = (17),
            PMD_OC3   = (18),
            PMD_OC4   = (19),
            PMD_OC5   = (20),
            PMD_OC6   = (21),
            PMD_OC7   = (22),
            PMD_OC8   = (23),
            PMD_IC1   = (24),
            PMD_IC2   = (25),
            PMD_IC3   = (26),
            PMD_IC4   = (27),
            PMD_IC5   = (28),
            PMD_IC6   = (29),
            PMD_IC7   = (30),
            PMD_IC8   = (31),
            PMD_I2C2  = (33),
            PMD_I2C3  = (34),
            PMD_UART3 = (35),
            PMD_CRC   = (39),
            PMD_PMP   = (40),
            PMD_RTC   = (41),
            PMD_COMP  = (42),
            PMD_LVD   = (49),
            PMD_CTMU  = (50),
            PMD_REFO  = (51),
            PMD_UART4 = (53),
            PMD_UPWM  = (54),
            PMD_OC9   = (64),
            PMD_IC9   = (72),
            PMD_SPI3  = (80)
        } PMD_PER;

    #elif (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)

        typedef enum __PMD_PER
        {
            PMD_ADC   = (0),
            PMD_SPI1  = (3),
            PMD_SPI2  = (4),
            PMD_UART1 = (5),
            PMD_UART2 = (6),
            PMD_I2C1  = (7),
            PMD_TMR1  = (11),
            PMD_TMR2  = (12),
            PMD_TMR3  = (13),
            PMD_TMR4  = (14),
            PMD_TMR5  = (15),
            PMD_OC1   = (16),
            PMD_OC2   = (17),
            PMD_OC3   = (18),
            PMD_OC4   = (19),
            PMD_OC5   = (20),
            PMD_OC6   = (21),
            PMD_OC7   = (22),
            PMD_OC8   = (23),
            PMD_IC1   = (24),
            PMD_IC2   = (25),
            PMD_IC3   = (26),
            PMD_IC4   = (27),
            PMD_IC5   = (28),
            PMD_IC6   = (29),
            PMD_IC7   = (30),
            PMD_IC8   = (31),
            PMD_I2C2  = (33),
            PMD_I2C3  = (34),
            PMD_UART3 = (35),
            PMD_CRC   = (39),
            PMD_PMP   = (40),
            PMD_RTC   = (41),
            PMD_COMP  = (42),
            PMD_USB   = (48),
            PMD_LVD   = (49),
            PMD_CTMU  = (50),
            PMD_REFO  = (51),
            PMD_UART4 = (53),
            PMD_UPWM  = (54),
            PMD_OC9   = (64),
            PMD_IC9   = (72),
            PMD_SPI3  = (80)
        } PMD_PER;

    #endif
#endif


/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_PMD)

        void csp_pmd_en(PMD_PER pmd);
        void csp_pmd_dis(PMD_PER pmd);
        BOOL csp_pmd_get(PMD_PER pmd);

    #else

        #include "csp_pmd.c"

    #endif

#endif

#endif /* #ifndef __CSP_PMD_H */
/* ********************************************************************************************** */
/* end of file: csp_pmd.h */
