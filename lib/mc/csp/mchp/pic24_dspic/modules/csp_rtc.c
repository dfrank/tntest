/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_rtc_acgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_rtc_alarm_conf_get (U08 *repeat)
{
    return (RTC.ALCFGRPT);
}




/* ********************************************************************************************** */
/* end of file: csp_rtc_acgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_rtc_acst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

void    _csp_rtc_wr_en(void);
#define _csp_rtc_wr_dis()         __CSP_BIT_REG_CLR(RTC.RCFGCAL, __RTC_WR_EN_BIT);

__CSP_FUNC void  csp_rtc_alarm_conf_set (UWORD val)
{
    _csp_rtc_wr_en();
    RTC.ALCFGRPT = val;
    _csp_rtc_wr_dis();
}




/* ********************************************************************************************** */
/* end of file: csp_rtc_acst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_rtc_aget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_rtc_alarm_get (RTC_TIME *ctime)
{
    U16 tmp;

    __CSP_ACC_RANG_REG(BFA_WR, RTC.ALCFGRPT, __RTC_PTR_FS, __RTC_PTR_FE, 3);    /* pointer to year */
    tmp = RTC.ALRMVAL;

    tmp = RTC.ALRMVAL;
    ctime->DAT = tmp & 0x00FF;
    ctime->MNT = tmp >> 8;

    tmp = RTC.ALRMVAL;
    ctime->HOU = tmp & 0x00FF;
    ctime->DAY = tmp >> 8;

    tmp = RTC.ALRMVAL;
    ctime->SEC = tmp & 0x00FF;
    ctime->MIN = tmp >> 8;

    ctime->YEA = 0;
}




/* ********************************************************************************************** */
/* end of file: csp_rtc_aget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_rtc_aset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

void    _csp_rtc_wr_en(void);
#define _csp_rtc_wr_dis()         __CSP_BIT_REG_CLR(RTC.RCFGCAL, __RTC_WR_EN_BIT);

__CSP_FUNC void  csp_rtc_alarm_set (RTC_TIME *ctime)
{
    UWORD temp;;

    _csp_rtc_wr_en();                                       /* Enable writing */

    temp = RTC.ALCFGRPT;                                    /* Save ALCFGRPT */

    __CSP_BIT_REG_CLR(RTC.ALCFGRPT, __RTC_ALARM_EN_BIT);                        /* Disable Alarm */
    __CSP_ACC_RANG_REG(BFA_WR,  RTC.ALCFGRPT, __RTC_PTR_FS,  __RTC_PTR_FE, 3);  /* year */

    RTC.ALRMVAL;
    RTC.ALRMVAL = (ctime->MIN << CHAR_BIT) | (ctime->SEC);
    RTC.ALRMVAL = (ctime->DAY << CHAR_BIT) | (ctime->HOU);
    RTC.ALRMVAL = (ctime->MNT << CHAR_BIT) | (ctime->DAT);

    if (temp & RTC_ALARM_EN) {
        __CSP_BIT_REG_SET(RTC.ALCFGRPT, __RTC_ALARM_EN_BIT);    /* Enable Alarm */
    }
    _csp_rtc_wr_dis();                                          /* Disable writing */
}




/* ********************************************************************************************** */
/* end of file: csp_rtc_aset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_rtc_cget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC RTC_CONF csp_rtc_conf_get (void)
{
    return __CSP_ACC_MASK_REG(BFA_RD, RTC.RCFGCAL, ~(__RTC_CAL_MASK));
}




/* ********************************************************************************************** */
/* end of file: csp_rtc_cget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_rtc_clgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC U08  csp_rtc_calib_get (void)
{
    return __CSP_ACC_RANG_VAL(BFA_RD, RTC.RCFGCAL, __RTC_CAL_FS, __RTC_CAL_FE);
}




/* ********************************************************************************************** */
/* end of file: csp_rtc_clgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_rtc_clst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

void    _csp_rtc_wr_en(void);
#define _csp_rtc_wr_dis()         __CSP_BIT_REG_CLR(RTC.RCFGCAL, __RTC_WR_EN_BIT);


__CSP_FUNC void  csp_rtc_calib_set (U08 val)
{
    _csp_rtc_wr_en();
    __CSP_ACC_RANG_REG(BFA_WR, RTC.RCFGCAL, __RTC_CAL_FS, __RTC_CAL_FE, val);
    _csp_rtc_wr_dis();
}




/* ********************************************************************************************** */
/* end of file: csp_rtc_clst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_rtc_cset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

void    _csp_rtc_wr_en(void);
#define _csp_rtc_wr_dis()         __CSP_BIT_REG_CLR(RTC.RCFGCAL, __RTC_WR_EN_BIT);

__CSP_FUNC void  csp_rtc_conf_set (RTC_CONF val)
{
    _csp_rtc_wr_en();
    __CSP_ACC_MASK_REG(BFA_WR, RTC.RCFGCAL, ~(__RTC_CAL_MASK), val);
    _csp_rtc_wr_dis();
}




/* ********************************************************************************************** */
/* end of file: csp_rtc_cset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_rtc_get.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_rtc_get (RTC_TIME *ctime)
{
    U16 tmp[2][4];

    tmp[0][0] = 0;
    tmp[0][0] = 1;

    do
    {
        __CSP_ACC_RANG_REG(BFA_WR,  RTC.RCFGCAL,  __RTC_PTR_FS, __RTC_PTR_FE, 3);  /* year */

        tmp[0][0] = RTC.RTCVAL;
        tmp[0][1] = RTC.RTCVAL;
        tmp[0][2] = RTC.RTCVAL;
        tmp[0][3] = RTC.RTCVAL;

        __CSP_ACC_RANG_REG(BFA_WR,  RTC.RCFGCAL,  __RTC_PTR_FS, __RTC_PTR_FE, 3);  /* year */

        tmp[1][0] = RTC.RTCVAL;
        tmp[1][1] = RTC.RTCVAL;
        tmp[1][2] = RTC.RTCVAL;
        tmp[1][3] = RTC.RTCVAL;

    } while (tmp[0][3] != tmp[1][3]);

    ctime->SEC = tmp[0][3] &  UCHAR_MAX;
    ctime->MIN = tmp[0][3] >> CHAR_BIT;
    ctime->HOU = tmp[0][2] &  UCHAR_MAX;
    ctime->DAY = tmp[0][2] >> CHAR_BIT;
    ctime->DAT = tmp[0][1] &  UCHAR_MAX;
    ctime->MNT = tmp[0][1] >> CHAR_BIT;
    ctime->YEA = tmp[0][0] &  UCHAR_MAX;
}




/* ********************************************************************************************** */
/* end of file: csp_rtc_get.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_rtc_set.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

void    _csp_rtc_wr_en(void);
#define _csp_rtc_wr_dis()         __CSP_BIT_REG_CLR(RTC.RCFGCAL, __RTC_WR_EN_BIT);

__CSP_FUNC void  csp_rtc_set (RTC_TIME *ctime)
{
    UWORD rtc, alarm;

    _csp_rtc_wr_en();                                /* Enable writing */

    rtc   = RTC.RCFGCAL;
    alarm = RTC.ALCFGRPT;
        
    __CSP_BIT_REG_CLR(RTC.RCFGCAL,  __RTC_EN_BIT);           /* Disable RTCC for properly writing */
    __CSP_BIT_REG_CLR(RTC.ALCFGRPT, __RTC_ALARM_EN_BIT);     /* Disable Alarm */

    __CSP_ACC_RANG_REG(BFA_WR,  RTC.RCFGCAL, __RTC_PTR_FS, __RTC_PTR_FE, 3);   /* year */

    RTC.RTCVAL =                                   (ctime->YEA);
    RTC.RTCVAL = ((UWORD)ctime->MNT << CHAR_BIT) | (ctime->DAT);
    RTC.RTCVAL = ((UWORD)ctime->DAY << CHAR_BIT) | (ctime->HOU);
    RTC.RTCVAL = ((UWORD)ctime->MIN << CHAR_BIT) | (ctime->SEC);

    if (rtc & RTC_EN) {
        __CSP_BIT_REG_SET(RTC.RCFGCAL, __RTC_EN_BIT);           /* Enable RTCC */
    }
    if (alarm & RTC_ALARM_EN) {
        __CSP_BIT_REG_SET(RTC.ALCFGRPT, __RTC_ALARM_EN_BIT);    /* Enable Alarm */
    }

    _csp_rtc_wr_dis();                               /* Disable writing */
}




/* ********************************************************************************************** */
/* end of file: csp_rtc_set.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_rtc_sget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_rtc_stat_get (void)
{
    return RTC.RCFGCAL;
}




/* ********************************************************************************************** */
/* end of file: csp_rtc_sget.c */
