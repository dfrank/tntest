/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_rtc.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_RTC_H
#define __CSP_RTC_H


#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    /* RTC programming model */
    /* --------------------- */

    typedef struct __RTC_TYPE
    {
        volatile UWORD ALRMVAL;
        volatile UWORD ALCFGRPT;
        volatile UWORD RTCVAL;
        volatile UWORD RCFGCAL;
    } RTC_TYPE;

    /* RTC base addresses */
    /* ------------------ */

    #define RTC_BASE_ADDR       0x0620

    /* Declare RTC model */
    /* ----------------- */

    extern  RTC_TYPE RTC __CSP_SFR(RTC_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    #define __RTC_CAL_FS            (0)
    #define __RTC_CAL_FE            (7)
    #define __RTC_CAL_MASK          (0xFF)

    #define __RTC_ALARM_REPEAT_MASK (0xFF)

    #define __RTC_WR_EN_BIT         (13)
    #define __RTC_EN_BIT            (15)
    #define __RTC_ALARM_EN_BIT      (15)

    #define __RTC_PTR_FS            (8)
    #define __RTC_PTR_FE            (9)


    /* Definitions for RTC API */
    /* ----------------------- */

    /* csp_rtc_conf_set() and csp_rtc_conf_get() parameters mask */

    typedef enum __RTC_CONF
    {
        RTC_EN          = (1 << 15),            /* RTCC module is enabled */
        RTC_DIS         = (0 << 15),            /* RTCC module is disabled */
                                                
        RTC_WR_EN       = (1 << 13),            /* RTCVAL<15:8> and RTCVAL<7:0> registers can be written to by the user */
        RTC_WR_DIS      = (0 << 13),            /* RTCVAL<15:8> and RTCVAL<7:0> registers are locked out from being written to by the user */
                                                
        RTC_OUT_EN      = (1 << 10),            /* RTCC clock output enabled */
        RTC_OUT_DIS     = (0 << 10),            /* RTCC clock output disabled */
                                                
        RTC_PTR_MIN_SEC = (0 <<  8),            /* Points to the MIN:SEC Value */
        RTC_PTR_WEE_HOU = (1 <<  8),            /* Points to the WEEK:HOUR Value */
        RTC_PTR_MNT_DAY = (2 <<  8),            /* Points to the MONTH:DAY Value */
        RTC_PTR_YEA     = (3 <<  8),            /* Points to the YEAR:--- Value */
    } RTC_CONF;


    /* csp_rtc_alarm_conf_set() and csp_rtc_alarm_conf_get parameters mask */

    typedef enum __RTC_ALARM_CONF
    {
        RTC_ALARM_EN            = (1 << 15),    /* Alarm is enabled */
        RTC_ALARM_DIS           = (0 << 15),    /* Alarm is disabled */
                                                
        RTC_ALARM_CHIME_EN      = (1 << 14),    /* Chime is enabled; ARPT<7:0> is allowed to roll over from 00h to FFh */
        RTC_ALARM_CHIME_DIS     = (0 << 14),    /* Chime is disabled; ARPT<7:0> stops once it reaches 00h */
                                                
        RTC_ALARM_MASK_HALF_SEC = (0 << 10),    /* Alarm Mask every half second */
        RTC_ALARM_MASK_SEC      = (1 << 10),    /* Alarm Mask every second */
        RTC_ALARM_MASK_10_SEC   = (2 << 10),    /* Alarm Mask every 10 seconds */
        RTC_ALARM_MASK_MIN      = (3 << 10),    /* Alarm Mask every minute */
        RTC_ALARM_MASK_10_MIN   = (4 << 10),    /* Alarm Mask every 10 minutes */
        RTC_ALARM_MASK_HOUR     = (5 << 10),    /* Alarm Mask every hour */
        RTC_ALARM_MASK_DAY      = (6 << 10),    /* Alarm Mask every day */
        RTC_ALARM_MASK_WEEK     = (7 << 10),    /* Alarm Mask every week */
        RTC_ALARM_MASK_MONTH    = (8 << 10),    /* Alarm Mask every month */
        RTC_ALARM_MASK_YEAR     = (9 << 10),    /* Alarm Mask every year */
                                                
        RTC_ALARM_PTR_MIN_SEC   = (0 <<  8),    /* Points to the MIN:SEC Value */
        RTC_ALARM_PTR_WEE_HOU   = (1 <<  8),    /* Points to the WEEK:HOUR Value */
        RTC_ALARM_PTR_MNT_DAY   = (2 <<  8),    /* Points to the MONTH:DAY Value */
        RTC_ALARM_PTR_YEA       = (3 <<  8)     /* Points to the YEAR:--- Value */
    } RTC_ALARM_CONF;

    /* csp_rtc_stat_get() return value type */

    typedef enum __RTC_STAT
    {
        RTC_STAT_SYNC_WARN = (1 << 12), /* RO - RTCVAL<15:8>, RTCVAL<7:0> and ALCFGRPT registers can change while reading */
        RTC_STAT_HALF_SEC  = (1 << 11)  /* RO - Second half period of a second  */
    } RTC_STAT;

    /* csp_rtc_set() and other functions parameters type */

    typedef struct _RTC_TIME
    {
        U08 SEC;     /* Seconds     */
        U08 MIN;     /* Minutes     */
        U08 HOU;     /* Hours       */
        U08 DAY;     /* Day of week */
        U08 DAT;     /* Date        */
        U08 MNT;     /* Month       */
        U08 YEA;     /* Year        */
    } RTC_TIME;


#endif


/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_RTC)

        void            csp_rtc_conf_set(RTC_CONF val);
        RTC_CONF        csp_rtc_conf_get(void);

        void            csp_rtc_calib_set(U08 val);
        U08             csp_rtc_calib_get(void);

        void            csp_rtc_alarm_conf_set(RTC_ALARM_CONF val);
        RTC_ALARM_CONF  csp_rtc_alarm_conf_get(void);

        void            csp_rtc_set(RTC_TIME *ctime);
        void            csp_rtc_get(RTC_TIME *ctime);

        void            csp_rtc_alarm_set(RTC_TIME *ctime);
        void            csp_rtc_alarm_get(RTC_TIME *ctime);

        RTC_STAT        csp_rtc_stat_get(void);

    #else

        #include "csp_rtc.c"

    #endif

#endif

#endif /* #ifndef __CSP_RTC_H */
/* ********************************************************************************************** */
/* end of file: csp_rtc.h */
