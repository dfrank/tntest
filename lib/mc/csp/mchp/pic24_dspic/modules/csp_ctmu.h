/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ctmu.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_CTMU_H
#define __CSP_CTMU_H

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    /* CTMU programming model */
    /* ---------------------- */

    typedef struct __CTMU_TYPE
    {
        volatile UWORD CTMUCON;
        volatile UWORD CTMUICON;
    } CTMU_TYPE;

    /* CTMU base addresses */
    /* ------------------- */

    #define CTMU_BASE_ADDR       0x033C

    /* Declare RTC model */
    /* ----------------- */

    extern  CTMU_TYPE CTMU __CSP_SFR(CTMU_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    #define __CTMU_CUR_FS           (10)
    #define __CTMU_CUR_FE           (15)

    #define __CTMU_CUR_CONF_FS      (8)
    #define __CTMU_CUR_CONF_FE      (9)



    #define __CTMU_DISCHARGE_BIT    (9)
    #define __CTMU_EDGE_1_EN_BIT    (0)
    #define __CTMU_EDGE_2_EN_BIT    (1)

    /* Definitions for CTMU API */
    /* ------------------------ */

    typedef enum __CTMU_CONF
    {
        CTMU_EN           =  (1 << 15),     /* Module is enabled  */
        CTMU_DIS          =  (0 << 15),     /* Module is disabled */
                                            
        CTMU_IDLE_STOP    =  (1 << 13),     /* Discontinue module operation when device enters Idle mode */
        CTMU_IDLE_CON     =  (0 << 13),     /* Continue module operation in Idle mode */
                                            
        CTMU_MODE_GEN     =  (1 << 12),     /* Enables edge delay generation  */
        CTMU_MODE_MEASURE =  (0 << 12),     /* Disables edge delay generation */
                                            
        CTMU_EDGE_EN      =  (1 << 11),     /* Edges are not blocked */
        CTMU_EDGE_DIS     =  (0 << 11),     /* Edges are blocked     */
                                            
        CTMU_EDGE_SEQ_EN  =  (1 << 10),     /* Edge 1 event must occur before Edge 2 event can occur */
        CTMU_EDGE_SEQ_DIS =  (0 << 10),     /* No edge sequence is needed */
                                            
        CTMU_DISCHARGE    =  (1 <<  9),     /* Analog current source output is grounded     */
        CTMU_CHARGE       =  (0 <<  9),     /* Analog current source output is not grounded */
                                            
        CTMU_TRIG_EN      =  (1 <<  8),     /* Trigger output is enabled  */
        CTMU_TRIG_DIS     =  (0 <<  8),     /* Trigger output is disabled */
                                            
        CTMU_EDGE_2_POS   =  (1 <<  7),     /* Edge 2 programmed for a positive level response */
        CTMU_EDGE_2_NEG   =  (0 <<  7),     /* Edge 2 programmed for a negative level response */
                                            
        CTMU_EDGE_2_INP_0 =  (0 <<  5),     /* Edge Source 0 selected */
        CTMU_EDGE_2_INP_1 =  (1 <<  5),     /* Edge Source 1 selected */
        CTMU_EDGE_2_INP_2 =  (2 <<  5),     /* Edge Source 2 selected */
        CTMU_EDGE_2_INP_3 =  (3 <<  5),     /* Edge Source 3 selected */
                                            
        CTMU_EDGE_1_POS   =  (1 <<  4),     /* Edge 1 programmed for a positive level response */
        CTMU_EDGE_1_NEG   =  (0 <<  4),     /* Edge 1 programmed for a negative level response */
                                            
        CTMU_EDGE_1_INP_0 =  (0 <<  2),     /* Edge Source 0 selected */
        CTMU_EDGE_1_INP_1 =  (1 <<  2),     /* Edge Source 1 selected */
        CTMU_EDGE_1_INP_2 =  (2 <<  2),     /* Edge Source 2 selected */
        CTMU_EDGE_1_INP_3 =  (3 <<  2),     /* Edge Source 3 selected */
        
        CTMU_EDGE_2_EN    =  (1 <<  1),
        CTMU_EDGE_2_DIS   =  (0 <<  1),
        
        CTMU_EDGE_1_EN    =  (1 <<  0),
        CTMU_EDGE_1_DIS   =  (0 <<  0)
    } CTMU_CONF;


    typedef enum __CTMU_CUR
    {
        CTMU_CUR_DIS         = (0),
        CTMU_CUR_BASE_X_1    = (1),
        CTMU_CUR_BASE_X_10   = (2),
        CTMU_CUR_BASE_X_100  = (3)
    } CTMU_CUR;

    typedef enum __CTMU_COMM
    {
        CTMU_COMM_DISCHARGE  = (1 << 0),
        CTMU_COMM_CHARGE     = (1 << 1),
        CTMU_COMM_EDGE_1_EN  = (1 << 2),
        CTMU_COMM_EDGE_1_DIS = (1 << 3),
        CTMU_COMM_EDGE_2_EN  = (1 << 4),
        CTMU_COMM_EDGE_2_DIS = (1 << 5)
    } CTMU_COMM;

    #define CTMU_CUR_TRIM_MAX       ( 31)
    #define CTMU_CUR_TRIM_MIN       (-31)


#endif

/* External functions prototypes */
/* ----------------------------- */

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    #if !defined(__CSP_BUILD_LIB)

        /* Module API definition */

        #if defined(__CSP_DEBUG_CTMU)

            void      csp_ctmu_conf_set(CTMU_CONF val);
            CTMU_CONF csp_ctmu_conf_get(void);

            void      csp_ctmu_cur_set(CTMU_CUR conf, UWORD val);
            CTMU_CUR  csp_ctmu_cur_get(UWORD *val);

            void      csp_ctmu_comm(CTMU_COMM com);

        #else

            #include "csp_ctmu.c"

        #endif

    #endif

#endif

#endif /* #ifndef __CSP_CTMU_H */
/* ********************************************************************************************** */
/* end of file: csp_ctmu.h */
