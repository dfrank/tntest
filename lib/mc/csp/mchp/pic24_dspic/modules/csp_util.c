/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_util_atmn.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC CSP_RETVAL  csp_util_adc_timing (U32 fcy, U32 freq, ADC_TM *adc)
{
    UWORD ret;

    if ((2 * fcy) < freq)
        return CSP_ERR_PARAM;

    ret = (2 * fcy / freq) - 1;

    if (ret > UCHAR_MAX)
        return CSP_ERR_PARAM;

    adc->val    = (UWORD)ret;
    adc->actual = (2 * fcy) / (adc->val + 1);

    return (CSP_ERR_NO);
}


/* ********************************************************************************************** */
/* end of file: csp_util_atmn.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_util_btpb.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_util_bin_to_bcd (UWORD val)
{
    return (val / 10 << 4) | (val % 10);
}


/* ********************************************************************************************** */
/* end of file: csp_util_btpb.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_util_fcyg.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC S32  csp_util_fcy_get (U32 freq)
{
    OSC_MODE    osc_mode;
    OSC_FRC_PS  osc_frc;

    osc_mode = csp_osc_get();
    osc_frc  = csp_osc_frc_get();

    switch (osc_mode)
    {
        /* FRC clock - FCY = 4 MHz */

        case OSC_MODE_FRC:
            return (__OSC_FRC_BASE_FREQ / 2);

        /* FRC with PLL clock */

        case OSC_MODE_FRC_PLL:

            switch (osc_frc)
            {
                case OSC_FRC_PS_1: return (((__OSC_FRC_BASE_FREQ / 1) * __OSC_PLL) / 2);
                case OSC_FRC_PS_2: return (((__OSC_FRC_BASE_FREQ / 2) * __OSC_PLL) / 2);
                default:           return (-1);
            }

        /* FRC with PS clock */

        case OSC_MODE_FRC_PS:

            switch (osc_frc)
            {
                case OSC_FRC_PS_1:     return ((__OSC_FRC_BASE_FREQ / 1)   / 2);
                case OSC_FRC_PS_2:     return ((__OSC_FRC_BASE_FREQ / 2)   / 2);
                case OSC_FRC_PS_4:     return ((__OSC_FRC_BASE_FREQ / 4)   / 2);
                case OSC_FRC_PS_8:     return ((__OSC_FRC_BASE_FREQ / 8)   / 2);
                case OSC_FRC_PS_16:    return ((__OSC_FRC_BASE_FREQ / 16)  / 2);
                case OSC_FRC_PS_32:    return ((__OSC_FRC_BASE_FREQ / 32)  / 2);
                case OSC_FRC_PS_64:    return ((__OSC_FRC_BASE_FREQ / 64)  / 2);
                case OSC_FRC_PS_256:   return ((__OSC_FRC_BASE_FREQ / 256) / 2);
                default:               return (-1);
            }

        /* LPRC clock */

        case OSC_MODE_LPRC:
            return (__OSC_LPRC_FREQ / 2);

        /* Primary oscilator clock */

        case OSC_MODE_PRIMARY:
            return (freq / 2);

        /* Primary oscillator with PLL */

        case OSC_MODE_PRIMARY_PLL:
            return (freq * __OSC_PLL / 2);

        /* Secondary oscillator */

        case OSC_MODE_SECONDARY:
            return (__OSC_SEC_FREQ / 2);

        default:
            return (-1);
    }
}

/* ********************************************************************************************** */
/* end of file: csp_util_fcyg.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_util_itmn.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC CSP_RETVAL  csp_util_i2c_timing (U32 fcy, U32 freq, I2C_TM *i2c)
{
    U32 val;

    val = (fcy / (freq * 2)) - 1;

    if ((val < 2)        ||
        (val > UINT_MAX)
       )
        return CSP_ERR_PARAM;

    i2c->val    = (U32)val;
    i2c->actual = fcy / (((U16)val + 1) * 2);

    return CSP_ERR_NO;
}


/* ********************************************************************************************** */
/* end of file: csp_util_itmn.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_util_pbtb.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_util_bcd_to_bin (UWORD val)
{
    return (val & 0x0F) + ((val >> 4) * 10);
}


/* ********************************************************************************************** */
/* end of file: csp_util_pbtb.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_util_stmn.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC CSP_RETVAL  csp_util_spi_timing (U32 fcy, U32 freq, SPI_TM *spi)
{
    const U16 ps_tbl[26][2] =
    {
        {  1, 31}, {  2, 27}, {  3, 23}, {  4, 19}, {  5, 15}, {  6, 11}, {  7,  7},
        {  8,  3}, { 12, 22}, { 16, 18}, { 20, 14}, { 24, 10}, { 28,  6}, { 32,  2},
        { 48, 21}, { 64, 17}, { 80, 13}, { 96,  9}, {112,  5}, {128,  1}, {192, 20},
        {256, 16}, {320, 12}, {384,  8}, {448,  4}, {512,  0}
    };

    U32 ps_tmp;
    U16 indx;

    ps_tmp = fcy / freq;

    for (indx = 0; indx < 26; indx++)
    {
        if (ps_tbl[indx][0] > ps_tmp)
            break;
    }

    if (indx == 0)
        indx = 1;   /* if fcy < freq */

    spi->val    = ps_tbl[indx-1][1];
    spi->actual = fcy / ps_tbl[indx-1][0];

    return CSP_ERR_NO;
}


/* ********************************************************************************************** */
/* end of file: csp_util_stmn.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_util_ttmn.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC CSP_RETVAL  csp_util_tmr_timing (U32 fcy, U32 period, TMR_TM *tmr)
{
    const U16 ps_tbl[] = {0, 3, 6, 8};
    const U16 ps_ret[] = {TMR_PS_1_1, TMR_PS_1_8, TMR_PS_1_64, TMR_PS_1_256};

    U64 per64;
    U16 ps_ptr = 0;

    for (;;)
    {
        per64 = ((U64)fcy * period) >> ps_tbl[ps_ptr];

        if (per64 > (0xFFFFULL * __UTIL_US_CONST + __UTIL_US_CONST - 1))
        {
            if (++ps_ptr >= sizeof(ps_tbl))
                return CSP_ERR_PARAM;
        }
        else
            break;
    }

    if (per64 == 0)
        return CSP_ERR_PARAM;

    tmr->period  = (U16)(per64 / __UTIL_US_CONST);
    tmr->ps_mask = ps_ret[ps_ptr];

    tmr->actual  = ((tmr->period * (U64)__UTIL_US_CONST) / fcy) << ps_tbl[ps_ptr];

    return CSP_ERR_NO;
}


/* ********************************************************************************************** */
/* end of file: csp_util_ttmn.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_util_utmn.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC CSP_RETVAL  csp_util_uart_timing (U32 fcy, U32 baud, UART_TM *uart)
{
    UWORD mode;
    U32   ret;

    mode = uart->high ? 4 : 16;

    ret = fcy / (mode * baud) - 1;

    if (ret > UINT_MAX)
        return CSP_ERR_PARAM;

    uart->val    = (UWORD)ret;
    uart->actual = fcy / (mode * ((UWORD)ret + 1));

    return CSP_ERR_NO;
}


/* ********************************************************************************************** */
/* end of file: csp_util_utmn.c */
