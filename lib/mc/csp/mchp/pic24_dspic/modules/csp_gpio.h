/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_GPIO_H
#define __CSP_GPIO_H

#if (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010)

    /* GPIO programming model */
    /* ---------------------- */

    typedef struct __GPIO_TYPE
    {
        volatile UWORD TRIS;
        volatile UWORD PORT;
        volatile UWORD LAT;
        __CSP_SKIP_BYTE(1018);
        volatile UWORD ODC;
    } GPIO_TYPE;

    /* GPIO base address and GPIO model declare */
    /* ---------------------------------------- */

    #define GPIO_A_BASE_ADDR        0x02C0
    #define GPIO_B_BASE_ADDR        0x02C6
    #define GPIO_C_BASE_ADDR        0x02CC
    #define GPIO_D_BASE_ADDR        0x02D2
    #define GPIO_E_BASE_ADDR        0x02D8
    #define GPIO_F_BASE_ADDR        0x02DE
    #define GPIO_G_BASE_ADDR        0x02E4

    extern  GPIO_TYPE PORTA __CSP_SFR(GPIO_A_BASE_ADDR);
    extern  GPIO_TYPE PORTB __CSP_SFR(GPIO_B_BASE_ADDR);
    extern  GPIO_TYPE PORTC __CSP_SFR(GPIO_C_BASE_ADDR);
    extern  GPIO_TYPE PORTD __CSP_SFR(GPIO_D_BASE_ADDR);
    extern  GPIO_TYPE PORTE __CSP_SFR(GPIO_E_BASE_ADDR);
    extern  GPIO_TYPE PORTF __CSP_SFR(GPIO_F_BASE_ADDR);
    extern  GPIO_TYPE PORTG __CSP_SFR(GPIO_G_BASE_ADDR);


#elif (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)

    /* GPIO programming model */
    /* ---------------------- */

    typedef struct __GPIO_TYPE
    {
        volatile UWORD TRIS;
        volatile UWORD PORT;
        volatile UWORD LAT;
        volatile UWORD ODC;
    } GPIO_TYPE;

    /* GPIO base address and GPIO model declare */
    /* ---------------------------------------- */

    #define GPIO_A_BASE_ADDR        0x02C0
    #define GPIO_B_BASE_ADDR        0x02C8

    extern  GPIO_TYPE PORTA __CSP_SFR(GPIO_A_BASE_ADDR);
    extern  GPIO_TYPE PORTB __CSP_SFR(GPIO_B_BASE_ADDR);

    #if (__CSP_PIN_COUNT == __CSP_PIN_COUNT_44)

        #define GPIO_C_BASE_ADDR        0x02D0
        extern  GPIO_TYPE PORTC __CSP_SFR(GPIO_C_BASE_ADDR);

    #endif

#elif (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110)

    /* GPIO programming model */
    /* ---------------------- */

    typedef struct __GPIO_TYPE
    {
        volatile UWORD TRIS;
        volatile UWORD PORT;
        volatile UWORD LAT;
        volatile UWORD ODC;
    } GPIO_TYPE;

    /* GPIO base address and GPIO model declare */
    /* ---------------------------------------- */

    #define GPIO_A_BASE_ADDR        0x02C0
    #define GPIO_B_BASE_ADDR        0x02C8
    #define GPIO_C_BASE_ADDR        0x02D0
    #define GPIO_D_BASE_ADDR        0x02D8
    #define GPIO_E_BASE_ADDR        0x02E0
    #define GPIO_F_BASE_ADDR        0x02E8
    #define GPIO_G_BASE_ADDR        0x02F0

    extern  GPIO_TYPE PORTB __CSP_SFR(GPIO_B_BASE_ADDR);
    extern  GPIO_TYPE PORTC __CSP_SFR(GPIO_C_BASE_ADDR);
    extern  GPIO_TYPE PORTD __CSP_SFR(GPIO_D_BASE_ADDR);
    extern  GPIO_TYPE PORTE __CSP_SFR(GPIO_E_BASE_ADDR);
    extern  GPIO_TYPE PORTF __CSP_SFR(GPIO_F_BASE_ADDR);
    extern  GPIO_TYPE PORTG __CSP_SFR(GPIO_G_BASE_ADDR);
    extern  GPIO_TYPE PORTA __CSP_SFR(GPIO_A_BASE_ADDR);

#elif (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)

    /* GPIO programming model */
    /* ---------------------- */

    typedef struct __GPIO_TYPE
    {
        volatile UWORD TRIS;
        volatile UWORD PORT;
        volatile UWORD LAT;
        volatile UWORD ODC;
    } GPIO_TYPE;

    /* GPIO base address and GPIO model declare */
    /* ---------------------------------------- */

    #define GPIO_B_BASE_ADDR        0x02C8
    #define GPIO_C_BASE_ADDR        0x02D0
    #define GPIO_D_BASE_ADDR        0x02D8
    #define GPIO_E_BASE_ADDR        0x02E0
    #define GPIO_F_BASE_ADDR        0x02E8
    #define GPIO_G_BASE_ADDR        0x02F0

    extern  GPIO_TYPE PORTB __CSP_SFR(GPIO_B_BASE_ADDR);
    extern  GPIO_TYPE PORTC __CSP_SFR(GPIO_C_BASE_ADDR);
    extern  GPIO_TYPE PORTD __CSP_SFR(GPIO_D_BASE_ADDR);
    extern  GPIO_TYPE PORTE __CSP_SFR(GPIO_E_BASE_ADDR);
    extern  GPIO_TYPE PORTF __CSP_SFR(GPIO_F_BASE_ADDR);
    extern  GPIO_TYPE PORTG __CSP_SFR(GPIO_G_BASE_ADDR);

    #if ((__CSP_PIN_COUNT == __CSP_PIN_COUNT_80)  ||  \
           (__CSP_PIN_COUNT == __CSP_PIN_COUNT_100)     \
          )

        #define GPIO_A_BASE_ADDR        0x02C0
        extern  GPIO_TYPE PORTA __CSP_SFR(GPIO_A_BASE_ADDR);

    #endif

#elif (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

    /* GPIO programming model */
    /* ---------------------- */

    typedef struct __GPIO_TYPE
    {
        volatile UWORD TRIS;
        volatile UWORD PORT;
        volatile UWORD LAT;
        volatile UWORD ODC;
    } GPIO_TYPE;

    /* GPIO base address and GPIO model declare */
    /* ---------------------------------------- */

    #define GPIO_A_BASE_ADDR        0x02C0
    #define GPIO_B_BASE_ADDR        0x02C8

    extern  GPIO_TYPE PORTA __CSP_SFR(GPIO_A_BASE_ADDR);
    extern  GPIO_TYPE PORTB __CSP_SFR(GPIO_B_BASE_ADDR);

#endif


#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    /* PAD programming model */
    /* --------------------- */

    typedef struct __PAD_TYPE
    {
        volatile UWORD PADCFG1;
    } PAD_TYPE;

    /* PAD base address */
    /* ---------------- */

    #define PAD_BASE_ADDR       0x02FC

    /* Declare PAD model */
    /* ----------------- */

    extern  PAD_TYPE PAD __CSP_SFR(PAD_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    /* Definitions for GPIO API */
    /* ------------------------ */

    /* csp_gpio_pin_set(), csp_gpio_pin_clr() and other functions parameters type */

    typedef enum __GPIO_PIN
    {
        GPIO_0,
        GPIO_1,
        GPIO_2,
        GPIO_3,
        GPIO_4,
        GPIO_5,
        GPIO_6,
        GPIO_7,
        GPIO_8,
        GPIO_9,
        GPIO_10,
        GPIO_11,
        GPIO_12,
        GPIO_13,
        GPIO_14,
        GPIO_15,
    } GPIO_PIN;

    /* csp_gpio_pin_dir_get() return type */

    typedef enum __GPIO_DIR
    {
        GPIO_DIR_OUT,
        GPIO_DIR_IN
    } GPIO_DIR;

    /* csp_gpio_pin_od_get() return type */

    typedef enum __GPIO_MODE
    {
        GPIO_MODE_NORMAL,
        GPIO_MODE_OD
    } GPIO_MODE;

    /* csp_gpio_pad_conf_set() and csp_gpio_pad_conf_set() parameters macro */

    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
        )

        typedef enum __GPIO_PAD_CONF
        {
            GPIO_PAD_PMP_IN_TTL     = (1 << 0),     /* PADCFG1<0> */
            GPIO_PAD_PMP_IN_SCHM    = (0),
        
            GPIO_PAD_RTCC_OUT_CLK   = (1 << 1),     /* PADCFG1<1> */
            GPIO_PAD_RTCC_OUT_ALARM = (0)
        } GPIO_PAD_CONF;

    #elif (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

        typedef enum __GPIO_PAD_CONF
        {
            /* PADCFG1<4> */
            GPIO_PAD_I2C_SMBUS      = (1 << 4),     /* The I2C module is configured for a longer SMBus input delay (nominal 300 ns delay)   */
            GPIO_PAD_I2C_NORMAL     = (0 << 4),     /* The I2C module is configured for a legacy input delay (nominal 150 ns delay)         */

            /* PADCFG1<3> */
            GPIO_PAD_OC1_DIS        = (1 << 3),     /* OC1 output will not be active on the pin; OCPWM1 can still be used for internal triggers  */
            GPIO_PAD_OC1_EN         = (0 << 3),     /* OC1 output will be active on the pin based on the OCPWM1 module settings     */

            /* PADCFG1<1:2> */
            GPIO_PAD_RTCC_OUT_SRC   = (2 << 1),     /* RTCC source clock is selected for the RTCC pin (can be LPRC or SOSC) */
            GPIO_PAD_RTCC_OUT_SEC   = (1 << 1),     /* RTCC seconds clock is selected for the RTCC pin  */
            GPIO_PAD_RTCC_OUT_ALARM = (0 << 1),     /* RTCC alarm pulse is selected for the RTCC pin    */
        } GPIO_PAD_CONF;

    #endif

#endif

/* Definition of rair PORT-PIN
 
    Instruction: usually, when you write BSP code you used following definition:
 
    #define FOO_PORT        &PORTA
    #define FOO_PIN         GPIO_1
 
    then you used it like this:
 
    csp_gpio_pin_dir_in(FOO_PORT, FOO_PIN);
 
    Instead of repeat FOO, you can use more readable:
 
    csp_gpio_pin_dir_in(GPIO_DEF(FOO));
*/
#define GPIO_DEF(a)                  a##_PORT, a##_PIN


/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_GPIO)



        void      csp_gpio_pin_od_en(GPIO_TYPE *gpio, GPIO_PIN pin);
        void      csp_gpio_pin_od_dis(GPIO_TYPE *gpio, GPIO_PIN pin);
        GPIO_MODE csp_gpio_pin_od_get(GPIO_TYPE *gpio, GPIO_PIN pin);

        void      csp_gpio_pin_dir_in(GPIO_TYPE *gpio, GPIO_PIN pin);
        void      csp_gpio_pin_dir_out(GPIO_TYPE *gpio, GPIO_PIN pin);
        void      csp_gpio_pin_dir_tgl(GPIO_TYPE *gpio, GPIO_PIN pin);
        GPIO_DIR  csp_gpio_pin_dir_get(GPIO_TYPE *gpio, GPIO_PIN pin);

        void      csp_gpio_pin_set(GPIO_TYPE *gpio, GPIO_PIN pin);
        void      csp_gpio_pin_clr(GPIO_TYPE *gpio, GPIO_PIN pin);
        void      csp_gpio_pin_tgl(GPIO_TYPE *gpio, GPIO_PIN pin);
        UWORD     csp_gpio_pin_check(GPIO_TYPE *gpio, GPIO_PIN pin);



        void      csp_gpio_port_od_en(GPIO_TYPE *gpio, UWORD mask);
        void      csp_gpio_port_od_dis(GPIO_TYPE *gpio, UWORD mask);
        UWORD     csp_gpio_port_od_get(GPIO_TYPE *gpio);

        void      csp_gpio_port_dir_in(GPIO_TYPE *gpio, UWORD mask);
        void      csp_gpio_port_dir_out(GPIO_TYPE *gpio, UWORD mask);
        void      csp_gpio_port_dir_tgl(GPIO_TYPE *gpio, UWORD mask);
        UWORD     csp_gpio_port_dir_get(GPIO_TYPE *gpio);

        void      csp_gpio_port_set(GPIO_TYPE *gpio, UWORD mask);
        void      csp_gpio_port_clr(GPIO_TYPE *gpio, UWORD mask);
        void      csp_gpio_port_tgl(GPIO_TYPE *gpio, UWORD mask);
        UWORD     csp_gpio_port_get(GPIO_TYPE *gpio);
        void      csp_gpio_port_wr(GPIO_TYPE *gpio, UWORD val);

        void          csp_gpio_pad_conf_set(PAD_TYPE *pad, GPIO_PAD_CONF val);
        GPIO_PAD_CONF csp_gpio_pad_conf_get(PAD_TYPE *pad);

    #else

        #include "csp_gpio.c"

    #endif

#endif

#endif /* #ifndef __CSP_GPIO_H */
/* ********************************************************************************************** */
/* end of file: csp_gpio.h */
