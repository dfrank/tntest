/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_OSC_H
#define __CSP_OSC_H


#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    /* Oscillator programming model */
    /* ---------------------------- */

    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
        )

        typedef struct __OSC_TYPE
        {
            volatile UWORD OSCCON;
            volatile UWORD CLKDIV;
            __CSP_SKIP_BYTE(2);
            volatile UWORD OSCTUN;
        } OSC_TYPE;

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) || \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)      \
          )

        typedef struct __OSC_TYPE
        {
            volatile UWORD OSCCON;
            volatile UWORD CLKDIV;
            __CSP_SKIP_BYTE(2);
            volatile UWORD OSCTUN;
            __CSP_SKIP_BYTE(4);
            volatile UWORD REFOCON;
        } OSC_TYPE;

    #endif

    /* Oscillator base address */
    /* ----------------------- */

    #define OSC_BASE_ADDR    0x0742

    /* Declare oscillator model */
    /* ------------------------ */

    extern  OSC_TYPE OSC __CSP_SFR(OSC_BASE_ADDR);


    /* Internal API definitions */
    /* ------------------------ */

    #define __OSC_COMM_LOCK         (1 << 7)    /* OSCCON<7> */
    #define __OSC_COMM_SWITCH       (1 << 0)    /* OSCCON<0> */
    #define __OSC_COMM_SWITCH_BIT   (0)

    #define __OSC_SECONDARY_EN      (1 << 1)    /* OSCCON<1> */

    #define __OSC_DOZE_CONF_MASK    (OSC_DOZE_RECOVER_EN | OSC_DOZE_EN)

    #define __OSC_DOZE_FS           (12)
    #define __OSC_DOZE_FE           (14)

    #define __OSC_FRC_PS_FS         (8)
    #define __OSC_FRC_PS_FE         (10)

    #define __OSC_MODE_FS           (12)
    #define __OSC_MODE_FE           (14)

    #define __OSC_L_FS              (0)
    #define __OSC_L_FE              (7)



    #define __OSC_FRC_BASE_FREQ     (8000000UL) /* Hz */

    #if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)
    #define __OSC_LPFRC_BASE_FREQ   (500000UL) /* Hz */
    #endif

    #define __OSC_LPRC_FREQ         (31000UL)   /* Hz */

    #define __OSC_SEC_FREQ          (32768UL)   /* Hz */

    #define __OSC_PLL               (4)

    /* Definitions for oscillator API */
    /* ------------------------------ */

    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
        )

        /* csp_osc_conf_set() and csp_osc_conf_get() parameters type */

        typedef enum __OSC_CONF
        {
            OSC_PRIMARY_SLEEP_STOP = (0 << 2),          /* OSCCON<2> */
            OSC_PRIMARY_SLEEP_CON  = (1 << 2),          /* OSCCON<2> */

            #if (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)

            OSC_USB_PLL_CPU_32_MHZ = (0 << 6),              /* CLKDIV */
            OSC_USB_PLL_CPU_16_MHZ = (1 << 6),              /* CLKDIV */
            OSC_USB_PLL_CPU_8_MHZ  = (2 << 6),              /* CLKDIV */
            OSC_USB_PLL_CPU_4_MHZ  = (3 << 6),              /* CLKDIV */

            #endif

        } OSC_CONF;
         
        #define __OSC_CONF_MASK_OSCCON      OSC_PRIMARY_SLEEP_CON    /* OSCCON<2>: OSC_PRIMARY_SLEEP_CON >> 16 */

        #if (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110)
            #define __OSC_CONF_MASK_CLKDIV  (0)
        #elif (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)
            #define __OSC_CONF_MASK_CLKDIV  (OSC_USB_PLL_CPU_4_MHZ)
        #endif

    #endif

    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
        )

        /* csp_osc_ref_conf_set() and csp_osc_ref_conf_get() parameters type */

        typedef enum __OSC_REF_CONF
        {
            OSC_REF_EN              = (1 << 15),
            OSC_REF_DIS             = (0 << 15),
    
            OSC_REF_SLEEP_STOP      = (0 << 13),
            OSC_REF_SLEEP_CON       = (1 << 13),
    
            OSC_REF_SOURCE_PRIMARY  = (1 << 12),
            OSC_REF_SOURCE_SYSTEM   = (0 << 12),
    
            OSC_REF_POSTSCALE_1     = (0  << 8),
            OSC_REF_POSTSCALE_2     = (1  << 8),
            OSC_REF_POSTSCALE_4     = (2  << 8),
            OSC_REF_POSTSCALE_8     = (3  << 8),
            OSC_REF_POSTSCALE_16    = (4  << 8),
            OSC_REF_POSTSCALE_32    = (5  << 8),
            OSC_REF_POSTSCALE_64    = (6  << 8),
            OSC_REF_POSTSCALE_128   = (7  << 8),
            OSC_REF_POSTSCALE_256   = (8  << 8),
            OSC_REF_POSTSCALE_512   = (9  << 8),
            OSC_REF_POSTSCALE_1024  = (10 << 8),
            OSC_REF_POSTSCALE_2048  = (11 << 8),
            OSC_REF_POSTSCALE_4096  = (12 << 8),
            OSC_REF_POSTSCALE_8192  = (13 << 8),
            OSC_REF_POSTSCALE_16384 = (14 << 8),
            OSC_REF_POSTSCALE_32768 = (15 << 8)
        } OSC_REF_CONF;

    #endif

    /* csp_osc_get() and csp_osc_set() parameters type */

    typedef enum __OSC_MODE
    {
        OSC_MODE_FRC         = 0,
        OSC_MODE_FRC_PLL     = 1,
        OSC_MODE_PRIMARY     = 2,
        OSC_MODE_PRIMARY_PLL = 3,
        OSC_MODE_SECONDARY   = 4,
        OSC_MODE_LPRC        = 5,
        #if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)
        OSC_MODE_LPFRC_PS    = 6,
        #endif
        OSC_MODE_FRC_PS      = 7
    } OSC_MODE;


    /* csp_osc_stat_get() and csp_osc_stat_clr() parameters type */

    typedef enum __OSC_STAT
    {
        OSC_STAT_LOCK        = (1 << 5),    /* OSCCON<5>, ro  */
        OSC_STAT_FAIL_DETECT = (1 << 3)     /* OSCCON<3>, r/c */
    } OSC_STAT;

    /* csp_osc_frc_set() and csp_osc_frc_get() parameters type */

    typedef enum __OSC_FRC
    {
        OSC_FRC_PS_1   = 0,
        OSC_FRC_PS_2   = 1,
        OSC_FRC_PS_4   = 2,
        OSC_FRC_PS_8   = 3,
        OSC_FRC_PS_16  = 4,
        OSC_FRC_PS_32  = 5,
        OSC_FRC_PS_64  = 6,
        OSC_FRC_PS_256 = 7
    } OSC_FRC_PS;

    /* csp_osc_doze_conf_set() and csp_osc_doze_conf_get() parameters mask */

    typedef enum __OSC_DOZE_CONF
    {
        OSC_DOZE_RECOVER_EN  = (1 << 15),   /* CLKDIV<15> */
        OSC_DOZE_RECOVER_DIS = (0),

        OSC_DOZE_EN          = (1 << 11),   /* CLKDIV<11> */
        OSC_DOZE_DIS         = (0)
    } OSC_DOZE_CONF;

    /* csp_osc_doze_set() and csp_osc_doze_set() parameters type */

    typedef enum __OSC_DOZE
    {
        OSC_DOZE_1_1   = 0,
        OSC_DOZE_1_2   = 1,
        OSC_DOZE_1_4   = 2,
        OSC_DOZE_1_8   = 3,
        OSC_DOZE_1_16  = 4,
        OSC_DOZE_1_32  = 5,
        OSC_DOZE_1_64  = 6,
        OSC_DOZE_1_128 = 7
    } OSC_DOZE;

#endif



/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_OSC)

        #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) || \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)      \
            )

            /* only for PIC24F GA1/GB1 family */

            void         csp_osc_ref_conf_set(OSC_REF_CONF val);
            OSC_REF_CONF csp_osc_ref_conf_get(void);
        #endif

        #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
            )

            void         csp_osc_conf_set(OSC_CONF val);
            OSC_CONF     csp_osc_conf_get(void);

        #endif

        void     csp_osc_set(OSC_MODE mode);
        OSC_MODE csp_osc_get(void);

        void     csp_osc_lock(void);
        void     csp_osc_unlock(void);

        void     csp_osc_sec_en(void);
        void     csp_osc_sec_dis(void);
        UWORD    csp_osc_sec_check(void);

        OSC_STAT csp_osc_stat_get(void);
        void     csp_osc_stat_clr(OSC_STAT mask);

        void       csp_osc_frc_set(OSC_FRC_PS val);
        OSC_FRC_PS csp_osc_frc_get(void);

        void     csp_osc_frc_cal_set(U08 val);
        U08      csp_osc_frc_cal_get(void);

        void          csp_osc_doze_conf_set(OSC_DOZE_CONF val);
        OSC_DOZE_CONF csp_osc_doze_conf_get(void);

        void     csp_osc_doze_set(OSC_DOZE val);
        OSC_DOZE csp_osc_doze_get(void);


    #else

        #include "csp_osc.c"

    #endif
#endif

#endif /* #ifndef __CSP_OSC_H */
/* ********************************************************************************************** */
/* end of file: csp_osc.h */
