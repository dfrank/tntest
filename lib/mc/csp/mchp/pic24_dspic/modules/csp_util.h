/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_util.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_UTIL_H
#define __CSP_UTIL_H

/* Definitions for Utils API */
/* ------------------------ */

typedef struct __TMR_TM
{
    UWORD period;
    UWORD ps_mask;
    U32   actual;
} TMR_TM;

typedef struct __I2C_TM
{
    UWORD val;
    U32   actual;
} I2C_TM;

typedef struct __SPI_TM
{
    SPI_BAUD_PS val;
    U32         actual;
} SPI_TM;

typedef struct __UART_TM
{
    BOOL  high;
    UWORD val;
    U32   actual;
} UART_TM;

typedef struct __ADC_TM
{
    UWORD val;
    U32   actual;
} ADC_TM;

/* Internal API definitions */
/* ------------------------ */

#define __UTIL_US_CONST         1000000UL       /* The count of usec in one second    */

/* CSP API declarations  */
/* --------------------- */

OSC_MODE   csp_osc_get(void);
OSC_FRC_PS csp_osc_frc_get(void);

/* External functions prototypes */
/* ----------------------------- */


#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_UTIL)

        S32        csp_util_fcy_get(U32 freq);
        
        CSP_RETVAL csp_util_tmr_timing(U32 fcy, U32 period, TMR_TM *tmr);
        CSP_RETVAL csp_util_i2c_timing(U32 fcy, U32 freq, I2C_TM *i2c);
        CSP_RETVAL csp_util_spi_timing(U32 fcy, U32 freq, SPI_TM *spi);
        CSP_RETVAL csp_util_uart_timing(U32 fcy, U32 baud, UART_TM *uart);
        CSP_RETVAL csp_util_adc_timing(U32 fcy, U32 freq, ADC_TM *adc);
        
        UWORD      csp_util_bin_to_bcd(UWORD val);
        UWORD      csp_util_bcd_to_bin(UWORD val);

    #else

        #include "csp_util.c"
    #endif

#endif

#endif /* #ifndef __CSP_UTIL_H */
/* ********************************************************************************************** */
/* end of file: csp_util.h */
