/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_oc_cget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )
    __CSP_FUNC UWORD  csp_oc_conf_get (OC_TYPE *oc)
    {
        return oc->OCCON;
    }

#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||    \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)       \
      )

    __CSP_FUNC U32  csp_oc_conf_get (OC_TYPE *oc)
    {
        AL_EXT_32 ret;
        ret.word_0 = oc->OCCON1;
        ret.word_1 = oc->OCCON2;
        return ret.full;
    }

#endif


/* ********************************************************************************************** */
/* end of file: csp_oc_cget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_oc_cset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    __CSP_FUNC void  csp_oc_conf_set (OC_TYPE *oc, UWORD val)
    {
        oc->OCCON = val;
    }

#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||    \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)       \
      )

    __CSP_FUNC void  csp_oc_conf_set (OC_TYPE *oc, U32 val)
    {
        oc->OCCON1 = ((AL_EXT_32)val).word_0;
        oc->OCCON2 = ((AL_EXT_32)val).word_1;
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_oc_cset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_oc_rget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_oc_r_get (OC_TYPE *oc)
{
    return (oc->OCR);
}

/* ********************************************************************************************** */
/* end of file: csp_oc_rget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_oc_rset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_oc_r_set (OC_TYPE *oc, UWORD val)
{
    oc->OCR = val;
}

/* ********************************************************************************************** */
/* end of file: csp_oc_rset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_oc_rsgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_oc_rs_get (OC_TYPE *oc)
{
    return (oc->OCRS);
}

/* ********************************************************************************************** */
/* end of file: csp_oc_rsgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_oc_rsst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_oc_rs_set (OC_TYPE *oc, UWORD val)
{
    oc->OCRS = val;
}

/* ********************************************************************************************** */
/* end of file: csp_oc_rsst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_oc_sclr.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
    )

    __CSP_FUNC void  csp_oc_stat_clr (OC_TYPE *oc, OC_STAT mask)
    {
        __CSP_ACC_MASK_VAL(BFA_CLR, oc->OCCON1, (OC_STAT_FAULT), UINT_MAX);
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_oc_sclr.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_oc_sget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    __CSP_FUNC OC_STAT  csp_oc_stat_get (OC_TYPE *oc)
    {
        return (OC_STAT)__CSP_ACC_MASK_VAL(BFA_RD, oc->OCCON, (OC_STAT_FAULT));
    }

#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
      )

    __CSP_FUNC OC_STAT  csp_oc_stat_get (OC_TYPE *oc)
    {
        return (OC_STAT)__CSP_ACC_MASK_VAL(BFA_RD, oc->OCCON1, (OC_STAT_FAULT)) |
               (OC_STAT)__CSP_ACC_MASK_VAL(BFA_RD, oc->OCCON2, (OC_STAT_RUNNING));
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_oc_sget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_oc_tmgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
    )

    __CSP_FUNC UWORD  csp_oc_tmr_get (OC_TYPE *oc)
    {
        return (oc->OCTMR);
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_oc_tmgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_oc_tmst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
    )

    __CSP_FUNC void  csp_oc_tmr_set (OC_TYPE *oc, UWORD val)
    {
        oc->OCTMR = val;
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_oc_tmst.c */
