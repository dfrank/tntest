/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_core.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_CORE_H
#define __CSP_CORE_H


#if (__CSP_CORE == __CSP_CORE_PIC24)

    /* Core programming model */
    /* ---------------------- */

    typedef struct __CORE_TYPE
    {
        volatile UWORD  WREG0   __CSP_DEPRECATED;
        volatile UWORD  WREG1   __CSP_DEPRECATED;
        volatile UWORD  WREG2   __CSP_DEPRECATED;
        volatile UWORD  WREG3   __CSP_DEPRECATED;
        volatile UWORD  WREG4   __CSP_DEPRECATED;
        volatile UWORD  WREG5   __CSP_DEPRECATED;
        volatile UWORD  WREG6   __CSP_DEPRECATED;
        volatile UWORD  WREG7   __CSP_DEPRECATED;
        volatile UWORD  WREG8   __CSP_DEPRECATED;
        volatile UWORD  WREG9   __CSP_DEPRECATED;
        volatile UWORD  WREG10  __CSP_DEPRECATED;
        volatile UWORD  WREG11  __CSP_DEPRECATED;
        volatile UWORD  WREG12  __CSP_DEPRECATED;
        volatile UWORD  WREG13  __CSP_DEPRECATED;
        volatile UWORD  WREG14  __CSP_DEPRECATED;
        volatile UWORD  WREG15  __CSP_DEPRECATED;
        volatile UWORD  SPLIM;
        __CSP_SKIP_BYTE(12);
        volatile UWORD  PCL     __CSP_DEPRECATED;
        volatile U08    PCH     __CSP_DEPRECATED;
        __CSP_SKIP_BYTE(1);
        volatile U08    TBLPAG;
        __CSP_SKIP_BYTE(1);
        volatile U08    PSVPAG;
        __CSP_SKIP_BYTE(1);
        volatile UWORD  RCOUNT;
        __CSP_SKIP_BYTE(10);
        volatile UWORD  SR;
        volatile UWORD  CORCON;
        __CSP_SKIP_BYTE(12);
        volatile UWORD  DISICNT;
    } CORE_TYPE;

    /* Core base address */
    /* ----------------- */

    #define CORE_BASE_ADDR      0x0000

    /* Declare core model */
    /* ------------------ */

    extern CORE_TYPE CORE __CSP_SFR(CORE_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    #define __CORE_PRI_FB       (5)     /* SR register 5 bit */
    #define __CORE_PRI_FE       (7)     /* SR register 8 bit*/


    /* Define core registers fields for direct user access */
    /* --------------------------------------------------- */

    #define STAT_C          (1 << 0)    /* SR<0> */
    #define STAT_Z          (1 << 1)    /* SR<1> */
    #define STAT_OV         (1 << 2)    /* SR<2> */
    #define STAT_N          (1 << 3)    /* SR<3> */
    #define STAT_RA         (1 << 4)    /* SR<4> */
    #define STAT_IPL0       (1 << 5)    /* SR<5> */
    #define STAT_IPL1       (1 << 6)    /* SR<6> */
    #define STAT_IPL2       (1 << 7)    /* SR<7> */
    #define STAT_DC         (1 << 8)    /* SR<8> */

    #define CORE_PSV        (1 << 2)    /* CORCON<2> */
    #define CORE_IPL3       (1 << 3)    /* CORCON<3> */

#endif


/* Define configuration words */
/* -------------------------- */

/*  Config word 1 Example:

__CSP_CONFIG_2(TWO_SPEED_STARTUP_EN |
               OSC_STARTUP_FRC      |
               CLK_SW_EN_CLK_MON_EN |
               OSCO_PIN_GPIO        |
               PRIMARY_OSC_DIS
              );

    Config word 2 Example

__CSP_CONFIG_2(JTAG_DIS              |
               CODE_PROTECT_EN       |
               CODE_WRITE_PROTECT_EN |
               BACKGROUND_DEBUG_DIS  |
               EMULATION_DIS         |
               ICD_PIN_PGX1          |
               WDT_EN                |
               WDT_WINDOW_DIS        |
               WDT_PRESCALE_32       |
               WDT_POSTSCALE_16
              );
*/

#if (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010)

    #define __CSP_CONFIG_2(x) __attribute__((section("__CONFIG2.sec,code"))) int __CSP_CONFIG_2 = (0xFFFF & (x))

        /* Two Speed Start-up: */

    #define TWO_SPEED_STARTUP_EN        (1 << 15)  /* Enabled  */
    #define TWO_SPEED_STARTUP_DIS       (0 << 15)  /* Disabled */

        /* Oscillator Selection: */

    #define OSC_STARTUP_FRC             (0 <<  8)  /* Fast RC oscillator                     */
    #define OSC_STARTUP_FRC_PLL         (1 <<  8)  /* Fast RC oscillator w/ divide and PLL   */
    #define OSC_STARTUP_PRIMARY         (2 <<  8)  /* Primary oscillator (XT, HS, EC)        */
    #define OSC_STARTUP_PRIMARY_PLL     (3 <<  8)  /* Primary oscillator (XT, HS, EC) w/ PLL */
    #define OSC_STARTUP_SECONDARY       (4 <<  8)  /* Secondary oscillator                   */
    #define OSC_STARTUP_LPRC            (5 <<  8)  /* Low power RC oscillator                */
    #define OSC_STARTUP_FRC_PS          (7 <<  8)  /* Fast RC oscillator with divide         */

        /* Clock switching and clock montor: */

    #define CLK_SW_EN_CLK_MON_EN        (0 <<  6)  /* Both enabled                 */
    #define CLK_SW_EN_CLK_MON_DIS       (1 <<  6)  /* Only clock switching enabled */
    #define CLK_SW_DIS_CLK_MON_DIS      (3 <<  6)  /* Both disabled                */

        /* OSCO/RC15 function: */

    #define OSCO_PIN_CLKO               (1 <<  5)  /* OSCO or Fosc/2 */
    #define OSCO_PIN_GPIO               (0 <<  5)  /* RC15           */

        /* Oscillator Selection: */

    #define PRIMARY_OSC_EC              (0 <<  0)  /* External clock   */
    #define PRIMARY_OSC_XT              (1 <<  0)  /* XT oscillator    */
    #define PRIMARY_OSC_HS              (2 <<  0)  /* HS oscillator    */
    #define PRIMARY_OSC_DIS             (3 <<  0)  /* Primary disabled */


    #define __CSP_CONFIG_1(x) __attribute__((section("__CONFIG1.sec,code"))) int __CSP_CONFIG_1 = (0xFFFF & (x))

        /* JTAG: */

    #define JTAG_EN                     (1 << 14)  /* Enabled  */
    #define JTAG_DIS                    (0 << 14)  /* Disabled */

        /* Code Protect: */

    #define CODE_PROTECT_EN             (0 << 13)  /* Enabled  */
    #define CODE_PROTECT_DIS            (1 << 13)  /* Disabled */

        /* Write Protect: */

    #define CODE_WRITE_EN               (1 << 12)  /* Enabled  */
    #define CODE_WRITE_DIS              (0 << 12)  /* Disabled */

        /* Background Debugger: */

    #define BACKGROUND_DEBUG_EN         (0 << 11)  /* Enabled  */
    #define BACKGROUND_DEBUG_DIS        (1 << 11)  /* Disabled */

        /* Clip-on Emulation mode: */

    #define EMULATION_EN                (0 << 10)  /* Enabled  */
    #define EMULATION_DIS               (1 << 10)  /* Disabled */

        /* ICD pins select: */

    #define ICD_PIN_PGX1                (0 <<  8)  /* EMUC/EMUD share PGC1/PGD1 */
    #define ICD_PIN_PGX2                (1 <<  8)  /* EMUC/EMUD share PGC2/PGD2 */

        /* Watchdog Timer: */

    #define WDT_EN                      (1 <<  7)  /* Enabled  */
    #define WDT_DIS                     (0 <<  7)  /* Disabled */

        /* Windowed WDT: */

    #define WDT_WINDOW_EN               (0 <<  6)  /* Enabled  */
    #define WDT_WINDOW_DIS              (1 <<  6)  /* Disabled */

        /* Watchdog prescaler: */

    #define WDT_PRESCALE_32             (0 <<  4)  /* 1:32  */
    #define WDT_PRESCALE_128            (1 <<  4)  /* 1:128 */

        /* Watchdog postscale: */

    #define WDT_POSTSCALE_1             ( 0 << 0)  /* 1:1     */
    #define WDT_POSTSCALE_2             ( 1 << 0)  /* 1:2     */
    #define WDT_POSTSCALE_4             ( 2 << 0)  /* 1:4     */
    #define WDT_POSTSCALE_8             ( 3 << 0)  /* 1:8     */
    #define WDT_POSTSCALE_16            ( 4 << 0)  /* 1:16    */
    #define WDT_POSTSCALE_32            ( 5 << 0)  /* 1:32    */
    #define WDT_POSTSCALE_64            ( 6 << 0)  /* 1:64    */
    #define WDT_POSTSCALE_128           ( 7 << 0)  /* 1:128   */
    #define WDT_POSTSCALE_256           ( 8 << 0)  /* 1:256   */
    #define WDT_POSTSCALE_512           ( 9 << 0)  /* 1:512   */
    #define WDT_POSTSCALE_1024          (10 << 0)  /* 1:1024  */
    #define WDT_POSTSCALE_2048          (11 << 0)  /* 1:2048  */
    #define WDT_POSTSCALE_4096          (12 << 0)  /* 1:4096  */
    #define WDT_POSTSCALE_8192          (13 << 0)  /* 1:8192  */
    #define WDT_POSTSCALE_16384         (14 << 0)  /* 1:16384 */
    #define WDT_POSTSCALE_32768         (15 << 0)  /* 1:32768 */

#elif (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)

   #define __CSP_CONFIG_2(x) __attribute__((section("__CONFIG2.sec,code"))) int __CSP_CONFIG_2 = (0xFFFF & (x))

        /* Two Speed Start-up: */

    #define TWO_SPEED_STARTUP_EN        (1 << 15)  /* Enabled  */
    #define TWO_SPEED_STARTUP_DIS       (0 << 15)  /* Disabled */

        /* Oscillator Selection: */

    #define OSC_STARTUP_FRC             (0 <<  8)  /* Fast RC oscillator                     */
    #define OSC_STARTUP_FRC_PLL         (1 <<  8)  /* Fast RC oscillator w/ divide and PLL   */
    #define OSC_STARTUP_PRIMARY         (2 <<  8)  /* Primary oscillator (XT, HS, EC)        */
    #define OSC_STARTUP_PRIMARY_PLL     (3 <<  8)  /* Primary oscillator (XT, HS, EC) w/ PLL */
    #define OSC_STARTUP_SECONDARY       (4 <<  8)  /* Secondary oscillator                   */
    #define OSC_STARTUP_LPRC            (5 <<  8)  /* Low power RC oscillator                */
    #define OSC_STARTUP_FRC_PS          (7 <<  8)  /* Fast RC oscillator with divide         */

        /* Clock switching and clock montor: */

    #define CLK_SW_EN_CLK_MON_EN        (0 <<  6)  /* Both enabled                 */
    #define CLK_SW_EN_CLK_MON_DIS       (1 <<  6)  /* Only clock switching enabled */
    #define CLK_SW_DIS_CLK_MON_DIS      (3 <<  6)  /* Both disabled                */

        /* OSCO/RA3 function: */

    #define OSCO_PIN_CLKO               (1 <<  5)  /* OSCO or Fosc/2 */
    #define OSCO_PIN_GPIO               (0 <<  5)  /* RC15           */

        /* RP Register Protection */

    #define PPS_REG_PROTECT_DIS         (0 <<  4)  /* Unlimited Writes To RP Registers */
    #define PPS_REG_PROTECT_EN          (1 <<  4)  /* Write RP Registers Once          */

        /* I2C1 pins Select */

    #define I2C1_PINS_SECONDARY         (0 <<  2)  /* Use Secondary I2C1 pins */
    #define I2C1_PINS_PRIMARY           (1 <<  2)  /* Use Primary I2C1 pins   */

        /* Oscillator Selection: */

    #define PRIMARY_OSC_EC              (0 <<  0)  /* External clock   */
    #define PRIMARY_OSC_XT              (1 <<  0)  /* XT oscillator    */
    #define PRIMARY_OSC_HS              (2 <<  0)  /* HS oscillator    */
    #define PRIMARY_OSC_DIS             (3 <<  0)  /* Primary disabled */


    #define __CSP_CONFIG_1(x) __attribute__((section("__CONFIG1.sec,code"))) int __CSP_CONFIG_1 = (0xFFFF & (x))

        /* JTAG: */

    #define JTAG_EN                     (1 << 14)  /* Enabled  */
    #define JTAG_DIS                    (0 << 14)  /* Disabled */

        /* Code Protect: */

    #define CODE_PROTECT_EN             (0 << 13)  /* Enabled  */
    #define CODE_PROTECT_DIS            (1 << 13)  /* Disabled */

        /* Write Protect: */

    #define CODE_WRITE_EN               (1 << 12)  /* Enabled  */
    #define CODE_WRITE_DIS              (0 << 12)  /* Disabled */

        /* Background Debugger: */

    #define BACKGROUND_DEBUG_EN         (0 << 11)  /* Enabled  */
    #define BACKGROUND_DEBUG_DIS        (1 << 11)  /* Disabled */

        /* Clip-on Emulation mode: */

    #define EMULATION_EN                (0 << 10)  /* Enabled  */
    #define EMULATION_DIS               (1 << 10)  /* Disabled */

        /* ICD pins select: */

    #define ICD_PIN_PGX3                (1 <<  8)  /* EMUC/EMUD share PGC3/PGD3 */
    #define ICD_PIN_PGX2                (2 <<  8)  /* EMUC/EMUD share PGC2/PGD2 */
    #define ICD_PIN_PGX1                (3 <<  8)  /* EMUC/EMUD share PGC1/PGD1 */

        /* Watchdog Timer: */

    #define WDT_EN                      (1 <<  7)  /* Enabled  */
    #define WDT_DIS                     (0 <<  7)  /* Disabled */

        /* Windowed WDT: */

    #define WDT_WINDOW_EN               (0 <<  6)  /* Enabled  */
    #define WDT_WINDOW_DIS              (1 <<  6)  /* Disabled */

        /* Watchdog prescaler: */

    #define WDT_PRESCALE_32             (0 <<  4)  /* 1:32  */
    #define WDT_PRESCALE_128            (1 <<  4)  /* 1:128 */


        /* Watchdog postscale: */

    #define WDT_POSTSCALE_1             ( 0 << 0)  /* 1:1     */
    #define WDT_POSTSCALE_2             ( 1 << 0)  /* 1:2     */
    #define WDT_POSTSCALE_4             ( 2 << 0)  /* 1:4     */
    #define WDT_POSTSCALE_8             ( 3 << 0)  /* 1:8     */
    #define WDT_POSTSCALE_16            ( 4 << 0)  /* 1:16    */
    #define WDT_POSTSCALE_32            ( 5 << 0)  /* 1:32    */
    #define WDT_POSTSCALE_64            ( 6 << 0)  /* 1:64    */
    #define WDT_POSTSCALE_128           ( 7 << 0)  /* 1:128   */
    #define WDT_POSTSCALE_256           ( 8 << 0)  /* 1:256   */
    #define WDT_POSTSCALE_512           ( 9 << 0)  /* 1:512   */
    #define WDT_POSTSCALE_1024          (10 << 0)  /* 1:1024  */
    #define WDT_POSTSCALE_2048          (11 << 0)  /* 1:2048  */
    #define WDT_POSTSCALE_4096          (12 << 0)  /* 1:4096  */
    #define WDT_POSTSCALE_8192          (13 << 0)  /* 1:8192  */
    #define WDT_POSTSCALE_16384         (14 << 0)  /* 1:16384 */
    #define WDT_POSTSCALE_32768         (15 << 0)  /* 1:32768 */

#elif (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110)

    /*

    Config word 3 Example: Protect code memory from start (protect 128 flash pages) and protect
                           configuration block of programm memoty

    __CSP_CONFIG_3(CODE_WRITE_PROT_MEM_START |
                   CODE_WRITE_CONF_MEM_EN    |
                   (128)
                  );
    */

    #define __CSP_CONFIG_3(x) __attribute__((section("__CONFIG3.sec,code"))) int __CSP_CONFIG_3 = ((x) | (15 << 9))

        /* Write protection location */

    #define CODE_WRITE_PROT_MEM_START   (0 << 15)   /* Code write protect from start of programm memory */
    #define CODE_WRITE_PROT_MEM_END     (1 << 15)   /* Code write protect from end of programm memory */

        /* Write Protect Configuration Page */

    #define CODE_WRITE_CONF_MEM_EN      (0 << 14)   /* Code configuration memory block write protect enabled  */
    #define CODE_WRITE_CONF_MEM_DIS     (1 << 14)   /* Code configuration memory block write protect disabled */

        /* Write protect enable */

    #define CODE_WRITE_BLOCK_EN         (0 << 13)   /* Code write protect enabled  */
    #define CODE_WRITE_BLOCK_DIS        (1 << 13)   /* Code write protect disabled */


    #define __CSP_CONFIG_2(x) __attribute__((section("__CONFIG2.sec,code"))) int __CSP_CONFIG_2 = ((x) | (7 << 12) | (1 << 3))

        /* Two Speed Start-up: */

    #define TWO_SPEED_STARTUP_EN        (1 << 15)   /* Enabled  */
    #define TWO_SPEED_STARTUP_DIS       (0 << 15)   /* Disabled */

        /* Oscillator Selection: */

    #define OSC_STARTUP_FRC             (0 <<  8)  /* Fast RC oscillator                     */
    #define OSC_STARTUP_FRC_PLL         (1 <<  8)  /* Fast RC oscillator w/ divide and PLL   */
    #define OSC_STARTUP_PRIMARY         (2 <<  8)  /* Primary oscillator (XT, HS, EC)        */
    #define OSC_STARTUP_PRIMARY_PLL     (3 <<  8)  /* Primary oscillator (XT, HS, EC) w/ PLL */
    #define OSC_STARTUP_SECONDARY       (4 <<  8)  /* Secondary oscillator                   */
    #define OSC_STARTUP_LPRC            (5 <<  8)  /* Low power RC oscillator                */
    #define OSC_STARTUP_FRC_PS          (7 <<  8)  /* Fast RC oscillator with divide         */

        /* Clock switching and clock montor: */

    #define CLK_SW_EN_CLK_MON_EN        (0 <<  6)  /* Both enabled                 */
    #define CLK_SW_EN_CLK_MON_DIS       (1 <<  6)  /* Only clock switching enabled */
    #define CLK_SW_DIS_CLK_MON_DIS      (3 <<  6)  /* Both disabled                */

        /* OSCO/RC15 function: */

    #define OSCO_PIN_CLKO               (1 <<  5)  /* OSCO or Fosc/2 */
    #define OSCO_PIN_GPIO               (0 <<  5)  /* RC15           */

        /* RP Register Protection */

    #define PPS_REG_PROTECT_DIS         (0 <<  4)  /* Unlimited Writes To RP Registers */
    #define PPS_REG_PROTECT_EN          (1 <<  4)  /* Write RP Registers Once          */

        /* I2C2 pins Select */

    #define I2C2_PINS_SECONDARY         (0 <<  2)  /* Use Secondary I2C2 pins */
    #define I2C2_PINS_PRIMARY           (1 <<  2)  /* Use Primary I2C2 pins   */

        /* Oscillator Selection: */

    #define PRIMARY_OSC_EC              (0 <<  0)  /* External clock   */
    #define PRIMARY_OSC_XT              (1 <<  0)  /* XT oscillator    */
    #define PRIMARY_OSC_HS              (2 <<  0)  /* HS oscillator    */
    #define PRIMARY_OSC_DIS             (3 <<  0)  /* Primary disabled */


    #define __CSP_CONFIG_1(x) __attribute__((section("__CONFIG1.sec,code"))) int __CSP_CONFIG_1 = ((x) | (1 << 5))

        /* JTAG: */

    #define JTAG_EN                     (1 << 14)  /* Enabled  */
    #define JTAG_DIS                    (0 << 14)  /* Disabled */

        /* Code Protect: */

    #define CODE_PROTECT_EN             (0 << 13)  /* Enabled  */
    #define CODE_PROTECT_DIS            (1 << 13)  /* Disabled */

        /* Write Protect: */

    #define CODE_WRITE_EN               (1 << 12)  /* Enabled  */
    #define CODE_WRITE_DIS              (0 << 12)  /* Disabled */

        /* Background Debugger: */

    #define BACKGROUND_DEBUG_EN         (0 << 11)  /* Enabled  */
    #define BACKGROUND_DEBUG_DIS        (1 << 11)  /* Disabled */

        /* Clip-on Emulation mode: */

    #define EMULATION_EN                (0 << 10)  /* Enabled  */
    #define EMULATION_DIS               (1 << 10)  /* Disabled */

        /* ICD pins select: */

    #define ICD_PIN_PGX3                (1 <<  8)  /* EMUC/EMUD share PGC3/PGD3 */
    #define ICD_PIN_PGX2                (2 <<  8)  /* EMUC/EMUD share PGC2/PGD2 */
    #define ICD_PIN_PGX1                (3 <<  8)  /* EMUC/EMUD share PGC1/PGD1 */

        /* Watchdog Timer: */

    #define WDT_EN                      (1 <<  7)  /* Enabled  */
    #define WDT_DIS                     (0 <<  7)  /* Disabled */

        /* Windowed WDT: */

    #define WDT_WINDOW_EN               (0 <<  6)  /* Enabled  */
    #define WDT_WINDOW_DIS              (1 <<  6)  /* Disabled */

        /* Watchdog prescaler: */

    #define WDT_PRESCALE_32             (0 <<  4)  /* 1:32  */
    #define WDT_PRESCALE_128            (1 <<  4)  /* 1:128 */


        /* Watchdog postscale: */

    #define WDT_POSTSCALE_1             ( 0 << 0)  /* 1:1     */
    #define WDT_POSTSCALE_2             ( 1 << 0)  /* 1:2     */
    #define WDT_POSTSCALE_4             ( 2 << 0)  /* 1:4     */
    #define WDT_POSTSCALE_8             ( 3 << 0)  /* 1:8     */
    #define WDT_POSTSCALE_16            ( 4 << 0)  /* 1:16    */
    #define WDT_POSTSCALE_32            ( 5 << 0)  /* 1:32    */
    #define WDT_POSTSCALE_64            ( 6 << 0)  /* 1:64    */
    #define WDT_POSTSCALE_128           ( 7 << 0)  /* 1:128   */
    #define WDT_POSTSCALE_256           ( 8 << 0)  /* 1:256   */
    #define WDT_POSTSCALE_512           ( 9 << 0)  /* 1:512   */
    #define WDT_POSTSCALE_1024          (10 << 0)  /* 1:1024  */
    #define WDT_POSTSCALE_2048          (11 << 0)  /* 1:2048  */
    #define WDT_POSTSCALE_4096          (12 << 0)  /* 1:4096  */
    #define WDT_POSTSCALE_8192          (13 << 0)  /* 1:8192  */
    #define WDT_POSTSCALE_16384         (14 << 0)  /* 1:16384 */
    #define WDT_POSTSCALE_32768         (15 << 0)  /* 1:32768 */


#elif (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)

    /*

    Config word 3 Example: Protect code memory from start (protect 128 flash pages) and protect
                           configuration block of programm memoty

    __CSP_CONFIG_3(CODE_WRITE_PROT_MEM_START |
                   CODE_WRITE_CONF_MEM_EN    |
                   (128)
                  );
    */

    #define __CSP_CONFIG_3(x) __attribute__((section("__CONFIG3.sec,code"))) int __CSP_CONFIG_3 = ((x) | 0x1E00)

        /* Write protection location */

    #define CODE_WRITE_PROT_MEM_START   (0 << 15)   /* Code write protect from start of programm memory */
    #define CODE_WRITE_PROT_MEM_END     (1 << 15)   /* Code write protect from end of programm memory */

        /* Write Protect Configuration Page */

    #define CODE_WRITE_CONF_MEM_EN      (1 << 14)   /* Code configuration memory block write protect enabled  */
    #define CODE_WRITE_CONF_MEM_DIS     (0 << 14)   /* Code configuration memory block write protect disabled */

        /* Write protect enable */

    #define CODE_WRITE_BLOCK_EN         (0 << 13)   /* Code write protect enabled  */
    #define CODE_WRITE_BLOCK_DIS        (1 << 13)   /* Code write protect disabled */


    #define __CSP_CONFIG_2(x) __attribute__((section("__CONFIG2.sec,code"))) int __CSP_CONFIG_2 = ((x) | 0x0004)

        /* Two Speed Start-up: */

    #define TWO_SPEED_STARTUP_EN        (1 << 15)   /* Enabled  */
    #define TWO_SPEED_STARTUP_DIS       (0 << 15)   /* Disabled */

        /* USB 96 MHz PLL Prescaler Select bits */

    #define USB_PLL_PRESCALE_1          (0 << 12)   /* 1:1  */
    #define USB_PLL_PRESCALE_2          (1 << 12)   /* 1:2  */
    #define USB_PLL_PRESCALE_3          (2 << 12)   /* 1:3  */
    #define USB_PLL_PRESCALE_4          (3 << 12)   /* 1:4  */
    #define USB_PLL_PRESCALE_5          (4 << 12)   /* 1:5  */
    #define USB_PLL_PRESCALE_6          (5 << 12)   /* 1:6  */
    #define USB_PLL_PRESCALE_10         (6 << 12)   /* 1:10 */
    #define USB_PLL_PRESCALE_12         (7 << 12)   /* 1:12 */

        /* Oscillator Selection: */

    #define OSC_STARTUP_FRC             (0 <<  8)  /* Fast RC oscillator                     */
    #define OSC_STARTUP_FRC_PLL         (1 <<  8)  /* Fast RC oscillator w/ divide and PLL   */
    #define OSC_STARTUP_PRIMARY         (2 <<  8)  /* Primary oscillator (XT, HS, EC)        */
    #define OSC_STARTUP_PRIMARY_PLL     (3 <<  8)  /* Primary oscillator (XT, HS, EC) w/ PLL */
    #define OSC_STARTUP_SECONDARY       (4 <<  8)  /* Secondary oscillator                   */
    #define OSC_STARTUP_LPRC            (5 <<  8)  /* Low power RC oscillator                */
    #define OSC_STARTUP_FRC_PS          (7 <<  8)  /* Fast RC oscillator with divide         */

        /* Clock switching and clock montor: */

    #define CLK_SW_EN_CLK_MON_EN        (0 <<  6)  /* Both enabled                 */
    #define CLK_SW_EN_CLK_MON_DIS       (1 <<  6)  /* Only clock switching enabled */
    #define CLK_SW_DIS_CLK_MON_DIS      (3 <<  6)  /* Both disabled                */

        /* OSCO/RC15 function: */

    #define OSCO_PIN_CLKO               (1 <<  5)  /* OSCO or Fosc/2 */
    #define OSCO_PIN_GPIO               (0 <<  5)  /* RC15           */

        /* RP Register Protection */

    #define PPS_REG_PROTECT_DIS         (0 <<  4)  /* Unlimited Writes To RP Registers */
    #define PPS_REG_PROTECT_EN          (1 <<  4)  /* Write RP Registers Once          */

        /* USB regulator control */

    #define USB_REG_EN                  (0 <<  3)
    #define USB_REG_DIS                 (1 <<  3)

        /* Oscillator Selection: */

    #define PRIMARY_OSC_EC              (0 <<  0)  /* External clock   */
    #define PRIMARY_OSC_XT              (1 <<  0)  /* XT oscillator    */
    #define PRIMARY_OSC_HS              (2 <<  0)  /* HS oscillator    */
    #define PRIMARY_OSC_DIS             (3 <<  0)  /* Primary disabled */



    #define __CSP_CONFIG_1(x) __attribute__((section("__CONFIG1.sec,code"))) int __CSP_CONFIG_1 = ((x) | 0x8420)

        /* JTAG: */

    #define JTAG_EN                     (1 << 14)  /* Enabled  */
    #define JTAG_DIS                    (0 << 14)  /* Disabled */

        /* Code Protect: */

    #define CODE_PROTECT_EN             (0 << 13)  /* Enabled  */
    #define CODE_PROTECT_DIS            (1 << 13)  /* Disabled */

        /* Write Protect: */

    #define CODE_WRITE_EN               (1 << 12)  /* Enabled  */
    #define CODE_WRITE_DIS              (0 << 12)  /* Disabled */

        /* Background Debugger: */

    #define BACKGROUND_DEBUG_EN         (0 << 11)  /* Enabled  */
    #define BACKGROUND_DEBUG_DIS        (1 << 11)  /* Disabled */

        /* Clip-on Emulation mode: */

    #define EMULATION_EN                (0 << 10)  /* Enabled  */
    #define EMULATION_DIS               (1 << 10)  /* Disabled */

        /* ICD pins select: */

    #define ICD_PIN_PGX3                (1 <<  8)  /* EMUC/EMUD share PGC3/PGD3 */
    #define ICD_PIN_PGX2                (2 <<  8)  /* EMUC/EMUD share PGC2/PGD2 */
    #define ICD_PIN_PGX1                (3 <<  8)  /* EMUC/EMUD share PGC1/PGD1 */

        /* Watchdog Timer: */

    #define WDT_EN                      (1 <<  7)  /* Enabled  */
    #define WDT_DIS                     (0 <<  7)  /* Disabled */

        /* Windowed WDT: */

    #define WDT_WINDOW_EN               (0 <<  6)  /* Enabled  */
    #define WDT_WINDOW_DIS              (1 <<  6)  /* Disabled */

        /* Watchdog prescaler: */

    #define WDT_PRESCALE_32             (0 <<  4)  /* 1:32  */
    #define WDT_PRESCALE_128            (1 <<  4)  /* 1:128 */


        /* Watchdog postscale: */

    #define WDT_POSTSCALE_1             ( 0 << 0)  /* 1:1     */
    #define WDT_POSTSCALE_2             ( 1 << 0)  /* 1:2     */
    #define WDT_POSTSCALE_4             ( 2 << 0)  /* 1:4     */
    #define WDT_POSTSCALE_8             ( 3 << 0)  /* 1:8     */
    #define WDT_POSTSCALE_16            ( 4 << 0)  /* 1:16    */
    #define WDT_POSTSCALE_32            ( 5 << 0)  /* 1:32    */
    #define WDT_POSTSCALE_64            ( 6 << 0)  /* 1:64    */
    #define WDT_POSTSCALE_128           ( 7 << 0)  /* 1:128   */
    #define WDT_POSTSCALE_256           ( 8 << 0)  /* 1:256   */
    #define WDT_POSTSCALE_512           ( 9 << 0)  /* 1:512   */
    #define WDT_POSTSCALE_1024          (10 << 0)  /* 1:1024  */
    #define WDT_POSTSCALE_2048          (11 << 0)  /* 1:2048  */
    #define WDT_POSTSCALE_4096          (12 << 0)  /* 1:4096  */
    #define WDT_POSTSCALE_8192          (13 << 0)  /* 1:8192  */
    #define WDT_POSTSCALE_16384         (14 << 0)  /* 1:16384 */
    #define WDT_POSTSCALE_32768         (15 << 0)  /* 1:32768 */


#elif (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

    extern __attribute__((space(prog))) int __CSP_CONFIG_FBS;
    #define __CSP_CONFIG_FBS(x) __attribute__((section("__FBS.sec"),space(prog))) int __CSP_CONFIG_FBS = ((x) | 0xFFF0)

        /* Boot segment write protect */
    
    #define BOOT_SEG_WRITE_PROT_EN      (0 << 0)    /* Boot segment is write-protected  */
    #define BOOT_SEG_WRITE_PROT_DIS     (1 << 0)    /* Boot segment may be written      */

        /* Boot segment security and size */

    #define BOOT_SEG_SEC_HIG_3K         (1 << 1)    /* High security, boot program Flash segment starts at 200h, ends at 0015FEh (only for PIC24F16KA)      */
    #define BOOT_SEG_SEC_HIG_1K         (2 << 1)    /* High security boot program Flash segment starts at 200h, ends at 000AFEh                             */
    #define BOOT_SEG_SEC_STD_3K         (5 << 1)    /* Standard security, boot program Flash segment starts at 200h, ends at 0015FEh (only for PIC24F16KA)  */
    #define BOOT_SEG_SEC_STD_1K         (6 << 1)    /* Standard security, boot program Flash segment starts at 200h, ends at 000AFEh                        */

    #define BOOT_SEG_DIS                (7 << 1)    /* No boot program Flash segment                                                                        */

    extern __attribute__((space(prog))) int __CSP_CONFIG_FGS;
    #define __CSP_CONFIG_FGS(x) __attribute__((section("__FGS.sec"),space(prog))) int __CSP_CONFIG_FGS = ((x) | 0xFFFC)

        /* General segment configuration */

    #define GEN_SEG_WRITE_PROT_EN       (0 << 0)    /* General segment is write-protected   */
    #define GEN_SEG_WRITE_PROT_DIS      (1 << 0)    /* General segment may be written       */

    #define GEN_SEG_SEC_STD             (0 << 1)    /* Standard security enabled            */
    #define GEN_SEG_SEC_DIS             (1 << 1)    /* No protection                        */

    extern __attribute__((space(prog))) int __CSP_CONFIG_FOSCSEL;
    #define __CSP_CONFIG_FOSCSEL(x) __attribute__((section("__FOSCSEL.sec"),space(prog))) int __CSP_CONFIG_FOSCSEL = ((x) | 0xFF78)

        /* Oscillator Selection: */

    #define OSC_STARTUP_FRC             (0 <<  0)  /* Fast RC oscillator                     */
    #define OSC_STARTUP_FRC_PLL         (1 <<  0)  /* Fast RC oscillator w/ divide and PLL   */
    #define OSC_STARTUP_PRIMARY         (2 <<  0)  /* Primary oscillator (XT, HS, EC)        */
    #define OSC_STARTUP_PRIMARY_PLL     (3 <<  0)  /* Primary oscillator (XT, HS, EC) w/ PLL */
    #define OSC_STARTUP_SECONDARY       (4 <<  0)  /* Secondary oscillator                   */
    #define OSC_STARTUP_LPRC            (5 <<  0)  /* Low power RC oscillator                */
    #define OSC_STARTUP_LPFRC_PS        (6 <<  0)  /* 500 kHz Low-Power FRC oscillator with divide-by-N */
    #define OSC_STARTUP_FRC_PS          (7 <<  0)  /* Fast RC oscillator with divide         */

        /*  Internal External Switchover (Two-Speed Start-up) */

    #define TWO_SPEED_STARTUP_EN        (1 <<  7)   /* Enabled  */
    #define TWO_SPEED_STARTUP_DIS       (0 <<  7)   /* Disabled */

    extern __attribute__((space(prog))) int __CSP_CONFIG_FOSC;
    #define __CSP_CONFIG_FOSC(x) __attribute__((section("__FOSC.sec"),space(prog))) int __CSP_CONFIG_FOSC = ((x) | 0xFF00)    

        /* Oscillator Selection: */

    #define PRIMARY_OSC_EC              (0 <<  0)  /* External clock   */
    #define PRIMARY_OSC_XT              (1 <<  0)  /* XT oscillator    */
    #define PRIMARY_OSC_HS              (2 <<  0)  /* HS oscillator    */
    #define PRIMARY_OSC_DIS             (3 <<  0)  /* Primary disabled */    

        /* CLKO Enable Configuration */

    #define OSCO_PIN_EN                 (1 <<  2)  /* CLKO output signal active on the OSCO pin */
    #define OSCO_PIN_DIS                (0 <<  2)  /* CLKO output disabled                      */

        /* Primary Oscillator Frequency Range */

    #define PRIMARY_OSC_FREQ_LOW        (1 <<  3)   /* Primary oscillator/external clock input frequency less than 100 kHz          */
    #define PRIMARY_OSC_FREQ_MIDDLE     (2 <<  3)   /* Primary oscillator/external clock input frequency between 100 kHz and 8 MHz  */
    #define PRIMARY_OSC_FREQ_HIGH       (3 <<  3)   /* Primary oscillator/external clock input frequency greater than 8 MHz*/

        /* Secondary Oscillator Configure */

    #define SEC_OSC_POWER_LOW           (0 << 5)    /* Secondary oscillator configured for low-power operation  */
    #define SEC_OSC_POWER_HIGH          (1 << 5)    /* Secondary oscillator configured for high-power operation */

        /* Clock switching and clock montor: */

    #define CLK_SW_EN_CLK_MON_EN        (0 <<  6)  /* Both enabled                 */
    #define CLK_SW_EN_CLK_MON_DIS       (1 <<  6)  /* Only clock switching enabled */
    #define CLK_SW_DIS_CLK_MON_DIS      (3 <<  6)  /* Both disabled                */
    
    extern __attribute__((space(prog))) int __CSP_CONFIG_FWDT;
    #define __CSP_CONFIG_FWDT(x) __attribute__((section("__FWDT.sec"),space(prog))) int __CSP_CONFIG_FWDT = ((x) | 0xFF20)    

        /* Watchdog postscale: */

    #define WDT_POSTSCALE_1             ( 0 << 0)  /* 1:1     */
    #define WDT_POSTSCALE_2             ( 1 << 0)  /* 1:2     */
    #define WDT_POSTSCALE_4             ( 2 << 0)  /* 1:4     */
    #define WDT_POSTSCALE_8             ( 3 << 0)  /* 1:8     */
    #define WDT_POSTSCALE_16            ( 4 << 0)  /* 1:16    */
    #define WDT_POSTSCALE_32            ( 5 << 0)  /* 1:32    */
    #define WDT_POSTSCALE_64            ( 6 << 0)  /* 1:64    */
    #define WDT_POSTSCALE_128           ( 7 << 0)  /* 1:128   */
    #define WDT_POSTSCALE_256           ( 8 << 0)  /* 1:256   */
    #define WDT_POSTSCALE_512           ( 9 << 0)  /* 1:512   */
    #define WDT_POSTSCALE_1024          (10 << 0)  /* 1:1024  */
    #define WDT_POSTSCALE_2048          (11 << 0)  /* 1:2048  */
    #define WDT_POSTSCALE_4096          (12 << 0)  /* 1:4096  */
    #define WDT_POSTSCALE_8192          (13 << 0)  /* 1:8192  */
    #define WDT_POSTSCALE_16384         (14 << 0)  /* 1:16384 */
    #define WDT_POSTSCALE_32768         (15 << 0)  /* 1:32768 */    

        /* Watchdog prescaler: */

    #define WDT_PRESCALE_32             (0 <<  4)  /* 1:32  */
    #define WDT_PRESCALE_128            (1 <<  4)  /* 1:128 */

        /* Windowed WDT: */

    #define WDT_WINDOW_EN               (0 <<  6)  /* Enabled  */
    #define WDT_WINDOW_DIS              (1 <<  6)  /* Disabled */

        /* Watchdog Timer: */

    #define WDT_EN                      (1 <<  7)  /* Enabled  */
    #define WDT_DIS                     (0 <<  7)  /* Disabled */

    extern __attribute__((space(prog))) int __CSP_CONFIG_FPOR;
    #define __CSP_CONFIG_FPOR(x) __attribute__((section("__FPOR.sec"),space(prog))) int __CSP_CONFIG_FPOR = ((x) | 0xFF04)    

        /* Brown-out Reset  */

    #define BOR_HW                      (3 << 0)    /* Brown-out Reset enabled in hardware; SBOREN bit disabled     */
    #define BOR_EN_IN_ACTIVE            (2 << 0)    /* Brown-out Reset enabled only while device is active and disabled in Sleep; SBOREN bit disabled   */
    #define BOR_SW                      (1 << 0)    /* Brown-out Reset controlled with the SBOREN bit setting       */
    #define BOR_DIS                     (0 << 0)    /* Brown-out Reset disabled in hardware; SBOREN bit disabled    */
        
        /* Power-up Timer */

    #define POR_DIS                     (0 << 3)    /* Power-up Timer disabled  */
    #define POR_EN                      (1 << 3)    /* Power-up Timer enabled   */

        /* Alternate I2C1 Pin Mapping */
    #if (__CSP_PIN_COUNT == __CSP_PIN_COUNT_28)
    #define I2C1_PINS_PRIMARY           (1 <<  4)  /* Default location for SCL1/SDA1 pins   */
    #define I2C1_PINS_SECONDARY         (0 <<  4)  /* Alternate location for SCL1/SDA1 pins */
    #endif

        /* Brown-out Reset value */

    #define BOR_VOLTAGE_LOWEST          (3 << 5)    /* Brown-out Reset set to lowest voltage        */
    #define BOR_VOLTAGE_NORMAL          (2 << 5)    /* Brown-out Reset                              */
    #define BOR_VOLTAGE_HIGHEST         (1 << 5)    /* Brown-out Reset set to highest voltage       */
    #define BOR_VOLTAGE_LP              (0 << 5)    /* Low-power Brown-out Reset occurs around 2.0V */

        /* MCLR PIN */

    #define MCLR_EN                     (1 << 7) /* MCLR pin enabled; RA5 input pin disabled    */
    #define MCLR_DIS                    (0 << 7) /* RA5 input pin enabled; MCLR disabled        */

    extern __attribute__((space(prog))) int __CSP_CONFIG_FICD;
    #define __CSP_CONFIG_FICD(x) __attribute__((section("__FICD.sec"),space(prog))) int __CSP_CONFIG_FICD = ((x) | 0xFF7C)

        /* ICD pins select: */

    #define ICD_PIN_PGX3                (1 <<  0)  /* EMUC/EMUD share PGC3/PGD3 */
    #define ICD_PIN_PGX2                (2 <<  0)  /* EMUC/EMUD share PGC2/PGD2 */
    #define ICD_PIN_PGX1                (3 <<  0)  /* EMUC/EMUD share PGC1/PGD1 */    

        /* DEBUG Mode */

    #define BACKGROUND_DEBUG_EN         (0 << 7)  /* Enabled  */
    #define BACKGROUND_DEBUG_DIS        (1 << 7)  /* Disabled */

    extern __attribute__((space(prog))) int __CSP_CONFIG_FDS;
    #define __CSP_CONFIG_FDS(x) __attribute__((section("__FDS.sec"),space(prog))) int __CSP_CONFIG_FDS = ((x) | 0xFF00)

        /* Deep Sleep Watchdog Timer Postscale */

    #define DSWDT_POSTSCALE_2_31        (15 << 0)   /* (25.7 days) nominal  */
    #define DSWDT_POSTSCALE_2_29        (14 << 0)   /* (6.4 days) nominal   */
    #define DSWDT_POSTSCALE_2_27        (13 << 0)   /* (38.5 hours) nominal*/
    #define DSWDT_POSTSCALE_2_25        (12 << 0)   /* (9.6 hours) nominal*/
    #define DSWDT_POSTSCALE_2_23        (11 << 0)   /* (2.4 hours) nominal*/
    #define DSWDT_POSTSCALE_2_21        (10 << 0)   /* (36 minutes) nominal*/
    #define DSWDT_POSTSCALE_2_19        ( 9 << 0)   /* (9 minutes) nominal*/
    #define DSWDT_POSTSCALE_2_17        ( 8 << 0)   /* (135 seconds) nominal*/
    #define DSWDT_POSTSCALE_2_15        ( 7 << 0)   /* (34 seconds) nominal*/
    #define DSWDT_POSTSCALE_2_13        ( 6 << 0)   /* (8.5 seconds) nominal*/
    #define DSWDT_POSTSCALE_2_11        ( 5 << 0)   /* (2.1 seconds) nominal*/
    #define DSWDT_POSTSCALE_2_9         ( 4 << 0)   /* (528 ms) nominal*/
    #define DSWDT_POSTSCALE_2_7         ( 3 << 0)   /* (132 ms) nominal*/
    #define DSWDT_POSTSCALE_2_5         ( 2 << 0)   /* (33 ms) nominal*/
    #define DSWDT_POSTSCALE_2_3         ( 1 << 0)   /* (8.3 ms) nominal*/
    #define DSWDT_POSTSCALE_2_1         ( 0 << 0)   /* (2.1 ms) nominal*/

    /* DSWDT Reference Clock */

    #define DSWDT_CLOCK_LPRC            (1 << 4)    /* DSWDT uses LPRC as reference clock   */
    #define DSWDT_CLOCK_SOSC            (0 << 4)    /* DSWDT uses SOSC as reference clock   */

    /* RTCC Reference Clock */

    #define RTCC_CLOCK_SOSC             (1 << 5)    /* RTCC uses SOSC as reference clock    */
    #define RTCC_CLOCK_LPRC             (0 << 5)    /* RTCC uses LPRC as reference clock    */

    /* Deep Sleep/Low-Power BOR */

    #define DSBOR_EN                    (1 << 6)    /* Deep Sleep BOR enabled in Deep Sleep     */
    #define DSBOR_DIS                   (0 << 6)    /* Deep Sleep BOR disabled in Deep Sleep    */

    /*  Deep Sleep Watchdog Timer Enable */

    #define DSWDT_EN                    (1 << 7)    /* DSWDT enabled    */
    #define DSWDT_DIS                   (0 << 7)    /* DSWDT disabled   */

#endif

/* External functions prototypes */
/* ----------------------------- */

    /* check for optimization definition */

#if !defined(__CSP_BUILD_LIB)

        /* module api definition */

    #if defined(__CSP_DEBUG_CORE)

        INT_PRI csp_core_priority_set(INT_PRI val);
        INT_PRI csp_core_priority_get(void);

        UWORD   csp_core_disi_get(void);



    #else

        #include "csp_core.c"

    #endif

    #define csp_core_disi_set(a)            {asm volatile ("disi #%0"::"i"(a));}

#endif

#endif /* #ifndef __CSP_CORE_H */
/* ********************************************************************************************** */
/* end of file: csp_core.h */
