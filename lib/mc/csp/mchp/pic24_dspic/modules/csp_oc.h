/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_oc.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_OC_H
#define __CSP_OC_H

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    /* Output compare programming model */
    /* -------------------------------- */

    typedef struct __OC_TYPE
    {
        volatile UWORD OCRS;
        volatile UWORD OCR;
        volatile UWORD OCCON;
    } OC_TYPE;

    /* Output compare base addresses */
    /* ----------------------------- */

    #define OC1_BASE_ADDR       0x0180
    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
        )
    #define OC2_BASE_ADDR       0x0186
    #define OC3_BASE_ADDR       0x018C
    #define OC4_BASE_ADDR       0x0192
    #define OC5_BASE_ADDR       0x0198
    #endif

    /* Declare Output Compare models */
    /* ----------------------------- */

    extern OC_TYPE OC1 __CSP_SFR(OC1_BASE_ADDR);
    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
        )
    extern OC_TYPE OC2 __CSP_SFR(OC2_BASE_ADDR);
    extern OC_TYPE OC3 __CSP_SFR(OC3_BASE_ADDR);
    extern OC_TYPE OC4 __CSP_SFR(OC4_BASE_ADDR);
    extern OC_TYPE OC5 __CSP_SFR(OC5_BASE_ADDR);
    #endif

    /* Internal API definitions */
    /* ------------------------ */

    /* Definitions for Output compare API */
    /* ---------------------------------- */

    /* csp_oc_conf_set() and csp_oc_conf_get() parameters mask */

    #define OC_IDLE_STOP            (1 << 13)   /* Output compare will halt in CPU Idle mode */
    #define OC_IDLE_CON             (0)         /* Output compare will continue to operate in CPU Idle mode */

    #define OC_BASE_TMR3            (1 <<  3)   /* Timer3 is the clock source for Output Compare */
    #define OC_BASE_TMR2            (0)         /* Timer2 is the clock source for Output Compare */

    #define OC_MODE_DIS             (0 <<  0)   /* Output compare channel is disabled */
    #define OC_MODE_SINGLE_LOW_HIGH (1 <<  0)   /* Initialize OCx pin low, compare event forces OCx pin high */
    #define OC_MODE_SINGLE_HIGH_LOW (2 <<  0)   /* Initialize OCx pin high, compare event forces OCx pin low */
    #define OC_MODE_SINGLE_TOGGLE   (3 <<  0)   /* Compare event toggles OCx pin */
    #define OC_MODE_DUAL_SING_PULSE (4 <<  0)   /* Initialize OCx pin low, generate single output pulse on OCx pin */
    #define OC_MODE_DUAL_CONT_PULSE (5 <<  0)   /* Initialize OCx pin low, generate continuous output pulses on OCx pin */
    #define OC_MODE_PWM_FAULT_DIS   (6 <<  0)   /* PWM mode on OCx, Fault pin disabled */
    #define OC_MODE_PWM_FAULT_EN    (7 <<  0)   /* PWM mode on OCx, Fault pin enabled */

    /* csp_oc_stat_get() return type */

    typedef enum __OC_STAT
    {
        OC_STAT_FAULT = (1 <<  4)   /* PWM Fault condition has occurred - RO */
    } OC_STAT;

#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||    \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)       \
      )

    /* Output compare programming model */
    /* -------------------------------- */

    typedef struct __OC_TYPE
    {
        volatile UWORD OCCON1;
        volatile UWORD OCCON2;
        volatile UWORD OCRS;
        volatile UWORD OCR;
        volatile UWORD OCTMR;
    } OC_TYPE;

    /* Output compare base addresses */
    /* ----------------------------- */

    #define OC1_BASE_ADDR       0x0190
    #define OC2_BASE_ADDR       0x019A
    #define OC3_BASE_ADDR       0x01A4
    #define OC4_BASE_ADDR       0x01AE
    #define OC5_BASE_ADDR       0x01B8
    #define OC6_BASE_ADDR       0x01C2
    #define OC7_BASE_ADDR       0x01CC
    #define OC8_BASE_ADDR       0x01D6
    #define OC9_BASE_ADDR       0x01E0

    /* Declare Output Compare models */
    /* ----------------------------- */

    extern OC_TYPE OC1 __CSP_SFR(OC1_BASE_ADDR);
    extern OC_TYPE OC2 __CSP_SFR(OC2_BASE_ADDR);
    extern OC_TYPE OC3 __CSP_SFR(OC3_BASE_ADDR);
    extern OC_TYPE OC4 __CSP_SFR(OC4_BASE_ADDR);
    extern OC_TYPE OC5 __CSP_SFR(OC5_BASE_ADDR);
    extern OC_TYPE OC6 __CSP_SFR(OC6_BASE_ADDR);
    extern OC_TYPE OC7 __CSP_SFR(OC7_BASE_ADDR);
    extern OC_TYPE OC8 __CSP_SFR(OC8_BASE_ADDR);
    extern OC_TYPE OC9 __CSP_SFR(OC9_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    #define OC_ADDR_BASE_DIFF       (OC2_BASE_ADDR - OC1_BASE_ADDR)

    /* Definitions for Output compare API */
    /* ---------------------------------- */

    /* csp_oc_conf_set() and csp_oc_conf_get() parameters mask */


    #define OC_FAULT_MODE_0         (1UL <<  31)    /* Fault mode is maintained until the Fault source is removed and the corresponding OCFLT0 bit is cleared in software */
    #define OC_FAULT_MODE_1         (0UL <<  31)    /* Fault mode is maintained until the Fault source is removed and a new PWM period starts  */

    #define OC_FAULT_PWM_HIGH       (1UL <<  30)
    #define OC_FAULT_PWM_LOW        (0UL <<  30)

    #define OC_FAULT_OC_TRIS        (1UL <<  29)
    #define OC_FAULT_OC_FLTOUT      (0UL <<  29)

    #define OC_INV_EN               (1UL <<  28)
    #define OC_INV_DIS              (0UL <<  28)

    #define OC_RES_16               (0UL <<  24)
    #define OC_RES_32               (1UL <<  24)

    #define OC_CON_MODE_TRIGGER     (1UL <<  23)
    #define OC_CON_MODE_SYNC        (0UL <<  23)

    #define OC_OUT_EN               (0UL  << 21)
    #define OC_OUT_TRIS             (1UL  << 21)

    #define OC_CON_NONE             (0UL  << 16)
    #define OC_CON_OC1              (1UL  << 16)
    #define OC_CON_OC2              (2UL  << 16)
    #define OC_CON_OC3              (3UL  << 16)
    #define OC_CON_OC4              (4UL  << 16)
    #define OC_CON_OC5              (5UL  << 16)
    #define OC_CON_OC6              (6UL  << 16)
    #define OC_CON_OC7              (7UL  << 16)
    #define OC_CON_OC8              (8UL  << 16)
    #define OC_CON_OC9              (9UL  << 16)
    #define OC_CON_IC5              (10UL << 16)
    #define OC_CON_TMR1             (11UL << 16)
    #define OC_CON_TMR2             (12UL << 16)
    #define OC_CON_TMR3             (13UL << 16)
    #define OC_CON_TMR4             (14UL << 16)
    #define OC_CON_TMR5             (15UL << 16)
    #define OC_CON_IC7              (18UL << 16)
    #define OC_CON_IC8              (19UL << 16)
    #define OC_CON_IC1              (20UL << 16)
    #define OC_CON_IC2              (21UL << 16)
    #define OC_CON_IC3              (22UL << 16)
    #define OC_CON_IC4              (23UL << 16)
    #define OC_CON_COMP1            (24UL << 16)
    #define OC_CON_COMP2            (25UL << 16)
    #define OC_CON_COMP3            (26UL << 16)
    #define OC_CON_AD               (27UL << 16)
    #define OC_CON_CTMU             (28UL << 16)
    #define OC_CON_IC6              (29UL << 16)
    #define OC_CON_IC9              (30UL << 16)
    #define OC_CON_SELF             (31UL << 16)


    #define OC_IDLE_STOP            (1 << 13)   /* Output compare will halt in CPU Idle mode */
    #define OC_IDLE_CON             (0)         /* Output compare will continue to operate in CPU Idle mode */

    #define OC_BASE_TCY             (7 << 10)
    #define OC_BASE_TMR1            (4 << 10)
    #define OC_BASE_TMR2            (3 << 10)
    #define OC_BASE_TMR3            (2 << 10)
    #define OC_BASE_TMR4            (1 << 10)
    #define OC_BASE_TMR5            (0 << 10)

    #define OC_FAULT_0_EN           (1 <<  7)
    #define OC_FAULT_0_DIS          (0 <<  7)

    #define OC_TRIG_MODE_HW         (1 <<  3)
    #define OC_TRIG_MODE_SW         (0 <<  3)

    #define OC_MODE_DIS             (0 <<  0)   /* Output compare channel is disabled */
    #define OC_MODE_SINGLE_LOW_HIGH (1 <<  0)   /* Initialize OCx pin low, compare event forces OCx pin high */
    #define OC_MODE_SINGLE_HIGH_LOW (2 <<  0)   /* Initialize OCx pin high, compare event forces OCx pin low */
    #define OC_MODE_SINGLE_TOGGLE   (3 <<  0)   /* Compare event toggles OCx pin */
    #define OC_MODE_DUAL_SING_PULSE (4 <<  0)   /* Initialize OCx pin low, generate single output pulse on OCx pin */
    #define OC_MODE_DUAL_CONT_PULSE (5 <<  0)   /* Initialize OCx pin low, generate continuous output pulses on OCx pin */
    #define OC_MODE_PWM_D_EDGE      (6 <<  0)   /* PWM mode: OCFA/B enabled, output set high when OCxTMR = OCxR and set low when OCxTMR = OCxRS */
    #define OC_MODE_PWM_S_EDGE      (7 <<  0)   /* PWM mode: OCFA/B enabled, output set high when OCxTMR = 0 and set low when OCxTMR = OCxR     */

    /* csp_oc_stat_get() return type */

    typedef enum __OC_STAT
    {
        OC_STAT_FAULT   = (1 <<  4),  /* PWM Fault condition has occurred - RO */
        OC_STAT_RUNNING = (1 <<  6)   /* Timer source has been triggered and is running */
    } OC_STAT;

#endif

/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_OC)


        OC_STAT csp_oc_stat_get(OC_TYPE *oc);

        void    csp_oc_r_set(OC_TYPE *oc, UWORD val);
        UWORD   csp_oc_r_get(OC_TYPE *oc);

        void    csp_oc_rs_set(OC_TYPE *oc, UWORD val);
        UWORD   csp_oc_rs_get(OC_TYPE *oc);

        #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
            )

            void    csp_oc_conf_set(OC_TYPE *oc, UWORD val);
            UWORD   csp_oc_conf_get(OC_TYPE *oc);


        #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
               (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
              )

            void    csp_oc_conf_set(OC_TYPE *oc, U32 val);
            U32     csp_oc_conf_get(OC_TYPE *oc);

            void    csp_oc_stat_clr(OC_TYPE *oc, OC_STAT mask);

            void    csp_oc_tmr_set(OC_TYPE *oc, UWORD val);
            UWORD   csp_oc_tmr_get(OC_TYPE *oc);

        #endif

    #else

        #include "csp_oc.c"

    #endif

#endif

#endif /* #ifndef __CSP_OC_H */
/* ********************************************************************************************** */
/* end of file: csp_oc.h */
