/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_pmp.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_PMP_H
#define __CSP_PMP_H

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
    )

    /* PMP programming model */
    /* --------------------- */

    typedef union __PMP_TYPE
    {
        struct
        {
            volatile UWORD PMCON;
            volatile UWORD PMMODE;
            volatile UWORD PMADDR;
            volatile UWORD PMDOUT;
            volatile UWORD PMDIN[2];
            volatile UWORD PMAEN;
            volatile UWORD PMSTAT;
        } M;

        struct
        {
            volatile UWORD PMCON;
            volatile UWORD PMMODE;
            volatile UWORD PMDOUT[2];
            volatile UWORD PMDIN[2];
            volatile UWORD PMAEN;
            volatile UWORD PMSTAT;
        } S;
    } PMP_TYPE;

    /* PMP base addresses */
    /* ------------------ */

    #define PMP_BASE_ADDR       0x0600

    /* Declare PMP model */
    /* ----------------- */

    extern  PMP_TYPE PMP __CSP_SFR(PMP_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    #define __PMP_STAT_BUSY_BIT     (15)

    /* Definitions for PMP API */
    /* ----------------------- */

    /* csp_pmp_conf_set() and csp_pmp_conf_get() parameters mask */

    #define PMP_EN                  (1ULL << 31)    /* PMP is enabled */
    #define PMP_DIS                 (0ULL << 31)    /* PMP is disabled, no off-chip access performed */

    #define PMP_IDLE_STOP           (1ULL << 29)    /* Discontinue module operation when device enters Idle mode */
    #define PMP_IDLE_CON            (0ULL << 29)    /* Continue module operation in Idle mode */

    #define PMP_ADDR_SEPARATE       (0ULL << 27)    /* Address and data appear on separate pins */
    #define PMP_ADDR_MUX_LB         (1ULL << 27)    /* Lower 8 bits of address are multiplexed on PMD<7:0> pins, upper 8 bits are on PMA<15:8> */
    #define PMP_ADDR_MUX_LB_HB      (2ULL << 27)    /* All 16 bits of address are multiplexed on PMD<7:0> pins */

    #define PMP_PIN_BEN_EN          (1ULL << 26)    /* PMBE port enabled */
    #define PMP_PIN_BEN_DIS         (0ULL << 26)    /* PMBE port disabled */

    #define PMP_PIN_WREN_EN         (1ULL << 25)    /* PMWR/PMENB port enabled */
    #define PMP_PIN_WREN_DIS        (0ULL << 25)    /* PMWR/PMENB port disabled */

    #define PMP_PIN_RDEN_EN         (1ULL << 24)    /* PMRD/PMWR port enabled */
    #define PMP_PIN_RDEN_DIS        (0ULL << 24)    /* PMRD/PMWR port disabled */

    #define PMP_PIN_PMALL_HIGH      (1ULL << 21)    /* Active-high (PMALL and PMALH) */
    #define PMP_PIN_PMALL_LOW       (1ULL << 21)    /* Active-low (PMALL and PMALH) */

    #define PMP_PIN_CS1_HIGH        (1ULL << 19)    /* Active-high (PMCS1/PMCS2) */
    #define PMP_PIN_CS1_LOW         (0ULL << 19)    /* Active-low (PMCS1/PMCS2) */

    #define PMP_PIN_BEN_HIGH        (1ULL << 18)    /* Byte enable active-high (PMBE) */
    #define PMP_PIN_BEN_LOW         (0ULL << 18)    /* Byte enable active-low (PMBE) */

    #define PMP_PIN_WREN_HIGH       (1ULL << 17)    /* PMWR/PMENB is active-high */
    #define PMP_PIN_WREN_LOW        (0ULL << 17)    /* PMWR/PMENB is active-low */

    #define PMP_PIN_RDEN_HIGH       (1ULL << 16)    /* PMRD/PMWR is active-high */
    #define PMP_PIN_RDEN_LOW        (0ULL << 16)    /* PMRD/PMWR is active low */


    #define PMP_INT_DIS             (0ULL << 13)    /* No Interrupt generated */
    #define PMP_INT_END_OF_CYCLE    (1ULL << 13)    /* Interrupt generated at the end of the read/write cycle */
    #define PMP_INT_END_OF_BUF      (3ULL << 13)    /* Interrupt generated when Read Buffer 3 is read or Write Buffer 3 is written ... */

    #define PMP_ADDR_INC_DIS        (0ULL << 11)    /* No increment or decrement of address */
    #define PMP_ADDR_INC            (1ULL << 11)    /* Increment ADDR<15,13:0> by 1 every read/write cycle */
    #define PMP_ADDR_DEC            (2ULL << 11)    /* Decrement ADDR<15,13:0> by 1 every read/write cycle */
    #define PMP_ADDR_INC_SLAVE      (3ULL << 11)    /* PSP read and write buffers auto-increment (Legacy PSP mode only) */

    #define PMP_DATA_8_BIT          (0ULL << 10)    /* 8-Bit mode: data register is 8 bits, a read or write to the data register invokes one 8-bit transfer */
    #define PMP_DATA_16_BIT         (1ULL << 10)    /* 16-Bit mode: data register is 16 bits, a read or write to the data register invokes two 8-bit transfers */

    #define PMP_MODE_SLAVE_LEGACY   (0ULL <<  8)    /* Legacy Parallel Slave Port, control signals (PMRD, PMWR, PMCSx and PMD<7:0>) */
    #define PMP_MODE_SLAVE_ENH      (1ULL <<  8)    /* Enhanced PSP, control signals (PMRD, PMWR, PMCSx, PMD<7:0> and PMA<1:0>) */
    #define PMP_MODE_MASTER_2       (2ULL <<  8)    /* Master Mode 2 (PMCSx, PMRD, PMWR, PMBE, PMA<x:0> and PMD<7:0>) */
    #define PMP_MODE_MASTER_1       (3ULL <<  8)    /* Master Mode 1 (PMCSx, PMRD/PMWR, PMENB, PMBE, PMA<x:0> and PMD<7:0>) */

    #define PMP_WAITB_1_TCY         (0ULL <<  6)    /* Data Setup to Read/Write Wait State is 1 TCY */
    #define PMP_WAITB_2_TCY         (1ULL <<  6)    /* Data Setup to Read/Write Wait State is 2 TCY */
    #define PMP_WAITB_3_TCY         (2ULL <<  6)    /* Data Setup to Read/Write Wait State is 3 TCY */
    #define PMP_WAITB_4_TCY         (3ULL <<  6)    /* Data Setup to Read/Write Wait State is 4 TCY */

    #define PMP_WAITM_0_TCY         (0ULL  << 2)    /* Read to Byte Enable Strobe Wait State is 0 TCY */
    #define PMP_WAITM_1_TCY         (1ULL  << 2)    /* Read to Byte Enable Strobe Wait State is 1 TCY */
    #define PMP_WAITM_2_TCY         (2ULL  << 2)    /* Read to Byte Enable Strobe Wait State is 2 TCY */
    #define PMP_WAITM_3_TCY         (3ULL  << 2)    /* Read to Byte Enable Strobe Wait State is 3 TCY */
    #define PMP_WAITM_4_TCY         (4ULL  << 2)    /* Read to Byte Enable Strobe Wait State is 4 TCY */
    #define PMP_WAITM_5_TCY         (5ULL  << 2)    /* Read to Byte Enable Strobe Wait State is 5 TCY */
    #define PMP_WAITM_6_TCY         (6ULL  << 2)    /* Read to Byte Enable Strobe Wait State is 6 TCY */
    #define PMP_WAITM_7_TCY         (7ULL  << 2)    /* Read to Byte Enable Strobe Wait State is 7 TCY */
    #define PMP_WAITM_8_TCY         (8ULL  << 2)    /* Read to Byte Enable Strobe Wait State is 8 TCY */
    #define PMP_WAITM_9_TCY         (9ULL  << 2)    /* Read to Byte Enable Strobe Wait State is 9 TCY */
    #define PMP_WAITM_10_TCY        (10ULL << 2)    /* Read to Byte Enable Strobe Wait State is 10 TCY */
    #define PMP_WAITM_11_TCY        (11ULL << 2)    /* Read to Byte Enable Strobe Wait State is 11 TCY */
    #define PMP_WAITM_12_TCY        (12ULL << 2)    /* Read to Byte Enable Strobe Wait State is 12 TCY */
    #define PMP_WAITM_13_TCY        (13ULL << 2)    /* Read to Byte Enable Strobe Wait State is 13 TCY */
    #define PMP_WAITM_14_TCY        (14ULL << 2)    /* Read to Byte Enable Strobe Wait State is 14 TCY */
    #define PMP_WAITM_15_TCY        (15ULL << 2)    /* Read to Byte Enable Strobe Wait State is 15 TCY */

    #define PMP_WAITE_1_TCY         (0ULL <<  0)    /* Data Hold After Strobe Wait State is 1 TCY */
    #define PMP_WAITE_2_TCY         (1ULL <<  0)    /* Data Hold After Strobe Wait State is 2 TCY */
    #define PMP_WAITE_3_TCY         (2ULL <<  0)    /* Data Hold After Strobe Wait State is 3 TCY */
    #define PMP_WAITE_4_TCY         (3ULL <<  0)    /* Data Hold After Strobe Wait State is 4 TCY */


    /* csp_pmp_stat_get() and csp_pmp_stat_clr() parameters type */

    typedef enum __PMP_STAT
    {
        PMP_STAT_INP_BUFS_FULL  = (1 << 15),   /* RO  - All writable input buffer registers are full */
        PMP_STAT_INP_BUFS_OVF   = (1 << 14),   /* R/C - A write attempt to a full input byte register occurred */
        PMP_STAT_INP_BUF3_FULL  = (1 << 11),   /* RO  - Input buffer 3 contains data that has not been read */
        PMP_STAT_INP_BUF2_FULL  = (1 << 10),   /* RO  - Input buffer 2 contains data that has not been read */
        PMP_STAT_INP_BUF1_FULL  = (1 <<  9),   /* RO  - Input buffer 1 contains data that has not been read */
        PMP_STAT_INP_BUF0_FULL  = (1 <<  8),   /* RO  - Input buffer 0 contains data that has not been read */
        PMP_STAT_OUT_BUFS_EMPTY = (1 <<  7),   /* RO  - All readable output buffer registers are empty */
        PMP_STAT_OUT_BUFS_UDF   = (1 <<  6),   /* R/C - A read occurred from an empty output byte register */
        PMP_STAT_OUT_BUF3_EMPTY = (1 <<  3),   /* RO  - Output 3 buffer is empty */
        PMP_STAT_OUT_BUF2_EMPTY = (1 <<  2),   /* RO  - Output 2 buffer is empty */
        PMP_STAT_OUT_BUF1_EMPTY = (1 <<  1),   /* RO  - Output 1 buffer is empty */
        PMP_STAT_OUT_BUF0_EMPTY = (1 <<  0)    /* RO  - Output 0 buffer is empty */
    } PMP_STAT;

    /* csp_pmp_slave_put() and csp_pmp_slave_get() parameters type */

    typedef enum __PMP_BUF
    {
        PMP_BUF_0,
        PMP_BUF_1,
        PMP_BUF_2,
        PMP_BUF_3,
    } PMP_BUF;

#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
    )

    /* csp_pmp_conf_set() and csp_pmp_conf_get() parameters mask */

    #define PMP_CS1_ADDR_CS2_ADDR   (0ULL << 22)    /* PMCS1 and PMCS2 function as address bits 15 and 14 */
    #define PMP_CS1_ADDR_CS2_CS     (1ULL << 22)    /* PMCS2 functions as chip select, PMCS1 functions as address bit 14 */
    #define PMP_CS1_CS_CS2_CS       (2ULL << 22)    /* PMCS1 and PMCS2 function as chip select */

    #define PMP_PIN_CS2_HIGH        (1ULL << 20)    /* Active-high (PMCS2) */
    #define PMP_PIN_CS2_LOW         (0ULL << 20)    /* Active-low (PMCS2) */

    #define PMP_PIN_CS1_HIGH        (1ULL << 19)    /* Active-high (PMCS1) */
    #define PMP_PIN_CS1_LOW         (0ULL << 19)    /* Active-low (PMCS1) */

    /* csp_pmp_addr_en() and csp_pmp_addr_dis() parameters type */

    typedef enum __PMP_ADDR
    {
        PMP_A15 = (1 << 15),
        PMP_A14 = (1 << 14),
        PMP_A13 = (1 << 13),
        PMP_A12 = (1 << 12),
        PMP_A11 = (1 << 11),
        PMP_A10 = (1 << 10),
        PMP_A9  = (1 <<  9),
        PMP_A8  = (1 <<  8),
        PMP_A7  = (1 <<  7),
        PMP_A6  = (1 <<  6),
        PMP_A5  = (1 <<  5),
        PMP_A4  = (1 <<  4),
        PMP_A3  = (1 <<  3),
        PMP_A2  = (1 <<  2),
        PMP_A1  = (1 <<  1),
        PMP_A0  = (1 <<  0)
    } PMP_ADDR;

    /* csp_pmp_cs_en() and csp_pmp_cs_dis() parameters type */

    typedef enum __PMP_CS
    {
        PMP_CS2 = (1 << 15),
        PMP_CS1 = (1 << 14)
    } PMP_CS;


#elif (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)

    /* csp_pmp_conf_set() and csp_pmp_conf_get() parameters mask */

    #define PMP_CS1_CS               (2ULL << 22)   /* PMCS1 functions as chip set */

    /* csp_pmp_addr_en() and csp_pmp_addr_dis() parameters type */

    typedef enum __PMP_ADDR
    {
        PMP_A14 = (1 << 14),
        #if (__CSP_PMP_ADDR_COUNT == __CSP_PMP_ADDE_CNT_11)
        PMP_A10 = (1 << 10),
        PMP_A9  = (1 <<  9),
        PMP_A8  = (1 <<  8),
        PMP_A7  = (1 <<  7),
        PMP_A6  = (1 <<  6),
        PMP_A5  = (1 <<  5),
        PMP_A4  = (1 <<  4),
        PMP_A3  = (1 <<  3),
        PMP_A2  = (1 <<  2),
        #endif
        PMP_A1  = (1 <<  1),
        PMP_A0  = (1 <<  0)
    } PMP_ADDR;

    /* csp_pmp_cs_en() and csp_pmp_cs_dis() parameters type */

    typedef enum __PMP_CS
    {
        PMP_CS1 = (1 << 14)
    } PMP_CS;


#endif


/* External functions prototypes */
/* ----------------------------- */

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
    )
    #if !defined(__CSP_BUILD_LIB)
    
        /* Module API definition */
    
        #if defined(__CSP_DEBUG_PMP)
    
            void     csp_pmp_conf_set(U32 val);
            U32      csp_pmp_conf_get(void);
    
            void     csp_pmp_addr_en(PMP_ADDR mask);
            void     csp_pmp_addr_dis(PMP_ADDR mask);
    
            UWORD    csp_pmp_busy_check(void);
    
            void     csp_pmp_addr_set(UWORD val);
            UWORD    csp_pmp_addr_get(void);
    
            PMP_STAT csp_pmp_stat_get(void);
            void     csp_pmp_stat_clr(PMP_STAT mask);
    
            void     csp_pmp_slave_put(PMP_BUF buf, U08 val);
            U08      csp_pmp_slave_get(PMP_BUF buf);
    
            void     csp_pmp_master_put(UWORD val);
            void     csp_pmp_master_lput(UWORD val);
            UWORD    csp_pmp_master_get(void);
    
            void     csp_pmp_master_wr(UWORD addr, UWORD val);
    
        #else
    
            #include "csp_pmp.c"

        #endif
    #endif

#endif

#endif /* #ifndef __CSP_PMP_H */
/* ********************************************************************************************** */
/* end of file: csp_pmp.h */
