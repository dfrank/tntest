/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_tmr.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_TMR_H
#define __CSP_TMR_H

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)        \
    )

    /* TIMER A programming model */
    /* ------------------------- */

    typedef struct __TMR_A_TYPE
    {
        volatile UWORD TMR;
        volatile UWORD PR;
        volatile UWORD TCON;
    } TMR_A_TYPE;

    /* TIMER B programming model */
    /* ------------------------- */

    typedef struct __TMR_B_TYPE
    {
        volatile UWORD TMR;
        __CSP_SKIP_BYTE(4);
        volatile UWORD PR;
        __CSP_SKIP_BYTE(2);
        volatile UWORD TCON;
    } TMR_B_TYPE;

    /* TIMER C programming model */
    /* ------------------------- */

    typedef struct __TMR_C_TYPE
    {
        volatile UWORD TMRHLD;
        volatile UWORD TMR;
        __CSP_SKIP_BYTE(2);
        volatile UWORD PR;
        __CSP_SKIP_BYTE(2);
        volatile UWORD TCON;
    } TMR_C_TYPE;

    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||   \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||   \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||   \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)      \
        )

        /* TIMERS base addresses */
        /* --------------------- */
        #define TMR1_BASE_ADDR      0x0100      /* TMR_A_TYPE */
        #define TMR2_BASE_ADDR      0x0106      /* TMR_B_TYPE */
        #define TMR3_BASE_ADDR      0x0108      /* TMR_C_TYPE */
        #define TMR4_BASE_ADDR      0x0114      /* TMR_B_TYPE */
        #define TMR5_BASE_ADDR      0x0116      /* TMR_C_TYPE */
    
        /* Declare TIMERS models */
        /* --------------------- */
        extern TMR_A_TYPE TMR1 __CSP_SFR(TMR1_BASE_ADDR);
        extern TMR_B_TYPE TMR2 __CSP_SFR(TMR2_BASE_ADDR);
        extern TMR_C_TYPE TMR3 __CSP_SFR(TMR3_BASE_ADDR);
        extern TMR_B_TYPE TMR4 __CSP_SFR(TMR4_BASE_ADDR);
        extern TMR_C_TYPE TMR5 __CSP_SFR(TMR5_BASE_ADDR);

    #elif (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

        /* TIMERS base addresses */
        /* --------------------- */
        #define TMR1_BASE_ADDR      0x0100      /* TMR_A_TYPE */
        #define TMR2_BASE_ADDR      0x0106      /* TMR_B_TYPE */
        #define TMR3_BASE_ADDR      0x0108      /* TMR_C_TYPE */
    
        /* Declare TIMERS models */
        /* --------------------- */
        extern TMR_A_TYPE TMR1 __CSP_SFR(TMR1_BASE_ADDR);
        extern TMR_B_TYPE TMR2 __CSP_SFR(TMR2_BASE_ADDR);
        extern TMR_C_TYPE TMR3 __CSP_SFR(TMR3_BASE_ADDR);

    #endif

    /* Internal API definitions */
    /* ------------------------ */

    #define __TMR_EN_BIT            (15)

    /* Definitions for TIMERS API */
    /* -------------------------- */

    /* Most PPS API functions parameters type */

    typedef enum __TMR_CONF
    {
        TMR_EN             = (1 << 15),     /* Starts the timer */
        TMR_DIS            = (0),           /* Stops the timer  */
    
        TMR_IDLE_STOP      = (1 << 13),     /* Discontinue timer operation when device enters Idle mode */
        TMR_IDLE_CON       = (0),           /* Continue timer operation in Idle mode                    */
    
        TMR_GATE_EN        = (1 <<  6),     /* Gated time accumulation enabled  */
        TMR_GATE_DIS       = (0),           /* Gated time accumulation disabled */
    
        TMR_PS_1_1         = (0 <<  4),     /* Timer Input Clock Prescale coefficient 1:1   */
        TMR_PS_1_8         = (1 <<  4),     /* Timer Input Clock Prescale coefficient 1:8   */
        TMR_PS_1_64        = (2 <<  4),     /* Timer Input Clock Prescale coefficient 1:64  */
        TMR_PS_1_256       = (3 <<  4),     /* Timer Input Clock Prescale coefficient 1:256 */
    
        TMR_SOURCE_EXT     = (1 <<  1),     /* External clock from TxCK pin */
        TMR_SOURCE_INT     = (0),           /* Internal clock (FOSC/2)      */
    
            /* Only for TMR_A */
    
        TMR_SYNC_EXT_EN    = (1 <<  2),     /* Synchronize external clock input        */
        TMR_SYNC_EXT_DIS   = (0),           /* Do not synchronize external clock input */
    
            /* Only for TMR_C */
    
        TMR_32BIT_MODE_EN  = (1 << 3),      /* TMR2 and TMR3 form a 32-bit timer        */
        TMR_32BIT_MODE_DIS = (0)            /* TMR2 and TMR3 form separate 16-bit timer */
    } TMR_CONF;

#endif

/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_TMR)
    

        void     csp_tmr_conf_set(void *tmr, TMR_CONF val);
        TMR_CONF csp_tmr_conf_get(void *tmr);
        void     csp_tmr_set(void *tmr, UWORD val);
        UWORD    csp_tmr_get(void *tmr);
        void     csp_tmr_period_set(void *tmr, UWORD val);
        UWORD    csp_tmr_period_get(void *tmr);
        void     csp_tmr_en(void *tmr);
        void     csp_tmr_dis(void *tmr);


        void     csp_tmr_a_conf_set(TMR_A_TYPE *tmr, TMR_CONF val);
        TMR_CONF csp_tmr_a_conf_get(TMR_A_TYPE *tmr);
        void     csp_tmr_a_set(TMR_A_TYPE *tmr, UWORD val);
        UWORD    csp_tmr_a_get(TMR_A_TYPE *tmr);
        void     csp_tmr_a_period_set(TMR_A_TYPE *tmr, UWORD val);
        UWORD    csp_tmr_a_period_get(TMR_A_TYPE *tmr);
        void     csp_tmr_a_en(TMR_A_TYPE *tmr);
        void     csp_tmr_a_dis(TMR_A_TYPE *tmr);

        void     csp_tmr_b_conf_set(TMR_B_TYPE *tmr, TMR_CONF val);
        TMR_CONF csp_tmr_b_conf_get(TMR_B_TYPE *tmr);
        void     csp_tmr_b_set(TMR_B_TYPE *tmr, UWORD val);
        UWORD    csp_tmr_b_get(TMR_B_TYPE *tmr);
        void     csp_tmr_b_period_set(TMR_B_TYPE *tmr, UWORD val);
        UWORD    csp_tmr_b_period_get(TMR_B_TYPE *tmr);
        void     csp_tmr_b_en(TMR_B_TYPE *tmr);
        void     csp_tmr_b_dis(TMR_B_TYPE *tmr);

        void     csp_tmr_c_conf_set(TMR_C_TYPE *tmr, TMR_CONF val);
        TMR_CONF csp_tmr_c_conf_get(TMR_C_TYPE *tmr);
        void     csp_tmr_c_set(TMR_C_TYPE *tmr, UWORD val);
        UWORD    csp_tmr_c_get(TMR_C_TYPE *tmr);
        void     csp_tmr_c_period_set(TMR_C_TYPE *tmr, UWORD val);
        UWORD    csp_tmr_c_period_get(TMR_C_TYPE *tmr);
        void     csp_tmr_c_en(TMR_C_TYPE *tmr);
        void     csp_tmr_c_dis(TMR_C_TYPE *tmr);

        void  csp_tmr_32b_set(TMR_B_TYPE *tmr, U32 val);
        U32   csp_tmr_32b_get(TMR_B_TYPE *tmr);
        void  csp_tmr_32b_period_set(TMR_B_TYPE *tmr, U32 val);
        U32   csp_tmr_32b_period_get(TMR_B_TYPE *tmr);

    #else

        #include "csp_tmr.c"

    #endif

#endif

#endif /* #ifndef __CSP_TMR_H */
/* ********************************************************************************************** */
/* end of file: csp_tmr.h */
