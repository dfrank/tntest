/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_ADC_H
#define __CSP_ADC_H

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
        )

        /* ADC programming model */
        /* --------------------- */

        typedef struct __ADC_TYPE
        {
            volatile UWORD ADCBUF[16];
            volatile UWORD ADCON1;
            volatile UWORD ADCON2;
            volatile UWORD ADCON3;
            __CSP_SKIP_BYTE(2);
            volatile UWORD ADCHS;
            __CSP_SKIP_BYTE(2);
            volatile UWORD ADPCFG;
            __CSP_SKIP_BYTE(2);
            volatile UWORD ADCSSL;
        } ADC_TYPE;

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
          )

        typedef struct __ADC_TYPE
        {
            volatile UWORD ADCBUF[16];
            volatile UWORD ADCON1;
            volatile UWORD ADCON2;
            volatile UWORD ADCON3;
            __CSP_SKIP_BYTE(2);
            volatile UWORD ADCHS0;
            volatile UWORD ADPCFGH;
            volatile UWORD ADPCFGL;
            __CSP_SKIP_BYTE(2);
            volatile UWORD ADCSSL;
            volatile UWORD ADCSSH;

        } ADC_TYPE;

    #endif

    /* ADC base addresses */
    /* ------------------ */

    #define ADC_BASE_ADDR       0x0300

    /* Declare ADC models */
    /* ------------------ */

    extern ADC_TYPE ADC1 __CSP_SFR(ADC_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    #define __ADC_CLOCK_FS          (0)
#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
    )
    #define __ADC_CLOCK_FE          (7)
#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)     \
    )
    #define __ADC_CLOCK_FE          (5)
#endif

    #define __ADC_SAMP_TIME_FS      (8)
    #define __ADC_SAMP_TIME_FE      (12)

    #define __ADC_MUXA_POS_FS       (0)
    #define __ADC_MUXA_POS_FE       (3)

    #define __ADC_MUXA_FULL_FS      (0)
    #define __ADC_MUXA_FULL_FE      (7)
    
    #define __ADC_MUXB_POS_FS       (8)
    #define __ADC_MUXB_POS_FE       (11)

    #define __ADC_MUXB_FULL_FS      (8)
    #define __ADC_MUXB_FULL_FE      (15)

    #define __ADC_SAMP_BIT          (1)

    #define __ADC_CON1_CONF_MASK      ~((1 << 0) | (1 << 1))
    #define __ADC_CON2_CONF_MASK      ~(1 << 7)
    #define __ADC_CON3_CONF_MASK      ~(ADC_CLOCK_RC)

    #define __ADC_EN_BIT            (15)


    /* Definitions for ADC API */
    /* ----------------------- */

    /* csp_adc_conf_set() and csp_adc_conf_get() parameters mask */

    #define ADC_EN                  (1ULL << 47)    /* A/D converter module is operating */
    #define ADC_DIS                 (0ULL << 47)    /* A/D converter is disabled */

    #define ADC_IDLE_STOP           (1ULL << 45)    /* Discontinue module operation when device enters Idle mode */
    #define ADC_IDLE_CON            (0ULL << 45)    /* Continue module operation in Idle mode */

    #define ADC_RESULT_INT          (0ULL << 40)    /* Integer (0000 00dd dddd dddd) */
    #define ADC_RESULT_SIGN_INT     (1ULL << 40)    /* Signed integer (ssss sssd dddd dddd) */
    #define ADC_RESULT_FRACT        (2ULL << 40)    /* Fractional (dddd dddd dd00 0000) */
    #define ADC_RESULT_SIGN_FRACT   (3ULL << 40)    /* Signed fractional (sddd dddd dd00 0000) */

    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
        )

        #define ADC_CONV_TRIG_MANUAL    (0ULL << 37)    /* Clearing SAMP bit ends sampling and starts conversion */
        #define ADC_CONV_TRIG_INT0      (1ULL << 37)    /* Active transition on INT0 pin ends sampling and starts conversion */
        #define ADC_CONV_TRIG_TMR3      (2ULL << 37)    /* Timer3 compare match ends sampling and starts conversion */
        #define ADC_CONV_TRIG_AUTO      (7ULL << 37)    /* Internal counter ends sampling and starts conversion (auto convert) */

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
          )

        #define ADC_CONV_TRIG_MANUAL    (0ULL << 37)    /* Clearing SAMP bit ends sampling and starts conversion */
        #define ADC_CONV_TRIG_INT0      (1ULL << 37)    /* Active transition on INT0 pin ends sampling and starts conversion */
        #define ADC_CONV_TRIG_TMR3      (2ULL << 37)    /* Timer3 compare match ends sampling and starts conversion */
        #define ADC_CONV_TRIG_TMR5      (4ULL << 37)    /* Timer5 compare match ends sampling and starts conversion */
        #define ADC_CONV_TRIG_CTMU      (6ULL << 37)    /* CTMU ends sampling and starts conversion */
        #define ADC_CONV_TRIG_AUTO      (7ULL << 37)    /* Internal counter ends sampling and starts conversion (auto convert) */

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102) \
          )

        #define ADC_CONV_TRIG_MANUAL    (0ULL << 37)    /* Clearing SAMP bit ends sampling and starts conversion */
        #define ADC_CONV_TRIG_INT0      (1ULL << 37)    /* Active transition on INT0 pin ends sampling and starts conversion */
        #define ADC_CONV_TRIG_TMR3      (2ULL << 37)    /* Timer3 compare match ends sampling and starts conversion */
        #define ADC_CONV_TRIG_CTMU      (6ULL << 37)    /* CTMU ends sampling and starts conversion */
        #define ADC_CONV_TRIG_AUTO      (7ULL << 37)    /* Internal counter ends sampling and starts conversion (auto convert) */

    #endif

    #define ADC_AUTO_CONV_EN        (1ULL << 34)    /* Sampling begins immediately after last conversion completes; SAMP bit is automatically set */
    #define ADC_AUTO_CONV_DIS       (0ULL << 34)    /* Sampling begins when SAMP bit is set */


    #define ADC_VREF_AVDD_AVSS      (0ULL << 29)    /* ADC refernce is AVDD for Vr+ and AVSS for Vr- */
    #define ADC_VREF_EXTP_AVSS      (1ULL << 29)    /* ADC refernce is Vref+ for Vr+ and AVSS for Vr- */
    #define ADC_VREF_AVDD_EXTM      (2ULL << 29)    /* ADC refernce is AVDD for Vr+ and Vref- for Vr- */
    #define ADC_VREF_EXTP_EXTM      (3ULL << 29)    /* ADC refernce is Vref+ for Vr+ and Vref- for Vr-*/

    #if  ((__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102) \
         )
    
        #define ADC_CALIBRATION_DIS (0ULL << 28)
        #define ADC_CALIBRATION_EN  (1ULL << 28)

    #endif

    #define ADC_SCAN_EN             (1ULL << 26)    /* Scan inputs */
    #define ADC_SCAN_DIS            (0ULL << 26)    /* Do not scan inputs */

    #define ADC_INT_CONV_1          (0ULL  << 18)   /* Interrupts at the completion of conversion for each sample/convert sequence */
    #define ADC_INT_CONV_2          (1ULL  << 18)   /* Interrupts at the completion of conversion for each 2nd sample/convert sequence */
    #define ADC_INT_CONV_3          (2ULL  << 18)   /* Interrupts at the completion of conversion for each 3d sample/convert sequence */
    #define ADC_INT_CONV_4          (3ULL  << 18)   /* Interrupts at the completion of conversion for each 4th sample/convert sequence */
    #define ADC_INT_CONV_5          (4ULL  << 18)   /* Interrupts at the completion of conversion for each 5th sample/convert sequence */
    #define ADC_INT_CONV_6          (5ULL  << 18)   /* Interrupts at the completion of conversion for each 6th sample/convert sequence */
    #define ADC_INT_CONV_7          (6ULL  << 18)   /* Interrupts at the completion of conversion for each 7th sample/convert sequence */
    #define ADC_INT_CONV_8          (7ULL  << 18)   /* Interrupts at the completion of conversion for each 8th sample/convert sequence */
    #define ADC_INT_CONV_9          (8ULL  << 18)   /* Interrupts at the completion of conversion for each 9th sample/convert sequence */
    #define ADC_INT_CONV_10         (9ULL  << 18)   /* Interrupts at the completion of conversion for each 10th sample/convert sequence */
    #define ADC_INT_CONV_11         (10ULL << 18)   /* Interrupts at the completion of conversion for each 11th sample/convert sequence */
    #define ADC_INT_CONV_12         (11ULL << 18)   /* Interrupts at the completion of conversion for each 12th sample/convert sequence */
    #define ADC_INT_CONV_13         (12ULL << 18)   /* Interrupts at the completion of conversion for each 13th sample/convert sequence */
    #define ADC_INT_CONV_14         (13ULL << 18)   /* Interrupts at the completion of conversion for each 14th sample/convert sequence */
    #define ADC_INT_CONV_15         (14ULL << 18)   /* Interrupts at the completion of conversion for each 15th sample/convert sequence */
    #define ADC_INT_CONV_16         (15ULL << 18)   /* Interrupts at the completion of conversion for each 16th sample/convert sequence */

    #define ADC_BUF_MODEL_TWO       (1ULL << 17)    /* Buffer configured as two 8-word buffers */
    #define ADC_BUF_MODEL_ONE       (0ULL << 17)    /* Buffer configured as one 16-word buffer */

    #define ADC_ALT_MUX_EN          (1ULL << 16)    /* Uses MUX A input multiplexer settings for first sample, then alternates between MUX B and MUX A */
    #define ADC_ALT_MUX_DIS         (0ULL << 16)    /* Always uses MUX A input multiplexer settings */


    #define ADC_CLOCK_RC            (1ULL << 15)    /* A/D internal RC clock */
    #define ADC_CLOCK_TCY           (0ULL << 15)    /* Clock derived from system clock */




    /* csp_adc_mux_X_set() and csp_adc_mux_X_get parameters type */

    typedef enum __ADC_MUX_NEG
    {
        ADC_MUX_NEG_VREFM = 0,
        ADC_MUX_NEG_AN1   = (1 << 7)
    } ADC_MUX_NEG;

    /* csp_adc_comm() parameter type */

    typedef enum __ADC_COMM
    {
        ADC_COMM_SAMP_START = (0),
        ADC_COMM_CONV_START = (1)
    } ADC_COMM;

    /* csp_adc_get() parameter type */

    typedef enum __ADC_BUF
    {
        ADC_BUF_0,
        ADC_BUF_1,
        ADC_BUF_2,
        ADC_BUF_3,
        ADC_BUF_4,
        ADC_BUF_5,
        ADC_BUF_6,
        ADC_BUF_7,
        ADC_BUF_8,
        ADC_BUF_9,
        ADC_BUF_A,
        ADC_BUF_B,
        ADC_BUF_C,
        ADC_BUF_D,
        ADC_BUF_E,
        ADC_BUF_F
    } ADC_BUF;

    /* Status bits in AD1CON1 and AD1CON2 returned by csp_adc_stat_get() */

    typedef enum __ADC_STAT
    {
        ADC_STAT_SAMP_DONE = (1 << 1),   /* R/W - AD1CON1<1> - At least one A/D sample/hold amplifier is sampling */
        ADC_STAT_CONV_DONE = (1 << 0),   /* R/C - AD1CON1<0> - A/D conversion is done */
        ADC_STAT_FILL_8_16 = (1 << 7)    /* RO  - AD1CON2<7> - A/D is currently filling ADC1BUF8-ADC1BUFF, user should access data in ADC1BUF0-ADC1BUF7 */
    } ADC_STAT;



    #if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

        /* csp_adc_mux_X_set() and csp_adc_mux_X_get parameters type */

        typedef enum __ADC_MUX_POS
        {
            ADC_MUX_POS_AN0,
            ADC_MUX_POS_AN1,
            ADC_MUX_POS_AN2,
            ADC_MUX_POS_AN3,
            ADC_MUX_POS_AN4,
            ADC_MUX_POS_AN5,
            ADC_MUX_POS_AVSS,
            ADC_MUX_POS_AVDD,


            ADC_MUX_POS_AN10  = 10,
            ADC_MUX_POS_AN11  = 11,
            ADC_MUX_POS_AN12  = 12,
            ADC_MUX_POS_NC    = 13,
            ADC_MUX_POS_VBG_2 = 14,
            ADC_MUX_POS_VBG   = 15,
        } ADC_MUX_POS;

        /* csp_adc_an_set() and other functions parameters type */
        
        typedef enum __ADC_CHAN
        {
            ADC_AN0  = (1 <<  0),
            ADC_AN1  = (1 <<  1),
            ADC_AN2  = (1 <<  2),
            ADC_AN3  = (1 <<  3),
            ADC_AN4  = (1 <<  4),
            ADC_AN5  = (1 <<  5),

            ADC_AN10 = (1 << 10),
            ADC_AN11 = (1 << 11),
            ADC_AN12 = (1 << 12),
        } ADC_CHAN;

        #define ADC_AN_ALL      0xFFFF

    #elif (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)

        /* csp_adc_mux_X_set() and csp_adc_mux_X_get parameters type */

        typedef enum __ADC_MUX_POS
        {
            ADC_MUX_POS_AN0,
            ADC_MUX_POS_AN1,
            ADC_MUX_POS_AN2,
            ADC_MUX_POS_AN3,
            ADC_MUX_POS_AN4,
            ADC_MUX_POS_AN5,
            ADC_MUX_POS_AN6,
            ADC_MUX_POS_AN7,
            ADC_MUX_POS_AN8,
            ADC_MUX_POS_AN9,
            ADC_MUX_POS_AN10,
            ADC_MUX_POS_AN11,
            ADC_MUX_POS_AN12
        } ADC_MUX_POS;

        /* csp_adc_an_set() and other functions parameters type */

        typedef enum __ADC_CHAN
        {
            ADC_AN0  = (1 <<  0),
            ADC_AN1  = (1 <<  1),
            ADC_AN2  = (1 <<  2),
            ADC_AN3  = (1 <<  3),
            ADC_AN4  = (1 <<  4),
            ADC_AN5  = (1 <<  5),
            ADC_AN6  = (1 <<  6),
            ADC_AN7  = (1 <<  7),
            ADC_AN8  = (1 <<  8),
            ADC_AN9  = (1 <<  9),
            ADC_AN10 = (1 << 10),
            ADC_AN11 = (1 << 11),
            ADC_AN12 = (1 << 12),
        } ADC_CHAN;

        #define ADC_AN_ALL      0xFFFF

    #elif (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010)

        /* csp_adc_mux_X_set() and csp_adc_mux_X_get parameters type */

        typedef enum __ADC_MUX_POS
        {
            ADC_MUX_POS_AN0,
            ADC_MUX_POS_AN1,
            ADC_MUX_POS_AN2,
            ADC_MUX_POS_AN3,
            ADC_MUX_POS_AN4,
            ADC_MUX_POS_AN5,
            ADC_MUX_POS_AN6,
            ADC_MUX_POS_AN7,
            ADC_MUX_POS_AN8,
            ADC_MUX_POS_AN9,
            ADC_MUX_POS_AN10,
            ADC_MUX_POS_AN11,
            ADC_MUX_POS_AN12,
            ADC_MUX_POS_AN13,
            ADC_MUX_POS_AN14,
            ADC_MUX_POS_AN15
        } ADC_MUX_POS;

        /* csp_adc_an_set() and other functions parameters type */

        typedef enum __ADC_CHAN
        {
            ADC_AN0  = (1 <<  0),
            ADC_AN1  = (1 <<  1),
            ADC_AN2  = (1 <<  2),
            ADC_AN3  = (1 <<  3),
            ADC_AN4  = (1 <<  4),
            ADC_AN5  = (1 <<  5),
            ADC_AN6  = (1 <<  6),
            ADC_AN7  = (1 <<  7),
            ADC_AN8  = (1 <<  8),
            ADC_AN9  = (1 <<  9),
            ADC_AN10 = (1 << 10),
            ADC_AN11 = (1 << 11),
            ADC_AN12 = (1 << 12),
            ADC_AN13 = (1 << 13),
            ADC_AN14 = (1 << 14),
            ADC_AN15 = (1 << 15),
        } ADC_CHAN;

        #define ADC_AN_ALL      0xFFFF

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
          )

        /* csp_adc_mux_X_set() and csp_adc_mux_X_get parameters type */

        typedef enum __ADC_MUX_POS
        {
            ADC_MUX_POS_AN0,
            ADC_MUX_POS_AN1,
            ADC_MUX_POS_AN2,
            ADC_MUX_POS_AN3,
            ADC_MUX_POS_AN4,
            ADC_MUX_POS_AN5,
            ADC_MUX_POS_AN6,
            ADC_MUX_POS_AN7,
            ADC_MUX_POS_AN8,
            ADC_MUX_POS_AN9,
            ADC_MUX_POS_AN10,
            ADC_MUX_POS_AN11,
            ADC_MUX_POS_AN12,
            ADC_MUX_POS_AN13,
            ADC_MUX_POS_AN14,
            ADC_MUX_POS_AN15,
            ADC_MUX_POS_VBG_2,
            ADC_MUX_POS_VBG,
        } ADC_MUX_POS;

        typedef enum __ADC_CHAN
        {
            ADC_AN0  = (1 <<  0),
            ADC_AN1  = (1 <<  1),
            ADC_AN2  = (1 <<  2),
            ADC_AN3  = (1 <<  3),
            ADC_AN4  = (1 <<  4),
            ADC_AN5  = (1 <<  5),
            ADC_AN6  = (1 <<  6),
            ADC_AN7  = (1 <<  7),
            ADC_AN8  = (1 <<  8),
            ADC_AN9  = (1 <<  9),
            ADC_AN10 = (1 << 10),
            ADC_AN11 = (1 << 11),
            ADC_AN12 = (1 << 12),
            ADC_AN13 = (1 << 13),
            ADC_AN14 = (1 << 14),
            ADC_AN15 = (1 << 15),
        } ADC_CHAN;

        #define ADC_AN_VBG_HALF     (1UL << 16)
        #define ADC_AN_VBG_FULL     (1UL << 17)

        #define ADC_AN_ALL          0xFFFFFFFFUL

    #endif

#endif


/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_ADC)

        void        csp_adc_conf_set(ADC_TYPE *adc, U64 val);
        U64         csp_adc_conf_get(ADC_TYPE *adc);

        void        csp_adc_clock_set(ADC_TYPE *adc, UWORD val);
        UWORD       csp_adc_clock_get(ADC_TYPE *adc);

        void        csp_adc_samp_time_set(ADC_TYPE *adc, UWORD val);
        UWORD       csp_adc_samp_time_get(ADC_TYPE *adc);

        void        csp_adc_comm(ADC_TYPE *adc, ADC_COMM comm);

        ADC_STAT    csp_adc_stat_get(ADC_TYPE *adc);
        void        csp_adc_stat_clr(ADC_TYPE *adc, ADC_STAT mask);

        void        csp_adc_mux_a_set(ADC_TYPE *adc, ADC_MUX_NEG neg, ADC_MUX_POS pos);
        ADC_MUX_POS csp_adc_mux_a_get(ADC_TYPE *adc, ADC_MUX_NEG *neg);

        void        csp_adc_mux_b_set(ADC_TYPE *adc, ADC_MUX_NEG neg, ADC_MUX_POS pos);
        ADC_MUX_POS csp_adc_mux_b_get(ADC_TYPE *adc, ADC_MUX_NEG *neg);

        void        csp_adc_scan_chain_set(ADC_TYPE *adc, ADC_CHAN mask);
        void        csp_adc_scan_chain_clr(ADC_TYPE *adc, ADC_CHAN mask);
        ADC_CHAN    csp_adc_scan_chain_get(ADC_TYPE *adc);

        #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
            )

            void  csp_adc_an_set(ADC_TYPE *adc, U16 mask);
            void  csp_adc_an_clr(ADC_TYPE *adc, U16 mask);
            U16   csp_adc_an_get(ADC_TYPE *adc);

        #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
               (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
              )

            void  csp_adc_an_set(ADC_TYPE *adc, U32 mask);
            void  csp_adc_an_clr(ADC_TYPE *adc, U32 mask);
            U32   csp_adc_an_get(ADC_TYPE *adc);

        #endif

        UWORD       csp_adc_get(ADC_TYPE *adc, ADC_BUF buf);

        void        csp_adc_en(ADC_TYPE *adc);
        void        csp_adc_dis(ADC_TYPE *adc);

    #else

        #include "csp_adc.c"

    #endif

#endif

#endif /* #ifndef __CSP_ADC_H */
/* ********************************************************************************************** */
/* end of file: csp_adc.h */
