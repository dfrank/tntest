/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_crc.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_CRC_H
#define __CSP_CRC_H


#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    /* CRC programming model */
    /* --------------------- */

    typedef struct __CRC_TYPE
    {
        volatile UWORD CRCCON;
        volatile UWORD CRCXOR;
        volatile UWORD CRCDAT;
        volatile UWORD CRCWDAT;
    } CRC_TYPE;

    /* CRC base addresses */
    /* ------------------ */

    #define CRC_BASE_ADDR       0x0640

    /* Declare CRC model */
    /* ----------------- */

    extern  CRC_TYPE CRC __CSP_SFR(CRC_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    #define __CRC_FIFO_CNT_FS   (8)
    #define __CRC_FIFO_CNT_FE   (12)

    /* Definitions for CRC API */
    /* ----------------------- */

    /* csp_crc_conf_set() and csp_crc_conf_get() parameters mask */

    typedef enum
    {
        CRC_IDLE_STOP   = (1 << 13),    /* Discontinue module operation when device enters Idle mode */
        CRC_IDLE_CON    = (0 << 13),    /* Continue module operation in Idle mode */
                                        
        CRC_SHIFTER_EN  = (1 <<  4),    /* Start CRC serial shifter */
        CRC_SHIFTER_DIS = (0 <<  4),    /* CRC serial shifter turned off */
                                        
        CRC_POLY_16     = (15 << 0),    /* Polynomial Length is 16 bit */
        CRC_POLY_15     = (14 << 0),    /* Polynomial Length is 15 bit */
        CRC_POLY_14     = (13 << 0),    /* Polynomial Length is 14 bit */
        CRC_POLY_13     = (12 << 0),    /* Polynomial Length is 13 bit */
        CRC_POLY_12     = (11 << 0),    /* Polynomial Length is 12 bit */
        CRC_POLY_11     = (10 << 0),    /* Polynomial Length is 11 bit */
        CRC_POLY_10     = ( 9 << 0),    /* Polynomial Length is 10 bit */
        CRC_POLY_9      = ( 8 << 0),    /* Polynomial Length is 9 bit */
        CRC_POLY_8      = ( 7 << 0),    /* Polynomial Length is 8 bit */
        CRC_POLY_7      = ( 6 << 0),    /* Polynomial Length is 7 bit */
        CRC_POLY_6      = ( 5 << 0),    /* Polynomial Length is 6 bit */
        CRC_POLY_5      = ( 4 << 0),    /* Polynomial Length is 5 bit */
        CRC_POLY_4      = ( 3 << 0),    /* Polynomial Length is 4 bit */
        CRC_POLY_3      = ( 2 << 0),    /* Polynomial Length is 3 bit */
        CRC_POLY_2      = ( 1 << 0),    /* Polynomial Length is 2 bit */
        CRC_POLY_1      = ( 0 << 0)     /* Polynomial Length is 1 bit */
    } CRC_CONF;

    /* csp_crc_stat_get() return type definition */

    typedef enum __CRC_STAT
    {
        CRC_STAT_FIFO_FULL  = (1 << 7), /* RO - FIFO is full */
        CRC_STAT_FIFO_EMPTY = (1 << 6)  /* RO - FIFO is empty */
    } CRC_STAT;

#endif


/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_CRC)

        void     csp_crc_conf_set(CRC_CONF val);
        CRC_CONF csp_crc_conf_get(void);

        void     csp_crc_poly_set(UWORD val);
        UWORD    csp_crc_poly_get(void);

        void     csp_crc_result_set(UWORD val);
        UWORD    csp_crc_result_get(void);

        CRC_STAT csp_crc_stat_get(void);

        U08      csp_crc_fifo_cnt_get(void);

        void     csp_crc_word_put(UWORD val);
        void     csp_crc_byte_put(U08 val);

    #else

        #include "csp_crc.c"

    #endif

#endif

#endif /* #ifndef __CSP_CRC_H */
/* ********************************************************************************************** */
/* end of file: csp_crc.h */
