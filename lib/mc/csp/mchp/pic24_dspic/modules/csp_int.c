/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_cfgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC INT_CONF csp_int_conf_get (void)
{
    return (
            #if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)
            (__CSP_ACC_RANG_REG(BFA_RD, INT.INTTREG, 13, 13) << 2) |
            #endif
            (__CSP_ACC_RANG_REG(BFA_RD, INT.INTCON2, __INT_CONFBIT_FS, __INT_CONFBIT_FE) << 1) |
            (__CSP_ACC_RANG_REG(BFA_RD, INT.INTCON1, __INT_CONFBIT_FS, __INT_CONFBIT_FE)  ^ 1)
           ); /* invert*/
}

/* ********************************************************************************************** */
/* end of file: csp_int_cfgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_cfst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_int_conf_set (INT_CONF val)
{
    __CSP_ACC_RANG_REG(BFA_WR, INT.INTCON2, __INT_CONFBIT_FS, __INT_CONFBIT_FE, ((val & INT_ALT_TABLE_EN) ? 1 : 0));
    __CSP_ACC_RANG_REG(BFA_WR, INT.INTCON1, __INT_CONFBIT_FS, __INT_CONFBIT_FE, ((val & INT_NESTING_EN  ) ? 0 : 1));
    #if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)
    __CSP_ACC_RANG_REG(BFA_WR, INT.INTTREG, 13, 13, ((val & INT_HOLD_HIGHEST) ? 1 : 0));
    #endif
}

/* ********************************************************************************************** */
/* end of file: csp_int_cfst.c */

/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_chck.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_int_check (INT_SOURCE int_src)
{
    volatile UWORD *iec_reg;
    UWORD mask;

    iec_reg = &INT.IEC0 + ((int_src - 8) / 16);
    mask    = (int_src - 8) % 16;

    return __CSP_BIT_VAL_GET(*iec_reg, mask);
}

/* ********************************************************************************************** */
/* end of file: csp_int_chck.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_dis.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_int_dis (INT_SOURCE int_src)
{
    volatile UWORD *iec_reg;
    UWORD mask;

    iec_reg = &INT.IEC0 + ((int_src - 8) / 16);
    mask    = (int_src - 8) % 16;

    __CSP_BIT_VAL_CLR(*iec_reg, mask);
}

/* ********************************************************************************************** */
/* end of file: csp_int_dis.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_en.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_int_en (INT_SOURCE int_src)
{
    volatile UWORD *iec_reg;
    UWORD mask;

    iec_reg = &INT.IEC0 + ((int_src - 8) / 16);
    mask    = (int_src - 8) % 16;

    __CSP_BIT_VAL_SET(*iec_reg, mask);
}

/* ********************************************************************************************** */
/* end of file: csp_int_en.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_exgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC INT_EXT_POL  csp_int_ext_get (INT_EXT ch)
{
    return (INT_EXT_POL)__CSP_BIT_REG_GET(INT.INTCON2, ch);
}

/* ********************************************************************************************** */
/* end of file: csp_int_exgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_exst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_int_ext_set (INT_EXT ch, INT_EXT_POL pol)
{
    __CSP_ACC_RANG_REG(BFA_WR, INT.INTCON2, ch, ch, pol);
}

/* ********************************************************************************************** */
/* end of file: csp_int_exst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_flck.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_int_flag_check (INT_SOURCE int_src)
{
    volatile UWORD *ifs_reg;
    UWORD mask;

    ifs_reg = &INT.IFS0 + ((int_src - 8) / 16);
    mask    = (int_src - 8) % 16;

    return __CSP_BIT_VAL_GET(*ifs_reg, mask);
}

/* ********************************************************************************************** */
/* end of file: csp_int_flck.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_flcl.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_int_flag_clr (INT_SOURCE int_src)
{
    volatile UWORD *ifs_reg;
    UWORD mask;

    ifs_reg = &INT.IFS0 + ((int_src - 8) / 16);
    mask    = (int_src - 8) % 16;

    __CSP_BIT_VAL_CLR(*ifs_reg, mask);
}

/* ********************************************************************************************** */
/* end of file: csp_int_flcl.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_flst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_int_flag_set (INT_SOURCE int_src)
{
    volatile UWORD *ifs_reg;
    UWORD mask;

    ifs_reg = &INT.IFS0 + ((int_src - 8) / 16);
    mask    = (int_src - 8) % 16;

    __CSP_BIT_VAL_SET(*ifs_reg, mask);
}

/* ********************************************************************************************** */
/* end of file: csp_int_flst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_prgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC INT_PRI  csp_int_priority_get (INT_SOURCE int_src)
{
    volatile UWORD *ipc_reg;
    UWORD sh;

    ipc_reg = &INT.IPC0 + ((int_src - 8) / 4);
    sh      = ((int_src - 8) % 4) * 4;

    return __CSP_ACC_MASK_IND(BFA_RD, ipc_reg, (__INT_PRI_MASK << sh)) >> sh;
}

/* ********************************************************************************************** */
/* end of file: csp_int_prgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_prst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_int_priority_set (INT_SOURCE int_src, INT_PRI ipl)
{
    volatile UWORD *ipc_reg;
    UWORD sh;

    ipc_reg = &INT.IPC0 + ((int_src - 8) / 4);
    sh      = ((int_src - 8) % 4) * 4;
    
    __CSP_ACC_MASK_IND(BFA_WR, ipc_reg, (__INT_PRI_MASK << sh), (ipl << sh));
}

/* ********************************************************************************************** */
/* end of file: csp_int_prs t.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_sget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)
__CSP_FUNC UWORD csp_int_stat_get (void)
{
    return (INT.INTTREG & __INT_STAT_MASK);
}
#endif

/* ********************************************************************************************** */
/* end of file: csp_int_sget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_tfck.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC BOOL  csp_int_trap_flag_check (INT_TRAP int_trap)
{
    volatile UWORD *ifs_reg;
    UWORD mask;

    ifs_reg = &INT.INTCON1 + (int_trap / 16);
    mask    = int_trap % 16;

    return __CSP_BIT_VAL_GET(*ifs_reg, mask);
}

/* ********************************************************************************************** */
/* end of file: csp_int_tfck.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_tfcl.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_int_trap_flag_clr (INT_TRAP int_trap)
{
    volatile UWORD *ifs_reg;
    UWORD mask;

    ifs_reg = &INT.INTCON1 + (int_trap / 16);
    mask    = int_trap % 16;

    __CSP_BIT_VAL_CLR(*ifs_reg, mask);
}

/* ********************************************************************************************** */
/* end of file: csp_int_tfcl.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_tfst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_int_trap_flag_set (INT_TRAP int_trap)
{
    volatile UWORD *ifs_reg;
    UWORD mask;

    ifs_reg = &INT.INTCON1 + (int_trap / 16);
    mask    = int_trap % 16;

    __CSP_BIT_VAL_SET(*ifs_reg, mask);
}

/* ********************************************************************************************** */
/* end of file: csp_int_tfst.c */
