/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_pps_ingt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)      \
    )

__CSP_FUNC PPS_PIN  csp_pps_inp_get (PPS_INP val)
{
    volatile UWORD *reg;
             UWORD  sh;

    reg = &PPS.RPINR0 + (val >> 1);
    sh  = 8 * (val & 1);

    return (PPS_PIN)__CSP_ACC_MASK_IND(BFA_RD, reg, (__PPS_INP_MASK << sh)) >> sh;
}

#endif




/* ********************************************************************************************** */
/* end of file: csp_pps_ingt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_pps_inst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)      \
    )

    __CSP_FUNC void  csp_pps_inp_set (PPS_INP val, PPS_PIN pin)
    {
        volatile UWORD *reg;
        UWORD sh;

        reg  = &PPS.RPINR0 + (val >> 1);
        sh   = (val & 1) * 8;

        __CSP_ACC_MASK_IND(BFA_WR, reg, (__PPS_INP_MASK << sh), (pin << sh));
    }

#endif




/* ********************************************************************************************** */
/* end of file: csp_pps_inst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_pps_lock.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)      \
    )

void _csp_pps_oscconl_write (U08 val);

__CSP_FUNC void  csp_pps_lock (void)
{
    _csp_pps_oscconl_write((U08)__CSP_ACC_RANG_REG(BFA_RD, OSC.OSCCON, __OSC_L_FS, __OSC_L_FE) | __PPS_LOCK);
}

#endif




/* ********************************************************************************************** */
/* end of file: csp_pps_lock.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_pps_ougt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)      \
    )

__CSP_FUNC PPS_OUT  csp_pps_out_get (PPS_PIN pin)
{
    volatile UWORD *reg;
    UWORD sh;

    reg = &PPS.RP0R0 + (pin >> 1);
    sh  = 8 * (pin & 1);

    return (PPS_OUT)__CSP_ACC_MASK_IND(BFA_RD, reg, (__PPS_OUT_MASK << sh)) >> sh;
}

#endif




/* ********************************************************************************************** */
/* end of file: csp_pps_ougt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_pps_oust.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)      \
    )

__CSP_FUNC void  csp_pps_out_set (PPS_OUT val, PPS_PIN pin)
{
    volatile UWORD *reg;
             UWORD sh;

    reg = &PPS.RP0R0 + (pin >> 1);
    sh  = 8 * (pin & 1);

    __CSP_ACC_MASK_IND(BFA_WR, reg, (__PPS_OUT_MASK << sh), (val << sh));
}

#endif




/* ********************************************************************************************** */
/* end of file: csp_pps_oust.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_pps_unlk.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)      \
    )

void _csp_pps_oscconl_write (U08 val);

__CSP_FUNC void  csp_pps_unlock (void)
{
    _csp_pps_oscconl_write((U08)__CSP_ACC_RANG_REG(BFA_RD, OSC.OSCCON, __OSC_L_FS, __OSC_L_FE) & ~__PPS_LOCK);
}

#endif




/* ********************************************************************************************** */
/* end of file: csp_pps_unlk.c */
