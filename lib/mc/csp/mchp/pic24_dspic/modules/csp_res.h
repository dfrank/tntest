/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_res.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_RES_H
#define __CSP_RES_H


#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||   \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)        \
    )

    /* Reset programming model */
    /* ----------------------- */

    typedef struct __RES_TYPE
    {
        volatile UWORD RCON;
    } RES_TYPE;

    /* Reset base address */
    /* ------------------ */

    #define RES_BASE_ADDR    0x0740

    /* Declare reset model */
    /* ------------------- */

    extern  RES_TYPE RES __CSP_SFR(RES_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||   \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||   \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||   \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)      \
        )
    #define __RES_CONF_MASK             (RES_VREG_SLEEP_EN | RES_WDT_EN)
    #elif (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)
    #define __RES_CONF_MASK             (RES_VREG_SLEEP_EN | RES_WDT_EN | RES_BOR_EN)
    #endif

    #define __RES_CAUSE_MASK            (~(__RES_CONF_MASK))

    #define __RES_WDT_EN_BIT            (5)

    /* Definitions for reset API */
    /* ------------------------- */

    /* csp_res_stat_get() and csp_res_stat_clr() parameters type */

    typedef enum __RES_CAUSE
    {
        RES_CAUSE_TRAP          = (1 << 15),   /* RCON<15>, r/w hs */
        RES_CAUSE_IOP_UNW       = (1 << 14),   /* RCON<14>, r/w hs */
        #if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)
        RES_CAUSE_DEEP_SLEEP    = (1 << 10),   /* RCON<10>, r/c hs */
        #endif
        #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||   \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||   \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||   \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)      \
            )
        RES_CAUSE_CONF_MISMATSH = (1 <<  9),   /* RCON<9>,  r/w hs */
        #endif
        RES_CAUSE_MCLR          = (1 <<  7),   /* RCON<7>,  r/w hs */
        RES_CAUSE_SOFTWARE      = (1 <<  6),   /* RCON<6>,  r/w hs */
        RES_CAUSE_WDT           = (1 <<  4),   /* RCON<4>,  r/w hs */
        RES_CAUSE_BOR           = (1 <<  1),   /* RCON<1>,  r/w hs */
        RES_CAUSE_POR           = (1 <<  0),   /* RCON<0>,  r/w hs */

        RES_CAUSE_SLEEP         = (1 <<  3),   /* RCON<3>,  r/w hs */
        RES_CAUSE_IDLE          = (1 <<  2)    /* RCON<2>,  r/w hs */

    } RES_CAUSE;

    /* csp_res_conf_set() and csp_res_conf_get() parameters mask */

    typedef enum __RES_CONF
    {
        RES_VREG_SLEEP_EN  = (1 <<  8),  /* RCON<8> */
        RES_VREG_SLEEP_DIS = (0),

        RES_WDT_EN         = (1 <<  5),  /* RCON<5> */
        RES_WDT_DIS        = (0),

        #if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)
        RES_BOR_EN         = (1 << 13),
        RES_BOR_DIS        = (0),
        #endif

    } RES_CONF;

#endif


/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_RES)

        RES_CAUSE csp_res_cause_get(void);
        void      csp_res_cause_clr(RES_CAUSE mask);

        void     csp_res_conf_set(RES_CONF val);
        RES_CONF csp_res_conf_get(void);

        void     csp_res_wdt_clr(void);
        void     csp_res_wdt_en(void);
        void     csp_res_wdt_dis(void);

        void     csp_res_reset(void);

    #else

        #include "csp_res.c"

    #endif

#endif

#endif /* #ifndef __CSP_RES_H */
/* ********************************************************************************************** */
/* end of file: csp_res.h */
