/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_i2c.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_I2C_H
#define __CSP_I2C_H

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    /* I2C programming model */
    /* --------------------- */

    typedef struct __I2C_TYPE
    {
        volatile U08   I2CRCV;
        __CSP_SKIP_BYTE(1);
        volatile U08   I2CTRN;
        __CSP_SKIP_BYTE(1);
        volatile UWORD I2CBRG;
        volatile UWORD I2CCON;
        volatile UWORD I2CSTAT;
        volatile UWORD I2CADD;
        volatile UWORD I2CMSK;
    } I2C_TYPE;


    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
        )

        /* I2C base addresses */
        /* ------------------ */

        #define I2C1_BASE_ADDR      0x0200
        #define I2C2_BASE_ADDR      0x0210

        /* Declare I2C models */
        /* ------------------ */

        extern I2C_TYPE I2C1 __CSP_SFR(I2C1_BASE_ADDR);
        extern I2C_TYPE I2C2 __CSP_SFR(I2C2_BASE_ADDR);

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
          )

        /* I2C base addresses */
        /* ------------------ */

        #define I2C1_BASE_ADDR      0x0200
        #define I2C2_BASE_ADDR      0x0210
        #define I2C3_BASE_ADDR      0x0270

        /* Declare I2C models */
        /* ------------------ */

        extern I2C_TYPE I2C1 __CSP_SFR(I2C1_BASE_ADDR);
        extern I2C_TYPE I2C2 __CSP_SFR(I2C2_BASE_ADDR);
        extern I2C_TYPE I2C3 __CSP_SFR(I2C3_BASE_ADDR);

    #elif (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

        /* I2C base addresses */
        /* ------------------ */

        #define I2C1_BASE_ADDR      0x0200

        /* Declare I2C models */
        /* ------------------ */

        extern I2C_TYPE I2C1 __CSP_SFR(I2C1_BASE_ADDR);

    #endif

    /* Internal API definitions */
    /* ------------------------ */

    #define __I2C_SCL_RELEASE_BIT   (12)        /* I2CxCON<12> */
    #define __I2C_ACK_DIS_BIT       (5)         /* I2CxCON<5>  */

    /* Definitions for I2C API */
    /* ----------------------- */

    /* csp_i2c_conf_set() and csp_i2c_conf_get() parameters mask */

    #define I2C_EN                  (1 << 15)   /* Enables the I2C module   */
    #define I2C_DIS                 (0)         /* Disables the I2Cx module */

    #define I2C_IDLE_STOP           (1 << 13)   /* Discontinue module operation when device enters an Idle mode */
    #define I2C_IDLE_CON            (0)         /* Continue module operation in Idle mode */

    #define I2C_IPMI_EN             (1 << 11)   /* IPMI Support mode is enabled; all addresses Acknowledged */
    #define I2C_IPMI_DIS            (0)         /* IPMI Support mode disabled */

    #define I2C_ADDR_10B            (1 << 10)   /* I2CxADD is a 10-bit slave address */
    #define I2C_ADDR_7B             (0)         /* I2CxADD is a 7-bit slave address */

    #define I2C_SLEW_RATE_DIS       (1 <<  9)   /* Slew rate control disabled */
    #define I2C_SLEW_RATE_EN        (0)         /* Slew rate control enabled */

    #define I2C_SMBUS_EN            (1 <<  8)   /* Enable I/O pin thresholds compliant with SMBus specification */
    #define I2C_SMBUS_DIS           (0)         /* Disable SMBus input thresholds */

    #define I2C_GEN_CALL_EN         (1 <<  7)   /* Enable interrupt when a general call address is received */
    #define I2C_GEN_CALL_DIS        (0)         /* General call address disabled */

    #define I2C_CLK_STRETCH_EN      (1 <<  6)   /* Enable software or receive clock stretching */
    #define I2C_CLK_STRETCH_DIS     (0)         /* Disable software or receive clock stretching */

    /* csp_i2c_comm() parameter type */

    typedef enum __I2C_COMM
    {
        I2C_COMM_START    = (0),  /* Initiate Start condition            */
        I2C_COMM_RSTART   = (1),  /* Initiate Repeated Start condition   */
        I2C_COMM_STOP     = (2),  /* Initiate Stop condition             */
        I2C_COMM_RECEIVE  = (3),  /* Enables Receive mode for I2C        */
        I2C_COMM_ACK      = (4),  /* Initiate Acknowledge sequence       */

        I2C_COMM_ACK_DIS  = (5),  /* Send NACK during Acknowledge        */
        I2C_COMM_ACK_EN   = (6),  /* Send ACK during Acknowledge         */

        I2C_COMM_SCL_HOLD = (7),  /* Hold SCLx clock low (clock stretch) */
        I2C_COMM_SCL_REL  = (8)   /* Release SCLx clock                  */

    } I2C_COMM;

    /* csp_i2c_stat_get() and csp_i2c_stat_clr() parameters type */

    typedef enum __I2C_STAT
    {
        I2C_STAT_NACK           = (1 << 15),    /* RO  - NACK received from slave */
        I2C_STAT_TRANSMIT       = (1 << 14),    /* RO  - Master transmit is in progress (8 bits + ACK) */
        I2C_STAT_BUS_COLL       = (1 << 10),    /* R/C - A bus collision has been detected during a master operation */
        I2C_STAT_GEN_CALL       = (1 <<  9),    /* RO  - General call address was received */
        I2C_STAT_ADDR_10B_MATCH = (1 <<  8),    /* RO  - 10-bit address was matched */
        I2C_STAT_WRITE_COLL     = (1 <<  7),    /* R/C - An attempt to write the I2CxTRN register failed because the I2C module is busy */
        I2C_STAT_RC_OVF         = (1 <<  6),    /* R/C - A byte was received while the I2CxRCV register is still holding the previous byte */
        I2C_STAT_LAST_DATA      = (1 <<  5),    /* RO  - Indicates that the last byte received was data */
        I2C_STAT_STOP           = (1 <<  4),    /* R/C - Indicates that a Stop bit has been detected last */
        I2C_STAT_START          = (1 <<  3),    /* R/C - Indicates that a Start (or Repeated Start) bit has been detected last */
        I2C_STAT_SLAVE_READ     = (1 <<  2),    /* RO  - Read: data transfer is output from slave */
        I2C_STAT_RC_BUF_FULL    = (1 <<  1),    /* RO  - Receive complete, I2CxRCV is full */
        I2C_STAT_TR_BUF_FULL    = (1 <<  0)     /* RO  - Transmit in progress, I2CxTRN is full */

    } I2C_STAT;

#endif


/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_I2C)

        void     csp_i2c_conf_set(I2C_TYPE *i2c, UWORD val);
        UWORD    csp_i2c_conf_get(I2C_TYPE *i2c);

        void     csp_i2c_baud_set(I2C_TYPE *i2c, UWORD val);
        UWORD    csp_i2c_baud_get(I2C_TYPE *i2c);

        void     csp_i2c_comm(I2C_TYPE *i2c, I2C_COMM comm);

        I2C_STAT csp_i2c_stat_get(I2C_TYPE *i2c);
        void     csp_i2c_stat_clr(I2C_TYPE *i2c, I2C_STAT mask);

        void     csp_i2c_addr_set(I2C_TYPE *i2c, UWORD val);
        UWORD    csp_i2c_addr_get(I2C_TYPE *i2c);

        void     csp_i2c_addr_mask_set(I2C_TYPE *i2c, UWORD val);
        UWORD    csp_i2c_addr_mask_get(I2C_TYPE *i2c);

        void     csp_i2c_put(I2C_TYPE *i2c, U08 val);
        U08      csp_i2c_get(I2C_TYPE *i2c);

    #else

        #include "csp_i2c.c"

    #endif

#endif

#endif /* #ifndef __CSP_I2C_H */
/* ********************************************************************************************** */
/* end of file: csp_i2c.h */
