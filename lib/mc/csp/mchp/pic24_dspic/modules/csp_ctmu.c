/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ctmu_cget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    __CSP_FUNC CTMU_CONF csp_ctmu_conf_get (void)
    {
        return (CTMU.CTMUCON);
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_comp_cget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ctmu_comm.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    __CSP_FUNC void  csp_ctmu_comm (CTMU_COMM com)
    {
        if (com & CTMU_COMM_CHARGE) {
            __CSP_BIT_REG_CLR(CTMU.CTMUCON, __CTMU_DISCHARGE_BIT);
            return;
        }
        if (com & CTMU_COMM_DISCHARGE) {
            __CSP_BIT_REG_SET(CTMU.CTMUCON, __CTMU_DISCHARGE_BIT);
            return;
        }
        if (com & CTMU_COMM_EDGE_1_EN) {
            __CSP_BIT_REG_SET(CTMU.CTMUCON, __CTMU_EDGE_1_EN_BIT);
            return;
        }
        if (com & CTMU_COMM_EDGE_1_DIS) {
            __CSP_BIT_REG_CLR(CTMU.CTMUCON, __CTMU_EDGE_1_EN_BIT);
            return;
        }
        if (com & CTMU_COMM_EDGE_2_EN) {
            __CSP_BIT_REG_SET(CTMU.CTMUCON, __CTMU_EDGE_2_EN_BIT);
            return;
        }
        if (com & CTMU_COMM_EDGE_2_DIS) {
            __CSP_BIT_REG_CLR(CTMU.CTMUCON, __CTMU_EDGE_2_EN_BIT);
            return;
        }
    }

#endif




/* ********************************************************************************************** */
/* end of file: csp_comp_comm.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ctmu_cset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    __CSP_FUNC void csp_ctmu_conf_set (CTMU_CONF val)
    {
        CTMU.CTMUCON = val;
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_comp_cset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ctmu_cugt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    __CSP_FUNC CTMU_CUR  csp_ctmu_cur_get (UWORD *val)
    {
        *val = __CSP_ACC_RANG_REG(BFA_RD, CTMU.CTMUICON, __CTMU_CUR_FS, __CTMU_CUR_FE);
        return (CTMU_CUR)__CSP_ACC_RANG_REG(BFA_RD, CTMU.CTMUICON, __CTMU_CUR_CONF_FS, __CTMU_CUR_CONF_FE);
    }

#endif




/* ********************************************************************************************** */
/* end of file: csp_comp_cugt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ctmu_cust.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    __CSP_FUNC void  csp_ctmu_cur_set (CTMU_CUR conf, UWORD val)
    {
        __CSP_ACC_RANG_REG(BFA_WR, CTMU.CTMUICON, __CTMU_CUR_CONF_FS, __CTMU_CUR_CONF_FE, conf);
        __CSP_ACC_RANG_REG(BFA_WR, CTMU.CTMUICON, __CTMU_CUR_FS,      __CTMU_CUR_FE,      val);
    }

#endif




/* ********************************************************************************************** */
/* end of file: csp_comp_cust.c */
