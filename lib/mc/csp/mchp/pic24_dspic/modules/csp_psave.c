/* *************************************************************************************************
     Project:         dsPIC33 Family CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     dsPIC33 Family Peripheral Support Library
     ----------------------------------------
     Copyright � 2008 Alex B.

   *************************************************************************************************
     MCU Family:      dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_psave_dscg.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif
#if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

    __CSP_FUNC PSAVE_DS_CAUSE csp_psave_ds_cause_get (void)
    {
        return DSLEEP.DSWSRC;
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_psave_dscg.c */

/* *************************************************************************************************
     Project:         dsPIC33 Family CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     dsPIC33 Family Peripheral Support Library
     ----------------------------------------
     Copyright � 2008 Alex B.

   *************************************************************************************************
     MCU Family:      dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_psave_dsex.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif
#if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

    __CSP_FUNC U16 csp_psave_ds_exit (U16 *p1)
    {
        *p1 = DSLEEP.DSGPR1;
        return DSLEEP.DSGPR0;
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_psave_dsex.c */

/* *************************************************************************************************
     Project:         dsPIC33 Family CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     dsPIC33 Family Peripheral Support Library
     ----------------------------------------
     Copyright � 2008 Alex B.

   *************************************************************************************************
     MCU Family:      dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_psave_dslp.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif
#if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

    __CSP_FUNC void  csp_psave_ds_enter (U16 p0, U16 p1)
    {
        DSLEEP.DSGPR0 = p0;
        DSLEEP.DSGPR1 = p1;

        __CSP_BIT_REG_SET(DSLEEP.DSCON, __PSAVE_DSLEEP_EN_BIT);
        asm volatile ("pwrsav #0");
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_psave_dslp.c */

/* *************************************************************************************************
     Project:         dsPIC33 Family CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     dsPIC33 Family Peripheral Support Library
     ----------------------------------------
     Copyright � 2008 Alex B.

   *************************************************************************************************
     MCU Family:      dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_psave_dsri.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif
#if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

    __CSP_FUNC void csp_psave_ds_release_io(void)
    {
        __CSP_BIT_REG_CLR(DSLEEP.DSCON, __PSAVE_DSLEEP_RELEASE_BIT);
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_psave_dsri.c */
/* *************************************************************************************************
     Project:         dsPIC33 Family CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     dsPIC33 Family Peripheral Support Library
     ----------------------------------------
     Copyright � 2008 Alex B.

   *************************************************************************************************
     MCU Family:      dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_psave_idle.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void csp_psave_idle (void)
{
    asm volatile ("pwrsav #1");
}

/* ********************************************************************************************** */
/* end of file: csp_psave_idle.c */

/* *************************************************************************************************
     Project:         dsPIC33 Family CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     dsPIC33 Family Peripheral Support Library
     ----------------------------------------
     Copyright � 2008 Alex B.

   *************************************************************************************************
     MCU Family:      dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_psave_slep.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_psave_sleep (void)
{
    asm volatile ("pwrsav #0");
}

/* ********************************************************************************************** */
/* end of file: csp_psave_slep.c */

