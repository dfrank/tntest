/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_pmd_dis.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_pmd_dis (PMD_PER pmd)
{
    volatile UWORD *pmd_reg;
    UWORD mask;

    pmd_reg = &PMD.PMD1 + (pmd) / 16;
    mask    = (pmd) % 16;

    __CSP_BIT_VAL_CLR(*pmd_reg, mask);
}




/* ********************************************************************************************** */
/* end of file: csp_pmd_dis.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_pmd_en.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_pmd_en (PMD_PER pmd)
{
    volatile UWORD *pmd_reg;
    UWORD mask;

    pmd_reg = &PMD.PMD1 + (pmd) / 16;
    mask    = (pmd) % 16;

    __CSP_BIT_VAL_SET(*pmd_reg, mask);
}




/* ********************************************************************************************** */
/* end of file: csp_pmd_en.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_pmd_get.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC U64  csp_pmd_get (PMD_PER pmd)
{
    volatile UWORD *pmd_reg;
    UWORD mask;

    pmd_reg = &PMD.PMD1 + (pmd) / 16;
    mask    = (pmd) % 16;

    return __CSP_BIT_VAL_GET(*pmd_reg, mask);
}

/* ********************************************************************************************** */
/* end of file: csp_pmd_get.c */
