/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_hlvd.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_HLVD_H
#define __CSP_HLVD_H


#if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

    /* Reset programming model */
    /* ----------------------- */

    typedef struct __HLVD_TYPE
    {
        volatile UWORD CON;
    } HLVD_TYPE;

    /* Reset base address */
    /* ------------------ */

    #define HLVD_BASE_ADDR    0x0756

    /* Declare reset model */
    /* ------------------- */

    extern  HLVD_TYPE HLVD __CSP_SFR(HLVD_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    

    /* Definitions for reset API */
    /* ------------------------- */

    /* csp_hlvd_conf_set() and csp_hlvd_conf_get() parameters mask */

    typedef enum __HLVD_STAT
    {
        HLVD_VBG_STABLE     = (1 << 5),
        HLVD_REF_STABLE     = (1 << 4)
    } HLVD_STAT;

    typedef enum __HLVD_CONF
    {
        HLVD_EN             = (1 << 15),
        HLVD_DIS            = (0),

        HLVD_IDLE_STOP      = (1 << 13),
        HLVD_IDLE_CON       = (0),

        HLVD_CAUSE_ABOVE    = (1 << 7),
        HLVD_CAUSE_BELOW    = (0),

        /* For actual trip point, refer to Section29.0 �Electrical Characteristics�. */
        HLVD_TP_15          = (0),
        HLVD_TP_14          = (1),
        HLVD_TP_13          = (2),
        HLVD_TP_12          = (3),
        HLVD_TP_11          = (4),
        HLVD_TP_10          = (5),
        HLVD_TP_9           = (6),
        HLVD_TP_8           = (7),
        HLVD_TP_7           = (8),
        HLVD_TP_6           = (9),
        HLVD_TP_5           = (10),
        HLVD_TP_4           = (11),
        HLVD_TP_3           = (12),
        HLVD_TP_2           = (13),
        HLVD_TP_1           = (14),
        HLVD_TP_0           = (15)
    } HLVD_CONF;

#endif

/* External functions prototypes */
/* ----------------------------- */
#if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

    #if !defined(__CSP_BUILD_LIB)
    
        /* Module API definition */
    
        #if defined(__CSP_DEBUG_HLVD)
            void        csp_hlvd_conf_set(HLVD_CONF val);
            HLVD_CONF   csp_hlvd_conf_get(void);
        #else
            #include "csp_hlvd.c"
        #endif
    
    #endif
#endif

#endif /* #ifndef __CSP_HLVD_H */
/* ********************************************************************************************** */
/* end of file: csp_hlvd.h */

