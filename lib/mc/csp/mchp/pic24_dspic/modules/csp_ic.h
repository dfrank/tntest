/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ic.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_IC_H
#define __CSP_IC_H

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    /* Input capture programming model */
    /* ------------------------------- */

    typedef struct __IC_TYPE
    {
        volatile UWORD ICBUF;
        volatile UWORD ICCON;
    } IC_TYPE;

    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
        )
    
        /* Input capture base addresses */
        /* ---------------------------- */
    
        #define IC1_BASE_ADDR       0x0140
        #define IC2_BASE_ADDR       0x0144
        #define IC3_BASE_ADDR       0x0148
        #define IC4_BASE_ADDR       0x014C
        #define IC5_BASE_ADDR       0x0150
    
        /* Declare Input Capture models */
        /* ---------------------------- */
    
        extern IC_TYPE IC1 __CSP_SFR(IC1_BASE_ADDR);
        extern IC_TYPE IC2 __CSP_SFR(IC2_BASE_ADDR);
        extern IC_TYPE IC3 __CSP_SFR(IC3_BASE_ADDR);
        extern IC_TYPE IC4 __CSP_SFR(IC4_BASE_ADDR);
        extern IC_TYPE IC5 __CSP_SFR(IC5_BASE_ADDR);

    #elif (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

        /* Input capture base addresses */
        /* ---------------------------- */
    
        #define IC1_BASE_ADDR       0x0140
    
        /* Declare Input Capture models */
        /* ---------------------------- */
    
        extern IC_TYPE IC1 __CSP_SFR(IC1_BASE_ADDR);

    #endif

    /* Internal API definitions */
    /* ------------------------ */

    /* Definitions for IC API */
    /* ---------------------- */

    /* csp_ic_conf_set() and csp_ic_conf_get() parameters type */

    #define IC_IDLE_STOP                (1 << 13)   /* Input capture will halt in CPU Idle mode                */
    #define IC_IDLE_CON                 (0)         /* Input capture will continue to operate in CPU Idle mode */

    #define IC_BASE_TMR2                (1 <<  7)   /* TMR2 contents are captured on capture event */
    #define IC_BASE_TMR3                (0)         /* TMR3 contents are captured on capture event */

    #define IC_INT_ON_1_EVENT           (0 <<  5)   /* Interrupt on every capture event        */
    #define IC_INT_ON_2_EVENT           (1 <<  5)   /* Interrupt on every second capture event */
    #define IC_INT_ON_3_EVENT           (2 <<  5)   /* Interrupt on every third capture event  */
    #define IC_INT_ON_4_EVENT           (3 <<  5)   /* Interrupt on every fourth capture event */

    #define IC_MODE_DIS                 (0 <<  0)   /* Input capture module turned off               */
    #define IC_MODE_1_RISE_FALL_EDGE    (1 <<  0)   /* Capture mode, every edge � rising and falling */
    #define IC_MODE_1_FALL_EDGE         (2 <<  0)   /* Capture mode, every falling edge              */
    #define IC_MODE_1_RISE_EDGE         (3 <<  0)   /* Capture mode, every rising edge               */
    #define IC_MODE_4_RISE_EDGE         (4 <<  0)   /* Capture mode, every 4th rising edge           */
    #define IC_MODE_16_RISE_EDGE        (5 <<  0)   /* Capture mode, every 16th rising edge          */
    #define IC_MODE_1_RISE_EDGE_IN_PWD  (7 <<  0)   /* Input capture functions as interrupt pin only
                                                       when device is in Sleep or Idle mode */

    /* csp_ic_stat_get() return type */

    typedef enum __IC_STAT
    {
        IC_STAT_BUF_OVF       = (1 <<  4),  /* Input capture overflow occurred - RO */
        IC_STAT_BUF_NOT_EMPTY = (1 <<  3)   /* Input capture buffer is not empty, at least one more capture value can be read - RO */
    } IC_STAT;


#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||    \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)       \
      )

    /* Input capture programming model */
    /* ------------------------------- */

    typedef struct __IC_TYPE
    {
        volatile UWORD ICCON1;
        volatile UWORD ICCON2;
        volatile UWORD ICBUF;
        volatile UWORD ICTMR;
    } IC_TYPE;

    /* Input capture base addresses */
    /* ---------------------------- */

    #define IC1_BASE_ADDR       0x0140
    #define IC2_BASE_ADDR       0x0148
    #define IC3_BASE_ADDR       0x0150
    #define IC4_BASE_ADDR       0x0158
    #define IC5_BASE_ADDR       0x0160
    #define IC6_BASE_ADDR       0x0168
    #define IC7_BASE_ADDR       0x0170
    #define IC8_BASE_ADDR       0x0178
    #define IC9_BASE_ADDR       0x0180

    extern IC_TYPE IC1 __CSP_SFR(IC1_BASE_ADDR);
    extern IC_TYPE IC2 __CSP_SFR(IC2_BASE_ADDR);
    extern IC_TYPE IC3 __CSP_SFR(IC3_BASE_ADDR);
    extern IC_TYPE IC4 __CSP_SFR(IC4_BASE_ADDR);
    extern IC_TYPE IC5 __CSP_SFR(IC5_BASE_ADDR);
    extern IC_TYPE IC6 __CSP_SFR(IC6_BASE_ADDR);
    extern IC_TYPE IC7 __CSP_SFR(IC7_BASE_ADDR);
    extern IC_TYPE IC8 __CSP_SFR(IC8_BASE_ADDR);
    extern IC_TYPE IC9 __CSP_SFR(IC9_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    #define IC_ADDR_BASE_DIFF           (IC2_BASE_ADDR - IC1_BASE_ADDR)

    /* Definitions for IC API */
    /* ---------------------- */

    /* csp_ic_conf_set() and csp_ic_conf_get() parameters type */

    #define IC_RES_16                   (0UL <<  24)
    #define IC_RES_32                   (1UL <<  24)

    #define IC_CON_MODE_TRIGGER         (1UL <<  23)
    #define IC_CON_MODE_SYNC            (0UL <<  23)

    #define IC_CON_NONE                 (0UL  << 16)
    #define IC_CON_OC1                  (1UL  << 16)
    #define IC_CON_OC2                  (2UL  << 16)
    #define IC_CON_OC3                  (3UL  << 16)
    #define IC_CON_OC4                  (4UL  << 16)
    #define IC_CON_OC5                  (5UL  << 16)
    #define IC_CON_OC6                  (6UL  << 16)
    #define IC_CON_OC7                  (7UL  << 16)
    #define IC_CON_OC8                  (8UL  << 16)
    #define IC_CON_OC9                  (9UL  << 16)
    #define IC_CON_IC5                  (10UL << 16)
    #define IC_CON_TMR1                 (11UL << 16)
    #define IC_CON_TMR2                 (12UL << 16)
    #define IC_CON_TMR3                 (13UL << 16)
    #define IC_CON_TMR4                 (14UL << 16)
    #define IC_CON_TMR5                 (15UL << 16)
    #define IC_CON_IC7                  (18UL << 16)
    #define IC_CON_IC8                  (19UL << 16)
    #define IC_CON_IC1                  (20UL << 16)
    #define IC_CON_IC2                  (21UL << 16)
    #define IC_CON_IC3                  (22UL << 16)
    #define IC_CON_IC4                  (23UL << 16)
    #define IC_CON_COMP1                (24UL << 16)
    #define IC_CON_COMP2                (25UL << 16)
    #define IC_CON_COMP3                (26UL << 16)
    #define IC_CON_AD                   (27UL << 16)
    #define IC_CON_CTMU                 (28UL << 16)
    #define IC_CON_IC6                  (29UL << 16)
    #define IC_CON_IC9                  (30UL << 16)


    #define IC_IDLE_STOP                (1 << 13)   /* Input capture will halt in CPU Idle mode                */
    #define IC_IDLE_CON                 (0)         /* Input capture will continue to operate in CPU Idle mode */

    #define IC_BASE_TCY                 (7 << 10)
    #define IC_BASE_TMR1                (4 << 10)
    #define IC_BASE_TMR2                (3 << 10)
    #define IC_BASE_TMR3                (2 << 10)
    #define IC_BASE_TMR4                (1 << 10)
    #define IC_BASE_TMR5                (0 << 10)

    #define IC_INT_ON_1_EVENT           (0 <<  5)   /* Interrupt on every capture event        */
    #define IC_INT_ON_2_EVENT           (1 <<  5)   /* Interrupt on every second capture event */
    #define IC_INT_ON_3_EVENT           (2 <<  5)   /* Interrupt on every third capture event  */
    #define IC_INT_ON_4_EVENT           (3 <<  5)   /* Interrupt on every fourth capture event */

    #define IC_MODE_DIS                 (0 <<  0)   /* Input capture module turned off               */
    #define IC_MODE_1_RISE_FALL_EDGE    (1 <<  0)   /* Capture mode, every edge � rising and falling */
    #define IC_MODE_1_FALL_EDGE         (2 <<  0)   /* Capture mode, every falling edge              */
    #define IC_MODE_1_RISE_EDGE         (3 <<  0)   /* Capture mode, every rising edge               */
    #define IC_MODE_4_RISE_EDGE         (4 <<  0)   /* Capture mode, every 4th rising edge           */
    #define IC_MODE_16_RISE_EDGE        (5 <<  0)   /* Capture mode, every 16th rising edge          */
    #define IC_MODE_1_RISE_EDGE_IN_PWD  (7 <<  0)   /* Input capture functions as interrupt pin only
                                                       when device is in Sleep or Idle mode */

    /* csp_ic_stat_get() return type */

    typedef enum __IC_STAT
    {
        IC_STAT_BUF_OVF       = (1 <<  4),  /* Input capture overflow occurred - RO */
        IC_STAT_BUF_NOT_EMPTY = (1 <<  3),  /* Input capture buffer is not empty, at least one more capture value can be read - RO */
        IC_STAT_RUNNING       = (1 <<  6)   /* Timer source has been triggered and is running (set in hardware, can be set in software) */
    } IC_STAT;

#endif

/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_IC)

        IC_STAT csp_ic_stat_get(IC_TYPE *ic);
        UWORD   csp_ic_get(IC_TYPE *ic);


        #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
            )

            void    csp_ic_conf_set(IC_TYPE *ic, UWORD val);
            UWORD   csp_ic_conf_get(IC_TYPE *ic);


        #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
               (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
              )

            void    csp_ic_conf_set(IC_TYPE *ic, U32 val);
            U32     csp_ic_conf_get(IC_TYPE *ic);

            U32     csp_ic_32b_get(IC_TYPE *ic);

            void    csp_ic_tmr_set(IC_TYPE *ic, UWORD val);
            UWORD   csp_ic_tmr_get(IC_TYPE *ic);

            void    csp_ic_tmr_32b_set(IC_TYPE *ic, U32 val);
            U32     csp_ic_tmr_32b_get(IC_TYPE *ic);

        #endif

    #else

        #include "csp_ic.c"

    #endif

#endif

#endif /* #ifndef __CSP_IC_H */
/* ********************************************************************************************** */
/* end of file: csp_ic.h */
