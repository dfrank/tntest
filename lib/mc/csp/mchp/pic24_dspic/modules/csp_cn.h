/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_cn.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_CN_H
#define __CSP_CN_H



#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
    )

    /* CN programming model */
    /* -------------------- */

    typedef struct __CN_TYPE
    {
        volatile UWORD CNEN;
        __CSP_SKIP_BYTE(6);
        volatile UWORD CNPU;
    } CN_TYPE;

    /* CN base address */
    /* --------------- */

    #define CN1_BASE_ADDR       0x0060
    #define CN2_BASE_ADDR       0x0062

    /* Declare CN model */
    /* ---------------- */

    extern CN_TYPE CN1  __CSP_SFR(CN1_BASE_ADDR);
    extern CN_TYPE CN2  __CSP_SFR(CN2_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    /* Definitions for CN API */
    /* ---------------------- */

    /* Most PPS API functions parameters type */

    typedef enum __CN_PIN
    {
        CN_PIN0  = (1 <<  0),
        CN_PIN1  = (1 <<  1),
        CN_PIN2  = (1 <<  2),
        CN_PIN3  = (1 <<  3),
        CN_PIN4  = (1 <<  4),
        CN_PIN5  = (1 <<  5),
        CN_PIN6  = (1 <<  6),
        CN_PIN7  = (1 <<  7),
        CN_PIN8  = (1 <<  8),
        CN_PIN9  = (1 <<  9),
        CN_PIN10 = (1 << 10),
        CN_PIN11 = (1 << 11),
        CN_PIN12 = (1 << 12),
        CN_PIN13 = (1 << 13),
        CN_PIN14 = (1 << 14),
        CN_PIN15 = (1 << 15)
    } CN_PIN;

    #define CN_PIN_ALL      (0xFFFF)


#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||    \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)       \
      )

    /* CN programming model */
    /* -------------------- */

    typedef struct __CN_TYPE
    {
        volatile UWORD CNPD;
        __CSP_SKIP_BYTE(10);
        volatile UWORD CNEN;
        __CSP_SKIP_BYTE(10);
        volatile UWORD CNPU;
    } CN_TYPE;

    /* CN base address */
    /* --------------- */

    #define CN1_BASE_ADDR       0x0054
    #define CN2_BASE_ADDR       0x0056
    #define CN3_BASE_ADDR       0x0058
    #define CN4_BASE_ADDR       0x005A
    #define CN5_BASE_ADDR       0x005C
    #define CN6_BASE_ADDR       0x005E

    /* Declare CN model */
    /* ---------------- */

    extern CN_TYPE CN1  __CSP_SFR(CN1_BASE_ADDR);
    extern CN_TYPE CN2  __CSP_SFR(CN2_BASE_ADDR);
    extern CN_TYPE CN3  __CSP_SFR(CN3_BASE_ADDR);
    extern CN_TYPE CN4  __CSP_SFR(CN4_BASE_ADDR);
    extern CN_TYPE CN5  __CSP_SFR(CN5_BASE_ADDR);
    extern CN_TYPE CN6  __CSP_SFR(CN6_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    /* Definitions for CN API */
    /* ---------------------- */

    /* Most PPS API functions parameters type */

    typedef enum __CN_PIN
    {
        CN_PIN0  = (1 <<  0),
        CN_PIN1  = (1 <<  1),
        CN_PIN2  = (1 <<  2),
        CN_PIN3  = (1 <<  3),
        CN_PIN4  = (1 <<  4),
        CN_PIN5  = (1 <<  5),
        CN_PIN6  = (1 <<  6),
        CN_PIN7  = (1 <<  7),
        CN_PIN8  = (1 <<  8),
        CN_PIN9  = (1 <<  9),
        CN_PIN10 = (1 << 10),
        CN_PIN11 = (1 << 11),
        CN_PIN12 = (1 << 12),
        CN_PIN13 = (1 << 13),
        CN_PIN14 = (1 << 14),
        CN_PIN15 = (1 << 15)
    } CN_PIN;

    #define CN_PIN_ALL      (0xFFFF)


#elif (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

    /* CN programming model */
    /* -------------------- */

    typedef struct __CN_TYPE
    {
        volatile UWORD CNEN;
        __CSP_SKIP_BYTE(6);
        volatile UWORD CNPU;
        __CSP_SKIP_BYTE(6);
        volatile UWORD CNPD;
    } CN_TYPE;

    /* CN base address */
    /* --------------- */

    #define CN1_BASE_ADDR       0x0060
    #define CN2_BASE_ADDR       0x0062

    /* Declare CN model */
    /* ---------------- */

    extern CN_TYPE CN1  __CSP_SFR(CN1_BASE_ADDR);
    extern CN_TYPE CN2  __CSP_SFR(CN2_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    /* Definitions for CN API */
    /* ---------------------- */

    /* Most PPS API functions parameters type */

    typedef enum __CN_PIN
    {
        CN_PIN0  = (1 <<  0),
        CN_PIN1  = (1 <<  1),
        CN_PIN2  = (1 <<  2),
        CN_PIN3  = (1 <<  3),
        CN_PIN4  = (1 <<  4),
        CN_PIN5  = (1 <<  5),
        CN_PIN6  = (1 <<  6),
        CN_PIN7  = (1 <<  7),
        CN_PIN8  = (1 <<  8),
        CN_PIN9  = (1 <<  9),
        CN_PIN10 = (1 << 10),
        CN_PIN11 = (1 << 11),
        CN_PIN12 = (1 << 12),
        CN_PIN13 = (1 << 13),
        CN_PIN14 = (1 << 14),
        CN_PIN15 = (1 << 15)
    } CN_PIN;

    #define CN_PIN_ALL      (0xFFFF)


#else
    "CN module type wrong definition"
#endif


/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if   defined(__CSP_DEBUG_CN)

        void   csp_cn_en(CN_TYPE *cn, CN_PIN mask);
        void   csp_cn_dis(CN_TYPE *cn, CN_PIN mask);
        void   csp_cn_wr(CN_TYPE *cn, CN_PIN val);
        CN_PIN csp_cn_get(CN_TYPE *cn);

        void   csp_cn_pullup_en(CN_TYPE *cn, CN_PIN mask);
        void   csp_cn_pullup_dis(CN_TYPE *cn, CN_PIN mask);
        void   csp_cn_pullup_wr(CN_TYPE *cn, CN_PIN val);
        CN_PIN csp_cn_pullup_get(CN_TYPE *cn);

        #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) || \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)      \
            )

            void   csp_cn_pulldw_en(CN_TYPE *cn, CN_PIN mask);
            void   csp_cn_pulldw_dis(CN_TYPE *cn, CN_PIN mask);
            void   csp_cn_pulldw_wr(CN_TYPE *cn, CN_PIN val);
            CN_PIN csp_cn_pulldw_get(CN_TYPE *cn);

        #endif

    #else

        #include "csp_cn.c"

    #endif

#endif

#endif /* #ifndef __CSP_CN_H */
/* ********************************************************************************************** */
/* end of file: csp_cn.h */
