/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_cget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
    )

    __CSP_FUNC OSC_CONF csp_osc_conf_get (void)
    {
        return (__CSP_ACC_MASK_REG(BFA_RD, OSC.OSCCON, __OSC_CONF_MASK_OSCCON)  |
                __CSP_ACC_MASK_REG(BFA_RD, OSC.CLKDIV, __OSC_CONF_MASK_CLKDIV));
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_osc_cget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_cset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
    )

    __CSP_FUNC void csp_osc_conf_set (OSC_CONF val)
    {
        __CSP_ACC_MASK_REG(BFA_WR, OSC.OSCCON, __OSC_CONF_MASK_OSCCON, val);
        __CSP_ACC_MASK_REG(BFA_WR, OSC.CLKDIV, __OSC_CONF_MASK_CLKDIV, val);
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_osc_cset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_dcgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC OSC_DOZE_CONF csp_osc_doze_conf_get (void)
{
    return (OSC_DOZE_CONF)__CSP_ACC_MASK_REG(BFA_RD, OSC.CLKDIV, __OSC_DOZE_CONF_MASK);
}

/* ********************************************************************************************** */
/* end of file: csp_osc_dcgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_dcst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void csp_osc_doze_conf_set (OSC_DOZE_CONF val)
{
    __CSP_ACC_MASK_REG(BFA_WR, OSC.CLKDIV, __OSC_DOZE_CONF_MASK, val);
}

/* ********************************************************************************************** */
/* end of file: csp_osc_dcst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_dzgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC OSC_DOZE csp_osc_doze_get (void)
{
    return (OSC_DOZE)__CSP_ACC_RANG_REG(BFA_RD, OSC.CLKDIV, __OSC_DOZE_FS, __OSC_DOZE_FE);
}

/* ********************************************************************************************** */
/* end of file: csp_osc_dzgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_dzst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void csp_osc_doze_set (OSC_DOZE val)
{
    __CSP_ACC_RANG_REG(BFA_WR, OSC.CLKDIV, __OSC_DOZE_FS, __OSC_DOZE_FE, val);
}

/* ********************************************************************************************** */
/* end of file: csp_osc_dzst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_fcgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC U08 csp_osc_frc_cal_get (void)
{
    return (OSC.OSCTUN);
}

/* ********************************************************************************************** */
/* end of file: csp_osc_fcgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_fcst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void csp_osc_frc_cal_set (U08 val)
{
    OSC.OSCTUN = val;
}

/* ********************************************************************************************** */
/* end of file: csp_osc_fcst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_frgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC OSC_FRC_PS csp_osc_frc_get (void)
{
    return (OSC_FRC_PS)__CSP_ACC_RANG_REG(BFA_RD, OSC.CLKDIV, __OSC_FRC_PS_FS, __OSC_FRC_PS_FE);
}

/* ********************************************************************************************** */
/* end of file: csp_osc_frgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_frst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void csp_osc_frc_set (OSC_FRC_PS val)
{
    __CSP_ACC_RANG_REG(BFA_WR, OSC.CLKDIV, __OSC_FRC_PS_FS, __OSC_FRC_PS_FE, val);
}

/* ********************************************************************************************** */
/* end of file: csp_osc_frst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_get.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC OSC_MODE csp_osc_get (void)
{
    return  (OSC_MODE)__CSP_ACC_RANG_REG(BFA_RD, OSC.OSCCON, __OSC_MODE_FS, __OSC_MODE_FE);
}

/* ********************************************************************************************** */
/* end of file: csp_osc_get.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_lock.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

void _csp_osc_oscconl_write(U08 val);

__CSP_FUNC void  csp_osc_lock (void)
{
    _csp_osc_oscconl_write((U08)__CSP_ACC_RANG_REG(BFA_RD, OSC.OSCCON, __OSC_L_FS, __OSC_L_FE) | __OSC_COMM_LOCK);
}

/* ********************************************************************************************** */
/* end of file: csp_osc_lock.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_rcgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) || \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)      \
    )

    __CSP_FUNC OSC_REF_CONF csp_osc_ref_conf_get (void)
    {
        return (OSC.REFOCON);
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_osc_rcgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_rcst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) || \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)      \
    )

    __CSP_FUNC void csp_osc_ref_conf_set (OSC_REF_CONF val)
    {
        OSC.REFOCON = val;
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_osc_rcst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_schk.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_osc_sec_check (void)
{
    return __CSP_ACC_MASK_REG(BFA_RD, OSC.OSCCON, __OSC_SECONDARY_EN);
}





/* ********************************************************************************************** */
/* end of file: csp_osc_schk.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_sdis.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

void _csp_osc_oscconl_write(U08 val);

__CSP_FUNC void  csp_osc_sec_dis (void)
{
    _csp_osc_oscconl_write((U08)__CSP_ACC_RANG_REG(BFA_RD, OSC.OSCCON, __OSC_L_FS, __OSC_L_FE) & (~__OSC_SECONDARY_EN));
}




/* ********************************************************************************************** */
/* end of file: csp_osc_sdis.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_seen.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

void _csp_osc_oscconl_write(U08 val);

__CSP_FUNC void  csp_osc_sec_en (void)
{
    _csp_osc_oscconl_write((U08)__CSP_ACC_RANG_REG(BFA_RD, OSC.OSCCON, __OSC_L_FS, __OSC_L_FE) | __OSC_SECONDARY_EN);
}




/* ********************************************************************************************** */
/* end of file: csp_osc_seen.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_set.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

void _csp_osc_oscconh_write(U08 val);
void _csp_osc_oscconl_write(U08 val);

__CSP_FUNC void  csp_osc_set (OSC_MODE mode)
{
    _csp_osc_oscconh_write((U08)mode);
    _csp_osc_oscconl_write((U08)__CSP_ACC_RANG_REG(BFA_RD, OSC.OSCCON, __OSC_L_FS, __OSC_L_FE) | __OSC_COMM_SWITCH);

    while ((__CSP_BIT_REG_GET(OSC.OSCCON, __OSC_COMM_SWITCH_BIT)));
}

/* ********************************************************************************************** */
/* end of file: csp_osc_set.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_stcl.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

void _csp_osc_oscconl_write(U08 val);

__CSP_FUNC void  csp_osc_stat_clr (OSC_STAT mask)
{
    _csp_osc_oscconl_write((U08)__CSP_ACC_RANG_REG(BFA_RD, OSC.OSCCON, __OSC_L_FS, __OSC_L_FE) & (~mask));
}

/* ********************************************************************************************** */
/* end of file: csp_osc_stcl.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_stgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC OSC_STAT  csp_osc_stat_get (void)
{
    return (OSC_STAT)(OSC.OSCCON);
}




/* ********************************************************************************************** */
/* end of file: csp_osc_stgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_osc_unlk.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

void _csp_osc_oscconl_write(U08 val);

__CSP_FUNC void  csp_osc_unlock (void)
{
    _csp_osc_oscconl_write((U08)__CSP_ACC_RANG_REG(BFA_RD, OSC.OSCCON, __OSC_L_FS, __OSC_L_FE) & ~__OSC_COMM_LOCK);
}

/* ********************************************************************************************** */
/* end of file: csp_osc_unlk.c */
