/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_nvm.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_NVM_H
#define __CSP_NVM_H


#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    /* NVM programming model */
    /* --------------------- */

    typedef struct __NVM_TYPE
    {
        volatile UWORD NVMCON;
        __CSP_SKIP_BYTE(4);
        volatile U08   NVMKEY;
        __CSP_SKIP_BYTE(1);
    } NVM_TYPE;

    /* NVM base addresses */
    /* ------------------ */

    #define NVM_BASE_ADDR       0x0760

    /* Declare NVM model */
    /* ----------------- */

    extern  NVM_TYPE NVM __CSP_SFR(NVM_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    #define _NVM_WR_BIT         (1 << 15)

    /* Definitions for NVM API */
    /* ----------------------- */

    /* csp_nvm_comm() parameter type */

    typedef enum __NVM_CONF
    {
        #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
            )

            NVM_CONF_PRG_WORD           = 0x4003,   /* Programm word */
            NVM_CONF_PRG_ROW            = 0x4001,   /* Program row   */
            NVM_CONF_ERASE_PAGE         = 0x4042    /* Erase page    */

        #elif (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

            NVM_CONF_EE_ERASE_WR        = (0 << 12),   /* ONLY EE: Automatic erase-before-write: write operations are preceded automatically by an erase of target */
            NVM_CONF_EE_WR              = (1 << 12),   /* ONLY EE: Write operation is executed without erasing target address(es) first */
            
            NVM_CONF_EE_ERASE_BULK      = (0x4050),  /* Erase entire data EEPROM */
            NVM_CONF_EE_ERASE_1W        = (0x4058),  /* Erase 1 word  */
            NVM_CONF_EE_ERASE_4W        = (0x4059),  /* Erase 4 word  */
            NVM_CONF_EE_ERASE_8W        = (0x405A),  /* Erase 8 word  */
            NVM_CONF_EE_PRG_WORD        = (0x4004),  /* Write 1 word  */
    
            NVM_CONF_FL_ERASE_ALL_BULK  = (0x4064),  /* Erase entire boot block (including code-protected boot block) */
            NVM_CONF_FL_ERASE_BOOT_BULK = (0x4068),  /* Erase entire boot block (including code-protected boot block) */
            NVM_CONF_FL_ERASE_GEN_BULK  = (0x404C),  /* Erase entire general memory block programming operations */
            NVM_CONF_FL_ERASE_CON_BULK  = (0x4054),  /* Erase entire configuration block (except code protection bits) */
            NVM_CONF_FL_ERASE_1ROW      = (0x4058),  /* Erase 1 row of Flash memory */
            NVM_CONF_FL_ERASE_2ROW      = (0x4059),  /* Erase 2 row of Flash memory */
            NVM_CONF_FL_ERASE_4ROW      = (0x405A),  /* Erase 4 row of Flash memory */
            NVM_CONF_FL_PRG_1ROW        = (0x4004),  /* Write 1 row of Flash memory (when ERASE bit is �0�) */

        #endif

    } NVM_CONF;

    #define NVM_ADDR_UNUSED             (0)

    #if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)
        #define NVM_EE_ADDR_START       (0x7FFE00UL)
        #define NVM_EE_ADDR_END         (0x800000UL)
        #define NVM_EE_SIZE_BYTES       (NVM_EE_ADDR_END - NVM_EE_ADDR_START)
    #endif

#endif

/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    U32  csp_nvm_word_get(U32 addr);
    void csp_nvm_word_put(U32 addr, U32 data);
    void csp_nvm_comm(U32 addr, NVM_CONF comm);

    #if defined(__CSP_DEBUG_NVM)
        BOOL csp_nvm_busy_check(void);    
    #else
        #include "csp_nvm.c"

    #endif

#endif

#endif /* #ifndef __CSP_NVM_H */
/* ********************************************************************************************** */
/* end of file: csp_nvm.h */
