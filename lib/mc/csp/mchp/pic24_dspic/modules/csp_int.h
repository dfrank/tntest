/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_INT_H
#define __CSP_INT_H


#if (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010)

    /* Interrupt programming model */
    /* --------------------------- */

    typedef struct __INT_TYPE
    {
        volatile UWORD INTCON1;
        volatile UWORD INTCON2;

        volatile UWORD IFS0;
        volatile UWORD IFS1;
        volatile UWORD IFS2;
        volatile UWORD IFS3;
        volatile UWORD IFS4;
        __CSP_SKIP_BYTE(6);

        volatile UWORD IEC0;
        volatile UWORD IEC1;
        volatile UWORD IEC2;
        volatile UWORD IEC3;
        volatile UWORD IEC4;
        __CSP_SKIP_BYTE(6);

        volatile UWORD IPC0;
        volatile UWORD IPC1;
        volatile UWORD IPC2;
        volatile UWORD IPC3;
        volatile UWORD IPC4;
        volatile UWORD IPC5;
        volatile UWORD IPC6;
        volatile UWORD IPC7;
        volatile UWORD IPC8;
        volatile UWORD IPC9;
        volatile UWORD IPC10;
        volatile UWORD IPC11;
        volatile UWORD IPC12;
        volatile UWORD IPC13;
        __CSP_SKIP_BYTE(2);
        volatile UWORD IPC15;
        volatile UWORD IPC16;

    } INT_TYPE;

    /* Definitions for interrupt API */
    /* ----------------------------- */

    /* csp_int_ext_set() and csp_int_ext_set() parameters type */

    typedef enum __INT_EXT
    {
        EINT0,      /* external interrupt INT0 */
        EINT1,      /* external interrupt INT1 */
        EINT2,      /* external interrupt INT2 */
        EINT3,      /* external interrupt INT3 */
        EINT4       /* external interrupt INT4 */
    } INT_EXT;

    /* csp_int_en(), csp_int_dis() and other functions parameters type */

    typedef enum __INT_SOURCE
    {
        INT_INT0            =  8,       /* External Interrupt 0         */
        INT_IC1             =  9,       /* Input Capture 1              */
        INT_OC1             = 10,       /* Output Compare 1             */
        INT_TMR1            = 11,       /* Timer1                       */

        INT_IC2             = 13,       /* Input Capture 2              */
        INT_OC2             = 14,       /* Output Compare 2             */
        INT_TMR2            = 15,       /* Timer2                       */
        INT_TMR3            = 16,       /* Timer3                       */
        INT_SPI1_ERR        = 17,       /* SPI1 Error                   */
        INT_SPI1_EVENT      = 18,       /* SPI1 Event                   */
        INT_UART1_RX        = 19,       /* UART1 Receiver               */
        INT_UART1_TX        = 20,       /* UART1 Transmitter            */
        INT_ADC1            = 21,       /* ADC1 Conversion Done         */

        INT_I2C1_SLAVE      = 24,       /* I2C1 Slave Event             */
        INT_I2C1_MASTER     = 25,       /* I2C1 Master Event            */
        INT_COMP            = 26,       /* Comparator Event             */
        INT_CN              = 27,       /* Input Change Notification    */
        INT_INT1            = 28,       /* External Interrupt 1         */

        INT_OC3             = 33,       /* Output Compare 3             */
        INT_OC4             = 34,       /* Output Compare 3             */
        INT_TMR4            = 35,       /* Timer4                       */
        INT_TMR5            = 36,       /* Timer5                       */
        INT_INT2            = 37,       /* External Interrupt 2         */
        INT_UART2_RX        = 38,       /* UART2 Receiver               */
        INT_UART2_TX        = 39,       /* UART2 Transmitter            */
        INT_SPI2_ERR        = 40,       /* SPI2 Error                   */
        INT_SPI2_EVENT      = 41,       /* SPI2 Event                   */

        INT_IC3             = 45,       /* Input Capture 3              */
        INT_IC4             = 46,       /* Input Capture 4              */
        INT_IC5             = 47,       /* Input Capture 5              */

        INT_OC5             = 49,       /* Output Compare 5             */

        INT_PMP             = 53,       /* Parallel Master Port         */

        INT_I2C2_SLAVE      = 57,       /* I2C2 Slave Event             */
        INT_I2C2_MASTER     = 58,       /* I2C2 Master Event            */

        INT_INT3            = 61,       /* External Interrupt 3         */
        INT_INT4            = 62,       /* External Interrupt 4         */

        INT_RTCC            = 70,       /* Real-Time Clock/Calendar     */

        INT_UART1_ERR       = 73,       /* UART1 Error                  */
        INT_UART2_ERR       = 74,       /* UART2 Error                  */
        INT_CRC             = 75        /* CRC Generator                */

    } INT_SOURCE;

#elif (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

    /* Interrupt programming model */
    /* --------------------------- */

    typedef struct __INT_TYPE
    {
        volatile UWORD INTCON1;
        volatile UWORD INTCON2;

        volatile UWORD IFS0;
        volatile UWORD IFS1;
        __CSP_SKIP_BYTE(2);
        volatile UWORD IFS3;
        volatile UWORD IFS4;
        __CSP_SKIP_BYTE(6);

        volatile UWORD IEC0;
        volatile UWORD IEC1;
        __CSP_SKIP_BYTE(2);
        volatile UWORD IEC3;
        volatile UWORD IEC4;
        __CSP_SKIP_BYTE(6);

        volatile UWORD IPC0;
        volatile UWORD IPC1;
        volatile UWORD IPC2;
        volatile UWORD IPC3;
        volatile UWORD IPC4;
        volatile UWORD IPC5;
        __CSP_SKIP_BYTE(2);
        volatile UWORD IPC7;
        __CSP_SKIP_BYTE(14);
        volatile UWORD IPC15;
        volatile UWORD IPC16;
        __CSP_SKIP_BYTE(2);
        volatile UWORD IPC18;
        volatile UWORD IPC19;
        __CSP_SKIP_BYTE(20);
        volatile UWORD INTTREG;
    } INT_TYPE;

    /* Definitions for interrupt API */
    /* ----------------------------- */

    /* csp_int_ext_set() and csp_int_ext_set() parameters type */

    typedef enum __INT_EXT
    {
        EINT0,      /* external interrupt INT0 */
        EINT1,      /* external interrupt INT1 */
        EINT2,      /* external interrupt INT2 */
    } INT_EXT;

    /* csp_int_en(), csp_int_dis() and other functions parameters type */

    typedef enum __INT_SOURCE
    {
        INT_INT0            =  8,       /* External Interrupt 0         */
        INT_IC1             =  9,       /* Input Capture 1              */
        INT_OC1             = 10,       /* Output Compare 1             */
        INT_TMR1            = 11,       /* Timer1                       */

        INT_TMR2            = 15,       /* Timer2                       */
        INT_TMR3            = 16,       /* Timer3                       */
        INT_SPI1_ERR        = 17,       /* SPI1 Error                   */
        INT_SPI1_EVENT      = 18,       /* SPI1 Event                   */
        INT_UART1_RX        = 19,       /* UART1 Receiver               */
        INT_UART1_TX        = 20,       /* UART1 Transmitter            */
        INT_ADC1            = 21,       /* ADC1 Conversion Done         */

        INT_NVM             = 23,       /* NVM Write Complete           */

        INT_I2C1_SLAVE      = 24,       /* I2C1 Slave Event             */
        INT_I2C1_MASTER     = 25,       /* I2C1 Master Event            */
        INT_COMP            = 26,       /* Comparator Event             */
        INT_CN              = 27,       /* Input Change Notification    */
        INT_INT1            = 28,       /* External Interrupt 1         */

        INT_INT2            = 37,       /* External Interrupt 2         */
        INT_UART2_RX        = 38,       /* UART2 Receiver               */
        INT_UART2_TX        = 39,       /* UART2 Transmitter            */

        INT_RTCC            = 70,       /* Real-Time Clock/Calendar     */

        INT_UART1_ERR       = 73,       /* UART1 Error                  */
        INT_UART2_ERR       = 74,       /* UART2 Error                  */
        INT_CRC             = 75,       /* CRC Generator                */

        INT_LVD             = 80,       /* LVD Interrupt                */

        INT_CTMU            = 85,       /* CTMU module                  */

    } INT_SOURCE;    


#elif (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)

    /* Interrupt programming model */
    /* --------------------------- */

    typedef struct __INT_TYPE
    {
        volatile UWORD INTCON1;
        volatile UWORD INTCON2;

        volatile UWORD IFS0;
        volatile UWORD IFS1;
        volatile UWORD IFS2;
        volatile UWORD IFS3;
        volatile UWORD IFS4;
        __CSP_SKIP_BYTE(6);

        volatile UWORD IEC0;
        volatile UWORD IEC1;
        volatile UWORD IEC2;
        volatile UWORD IEC3;
        volatile UWORD IEC4;
        __CSP_SKIP_BYTE(6);

        volatile UWORD IPC0;
        volatile UWORD IPC1;
        volatile UWORD IPC2;
        volatile UWORD IPC3;
        volatile UWORD IPC4;
        volatile UWORD IPC5;
        volatile UWORD IPC6;
        volatile UWORD IPC7;
        volatile UWORD IPC8;
        volatile UWORD IPC9;
        volatile UWORD IPC10;
        volatile UWORD IPC11;
        volatile UWORD IPC12;
        __CSP_SKIP_BYTE(4);
        volatile UWORD IPC15;
        volatile UWORD IPC16;
        __CSP_SKIP_BYTE(2);
        volatile UWORD IPC18;

    } INT_TYPE;

    /* Definitions for interrupt API */
    /* ----------------------------- */

    /* csp_int_ext_set() and csp_int_ext_set() parameters type */

    typedef enum __INT_EXT
    {
        EINT0,      /* external interrupt INT0 */
        EINT1,      /* external interrupt INT1 */
        EINT2       /* external interrupt INT2 */
    } INT_EXT;

    /* csp_int_en(), csp_int_dis() and other functions parameters type */

    typedef enum __INT_SOURCE
    {
        INT_INT0            =  8,       /* External Interrupt 0         */
        INT_IC1             =  9,       /* Input Capture 1              */
        INT_OC1             = 10,       /* Output Compare 1             */
        INT_TMR1            = 11,       /* Timer1                       */

        INT_IC2             = 13,       /* Input Capture 2              */
        INT_OC2             = 14,       /* Output Compare 2             */
        INT_TMR2            = 15,       /* Timer2                       */
        INT_TMR3            = 16,       /* Timer3                       */
        INT_SPI1_ERR        = 17,       /* SPI1 Error                   */
        INT_SPI1_EVENT      = 18,       /* SPI1 Event                   */
        INT_UART1_RX        = 19,       /* UART1 Receiver               */
        INT_UART1_TX        = 20,       /* UART1 Transmitter            */
        INT_ADC1            = 21,       /* ADC1 Conversion Done         */

        INT_I2C1_SLAVE      = 24,       /* I2C1 Slave Event             */
        INT_I2C1_MASTER     = 25,       /* I2C1 Master Event            */
        INT_COMP            = 26,       /* Comparator Event             */
        INT_CN              = 27,       /* Input Change Notification    */
        INT_INT1            = 28,       /* External Interrupt 1         */

        INT_OC3             = 33,       /* Output Compare 3             */
        INT_OC4             = 34,       /* Output Compare 3             */
        INT_TMR4            = 35,       /* Timer4                       */
        INT_TMR5            = 36,       /* Timer5                       */
        INT_INT2            = 37,       /* External Interrupt 2         */
        INT_UART2_RX        = 38,       /* UART2 Receiver               */
        INT_UART2_TX        = 39,       /* UART2 Transmitter            */
        INT_SPI2_ERR        = 40,       /* SPI2 Error                   */
        INT_SPI2_EVENT      = 41,       /* SPI2 Event                   */

        INT_IC3             = 45,       /* Input Capture 3              */
        INT_IC4             = 46,       /* Input Capture 4              */
        INT_IC5             = 47,       /* Input Capture 5              */

        INT_OC5             = 49,       /* Output Compare 5             */

        INT_PMP             = 53,       /* Parallel Master Port         */

        INT_I2C2_SLAVE      = 57,       /* I2C2 Slave Event             */
        INT_I2C2_MASTER     = 58,       /* I2C2 Master Event            */

        INT_RTCC            = 70,       /* Real-Time Clock/Calendar     */

        INT_UART1_ERR       = 73,       /* UART1 Error                  */
        INT_UART2_ERR       = 74,       /* UART2 Error                  */
        INT_CRC             = 75,       /* CRC Generator                */

        INT_LVD             = 80        /* LVD Interrupt                */

    } INT_SOURCE;

#elif (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110)

    /* Interrupt programming model */
    /* --------------------------- */

    typedef struct __INT_TYPE
    {
        volatile UWORD INTCON1;
        volatile UWORD INTCON2;

        volatile UWORD IFS0;
        volatile UWORD IFS1;
        volatile UWORD IFS2;
        volatile UWORD IFS3;
        volatile UWORD IFS4;
        volatile UWORD IFS5;
        __CSP_SKIP_BYTE(4);

        volatile UWORD IEC0;
        volatile UWORD IEC1;
        volatile UWORD IEC2;
        volatile UWORD IEC3;
        volatile UWORD IEC4;
        volatile UWORD IEC5;
        __CSP_SKIP_BYTE(4);

        volatile UWORD IPC0;
        volatile UWORD IPC1;
        volatile UWORD IPC2;
        volatile UWORD IPC3;
        volatile UWORD IPC4;
        volatile UWORD IPC5;
        volatile UWORD IPC6;
        volatile UWORD IPC7;
        volatile UWORD IPC8;
        volatile UWORD IPC9;
        volatile UWORD IPC10;
        volatile UWORD IPC11;
        volatile UWORD IPC12;
        volatile UWORD IPC13;
        __CSP_SKIP_BYTE(2);
        volatile UWORD IPC15;
        volatile UWORD IPC16;
        __CSP_SKIP_BYTE(2);
        volatile UWORD IPC18;
        volatile UWORD IPC19;
        volatile UWORD IPC20;
        volatile UWORD IPC21;
        volatile UWORD IPC22;
        volatile UWORD IPC23;

    } INT_TYPE;

    /* Definitions for interrupt API */
    /* ----------------------------- */

    /* csp_int_ext_set() and csp_int_ext_set() parameters type */

    typedef enum __INT_EXT
    {
        EINT0,      /* external interrupt INT0 */
        EINT1,      /* external interrupt INT1 */
        EINT2,      /* external interrupt INT2 */
        EINT3,      /* external interrupt INT3 */
        EINT4       /* external interrupt INT4 */
    } INT_EXT;

    /* csp_int_en(), csp_int_dis() and other functions parameters type */

    typedef enum __INT_SOURCE
    {
        INT_INT0            =  8,       /* External Interrupt 0         */
        INT_IC1             =  9,       /* Input Capture 1              */
        INT_OC1             = 10,       /* Output Compare 1             */
        INT_TMR1            = 11,       /* Timer1                       */

        INT_IC2             = 13,       /* Input Capture 2              */
        INT_OC2             = 14,       /* Output Compare 2             */
        INT_TMR2            = 15,       /* Timer2                       */
        INT_TMR3            = 16,       /* Timer3                       */
        INT_SPI1_ERR        = 17,       /* SPI1 Error                   */
        INT_SPI1_EVENT      = 18,       /* SPI1 Event                   */
        INT_UART1_RX        = 19,       /* UART1 Receiver               */
        INT_UART1_TX        = 20,       /* UART1 Transmitter            */
        INT_ADC1            = 21,       /* ADC1 Conversion Done         */

        INT_I2C1_SLAVE      = 24,       /* I2C1 Slave Event             */
        INT_I2C1_MASTER     = 25,       /* I2C1 Master Event            */
        INT_COMP            = 26,       /* Comparator Event             */
        INT_CN              = 27,       /* Input Change Notification    */
        INT_INT1            = 28,       /* External Interrupt 1         */

        INT_IC7             = 30,       /* Input Capture 7              */
        INT_IC8             = 31,       /* Input Capture 8              */

        INT_OC3             = 33,       /* Output Compare 3             */
        INT_OC4             = 34,       /* Output Compare 4             */
        INT_TMR4            = 35,       /* Timer4                       */
        INT_TMR5            = 36,       /* Timer5                       */
        INT_INT2            = 37,       /* External Interrupt 2         */
        INT_UART2_RX        = 38,       /* UART2 Receiver               */
        INT_UART2_TX        = 39,       /* UART2 Transmitter            */
        INT_SPI2_ERR        = 40,       /* SPI2 Error                   */
        INT_SPI2_EVENT      = 41,       /* SPI2 Event                   */

        INT_IC3             = 45,       /* Input Capture 3              */
        INT_IC4             = 46,       /* Input Capture 4              */
        INT_IC5             = 47,       /* Input Capture 5              */
        INT_IC6             = 48,       /* Input Capture 6              */

        INT_OC5             = 49,       /* Output Compare 5             */
        INT_OC6             = 50,       /* Output Compare 6             */
        INT_OC7             = 51,       /* Output Compare 7             */
        INT_OC8             = 52,       /* Output Compare 8             */
        INT_PMP             = 53,       /* Parallel Master Port         */

        INT_I2C2_SLAVE      = 57,       /* I2C2 Slave Event             */
        INT_I2C2_MASTER     = 58,       /* I2C2 Master Event            */

        INT_INT3            = 61,       /* External Interrupt 3         */
        INT_INT4            = 62,       /* External Interrupt 4         */

        INT_RTCC            = 70,       /* Real-Time Clock/Calendar     */

        INT_UART1_ERR       = 73,       /* UART1 Error                  */
        INT_UART2_ERR       = 74,       /* UART2 Error                  */
        INT_CRC             = 75,       /* CRC Generator                */

        INT_LVD             = 80,       /* Low-Voltage detect           */

        INT_CTMU            = 85,       /* CTMU module                  */

        INT_UART3_ERR       = 89,       /* UART3 Error                  */
        INT_UART3_RX        = 90,       /* UART3 Receiver               */
        INT_UART3_TX        = 91,       /* UART3 Transmitter            */
        INT_I2C3_SLAVE      = 92,       /* I2C3 Slave Event             */
        INT_I2C3_MASTER     = 93,       /* I2C3 Master Event            */

        INT_UART4_ERR       = 95,       /* UART4 Error                  */
        INT_UART4_RX        = 96,       /* UART4 Receiver               */
        INT_UART4_TX        = 97,       /* UART4 Transmitter            */
        INT_SPI3_ERR        = 98,       /* SPI3 Error                   */
        INT_SPI3_EVENT      = 99,       /* SPI3 Event                   */
        INT_OC9             = 100,      /* Output Compare 9             */
        INT_IC9             = 101       /* Input Capture 9              */

    } INT_SOURCE;


#elif (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)

    /* Interrupt programming model */
    /* --------------------------- */

    typedef struct __INT_TYPE
    {
        volatile UWORD INTCON1;
        volatile UWORD INTCON2;

        volatile UWORD IFS0;
        volatile UWORD IFS1;
        volatile UWORD IFS2;
        volatile UWORD IFS3;
        volatile UWORD IFS4;
        volatile UWORD IFS5;
        __CSP_SKIP_BYTE(4);

        volatile UWORD IEC0;
        volatile UWORD IEC1;
        volatile UWORD IEC2;
        volatile UWORD IEC3;
        volatile UWORD IEC4;
        volatile UWORD IEC5;
        __CSP_SKIP_BYTE(4);

        volatile UWORD IPC0;
        volatile UWORD IPC1;
        volatile UWORD IPC2;
        volatile UWORD IPC3;
        volatile UWORD IPC4;
        volatile UWORD IPC5;
        volatile UWORD IPC6;
        volatile UWORD IPC7;
        volatile UWORD IPC8;
        volatile UWORD IPC9;
        volatile UWORD IPC10;
        volatile UWORD IPC11;
        volatile UWORD IPC12;
        volatile UWORD IPC13;
        __CSP_SKIP_BYTE(2);
        volatile UWORD IPC15;
        volatile UWORD IPC16;
        __CSP_SKIP_BYTE(2);
        volatile UWORD IPC18;
        volatile UWORD IPC19;
        volatile UWORD IPC20;
        volatile UWORD IPC21;
        volatile UWORD IPC22;
        volatile UWORD IPC23;

    } INT_TYPE;

    /* Definitions for interrupt API */
    /* ----------------------------- */

    /* csp_int_ext_set() and csp_int_ext_set() parameters type */

    typedef enum __INT_EXT
    {
        EINT0,      /* external interrupt INT0 */
        EINT1,      /* external interrupt INT1 */
        EINT2,      /* external interrupt INT2 */
        EINT3,      /* external interrupt INT3 */
        EINT4       /* external interrupt INT4 */
    } INT_EXT;

    /* csp_int_en(), csp_int_dis() and other functions parameters type */

    typedef enum __INT_SOURCE
    {
        INT_INT0            =  8,       /* External Interrupt 0         */
        INT_IC1             =  9,       /* Input Capture 1              */
        INT_OC1             = 10,       /* Output Compare 1             */
        INT_TMR1            = 11,       /* Timer1                       */

        INT_IC2             = 13,       /* Input Capture 2              */
        INT_OC2             = 14,       /* Output Compare 2             */
        INT_TMR2            = 15,       /* Timer2                       */
        INT_TMR3            = 16,       /* Timer3                       */
        INT_SPI1_ERR        = 17,       /* SPI1 Error                   */
        INT_SPI1_EVENT      = 18,       /* SPI1 Event                   */
        INT_UART1_RX        = 19,       /* UART1 Receiver               */
        INT_UART1_TX        = 20,       /* UART1 Transmitter            */
        INT_ADC1            = 21,       /* ADC1 Conversion Done         */

        INT_I2C1_SLAVE      = 24,       /* I2C1 Slave Event             */
        INT_I2C1_MASTER     = 25,       /* I2C1 Master Event            */
        INT_COMP            = 26,       /* Comparator Event             */
        INT_CN              = 27,       /* Input Change Notification    */
        INT_INT1            = 28,       /* External Interrupt 1         */

        INT_IC7             = 30,       /* Input Capture 7              */
        INT_IC8             = 31,       /* Input Capture 8              */

        INT_OC3             = 33,       /* Output Compare 3             */
        INT_OC4             = 34,       /* Output Compare 4             */
        INT_TMR4            = 35,       /* Timer4                       */
        INT_TMR5            = 36,       /* Timer5                       */
        INT_INT2            = 37,       /* External Interrupt 2         */
        INT_UART2_RX        = 38,       /* UART2 Receiver               */
        INT_UART2_TX        = 39,       /* UART2 Transmitter            */
        INT_SPI2_ERR        = 40,       /* SPI2 Error                   */
        INT_SPI2_EVENT      = 41,       /* SPI2 Event                   */

        INT_IC3             = 45,       /* Input Capture 3              */
        INT_IC4             = 46,       /* Input Capture 4              */
        INT_IC5             = 47,       /* Input Capture 5              */
        INT_IC6             = 48,       /* Input Capture 6              */

        INT_OC5             = 49,       /* Output Compare 5             */
        INT_OC6             = 50,       /* Output Compare 6             */
        INT_OC7             = 51,       /* Output Compare 7             */
        INT_OC8             = 52,       /* Output Compare 8             */
        INT_PMP             = 53,       /* Parallel Master Port         */

        INT_I2C2_SLAVE      = 57,       /* I2C2 Slave Event             */
        INT_I2C2_MASTER     = 58,       /* I2C2 Master Event            */

        INT_INT3            = 61,       /* External Interrupt 3         */
        INT_INT4            = 62,       /* External Interrupt 4         */

        INT_RTCC            = 70,       /* Real-Time Clock/Calendar     */

        INT_UART1_ERR       = 73,       /* UART1 Error                  */
        INT_UART2_ERR       = 74,       /* UART2 Error                  */
        INT_CRC             = 75,       /* CRC Generator                */

        INT_LVD             = 80,       /* Low-Voltage detect           */

        INT_CTMU            = 85,       /* CTMU module                  */

        INT_UART3_ERR       = 89,       /* UART3 Error                  */
        INT_UART3_RX        = 90,       /* UART3 Receiver               */
        INT_UART3_TX        = 91,       /* UART3 Transmitter            */
        INT_I2C3_SLAVE      = 92,       /* I2C3 Slave Event             */
        INT_I2C3_MASTER     = 93,       /* I2C3 Master Event            */
        INT_USB            = 94,       /* USB Event                    */
        INT_UART4_ERR       = 95,       /* UART4 Error                  */
        INT_UART4_RX        = 96,       /* UART4 Receiver               */
        INT_UART4_TX        = 97,       /* UART4 Transmitter            */
        INT_SPI3_ERR        = 98,       /* SPI3 Error                   */
        INT_SPI3_EVENT      = 99,       /* SPI3 Event                   */
        INT_OC9             = 100,      /* Output Compare 9             */
        INT_IC9             = 101       /* Input Capture 9              */

    } INT_SOURCE;

#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    /* Interrupt base address */
    /* ---------------------- */

    #define INT_BASE_ADDR        0x0080

    /* Declare interrupt model */
    /* ----------------------- */

    extern INT_TYPE INT  __CSP_SFR(INT_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    #define __INT_ALT_TABLE_BIT     (1 << 15)
    #define __INT_NESTING_BIT       (1 << 15)

    #define __INT_CONFBIT_FS        (15)
    #define __INT_CONFBIT_FE        (15)

    #define __INT_PRI_MASK          (0x0007)

    #if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)
    #define __INT_STAT_MASK         (0x8F7F)
    #endif


    /* Definitions for interrupt API */
    /* ----------------------------- */

    /* csp_int_trap_flag_set(),  csp_int_trap_flag_clr() and csp_int_trap_flag_check() parameters type*/

    typedef enum __INT_TRAP
    {
        INT_TRAP_OSCILLATOR =  1,           /* Oscillator Failure */
        INT_TRAP_STACK      =  2,           /* Stack error        */
        INT_TRAP_ADDRESS    =  3,           /* Address Error      */
        INT_TRAP_MATH       =  4            /* Math Error         */

    } INT_TRAP;

    /* csp_int_conf_set() and csp_int_conf_get() parameters mask */

    typedef enum __INT_CONF
    {
        INT_NESTING_EN    = (1 << 0),       /* Enable nesting interrupts  */
        INT_NESTING_DIS   = (0),            /* Disable nesting interrupts */

        INT_ALT_TABLE_EN  = (1 << 1),       /* Enable alternative table  */
        INT_ALT_TABLE_DIS = (0),            /* Disable alternative table */

        #if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)
        INT_HOLD_HIGHEST  = (1 << 2),       /* VECNUM will contain the value of the highest priority pending interrupt, instead of the current interrupt */
        INT_HOLD_LAST     = (0),            /* VECNUM will contain the value of the last Acknowledged interrupt */
        #endif

    } INT_CONF;

    /* csp_int_ext_set() and csp_int_ext_get() parameters type */

    typedef enum __INT_EXT_POL
    {
        INT_EXT_POL_POS,                /* Interrupt on rise edge    */
        INT_EXT_POL_NEG,                /* Interrupt on falling edge */
    } INT_EXT_POL;

    /* csp_int_priority_set() and csp_int_priority_get() parameters type */

        /* see in csp.h */


    /* Interrupt service routine qualifier */

    #define CSP_ISR                 __attribute__((interrupt, no_auto_psv, section(".isr")))
    #define CSP_ISR_FAST            __attribute__((interrupt, shadow, no_auto_psv, section(".isr")))
    #define CSP_ISR_AUTOPSV         __attribute__((interrupt, auto_psv, section(".isr")))
    #define CSP_ISR_AUTOPSV_FAST    __attribute__((interrupt, shadow, auto_psv, section(".isr")))

#endif

/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_INT)

        void        csp_int_conf_set(INT_CONF val);
        INT_CONF    csp_int_conf_get(void);
        
        #if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)
        UWORD       csp_int_stat_get(void);
        #endif

        void        csp_int_ext_set(INT_EXT ch, INT_EXT_POL pol);
        INT_EXT_POL csp_int_ext_get(INT_EXT ch);

        void        csp_int_priority_set(INT_SOURCE int_src, INT_PRI ipl);
        INT_PRI     csp_int_priority_get(INT_SOURCE int_src);

        void        csp_int_en(INT_SOURCE int_src);
        void        csp_int_dis(INT_SOURCE int_src);
        UWORD       csp_int_check(INT_SOURCE int_src);

        void        csp_int_flag_set(INT_SOURCE int_src);
        void        csp_int_flag_clr(INT_SOURCE int_src);
        UWORD       csp_int_flag_check(INT_SOURCE int_src);

        void        csp_int_trap_flag_set(INT_TRAP int_trap);
        void        csp_int_trap_flag_clr(INT_TRAP int_trap);
        UWORD       csp_int_trap_flag_check(INT_TRAP int_trap);


    #else

        #include "csp_int.c"
        
    #endif

#endif

#endif /* #ifndef __CSP_INT_H */
/* ********************************************************************************************** */
/* end of file: csp_int.h */
