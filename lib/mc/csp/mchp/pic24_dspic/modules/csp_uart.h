/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_uart.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_UART_H
#define __CSP_UART_H

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    /* UART programming model */
    /* ---------------------- */

    typedef struct __UART_TYPE
    {
        volatile UWORD UMODE;
        volatile UWORD USTA;
        volatile UWORD UTXREG;
        volatile UWORD URXREG;
        volatile UWORD UBRG;
    } UART_TYPE;


    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
        )

        /* UART base addresses */
        /* ------------------- */

        #define UART1_BASE_ADDR     0x0220
        #define UART2_BASE_ADDR     0x0230

        /* Declare UART models */
        /* ------------------- */

        extern UART_TYPE UART1 __CSP_SFR(UART1_BASE_ADDR);
        extern UART_TYPE UART2 __CSP_SFR(UART2_BASE_ADDR);

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
          )

        /* UART base addresses */
        /* ------------------- */

        #define UART1_BASE_ADDR     0x0220
        #define UART2_BASE_ADDR     0x0230
        #define UART3_BASE_ADDR     0x0250
        #define UART4_BASE_ADDR     0x02B0

        /* Declare UART models */
        /* ------------------- */

        extern UART_TYPE UART1 __CSP_SFR(UART1_BASE_ADDR);
        extern UART_TYPE UART2 __CSP_SFR(UART2_BASE_ADDR);
        extern UART_TYPE UART3 __CSP_SFR(UART3_BASE_ADDR);
        extern UART_TYPE UART4 __CSP_SFR(UART4_BASE_ADDR);

    #endif

    /* Internal API definitions */
    /* ------------------------ */

    #define __UART_WAKEUP_BIT       (7)
    #define __UART_AUTOBAUD_BIT     (5)
    #define __UART_BREAK_BIT        (11)

    #define __UART_STAT_MASK        (UART_STAT_TX_BUF_FULL | UART_STAT_TX_SREG_EMPTY |  \
                                     UART_STAT_RX_IDLE | UART_STAT_RX_BUF_NOT_EMPTY |   \
                                     UART_STAT_ERR_PARITY | UART_STAT_ERR_FRAME |       \
                                     UART_STAT_ERR_OVF)

    /* Definitions for UART API */
    /* ------------------------ */

    /* csp_uart_conf_set() and csp_uart_conf_get() parameters mask */

    #define UART_EN                 (1UL << 31) /* UART is enabled */
    #define UART_DIS                (0UL << 31) /* UART is disabled */

    #define UART_DEBUG_FREEZE_EN    (1UL << 30) /* When emulator is in Debug mode, module freezes operation */
    #define UART_DEBUG_FREEZE_DIS   (0UL << 30) /* When emulator is in Debug mode, module continues operation */

    #define UART_IDLE_STOP          (1UL << 29) /* Discontinue operation when device enters Idle mode */
    #define UART_IDLE_CON           (0UL << 29) /* Continue operation in Idle mode */

    #define UART_IRDA_EN            (1UL << 28) /* IrDA encoder and decoder enabled */
    #define UART_IRDA_DIS           (0UL << 28) /* IrDA encoder and decoder disabled */

    #define UART_RTS_SIMPLEX        (1UL << 27) /* URTS in Simplex mode */
    #define UART_RTS_FLOW_CONTROL   (0UL << 27) /* URTS in Flow Control mode */

    #define UART_PINS_TX_RX_BCLK    (3UL << 24) /* UTX, URX and BCLK pins are enabled and used; UCTS pin is controlled by port latches */
    #define UART_PINS_TX_RX_RTS_CTS (2UL << 24) /* UTX, URX, UCTS and URTS pins are enabled and used */
    #define UART_PINS_TX_RX_RTS     (1UL << 24) /* UTX, URX and URTS pins are enabled and used; UCTS pin is controlled by port latches */
    #define UART_PINS_TX_RX         (0UL << 24) /* Only UTX and URX pins are enabled and used */

    #define UART_LOOPBACK_EN        (1UL << 22) /* Enable Loopback mode */
    #define UART_LOOPBACK_DIS       (0UL << 22) /* Loopback mode is disabled */

    #define UART_RX_IDLE_0          (1UL << 20) /* UxRX Idle state is �0� */
    #define UART_RX_IDLE_1          (0UL << 20) /* UxRX Idle state is �1� */

    #define UART_HIGH_BAUD_EN       (1UL << 19) /* High speed */
    #define UART_HIGH_BAUD_DIS      (0UL << 19) /* Low speed Q*/

    #define UART_MODE_8_NO          (0UL << 17) /* 8-bit data, no parity */
    #define UART_MODE_8_EVEN        (1UL << 17) /* 8-bit data, even parity */
    #define UART_MODE_8_ODD         (2UL << 17) /* 8-bit data, odd parity */
    #define UART_MODE_9_NO          (3UL << 17) /* 9-bit data, no parity */

    #define UART_STOP_2             (1UL << 16) /* 2 Stop bits */
    #define UART_STOP_1             (0UL << 16) /* 1 Stop bit */


    #define UART_TX_INT_MODE_1      (4UL << 13) /* Interrupt generated when a character is transferred to the Transmit Shift register and the transmit buffer becomes empty */
    #define UART_TX_INT_MODE_2      (1UL << 13) /* Interrupt generated when the last transmission is over  */
    #define UART_TX_INT_MODE_3      (0UL << 13) /* Interrupt generated when any character is transferred to the Transmit Shift Register*/

    #define UART_RX_INT_MODE_1      (3UL <<  6) /* Interrupt flag bit is set when receive buffer is full */
    #define UART_RX_INT_MODE_2      (2UL <<  6) /* Interrupt flag bit is set when receive buffer is 3/4 full */
    #define UART_RX_INT_MODE_3      (1UL <<  6) /* Interrupt flag bit is set when a character is received */

    #define UART_TX_EN              (1UL << 10) /* UART transmitter enabled */
    #define UART_TX_DIS             (0UL << 10) /* UART transmitter disabled, any pending transmission is aborted and buffer is reset */

    #define UART_TX_IDLE_0          (1UL << 14) /* UTX Idle state is �0� */
    #define UART_TX_IDLE_1          (0UL << 14) /* UTX Idle state is �1�*/

    #define UART_RX_ADDR_DET_EN     (1UL <<  5) /* Address Detect mode enabled */
    #define UART_RX_ADDR_DET_DIS    (0UL <<  5) /* Address Detect mode disabled */


    /* csp_uart_stat_get() and csp_uart_stat_clr() parameters type */

    typedef enum __UART_STAT
    {
        UART_STAT_TX_BUF_FULL      = (1 <<  9),  /* RO  - Transmit buffer is full */
        UART_STAT_TX_SREG_EMPTY    = (1 <<  8),  /* RO  - Transmit Shift register is empty and transmit buffer is empty */
        UART_STAT_RX_IDLE          = (1 <<  4),  /* RO  - Receiver is Idle */
        UART_STAT_RX_BUF_NOT_EMPTY = (1 <<  0),  /* RO  - Receive buffer has data, at least one more character can be read */
        UART_STAT_ERR_PARITY       = (1 <<  3),  /* RO  - Parity error has been detected for the current character */
        UART_STAT_ERR_FRAME        = (1 <<  2),  /* RO  - Framing error has been detected for the current character */
        UART_STAT_ERR_OVF          = (1 <<  1)   /* R/C - Receive buffer has overflowed */
    } UART_STAT;

    /* csp_uart_comm() parameter type */

    typedef enum __UART_COMM
    {
        UART_COMM_WAKEUP   = (0),
        UART_COMM_AUTOBAUD = (1),
        UART_COMM_BREAK    = (2)
    } UART_COMM;

#endif


/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_UART)

        void      csp_uart_conf_set(UART_TYPE *uart, U32 val);
        U32       csp_uart_conf_get(UART_TYPE *uart);

        void      csp_uart_baud_set(UART_TYPE *uart, UWORD val);
        void      csp_uart_baud_set_auto (UART_TYPE *uart, unsigned long periph_bus_clk_freq, unsigned long baudrate);
        UWORD     csp_uart_baud_get(UART_TYPE *uart);

        void      csp_uart_comm(UART_TYPE *uart, UART_COMM comm);

        UART_STAT csp_uart_stat_get(UART_TYPE *uart);
        void      csp_uart_stat_clr(UART_TYPE *uart, UART_STAT mask);

        void      csp_uart_put(UART_TYPE *uart, UWORD val);
        U16       csp_uart_get(UART_TYPE *uart);

    #else

        #include "csp_uart.c"

    #endif

#endif

#endif /* #ifndef __CSP_UART_H */
/* ********************************************************************************************** */
/* end of file: csp_uart.h */
