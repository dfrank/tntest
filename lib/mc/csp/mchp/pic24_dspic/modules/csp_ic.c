/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ic_cget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    __CSP_FUNC UWORD  csp_ic_conf_get (IC_TYPE *ic)
    {
        return ic->ICCON;
    }

#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||    \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110)       \
      )

    __CSP_FUNC U32  csp_ic_conf_get (IC_TYPE *ic)
    {
        AL_EXT_32 ret;
        ret.word_0 = ic->ICCON1;
        ret.word_1 = ic->ICCON2;
        return ret.full;
    }

#endif


/* ********************************************************************************************** */
/* end of file: csp_ic_cget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ic_cset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif


#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    __CSP_FUNC void  csp_ic_conf_set (IC_TYPE *ic, UWORD val)
    {
        ic->ICCON = val;
    }

#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||    \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110)       \
      )

    __CSP_FUNC void  csp_ic_conf_set (IC_TYPE *ic, U32 val)
    {
        ic->ICCON1 = ((AL_EXT_32)val).word_0;
        ic->ICCON2 = ((AL_EXT_32)val).word_1;
    }

#endif



/* ********************************************************************************************** */
/* end of file: csp_ic_cset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ic_get.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_ic_get (IC_TYPE *ic)
{
    return ic->ICBUF;
}

/* ********************************************************************************************** */
/* end of file: csp_ic_get.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ic_sget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    __CSP_FUNC IC_STAT  csp_ic_stat_get(IC_TYPE *ic)
    {
        return (IC_STAT)__CSP_ACC_MASK_VAL(BFA_RD, ic->ICCON, (IC_STAT_BUF_OVF | IC_STAT_BUF_NOT_EMPTY));
    }

#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||    \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110)       \
      )

    __CSP_FUNC IC_STAT  csp_ic_stat_get(IC_TYPE *ic)
    {
        return (IC_STAT)__CSP_ACC_MASK_VAL(BFA_RD, ic->ICCON1, (IC_STAT_BUF_OVF | IC_STAT_BUF_NOT_EMPTY)) |
               (IC_STAT)__CSP_ACC_MASK_VAL(BFA_RD, ic->ICCON2, (IC_STAT_RUNNING));
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_ic_sget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ic_tget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
    )

    __CSP_FUNC U32  csp_ic_32b_get (IC_TYPE *ic)
    {
        AL_EXT_32 ret;

        ret.word_0 = ic->ICBUF;
        ret.word_1 = ((IC_TYPE*)((U08*)ic + IC_ADDR_BASE_DIFF))->ICBUF;

        return (ret.full);
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_ic_tget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ic_tmgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
    )

    __CSP_FUNC UWORD  csp_ic_tmr_get (IC_TYPE *ic)
    {
        return (ic->ICTMR);
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_ic_tmgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ic_tmst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
    )

    __CSP_FUNC void  csp_ic_tmr_set (IC_TYPE *ic, UWORD val)
    {
        ic->ICTMR = val;
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_ic_tmst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ic_ttgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
    )

    __CSP_FUNC U32  csp_ic_tmr_32b_get (IC_TYPE *ic)
    {
        AL_EXT_32 ret;

        ret.word_0 = ic->ICTMR;
        ret.word_1 = ((IC_TYPE*)((U08*)ic + IC_ADDR_BASE_DIFF))->ICTMR;

        return (ret.full);
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_ic_ttgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_ic_ttst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) || \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)    \
    )

    __CSP_FUNC void  csp_ic_tmr_32b_set (IC_TYPE *ic, U32 val)
    {
        ((IC_TYPE*)((U08*)ic + IC_ADDR_BASE_DIFF))->ICTMR = (UWORD)(val >> 16);
        ic->ICTMR                                         = (UWORD)(val);
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_ic_ttst.c */
