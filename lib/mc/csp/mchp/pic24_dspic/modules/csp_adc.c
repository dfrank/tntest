/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_aclr.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif


#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    __CSP_FUNC void  csp_adc_an_clr (ADC_TYPE *adc, U16 mask)
    {
        __CSP_ACC_MASK_VAL(BFA_SET, adc->ADPCFG, mask, UINT_MAX);
    }

#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
      )

    __CSP_FUNC void  csp_adc_an_clr (ADC_TYPE *adc, U32 mask)
    {
        __CSP_ACC_MASK_VAL(BFA_SET, adc->ADPCFGH, ((AL_EXT_32)mask).word_1, UINT_MAX);
        __CSP_ACC_MASK_VAL(BFA_SET, adc->ADPCFGL, ((AL_EXT_32)mask).word_0, UINT_MAX);
    }

#endif

/* ********************************************************************************************** */
/* end of file: csp_adc_aclr.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_aget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif


#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

__CSP_FUNC U16  csp_adc_an_get (ADC_TYPE *adc)
{
        return (ADC_CHAN)(adc->ADPCFG);
}

#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
      )

__CSP_FUNC U32  csp_adc_an_get (ADC_TYPE *adc)
{      
        AL_EXT_32 ret;
        
        ret.word_0 = adc->ADPCFGL;
        ret.word_1 = adc->ADPCFGH;
        
        return (ADC_CHAN)(ret.full);
}

#endif

/* ********************************************************************************************** */
/* end of file: csp_adc_aget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_aset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif


#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

__CSP_FUNC void  csp_adc_an_set (ADC_TYPE *adc, U16 mask)
{
        __CSP_ACC_MASK_VAL(BFA_CLR, adc->ADPCFG, mask, UINT_MAX);
}

#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
      )

__CSP_FUNC void  csp_adc_an_set (ADC_TYPE *adc, U32 mask)
{
        __CSP_ACC_MASK_VAL(BFA_CLR, adc->ADPCFGH, ((AL_EXT_32)mask).word_1, UINT_MAX);
        __CSP_ACC_MASK_VAL(BFA_CLR, adc->ADPCFGL, ((AL_EXT_32)mask).word_0, UINT_MAX);
}

#endif

/* ********************************************************************************************** */
/* end of file: csp_adc_aset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_cget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC U64  csp_adc_conf_get (ADC_TYPE *adc)
{
    AL_EXT_64 ret;

    ret.word_0 = adc->ADCON3;
    ret.word_1 = adc->ADCON2;
    ret.word_2 = adc->ADCON1;
    ret.word_3 = 0;

    return ret.full;
}

/* ********************************************************************************************** */
/* end of file: csp_adc_cget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_clgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_adc_clock_get (ADC_TYPE *adc)
{
    return __CSP_ACC_RANG_VAL(BFA_RD, adc->ADCON3, __ADC_CLOCK_FS, __ADC_CLOCK_FE);
}

/* ********************************************************************************************** */
/* end of file: csp_adc_clgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_clst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_adc_clock_set (ADC_TYPE *adc, UWORD val)
{
    __CSP_ACC_RANG_VAL(BFA_WR, adc->ADCON3, __ADC_CLOCK_FS, __ADC_CLOCK_FE, val);
}

/* ********************************************************************************************** */
/* end of file: csp_adc_clst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_comm.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_adc_comm (ADC_TYPE *adc, ADC_COMM comm)
{
    switch (comm)
    {
        case ADC_COMM_SAMP_START:
            __CSP_BIT_VAL_SET(adc->ADCON1, __ADC_SAMP_BIT);
            break;
        case ADC_COMM_CONV_START:
            __CSP_BIT_VAL_CLR(adc->ADCON1, __ADC_SAMP_BIT);
            break;
    }
}

/* ********************************************************************************************** */
/* end of file: csp_adc_comm.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_cset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_adc_conf_set (ADC_TYPE *adc, U64 val)
{
    adc->ADCON3 = ((AL_EXT_64)val).word_0;
    adc->ADCON2 = ((AL_EXT_64)val).word_1;
    adc->ADCON1 = ((AL_EXT_64)val).word_2;
}

/* ********************************************************************************************** */
/* end of file: csp_adc_cset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_disb.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif


__CSP_FUNC void  csp_adc_dis (ADC_TYPE *adc)
{
    __CSP_BIT_VAL_CLR(adc->ADCON1, __ADC_EN_BIT);
}


/* ********************************************************************************************** */
/* end of file: csp_adc_disb.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_enbl.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif


__CSP_FUNC void  csp_adc_en (ADC_TYPE *adc)
{
    __CSP_BIT_VAL_SET(adc->ADCON1, __ADC_EN_BIT);
}


/* ********************************************************************************************** */
/* end of file: csp_adc_enbl.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_get.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_adc_get (ADC_TYPE *adc, ADC_BUF buf)
{
    return adc->ADCBUF[buf];
}

/* ********************************************************************************************** */
/* end of file: csp_adc_get.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_magt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC ADC_MUX_POS  csp_adc_mux_a_get (ADC_TYPE *adc, ADC_MUX_NEG *neg)
{
    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
         )

        *neg = __CSP_ACC_MASK_VAL(BFA_RD, adc->ADCHS, ADC_MUX_NEG_AN1);
        return __CSP_ACC_RANG_VAL(BFA_RD, adc->ADCHS, __ADC_MUXA_POS_FS,  __ADC_MUXA_POS_FE);

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
          )

        *neg = __CSP_ACC_MASK_VAL(BFA_RD, adc->ADCHS0, ADC_MUX_NEG_AN1);
        return __CSP_ACC_RANG_VAL(BFA_RD, adc->ADCHS0, __ADC_MUXA_POS_FS,  __ADC_MUXA_POS_FE);

    #endif
}

/* ********************************************************************************************** */
/* end of file: csp_adc_magt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_mast.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_adc_mux_a_set (ADC_TYPE *adc, ADC_MUX_NEG neg, ADC_MUX_POS pos)
{
    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
         )

        __CSP_ACC_RANG_VAL(BFA_WR, adc->ADCHS, __ADC_MUXA_FULL_FS, __ADC_MUXA_FULL_FE, (neg | pos));

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
          )

        __CSP_ACC_RANG_VAL(BFA_WR, adc->ADCHS0, __ADC_MUXA_FULL_FS, __ADC_MUXA_FULL_FE, (neg | pos));

    #endif
}

/* ********************************************************************************************** */
/* end of file: csp_adc_mast.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_mbgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC ADC_MUX_POS  csp_adc_mux_b_get (ADC_TYPE *adc, ADC_MUX_NEG *neg)
{
    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
         )

        *neg = __CSP_ACC_MASK_VAL(BFA_RD, adc->ADCHS, (1 << 15)) ? ADC_MUX_NEG_AN1 : ADC_MUX_NEG_VREFM;
        return __CSP_ACC_RANG_VAL(BFA_RD, adc->ADCHS, __ADC_MUXB_POS_FS, __ADC_MUXB_POS_FE);

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
          )

        *neg = __CSP_ACC_MASK_VAL(BFA_RD, adc->ADCHS0, (1 << 15)) ? ADC_MUX_NEG_AN1 : ADC_MUX_NEG_VREFM;
        return __CSP_ACC_RANG_VAL(BFA_RD, adc->ADCHS0, __ADC_MUXB_POS_FS, __ADC_MUXB_POS_FE);

    #endif

}

/* ********************************************************************************************** */
/* end of file: csp_adc_mbgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_mbst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_adc_mux_b_set (ADC_TYPE *adc, ADC_MUX_NEG neg, ADC_MUX_POS pos)
{
    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
         )

        __CSP_ACC_RANG_VAL(BFA_WR, adc->ADCHS, __ADC_MUXB_FULL_FS, __ADC_MUXB_FULL_FE, (neg | pos));

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
          )

        __CSP_ACC_RANG_VAL(BFA_WR, adc->ADCHS0, __ADC_MUXB_FULL_FS, __ADC_MUXB_FULL_FE, (neg | pos));

    #endif
}

/* ********************************************************************************************** */
/* end of file: csp_adc_mbst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_sccl.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_adc_scan_chain_clr (ADC_TYPE *adc, ADC_CHAN mask)
{
    __CSP_ACC_MASK_VAL(BFA_CLR, adc->ADCSSL, mask, UINT_MAX);
}

/* ********************************************************************************************** */
/* end of file: csp_adc_sccl.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_scgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC ADC_CHAN  csp_adc_scan_chain_get (ADC_TYPE *adc)
{
    return (ADC_CHAN)(adc->ADCSSL);
}

/* ********************************************************************************************** */
/* end of file: csp_adc_scgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_sclr.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_adc_stat_clr (ADC_TYPE *adc, ADC_STAT mask)
{
    __CSP_ACC_MASK_VAL(BFA_CLR, adc->ADCON1, (ADC_STAT_SAMP_DONE | ADC_STAT_CONV_DONE), mask);
}

/* ********************************************************************************************** */
/* end of file: csp_adc_sclr.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_scst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_adc_scan_chain_set (ADC_TYPE *adc, ADC_CHAN mask)
{
    __CSP_ACC_MASK_VAL(BFA_SET, adc->ADCSSL, mask, UINT_MAX);
}

/* ********************************************************************************************** */
/* end of file: csp_adc_scst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_sget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC ADC_STAT  csp_adc_stat_get (ADC_TYPE *adc)
{
    return (ADC_STAT)(__CSP_ACC_MASK_VAL(BFA_RD, adc->ADCON1, (ADC_STAT_SAMP_DONE | ADC_STAT_CONV_DONE)) |
                      __CSP_ACC_MASK_VAL(BFA_RD, adc->ADCON2, (ADC_STAT_FILL_8_16)));
}

/* ********************************************************************************************** */
/* end of file: csp_adc_sget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_tmgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_adc_samp_time_get (ADC_TYPE *adc)
{
    return __CSP_ACC_RANG_VAL(BFA_RD, adc->ADCON3, __ADC_SAMP_TIME_FS, __ADC_SAMP_TIME_FE);
}

/* ********************************************************************************************** */
/* end of file: csp_adc_tmgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_adc_tmst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_adc_samp_time_set (ADC_TYPE *adc, UWORD val)
{
    __CSP_ACC_RANG_VAL(BFA_WR, adc->ADCON3, __ADC_SAMP_TIME_FS, __ADC_SAMP_TIME_FE, val);
}


/* ********************************************************************************************** */
/* end of file: csp_adc_tmst.c */
