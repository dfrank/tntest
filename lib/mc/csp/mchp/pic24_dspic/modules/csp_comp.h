/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_comp.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_COMP_H
#define __CSP_COMP_H

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )
    
    
    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
        )
    
        /* Comparators programming model */
        /* ----------------------------- */
    
        typedef struct __COMP_TYPE
        {
            volatile UWORD CMCON;
            volatile UWORD CVRCON;
        } COMP_TYPE;
    
        /* Comparators base addresses */
        /* -------------------------- */
    
        #define COMP_BASE_ADDR 0x0630
    
        /* Declare Comparators models */
        /* -------------------------- */
    
        extern  COMP_TYPE COMP __CSP_SFR(COMP_BASE_ADDR);
    
        /* Internal API definitions */
        /* ------------------------ */
    
        #define __COMP_VR_VAL_MASK      (0x000F)
        #define __COMP_STAT_CLR_MASK    (COMP_STAT_2_EVENT | COMP_STAT_1_EVENT)
    
    
        /* Definitions for Comparators API */
        /* ------------------------------- */
    
        /* csp_comp_conf_set() and csp_comp_conf_get() parameters mask */
    
        typedef enum __COMP_CONF
        {
            COMP_IDLE_STOP      = (1 << 15),    /* When device enters Idle mode, module does not generate interrupts */
            COMP_IDLE_CON       = (0 << 15),    /* Continue normal module operation in Idle mode */
                                                
            COMP_2_EN           = (1 << 11),    /* Comparator 2 is enabled */
            COMP_2_DIS          = (0 << 11),    /* Comparator 2 is disabled */
                                                
            COMP_1_EN           = (1 << 10),    /* Comparator 1 is enabled */
            COMP_1_DIS          = (0 << 10),    /* Comparator 1 is disabled */
                                                
            COMP_2_OUT_EN       = (1 <<  9),    /* Comparator 2 output is driven on the output pad */
            COMP_2_OUT_DIS      = (0 <<  9),    /* Comparator 2 output is not driven on the output pad */
                                                
            COMP_1_OUT_EN       = (1 <<  8),    /* Comparator 1 output is driven on the output pad */
            COMP_1_OUT_DIS      = (0 <<  8),    /* Comparator 1 output is not driven on the output pad */
                                                
            COMP_2_INV_EN       = (1 <<  5),    /* Comparator 2 output inverted */
            COMP_2_INV_DIS      = (0 <<  5),    /* Comparator 2 output not inverted */
                                                
            COMP_1_INV_EN       = (1 <<  4),    /* Comparator 2 output inverted */
            COMP_1_INV_DIS      = (0 <<  4),    /* Comparator 2 output not inverted */
                                                
            COMP_2_NEG_INP_POS  = (1 <<  3),    /* Negative input is connected to C2IN+ */
            COMP_2_NEG_INP_NEG  = (0 <<  3),    /* Negative input is connected to C2IN- */
                                                
            COMP_2_POS_INP_POS  = (1 <<  2),    /* Positive input is connected to C2IN+ */
            COMP_2_POS_INP_VREF = (0 <<  2),    /* Positive input is connected to CVref */
                                                
            COMP_1_NEG_INP_POS  = (1 <<  1),    /* Negative input is connected to C1IN+ */
            COMP_1_NEG_INP_NEG  = (0 <<  1),    /* Negative input is connected to C1IN- */
                                                
            COMP_1_POS_INP_POS  = (1 <<  0),    /* Positive input is connected to C1IN+ */
            COMP_1_POS_INP_VREF = (0 <<  0)     /* Positive input is connected to CVref */
        } COMP_CONF;
    
        /* csp_comp_stat_get() and csp_comp_stat_clr() parameters type */
    
        typedef enum __COMP_STAT
        {
            COMP_STAT_2_EVENT = (1 << 13),  /* R/C - Comparator 2 output changed states */
            COMP_STAT_1_EVENT = (1 << 12),  /* R/C - Comparator 1 output changed states */
            COMP_STAT_2_OUT   = (1 <<  7),  /* RO  - Comparator 2 output */
            COMP_STAT_1_OUT   = (1 <<  6)   /* RO  - Comparator 1 output */
    
        } COMP_STAT;
    
    
        /* csp_comp_check() parameter type */
    
        typedef enum __COMP_NUM
        {
            COMP_1 = COMP_STAT_1_OUT,
            COMP_2 = COMP_STAT_2_OUT
        } COMP_NUM;
    
    
    
    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
          )
    
        /* Comparators programming model */
        /* ----------------------------- */
    
        typedef struct __COMP_TYPE
        {
            volatile UWORD CMCON;
        } COMP_TYPE;
    
        typedef struct __COMP_REF_TYPE
        {
            volatile UWORD CMSTAT;
            volatile UWORD CVRCON;
        } COMP_REF_TYPE;
    
        /* Comparators base addresses */
        /* -------------------------- */
    
        #define COMP1_BASE_ADDR     0x0634
        #define COMP2_BASE_ADDR     0x0636
        #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
            )
        #define COMP3_BASE_ADDR     0x0638
        #endif
        #define COMPREF_BASE_ADDR   0x0630
    
        /* Declare Comparators models */
        /* -------------------------- */
    
        extern  COMP_TYPE  COMP1 __CSP_SFR(COMP1_BASE_ADDR);
        extern  COMP_TYPE  COMP2 __CSP_SFR(COMP2_BASE_ADDR);
        #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
            )
        extern  COMP_TYPE  COMP3 __CSP_SFR(COMP3_BASE_ADDR);
        #endif
        extern  COMP_REF_TYPE COMP_REF __CSP_SFR(COMPREF_BASE_ADDR);
        
        /* Internal API definitions */
        /* ------------------------ */
    
        #define __COMP_VR_VAL_MASK      (0x000F)
        #define __COMP_STAT_CLR_MASK    (COMP_STAT_EVENT)
    
        /* Definitions for Comparators API */
        /* ------------------------------- */
    
        /* csp_comp_conf_set() and csp_comp_conf_get() parameters mask */
    
        typedef enum __COMP_CONF
        {
            COMP_EN            = (1 << 15),
            COMP_DIS           = (0 << 15),
        
            COMP_OUT_EN        = (1 << 14),
            COMP_OUT_DIS       = (0 << 14),
        
            COMP_INV_EN        = (1 << 13),
            COMP_INV_DIS       = (0 << 13),
        
            #if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)
            COMP_LOW_POW_EN    = (1 << 12),
            COMP_LOW_POW_DIS   = (0 << 12),
            #endif

            COMP_RESP_DIS      = (0 <<  6),
            COMP_RESP_LOW_HIGH = (1 <<  6),
            COMP_RESP_HIGH_LOW = (2 <<  6),
            COMP_RESP_BOTH     = (3 <<  6),
        
            COMP_POS_VREF      = (1 <<  4),
            COMP_POS_CINA      = (0 <<  4),
        
            COMP_NEG_CINB      = (0 <<  0),
            COMP_NEG_CINC      = (1 <<  0),
            COMP_NEG_CIND      = (2 <<  0),
            COMP_NEG_VBG_2     = (3 <<  0)
        } COMP_CONF;
        
        /* csp_comp_stat_get() and csp_comp_stat_clr() parameters type */
    
        typedef enum __COMP_STAT
        {
            COMP_STAT_EVENT   = (1 <<  9),  /* R/W - Comparator output changed states */
            COMP_STAT_OUT     = (1 <<  8)   /* RO  - */
        } COMP_STAT;
    
        /* csp_comp_check() parameter type */
    
        typedef enum __COMP_NUM
        {
            COMP_1 = (1 << 0),
            COMP_2 = (1 << 1),
        #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
            )
            COMP_3 = (1 << 2),
        #endif
        } COMP_NUM;
    
    #endif

    /* csp_comp_ref_set() and csp_comp_ref_get() patameters type */

    typedef enum __COMP_REF_CONF
    {
        COMP_VR_EN         = (1UL <<  7),   /* Comparator voltage reference circuit powered on */
        COMP_VR_DIS        = (0UL <<  7),   /* Comparator voltage reference circuit powered down */
    
        COMP_VR_OUT_EN     = (1UL <<  6),   /* Voltage level is output on CVREF pin */
        COMP_VR_OUT_DIS    = (0UL <<  6),   /* Voltage level is disconnected from CVREF pin */
    
        COMP_VR_RANGE_LOW  = (1UL <<  5),   /* to 0.67 CVRSRC, with CVRSRC/24 step size */
        COMP_VR_RANGE_HIGH = (0UL <<  5),   /* 0.25 CVRSRC to 0.75 CVRSRC, with CVRSRC/32 step size */
    
        COMP_VR_REF_VREF   = (1UL <<  4),   /* Comparator voltage reference source, CVRSRC = (VREF+) � (VREF-) */
        COMP_VR_REF_AVDD   = (0UL <<  4),   /* Comparator voltage reference source, CVRSRC = AVDD � AVSS */
    
        COMP_VR_0          = ( 0 << 0),
        COMP_VR_1          = ( 1 << 0),
        COMP_VR_2          = ( 2 << 0),
        COMP_VR_3          = ( 3 << 0),
        COMP_VR_4          = ( 4 << 0),
        COMP_VR_5          = ( 5 << 0),
        COMP_VR_6          = ( 6 << 0),
        COMP_VR_7          = ( 7 << 0),
        COMP_VR_8          = ( 8 << 0),
        COMP_VR_9          = ( 9 << 0),
        COMP_VR_10         = (10 << 0),
        COMP_VR_11         = (11 << 0),
        COMP_VR_12         = (12 << 0),
        COMP_VR_13         = (13 << 0),
        COMP_VR_14         = (14 << 0),
        COMP_VR_15         = (15 << 0)
    } COMP_REF_CONF;

#endif

/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_COMP)

        UWORD     csp_comp_check(COMP_NUM num);

        void      csp_comp_ref_set(UWORD val);
        UWORD     csp_comp_ref_get(void);


        #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
             (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
            )

            void      csp_comp_conf_set(COMP_CONF val);
            COMP_CONF csp_comp_conf_get(void);

            COMP_STAT csp_comp_stat_get(void);
            void      csp_comp_stat_clr(COMP_STAT mask);

        #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
               (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
               (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
              )

            void      csp_comp_conf_set(COMP_TYPE *comp, COMP_CONF val);
            COMP_CONF csp_comp_conf_get(COMP_TYPE *comp);

            COMP_STAT csp_comp_stat_get(COMP_TYPE *comp);
            void      csp_comp_stat_clr(COMP_TYPE *comp, COMP_STAT mask);

        #endif

    #else

        #include "csp_comp.c"

    #endif

#endif

#endif /* #ifndef __CSP_COMP_H */
/* ********************************************************************************************** */
/* end of file: csp_comp.h */
