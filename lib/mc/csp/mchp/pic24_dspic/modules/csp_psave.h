/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_psave.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_PSAVE_H
#define __CSP_PSAVE_H


#if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

    /* DeepSleep programming model */
    /* --------------------------- */

    typedef struct __DSLEEP_TYPE
    {
        volatile UWORD DSCON;
        volatile UWORD DSWSRC;
        volatile UWORD DSGPR0;
        volatile UWORD DSGPR1;
    } DSLEEP_TYPE;

    /* DeepSleep base address */
    /* ---------------------- */

    #define DSLEEP_BASE_ADDR    0x0758

    /* Declare deepsleep model */
    /* ---------------------DeepSleep-- */

    extern  DSLEEP_TYPE DSLEEP __CSP_SFR(DSLEEP_BASE_ADDR);

    /* Internal API definitions */
    /* ------------------------ */

    #define __PSAVE_DSLEEP_EN_BIT       (15)
    #define __PSAVE_DSLEEP_RELEASE_BIT  (0)

    /* Definitions for reset API */
    /* ------------------------- */

    /* csp_psave_ds_exit() */
    
    typedef enum __PSAVE_DS_CAUSE
    {
        PSAVE_DS_CAUSE_POR      = (1 << 0),
        PSAVE_DS_CAUSE_MCLR     = (1 << 2),
        PSAVE_DS_CAUSE_RTCC     = (1 << 3),
        PSAVE_DS_CAUSE_WDT      = (1 << 4),
        PSAVE_DS_CAUSE_FAULT    = (1 << 7),
        PSAVE_DS_CAUSE_INT0     = (1 << 8),

    } PSAVE_DS_CAUSE;


#endif


/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)
    /* Module API definition */
    #if defined(__CSP_DEBUG_PSAVE)

        void    csp_psave_sleep(void);
        void    csp_psave_idle(void);
    
        #if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

            void csp_psave_ds_enter(U16 p0, U16 p1);
            U16  csp_psave_ds_exit(U16 *p1);
            
            void csp_psave_ds_release_io(void);

            PSAVE_DS_CAUSE csp_psave_ds_cause_get(void);

        #endif

    #else
        #include "csp_psave.c"
    #endif

#endif

#endif /* #ifndef __CSP_PSAVE_H */
/* ********************************************************************************************** */
/* end of file: csp_psave.h */
