/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_comp_cget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
    )

    __CSP_FUNC UWORD  csp_comp_conf_get (void)
    {
        return COMP.CMCON;
    }

#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
      )

    __CSP_FUNC UWORD  csp_comp_conf_get (COMP_TYPE *comp)
    {
        return comp->CMCON;
    }

#endif


/* ********************************************************************************************** */
/* end of file: csp_comp_cget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_comp_chck.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif



__CSP_FUNC UWORD  csp_comp_check (COMP_NUM num)
{
    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
        )

        return __CSP_ACC_MASK_REG(BFA_RD, COMP.CMCON, (COMP_1 | COMP_2) & num);

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
          )

        return __CSP_ACC_MASK_REG(BFA_RD, COMP_REF.CMSTAT, (COMP_1 | COMP_2 | COMP_3) & num);

    #elif (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

        return __CSP_ACC_MASK_REG(BFA_RD, COMP_REF.CMSTAT, (COMP_1 | COMP_2) & num);

    #endif
}




/* ********************************************************************************************** */
/* end of file: csp_comp_chck.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_comp_cset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif


#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
    )

    __CSP_FUNC void  csp_comp_conf_set (UWORD val)
    {
        COMP.CMCON  = val;
    }

#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
      )

    __CSP_FUNC void  csp_comp_conf_set (COMP_TYPE *comp, UWORD val)
    {
        comp->CMCON = val;
    }

#endif





/* ********************************************************************************************** */
/* end of file: csp_comp_cset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_comp_rget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_comp_ref_get (void)
{
    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
        )

        return (COMP.CVRCON);

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
          )

        return (COMP_REF.CVRCON);

    #endif
}




/* ********************************************************************************************** */
/* end of file: csp_comp_rget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_comp_rset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_comp_ref_set (UWORD val)
{
    #if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
        )

        COMP.CVRCON = val;

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
          )

        COMP_REF.CVRCON = val;

    #endif
}




/* ********************************************************************************************** */
/* end of file: csp_comp_rset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_comp_sclr.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
    )

    __CSP_FUNC void  csp_comp_stat_clr(COMP_STAT mask)
    {
        __CSP_ACC_MASK_REG(BFA_CLR, COMP.CMCON, __COMP_STAT_CLR_MASK, mask);
    }

#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
      )

    __CSP_FUNC void  csp_comp_stat_clr(COMP_TYPE *comp, COMP_STAT mask)
    {
        __CSP_ACC_MASK_VAL(BFA_CLR, comp->CMCON, __COMP_STAT_CLR_MASK, mask);
    }

#endif


/* ********************************************************************************************** */
/* end of file: comp.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_comp_sget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
    )

    __CSP_FUNC COMP_STAT  csp_comp_stat_get (void)
    {
        return (COMP_STAT)(COMP.CMCON);
    }

#elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
       (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
      )

    __CSP_FUNC COMP_STAT  csp_comp_stat_get (COMP_TYPE *comp)
    {
        return (COMP_STAT)(comp->CMCON);
    }

#endif


/* ********************************************************************************************** */
/* end of file: comp.c */
