/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_SPI_H
#define __CSP_SPI_H

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110) ||  \
     (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)       \
    )

    /* SPI programming model */
    /* --------------------- */

    typedef struct __SPI_TYPE
    {
        volatile UWORD SPISTAT;
        volatile UWORD SPICON1;
        volatile UWORD SPICON2;
        __CSP_SKIP_BYTE(2);
        volatile UWORD SPIBUF;
    } SPI_TYPE;


    #if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

        /* SPI base addresses */
        /* ------------------ */
        #define SPI1_BASE_ADDR      0x0240

        /* Declare SPI models */
        /* ------------------ */
        extern SPI_TYPE SPI1 __CSP_SFR(SPI1_BASE_ADDR);

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
         (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)      \
        )

        /* SPI base addresses */
        /* ------------------ */

        #define SPI1_BASE_ADDR      0x0240
        #define SPI2_BASE_ADDR      0x0260

        /* Declare SPI models */
        /* ------------------ */

        extern SPI_TYPE SPI1 __CSP_SFR(SPI1_BASE_ADDR);
        extern SPI_TYPE SPI2 __CSP_SFR(SPI2_BASE_ADDR);

    #elif ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
           (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
          )

        /* SPI base addresses */
        /* ------------------ */

        #define SPI1_BASE_ADDR      0x0240
        #define SPI2_BASE_ADDR      0x0260
        #define SPI3_BASE_ADDR      0x0280

        /* Declare SPI models */
        /* ------------------ */

        extern SPI_TYPE SPI1 __CSP_SFR(SPI1_BASE_ADDR);
        extern SPI_TYPE SPI2 __CSP_SFR(SPI2_BASE_ADDR);
        extern SPI_TYPE SPI3 __CSP_SFR(SPI3_BASE_ADDR);

    #endif


    /* Internal API definitions */
    /* ------------------------ */

    #define __SPI_STAT_MASK         (SPI_STAT_SREG_EMPTY | SPI_STAT_OVF |           \
                                     SPI_STAT_RX_BUF_EMPTY | SPI_STAT_TX_BUF_FULL | \
                                     SPI_STAT_RX_BUF_FULL)

    #define __SPI_BAUD_MASK         (0x1F)
    #define __SPI_BAUD_FS           (0)
    #define __SPI_BAUD_FE           (4)

    #define __SPI_BC_FS             (8)
    #define __SPI_BC_FE             (10)

    #define __SPI_INT_MODE_FS       (2)
    #define __SPI_INT_MODE_FE       (4)

    #define __SPI_EN_BIT            (15)

    #define __SPI_STAT_RX_BUF_FULL_BIT  (0)
    #define __SPI_STAT_TX_BUF_FULL_BIT  (1)

    /* Definitions for I2C API */
    /* ----------------------- */

    /* csp_spi_conf_set() and csp_spi_conf_get parameters mask */

    #define SPI_EN                  (1ULL << 47)    /* Enables module */
    #define SPI_DIS                 (0ULL << 47)    /* Disables module */

    #define SPI_IDLE_STOP           (1ULL << 45)    /* Discontinue module operation when device enters Idle mode */
    #define SPI_IDLE_CON            (0ULL << 45)    /* Continue module operation in Idle mode */

    /* ONLY FOR COMPATIBILITY */
    #define SPI_INT_MODE_1          (0ULL << 34)    /* Interrupt when the last data in the receive buffer is read, as a result, the buffer is empty */
    #define SPI_INT_MODE_2          (1ULL << 34)    /* Interrupt when data is available in receive buffer */
    #define SPI_INT_MODE_3          (2ULL << 34)    /* Interrupt when SPIx receive buffer is 3/4 or more full */
    #define SPI_INT_MODE_4          (3ULL << 34)    /* Interrupt when SPIx receive buffer is full */
    #define SPI_INT_MODE_5          (4ULL << 34)    /* Interrupt when one data is shifted into the SPIxSR, as a result, the TX FIFO has one open spot */
    #define SPI_INT_MODE_6          (5ULL << 34)    /* Interrupt when the last bit is shifted out of SPIxSR, now the transmit is complete */
    #define SPI_INT_MODE_7          (6ULL << 34)    /* Interrupt when last bit is shifted into SPIxSR, as a result, the TX FIFO is empty */
    #define SPI_INT_MODE_8          (7ULL << 34)    /* Interrupt when SPIx transmit buffer is full */


    #define SPI_PIN_SCK_EN          (0ULL << 28)    /* Internal SPI clock is enabled */
    #define SPI_PIN_SCK_DIS         (1ULL << 28)    /* Internal SPI clock is disabled, pin functions as I/O */

    #define SPI_PIN_SDO_EN          (0ULL << 27)    /* SDO pin is controlled by the module */
    #define SPI_PIN_SDO_DIS         (1ULL << 27)    /* SDO pin is not used by module, pin functions as I/O */

    #define SPI_8_BIT               (0ULL << 26)    /* Communication is byte-wide (8 bits) */
    #define SPI_16_BIT              (1ULL << 26)    /* Communication is word-wide (16 bits) */

    #define SPI_DATA_SAMPLE_END     (1ULL << 25)    /* Input data sampled at end of data output time */
    #define SPI_DATA_SAMPLE_MIDDLE  (0ULL << 25)    /* Input data sampled at middle of data output time */

    #define SPI_DATA_OUT_ACT_INACT  (1ULL << 24)    /* Serial output data changes on transition from active clock state to Idle clock state */
    #define SPI_DATA_OUT_INACT_ACT  (0ULL << 24)    /* Serial output data changes on transition from Idle clock state to active clock state */

    #define SPI_PIN_SS_EN           (1ULL << 23)    /* SS pin used for Slave mode */
    #define SPI_PIN_SS_DIS          (0ULL << 23)    /* SS pin not used by module, pin controlled by port function */

    #define SPI_CLK_IDLE_1          (1ULL << 22)    /* Idle state for clock is a high level; active state is a low level */
    #define SPI_CLK_IDLE_0          (0ULL << 22)    /* Idle state for clock is a low level; active state is a high level */

    #define SPI_MODE_MASTER         (1ULL << 21)    /* Master mode */
    #define SPI_MODE_SLAVE          (0ULL << 21)    /* Slave mode */


    #define SPI_FRAME_EN            (1ULL << 15)    /* Framed SPIx support enabled */
    #define SPI_FRAME_DIS           (0ULL << 15)    /* Framed SPIx support disabled */

    #define SPI_FRAME_INPUT         (1ULL << 14)    /* Frame sync pulse input (slave) */
    #define SPI_FRAME_OUTPUT        (0ULL << 14)    /* Frame sync pulse output (master) */

    #define SPI_FRAME_ACTIVE_1      (1ULL << 13)    /* Frame sync pulse is active-high */
    #define SPI_FRAME_ACTIVE_0      (0ULL << 13)    /* Frame sync pulse is active-low */

    #define SPI_FRAME_FB_WITH       (1ULL <<  1)    /* Frame sync pulse coincides with first bit clock */
    #define SPI_FRAME_FB_BEFORE     (0ULL <<  1)    /* Frame sync pulse precedes first bit clock */

    #define SPI_ENCH_BUFF_EN        (1ULL <<  0)    /* Enhanced Buffer enabled */
    #define SPI_ENCH_BUFF_DIS       (0ULL <<  0)    /* Enhanced Buffer disabled (Legacy mode) */


    /* csp_spi_stat_get() and csp_spi_stat_clr() parameters type */

    typedef enum __SPI_STAT
    {
        SPI_STAT_SREG_EMPTY   = (1 << 7),   /* RO  - SPIx Shift register is empty and ready to send or receive  */
        SPI_STAT_OVF          = (1 << 6),   /* R/C - New byte/word is completely received; CPU has not read previous data in the SPIBUF register  */
        SPI_STAT_RX_BUF_EMPTY = (1 << 5),   /* R/W - RX FIFO is empty */
        SPI_STAT_TX_BUF_FULL  = (1 << 1),   /* RO  - Transmit not yet started, SPIxTXB is full  */
        SPI_STAT_RX_BUF_FULL  = (1 << 0)    /* RO  - Receive complete, SPIxRXB is full  */

    } SPI_STAT;

    /* csp_spi_int_mode_set() and csp_spi_int_mode_get() parameters type */
    typedef enum __SPI_INT_MODE
    {
        SPI_INT_MODE_RX_EMPTY       = (0),    /* Interrupt when the last data in the receive buffer is read, as a result, the buffer is empty */
        SPI_INT_MODE_RX_AVAILABE    = (1),    /* Interrupt when data is available in receive buffer */
        SPI_INT_MODE_RX_ALMOST_FULL = (2),    /* Interrupt when SPIx receive buffer is 3/4 or more full */
        SPI_INT_MODE_RX_FULL        = (3),    /* Interrupt when SPIx receive buffer is full */
        SPI_INT_MODE_TX_BUF_FREE    = (4),    /* Interrupt when one data is shifted into the SPIxSR, as a result, the TX FIFO has one open spot */
        SPI_INT_MODE_TX_DONE        = (5),    /* Interrupt when the last bit is shifted out of SPIxSR, now the transmit is complete */
        SPI_INT_MODE_TX_EMPTY       = (6),    /* Interrupt when last bit is shifted into SPIxSR, as a result, the TX FIFO is empty */
        SPI_INT_MODE_TX_FULL        = (7)     /* Interrupt when SPIx transmit buffer is full */
    } SPI_INT_MODE;


    /* csp_spi_baud_set() and csp_spi_baud_get() parameters type */

    typedef enum __SPI_BAUD_PS
    {
        SPI_BAUD_1_1   = 31,
        SPI_BAUD_1_2   = 27,
        SPI_BAUD_1_3   = 23,
        SPI_BAUD_1_4   = 19,
        SPI_BAUD_1_5   = 15,
        SPI_BAUD_1_6   = 11,
        SPI_BAUD_1_7   = 7,
        SPI_BAUD_1_8   = 3,

        SPI_BAUD_1_12  = 22,
        SPI_BAUD_1_16  = 18,
        SPI_BAUD_1_20  = 14,
        SPI_BAUD_1_24  = 10,
        SPI_BAUD_1_28  = 6,
        SPI_BAUD_1_32  = 2,

        SPI_BAUD_1_48  = 21,
        SPI_BAUD_1_64  = 17,
        SPI_BAUD_1_80  = 13,
        SPI_BAUD_1_96  = 9,
        SPI_BAUD_1_112 = 5,
        SPI_BAUD_1_128 = 1,

        SPI_BAUD_1_192 = 20,
        SPI_BAUD_1_256 = 16,
        SPI_BAUD_1_320 = 12,
        SPI_BAUD_1_384 = 8,
        SPI_BAUD_1_448 = 4,
        SPI_BAUD_1_512 = 0

    } SPI_BAUD_PS;

#endif


/* External functions prototypes */
/* ----------------------------- */

#if !defined(__CSP_BUILD_LIB)

    /* Module API definition */

    #if defined(__CSP_DEBUG_SPI)

        void        csp_spi_en(SPI_TYPE *spi);
        void        csp_spi_dis(SPI_TYPE *spi);

        void        csp_spi_conf_set(SPI_TYPE *spi, U64 val);
        U64         csp_spi_conf_get(SPI_TYPE *spi);

        void            csp_spi_int_mode_set(SPI_TYPE *spi, SPI_INT_MODE val);
        SPI_INT_MODE    csp_spi_int_mode_get(SPI_TYPE *spi);

        void        csp_spi_baud_set(SPI_TYPE *spi, SPI_BAUD_PS val);
        SPI_BAUD_PS csp_spi_baud_get(SPI_TYPE *spi);

        SPI_STAT    csp_spi_stat_get(SPI_TYPE *spi);
        void        csp_spi_stat_clr(SPI_TYPE *spi, SPI_STAT mask);

        void        csp_spi_put(SPI_TYPE *spi, UWORD val);
        void        csp_spi_put_bl(SPI_TYPE *spi, UWORD val);

        UWORD       csp_spi_get(SPI_TYPE *spi);

        UWORD       csp_spi_exch(SPI_TYPE *spi, UWORD val);

        UWORD       csp_spi_bufcnt_get(SPI_TYPE *spi);

        
    #else

        #include "csp_spi.c"

    #endif

#endif

#endif /* #ifndef __CSP_SPI_H */
/* ********************************************************************************************** */
/* end of file: csp_spi.h */

