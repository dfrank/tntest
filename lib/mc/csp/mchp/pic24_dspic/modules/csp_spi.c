/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_bcgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_spi_bufcnt_get (SPI_TYPE *spi)
{
    return __CSP_ACC_RANG_VAL(BFA_RD, spi->SPISTAT, __SPI_BC_FS, __SPI_BC_FE);
}

/* ********************************************************************************************** */
/* end of file: csp_spi_bcgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_bget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC SPI_BAUD_PS  csp_spi_baud_get (SPI_TYPE *spi)
{
    return (SPI_BAUD_PS)__CSP_ACC_RANG_VAL(BFA_RD, spi->SPICON1, __SPI_BAUD_FS, __SPI_BAUD_FE);
}

/* ********************************************************************************************** */
/* end of file: csp_spi_bget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_bset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_spi_baud_set (SPI_TYPE *spi, SPI_BAUD_PS val)
{
    __CSP_ACC_RANG_VAL(BFA_WR, spi->SPICON1, __SPI_BAUD_FS, __SPI_BAUD_FE, val);
}

/* ********************************************************************************************** */
/* end of file: csp_spi_bset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_cget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC U64  csp_spi_conf_get (SPI_TYPE *spi)
{
    AL_EXT_64 ret;

    ret.word_0 = spi->SPICON2;
    ret.word_1 = __CSP_ACC_MASK_VAL(BFA_RD, spi->SPICON1, (~__SPI_BAUD_MASK));
    ret.word_2 = spi->SPISTAT;
    ret.word_3 = 0;

    return ret.full;
}

/* ********************************************************************************************** */
/* end of file: csp_spi_cget.c */

/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_cset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_spi_conf_set (SPI_TYPE *spi, U64 val)
{
    spi->SPICON2  = ((AL_EXT_64)val).word_0;
    __CSP_ACC_MASK_VAL(BFA_WR, spi->SPICON1, ~(__SPI_BAUD_MASK), ((AL_EXT_64)val).word_1);
    spi->SPISTAT  = ((AL_EXT_64)val).word_2;
}

/* ********************************************************************************************** */
/* end of file: csp_spi_cset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_disb.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_spi_dis (SPI_TYPE *spi)
{
    __CSP_BIT_VAL_CLR(spi->SPISTAT, __SPI_EN_BIT);
}

/* ********************************************************************************************** */
/* end of file: csp_spi_disb.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_enbl.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_spi_en (SPI_TYPE *spi)
{
    __CSP_BIT_VAL_SET(spi->SPISTAT, __SPI_EN_BIT);
}

/* ********************************************************************************************** */
/* end of file: csp_spi_enbl.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_exch.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_spi_exch (SPI_TYPE *spi, UWORD val)
{
    spi->SPIBUF = val;
    while (!(__CSP_BIT_VAL_GET(spi->SPISTAT, __SPI_STAT_RX_BUF_FULL_BIT)));
    return spi->SPIBUF;
}

/* ********************************************************************************************** */
/* end of file: csp_spi_put.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_get.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UWORD  csp_spi_get (SPI_TYPE *spi)
{
    return (spi->SPIBUF);
}

/* ********************************************************************************************** */
/* end of file: spi.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_imgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

           
__CSP_FUNC SPI_INT_MODE csp_spi_int_mode_get(SPI_TYPE *spi)
{
    return (SPI_INT_MODE)__CSP_ACC_RANG_VAL(BFA_RD, spi->SPISTAT, __SPI_INT_MODE_FS, __SPI_INT_MODE_FE);
}

/* ********************************************************************************************** */
/* end of file: csp_spi_imgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_imst.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

           
__CSP_FUNC void  csp_spi_int_mode_set(SPI_TYPE *spi, SPI_INT_MODE val)
{
    __CSP_ACC_RANG_VAL(BFA_WR, spi->SPISTAT, __SPI_INT_MODE_FS, __SPI_INT_MODE_FE, val);
}

/* ********************************************************************************************** */
/* end of file: csp_spi_imst.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_publ.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_spi_put_bl (SPI_TYPE *spi, UWORD val)
{
    while (__CSP_BIT_VAL_GET(spi->SPISTAT, __SPI_STAT_TX_BUF_FULL_BIT)); /* SPI_STAT_TX_BUF_FULL */
    spi->SPIBUF = val;
}

/* ********************************************************************************************** */
/* end of file: csp_spi_put.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_put.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_spi_put (SPI_TYPE *spi, UWORD val)
{
    spi->SPIBUF = val;
}

/* ********************************************************************************************** */
/* end of file: csp_spi_put.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_sclr.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_spi_stat_clr (SPI_TYPE *spi, SPI_STAT mask)
{
    __CSP_ACC_MASK_VAL(BFA_CLR, spi->SPISTAT, __SPI_STAT_MASK, mask);
}

/* ********************************************************************************************** */
/* end of file: csp_spi_sclr.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_sget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC SPI_STAT  csp_spi_stat_get (SPI_TYPE *spi)
{
    return (SPI_STAT)__CSP_ACC_MASK_VAL(BFA_RD, spi->SPISTAT, __SPI_STAT_MASK);
}

/* ********************************************************************************************** */
/* end of file: spi.c */
