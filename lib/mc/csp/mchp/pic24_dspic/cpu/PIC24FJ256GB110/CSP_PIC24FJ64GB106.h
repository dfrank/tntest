/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------

     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            P24FJ64GB106_info.h
     Description:

   ********************************************************************************************** */

#ifndef __P24FJ64GB106_INFO_H
#define __P24FJ64GB106_INFO_H

#define __CSP_CORE                  __CSP_CORE_PIC24
#define __CSP_FAMILY                __CSP_FAMILY_PIC24FJ256GB110

#define __CSP_PRG_MEM_SIZE          __CSP_PRG_MEM_SIZE_64K
#define __CSP_DAT_MEM_SIZE          __CSP_DAT_MEM_SIZE_16K

#define __CSP_PIN_COUNT             __CSP_PIN_COUNT_64

#endif /* #ifndef __P24FJ64GB106_INFO_H */
/* ********************************************************************************************** */
/* end of file: P24FJ64GB106_info.h*/

