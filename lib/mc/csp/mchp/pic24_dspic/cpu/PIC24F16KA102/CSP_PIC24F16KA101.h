/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008-2010, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------

     Copyright � 2008-2010 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            P24F16KA101_info.h
     Description:

   ********************************************************************************************** */

#ifndef __P24F08KA101_INFO_H
#define __P24F08KA101_INFO_H

#define __CSP_CORE                  __CSP_CORE_PIC24
#define __CSP_FAMILY                __CSP_FAMILY_PIC24F16KA102

#define __CSP_PRG_MEM_SIZE          __CSP_PRG_MEM_SIZE_16K
#define __CSP_DAT_MEM_SIZE          __CSP_DAT_MEM_SIZE_1_5K

#define __CSP_PIN_COUNT             __CSP_PIN_COUNT_20

#endif /* #ifndef __P24F08KA101_INFO_H */
/* ********************************************************************************************** */
/* end of file: P24FJ128GA006_info.h*/

