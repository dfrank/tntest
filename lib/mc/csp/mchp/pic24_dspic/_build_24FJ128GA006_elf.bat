@echo off

REM This commands call bat-file, that compile TNKernel source files for PIC24 and dsPIC Microchip cores and archive it to library file
REM
REM Second parameter - suffix for output library file in \LIB folder
REM
REM Third poarameter - sources compilation optimization level (See MPLAB C30 User Guide, 3.5.6 for reference)
REM - 0 - Do not optimize
REM - 1 - Optimize
REM - 2 - Optimize even more
REM - 3 - Optimize yet more
REM - s - Optimize for speed
REM
REM Fourth parameter - used code model (See MPLAB C30 User Guide, table 3.2)
REM - large-data - use it for all devices with Data Memory large then 6 kBytes
REM - small-data - use it for all other devices
REM
REM Fifth parameter - obj and lib type
REM - elf  - ELF mode
REM - coff - COFF mode
REM

REM if exist cspi_revision.h del cspi_revision.h
REM SubWCRev ..\ cspi_revision.h.tpl cspi_revision.h -n

call _xcmp.bat 24FJ128GA006 s small-data elf

beep
pause
