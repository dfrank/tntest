@echo off
if exist *.o del *.o

echo.
echo [PIC24/dsPIC33 CSP]: Start building PIC24/dsPIC33 CSP lib
echo.

REM CSP CORE MODULE
echo [PIC24/dsPIC33 CSP]: Core module...

pic30-gcc -mcpu=%1 -c -x c "modules\\core\\csp_core_disg.c"  -o "csp_core_disg.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\core\\csp_core_prig.c"  -o "csp_core_prig.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\core\\csp_core_prip.c"  -o "csp_core_prip.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP OSCILLATOR MODULE
echo [PIC24/dsPIC33 CSP]: Oscillator module...

pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_cset.c"   -o "csp_osc_cset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_cget.c"   -o "csp_osc_cget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_rcst.c"   -o "csp_osc_rcst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_rcgt.c"   -o "csp_osc_rcgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_dcgt.c"   -o "csp_osc_dcgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_dcst.c"   -o "csp_osc_dcst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_dzgt.c"   -o "csp_osc_dzgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_dzst.c"   -o "csp_osc_dzst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_fcgt.c"   -o "csp_osc_fcgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_fcst.c"   -o "csp_osc_fcst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_frgt.c"   -o "csp_osc_frgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_frst.c"   -o "csp_osc_frst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_get.c"    -o "csp_osc_get.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_lock.c"   -o "csp_osc_lock.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_schk.c"   -o "csp_osc_schk.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_sdis.c"   -o "csp_osc_sdis.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_seen.c"   -o "csp_osc_seen.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_set.c"    -o "csp_osc_set.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_stcl.c"   -o "csp_osc_stcl.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_stgt.c"   -o "csp_osc_stgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\osc\\csp_osc_unlk.c"   -o "csp_osc_unlk.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-as  -g  -omf=%4      "modules\\osc\\csp_osc_olwr.S"   -o "csp_osc_olwr.o"
pic30-as  -g  -omf=%4      "modules\\osc\\csp_osc_ohwr.S"   -o "csp_osc_ohwr.o"

REM CSP RESET MODULE
echo [PIC24/dsPIC33 CSP]: Reset module...

pic30-gcc -mcpu=%1 -c -x c "modules\\res\\csp_res_cfgt.c"   -o "csp_res_cfgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\res\\csp_res_cfst.c"   -o "csp_res_cfst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\res\\csp_res_sres.c"   -o "csp_res_sres.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\res\\csp_res_stcl.c"   -o "csp_res_stcl.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\res\\csp_res_stgt.c"   -o "csp_res_stgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\res\\csp_res_wdtc.c"   -o "csp_res_wdtc.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\res\\csp_res_wdte.c"   -o "csp_res_wdte.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\res\\csp_res_wdtd.c"   -o "csp_res_wdtd.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP INTERRUPT MODULE
echo [PIC24/dsPIC33 CSP]: Interrupt module...

pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_cfgt.c"   -o "csp_int_cfgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_cfst.c"   -o "csp_int_cfst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_chck.c"   -o "csp_int_chck.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_dis.c"    -o "csp_int_dis.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_en.c"     -o "csp_int_en.o"    -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_exgt.c"   -o "csp_int_exgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_exst.c"   -o "csp_int_exst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_flck.c"   -o "csp_int_flck.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_flcl.c"   -o "csp_int_flcl.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_flst.c"   -o "csp_int_flst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_prgt.c"   -o "csp_int_prgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_prst.c"   -o "csp_int_prst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_tfck.c"   -o "csp_int_tfck.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_tfcl.c"   -o "csp_int_tfcl.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_tfst.c"   -o "csp_int_tfst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\int\\csp_int_sget.c"   -o "csp_int_sget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP GPIO MODULE
echo [PIC24/dsPIC33 CSP]: GPIO module...

pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_pchk.c"  -o "csp_gpio_pchk.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_pclr.c"  -o "csp_gpio_pclr.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_pdcg.c"  -o "csp_gpio_pdcg.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_pdcs.c"  -o "csp_gpio_pdcs.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_pdgt.c"  -o "csp_gpio_pdgt.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_pdin.c"  -o "csp_gpio_pdin.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_pdou.c"  -o "csp_gpio_pdou.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_pdtg.c"  -o "csp_gpio_pdtg.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_pods.c"  -o "csp_gpio_pods.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_poen.c"  -o "csp_gpio_poen.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_pogt.c"  -o "csp_gpio_pogt.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_pset.c"  -o "csp_gpio_pset.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_ptgl.c"  -o "csp_gpio_ptgl.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_tclr.c"  -o "csp_gpio_tclr.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_tdgt.c"  -o "csp_gpio_tdgt.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_tdin.c"  -o "csp_gpio_tdin.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_tdou.c"  -o "csp_gpio_tdou.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_tdtg.c"  -o "csp_gpio_tdtg.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_tget.c"  -o "csp_gpio_tget.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_tods.c"  -o "csp_gpio_tods.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_toen.c"  -o "csp_gpio_toen.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_togt.c"  -o "csp_gpio_togt.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_tset.c"  -o "csp_gpio_tset.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_ttgl.c"  -o "csp_gpio_ttgl.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\gpio\\csp_gpio_twrt.c"  -o "csp_gpio_twrt.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP PPS MODULE
echo [PIC24/dsPIC33 CSP]: PPS module...

pic30-gcc -mcpu=%1 -c -x c "modules\\pps\\csp_pps_ingt.c"   -o "csp_pps_ingt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pps\\csp_pps_inst.c"   -o "csp_pps_inst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pps\\csp_pps_lock.c"   -o "csp_pps_lock.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pps\\csp_pps_ougt.c"   -o "csp_pps_ougt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pps\\csp_pps_oust.c"   -o "csp_pps_oust.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pps\\csp_pps_unlk.c"   -o "csp_pps_unlk.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-as  -g  -omf=%4      "modules\\pps\\csp_pps_artn.S"   -o "csp_pps_artn.o"

REM CSP CN MODULE
echo [PIC24/dsPIC33 CSP]: CN module...

pic30-gcc -mcpu=%1 -c -x c "modules\\cn\\csp_cn_pden.c"    -o "csp_cn_pden.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\cn\\csp_cn_pdds.c"    -o "csp_cn_pdds.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\cn\\csp_cn_pdwr.c"    -o "csp_cn_pdwr.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\cn\\csp_cn_pdgt.c"    -o "csp_cn_pdgt.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\cn\\csp_cn_disb.c"    -o "csp_cn_disb.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\cn\\csp_cn_enbl.c"    -o "csp_cn_enbl.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\cn\\csp_cn_getc.c"    -o "csp_cn_getc.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\cn\\csp_cn_puds.c"    -o "csp_cn_puds.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\cn\\csp_cn_puen.c"    -o "csp_cn_puen.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\cn\\csp_cn_pugt.c"    -o "csp_cn_pugt.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\cn\\csp_cn_puwr.c"    -o "csp_cn_puwr.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\cn\\csp_cn_wrte.c"    -o "csp_cn_wrte.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP TIMERS MODULE
echo [PIC24/dsPIC33 CSP]: Timers module...

pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_ucgt.c"   -o "csp_tmr_ucgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_ucst.c"   -o "csp_tmr_ucst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_udis.c"   -o "csp_tmr_udis.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_uenb.c"   -o "csp_tmr_uenb.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_uget.c"   -o "csp_tmr_uget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_upgt.c"   -o "csp_tmr_upgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_upst.c"   -o "csp_tmr_upst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_uset.c"   -o "csp_tmr_uset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_acgt.c"   -o "csp_tmr_acgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_acst.c"   -o "csp_tmr_acst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_adis.c"   -o "csp_tmr_adis.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_aenb.c"   -o "csp_tmr_aenb.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_aget.c"   -o "csp_tmr_aget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_apgt.c"   -o "csp_tmr_apgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_apst.c"   -o "csp_tmr_apst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_aset.c"   -o "csp_tmr_aset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_bcgt.c"   -o "csp_tmr_bcgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_bcst.c"   -o "csp_tmr_bcst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_bdis.c"   -o "csp_tmr_bdis.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_benb.c"   -o "csp_tmr_benb.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_bget.c"   -o "csp_tmr_bget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_bpgt.c"   -o "csp_tmr_bpgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_bpst.c"   -o "csp_tmr_bpst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_bset.c"   -o "csp_tmr_bset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_ccgt.c"   -o "csp_tmr_ccgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_ccst.c"   -o "csp_tmr_ccst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_cdis.c"   -o "csp_tmr_cdis.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_cenb.c"   -o "csp_tmr_cenb.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_cget.c"   -o "csp_tmr_cget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_cpgt.c"   -o "csp_tmr_cpgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_cpst.c"   -o "csp_tmr_cpst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_cset.c"   -o "csp_tmr_cset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_tget.c"   -o "csp_tmr_tget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_tpgt.c"   -o "csp_tmr_tpgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_tpst.c"   -o "csp_tmr_tpst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\tmr\\csp_tmr_tset.c"   -o "csp_tmr_tset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP INPUT CAPTURE MODULE
echo [PIC24/dsPIC33 CSP]: Input capture module...

pic30-gcc -mcpu=%1 -c -x c "modules\\ic\\csp_ic_tget.c"    -o "csp_ic_tget.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\ic\\csp_ic_tmst.c"    -o "csp_ic_tmst.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\ic\\csp_ic_tmgt.c"    -o "csp_ic_tmgt.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\ic\\csp_ic_ttst.c"    -o "csp_ic_ttst.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\ic\\csp_ic_ttgt.c"    -o "csp_ic_ttgt.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\ic\\csp_ic_cget.c"    -o "csp_ic_cget.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\ic\\csp_ic_cset.c"    -o "csp_ic_cset.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\ic\\csp_ic_get.c"     -o "csp_ic_get.o"    -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\ic\\csp_ic_sget.c"    -o "csp_ic_sget.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP OUTPUT COMPARE MODULE
echo [PIC24/dsPIC33 CSP]: Output compare module...

pic30-gcc -mcpu=%1 -c -x c "modules\\oc\\csp_oc_sclr.c"    -o "csp_oc_sclr.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\oc\\csp_oc_tmst.c"    -o "csp_oc_tmst.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\oc\\csp_oc_tmgt.c"    -o "csp_oc_tmgt.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\oc\\csp_oc_cget.c"    -o "csp_oc_cget.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\oc\\csp_oc_cset.c"    -o "csp_oc_cset.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\oc\\csp_oc_rget.c"    -o "csp_oc_rget.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\oc\\csp_oc_rset.c"    -o "csp_oc_rset.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\oc\\csp_oc_rsgt.c"    -o "csp_oc_rsgt.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\oc\\csp_oc_rsst.c"    -o "csp_oc_rsst.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\oc\\csp_oc_sget.c"    -o "csp_oc_sget.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP I2C MODULE
echo [PIC24/dsPIC33 CSP]: I2C module...

pic30-gcc -mcpu=%1 -c -x c "modules\\i2c\\csp_i2c_aget.c"   -o "csp_i2c_aget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\i2c\\csp_i2c_amgt.c"   -o "csp_i2c_amgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\i2c\\csp_i2c_amst.c"   -o "csp_i2c_amst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\i2c\\csp_i2c_aset.c"   -o "csp_i2c_aset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\i2c\\csp_i2c_bget.c"   -o "csp_i2c_bget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\i2c\\csp_i2c_bset.c"   -o "csp_i2c_bset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\i2c\\csp_i2c_cget.c"   -o "csp_i2c_cget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\i2c\\csp_i2c_comm.c"   -o "csp_i2c_comm.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\i2c\\csp_i2c_cset.c"   -o "csp_i2c_cset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\i2c\\csp_i2c_get.c"    -o "csp_i2c_get.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\i2c\\csp_i2c_put.c"    -o "csp_i2c_put.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\i2c\\csp_i2c_sclr.c"   -o "csp_i2c_sclr.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\i2c\\csp_i2c_sget.c"   -o "csp_i2c_sget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP SPI MODULE
echo [PIC24/dsPIC33 CSP]: SPI module...

pic30-gcc -mcpu=%1 -c -x c "modules\\spi\\csp_spi_publ.c"   -o "csp_spi_publ.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\spi\\csp_spi_exch.c"   -o "csp_spi_exch.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\spi\\csp_spi_disb.c"   -o "csp_spi_disb.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\spi\\csp_spi_enbl.c"   -o "csp_spi_enbl.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\spi\\csp_spi_bcgt.c"   -o "csp_spi_bcgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\spi\\csp_spi_bget.c"   -o "csp_spi_bget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\spi\\csp_spi_bset.c"   -o "csp_spi_bset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\spi\\csp_spi_cget.c"   -o "csp_spi_cget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\spi\\csp_spi_cset.c"   -o "csp_spi_cset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\spi\\csp_spi_get.c"    -o "csp_spi_get.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\spi\\csp_spi_put.c"    -o "csp_spi_put.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\spi\\csp_spi_sclr.c"   -o "csp_spi_sclr.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\spi\\csp_spi_sget.c"   -o "csp_spi_sget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\spi\\csp_spi_imst.c"   -o "csp_spi_imst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\spi\\csp_spi_imgt.c"   -o "csp_spi_imgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP UART MODULE
echo [PIC24/dsPIC33 CSP]: UART module...

pic30-gcc -mcpu=%1 -c -x c "modules\\uart\\csp_uart_bget.c"  -o "csp_uart_bget.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\uart\\csp_uart_bset.c"  -o "csp_uart_bset.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\uart\\csp_uart_cget.c"  -o "csp_uart_cget.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\uart\\csp_uart_comm.c"  -o "csp_uart_comm.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\uart\\csp_uart_cset.c"  -o "csp_uart_cset.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\uart\\csp_uart_get.c"   -o "csp_uart_get.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\uart\\csp_uart_put.c"   -o "csp_uart_put.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\uart\\csp_uart_sclr.c"  -o "csp_uart_sclr.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\uart\\csp_uart_sget.c"  -o "csp_uart_sget.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP ADC MODULE
echo [PIC24/dsPIC33 CSP]: ADC module...

pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_aget.c"   -o "csp_adc_aget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_aset.c"   -o "csp_adc_aset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_aclr.c"   -o "csp_adc_aclr.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_cget.c"   -o "csp_adc_cget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_clgt.c"   -o "csp_adc_clgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_clst.c"   -o "csp_adc_clst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_comm.c"   -o "csp_adc_comm.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_cset.c"   -o "csp_adc_cset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_get.c"    -o "csp_adc_get.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_magt.c"   -o "csp_adc_magt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_mast.c"   -o "csp_adc_mast.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_mbgt.c"   -o "csp_adc_mbgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_mbst.c"   -o "csp_adc_mbst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_scgt.c"   -o "csp_adc_scgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_sclr.c"   -o "csp_adc_sclr.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_scst.c"   -o "csp_adc_scst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_sccl.c"   -o "csp_adc_sccl.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_sget.c"   -o "csp_adc_sget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_tmgt.c"   -o "csp_adc_tmgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_tmst.c"   -o "csp_adc_tmst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_enbl.c"   -o "csp_adc_enbl.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\adc\\csp_adc_disb.c"   -o "csp_adc_disb.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP COMPARATORS MODULE
echo [PIC24/dsPIC33 CSP]: Comparators module...

pic30-gcc -mcpu=%1 -c -x c "modules\\comp\\csp_comp_cget.c"  -o "csp_comp_cget.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\comp\\csp_comp_chck.c"  -o "csp_comp_chck.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\comp\\csp_comp_cset.c"  -o "csp_comp_cset.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\comp\\csp_comp_rget.c"  -o "csp_comp_rget.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\comp\\csp_comp_rset.c"  -o "csp_comp_rset.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\comp\\csp_comp_sclr.c"  -o "csp_comp_sclr.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\comp\\csp_comp_sget.c"  -o "csp_comp_sget.o" -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP CRC MODULE
echo [PIC24/dsPIC33 CSP]: CRC module...

pic30-gcc -mcpu=%1 -c -x c "modules\\crc\\csp_crc_bput.c"   -o "csp_crc_bput.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\crc\\csp_crc_cget.c"   -o "csp_crc_cget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\crc\\csp_crc_cset.c"   -o "csp_crc_cset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\crc\\csp_crc_fcgt.c"   -o "csp_crc_fcgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\crc\\csp_crc_pget.c"   -o "csp_crc_pget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\crc\\csp_crc_pset.c"   -o "csp_crc_pset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\crc\\csp_crc_rget.c"   -o "csp_crc_rget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\crc\\csp_crc_rset.c"   -o "csp_crc_rset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\crc\\csp_crc_sget.c"   -o "csp_crc_sget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\crc\\csp_crc_wput.c"   -o "csp_crc_wput.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP PMP MODULE
echo [PIC24/dsPIC33 CSP]: PMP module...

pic30-gcc -mcpu=%1 -c -x c "modules\\pmp\\csp_pmp_adis.c"   -o "csp_pmp_adis.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmp\\csp_pmp_aenb.c"   -o "csp_pmp_aenb.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmp\\csp_pmp_aget.c"   -o "csp_pmp_aget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmp\\csp_pmp_aset.c"   -o "csp_pmp_aset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmp\\csp_pmp_bchk.c"   -o "csp_pmp_bchk.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmp\\csp_pmp_cget.c"   -o "csp_pmp_cget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmp\\csp_pmp_cset.c"   -o "csp_pmp_cset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmp\\csp_pmp_msgt.c"   -o "csp_pmp_msgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmp\\csp_pmp_mspt.c"   -o "csp_pmp_mspt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmp\\csp_pmp_mswr.c"   -o "csp_pmp_mswr.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmp\\csp_pmp_sclr.c"   -o "csp_pmp_sclr.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmp\\csp_pmp_sget.c"   -o "csp_pmp_sget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmp\\csp_pmp_slgt.c"   -o "csp_pmp_slgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmp\\csp_pmp_slpt.c"   -o "csp_pmp_slpt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmp\\csp_pmp_mspl.c"   -o "csp_pmp_mspl.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP RTC MODULE
echo [PIC24/dsPIC33 CSP]: RTCC module...

pic30-gcc -mcpu=%1 -c -x c "modules\\rtc\\csp_rtc_acgt.c"   -o "csp_rtc_acgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\rtc\\csp_rtc_acst.c"   -o "csp_rtc_acst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\rtc\\csp_rtc_aget.c"   -o "csp_rtc_aget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\rtc\\csp_rtc_aset.c"   -o "csp_rtc_aset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\rtc\\csp_rtc_cget.c"   -o "csp_rtc_cget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\rtc\\csp_rtc_clgt.c"   -o "csp_rtc_clgt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\rtc\\csp_rtc_clst.c"   -o "csp_rtc_clst.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\rtc\\csp_rtc_cset.c"   -o "csp_rtc_cset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\rtc\\csp_rtc_get.c"    -o "csp_rtc_get.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\rtc\\csp_rtc_set.c"    -o "csp_rtc_set.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\rtc\\csp_rtc_sget.c"   -o "csp_rtc_sget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-as  -g  -omf=%4      "modules\\rtc\\csp_rtc_artn.S"   -o "csp_rtc_artn.o"


REM CSP NVM MODULE
echo [PIC24/dsPIC33 CSP]: NVM module...

pic30-as  -g  -omf=%4      "modules\\nvm\\csp_nvm_comm.S"   -o "csp_nvm_comm.o"
pic30-as  -g  -omf=%4      "modules\\nvm\\csp_nvm_wget.S"   -o "csp_nvm_wget.o"
pic30-as  -g  -omf=%4      "modules\\nvm\\csp_nvm_wput.S"   -o "csp_nvm_wput.o"
pic30-gcc -mcpu=%1 -c -x c "modules\\nvm\\csp_nvm_bchk.c"   -o "csp_nvm_bchk.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP PMD MODULE
echo [PIC24/dsPIC33 CSP]: PMD module...

pic30-gcc -mcpu=%1 -c -x c "modules\\pmd\\csp_pmd_dis.c"    -o "csp_pmd_dis.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmd\\csp_pmd_en.c"     -o "csp_pmd_en.o"    -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\pmd\\csp_pmd_get.c"    -o "csp_pmd_get.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP CTMU MODULE
echo [PIC24/dsPIC33 CSP]: CTMU module...

pic30-gcc -mcpu=%1 -c -x c "modules\\ctmu\\csp_ctmu_cset.c"   -o "csp_ctmu_cset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\ctmu\\csp_ctmu_cget.c"   -o "csp_ctmu_cget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\ctmu\\csp_ctmu_cust.c"   -o "csp_ctmu_cust.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\ctmu\\csp_ctmu_cugt.c"   -o "csp_ctmu_cugt.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\ctmu\\csp_ctmu_comm.c"   -o "csp_ctmu_comm.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP HLVD MODULE
echo [PIC24/dsPIC33 CSP]: HLVD module...

pic30-gcc -mcpu=%1 -c -x c "modules\\hlvd\\csp_hlvd_cset.c"   -o "csp_hlvd_cset.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\hlvd\\csp_hlvd_cget.c"   -o "csp_hlvd_cget.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2

REM CSP Power Save MODULE
echo [PIC24/dsPIC33 CSP]: PSAVE module...

pic30-gcc -mcpu=%1 -c -x c "modules\\psave\\csp_psave_idle.c"   -o "csp_psave_idle.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\psave\\csp_psave_slep.c"   -o "csp_psave_slep.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\psave\\csp_psave_dslp.c"   -o "csp_psave_dslp.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\psave\\csp_psave_dsex.c"   -o "csp_psave_dsex.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\psave\\csp_psave_dscg.c"   -o "csp_psave_dscg.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\psave\\csp_psave_dsri.c"   -o "csp_psave_dsri.o"  -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2


REM CSP UTILS MODULE
echo [PIC24/dsPIC33 CSP]: Utilities module...

pic30-gcc -mcpu=%1 -c -x c "modules\\util\\csp_util_fcyg.c" -o "csp_util_fcyg.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\util\\csp_util_ttmn.c" -o "csp_util_ttmn.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\util\\csp_util_itmn.c" -o "csp_util_itmn.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\util\\csp_util_stmn.c" -o "csp_util_stmn.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\util\\csp_util_utmn.c" -o "csp_util_utmn.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\util\\csp_util_atmn.c" -o "csp_util_atmn.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\util\\csp_util_btpb.c" -o "csp_util_btpb.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2
pic30-gcc -mcpu=%1 -c -x c "modules\\util\\csp_util_pbtb.c" -o "csp_util_pbtb.o"   -g -D__CSP_BUILD_LIB -Wall -omf=%4 -msmall-code -m%3  -mconst-in-code -O%2




cd  _lib
if exist csp_%1_%4.a del csp_%1_%4.a
cd  ..

echo.
echo [PIC24/dsPIC33 CSP]: Building LIB...

pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_core_disg.o csp_core_prig.o csp_core_prip.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_osc_dcgt.o csp_osc_dcst.o csp_osc_dzgt.o csp_osc_dzst.o csp_osc_fcgt.o csp_osc_fcst.o csp_osc_frgt.o csp_osc_frst.o csp_osc_get.o csp_osc_lock.o csp_osc_schk.o csp_osc_sdis.o csp_osc_seen.o csp_osc_set.o csp_osc_stcl.o csp_osc_stgt.o csp_osc_unlk.o csp_osc_olwr.o csp_osc_ohwr.o csp_osc_rcst.o csp_osc_rcgt.o csp_osc_cset.o csp_osc_cget.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_res_cfgt.o csp_res_cfst.o csp_res_sres.o csp_res_stcl.o csp_res_stgt.o csp_res_wdtc.o csp_res_wdte.o csp_res_wdtd.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_int_cfgt.o csp_int_cfst.o csp_int_chck.o csp_int_dis.o csp_int_en.o csp_int_exgt.o csp_int_exst.o csp_int_flck.o csp_int_flcl.o csp_int_flst.o csp_int_prgt.o csp_int_prst.o csp_int_tfck.o csp_int_tfcl.o csp_int_tfst.o csp_int_sget.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_gpio_pchk.o csp_gpio_pclr.o csp_gpio_pdcg.o csp_gpio_pdcs.o csp_gpio_pdgt.o csp_gpio_pdin.o csp_gpio_pdou.o csp_gpio_pdtg.o csp_gpio_pods.o csp_gpio_poen.o csp_gpio_pogt.o csp_gpio_pset.o csp_gpio_ptgl.o csp_gpio_tclr.o csp_gpio_tdgt.o csp_gpio_tdin.o csp_gpio_tdou.o csp_gpio_tdtg.o csp_gpio_tget.o csp_gpio_tods.o csp_gpio_toen.o csp_gpio_togt.o csp_gpio_tset.o csp_gpio_ttgl.o csp_gpio_twrt.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_pps_ingt.o csp_pps_inst.o csp_pps_lock.o csp_pps_ougt.o csp_pps_oust.o csp_pps_unlk.o csp_pps_artn.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_cn_disb.o csp_cn_enbl.o csp_cn_getc.o csp_cn_puds.o csp_cn_puen.o csp_cn_pugt.o csp_cn_puwr.o csp_cn_wrte.o csp_cn_pden.o csp_cn_pdds.o csp_cn_pdwr.o csp_cn_pdgt.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_tmr_acgt.o csp_tmr_acst.o csp_tmr_adis.o csp_tmr_aenb.o csp_tmr_aget.o csp_tmr_apgt.o csp_tmr_apst.o csp_tmr_aset.o csp_tmr_bcgt.o csp_tmr_bcst.o csp_tmr_bdis.o csp_tmr_benb.o csp_tmr_bget.o csp_tmr_bpgt.o csp_tmr_bpst.o csp_tmr_bset.o csp_tmr_ccgt.o csp_tmr_ccst.o csp_tmr_cdis.o csp_tmr_cenb.o csp_tmr_cget.o csp_tmr_cpgt.o csp_tmr_cpst.o csp_tmr_cset.o csp_tmr_tget.o csp_tmr_tpgt.o csp_tmr_tpst.o csp_tmr_tset.o csp_tmr_ucgt.o csp_tmr_ucst.o csp_tmr_udis.o csp_tmr_uenb.o csp_tmr_uget.o csp_tmr_upgt.o csp_tmr_upst.o csp_tmr_uset.o  
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_ic_cget.o csp_ic_cset.o csp_ic_get.o csp_ic_sget.o csp_ic_tget.o csp_ic_tmst.o csp_ic_tmgt.o csp_ic_ttst.o csp_ic_ttgt.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_oc_cget.o csp_oc_cset.o csp_oc_rget.o csp_oc_rset.o csp_oc_rsgt.o csp_oc_rsst.o csp_oc_sget.o csp_oc_sclr.o csp_oc_tmst.o csp_oc_tmgt.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_i2c_aget.o csp_i2c_amgt.o csp_i2c_amst.o csp_i2c_aset.o csp_i2c_bget.o csp_i2c_bset.o csp_i2c_cget.o csp_i2c_comm.o csp_i2c_cset.o csp_i2c_get.o csp_i2c_put.o csp_i2c_sclr.o csp_i2c_sget.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_spi_bcgt.o csp_spi_bget.o csp_spi_bset.o csp_spi_cget.o csp_spi_cset.o csp_spi_get.o csp_spi_put.o csp_spi_sclr.o csp_spi_sget.o csp_spi_enbl.o csp_spi_disb.o csp_spi_exch.o csp_spi_publ.o csp_spi_imst.o csp_spi_imgt.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_uart_bget.o csp_uart_bset.o csp_uart_cget.o csp_uart_comm.o csp_uart_cset.o csp_uart_get.o csp_uart_put.o csp_uart_sclr.o csp_uart_sget.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_adc_aget.o csp_adc_aset.o csp_adc_cget.o csp_adc_clgt.o csp_adc_clst.o csp_adc_comm.o csp_adc_cset.o csp_adc_get.o csp_adc_magt.o csp_adc_mast.o csp_adc_mbgt.o csp_adc_mbst.o csp_adc_scgt.o csp_adc_sclr.o csp_adc_scst.o csp_adc_sget.o csp_adc_tmgt.o csp_adc_tmst.o csp_adc_aclr.o csp_adc_sccl.o csp_adc_enbl.o csp_adc_disb.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_comp_cget.o csp_comp_chck.o csp_comp_cset.o csp_comp_rget.o csp_comp_rset.o csp_comp_sclr.o csp_comp_sget.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_crc_bput.o csp_crc_cget.o csp_crc_cset.o csp_crc_fcgt.o csp_crc_pget.o csp_crc_pset.o csp_crc_rget.o csp_crc_rset.o csp_crc_sget.o csp_crc_wput.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_pmp_adis.o csp_pmp_aenb.o csp_pmp_aget.o csp_pmp_aset.o csp_pmp_bchk.o csp_pmp_cget.o csp_pmp_cset.o csp_pmp_msgt.o csp_pmp_mspt.o csp_pmp_mswr.o csp_pmp_sclr.o csp_pmp_sget.o csp_pmp_slgt.o csp_pmp_slpt.o csp_pmp_mspl.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_rtc_acgt.o csp_rtc_acst.o csp_rtc_aget.o csp_rtc_aset.o csp_rtc_cget.o csp_rtc_clgt.o csp_rtc_clst.o csp_rtc_cset.o csp_rtc_get.o csp_rtc_set.o csp_rtc_sget.o csp_rtc_artn.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_nvm_comm.o csp_nvm_wget.o csp_nvm_wput.o csp_nvm_bchk.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_pmd_dis.o csp_pmd_en.o csp_pmd_get.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_util_fcyg.o csp_util_ttmn.o csp_util_itmn.o csp_util_stmn.o csp_util_utmn.o csp_util_atmn.o csp_util_btpb.o csp_util_pbtb.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_ctmu_cset.o csp_ctmu_cget.o csp_ctmu_cust.o csp_ctmu_cugt.o csp_ctmu_comm.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_hlvd_cset.o csp_hlvd_cget.o
pic30-ar.exe -omf=%4 q "_lib\\csp_%1_%4.a" csp_psave_idle.o csp_psave_slep.o csp_psave_dslp.o csp_psave_dsex.o csp_psave_dscg.o csp_psave_dsri.o



if exist *.o del *.o
echo [PIC24/dsPIC33 CSP]: DONE
echo.

