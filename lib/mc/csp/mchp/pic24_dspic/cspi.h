/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_H
#define __CSP_H

#include <limits.h>
#include <stddef.h>

#include "csp_types.h"
#include "cspi_revision.h"
#include "cspi_bfa.h"


#ifndef __packed
#define __packed    __attribute__((packed))
#endif

#ifndef __far
#define __far       __attribute__((far))
#endif

#ifndef __rom
#define __rom       const
#endif


/* Global types definition */
/* ----------------------- */


#ifndef _U64_DEFINED
typedef unsigned long long   U64;
#define _U64_DEFINED
#endif


#ifndef _WORD_DEFINED
typedef          S16         WORD;
typedef          S32         PWORD;
#define _WORD_DEFINED
#endif

#ifndef _UWORD_DEFINED
typedef          U16         UWORD;
typedef          U16         UPWORD;
#define _UWORD_DEFINED
#endif

#ifndef _SWORD_DEFINED
typedef          S16         SWORD;
typedef          S16         SPWORD;
#define _SWORD_DEFINED
#endif



#ifndef _BOOL_DEFINED
typedef int_fast8_t     BOOL;
#define _BOOL_DEFINED
#endif

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#ifndef NULL
#define NULL    0
#endif



#ifndef _AL_EXT_64_DEFINED
typedef union __AL_EXT_64
{
    U64 full;
    struct
    {
        U32 dword_0;
        U32 dword_1;
    };
    struct
    {
        U16 word_0;
        U16 word_1;
        U16 word_2;
        U16 word_3;
    };
} AL_EXT_64;
#define _AL_EXT_64_DEFINED
#endif


#ifndef _AL_EXT_32_DEFINED
typedef union __AL_EXT_32
{
    U32 full;
    struct
    {
        U16 word_0;
        U16 word_1;
    };
} AL_EXT_32;
#define _AL_EXT_32_DEFINED
#endif


#ifndef _U16_STR_DEFINED
typedef union
{
    U16 Val;
    U08 v[2] /*__packed*/;
    struct   __packed
    {
        U08 LB;
        U08 HB;
    } byte;
} U16_STR;
#define _U16_STR_DEFINED
#endif


#ifndef _U32_STR_DEFINED
typedef union
{
    U32     Val;
    UWORD   w[2] __packed;
    U08     v[4] /*__packed*/;
} U32_STR;
#define _U32_STR_DEFINED
#endif



/* Usefil defines */
/* ---------------*/


typedef enum __CSP_RETVAL
{
    CSP_ERR_NO,
    CSP_ERR_PARAM
} CSP_RETVAL;

/* CPU info macroses definition */
/* ---------------------------- */

#define __CSP_CORE_PIC24                0
#define __CSP_CORE_DSPIC                1


#define __CSP_FAMILY_PIC24FJ128GA010    0   /* First  high-pin generation             */
#define __CSP_FAMILY_PIC24FJ64GA004     1   /* Second low-pin generation              */
#define __CSP_FAMILY_PIC24FJ256GA110    2   /* Second high-pin generation             */
#define __CSP_FAMILY_PIC24FJ256GB110    3   /* Third high-pin generation with USB OTG */


#define __CSP_FAMILY_PIC24HJ12GP202     4   /* PIC24HJ12GP201
                                               PIC24HJ12GP202
                                            */
#define __CSP_FAMILY_PIC24HJ32GP204     5   /* PIC24HJ32GP202
                                               PIC24HJ32GP204
                                               PIC24HJ16GP304
                                            */
#define __CSP_FAMILY_PIC24HJ128GP504    6   /* PIC24HJ32GP302
                                               PIC24HJ32GP304
                                               PIC24HJ64GP202
                                               PIC24HJ64GP204
                                               PIC24HJ64GP502
                                               PIC24HJ64GP504
                                               PIC24HJ128GP202
                                               PIC24HJ128GP204
                                               PIC24HJ128GP502
                                               PIC24HJ128GP504
                                             */
#define __CSP_FAMILY_PIC24HJ256GP610    7   /* PIC24HJ64GP206
                                               PIC24HJ64GP210
                                               PIC24HJ64GP506
                                               PIC24HJ64GP510
                                               PIC24HJ128GP206
                                               PIC24HJ128GP210
                                               PIC24HJ128GP306
                                               PIC24HJ128GP310
                                               PIC24HJ128GP506
                                               PIC24HJ128GP510
                                               PIC24HJ256GP206
                                               PIC24HJ256GP210
                                               PIC24HJ256GP610
                                            */
#define __CSP_FAMILY_PIC24F16KA102      8   /* PIC24F08KA101
                                               PIC24F16KA101
                                               PIC24F08KA102
                                               PIC24F16KA102
                                            */

#define __CSP_PRG_MEM_SIZE_8K           8448UL
#define __CSP_PRG_MEM_SIZE_16K          16890UL
#define __CSP_PRG_MEM_SIZE_32K          33786UL
#define __CSP_PRG_MEM_SIZE_48K          50682UL
#define __CSP_PRG_MEM_SIZE_64K          66048UL
#define __CSP_PRG_MEM_SIZE_96K          98298UL
#define __CSP_PRG_MEM_SIZE_128K         132096UL
#define __CSP_PRG_MEM_SIZE_192K         201216UL
#define __CSP_PRG_MEM_SIZE_256K         262656UL

#define __CSP_DAT_MEM_SIZE_16K          16384
#define __CSP_DAT_MEM_SIZE_8K           8192
#define __CSP_DAT_MEM_SIZE_4K           4096
#define __CSP_DAT_MEM_SIZE_1_5K         1536

#define __CSP_PIN_COUNT_20              28
#define __CSP_PIN_COUNT_28              28
#define __CSP_PIN_COUNT_44              44
#define __CSP_PIN_COUNT_64              64
#define __CSP_PIN_COUNT_80              80
#define __CSP_PIN_COUNT_100             100


/* Include CPU info header */
/* ----------------------- */

#if   defined(__PIC24FJ64GA006__)
      #include "cpu/PIC24FJ128GA010/CSP_PIC24FJ64GA006.h"
#elif defined(__PIC24FJ64GA008__)
      #include "cpu/PIC24FJ128GA010/CSP_PIC24FJ64GA008.h"
#elif defined(__PIC24FJ64GA010__)
      #include "cpu/PIC24FJ128GA010/CSP_PIC24FJ64GA010.h"
#elif defined(__PIC24FJ96GA006__)
      #include "cpu/PIC24FJ128GA010/CSP_PIC24FJ96GA006.h"
#elif defined(__PIC24FJ96GA008__)
      #include "cpu/PIC24FJ128GA010/CSP_PIC24FJ96GA008.h"
#elif defined(__PIC24FJ96GA010__)
      #include "cpu/PIC24FJ128GA010/CSP_PIC24FJ96GA010.h"
#elif defined(__PIC24FJ128GA006__)
      #include "cpu/PIC24FJ128GA010/CSP_PIC24FJ128GA006.h"
#elif defined(__PIC24FJ128GA008__)
      #include "cpu/PIC24FJ128GA010/CSP_PIC24FJ128GA008.h"
#elif defined(__PIC24FJ128GA010__)
      #include "cpu/PIC24FJ128GA010/CSP_PIC24FJ128GA010.h"

#elif defined(__PIC24FJ16GA002__)
      #include "cpu/PIC24FJ64GA004/CSP_PIC24FJ16GA002.h"
#elif defined(__PIC24FJ16GA004__)
      #include "cpu/PIC24FJ64GA004/CSP_PIC24FJ16GA004.h"
#elif defined(__PIC24FJ32GA002__)
      #include "cpu/PIC24FJ64GA004/CSP_PIC24FJ32GA002.h"
#elif defined(__PIC24FJ32GA004__)
      #include "cpu/PIC24FJ64GA004/CSP_PIC24FJ32GA004.h"
#elif defined(__PIC24FJ48GA002__)
      #include "cpu/PIC24FJ64GA004/CSP_PIC24FJ48GA002.h"
#elif defined(__PIC24FJ48GA004__)
      #include "cpu/PIC24FJ64GA004/CSP_PIC24FJ48GA004.h"
#elif defined(__PIC24FJ64GA002__)
      #include "cpu/PIC24FJ64GA004/CSP_PIC24FJ64GA002.h"
#elif defined(__PIC24FJ64GA004__)
      #include "cpu/PIC24FJ64GA004/CSP_PIC24FJ64GA004.h"

#elif defined(__PIC24FJ128GA106__)
      #include "cpu/PIC24FJ256GA110/CSP_PIC24FJ128GA106.h"
#elif defined(__PIC24FJ128GA108__)
      #include "cpu/PIC24FJ256GA110/CSP_PIC24FJ128GA108.h"
#elif defined(__PIC24FJ128GA110__)
      #include "cpu/PIC24FJ256GA110/CSP_PIC24FJ128GA110.h"
#elif defined(__PIC24FJ192GA106__)
      #include "cpu/PIC24FJ256GA110/CSP_PIC24FJ192GA106.h"
#elif defined(__PIC24FJ192GA108__)
      #include "cpu/PIC24FJ256GA110/CSP_PIC24FJ192GA108.h"
#elif defined(__PIC24FJ192GA110__)
      #include "cpu/PIC24FJ256GA110/CSP_PIC24FJ192GA110.h"
#elif defined(__PIC24FJ256GA106__)
      #include "cpu/PIC24FJ256GA110/CSP_PIC24FJ256GA106.h"
#elif defined(__PIC24FJ256GA108__)
      #include "cpu/PIC24FJ256GA110/CSP_PIC24FJ256GA108.h"
#elif defined(__PIC24FJ256GA110__)
      #include "cpu/PIC24FJ256GA110/CSP_PIC24FJ256GA110.h"

#elif defined(__PIC24FJ64GB106__)
      #include "cpu/PIC24FJ256GB110/CSP_PIC24FJ64GB106.h"
#elif defined(__PIC24FJ64GB108__)
      #include "cpu/PIC24FJ256GB110/CSP_PIC24FJ64GB108.h"
#elif defined(__PIC24FJ64GB110__)
      #include "cpu/PIC24FJ256GB110/CSP_PIC24FJ64GB110.h"
#elif defined(__PIC24FJ128GB106__)
      #include "cpu/PIC24FJ256GB110/CSP_PIC24FJ128GB106.h"
#elif defined(__PIC24FJ128GB108__)
      #include "cpu/PIC24FJ256GB110/CSP_PIC24FJ128GB108.h"
#elif defined(__PIC24FJ128GB110__)
      #include "cpu/PIC24FJ256GB110/CSP_PIC24FJ128GB110.h"
#elif defined(__PIC24FJ192GB106__)
      #include "cpu/PIC24FJ256GB110/CSP_PIC24FJ192GB106.h"
#elif defined(__PIC24FJ192GB108__)
      #include "cpu/PIC24FJ256GB110/CSP_PIC24FJ192GB108.h"
#elif defined(__PIC24FJ192GB110__)
      #include "cpu/PIC24FJ256GB110/CSP_PIC24FJ192GB110.h"
#elif defined(__PIC24FJ256GB106__)
      #include "cpu/PIC24FJ256GB110/CSP_PIC24FJ256GB106.h"
#elif defined(__PIC24FJ256GB108__)
      #include "cpu/PIC24FJ256GB110/CSP_PIC24FJ256GB108.h"
#elif defined(__PIC24FJ256GB110__)
      #include "cpu/PIC24FJ256GB110/CSP_PIC24FJ256GB110.h"

#elif defined(__PIC24F08KA101__)
      #include "cpu/PIC24F16KA102/CSP_PIC24F08KA101.h"
#elif defined(__PIC24F16KA101__)
      #include "cpu/PIC24F16KA102/CSP_PIC24F16KA101.h"
#elif defined(__PIC24F08KA102__)
      #include "cpu/PIC24F16KA102/CSP_PIC24F08KA102.h"
#elif defined(__PIC24F16KA102__)
      #include "cpu/PIC24F16KA102/CSP_PIC24F16KA102.h"


#else
    #error "PIC24F/PIC24H/dsPIC33 Peripheral library dosn't support selected CPU"
#endif

/* Useful macroses */
/* --------------- */

#define Nop()                       {__asm__ volatile ("nop");}

/* Include modules headers */
/* ----------------------- */

#define __CSP_SFR(addr)             __attribute__((sfr(addr)))

#define __CSP_SB_JOIN(a,b)          a##b
#define __CSP_SB_JOINN(a,b)         __CSP_SB_JOIN(a,b)
#define __CSP_SKIP_BYTE(val)        volatile U08 __CSP_SB_JOINN(EMPTY, __LINE__)[val]

#define __CSP_DEPRECATED            __attribute__((__deprecated__,__unsafe__))


#define __CSP_OPTIMIZE_FOR_SIZE     0
#define __CSP_OPTIMIZE_FOR_SPEED    1


#if defined(__CSP_BUILD_LIB)
    #define __CSP_ACC_MASK_REG(comm, reg_name, mask, ...)          BFAM(comm, reg_name, mask, __VA_ARGS__)
    #define __CSP_ACC_MASK_VAL(comm, reg_name, mask, ...)          BFAMD(comm, reg_name, mask, __VA_ARGS__)
    #define __CSP_ACC_MASK_IND(comm, reg_name, mask, ...)          BFAMI(comm, reg_name, mask, __VA_ARGS__)
    #define __CSP_ACC_RANG_REG(comm, reg_name, lower, upper, ...)  BFAR(comm, reg_name, lower, upper, __VA_ARGS__)
    #define __CSP_ACC_RANG_VAL(comm, reg_name, lower, upper, ...)  BFARD(comm, reg_name, lower, upper, __VA_ARGS__)

    #define __CSP_BIT_REG_SET(reg_name, bit)                       BFAR(BFA_SET, reg_name, bit, bit, 1)
    #define __CSP_BIT_REG_CLR(reg_name, bit)                       BFAR(BFA_CLR, reg_name, bit, bit, 1)
    #define __CSP_BIT_REG_GET(reg_name, bit)                       BFAR(BFA_RD,  reg_name, bit, bit)

    #define __CSP_BIT_VAL_SET(val, bit)                            BFARD(BFA_SET, val, bit, bit, 1)
    #define __CSP_BIT_VAL_CLR(val, bit)                            BFARD(BFA_CLR, val, bit, bit, 1)
    #define __CSP_BIT_VAL_GET(val, bit)                            BFARD(BFA_RD,  val, bit, bit)

#else
    #define __CSP_ACC_MASK_REG(comm, reg_name, mask, ...)          BFAM(comm, reg_name, mask, __VA_ARGS__)
    #define __CSP_ACC_MASK_VAL(comm, reg_name, mask, ...)          BFAM(comm, reg_name, mask, __VA_ARGS__)
    #define __CSP_ACC_MASK_IND(comm, reg_name, mask, ...)          BFAMI(comm, reg_name, mask, __VA_ARGS__)
    #define __CSP_ACC_RANG_REG(comm, reg_name, lower, upper, ...)  BFAR(comm, reg_name, lower, upper, __VA_ARGS__)
    #define __CSP_ACC_RANG_VAL(comm, reg_name, lower, upper, ...)  BFAR(comm, reg_name, lower, upper, __VA_ARGS__)

    #define __CSP_BIT_REG_SET(reg_name, bit)                       BFAR(BFA_SET, reg_name, bit, bit, 1)
    #define __CSP_BIT_REG_CLR(reg_name, bit)                       BFAR(BFA_CLR, reg_name, bit, bit, 1)
    #define __CSP_BIT_REG_GET(reg_name, bit)                       BFAR(BFA_RD,  reg_name, bit, bit)

    #define __CSP_BIT_VAL_SET(val, bit)                            BFAR(BFA_SET, val, bit, bit, 1)
    #define __CSP_BIT_VAL_CLR(val, bit)                            BFAR(BFA_CLR, val, bit, bit, 1)
    #define __CSP_BIT_VAL_GET(val, bit)                            BFAR(BFA_RD, val, bit, bit)
#endif


#define CSP_PHYS_ADDR_GET(a)            (a)

#if defined(__CSP_BUILD_LIB)
    #define __CSP_FUNC              __attribute__((section (".csp_code")))
#else
    #if defined(__XC16)
        #define __CSP_FUNC              __attribute__((always_inline)) inline extern
    #else
        #define __CSP_FUNC              inline extern
    #endif
#endif


/* Debug Definitions */
/* ----------------- */

#define CSP_DEB_HALT()              {__asm__ volatile(".pword 0xDA4000"); __asm__ volatile ("nop");}

/* Default optimization settings */
/* ----------------------------- */

#if 0

#if !defined(__CSP_DEBUG_CORE)
    #define __CSP_DEBUG_CORE
#endif
#if !defined(__CSP_DEBUG_CN)
    #define __CSP_DEBUG_CN
#endif
#if !defined(__CSP_DEBUG_INT)
    #define __CSP_DEBUG_INT
#endif
#if !defined(__CSP_DEBUG_TMR)
    #define __CSP_DEBUG_TMR
#endif
#if !defined(__CSP_DEBUG_IC)
    #define __CSP_DEBUG_IC
#endif
#if !defined(__CSP_DEBUG_OC)
    #define __CSP_DEBUG_OC
#endif
#if !defined(__CSP_DEBUG_I2C)
    #define __CSP_DEBUG_I2C
#endif
#if !defined(__CSP_DEBUG_UART)
    #define __CSP_DEBUG_UART
#endif
#if !defined(__CSP_DEBUG_SPI)
    #define __CSP_DEBUG_SPI
#endif
#if !defined(__CSP_DEBUG_ADC)
    #define __CSP_DEBUG_ADC
#endif
#if !defined(__CSP_DEBUG_GPIO)
    #define __CSP_DEBUG_GPIO
#endif
#if !defined(__CSP_DEBUG_PMP)
    #define __CSP_DEBUG_PMP
#endif
#if !defined(__CSP_DEBUG_NVM)
   #define __CSP_DEBUG_NVM
#endif
#if !defined(__CSP_DEBUG_RTC)
    #define __CSP_DEBUG_RTC
#endif
#if !defined(__CSP_DEBUG_COMP)
    #define __CSP_DEBUG_COMP
#endif
#if !defined(__CSP_DEBUG_CRC)
    #define __CSP_DEBUG_CRC
#endif
#if !defined(__CSP_DEBUG_OSC)
    #define __CSP_DEBUG_OSC
#endif
#if !defined(__CSP_DEBUG_RES)
    #define __CSP_DEBUG_RES
#endif
#if !defined(__CSP_DEBUG_PMD)
    #define __CSP_DEBUG_PMD
#endif
#if !defined(__CSP_DEBUG_PPS)
    #define __CSP_DEBUG_PPS
#endif
#if !defined(__CSP_DEBUG_CTMU)
    #define __CSP_DEBUG_CTMU
#endif
#if !defined(__CSP_DEBUG_USB)
    #define __CSP_DEBUG_USB
#endif
#if !defined(__CSP_DEBUG_UTIL)
    #define __CSP_DEBUG_UTIL
#endif
#if !defined(__CSP_DEBUG_PSAVE)
    #define __CSP_DEBUG_PSAVE
#endif
 
#endif 

/* Common modules definitions */
/* -------------------------- */

    /* Interrupt priorities level */

typedef enum __INT_PRI
{
    INT_PRI_0__DISABLED,
    INT_PRI_1,
    INT_PRI_2,
    INT_PRI_3,
    INT_PRI_4,
    INT_PRI_5,
    INT_PRI_6,
    INT_PRI_7,

    //-- why are there priorities 8..15 ?...
#if 0
    INT_PRI_8,
    INT_PRI_9,
    INT_PRI_10,
    INT_PRI_11,
    INT_PRI_12,
    INT_PRI_13,
    INT_PRI_14,
    INT_PRI_15,
#endif
} INT_PRI;

/* Include modules API */
/* ------------------- */

#include "modules/csp_core.h"          /* Core         */
#include "modules/csp_cn.h"            /* CN           */
#include "modules/csp_int.h"           /* Interrupts   */
#include "modules/csp_tmr.h"           /* TMR          */
#include "modules/csp_ic.h"            /* IC           */
#include "modules/csp_oc.h"            /* OC           */
#include "modules/csp_i2c.h"           /* I2C          */
#include "modules/csp_uart.h"          /* UART         */
#include "modules/csp_spi.h"           /* SPI          */
#include "modules/csp_adc.h"           /* ADC          */
#include "modules/csp_gpio.h"          /* GPIO         */
#include "modules/csp_pmp.h"           /* PMP          */
#include "modules/csp_rtc.h"           /* RTC          */
#include "modules/csp_comp.h"          /* Comparators  */
#include "modules/csp_crc.h"           /* CRC          */
#include "modules/csp_osc.h"           /* OSC          */
#include "modules/csp_res.h"           /* Reset        */
#include "modules/csp_psave.h"         /* Power save   */
#include "modules/csp_nvm.h"           /* NVM          */
#include "modules/csp_pmd.h"           /* PMD          */
#include "modules/csp_pps.h"           /* PPS          */
#include "modules/csp_ctmu.h"          /* CTMU         */
#include "modules/csp_usb.h"           /* USB          */
#include "modules/csp_hlvd.h"          /* HLVD         */
#include "modules/csp_util.h"          /* Utilities    */

#endif  /* __CSP_H */
/* ********************************************************************************************** */
/* end of file: csp.h*/
