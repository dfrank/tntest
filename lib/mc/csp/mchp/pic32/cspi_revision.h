/* *************************************************************************************************
     Project:         PIC24F Family CPU Support Library
     Author:          [GS] Gamma SPb (�) 2007, http://www.gamma.spb.ru

   *************************************************************************************************
     Distribution:

     PIC24F Family Peripheral Support Library
     ----------------------------------------
     Copyright � 2006-2007 Gamma SPb

   *************************************************************************************************
     MCU Family:      PIC24FJ
     Compiler:        Microchip C30 3.01

   *************************************************************************************************
     File:            csp_revision.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_REVISION_H
#define __CSP_REVISION_H

#define __CSP_REVISION                 ($WCREV$)
#define __CSP_REVISION_TIME_STRING     "$WCDATE$"
#define __CSP_BUILD_TIME_STRING        "$WCNOW$"

#endif  /* __CSP_REVISION_H */
/* ********************************************************************************************** */
/* end of file: csp_revision.h */
