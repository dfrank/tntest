
" определяем путь к папке .vimprj
let s:sPath = expand('<sfile>:p:h')

" указываем плагину project.tar.gz, какой файл проектов использовать
"
" ВНИМАНИЕ! 
"     для того, чтобы это работало, необходимо немного подправить
"     плагин project.tar.gz. См. http://habrahabr.ru/blogs/vim/102954/
let g:proj_project_filename=s:sPath.'/.vimprojects'

" Путь до основного окружения
let s:sEnvPath = $INDEXER_PROJECT_ROOT."/../../../../.."

let g:indexer_indexerListFilename = s:sPath.'/.indexer_files'
let g:indexer_getAllSubdirsFromIndexerListFile = 1

" подключаем теги используемых библиотек

exec "set tags+=".$VIMPRJ_ENV__PATH__LIB_MC_CSP__COMMON__TAGS

let g:FFS_paths = [
         \     $VIMPRJ_ENV__PATH__LIB_MC_CSP__COMMON,
         \  ]


" добавляем пути библиотек в path
"exec "set path+=".s:sEnvPath."/lib"
"exec "set path+=".s:sEnvPath."/common/bk90"

" указываем специфические для данного проекта настройки форматирования
let &tabstop = 3
let &shiftwidth = 3

" указываем прочие настройки для данного проекта
"let g:indexer_ctagsCommandLineOptions = '--c-kinds=+l --fields=+iaS --extra=+q'

let s:o_dir = $INDEXER_PROJECT_ROOT.'/output/tmp/compiled_in_vim'

if !isdirectory(s:o_dir)
   call mkdir(s:o_dir, "p")
endif

"call confirm("src appl/my.vim")

"let &makeprg = MakeprgGenerate($INDEXER_PROJECT_ROOT.'/'.s:sMcpProject, 'MPLAB_8_mcp', {
         "\ '-o': s:o_dir.'/%:t:r.o',
         "\ })

let g:indexer_handlePath = 0

let s:sCompilerExecutable = ''
if has('win32') || has('win64')
   let s:sCompilerExecutable = 'xc32-gcc.exe'
else
   let s:sCompilerExecutable = 'xc32-gcc'
endif


" Path to MPLAB .mcp project
"let s:sProject = 'charger_fw.mcp'
         "\              .' -g -D__DEBUG -omf=coff -x c -c -mcpu=24FJ256GB106 -Wall '

let s:sProject = 'csp_PIC32.X'


call CheckNeededSymbols(
         \  "The following items needs to be defined in your vimfiles/machine_local_conf/current/variables.vim file to make things work: ",
         \  "",
         \  [
         \     '$MACHINE_LOCAL_CONF__PATH__MICROCHIP_XC32',
         \  ]
         \  )

call envcontrol#set_project_file($INDEXER_PROJECT_ROOT.'/'.s:sProject, 'MPLAB_X', {
         \        'parser_params': {
         \           'compiler_command_without_includes' : ''
         \              .'"'.s:sCompilerExecutable.'" '
         \              .' -D__CSP_BUILD_LIB'
         \              .' -g -D__DEBUG -x c -c -mprocessor=32MX440F512H -Wall '
         \              .' -Os -MMD -MF "'.s:o_dir.'/%:t:r.o.d" -o "'.s:o_dir.'/%:t:r.o"',
         \        },
         \        'handle_clang' : 1,
         \        'add_paths' : [
         \           $MACHINE_LOCAL_CONF__PATH__MICROCHIP_XC32.'/lib/gcc/pic32mx/4.5.2/include',
         \           $MACHINE_LOCAL_CONF__PATH__MICROCHIP_XC32.'/pic32mx/include',
         \        ],
         \        'clang_add_params' : ''
         \              .'  -D __C32__'
         \              .'  -D __CLANG_FOR_COMPLETION__'
         \              .'  -D __LANGUAGE_C__'
         \              .'  -D __PIC32MX__'
         \              .'  -D __32MX440F512H__'
         \              .'  -D __DEBUG'
         \              .'  -D__CSP_BUILD_LIB'
         \              .'  -Wno-builtin-requires-header'
         \              .'  -Wno-unused-value'
         \              .'  -Wno-implicit-function-declaration'
         \              .'  -Wno-attributes'
         \              .'  -Wno-invalid-source-encoding'
         \              ,
         \ })



         "\        'compiler_executable' : '"C:\Program Files\Microchip\mplabc30\v3.25\bin\pic30-gcc.exe"',

"let &makeprg = 'pic30-gcc.exe -mcpu=24FJ256GB106 -x c -c   "%:p" -o"'.s:o_dir.'/%:t:r.o" -I"'.s:sEnvPath.'\lib\c" -I"'.s:sEnvPath.'\lib\mc" -I"'.$INDEXER_PROJECT_ROOT.'\source" -I"'.$INDEXER_PROJECT_ROOT.'\source\appl" -I"'.$INDEXER_PROJECT_ROOT.'\source\conf" -I"'.$INDEXER_PROJECT_ROOT.'\source\utils\dtp" -I"'.$INDEXER_PROJECT_ROOT.'\source\utils" -I"'.$INDEXER_PROJECT_ROOT.'\source\displays" -I"'.$INDEXER_PROJECT_ROOT.'\source\bsp" -I"'.$INDEXER_PROJECT_ROOT.'\source\param" -I"'.s:sEnvPath.'\common\bk100" -I"'.s:sEnvPath.'\common\bk90" -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -g -Wall -mlarge-code -mlarge-data -mconst-in-code -Os'

"call confirm("appl")

