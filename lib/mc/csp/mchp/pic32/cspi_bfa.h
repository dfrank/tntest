/***************************************************************************************************
 *   Project:       Atomic bitfield access macros definition for MCHP 16-bit CPU Support Library
 *   Author:        Leon van Snippenberg, http://www.avix-rt.com/
 ***************************************************************************************************
 *   Distribution:
 *
 ***************************************************************************************************
 *   MCU Family:    PIC24/dsPIC
 *   Compiler:      MCHP C30 3.11
 ***************************************************************************************************
 *   File:          csp_bfa.h
 *   Description:   Atomic bitfield access macros definition
 *
 ***************************************************************************************************
 *   History:       03.03.2009 - Created
 *
 **************************************************************************************************/

#ifndef __CSP_BFA_H
#define __CSP_BFA_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

/* ��������� ������� */

#define BFA_SET   0x1111
#define BFA_CLR   0x2222
#define BFA_INV   0x3333        /* ��������� �������������� �������� ����   */

#define BFA_WR    0xAAAA        /* ������ � ������� ����                    */ 
#define BFA_RD    0xBBBB        /* ������ �������� ����                     */

/* �������� ����� ��� ������� ���������. ����� ������� ����������� ������ �� ����������
   ������������� ��������� �������. � ���� ������ ���������� ������ ������:
   appl.c:11: error: '__BFA_COMMAND_ERROR_0' undeclared (first use in this function)
*/

#define __BFA_COMM_ERR(a)               __BFA_COMMAND_ERROR_##a
#define __BFA_COMM_GET(a)               __BFA_COMM_ERR(a)

typedef unsigned int __BFA_COMM_GET(BFA_SET);
typedef unsigned int __BFA_COMM_GET(BFA_CLR);
typedef unsigned int __BFA_COMM_GET(BFA_INV);

typedef unsigned int __BFA_COMM_GET(BFA_WR);
typedef unsigned int __BFA_COMM_GET(BFA_RD);


/* ��� ������� ���������� ���������� �� ���������� ��������� � ���� ��������� ��� �������������
   ������� ������������ ������� BFA().
   �������� ������ ���� ������������, ��� ���� � ��� ��������� ������ ���� ����������� ��
   ����������� ��������: ��� ���� ������ ����� ������� BITS, ��� ��������� - ������� bits.
   ��������:
     PORTABITS - ��� ���������, ����������� PORTA
     PORTAbits - ��� ��������� �� ������ ����������� ����������� �����
 
   ������� �������, ��� � �������� ������ ������������ ������� BFA() ������������ ��� ������� �
   ������������ ��������� �����������, ������� ������� �������� ������� � ������������ �����
   ����������� �30 ��� ������� �����������. ������� ��� ����������� ������ �� �������������.
 
   ��� �� �����, ���� �� ������ �������� ���������� �� ���������� ��������, ��� ������������
   ������ ������������ ���������� ������� ��� ����������� ��������, ���������� ���������
   ��������� �������:
   - ��������� ������ ���� ������������
   - ��� ���� ��������� � ��� ��������� ������ ����� ���� ������
   - ������� ����� ���� ������������ � ������� __BFA_STRUCT_TYPE
   - ������� ����� ��������� ������������ � ������� __BFA_STRUCT_VAL
*/

#define __BFA_STRUCT_TYPE(a)            __##a##bits_t
#define __BFA_STRUCT_VAL(a)             a##bits

#if 1

#define     __ATOMIC_XOR(ptr, value)                  \
__asm__ __volatile__(                                 \
      "ll      $t1,     0(%0);"                       \
      "xor     $t1,     $t1,     %1;"                 \
      "sc      $t1,     0(%0);"                       \
      "beqz    $t1,     -16;"                         \
      :                                               \
      : "r"(ptr), "r"(value)                          \
      : "t1", "memory"                                \
      );                                              \


#define     __ATOMIC_OR(ptr, value)                   \
   __asm__ __volatile__(                              \
         "ll      $t1,     0(%0);"                    \
         "or      $t1,     $t1,     %1;"              \
         "sc      $t1,     0(%0);"                    \
         "beqz    $t1,     -16;"                      \
         :                                            \
         : "r"(ptr), "r"(value)                       \
         : "t1", "memory"                             \
         );                                           \


#define     __ATOMIC_AND(ptr, value)                  \
   __asm__ __volatile__(                              \
         "ll      $t1,     0(%0);"                    \
         "and     $t1,     $t1,     %1;"              \
         "sc      $t1,     0(%0);"                    \
         "beqz    $t1,     -16;"                      \
         :                                            \
         : "r"(ptr), "r"(value)                       \
         : "t1", "memory"                             \
         );                                           \


#endif

#if 0
#define        __ATOMIC_XOR(ptr, value)      __sync_fetch_and_xor(ptr, value)
#define        __ATOMIC_OR(ptr, value)       __sync_fetch_and_or(ptr, value)
#define        __ATOMIC_AND(ptr, value)      __sync_fetch_and_and(ptr, value)
#endif 


/**
 * ������ ���������� ������� � ����������� ���� ���������
 *  
 * comm       - ����������� �������: 
 *                  - BFA_WR  - ������ �������� ����
 *                  - BFA_RD  - ������ �������� ����
 *                  - BFA_SET - ��������� ����� � ������� ���� �� �����
 *                  - BFA_CLR - ����� ����� � ������� ���� �� �����
 *                  - BFA_INV - �������������� ����� � ������� ���� �� �����
 * reg_name   - �������� �������� (PORTA, CMCON, ...). ������ ������ �������� SFR!
 * field_name - ��� ���� � ��������� �������� (��. ������������ �� ���������� � ������������ ���� 
 *              ��� �����������. �������� ��� �������� IPC0 ��� ���� ���� T1IP)
 * ...        - ��������, ����������� ������ ���� comm = BFA_WR. �������� ������ ���� ����� 
 *              ��������, ������� ������������ � ���� ���������.
 *              � �������� ����� ��������� ����� ������������ ����������.
 *  
 * ������� ������: 
 *              BFA(BFA_WR,  IPC0, INT0IP, 0); - ��������� ���� INT0IP �������� IPC0 � 0
 *              BFA(BFA_INV, IPC0, INT0IP, 1); - �������������� �������� ���� ���� INT0IP �������� IPC0
 *              a = BFA(BFA_RD, IPC0, INT0IP); - �������� ���� INT0IP �������� IPC0 �����������
 *                                               � ���������� a
 *              BFA(BFA_WR,  IPC0, INT0IP, a); - � ���� INT0IP �������� IPC0 ������������ ��������
 *                                               ���������� a
 *              BFA(BFA_SET, IPC0, INT0IP, (1 << 2)); - ��������� �������� ���� ���� INT0IP �������� IPC0
 */
#define BFA(comm, reg_name, field_name, ...)                                \
    ({                                                                      \
        union                                                               \
        {                                                                   \
            __BFA_STRUCT_TYPE(reg_name) o;                                  \
            unsigned int                i;                                  \
        } lcur, lmask={}, lval={};                                          \
                                                                            \
        lmask.o.field_name = -1;                                            \
                                                                            \
        ((comm) == BFA_WR)                                                  \
        ?({                                                                 \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            lval.o.field_name = v;                                          \
            lcur.o = __BFA_STRUCT_VAL(reg_name);                            \
            __ATOMIC_XOR(&(reg_name), (lmask.i & (lval.i^lcur.i)));         \
        }), 0                                                               \
                                                                            \
        :(((comm) == BFA_INV)                                               \
        ?({                                                                 \
            __ATOMIC_XOR(&(reg_name), (lmask.i));                  \
        }), 0                                                               \
                                                                            \
        :((((comm) == BFA_SET)                                              \
        ?({                                                                 \
            __ATOMIC_OR(&(reg_name), (lmask.i));                   \
        }), 0                                                               \
                                                                            \
        :(((((comm) == BFA_CLR)                                             \
        ?({                                                                 \
            __ATOMIC_AND(&(reg_name), ~(lmask.i));                 \
        }), 0                                                               \
                                                                            \
        :({ /* BFA_RD */                                                    \
             __BFA_STRUCT_VAL(reg_name).field_name;                         \
        })))))));                                                           \
        })

/**
 * ������ ���������� ������� � ���� ���������, ���������� � ���� ��������� 
 *  
 * comm       - ����������� �������: 
 *                  - BFA_WR  - ������ �������� ����
 *                  - BFA_RD  - ������ �������� ����
 *                  - BFA_SET - ��������� ����� � ������� ���� �� �����
 *                  - BFA_CLR - ����� ����� � ������� ���� �� �����
 *                  - BFA_INV - �������������� ����� � ������� ���� �� ����� 
 * reg_name   - �������� ���������� (PORTA, CMCON, ...). ���������� ������ ���� ����������� � 
 *              ������� ������� ������ (near) - � ������ 8 �� ���.
 * lower      - ������� ��� ���� (��������, 0) 
 * upper      - ������� ��� ���� (��������, 5) 
 * ...        - ��������, ����������� ������ ���� comm = BFA_WR. �������� ������ ���� ����� 
 *              ��������, ������� ������������ � ���� ���������.
 *              � �������� ����� ��������� ����� ������������ ����������.
 *  
 *  ������� ������:
 *              BFAR(BFA_WR,  TRISB, 0,  7, 0xAA); - ������ ��������� 0xAA � ������� ���� �������� TRISB
 *              BFAR(BFA_INV, TRISB, 8, 15, 0x0F); - �������������� ������� ������� � ������� ����� �������� TRISB
 *              BFAR(BFA_WR,  TRISB, 0,  7,    a); - ������ �������� ���������� � � ������� ���� �������� TRISB
 *              b = BFAR(BFA_RD, TRISB, 0,  7);    - �������� �������� �������� ����� �������� TRISB � ���������� b 
 */
#define BFAR(comm, reg_name, lower, upper, ...)                                     \
    ({                                                                              \
        unsigned int lmask, lmaskl, lmasku;                                         \
        lmaskl = (1 << (lower));                                                    \
        lmasku = (1 << (upper));                                                    \
        lmask  = ((lmaskl-1) ^ (lmasku-1)) | lmaskl | lmasku;                       \
        ((comm) == BFA_WR)                                                          \
        ?({                                                                         \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                                 \
            v = (v << (((lower) < (upper)) ? (lower) : (upper))) & lmask;           \
           __ATOMIC_XOR(&(reg_name), (lmask & (v ^ (reg_name))));            \
        }), 0                                                                       \
        :(((comm) == BFA_INV)                                                       \
        ?({                                                                         \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                                 \
                v = (v << (((lower) < (upper)) ? (lower) : (upper))) & lmask;       \
                __ATOMIC_XOR(&(reg_name), (lmask & v));                      \
        }), 0                                                                       \
                                                                                    \
        :(((comm) == BFA_SET)                                                      \
        ?({                                                                         \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                                 \
               v = (v << (((lower) < (upper)) ? (lower) : (upper))) & lmask;        \
               __ATOMIC_OR(&(reg_name), (lmask & v));                        \
        }), 0                                                                       \
                                                                                    \
        :((((comm) == BFA_CLR)                                                      \
        ?({                                                                         \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                                 \
            v = (v << (((lower) < (upper)) ? (lower) : (upper))) & lmask;           \
            __ATOMIC_AND(&(reg_name), ~(lmask & v));                         \
        }), 0                                                                       \
                                                                                    \
        :({ /* BFA_RD */                                                            \
            (reg_name & lmask) >> (((lower) < (upper)) ? (lower) : (upper));        \
        })))));                                                                   \
        })


/**
 * ������ ���������� ������� �� ��������� � ���� ���������, ���������� � ���� ��������� 
 *  
 * comm       - ����������� �������: 
 *                  - BFA_WR  - ������ �������� ����
 *                  - BFA_RD  - ������ �������� ����
 *                  - BFA_SET - ��������� ����� � ������� ���� �� �����
 *                  - BFA_CLR - ����� ����� � ������� ���� �� �����
 *                  - BFA_INV - �������������� ����� � ������� ���� �� ����� 
 * ptr        - ��������� �� ����������
 * lower      - ������� ��� ���� (��������, 0) 
 * upper      - ������� ��� ���� (��������, 5) 
 * ...        - ��������, ����������� ������ ���� comm = BFA_WR. �������� ������ ���� ����� 
 *              ��������, ������� ������������ � ���� ���������.
 *              � �������� ����� ��������� ����� ������������ ����������.
 *  
 *  ������� ������:
 *              BFARI(BFA_WR,  &TRISB, 0,  7, 0xAA); - ������ ��������� 0xAA � ������� ���� �������� TRISB
 *              BFARI(BFA_INV, &TRISB, 8, 15, 0x0F); - �������������� ������� ������� � ������� ����� �������� TRISB
 *              BFARI(BFA_WR,  &TRISB, 0,  7,    a); - ������ �������� ���������� � � ������� ���� �������� TRISB
 *              b = BFARI(BFA_RD, &TRISB, 0,  7);    - �������� �������� �������� ����� �������� TRISB � ���������� b  
 *
 */
#define BFARI(comm, ptr, lower, upper, ...)                                 \
    ({                                                                      \
        unsigned int lmask, lmaskl, lmasku;                                 \
        unsigned int *addrpt;                                               \
        lmaskl = (1 << (lower));                                            \
        lmasku = (1 << (upper));                                            \
        lmask  = ((lmaskl-1) ^ (lmasku-1)) | lmaskl | lmasku;               \
        addrpt = (unsigned int*)ptr;                                        \
                                                                            \
        ((comm) == BFA_WR) ?                                                \
        ({                                                                  \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            v = (v << (((lower) < (upper)) ? (lower) : (upper))) & lmask;   \
            __ATOMIC_XOR((addrpt), (lmask & (v ^ *addrpt)));        \
        }), 0                                                               \
                                                                            \
        :(((comm) == BFA_INV)                                               \
        ?({                                                                 \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            v = (v << (((lower) < (upper)) ? (lower) : (upper))) & lmask;   \
            __ATOMIC_XOR((addrpt), (lmask & v));        \
        }), 0                                                               \
                                                                            \
        :((((comm) == BFA_SET)                                              \
        ?({                                                                 \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            v = (v << (((lower) < (upper)) ? (lower) : (upper))) & lmask;   \
            __ATOMIC_OR((addrpt), (lmask & v));        \
        }), 0                                                               \
                                                                            \
        :(((((comm) == BFA_CLR)                                             \
        ?({                                                                 \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            v = (v << (((lower) < (upper)) ? (lower) : (upper))) & lmask;   \
            __ATOMIC_AND((addrpt), ~(lmask & v));        \
        }), 0                                                               \
                                                                            \
        :({ /* BFA_RD */                                                    \
            (*addrpt & lmask) >> (((lower) < (upper)) ? (lower) : (upper)); \
        })                                                                  \
                                                                            \
        ))))));                                                             \
    })


/**
 * ������ ���������� ������� � ������ �������� ��� ����������
 *  
 * comm       - ����������� �������: 
 *                  - BFA_WR  - ������ �������� ����
 *                  - BFA_RD  - ������ �������� ����
 *                  - BFA_SET - ��������� ����� � ������� ���� �� �����
 *                  - BFA_CLR - ����� ����� � ������� ���� �� �����
 *                  - BFA_INV - �������������� ����� � ������� ���� �� ����� 
 * val        - ���������� (����� ��������� � ����� ������� ���)
 * lower      - ������� ��� ���� (��������, 0) 
 * upper      - ������� ��� ���� (��������, 5) 
 * ...        - ��������, ����������� ������ ���� comm = BFA_WR. �������� ������ ���� ����� 
 *              ��������, ������� ������������ � ���� ���������.
 *              � �������� ����� ��������� ����� ������������ ����������.
 *  
 *  ������� ������:
 *              BFARD(BFA_WR,  data,       0,  7, 0xAA); - ������ ��������� 0xAA � ������� ���� ���������� data
 *              BFARD(BFA_INV, temp->data, 8, 15, 0x0F); - �������������� ������� ������� � ������� ����� �������� data ��������� temp
 *
 */
#define BFARD(comm, val, lower, upper, ...)                                 \
    ({                                                                      \
        unsigned int lmask, lmaskl, lmasku;                                 \
        volatile unsigned int *addrpt;                                      \
        lmaskl = (1 << (lower));                                            \
        lmasku = (1 << (upper));                                            \
        lmask  = ((lmaskl-1) ^ (lmasku-1)) | lmaskl | lmasku;               \
        addrpt = (unsigned int*)&val;                                       \
                                                                            \
        ((comm) == BFA_WR) ?                                                \
        ({                                                                  \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            v = (v << (((lower) < (upper)) ? (lower) : (upper))) & lmask;   \
            __ATOMIC_XOR((addrpt), (lmask & (v ^ *addrpt)));        \
        }), 0                                                               \
                                                                            \
        :(((comm) == BFA_INV)                                               \
        ?({                                                                 \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            v = (v << (((lower) < (upper)) ? (lower) : (upper))) & lmask;   \
            __ATOMIC_XOR((addrpt), (lmask & v));        \
        }), 0                                                               \
                                                                            \
        :((((comm) == BFA_SET)                                              \
        ?({                                                                 \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            v = (v << (((lower) < (upper)) ? (lower) : (upper))) & lmask;   \
            __ATOMIC_OR(addrpt, (lmask & v));                                  \
        }), 0                                                               \
                                                                            \
        :(((((comm) == BFA_CLR)                                             \
        ?({                                                                 \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            v = (v << (((lower) < (upper)) ? (lower) : (upper))) & lmask;   \
            __ATOMIC_AND((addrpt), ~(lmask & v));        \
        }), 0                                                               \
                                                                            \
        :({ /* BFA_RD */                                                    \
            (*addrpt & lmask) >> (((lower) < (upper)) ? (lower) : (upper)); \
        })                                                                  \
                                                                            \
        ))))));                                                             \
    })


























/**
 * ������ ���������� ������� � ���� ���������, ���������� � ���� �����
 *  
 * comm       - ����������� �������: 
 *                  - BFA_WR  - ������ �������� ����
 *                  - BFA_RD  - ������ �������� ����
 *                  - BFA_SET - ��������� ����� � ������� ���� �� �����
 *                  - BFA_CLR - ����� ����� � ������� ���� �� �����
 *                  - BFA_INV - �������������� ����� � ������� ���� �� ����� 
 * reg_name   - �������� ���������� (PORTA, CMCON, ...). ���������� ������ ���� ����������� � 
 *              ������� ������� ������ (near) - � ������ 8 �� ���.
 * mask       - ����� �������. ���� �����, ������ 1 ��������� �� ���� ��������, � ������� ����� 
 *              ��������� �������� 
 * ...        - ��������, ����������� ������ ���� comm = BFA_WR. �������� ������ ���� ����� 
 *              ��������, ������� ������������ � ���� ���������.
 *              � �������� ����� ��������� ����� ������������ ����������.
 *  
 *  ������� ������:
 *              BFAM(BFA_WR, TRISB, 0x000F, b);  - ������ ���������� b � ������� ������� �������� TRISB
 */
#define BFAM(comm, reg_name, mask, ...)                                             \
    ({                                                                              \
        ((comm) == BFA_WR)                                                          \
        ?({                                                                         \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                                 \
            __ATOMIC_XOR(&(reg_name), (mask & (v ^ reg_name)));                               \
        }), 0                                                                       \
        :(((comm) == BFA_INV)                                                       \
        ?({                                                                         \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                                 \
            __ATOMIC_XOR(&(reg_name), (mask & v));        \
        }), 0                                                                       \
                                                                                    \
        :((((comm) == BFA_SET)                                                      \
        ?({                                                                         \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                                 \
            __ATOMIC_OR(&(reg_name), (mask & v));                                  \
        }), 0                                                                       \
        :(((((comm) == BFA_CLR)                                                     \
        ?({                                                                         \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                                 \
            __ATOMIC_AND(&(reg_name), ~(mask & v));        \
        }), 0                                                                       \
        :({ /* BFA_RD */                                                            \
            (reg_name & mask);                                                      \
        })))))));                                                                   \
        })




#define BFAMI(comm, ptr, mask, ...)                                         \
    ({                                                                      \
        unsigned int *addrpt;                                               \
        addrpt = (unsigned int*)ptr;                                        \
                                                                            \
        ((comm) == BFA_WR) ?                                                \
        ({                                                                  \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            __ATOMIC_XOR((addrpt), (mask & (v ^ *addrpt)));                               \
        }), 0                                                               \
                                                                            \
        :(((comm) == BFA_INV)                                               \
        ?({                                                                 \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            __ATOMIC_XOR((addrpt), (mask & v));        \
        }), 0                                                               \
                                                                            \
        :((((comm) == BFA_SET)                                              \
        ?({                                                                 \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            __ATOMIC_OR((addrpt), (mask & v));                                  \
        }), 0                                                               \
                                                                            \
        :(((((comm) == BFA_CLR)                                             \
        ?({                                                                 \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            __ATOMIC_AND((addrpt), ~(mask & v));        \
        }), 0                                                               \
                                                                            \
        :({ /* BFA_RD */                                                    \
            (*addrpt & mask);                                               \
        })                                                                  \
                                                                            \
        ))))));                                                             \
    })




#define BFAMD(comm, val, mask, ...)                                         \
    ({                                                                      \
        volatile unsigned int *addrpt;                                      \
        addrpt = (unsigned int*)&val;                                       \
                                                                            \
        ((comm) == BFA_WR) ?                                                \
        ({                                                                  \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            __ATOMIC_XOR(addrpt, (mask & (v ^ *addrpt)));                       \
        }), 0                                                               \
                                                                            \
        :(((comm) == BFA_INV)                                               \
        ?({                                                                 \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            __ATOMIC_XOR((addrpt), (mask & v));        \
        }), 0                                                               \
                                                                            \
        :((((comm) == BFA_SET)                                              \
        ?({                                                                 \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            __ATOMIC_OR((addrpt), (mask & v));                                  \
        }), 0                                                               \
                                                                            \
        :(((((comm) == BFA_CLR)                                             \
        ?({                                                                 \
            __BFA_COMM_GET(comm) v = __VA_ARGS__+0;                         \
            __ATOMIC_AND((addrpt), ~(mask & v));                    \
        }), 0                                                               \
                                                                            \
        :({ /* BFA_RD */                                                    \
            (*addrpt & mask);                                               \
        })                                                                  \
                                                                            \
        ))))));                                                             \
    })



/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/


#endif /* __CSP_BFA_H */
/***************************************************************************************************
    end of file: csp_bfa.h
 **************************************************************************************************/
