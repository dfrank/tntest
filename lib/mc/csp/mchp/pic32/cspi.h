/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp.h
     Description:

   ********************************************************************************************** */

#ifndef __CSP_H
#define __CSP_H

#include <limits.h>
#include <stddef.h>

#include <p32xxxx.h>
//#include <plib.h>

#include <sys/attribs.h>

#include "csp_types.h"
#include "cspi_revision.h"
#include "cspi_bfa.h"


#ifndef __packed
#define __packed    __attribute__((packed))
#endif

#ifndef __far
#define __far       __attribute__((far))
#endif

#ifndef __rom
#define __rom       const
#endif


/* Global types definition */
/* ----------------------- */


#ifndef _U64_DEFINED
typedef unsigned long long   U64;
#define _U64_DEFINED
#endif


#ifndef _WORD_DEFINED
typedef          S32         PWORD;
#define _WORD_DEFINED
#endif

#ifndef _UWORD_DEFINED
typedef          U32         UPWORD;
#define _UWORD_DEFINED
#endif

#ifndef _SWORD_DEFINED
typedef          S32         SPWORD;
#define _SWORD_DEFINED
#endif



#ifndef _AL_EXT_64_DEFINED
typedef union __AL_EXT_64
{
    U64 full;
    struct
    {
        U32 word_0;
        U32 word_1;
    };
} AL_EXT_64;
#define _AL_EXT_64_DEFINED
#endif


#ifndef _AL_EXT_32_DEFINED
typedef union __AL_EXT_32
{
    U32 full;
    struct
    {
        U32 word_0;
    };
} AL_EXT_32;
#define _AL_EXT_32_DEFINED
#endif


#if 1
#ifndef _U16_STR_DEFINED
typedef union
{
    U16 Val;
    U08 v[2];
    struct   __packed
    {
        U08 LB;
        U08 HB;
    } byte;
} U16_STR;
#define _U16_STR_DEFINED
#endif
#endif


#ifndef _U32_STR_DEFINED
typedef union
{
    U32     Val;
    U16     s[2] __packed;
    U08     v[4];
} U32_STR;
#define _U32_STR_DEFINED
#endif



/* Useful defines */
/* ---------------*/


typedef enum __CSP_RETVAL
{
    CSP_ERR_NO,
    CSP_ERR_PARAM
} CSP_RETVAL;

/* CPU info macroses definition */
/* ---------------------------- */

#define __CSP_CORE_PIC32                1


#define __CSP_FAMILY_PIC32MX1XX_2XX     0
#define __CSP_FAMILY_PIC32MX3XX_4XX     1


#define __CSP_PRG_MEM_SIZE_8K           8448UL
#define __CSP_PRG_MEM_SIZE_16K          16890UL
#define __CSP_PRG_MEM_SIZE_32K          33786UL
#define __CSP_PRG_MEM_SIZE_48K          50682UL
#define __CSP_PRG_MEM_SIZE_64K          66048UL
#define __CSP_PRG_MEM_SIZE_96K          98298UL
#define __CSP_PRG_MEM_SIZE_128K         132096UL
#define __CSP_PRG_MEM_SIZE_192K         201216UL
#define __CSP_PRG_MEM_SIZE_256K         262656UL
#define __CSP_PRG_MEM_SIZE_512K         536560UL    //TODO: i got this value in mplab.
                                                    //  But, in case of PIC24, in mplab is stated:
                                                    //  "87548 words". Each word is 3-byte,
                                                    //  so, value should be 262644.
                                                    //  But, it is 262656 (by Alex Borisov)

#define __CSP_DAT_MEM_SIZE_32K          32768
#define __CSP_DAT_MEM_SIZE_16K          16384
#define __CSP_DAT_MEM_SIZE_8K           8192
#define __CSP_DAT_MEM_SIZE_4K           4096
#define __CSP_DAT_MEM_SIZE_1_5K         1536

#define __CSP_PIN_COUNT_64              64
#define __CSP_PIN_COUNT_100             100
#define __CSP_PIN_COUNT_121             121


//--TODO: maybe move to family header?


/* Include CPU info header */
/* ----------------------- */

#if defined(__32MX440F512H__)
    #include "cpu/PIC32MX3XX_4XX/CSP_PIC32MX440F512H.h"
#else
    #error "PIC32 Peripheral library dosn't support selected CPU"
#endif

/* Useful macros */
/* --------------- */

//COMMENTED because it complains that it is redefined (somewhere under p32xxxx.h, it seems)
//#define Nop()                       {__asm__ volatile ("nop");}

/* Include modules headers */
/* ----------------------- */

#define __CSP_SFR(addr)             __attribute__((address(addr))) //__attribute__((sfr(addr)))

#define __CSP_SB_JOIN(a,b)          a##b
#define __CSP_SB_JOINN(a,b)         __CSP_SB_JOIN(a,b)
#define __CSP_SKIP_BYTE(val)        volatile U08 __CSP_SB_JOINN(Z__EMPTY, __LINE__)[val]

#define __CSP_DEPRECATED            __attribute__((__deprecated__))


#define __CSP_OPTIMIZE_FOR_SIZE     0
#define __CSP_OPTIMIZE_FOR_SPEED    1


#if 1
#if defined(__CSP_BUILD_LIB)
    #define __CSP_ACC_MASK_REG(comm, reg_name, mask, ...)          BFAM(comm, reg_name, mask, __VA_ARGS__)
    #define __CSP_ACC_MASK_VAL(comm, reg_name, mask, ...)          BFAMD(comm, reg_name, mask, __VA_ARGS__)
    #define __CSP_ACC_MASK_IND(comm, reg_name, mask, ...)          BFAMI(comm, reg_name, mask, __VA_ARGS__)
    #define __CSP_ACC_RANG_REG(comm, reg_name, lower, upper, ...)  BFAR(comm, reg_name, lower, upper, __VA_ARGS__)
    #define __CSP_ACC_RANG_VAL(comm, reg_name, lower, upper, ...)  BFARD(comm, reg_name, lower, upper, __VA_ARGS__)

    #define __CSP_BIT_REG_SET(reg_name, bit)                       BFAR(BFA_SET, reg_name, bit, bit, 1)
    #define __CSP_BIT_REG_CLR(reg_name, bit)                       BFAR(BFA_CLR, reg_name, bit, bit, 1)
    #define __CSP_BIT_REG_GET(reg_name, bit)                       BFAR(BFA_RD,  reg_name, bit, bit)

    #define __CSP_BIT_VAL_SET(val, bit)                            BFARD(BFA_SET, val, bit, bit, 1)
    #define __CSP_BIT_VAL_CLR(val, bit)                            BFARD(BFA_CLR, val, bit, bit, 1)
    #define __CSP_BIT_VAL_GET(val, bit)                            BFARD(BFA_RD,  val, bit, bit)

#else
    #define __CSP_ACC_MASK_REG(comm, reg_name, mask, ...)          BFAM(comm, reg_name, mask, __VA_ARGS__)
    #define __CSP_ACC_MASK_VAL(comm, reg_name, mask, ...)          BFAM(comm, reg_name, mask, __VA_ARGS__)
    #define __CSP_ACC_MASK_IND(comm, reg_name, mask, ...)          BFAMI(comm, reg_name, mask, __VA_ARGS__)
    #define __CSP_ACC_RANG_REG(comm, reg_name, lower, upper, ...)  BFAR(comm, reg_name, lower, upper, __VA_ARGS__)
    #define __CSP_ACC_RANG_VAL(comm, reg_name, lower, upper, ...)  BFAR(comm, reg_name, lower, upper, __VA_ARGS__)

    #define __CSP_BIT_REG_SET(reg_name, bit)                       BFAR(BFA_SET, reg_name, bit, bit, 1)
    #define __CSP_BIT_REG_CLR(reg_name, bit)                       BFAR(BFA_CLR, reg_name, bit, bit, 1)
    #define __CSP_BIT_REG_GET(reg_name, bit)                       BFAR(BFA_RD,  reg_name, bit, bit)

    #define __CSP_BIT_VAL_SET(val, bit)                            BFAR(BFA_SET, val, bit, bit, 1)
    #define __CSP_BIT_VAL_CLR(val, bit)                            BFAR(BFA_CLR, val, bit, bit, 1)
    #define __CSP_BIT_VAL_GET(val, bit)                            BFAR(BFA_RD, val, bit, bit)
#endif
#endif


#define CSP_PHYS_ADDR_GET(a)            (a)

#if defined(__CSP_BUILD_LIB)
    #define __CSP_FUNC              __attribute__((section (".csp_code")))
#else
    #define __CSP_FUNC              inline extern
#endif


/* Debug Definitions */
/* ----------------- */

#define CSP_DEB_HALT()              {__asm__ volatile(" sdbbp 0"); __asm__ volatile ("nop");}

/* Default optimization settings */
/* ----------------------------- */

#if 1

#if !defined(__CSP_DEBUG_CORE)
    #define __CSP_DEBUG_CORE
#endif
#if !defined(__CSP_DEBUG_CN)
    #define __CSP_DEBUG_CN
#endif
#if !defined(__CSP_DEBUG_INT)
    #define __CSP_DEBUG_INT
#endif
#if !defined(__CSP_DEBUG_TMR)
    #define __CSP_DEBUG_TMR
#endif
#if !defined(__CSP_DEBUG_IC)
    #define __CSP_DEBUG_IC
#endif
#if !defined(__CSP_DEBUG_OC)
    #define __CSP_DEBUG_OC
#endif
#if !defined(__CSP_DEBUG_I2C)
    #define __CSP_DEBUG_I2C
#endif
#if !defined(__CSP_DEBUG_UART)
    #define __CSP_DEBUG_UART
#endif
#if !defined(__CSP_DEBUG_SPI)
    #define __CSP_DEBUG_SPI
#endif
#if !defined(__CSP_DEBUG_ADC)
    #define __CSP_DEBUG_ADC
#endif
#if !defined(__CSP_DEBUG_GPIO)
    #define __CSP_DEBUG_GPIO
#endif
#if !defined(__CSP_DEBUG_PMP)
    #define __CSP_DEBUG_PMP
#endif
#if !defined(__CSP_DEBUG_RTC)
    #define __CSP_DEBUG_RTC
#endif
#if !defined(__CSP_DEBUG_COMP)
    #define __CSP_DEBUG_COMP
#endif
#if !defined(__CSP_DEBUG_CRC)
    #define __CSP_DEBUG_CRC
#endif
#if !defined(__CSP_DEBUG_OSC)
    #define __CSP_DEBUG_OSC
#endif
#if !defined(__CSP_DEBUG_RES)
    #define __CSP_DEBUG_RES
#endif
#if !defined(__CSP_DEBUG_PMD)
    #define __CSP_DEBUG_PMD
#endif
#if !defined(__CSP_DEBUG_PPS)
    #define __CSP_DEBUG_PPS
#endif
#if !defined(__CSP_DEBUG_CTMU)
    #define __CSP_DEBUG_CTMU
#endif
#if !defined(__CSP_DEBUG_USB)
    #define __CSP_DEBUG_USB
#endif
#if !defined(__CSP_DEBUG_UTIL)
    #define __CSP_DEBUG_UTIL
#endif
#if !defined(__CSP_DEBUG_PSAVE)
    #define __CSP_DEBUG_PSAVE
#endif
 
#endif 

/* Common modules definitions */
/* -------------------------- */

    /* Interrupt priorities level */

enum E_CSP_InterruptPri
{
    CSP_INT_PRI_0__DISABLED, //-- interrupt is disabled
    CSP_INT_PRI_1,
    CSP_INT_PRI_2,
    CSP_INT_PRI_3,
    CSP_INT_PRI_4,
    CSP_INT_PRI_5,
    CSP_INT_PRI_6,
    CSP_INT_PRI_7,
};


#define     __CSP_CLR_REG_OFFSET    (0x04 / sizeof (UPWORD))
#define     __CSP_SET_REG_OFFSET    (0x08 / sizeof (UPWORD))
#define     __CSP_INV_REG_OFFSET    (0x0c / sizeof (UPWORD))

#define     __CSP_CLR_REG(addr)     *((addr) + __CSP_CLR_REG_OFFSET)
#define     __CSP_SET_REG(addr)     *((addr) + __CSP_SET_REG_OFFSET)
#define     __CSP_INV_REG(addr)     *((addr) + __CSP_INV_REG_OFFSET)

#define     __CSP_REG_DEF(_name_)                           \
   volatile UPWORD _name_;                                  \
   volatile UPWORD _##_name_##_CLR;                         \
   volatile UPWORD _##_name_##_SET;                         \
   volatile UPWORD _##_name_##_INV;                         
   
typedef struct S_CSP_P32Reg {
   UPWORD reg;
   UPWORD CLR;
   UPWORD SET;
   UPWORD INV;
} T_CSP_P32Reg;

//#define     __CSP_CLR_REG(addr)     *((UPWORD *)((U08 *)(addr) + __CSP_CLR_REG_OFFSET))
//#define     __CSP_SET_REG(addr)     *((UPWORD *)((U08 *)(addr) + __CSP_SET_REG_OFFSET))
//#define     __CSP_INV_REG(addr)     *((UPWORD *)((U08 *)(addr) + __CSP_INV_REG_OFFSET))

/* Include modules API */
/* ------------------- */

//#include "modules/csp_core.h"          /* Core         */
#include "modules/csp_cn.h"            /* CN           */
#include "modules/csp_int.h"           /* Interrupts   */
#include "modules/csp_tmr.h"           /* TMR          */
#include "modules/csp_ic.h"            /* IC           */
#include "modules/csp_oc.h"            /* OC           */
#include "modules/csp_i2c.h"           /* I2C          */
#include "modules/csp_uart.h"          /* UART         */
#include "modules/csp_spi.h"           /* SPI          */
//#include "modules/csp_adc.h"           /* ADC          */
#include "modules/csp_gpio.h"          /* GPIO         */
//#include "modules/csp_pmp.h"           /* PMP          */
//#include "modules/csp_rtc.h"           /* RTC          */
//#include "modules/csp_comp.h"          /* Comparators  */
//#include "modules/csp_crc.h"           /* CRC          */
//#include "modules/csp_osc.h"           /* OSC          */
//#include "modules/csp_res.h"           /* Reset        */
//#include "modules/csp_psave.h"         /* Power save   */
//#include "modules/csp_nvm.h"           /* NVM          */
//#include "modules/csp_pmd.h"           /* PMD          */
//#include "modules/csp_pps.h"           /* PPS          */
//#include "modules/csp_ctmu.h"          /* CTMU         */
//#include "modules/csp_usb.h"           /* USB          */
//#include "modules/csp_hlvd.h"          /* HLVD         */
//#include "modules/csp_util.h"          /* Utilities    */

#endif  /* __CSP_H */
/* ********************************************************************************************** */
/* end of file: csp.h*/
