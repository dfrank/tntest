/* *************************************************************************************************
Project:         MCHP 16-bit CPU Support Library
Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

 *************************************************************************************************
Distribution:

MCHP 16-bit CPU Support Library
----------------------------------------
Copyright � 2006-2008 Alex B.

 *************************************************************************************************
 MCU Family:      PIC24F/PIC24H/dsPIC33
Compiler:        Microchip C30 3.11

 *************************************************************************************************
File:            csp_usb.h
Description:

 ********************************************************************************************** */

#ifndef __CSP_USB_H
#define __CSP_USB_H

#include "../cspi.h"

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC32MX3XX_4XX))


/* USB programming model */
/* ---------------------- */

typedef struct S_CSP_USB
{
   volatile UPWORD OTGIR;       /* USB OTG Interrupt Status Register (Host Mode)    */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD OTGIE;       /* USB OTG Interrupt Enable Register (Host Mode)    */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD OTGSTAT;     /* USB OTG Status Register                          */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD OTGCON;      /* USB OTG Control Register                         */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD PWRC;        /* USB Power Control Register                       */
   __CSP_SKIP_BYTE(12 + 0x170);
   volatile UPWORD IR;          /* USB Interrupt Status Register (Device Mode Only) */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD IE;          /* USB Interrupt Enable Register (All USB Modes)    */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EIR;         /* USB Error Interrupt Status Register              */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EIE;         /* USB Error Interrupt Enable Register              */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD STAT;        /* USB Status Register                              */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD CON;         /* USB Control Register                             */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD ADDR;        /* USB Address Register                             */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD BDTP1;       /* USB Buffer Descriptor Table Location             */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD FRML;        /* USB Frame Number Low Register                    */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD FRMH;        /* USB Frame Number High Register                   */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD TOK;         /* USB Token Register (Host Mode Only)              */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD SOF;         /* USB OTG Start-of-Token Threshold Register (Host Mode Only)   */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD BDTP2;       /* USB Buffer Descriptor Table Location             */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD BDTP3;       /* USB Buffer Descriptor Table Location             */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD CNFG1;       /* USB Configuration Register 1                     */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP0;         /* USB Endpoint 0 Control Registers                 */
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP1;         /* USB Endpoint 1 Control Registers                 */ 
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP2;         /* USB Endpoint 2 Control Registers                 */ 
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP3;         /* USB Endpoint 3 Control Registers                 */ 
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP4;         /* USB Endpoint 4 Control Registers                 */ 
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP5;         /* USB Endpoint 5 Control Registers                 */ 
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP6;         /* USB Endpoint 6 Control Registers                 */ 
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP7;         /* USB Endpoint 7 Control Registers                 */ 
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP8;         /* USB Endpoint 8 Control Registers                 */ 
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP9;         /* USB Endpoint 9 Control Registers                 */ 
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP10;        /* USB Endpoint 10 Control Registers                */ 
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP11;        /* USB Endpoint 11 Control Registers                */ 
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP12;        /* USB Endpoint 12 Control Registers                */ 
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP13;        /* USB Endpoint 13 Control Registers                */ 
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP14;        /* USB Endpoint 14 Control Registers                */ 
   __CSP_SKIP_BYTE(12);
   volatile UPWORD EP15;        /* USB Endpoint 15 Control Registers                */ 
   //volatile UPWORD PWMRRS;      /* USB VBUS PWM Generator Period Register           */ 
   //volatile UPWORD PWMCON;      /* USB VBUS PWM Generator Control Register          */ 
} T_CSP_USB;

#define USB_TYPE  T_CSP_USB

/* USB base addresses */
/* ------------------- */
#define USB_BASE_VIRT_ADDR           (0xbf880000 + 0x5040)

/* Declare USB models */
/* ------------------- */
extern T_CSP_USB CSP_USB;

/* Internal API definitions */
/* ------------------------ */




/* Definitions for UART API */
/* ------------------------ */

/* For CON */

typedef enum __USB_EP_INDX
{
   USB_EP0,
   USB_EP1,
   USB_EP2,
   USB_EP3,
   USB_EP4,
   USB_EP5,
   USB_EP6,
   USB_EP7,
   USB_EP8,
   USB_EP9,
   USB_EP10,
   USB_EP11,
   USB_EP12,
   USB_EP13,
   USB_EP14,
   USB_EP15,
} USB_EP_INDX;

typedef enum __USB_EP_DIR
{
   USB_EP_OUT = 0,
   USB_EP_IN  = 1,
} USB_EP_DIR;

typedef enum __USB_EP_PP
{
   USB_EP_EVEN = 0,
   USB_EP_ODD  = 1,
} USB_EP_PP;





typedef enum __USB_CON
{
   USB_MODE_DEVICE_EN      = (1 << 0),     /* USB module operates in Device mode when U1PWRC<0> is set; D+ pull-up is activated in hardware (device attached) */
   USB_MODE_DEVICE_DIS     = (0 << 0),     /* USB Device mode circuitry disabled (device detached) */

   USB_PP_RESET            = (1 << 1),     /* Reset all Ping-Pong Buffer Pointers to the EVEN BD banks */

   USB_RESUME_SIG          = (1 << 2),     /* Resume signaling activated */

   USB_MODE_HOST_EN        = (1 << 3),     /* USB module operates in Host mode when U1PWRC<0> is set; pull-downs on D+ and D- are activated in hardware*/
   USB_MODE_HOST_DIS       = (0 << 3),     

   USB_PACKET_DIS          = (1 << 5),     /* SIE token and packet processing disabled; automatically set when a SETUP token is received */
   USB_PACKET_EN           = (0 << 5),     /* SIE token and packet processing enabled */

   USB_SE0_FLAG            = (1 << 6),     /* Single-ended zero active on the USB bus */
   /* host */                              
   USB_SOF_EN              = (1 << 0),     /* Start-of-Frame token automatically sent every 1 millisecond */
   USB_SOF_DIS             = (0 << 0),     /* Start-of-Frame token generation disabled  */        

   USB_RESUME_EN           = (1 << 2),     /* Resume signaling activated; software must set bit for 10 ms and then clear to enable remote wake-up */
   USB_RESUME_DIS          = (0 << 2),     /* Resume signaling disabled */

   USB_RESET               = (1 << 4),     /* USB Reset has been generated; for Software Reset, application must set this bit for 50 ms, then clear it */

   USB_TOKEN_BUSY          = (1 << 5),     /* Token being executed by the USB module in On-The-Go state */

   USB_JSTATE_FLAG         = (1 << 7),     /* J state (differential �0� in low speed, differential �1� in full speed) detected on the USB */
} USB_CON;


/* For EP */
typedef enum __USB_EP_CONF
{
   USB_EP_HS_EN            = (1 << 0),     /* Endpoint handshake enabled */
   USB_EP_HS_DIS           = (0 << 0),     /* Endpoint handshake disabled (typically used for isochronous endpoints) */

   USB_EP_STALL            = (1 << 1),     /* Endpoint n was stalled */

   USB_EP_TX_EN            = (1 << 2),     /* Endpoint n transmit enabled  */
   USB_EP_TX_DIS           = (0 << 2),     /* Endpoint n transmit disabled  */

   USB_EP_RX_EN            = (1 << 3),     /* Endpoint n receive enabled   */
   USB_EP_RX_DIS           = (0 << 3),     /* Endpoint n receive disabled  */

   USB_EP_CONTROL_EN       = (0 << 4),     /* Enable Endpoint n for control (SETUP) transfers; TX and RX transfers are also allowed */
   USB_EP_CONTROL_DIS      = (1 << 4),     /* Disable Endpoint n from control transfers; only TX and RX transfers are allowed */

   USB_EP_NAK_RETRY_EN     = (0 << 6),     /* HOST: Retry NAK transactions enabled; retry done in hardware */
   USB_EP_NAK_RETRY_DIS    = (1 << 6),     /* HOST: Retry NAK transactions disabled  */

   USB_EP_LOW_SP_DIR_EN    = (1 << 7),     /* HOST: Direct connection to a low-speed device enabled */
   USB_EP_LOW_SP_DIR_DIS   = (0 << 7),     /* Direct connection to a low-speed device disabled */
} USB_EP_CONF;


/* For PWRC */

#define USB_EN_BIT          0

typedef enum __USB_POW_CON
{
   USB_EN                  = (1 << USB_EN_BIT),     /* USB OTG module is enabled */
   USB_DIS                 = (0 << USB_EN_BIT),     /* USB OTG module is disabled */

   USB_SUSPEND_EN          = (1 << 1),     /* USB OTG module is in Suspend mode; USB clock is gated and the transceiver is placed in a low-power state*/
   USB_SUSPEND_DIS         = (0 << 1),     /* Normal USB OTG operation */

   USB_SUSPEND_GUARD       = (1 << 4),     /* Indicate to the USB module that it is about to be suspended or powered down  */

   USB_STAT_ACTIVE         = (1 << 7)      /* Module should not be suspended at the moment (requires GUARD bit to be set) */
} USB_POW_CON;


/* For CNFG1 and CNFG2 */

typedef enum E_CSP_USB_Cfg
{
   //-- CNFG1 reg
   USB_OTG_IDLE_STOP          = (1 <<  4),    /* Discontinue module operation when device enters Idle mode    */
   USB_OTG_IDLE_CON           = (0 <<  4),    /* Continue module operation in Idle mode */

   USB_OE_SIG_EN              = (1 <<  6),    /* OE signal active; it indicates intervals during which the D+/D- lines are driving */
   USB_OE_SIG_DIS             = (0 <<  6),    /* OE signal inactive */

   USB_EYE_TEST_EN            = (1 <<  7),    /* Eye pattern test enabled */
   USB_EYE_TEST_DIS           = (0 <<  7),    /* Eye pattern test disbled */

#if 0
   USB_VBUS_PULLUP_EN         = (1 <<  1 << 8),    /* Pull-up on VBUS pin enabled */
   USB_VBUS_PULLUP_DIS        = (0 <<  1 << 8),    /* Pull-up on VBUS pin disabled */

   USB_VBUS_PULLDN_EN         = (1 <<  1 << 8),    /* Pull-up on VBUS pin enabled */
   USB_VBUS_PULLDN_DIS        = (0 <<  1 << 8),    /* Pull-up on VBUS pin disabled */
#endif

#if 0
   USB_PP_DIS                 = (0 <<  0),    /* EVEN/ODD ping-pong buffers disabled                      */
   USB_PP_EP0_OUT_EN          = (1 <<  0),    /* EVEN/ODD ping-pong buffer enabled for OUT Endpoint 0     */
   USB_PP_EN                  = (2 <<  0),    /* EVEN/ODD ping-pong buffers enabled for all endpoints     */
   USB_PP_EP1_EP15_EN         = (3 <<  0),    /* EVEN/ODD ping-pong buffers enabled for Endpoints 1 to 15 */

   USB_CHIP_TRANS_DIS         = (1 <<  8),    /* On-chip transceiver and VBUS detection disabled; digital transceiver interface enabled */
   USB_CHIP_TRANS_EN          = (0 <<  8),    /* On-chip transceiver and VBUS detection active */

   USB_VBUS_COMP_DIS          = (1 <<  9),    /* On-chip charge VBUS comparator disabled; digital input status interface enabled */
   USB_VBUS_COMP_EN           = (0 <<  9),    /* On-chip charge VBUS comparator active */

   USB_5V_BOOST_DIS           = (1 << 10),    /* On-chip boost regulator circuit disabled; digital output control interface enabled */
   USB_5V_BOOST_EN            = (0 << 10),    /* On-chip boost regulator circuit active */

   USB_EXT_TRANS_I2C          = (1 << 11),    /* External module(s) controlled via I2C interface */
   USB_EXT_TRANS_PINS         = (0 << 11),    /* External module(s) controlled via standart pin interface */

   USB_VBUS_PULLUP_EN         = (1 << 12),    /* Pull-up on VBUS pin enabled */
   USB_VBUS_PULLUP_DIS        = (0 << 12),    /* Pull-up on VBUS pin disabled */

   USB_VBUS_COMP_3_PIN        = (1 << 13),    /* Use 3-pin input configuration for VBUS comparators */
   USB_VBUS_COMP_2_PIN        = (0 << 13),    /* Use 2-pin input configuration for VBUS comparators */
#endif
} T_CSP_USB_Cfg;



/* For EIR */

typedef enum __USB_ERR
{
   USB_ERR_PID_CHECK       = (1 << 0),     /* PID Check Failure                */
   USB_ERR_EOF             = (1 << 1),     /* HOST: End-Of-Frame (EOF) Error   */
   USB_ERR_CRC5            = (1 << 1),     /* DEVICE: CRC5 Host Error          */
   USB_ERR_CRC16           = (1 << 2),     /* CRC16 Failure                    */
   USB_ERR_DATA_FIELD_SIZE = (1 << 3),     /* Data Field Size Error            */
   USB_ERR_BUS_TO          = (1 << 4),     /* Bus Turnaround Time-out Error    */
   USB_ERR_DMA             = (1 << 5),     /* DMA Error                        */
   USB_ERR_BIT_STUFF       = (1 << 7),     /* Bit Stuff Error                  */
   USB_ERR_ALL             = (0xFF)
} USB_ERR;


/* For IR   */
typedef enum __USB_INT
{
   USB_INT_DETACH          = (1 << 0),     /* HOST: A peripheral detachment has been detected by the module; Reset state must be cleared before this bit can be re-asserted */
   USB_INT_USB_RESET       = (1 << 0),     /* DEVICE: Valid USB Reset has occurred for at least 2.5 ?s; Reset state must be cleared before this bit can be re-asserted     */
   USB_INT_ERR             = (1 << 1),     /* USB Error Condition Interrupt    */
   USB_INT_SOF             = (1 << 2),     /* Start-of-Frame Token Interrupt   */
   USB_INT_TOKEN_COMP      = (1 << 3),     /* HOST: Token Processing Complete Interrupt  */
   USB_INT_TRANS_COMP      = (1 << 3),     /* DEVICE: Transaction Complete Interrupt   */
   USB_INT_IDLE            = (1 << 4),     /* Idle Detect Interrupt    */
   USB_INT_RESUME          = (1 << 5),     /* Resume Interrupt         */
   USB_INT_ATTACH          = (1 << 6),     /* HOST: Peripheral Attach Interrupt    */
   USB_INT_STALL           = (1 << 7),     /* STALL Handshake Interrupt    */
   USB_INT_ALL             = (0xFF)
} USB_INT;

typedef enum __USB_OTG_INT
{
   USB_OTG_INT_A_VBUS_CH   = (1 << 0),     /* VBUS change on A-device detected; VBUS has crossed VA_VBUS_VLD (as defined in the USB OTG Specification) */
   USB_OTG_INT_B_VBUS_CH   = (1 << 2),     /* VBUS change on B-device detected; VBUS has crossed VB_SESS_END (as defined in the USB OTG Specification) */
   USB_OTG_INT_SES_VALID   = (1 << 3),     /* VBUS has crossed VA_SESS_VLD (as defined in the USB OTG Specification) */
   USB_OTG_INT_ACTIVITY    = (1 << 4),     /* Activity on the D+/D- lines or VBUS detected */
   USB_OTG_INT_LINE_STATE  = (1 << 5),     /* USB line state (as defined by the SE0 and JSTATE bits) has been stable for 1ms, but different from last time */
   USB_OTG_INT_1_MS        = (1 << 6),     /* The 1 millisecond timer has expired  */
   USB_OTG_INT_ID_CHANGE   = (1 << 7),     /* Change in ID state detected  */
} USB_OTG_INT;


/* For OTGCON */
typedef enum __USB_OTG_CON
{
   USB_OTG_VBUS_DISCH_EN   = (1 << 0),
   USB_OTG_VBUS_DISCH_DIS  = (0 << 0),

   //USB_OTG_VBUS_CH_3V      = (1 << 1),
   //USB_OTG_VBUS_CH_5V      = (0 << 1),

   USB_OTG_EN              = (1 << 2),
   USB_OTG_DIS             = (0 << 2),

   USB_OTG_VBUS_EN         = (1 << 3),
   USB_OTG_VBUS_DIS        = (0 << 3),

   USB_OTG_DM_PULLDW_EN    = (1 << 4),
   USB_OTG_DM_PULLDW_DIS   = (0 << 4),

   USB_OTG_DP_PULLDW_EN    = (1 << 5),
   USB_OTG_DP_PULLDW_DIS   = (0 << 5),

   USB_OTG_DM_PULLUP_EN    = (1 << 6),
   USB_OTG_DM_PULLUP_DIS   = (0 << 6),

   USB_OTG_DP_PULLUP_EN    = (1 << 7),
   USB_OTG_DP_PULLUP_DIS   = (0 << 7),
} USB_OTG_CON;


/* For OTGSTAT */
typedef enum __USB_OTGSTAT
{
   USB_ID_PIN_STATE        = (1 << 7),     /* No cable is attached or a type B plug has been plugged into the USB receptacle   */
   USB_LINE_STATE_STABLE   = (1 << 5),     /* The USB line state (as defined by SE0 and JSTATE) has been stable for the previous 1 ms  */
   USB_VBUS_VA_VBUS_VLD    = (1 << 0),     /* The VBUS voltage is above VA_VBUS_VLD (as defined in the USB OTG Specification) on the A-device      (5.1.1 / 4.4V)  */
   USB_VBUS_VA_SESS_VLD    = (1 << 3),     /* The VBUS voltage is above VA_SESS_VLD (as defined in the USB OTG Specification) on the A or B-device (5.3.6 / 2.0V)  */
   USB_VBUS_VB_SESS_END    = (1 << 2)      /* The VBUS voltage is below VB_SESS_END (as defined in the USB OTG Specification) on the B-device      (5.3.2 / 0.8V)  */
} USB_OTGSTAT;

/* For STAT */

#define __USB_STAT_LAST_DIR_BIT     3
#define __USB_STAT_LAST_PP_BIT      2

typedef enum __USB_STAT
{
   USB_STAT_PP_LAST_ODD    = (1  << __USB_STAT_LAST_PP_BIT),
   USB_STAT_PP_LAST_EVEN   = (0  << __USB_STAT_LAST_PP_BIT),

   USB_STAT_LAST_TX        = (1  << __USB_STAT_LAST_DIR_BIT),
   USB_STAT_LAST_RX        = (0  << __USB_STAT_LAST_DIR_BIT),

   USB_STAT_LAST_EP0       = (0  << 4),
   USB_STAT_LAST_EP1       = (1  << 4),
   USB_STAT_LAST_EP2       = (2  << 4),
   USB_STAT_LAST_EP3       = (3  << 4),
   USB_STAT_LAST_EP4       = (4  << 4),
   USB_STAT_LAST_EP5       = (5  << 4),
   USB_STAT_LAST_EP6       = (6  << 4),
   USB_STAT_LAST_EP7       = (7  << 4),
   USB_STAT_LAST_EP8       = (8  << 4),
   USB_STAT_LAST_EP9       = (9  << 4),
   USB_STAT_LAST_EP10      = (10 << 4),
   USB_STAT_LAST_EP11      = (11 << 4),
   USB_STAT_LAST_EP12      = (12 << 4),
   USB_STAT_LAST_EP13      = (13 << 4),
   USB_STAT_LAST_EP14      = (14 << 4),
   USB_STAT_LAST_EP15      = (15 << 4),

} USB_STAT;

#define __USB_STAT_LAST_EP_FS       4
#define __USB_STAT_LAST_EP_FE       7



/* Buffer Descriptor Status Register layout */

typedef union __USB_BD_STAT
{
   struct {
unsigned        : 2;
                  unsigned BSTALL : 1;    /* Buffer Stall Enable      */
                  unsigned DTSEN  : 1;    /* Data Toggle Synch Enable */
unsigned        : 2;
                  unsigned DTS    : 1;    /* Data Toggle Synch Value  */
                  unsigned UOWN   : 1;    /* USB Ownership            */
   };
   struct {
unsigned        : 2;
                  unsigned PID    : 4;    /* Packet Identifier        */
unsigned        : 2;
   };
   U08 Val;
} USB_BD_STAT;                      //Buffer Descriptor Status Register


/* BDT Entry Layout */

typedef union __USB_BDT_ENTRY
{
   union {
      struct {
         U08         CNT  ;
         USB_BD_STAT STAT __packed;
      };
      struct {
         unsigned count : 10;
unsigned       :  6;
                  U08* ADDR;
      };
   };
   U32    Val;
   UPWORD  v[2];
} USB_BDT_ENTRY;


#define BD_STALL      (1 << 2)          /* Buffer Stall enable      */
#define BD_DTSEN      (1 << 3)          /* Data Toggle Synch enable */

#define BD_DAT1       (1 << 6)          /* DATA1 packet expected next   */
#define BD_DAT0       (0 << 6)          /* DATA0 packet expected next   */

#define BD_USIE       (1 << 7)          /* SIE owns buffer          */
#define BD_UCPU       (0 << 7)          /* CPU owns buffer          */

#define BD_DTS_MASK         (1 << 6)    /* DTS Mask     */
#define USB_BD_STAT_MASK    (0xFC)      /* 0b11111100   */

#endif





/* External functions prototypes */
/* ----------------------------- */

void csp_usb_pp_next (volatile USB_BDT_ENTRY **p);

//-- OTG status
USB_OTGSTAT csp_usb_otg_stat_get (void);

//-- usb interrupts
void csp_usb_int_en (USB_INT mask);
void csp_usb_int_dis (USB_INT mask);
USB_INT csp_usb_int_get (void);
USB_INT csp_usb_int_flag_get (void);
void csp_usb_int_flag_clr (USB_INT mask);

//-- usb errors
void csp_usb_err_en (USB_ERR mask);
void csp_usb_err_dis (USB_ERR mask);
USB_ERR csp_usb_err_flag_get (void);
void csp_usb_err_flag_clr (USB_ERR mask);

//-- USB OTG int
void csp_usb_otg_int_en (USB_OTG_INT mask);
void csp_usb_otg_int_dis (USB_OTG_INT mask);
USB_OTG_INT csp_usb_otg_int_get (void);
USB_OTG_INT csp_usb_otg_int_flag_get (void);
void csp_usb_otg_int_flag_clr (USB_OTG_INT mask);

//-- OTG conf
void csp_usb_otg_con_put (USB_OTG_CON val);
USB_OTG_CON csp_usb_otg_con_get (void);
void csp_usb_otg_con_set (USB_OTG_CON mask);
void csp_usb_otg_con_clr (USB_OTG_CON mask);

//-- USB config
void csp_usb_cnfg_set (T_CSP_USB_Cfg val);
T_CSP_USB_Cfg csp_usb_cnfg_get (void);


//-- USB CON
void csp_usb_con_put (USB_CON val);
USB_CON csp_usb_con_get (void);
void csp_usb_con_set (USB_CON mask);
void csp_usb_con_clr (USB_CON mask);

//-- USB power
void csp_usb_power_conf_set (USB_POW_CON val);
USB_POW_CON csp_usb_power_conf_get (void);
void csp_usb_en (void);
void csp_usb_dis (void);

//-- BDT
void csp_usb_bdt_set (void *pt);

//-- Addr
void csp_usb_addr_set (UPWORD addr);
UPWORD csp_usb_addr_get (void);

//-- EP
void csp_usb_ep_conf_put (USB_EP_INDX ep, USB_EP_CONF val);
USB_EP_CONF csp_usb_ep_conf_get (USB_EP_INDX ep);
void csp_usb_ep_conf_set (USB_EP_INDX ep, USB_EP_CONF mask);
void csp_usb_ep_conf_clr (USB_EP_INDX ep, USB_EP_CONF mask);

//-- Stat
USB_STAT csp_usb_stat_get (void);
USB_EP_INDX csp_usb_stat_ep_get (void);

#define CSP_USB_EP_LAST_INDX(val)   __CSP_ACC_RANG_REG(BFA_RD, val, __USB_STAT_LAST_EP_FS, __USB_STAT_LAST_EP_FE)
#define CSP_USB_EP_LAST_DIR(val)    __CSP_BIT_VAL_GET(val, __USB_STAT_LAST_DIR_BIT)
#define CSP_USB_EP_LAST_PP(val)     __CSP_BIT_VAL_GET(val, __USB_STAT_LAST_PP_BIT)


#if ((__CSP_FAMILY == __CSP_FAMILY_PIC32MX3XX_4XX))
#if !defined(__CSP_BUILD_LIB)

/* Module API definition */

#if defined(__CSP_DEBUG_USB)


#else


#endif

#endif
#endif

#endif /* #ifndef __CSP_USB_H */
/* ********************************************************************************************** */
/* end of file: csp_usb.h */
