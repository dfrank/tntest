/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_spi_bcgt.c
     Description:

   ********************************************************************************************** */

#include "csp_spi.h"

//#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
//#endif

#if defined __32MX440F512H__

//TODO: SPI1 is not present only on PIC32MX420FXXXX/440FXXXX devices.
//T_CSP_SPI_Reg CSP_SPI1 __CSP_SFR((CSP_SPI1_BASE_VIRT_ADDR));
T_CSP_SPI_Reg CSP_SPI2 __CSP_SFR((CSP_SPI2_BASE_VIRT_ADDR));

#else
#error CPU is not yet supported by CSP SPI module
#endif


//TODO
#if   CSP_SPI__SPI_ADVANCED_MODULE
#if 0
__CSP_FUNC UPWORD  csp_spi_bufcnt_get (T_CSP_SPI_Reg *spi)
{
    return __CSP_ACC_RANG_VAL(BFA_RD, spi->SPISTAT, __CSP_SPI_BC_FS, __CSP_SPI_BC_FE);
}
#endif
#endif


__CSP_FUNC enum E_CSP_SPI_BaudRatePrescaler csp_spi_baud_get (T_CSP_SPI_Reg *spi)
{
   return (enum E_CSP_SPI_BaudRatePrescaler)__CSP_ACC_RANG_VAL(BFA_RD, spi->BRG, __CSP_SPI_BAUD_FS, __CSP_SPI_BAUD_FE);
}


__CSP_FUNC void  csp_spi_baud_set (T_CSP_SPI_Reg *spi, enum E_CSP_SPI_BaudRatePrescaler val)
{
   //TODO: atomic!
    __CSP_ACC_RANG_VAL(BFA_WR, spi->BRG, __CSP_SPI_BAUD_FS, __CSP_SPI_BAUD_FE, val);
}


__CSP_FUNC U64  csp_spi_conf_get (T_CSP_SPI_Reg *spi)
{
    AL_EXT_64 ret;

    ret.word_1 = spi->CON;
#if CSP_SPI__SPI_ADVANCED_MODULE
    ret.word_0 = spi->CON2;
#endif

    return ret.full;
}


__CSP_FUNC void  csp_spi_conf_set (T_CSP_SPI_Reg *spi, U64 val)
{
    spi->CON  = ((AL_EXT_64)val).word_1;
#if CSP_SPI__SPI_ADVANCED_MODULE
    spi->CON2 = ((AL_EXT_64)val).word_0;
#endif
}


__CSP_FUNC void  csp_spi_dis (T_CSP_SPI_Reg *spi)
{
   __CSP_CLR_REG(&spi->CON) = (1 << __CSP_SPI_EN_BIT);
    //__CSP_BIT_VAL_CLR(spi->CON, __CSP_SPI_EN_BIT);
}


__CSP_FUNC void  csp_spi_en (T_CSP_SPI_Reg *spi)
{
   __CSP_SET_REG(&spi->CON) = (1 << __CSP_SPI_EN_BIT);
    //__CSP_BIT_VAL_SET(spi->CON, __CSP_SPI_EN_BIT);
}


__CSP_FUNC UPWORD  csp_spi_exch (T_CSP_SPI_Reg *spi, UPWORD val)
{
    spi->BUF = val;
    while (!(__CSP_BIT_VAL_GET(spi->STAT, __CSP_SPI_STAT_TX_BUF_EMPTY_BIT)));
    while (!(__CSP_BIT_VAL_GET(spi->STAT, __CSP_SPI_STAT_RX_BUF_FULL_BIT)));
    return spi->BUF;
}


__CSP_FUNC UPWORD  csp_spi_get (T_CSP_SPI_Reg *spi)
{
    return (spi->BUF);
}

           
//TODO
#if   CSP_SPI__SPI_ADVANCED_MODULE
#if 0
__CSP_FUNC SPI_INT_MODE csp_spi_int_mode_get(T_CSP_SPI_Reg *spi)
{
    return (SPI_INT_MODE)__CSP_ACC_RANG_VAL(BFA_RD, spi->SPISTAT, __CSP_SPI_INT_MODE_FS, __CSP_SPI_INT_MODE_FE);
}

           
__CSP_FUNC void  csp_spi_int_mode_set(T_CSP_SPI_Reg *spi, SPI_INT_MODE val)
{
    __CSP_ACC_RANG_VAL(BFA_WR, spi->SPISTAT, __CSP_SPI_INT_MODE_FS, __CSP_SPI_INT_MODE_FE, val);
}
#endif
#endif


__CSP_FUNC void  csp_spi_put_bl (T_CSP_SPI_Reg *spi, UPWORD val)
{
    while (__CSP_BIT_VAL_GET(spi->STAT, __CSP_SPI_STAT_TX_BUF_FULL_BIT)); /* SPI_STAT_TX_BUF_FULL */
    spi->BUF = val;
}


__CSP_FUNC void  csp_spi_put (T_CSP_SPI_Reg *spi, UPWORD val)
{
    spi->BUF = val;
}


__CSP_FUNC void  csp_spi_stat_clr (T_CSP_SPI_Reg *spi, enum E_CSP_Stat mask)
{
   __CSP_CLR_REG(&spi->STAT) = mask;
    //__CSP_ACC_MASK_VAL(BFA_CLR, spi->STAT, __CSP_SPI_STAT_MASK, mask);
}


__CSP_FUNC enum E_CSP_Stat  csp_spi_stat_get (T_CSP_SPI_Reg *spi)
{
   return (enum E_CSP_Stat)__CSP_ACC_MASK_VAL(BFA_RD, spi->STAT, __CSP_SPI_STAT_MASK);
}

