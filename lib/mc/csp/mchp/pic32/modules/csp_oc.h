/* *************************************************************************************************
Project:         MCHP 16-bit CPU Support Library
Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

 *************************************************************************************************
Distribution:

MCHP 16-bit CPU Support Library
----------------------------------------
Copyright � 2006-2008 Alex B.

 *************************************************************************************************
 MCU Family:      PIC24F/PIC24H/dsPIC33
Compiler:        Microchip C30 3.11

 *************************************************************************************************
File:            csp_oc.h
Description:

 ********************************************************************************************** */

#ifndef __CSP_OC_H
#define __CSP_OC_H

#include "../cspi.h"

/* Output compare programming model */
/* -------------------------------- */

typedef struct S_CSP_OC
{
   volatile UPWORD OCCON;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD OCR;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD OCRS;
   __CSP_SKIP_BYTE(12);
} T_CSP_OC;

/* Output compare base addresses */
/* ----------------------------- */

/* Declare Output Compare models */
/* ----------------------------- */

#ifdef _OCMP1
extern T_CSP_OC CSP_OC1;
#endif

#ifdef _OCMP2
extern T_CSP_OC CSP_OC2;
#endif

#ifdef _OCMP3
extern T_CSP_OC CSP_OC3;
#endif

#ifdef _OCMP4
extern T_CSP_OC CSP_OC4;
#endif

#ifdef _OCMP5
extern T_CSP_OC CSP_OC5;
#endif


/* Internal API definitions */
/* ------------------------ */

/* Definitions for Output compare API */
/* ---------------------------------- */

/* csp_oc_conf_set() and csp_oc_conf_get() parameters mask */

#define CSP_OC_EN                   (1 << 15)   /* Output compare peripheral is enabled */
#define CSP_OC_DIS                  (0 << 15)   /* Output compare peripheral is disabled and not drawing current. */

#define CSP_OC_IDLE_STOP            (1 << 13)   /* Output compare will halt in CPU Idle mode */
#define CSP_OC_IDLE_CON             (0)         /* Output compare will continue to operate in CPU Idle mode */

#define CSP_OC_CMP_MODE_32          (1 <<  5)   /* OCR and/or OCRS is used for comparsions to the 32-bit timer source */
#define CSP_OC_CMP_MODE_16          (0 <<  5)   /* OCR and OCRS are used for comparsions to the 16-bit timer source */

#define CSP_OC_BASE_TMR3            (1 <<  3)   /* Timer3 is the clock source for Output Compare */
#define CSP_OC_BASE_TMR2            (0)         /* Timer2 is the clock source for Output Compare */

#define CSP_OC_MODE_DIS             (0 <<  0)   /* Output compare channel is disabled */
#define CSP_OC_MODE_SINGLE_LOW_HIGH (1 <<  0)   /* Initialize OCx pin low, compare event forces OCx pin high */
#define CSP_OC_MODE_SINGLE_HIGH_LOW (2 <<  0)   /* Initialize OCx pin high, compare event forces OCx pin low */
#define CSP_OC_MODE_SINGLE_TOGGLE   (3 <<  0)   /* Compare event toggles OCx pin */
#define CSP_OC_MODE_DUAL_SING_PULSE (4 <<  0)   /* Initialize OCx pin low, generate single output pulse on OCx pin */
#define CSP_OC_MODE_DUAL_CONT_PULSE (5 <<  0)   /* Initialize OCx pin low, generate continuous output pulses on OCx pin */
#define CSP_OC_MODE_PWM_FAULT_DIS   (6 <<  0)   /* PWM mode on OCx, Fault pin disabled */
#define CSP_OC_MODE_PWM_FAULT_EN    (7 <<  0)   /* PWM mode on OCx, Fault pin enabled */

/* csp_oc_stat_get() return type */

enum E_CSP_OS_Stat
{
   CSP_OC_STAT_FAULT = (1 <<  4)   /* PWM Fault condition has occurred - RO */
};


/* External functions prototypes */
/* ----------------------------- */


UPWORD  csp_oc_conf_get (T_CSP_OC *oc);
void  csp_oc_conf_set (T_CSP_OC *oc, UPWORD val);

enum E_CSP_OS_Stat csp_oc_stat_get(T_CSP_OC *oc);

void    csp_oc_r_set(T_CSP_OC *oc, UPWORD val);
UPWORD   csp_oc_r_get(T_CSP_OC *oc);

void    csp_oc_rs_set(T_CSP_OC *oc, UPWORD val);
UPWORD   csp_oc_rs_get(T_CSP_OC *oc);

#if !defined(__CSP_BUILD_LIB)

/* Module API definition */

#if defined(__CSP_DEBUG_OC)

//-- prototypes were here

#else

#include "csp_oc.c"

#endif

#endif

#endif /* #ifndef __CSP_OC_H */
/* ********************************************************************************************** */
/* end of file: csp_oc.h */
