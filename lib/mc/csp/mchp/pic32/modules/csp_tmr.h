/* *************************************************************************************************
Project:         MCHP 16-bit CPU Support Library
Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

 *************************************************************************************************
Distribution:

MCHP 16-bit CPU Support Library
----------------------------------------
Copyright � 2006-2008 Alex B.

 *************************************************************************************************
 MCU Family:      PIC24F/PIC24H/dsPIC33
Compiler:        Microchip C30 3.11

 *************************************************************************************************
File:            csp_oc.h
Description:

 ********************************************************************************************** */

#ifndef __CSP_TMR_H
#define __CSP_TMR_H

#include "../cspi.h"

/* Output compare programming model */
/* -------------------------------- */

typedef volatile struct S_CSP_TmrTypeA
{
   __CSP_REG_DEF(CON);
   __CSP_REG_DEF(TMR);
   __CSP_REG_DEF(PR);
} T_CSP_TmrTypeA;

typedef volatile struct S_CSP_TmrTypeB
{
   __CSP_REG_DEF(CON);
   __CSP_REG_DEF(TMR);
   __CSP_REG_DEF(PR);
} T_CSP_TmrTypeB;

/* Output compare base addresses */
/* ----------------------------- */

/* Declare Output Compare models */
/* ----------------------------- */

extern T_CSP_TmrTypeA CSP_TMR_1;
extern T_CSP_TmrTypeB CSP_TMR_2;
extern T_CSP_TmrTypeB CSP_TMR_3;
extern T_CSP_TmrTypeB CSP_TMR_4;
extern T_CSP_TmrTypeB CSP_TMR_5;


/* Internal API definitions */
/* ------------------------ */

#define _CSP_OFS_TMR_A__ON             15
#define _CSP_OFS_TMR_A__SIDL           13
#define _CSP_OFS_TMR_A__TWDIS          12
#define _CSP_OFS_TMR_A__TWIP           11
#define _CSP_OFS_TMR_A__TGATE          7
#define _CSP_OFS_TMR_A__TCKPS          4
#define _CSP_OFS_TMR_A__TSYNC          2
#define _CSP_OFS_TMR_A__TCS            1

#define _CSP_OFS_TMR_B__ON             15
#define _CSP_OFS_TMR_B__SIDL           13
#define _CSP_OFS_TMR_B__TGATE          7
#define _CSP_OFS_TMR_B__TCKPS          4
#define _CSP_OFS_TMR_B__T32            3
#define _CSP_OFS_TMR_B__TCS            1



/* Definitions for Output compare API */
/* ---------------------------------- */

/* csp_tmr_a_conf_set() and csp_tmr_a_conf_get() parameters mask */

#define CSP_TMR_A__EN                     (1 << _CSP_OFS_TMR_A__ON)    /* Timer is enabled */
#define CSP_TMR_A__DIS                    (0)                          /* Timer is disabled */

#define CSP_TMR_A__IDLE_STOP              (1 << _CSP_OFS_TMR_A__SIDL)  /* Discontinue timer operation when device enters Idle mode */
#define CSP_TMR_A__IDLE_CON               (0)                          /* Continue timer operation in Idle mode                    */

#define CSP_TMR_A__ASYNC_WRITE_DIS        (1 << _CSP_OFS_TMR_A__TWDIS) /* Writes to TMR are ignored until pending write operation completes */
#define CSP_TMR_A__ASYNC_WRITE_EN         (0)                          /* Back-to-back writes are enabled (Legacy Asynchronous Timer functionality) */

#define CSP_TMR_A__GATE_EN                (1 << _CSP_OFS_TMR_A__TGATE) /* Gated time accumulation is enabled */
#define CSP_TMR_A__GATE_DIS               (0)                          /* Gated time accumulation is disabled */

//-- NOTE: When using 1:1 PBCLK divisor, the user�s software should not read/write the peripheral SFRs in the 
//         SYSCLK cycle immediately following the instruction that clears the module�s ON bit.
#define CSP_TMR_A__PS_1_1                 (0 << _CSP_OFS_TMR_A__TCKPS) /* 1:1 prescale value */
#define CSP_TMR_A__PS_1_8                 (1 << _CSP_OFS_TMR_A__TCKPS) /* 1:8 prescale value */
#define CSP_TMR_A__PS_1_64                (2 << _CSP_OFS_TMR_A__TCKPS) /* 1:64 prescale value */
#define CSP_TMR_A__PS_1_256               (3 << _CSP_OFS_TMR_A__TCKPS) /* 1:256 prescale value */

#define CSP_TMR_A__SYNC_EXT_EN            (1 << _CSP_OFS_TMR_A__TSYNC) /* External clock input is synchronized */
#define CSP_TMR_A__SYNC_EXT_DIS           (0)                          /* External clock input is not synchronized */

#define CSP_TMR_A__SOURCE_EXT             (1 << _CSP_OFS_TMR_A__TCS)   /* External clock from TxCLl pin */
#define CSP_TMR_A__SOURCE_INT             (0)                          /* Internal peripheral clock */





/* csp_tmr_b_conf_set() and csp_tmr_b_conf_get() parameters mask */

#define CSP_TMR_B__EN                     (1 << _CSP_OFS_TMR_B__ON)    /* Module is enabled */
#define CSP_TMR_B__DIS                    (0)                          /* Module is disabled */

#define CSP_TMR_B__IDLE_STOP              (1 << _CSP_OFS_TMR_B__SIDL)  /* Discontinue timer operation when device enters Idle mode */
#define CSP_TMR_B__IDLE_CON               (0)                          /* Continue timer operation in Idle mode                    */

//-- only when TCS = 0:
#define CSP_TMR_B__GATE_EN                (1 << _CSP_OFS_TMR_B__TGATE) /* Gated time accumulation is enabled */
#define CSP_TMR_B__GATE_DIS               (0)                          /* Gated time accumulation is disabled */

#define CSP_TMR_B__PS_1_1                 (0 << _CSP_OFS_TMR_B__TCKPS) /* 1:1 prescale value */
#define CSP_TMR_B__PS_1_2                 (1 << _CSP_OFS_TMR_B__TCKPS) /*  */
#define CSP_TMR_B__PS_1_4                 (2 << _CSP_OFS_TMR_B__TCKPS) /*  */
#define CSP_TMR_B__PS_1_8                 (3 << _CSP_OFS_TMR_B__TCKPS) /*  */
#define CSP_TMR_B__PS_1_16                (4 << _CSP_OFS_TMR_B__TCKPS) /*  */
#define CSP_TMR_B__PS_1_32                (5 << _CSP_OFS_TMR_B__TCKPS) /*  */
#define CSP_TMR_B__PS_1_64                (6 << _CSP_OFS_TMR_B__TCKPS) /*  */
#define CSP_TMR_B__PS_1_256               (7 << _CSP_OFS_TMR_B__TCKPS) /*  */

//-- 32 bit is available only on even-numbered Type B timers, such as CSP_TMR_2, CSP_TMR_4, and so on.
#define CSP_TMR_B__32_BIT                 (1 << _CSP_OFS_TMR_B__T32)    /* TMRx and TMRy form a 32-bit timer */
#define CSP_TMR_B__16_BIT                 (0)                          /* TMRx and TMRy form separate 16-bit timers */

#define CSP_TMR_B__SOURCE_EXT             (1 << _CSP_OFS_TMR_B__TCS)   /* External clock from TxCK pin */
#define CSP_TMR_B__SOURCE_INT             (0)                          /* Internal peripheral clock */


/* csp_oc_stat_get() return type */

enum E_CSP_TMR_A_Status
{
   CSP_TMR_A_STATUS__ASYNC_TMR_WRITE_IN_PROGRESS = (1 << _CSP_OFS_TMR_A__TWIP),   /* Asyncronous write to TMR register in progress */
};

#define CSP_TMR_A_STATUS_MASK  (0                                 \
      | CSP_TMR_A_STATUS__ASYNC_TMR_WRITE_IN_PROGRESS             \
      )




/* External functions prototypes */
/* ----------------------------- */

void     csp_tmr_a__conf__set          (T_CSP_TmrTypeA *p_tmr_a, UPWORD val);
UPWORD   csp_tmr_a__conf__get          (T_CSP_TmrTypeA *p_tmr_a);
void     csp_tmr_a__en                 (T_CSP_TmrTypeA *p_tmr_a);
void     csp_tmr_a__dis                (T_CSP_TmrTypeA *p_tmr_a);
void     csp_tmr_a__period__set        (T_CSP_TmrTypeA *p_tmr_a, UPWORD period);
UPWORD   csp_tmr_a__period__get        (T_CSP_TmrTypeA *p_tmr_a);
void     csp_tmr_a__value__set         (T_CSP_TmrTypeA *p_tmr_a, UPWORD val);
UPWORD   csp_tmr_a__value__get         (T_CSP_TmrTypeA *p_tmr_a);
enum E_CSP_TMR_A_Status csp_tmr_a__status__get(T_CSP_TmrTypeA *p_tmr_a);

void     csp_tmr_b__conf__set          (T_CSP_TmrTypeB *p_tmr_b, UPWORD val);
UPWORD   csp_tmr_b__conf__get          (T_CSP_TmrTypeB *p_tmr_b);
void     csp_tmr_b__en                 (T_CSP_TmrTypeB *p_tmr_b);
void     csp_tmr_b__dis                (T_CSP_TmrTypeB *p_tmr_b);
void     csp_tmr_b__period__set        (T_CSP_TmrTypeB *p_tmr_b, UPWORD period);
UPWORD   csp_tmr_b__period__get        (T_CSP_TmrTypeB *p_tmr_b);
void     csp_tmr_b__value__set         (T_CSP_TmrTypeB *p_tmr_b, UPWORD val);
UPWORD   csp_tmr_b__value__get         (T_CSP_TmrTypeB *p_tmr_b);

/**
 * NOTE for csp_tmr_b__value_32bit__get() / ...set() :
 * For Timer2+Timer3 32-bit value, first argument can be either &CSP_TMR_2 or &CSP_TMR_3.
 * The same is for Timer4+Timer5 32-bit value.
 */
U32      csp_tmr_b__value_32bit__get   (T_CSP_TmrTypeB *p_tmr_b);
void     csp_tmr_b__value_32bit__set   (T_CSP_TmrTypeB *p_tmr_b, U32 val);

U32      csp_tmr_b__period_32bit__get  (T_CSP_TmrTypeB *p_tmr_b);
void     csp_tmr_b__period_32bit__set  (T_CSP_TmrTypeB *p_tmr_b, U32 period);


#if !defined(__CSP_BUILD_LIB)

/* Module API definition */

#if defined(__CSP_DEBUG_OC)

//-- prototypes were here

#else

#include "csp_tmr.c"

#endif

#endif

#endif /* #ifndef __CSP_TMR_H */
/* ********************************************************************************************** */
/* end of file: csp_oc.h */
