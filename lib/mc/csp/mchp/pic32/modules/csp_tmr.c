/* *************************************************************************************************
Project:         MCHP 16-bit CPU Support Library
Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

 *************************************************************************************************
Distribution:

MCHP 16-bit CPU Support Library
----------------------------------------
Copyright � 2006-2008 Alex B.

 *************************************************************************************************
 MCU Family:      PIC24F/PIC24H/dsPIC33
Compiler:        Microchip C30 3.11

 *************************************************************************************************
File:            csp_oc_cget.c
Description:

 ********************************************************************************************** */

#include "csp_tmr.h"

#include "../cspi.h"

#if defined(__CSP_BUILD_LIB)

#define _TMR1_BASE_VIRT_ADDR         (0xBF800000 + 0x0600)
#define _TMR2_BASE_VIRT_ADDR         (0xBF800000 + 0x0800)
#define _TMR3_BASE_VIRT_ADDR         (0xBF800000 + 0x0a00)
#define _TMR4_BASE_VIRT_ADDR         (0xBF800000 + 0x0c00)
#define _TMR5_BASE_VIRT_ADDR         (0xBF800000 + 0x0e00)

T_CSP_TmrTypeA CSP_TMR_1 __CSP_SFR((_TMR1_BASE_VIRT_ADDR));
T_CSP_TmrTypeB CSP_TMR_2 __CSP_SFR((_TMR2_BASE_VIRT_ADDR));
T_CSP_TmrTypeB CSP_TMR_3 __CSP_SFR((_TMR3_BASE_VIRT_ADDR));
T_CSP_TmrTypeB CSP_TMR_4 __CSP_SFR((_TMR4_BASE_VIRT_ADDR));
T_CSP_TmrTypeB CSP_TMR_5 __CSP_SFR((_TMR5_BASE_VIRT_ADDR));

#endif

__attribute__((always_inline))
static inline void _32bit_timer_pointers_get(
      T_CSP_TmrTypeB *p_tmr_b,            //-- input
      T_CSP_TmrTypeB **pp_tmr_least_s,    //-- output
      T_CSP_TmrTypeB **pp_tmr_most_s      //-- output
      )
{
   if (p_tmr_b == &CSP_TMR_2 || p_tmr_b == &CSP_TMR_3){
      *pp_tmr_least_s = &CSP_TMR_2;
      *pp_tmr_most_s  = &CSP_TMR_3;
   } else if (p_tmr_b == &CSP_TMR_4 ||  p_tmr_b == &CSP_TMR_5){
      *pp_tmr_least_s = &CSP_TMR_4;
      *pp_tmr_most_s  = &CSP_TMR_5;
   } else {
      //-- wrong pointer given
      *pp_tmr_least_s = NULL;
      *pp_tmr_most_s  = NULL;
   }

}




__CSP_FUNC void  csp_tmr_a__conf__set(T_CSP_TmrTypeA *p_tmr_a, UPWORD val)
{
   p_tmr_a->CON = val;
}

__CSP_FUNC UPWORD   csp_tmr_a__conf__get(T_CSP_TmrTypeA *p_tmr_a)
{
   return p_tmr_a->CON;
}

__CSP_FUNC void csp_tmr_a__en(T_CSP_TmrTypeA *p_tmr_a)
{
   p_tmr_a->_CON_SET = (1 << _CSP_OFS_TMR_A__ON);
}

__CSP_FUNC void csp_tmr_a__dis(T_CSP_TmrTypeA *p_tmr_a)
{
   p_tmr_a->_CON_CLR = (1 << _CSP_OFS_TMR_A__ON);
}

__CSP_FUNC void  csp_tmr_a__period__set(T_CSP_TmrTypeA *p_tmr_a, UPWORD period)
{
   p_tmr_a->PR = period;
}

__CSP_FUNC UPWORD   csp_tmr_a__period__get(T_CSP_TmrTypeA *p_tmr_a)
{
   return p_tmr_a->PR;
}

__CSP_FUNC void  csp_tmr_a__value__set(T_CSP_TmrTypeA *p_tmr_a, UPWORD val)
{
   p_tmr_a->TMR = val;
}

__CSP_FUNC UPWORD   csp_tmr_a__value__get(T_CSP_TmrTypeA *p_tmr_a)
{
   return p_tmr_a->TMR;
}

__CSP_FUNC enum E_CSP_TMR_A_Status csp_tmr_a__status__get(T_CSP_TmrTypeA *p_tmr_a)
{
   return (p_tmr_a->CON & CSP_TMR_A_STATUS_MASK);
}





__CSP_FUNC void  csp_tmr_b__conf__set(T_CSP_TmrTypeB *p_tmr_b, UPWORD val)
{
   p_tmr_b->CON = val;
}

__CSP_FUNC UPWORD   csp_tmr_b__conf__get(T_CSP_TmrTypeB *p_tmr_b)
{
   return p_tmr_b->CON;
}

__CSP_FUNC void csp_tmr_b__en(T_CSP_TmrTypeB *p_tmr_b)
{
   p_tmr_b->_CON_SET = (1 << _CSP_OFS_TMR_B__ON);
}

__CSP_FUNC void csp_tmr_b__dis(T_CSP_TmrTypeB *p_tmr_b)
{
   p_tmr_b->_CON_CLR = (1 << _CSP_OFS_TMR_B__ON);
}

__CSP_FUNC void  csp_tmr_b__period__set(T_CSP_TmrTypeB *p_tmr_b, UPWORD period)
{
   p_tmr_b->PR = period;
}

__CSP_FUNC UPWORD   csp_tmr_b__period__get(T_CSP_TmrTypeB *p_tmr_b)
{
   return p_tmr_b->PR;
}

__CSP_FUNC void  csp_tmr_b__value__set(T_CSP_TmrTypeB *p_tmr_b, UPWORD val)
{
   p_tmr_b->TMR = val;
}

__CSP_FUNC UPWORD   csp_tmr_b__value__get(T_CSP_TmrTypeB *p_tmr_b)
{
   return p_tmr_b->TMR;
}




/*
 * NOTE for csp_tmr_b__value_32bit__get() / ...set() :
 * For Timer2+Timer3 32-bit value, first argument can be either &CSP_TMR_2 or &CSP_TMR_3.
 * The same is for Timer4+Timer5 32-bit value.
 */

__CSP_FUNC U32   csp_tmr_b__value_32bit__get(T_CSP_TmrTypeB *p_tmr_b)
{
   U32 ret = 0;

   T_CSP_TmrTypeB *p_tmr_least_s = NULL;
   T_CSP_TmrTypeB *p_tmr_most_s = NULL;

   _32bit_timer_pointers_get(p_tmr_b, &p_tmr_least_s, &p_tmr_most_s);

   ret =    (p_tmr_most_s->TMR  & 0xffff);
   ret <<= 16;
   ret |=   (p_tmr_least_s->TMR & 0xffff);

   return ret;
}

__CSP_FUNC void  csp_tmr_b__value_32bit__set(T_CSP_TmrTypeB *p_tmr_b, U32 val)
{
   T_CSP_TmrTypeB *p_tmr_least_s = NULL;
   T_CSP_TmrTypeB *p_tmr_most_s = NULL;

   _32bit_timer_pointers_get(p_tmr_b, &p_tmr_least_s, &p_tmr_most_s);

   p_tmr_most_s->TMR  = ((val >> 16) & 0xffff);
   p_tmr_least_s->TMR = ((val >> 0) /*NOTE: don't & 0xffff here on purpose, see below*/);

   //-- NOTE: if CSP_TMR_B__32_BIT is already set,
   //   then p_tmr_least_s->TMR works already as 32-bit register.
   //   The code above works independently of CSP_TMR_B__32_BIT bit.
}

__CSP_FUNC U32 csp_tmr_b__period_32bit__get(T_CSP_TmrTypeB *p_tmr_b)
{
   U32 ret = 0;

   T_CSP_TmrTypeB *p_tmr_least_s = NULL;
   T_CSP_TmrTypeB *p_tmr_most_s = NULL;

   _32bit_timer_pointers_get(p_tmr_b, &p_tmr_least_s, &p_tmr_most_s);

   ret =    (p_tmr_most_s->PR  & 0xffff);
   ret <<= 16;
   ret |=   (p_tmr_least_s->PR & 0xffff);

   return ret;
}

__CSP_FUNC void  csp_tmr_b__period_32bit__set(T_CSP_TmrTypeB *p_tmr_b, U32 period)
{
   T_CSP_TmrTypeB *p_tmr_least_s = NULL;
   T_CSP_TmrTypeB *p_tmr_most_s = NULL;

   _32bit_timer_pointers_get(p_tmr_b, &p_tmr_least_s, &p_tmr_most_s);

   p_tmr_most_s->PR  = ((period >> 16) & 0xffff);
   p_tmr_least_s->PR = ((period >> 0) /*NOTE: don't & 0xffff here on purpose, see below*/);

   //-- NOTE: if CSP_TMR_B__32_BIT is already set,
   //   then p_tmr_least_s->PR works already as 32-bit register
   //   The code above works independently of CSP_TMR_B__32_BIT bit.
}




/* ********************************************************************************************** */
/* end of file: csp_oc_tmst.c */
