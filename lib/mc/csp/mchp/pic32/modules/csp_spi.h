/* *************************************************************************************************
Project:         MCHP 16-bit CPU Support Library
Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

 *************************************************************************************************
Distribution:

MCHP 16-bit CPU Support Library
----------------------------------------
Copyright � 2006-2008 Alex B.

 *************************************************************************************************
 MCU Family:      PIC24F/PIC24H/dsPIC33
Compiler:        Microchip C30 3.11

 *************************************************************************************************
File:            csp_spi.h
Description:

 ********************************************************************************************** */

#ifndef __CSP_SPI_H
#define __CSP_SPI_H

#include "../cspi.h"

#define  CSP_SPI__SPI_ADVANCED_MODULE                          \
   (0                                                          \
    || __CSP_FAMILY == __CSP_FAMILY_PIC32MX1XX_2XX             \
   )


#if !CSP_SPI__SPI_ADVANCED_MODULE


/* SPI programming model */
/* --------------------- */

typedef struct S_CSP_SPI_Reg
{
   volatile UPWORD CON;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD STAT;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD BUF;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD BRG;
} T_CSP_SPI_Reg;



/* SPI base addresses */
/* ------------------ */
#define CSP_SPI_BASE_VIRT_ADDR         0xbf800000U

#define CSP_SPI1_BASE_VIRT_ADDR        (CSP_SPI_BASE_VIRT_ADDR + 0x5800)
#define CSP_SPI2_BASE_VIRT_ADDR        (CSP_SPI_BASE_VIRT_ADDR + 0x5a00)

/* Declare SPI models */
/* ------------------ */
//TODO: SPI1 is not present only on PIC32MX420FXXXX/440FXXXX devices.
//extern T_CSP_SPI_Reg CSP_SPI1;
extern T_CSP_SPI_Reg CSP_SPI2;


#else
#error CPU is not yet supported by CSP SPI module
#endif

/* Internal API definitions */
/* ------------------------ */


#if   CSP_SPI__SPI_ADVANCED_MODULE
#define __CSP_SPI_STAT_MASK     (0      \
      | CSP_SPI_STAT_SREG_EMPTY         \
      | CSP_SPI_STAT_RX_BUF_EMPTY       \
      | CSP_SPI_STAT_TX_BUF_FULL        \
                                        \
      | CSP_SPI_STAT_BUSY               \
      | CSP_SPI_STAT_OVF                \
      | CSP_SPI_STAT_TX_BUF_EMPTY       \
      | CSP_SPI_STAT_RX_BUF_FULL        \
      )

#else

#define __CSP_SPI_STAT_MASK     (0      \
      | CSP_SPI_STAT_BUSY               \
      | CSP_SPI_STAT_OVF                \
      | CSP_SPI_STAT_TX_BUF_EMPTY       \
      | CSP_SPI_STAT_RX_BUF_FULL        \
      )
#endif



#define __CSP_SPI_BAUD_FS           (0)
#define __CSP_SPI_BAUD_FE           (8)

#define __CSP_SPI_BC_FS             (8)
#define __CSP_SPI_BC_FE             (10)

#define __CSP_SPI_INT_MODE_FS       (2)
#define __CSP_SPI_INT_MODE_FE       (4)

#define __CSP_SPI_EN_BIT            (15)

#define __CSP_SPI_STAT_RX_BUF_FULL_BIT  (0)
#define __CSP_SPI_STAT_TX_BUF_FULL_BIT  (1)
#define __CSP_SPI_STAT_TX_BUF_EMPTY_BIT (3)

/* Definitions for I2C API */
/* ----------------------- */

/* csp_spi_conf_set() and csp_spi_conf_get parameters mask */

#define CSP_SPI_FRAME_EN                              (1ULL << 31 << 32)    /* Framed SPI support is enabled */
#define CSP_SPI_FRAME_DIS                             (0ULL << 31 << 32)    /* Framed SPI support is disabled */
#define CSP_SPI_FRAME_INPUT                           (1ULL << 30 << 32)    /* Frame sync pulse input (slave) */
#define CSP_SPI_FRAME_OUTPUT                          (0ULL << 30 << 32)    /* Frame sync pulse output (master) */
#define CSP_SPI_FRAME_ACTIVE_1                        (1ULL << 29 << 32)    /* Frame sync pulse is active-high */
#define CSP_SPI_FRAME_ACTIVE_2                        (0ULL << 29 << 32)    /* Frame sync pulse is active-low */

#if   CSP_SPI__SPI_ADVANCED_MODULE
//-- this bit isn't used if CSP_SPI_FRAME_EN is set.
#  define CSP_SPI_PIN_MSS_EN                            (1ULL << 28 << 32)    /* Slave select SPI support enabled. The SS pin is automatically driven during transmission in Master mode. Polarity is determined by (CSP_SPI_FRAME_ACTIVE_1 / CSP_SPI_FRAME_ACTIVE_0) bit */
#  define CSP_SPI_PIN_MSS_DIS                           (0ULL << 28 << 32)    /* Slave select SPI support is disabled */

#  define CSP_SPI_FRAME_SYN_PULSE_WIDTH__WORD           (1ULL << 27 << 32)    /* Frame sync pulse is one word length wide, as defined by CSP_SPI_COMM_MODE__... */
#  define CSP_SPI_FRAME_SYN_PULSE_WIDTH__CLK            (0ULL << 27 << 32)    /* Frame sync pulse is one clock wide */

#  define CSP_SPI_FRAME_SYN_PULSE_CNT__32               (5ULL << 24 << 32)    /* Generate a frame sync pulse on every 32 data characters */
#  define CSP_SPI_FRAME_SYN_PULSE_CNT__16               (4ULL << 24 << 32)    /* Generate a frame sync pulse on every 16 data characters */
#  define CSP_SPI_FRAME_SYN_PULSE_CNT__8                (3ULL << 24 << 32)    /* Generate a frame sync pulse on every 8 data characters */
#  define CSP_SPI_FRAME_SYN_PULSE_CNT__4                (2ULL << 24 << 32)    /* Generate a frame sync pulse on every 4 data characters */
#  define CSP_SPI_FRAME_SYN_PULSE_CNT__2                (1ULL << 24 << 32)    /* Generate a frame sync pulse on every 2 data characters */
#  define CSP_SPI_FRAME_SYN_PULSE_CNT__1                (0ULL << 24 << 32)    /* Generate a frame sync pulse on every data character */
#endif

#define CSP_SPI_BRG_SRC__MCLK                         (1ULL << 23 << 32)    /* MCLK is used by the Baud Rate Generator */
#define CSP_SPI_BRG_SRC__PBCLK                        (0ULL << 23 << 32)    /* PBCLK is used by the Baud Rate Generator */

#define CSP_SPI_FRAME_FB_WITH                         (1ULL << 17 << 32)    /* Frame sync pulse coincides with first bit clock */
#define CSP_SPI_FRAME_FB_BEFORE                       (0ULL << 17 << 32)    /* Frame sync pulse precedes first bit clock */

#if   CSP_SPI__SPI_ADVANCED_MODULE
#  define CSP_SPI_ENCH_BUFF_EN                          (1ULL << 16 << 32)    /* Enhanced Buffer enabled */
#  define CSP_SPI_ENCH_BUFF_DIS                         (0ULL << 16 << 32)    /* Enhanced Buffer disabled (Legacy mode) */
#endif

/*
 * NOTE: When CSP_SPI_EN bit is set, CSP_SPI_PIN_SDO_EN/DIS and CSP_SPI_PIN_SDI_EN/DIS are the only other bits that can be modified.
 * When using the 1:1 PBCLK divisor, the user�s software should not read or write the peripheral�s SFRs
 * in the SYSCLK cycle immediately following the instruction that clears the module�s CSP_SPI_EN bit.
 */
#define CSP_SPI_EN                                    (1ULL << 15 << 32)    /* SPI Peripheral is enabled */
#define CSP_SPI_DIS                                   (0ULL << 15 << 32)    /* SPI Peripheral is disabled */

#define CSP_SPI_IDLE_STOP                             (1ULL << 13 << 32)    /* Discontinue operation when CPU enters in Idle mode */
#define CSP_SPI_IDLE_CON                              (0ULL << 13 << 32)    /* Continue operation in Idle mode */
#define CSP_SPI_PIN_SDO_EN                            (0ULL << 12 << 32)    /* SDO pin is controlled by the module */
#define CSP_SPI_PIN_SDO_DIS                           (1ULL << 12 << 32)    /* SDO pin is not used by module, pin functions as I/O */

//-- !!! Use the following if only AUDEN is 0:
#define CSP_SPI_8_BIT                                 (0ULL << 10 << 32)    /* 8-bit communication */
#define CSP_SPI_16_BIT                                (1ULL << 10 << 32)    /* 16-bit communication */
#define CSP_SPI_32_BIT                                (2ULL << 10 << 32)    /* 32-bit communication */
//-- !!! Use the following if only AUDEN is 1:
#define CSP_SPI_AUD__16_16_16_32                      (0ULL << 10 << 32)    /* 16-bit Data, 16-bit FIFO, 16-bit Channel/32-bit Frame */
#define CSP_SPI_AUD__16_16_32_64                      (1ULL << 10 << 32)    /* 16-bit Data, 16-bit FIFO, 32-bit Channel/64-bit Frame */
#define CSP_SPI_AUD__32_32_32_64                      (2ULL << 10 << 32)    /* 32-bit Data, 32-bit FIFO, 32-bit Channel/64-bit Frame */
#define CSP_SPI_AUD__24_32_32_64                      (3ULL << 10 << 32)    /* 24-bit Data, 32-bit FIFO, 32-bit Channel/64-bit Frame */
//---------------------

//-- only in master mode:
#define CSP_SPI_DATA_SAMPLE_END                       (1ULL << 9 << 32)    /* Input data sampled at end of data output time */
#define CSP_SPI_DATA_SAMPLE_MIDDLE                    (0ULL << 9 << 32)    /* Input data sampled at middle of data output time */

//-- this bit should be set to 0 (CSP_SPI_DATA_OUT_INACT_ACT) if Framed mode 
//   is used (CSP_SPI_FRAME_EN).
#define CSP_SPI_DATA_OUT_ACT_INACT                    (1ULL << 8 << 32)    /* Serial output data changes on transition from active clock state to Idle clock state */
#define CSP_SPI_DATA_OUT_INACT_ACT                    (0ULL << 8 << 32)    /* Serial output data changes on transition from Idle clock state to active clock state */

#define CSP_SPI_PIN_SS_EN                             (1ULL << 7 << 32)    /* SSx pin used for Slave mode */
#define CSP_SPI_PIN_SS_DIS                            (0ULL << 7 << 32)    /* SSx pin not used for Slave mode, pin controlled by port function */

#define CSP_SPI_CLK_IDLE_1                                (1ULL << 6 << 32)    /* Idle state for clock is a high level; active state is a low level */
#define CSP_SPI_CLK_IDLE_0                                (0ULL << 6 << 32)    /* Idle state for clock is a low level; active state is a high level */

#define CSP_SPI_MODE_MASTER                           (1ULL << 5 << 32)    /* Master mode */
#define CSP_SPI_MODE_SLAVE                            (0ULL << 5 << 32)    /* Slave mode */

#define CSP_SPI_PIN_SDI_EN                            (0ULL << 4 << 32)    /* SDIx pin is controlled by SPI module */
#define CSP_SPI_PIN_SDI_DIS                           (1ULL << 4 << 32)    /* SDIx pin is not used by the SPI module (pin is controlled by PORT function) */

#if   CSP_SPI__SPI_ADVANCED_MODULE
//-- note: CSP_SPI_INT_TX__... valid only if CSP_SPI_ENCH_BUFF_EN
#  define CSP_SPI_INT_TX__NOT_FULL                      (3ULL << 2 << 32)    /* Interrupt when the buffer is not full (has one or more empty elements) */
#  define CSP_SPI_INT_TX__HALF                          (2ULL << 2 << 32)    /* Interrupt when the buffer is empty by one-half or more */
#  define CSP_SPI_INT_TX__EMPTY                         (1ULL << 2 << 32)    /* Interrupt when the buffer is completely empty */
#  define CSP_SPI_INT_TX__TX_COMPLETE                   (0ULL << 2 << 32)    /* Interrupt when the last transfer is shifted out of SPISR and transmit operations are complete */

//-- note: CSP_SPI_INT_RX__... valid only if CSP_SPI_ENCH_BUFF_EN
#  define CSP_SPI_INT_RX__FULL                          (3ULL << 0 << 32)    /* Interrupt when the buffer is full */
#  define CSP_SPI_INT_RX__HALF                          (2ULL << 0 << 32)    /* Interrupt when the buffer is full by one-half or more */
#  define CSP_SPI_INT_RX__NOT_EMPTY                     (1ULL << 0 << 32)    /* Interrupt when the buffer is not empty */
#  define CSP_SPI_INT_RX__EMPTY                         (0ULL << 0 << 32)    /* Interrupt when the last word in the receive buffer is read (i.e. buffer is empty) */
#endif

#if   CSP_SPI__SPI_ADVANCED_MODULE
//TODO: SPIxCON2

#if 0
#define CSP_SPI_                                      (1ULL << 0)    /*  */
#endif

#endif





/* csp_spi_stat_get() and csp_spi_stat_clr() parameters type */

enum E_CSP_Stat {

#if   CSP_SPI__SPI_ADVANCED_MODULE
   CSP_SPI_STAT_SREG_EMPTY   = (1 << 7),   /* RO  - SPIx Shift register is empty and ready to send or receive (valid if only CSP_SPI_ENCH_BUFF_EN) */
   CSP_SPI_STAT_RX_BUF_EMPTY = (1 << 5),   /* RO  - RX FIFO is empty  (valid if only CSP_SPI_ENCH_BUFF_EN)*/
   CSP_SPI_STAT_TX_BUF_FULL  = (1 << 1),   /* RO  - Transmit not yet started, SPIxTXB is full  */
#endif
   CSP_SPI_STAT_BUSY         = (1 << 11),  /* RO  - SPI peripheral is currently busy with some transactions */
   CSP_SPI_STAT_OVF          = (1 << 6),   /* R/C - New byte/word is completely received; CPU has not read previous data in the SPIBUF register  */
   CSP_SPI_STAT_TX_BUF_EMPTY = (1 << 3),    /* RO  - Transmit complete,SPIxTXB (which is SPIxBUF in standard mode) is empty  */
   CSP_SPI_STAT_RX_BUF_FULL  = (1 << 0),    /* RO  - Receive complete, SPIxRXB (which is SPIxBUF in standard mode) is full  */
};

#if   CSP_SPI__SPI_ADVANCED_MODULE
//TODO
#if 0
/* csp_spi_int_mode_set() and csp_spi_int_mode_get() parameters type */
typedef enum __SPI_INT_MODE
{
   CSP_SPI_INT_MODE_RX_EMPTY       = (0),    /* Interrupt when the last data in the receive buffer is read, as a result, the buffer is empty */
   CSP_SPI_INT_MODE_RX_AVAILABE    = (1),    /* Interrupt when data is available in receive buffer */
   CSP_SPI_INT_MODE_RX_ALMOST_FULL = (2),    /* Interrupt when SPIx receive buffer is 3/4 or more full */
   CSP_SPI_INT_MODE_RX_FULL        = (3),    /* Interrupt when SPIx receive buffer is full */
   CSP_SPI_INT_MODE_TX_BUF_FREE    = (4),    /* Interrupt when one data is shifted into the SPIxSR, as a result, the TX FIFO has one open spot */
   CSP_SPI_INT_MODE_TX_DONE        = (5),    /* Interrupt when the last bit is shifted out of SPIxSR, now the transmit is complete */
   CSP_SPI_INT_MODE_TX_EMPTY       = (6),    /* Interrupt when last bit is shifted into SPIxSR, as a result, the TX FIFO is empty */
   CSP_SPI_INT_MODE_TX_FULL        = (7)     /* Interrupt when SPIx transmit buffer is full */
} SPI_INT_MODE;
#endif
#endif


/* csp_spi_baud_set() and csp_spi_baud_get() parameters type */

//-- enum E_CSP_SPI_BaudRatePrescaler {{{
enum E_CSP_SPI_BaudRatePrescaler {
   CSP_SPI_BAUD_1_2            = 0,
   CSP_SPI_BAUD_1_4            = 1,
   CSP_SPI_BAUD_1_6            = 2,
   CSP_SPI_BAUD_1_8            = 3,
   CSP_SPI_BAUD_1_10           = 4,
   CSP_SPI_BAUD_1_12           = 5,
   CSP_SPI_BAUD_1_14           = 6,
   CSP_SPI_BAUD_1_16           = 7,
   CSP_SPI_BAUD_1_18           = 8,
   CSP_SPI_BAUD_1_20           = 9,
   CSP_SPI_BAUD_1_22           = 10,
   CSP_SPI_BAUD_1_24           = 11,
   CSP_SPI_BAUD_1_26           = 12,
   CSP_SPI_BAUD_1_28           = 13,
   CSP_SPI_BAUD_1_30           = 14,
   CSP_SPI_BAUD_1_32           = 15,
   CSP_SPI_BAUD_1_34           = 16,
   CSP_SPI_BAUD_1_36           = 17,
   CSP_SPI_BAUD_1_38           = 18,
   CSP_SPI_BAUD_1_40           = 19,
   CSP_SPI_BAUD_1_42           = 20,
   CSP_SPI_BAUD_1_44           = 21,
   CSP_SPI_BAUD_1_46           = 22,
   CSP_SPI_BAUD_1_48           = 23,
   CSP_SPI_BAUD_1_50           = 24,
   CSP_SPI_BAUD_1_52           = 25,
   CSP_SPI_BAUD_1_54           = 26,
   CSP_SPI_BAUD_1_56           = 27,
   CSP_SPI_BAUD_1_58           = 28,
   CSP_SPI_BAUD_1_60           = 29,
   CSP_SPI_BAUD_1_62           = 30,
   CSP_SPI_BAUD_1_64           = 31,
   CSP_SPI_BAUD_1_66           = 32,
   CSP_SPI_BAUD_1_68           = 33,
   CSP_SPI_BAUD_1_70           = 34,
   CSP_SPI_BAUD_1_72           = 35,
   CSP_SPI_BAUD_1_74           = 36,
   CSP_SPI_BAUD_1_76           = 37,
   CSP_SPI_BAUD_1_78           = 38,
   CSP_SPI_BAUD_1_80           = 39,
   CSP_SPI_BAUD_1_82           = 40,
   CSP_SPI_BAUD_1_84           = 41,
   CSP_SPI_BAUD_1_86           = 42,
   CSP_SPI_BAUD_1_88           = 43,
   CSP_SPI_BAUD_1_90           = 44,
   CSP_SPI_BAUD_1_92           = 45,
   CSP_SPI_BAUD_1_94           = 46,
   CSP_SPI_BAUD_1_96           = 47,
   CSP_SPI_BAUD_1_98           = 48,
   CSP_SPI_BAUD_1_100          = 49,
   CSP_SPI_BAUD_1_102          = 50,
   CSP_SPI_BAUD_1_104          = 51,
   CSP_SPI_BAUD_1_106          = 52,
   CSP_SPI_BAUD_1_108          = 53,
   CSP_SPI_BAUD_1_110          = 54,
   CSP_SPI_BAUD_1_112          = 55,
   CSP_SPI_BAUD_1_114          = 56,
   CSP_SPI_BAUD_1_116          = 57,
   CSP_SPI_BAUD_1_118          = 58,
   CSP_SPI_BAUD_1_120          = 59,
   CSP_SPI_BAUD_1_122          = 60,
   CSP_SPI_BAUD_1_124          = 61,
   CSP_SPI_BAUD_1_126          = 62,
   CSP_SPI_BAUD_1_128          = 63,
   CSP_SPI_BAUD_1_130          = 64,
   CSP_SPI_BAUD_1_132          = 65,
   CSP_SPI_BAUD_1_134          = 66,
   CSP_SPI_BAUD_1_136          = 67,
   CSP_SPI_BAUD_1_138          = 68,
   CSP_SPI_BAUD_1_140          = 69,
   CSP_SPI_BAUD_1_142          = 70,
   CSP_SPI_BAUD_1_144          = 71,
   CSP_SPI_BAUD_1_146          = 72,
   CSP_SPI_BAUD_1_148          = 73,
   CSP_SPI_BAUD_1_150          = 74,
   CSP_SPI_BAUD_1_152          = 75,
   CSP_SPI_BAUD_1_154          = 76,
   CSP_SPI_BAUD_1_156          = 77,
   CSP_SPI_BAUD_1_158          = 78,
   CSP_SPI_BAUD_1_160          = 79,
   CSP_SPI_BAUD_1_162          = 80,
   CSP_SPI_BAUD_1_164          = 81,
   CSP_SPI_BAUD_1_166          = 82,
   CSP_SPI_BAUD_1_168          = 83,
   CSP_SPI_BAUD_1_170          = 84,
   CSP_SPI_BAUD_1_172          = 85,
   CSP_SPI_BAUD_1_174          = 86,
   CSP_SPI_BAUD_1_176          = 87,
   CSP_SPI_BAUD_1_178          = 88,
   CSP_SPI_BAUD_1_180          = 89,
   CSP_SPI_BAUD_1_182          = 90,
   CSP_SPI_BAUD_1_184          = 91,
   CSP_SPI_BAUD_1_186          = 92,
   CSP_SPI_BAUD_1_188          = 93,
   CSP_SPI_BAUD_1_190          = 94,
   CSP_SPI_BAUD_1_192          = 95,
   CSP_SPI_BAUD_1_194          = 96,
   CSP_SPI_BAUD_1_196          = 97,
   CSP_SPI_BAUD_1_198          = 98,
   CSP_SPI_BAUD_1_200          = 99,
   CSP_SPI_BAUD_1_202          = 100,
   CSP_SPI_BAUD_1_204          = 101,
   CSP_SPI_BAUD_1_206          = 102,
   CSP_SPI_BAUD_1_208          = 103,
   CSP_SPI_BAUD_1_210          = 104,
   CSP_SPI_BAUD_1_212          = 105,
   CSP_SPI_BAUD_1_214          = 106,
   CSP_SPI_BAUD_1_216          = 107,
   CSP_SPI_BAUD_1_218          = 108,
   CSP_SPI_BAUD_1_220          = 109,
   CSP_SPI_BAUD_1_222          = 110,
   CSP_SPI_BAUD_1_224          = 111,
   CSP_SPI_BAUD_1_226          = 112,
   CSP_SPI_BAUD_1_228          = 113,
   CSP_SPI_BAUD_1_230          = 114,
   CSP_SPI_BAUD_1_232          = 115,
   CSP_SPI_BAUD_1_234          = 116,
   CSP_SPI_BAUD_1_236          = 117,
   CSP_SPI_BAUD_1_238          = 118,
   CSP_SPI_BAUD_1_240          = 119,
   CSP_SPI_BAUD_1_242          = 120,
   CSP_SPI_BAUD_1_244          = 121,
   CSP_SPI_BAUD_1_246          = 122,
   CSP_SPI_BAUD_1_248          = 123,
   CSP_SPI_BAUD_1_250          = 124,
   CSP_SPI_BAUD_1_252          = 125,
   CSP_SPI_BAUD_1_254          = 126,
   CSP_SPI_BAUD_1_256          = 127,
   CSP_SPI_BAUD_1_258          = 128,
   CSP_SPI_BAUD_1_260          = 129,
   CSP_SPI_BAUD_1_262          = 130,
   CSP_SPI_BAUD_1_264          = 131,
   CSP_SPI_BAUD_1_266          = 132,
   CSP_SPI_BAUD_1_268          = 133,
   CSP_SPI_BAUD_1_270          = 134,
   CSP_SPI_BAUD_1_272          = 135,
   CSP_SPI_BAUD_1_274          = 136,
   CSP_SPI_BAUD_1_276          = 137,
   CSP_SPI_BAUD_1_278          = 138,
   CSP_SPI_BAUD_1_280          = 139,
   CSP_SPI_BAUD_1_282          = 140,
   CSP_SPI_BAUD_1_284          = 141,
   CSP_SPI_BAUD_1_286          = 142,
   CSP_SPI_BAUD_1_288          = 143,
   CSP_SPI_BAUD_1_290          = 144,
   CSP_SPI_BAUD_1_292          = 145,
   CSP_SPI_BAUD_1_294          = 146,
   CSP_SPI_BAUD_1_296          = 147,
   CSP_SPI_BAUD_1_298          = 148,
   CSP_SPI_BAUD_1_300          = 149,
   CSP_SPI_BAUD_1_302          = 150,
   CSP_SPI_BAUD_1_304          = 151,
   CSP_SPI_BAUD_1_306          = 152,
   CSP_SPI_BAUD_1_308          = 153,
   CSP_SPI_BAUD_1_310          = 154,
   CSP_SPI_BAUD_1_312          = 155,
   CSP_SPI_BAUD_1_314          = 156,
   CSP_SPI_BAUD_1_316          = 157,
   CSP_SPI_BAUD_1_318          = 158,
   CSP_SPI_BAUD_1_320          = 159,
   CSP_SPI_BAUD_1_322          = 160,
   CSP_SPI_BAUD_1_324          = 161,
   CSP_SPI_BAUD_1_326          = 162,
   CSP_SPI_BAUD_1_328          = 163,
   CSP_SPI_BAUD_1_330          = 164,
   CSP_SPI_BAUD_1_332          = 165,
   CSP_SPI_BAUD_1_334          = 166,
   CSP_SPI_BAUD_1_336          = 167,
   CSP_SPI_BAUD_1_338          = 168,
   CSP_SPI_BAUD_1_340          = 169,
   CSP_SPI_BAUD_1_342          = 170,
   CSP_SPI_BAUD_1_344          = 171,
   CSP_SPI_BAUD_1_346          = 172,
   CSP_SPI_BAUD_1_348          = 173,
   CSP_SPI_BAUD_1_350          = 174,
   CSP_SPI_BAUD_1_352          = 175,
   CSP_SPI_BAUD_1_354          = 176,
   CSP_SPI_BAUD_1_356          = 177,
   CSP_SPI_BAUD_1_358          = 178,
   CSP_SPI_BAUD_1_360          = 179,
   CSP_SPI_BAUD_1_362          = 180,
   CSP_SPI_BAUD_1_364          = 181,
   CSP_SPI_BAUD_1_366          = 182,
   CSP_SPI_BAUD_1_368          = 183,
   CSP_SPI_BAUD_1_370          = 184,
   CSP_SPI_BAUD_1_372          = 185,
   CSP_SPI_BAUD_1_374          = 186,
   CSP_SPI_BAUD_1_376          = 187,
   CSP_SPI_BAUD_1_378          = 188,
   CSP_SPI_BAUD_1_380          = 189,
   CSP_SPI_BAUD_1_382          = 190,
   CSP_SPI_BAUD_1_384          = 191,
   CSP_SPI_BAUD_1_386          = 192,
   CSP_SPI_BAUD_1_388          = 193,
   CSP_SPI_BAUD_1_390          = 194,
   CSP_SPI_BAUD_1_392          = 195,
   CSP_SPI_BAUD_1_394          = 196,
   CSP_SPI_BAUD_1_396          = 197,
   CSP_SPI_BAUD_1_398          = 198,
   CSP_SPI_BAUD_1_400          = 199,
   CSP_SPI_BAUD_1_402          = 200,
   CSP_SPI_BAUD_1_404          = 201,
   CSP_SPI_BAUD_1_406          = 202,
   CSP_SPI_BAUD_1_408          = 203,
   CSP_SPI_BAUD_1_410          = 204,
   CSP_SPI_BAUD_1_412          = 205,
   CSP_SPI_BAUD_1_414          = 206,
   CSP_SPI_BAUD_1_416          = 207,
   CSP_SPI_BAUD_1_418          = 208,
   CSP_SPI_BAUD_1_420          = 209,
   CSP_SPI_BAUD_1_422          = 210,
   CSP_SPI_BAUD_1_424          = 211,
   CSP_SPI_BAUD_1_426          = 212,
   CSP_SPI_BAUD_1_428          = 213,
   CSP_SPI_BAUD_1_430          = 214,
   CSP_SPI_BAUD_1_432          = 215,
   CSP_SPI_BAUD_1_434          = 216,
   CSP_SPI_BAUD_1_436          = 217,
   CSP_SPI_BAUD_1_438          = 218,
   CSP_SPI_BAUD_1_440          = 219,
   CSP_SPI_BAUD_1_442          = 220,
   CSP_SPI_BAUD_1_444          = 221,
   CSP_SPI_BAUD_1_446          = 222,
   CSP_SPI_BAUD_1_448          = 223,
   CSP_SPI_BAUD_1_450          = 224,
   CSP_SPI_BAUD_1_452          = 225,
   CSP_SPI_BAUD_1_454          = 226,
   CSP_SPI_BAUD_1_456          = 227,
   CSP_SPI_BAUD_1_458          = 228,
   CSP_SPI_BAUD_1_460          = 229,
   CSP_SPI_BAUD_1_462          = 230,
   CSP_SPI_BAUD_1_464          = 231,
   CSP_SPI_BAUD_1_466          = 232,
   CSP_SPI_BAUD_1_468          = 233,
   CSP_SPI_BAUD_1_470          = 234,
   CSP_SPI_BAUD_1_472          = 235,
   CSP_SPI_BAUD_1_474          = 236,
   CSP_SPI_BAUD_1_476          = 237,
   CSP_SPI_BAUD_1_478          = 238,
   CSP_SPI_BAUD_1_480          = 239,
   CSP_SPI_BAUD_1_482          = 240,
   CSP_SPI_BAUD_1_484          = 241,
   CSP_SPI_BAUD_1_486          = 242,
   CSP_SPI_BAUD_1_488          = 243,
   CSP_SPI_BAUD_1_490          = 244,
   CSP_SPI_BAUD_1_492          = 245,
   CSP_SPI_BAUD_1_494          = 246,
   CSP_SPI_BAUD_1_496          = 247,
   CSP_SPI_BAUD_1_498          = 248,
   CSP_SPI_BAUD_1_500          = 249,
   CSP_SPI_BAUD_1_502          = 250,
   CSP_SPI_BAUD_1_504          = 251,
   CSP_SPI_BAUD_1_506          = 252,
   CSP_SPI_BAUD_1_508          = 253,
   CSP_SPI_BAUD_1_510          = 254,
   CSP_SPI_BAUD_1_512          = 255,
   CSP_SPI_BAUD_1_514          = 256,
   CSP_SPI_BAUD_1_516          = 257,
   CSP_SPI_BAUD_1_518          = 258,
   CSP_SPI_BAUD_1_520          = 259,
   CSP_SPI_BAUD_1_522          = 260,
   CSP_SPI_BAUD_1_524          = 261,
   CSP_SPI_BAUD_1_526          = 262,
   CSP_SPI_BAUD_1_528          = 263,
   CSP_SPI_BAUD_1_530          = 264,
   CSP_SPI_BAUD_1_532          = 265,
   CSP_SPI_BAUD_1_534          = 266,
   CSP_SPI_BAUD_1_536          = 267,
   CSP_SPI_BAUD_1_538          = 268,
   CSP_SPI_BAUD_1_540          = 269,
   CSP_SPI_BAUD_1_542          = 270,
   CSP_SPI_BAUD_1_544          = 271,
   CSP_SPI_BAUD_1_546          = 272,
   CSP_SPI_BAUD_1_548          = 273,
   CSP_SPI_BAUD_1_550          = 274,
   CSP_SPI_BAUD_1_552          = 275,
   CSP_SPI_BAUD_1_554          = 276,
   CSP_SPI_BAUD_1_556          = 277,
   CSP_SPI_BAUD_1_558          = 278,
   CSP_SPI_BAUD_1_560          = 279,
   CSP_SPI_BAUD_1_562          = 280,
   CSP_SPI_BAUD_1_564          = 281,
   CSP_SPI_BAUD_1_566          = 282,
   CSP_SPI_BAUD_1_568          = 283,
   CSP_SPI_BAUD_1_570          = 284,
   CSP_SPI_BAUD_1_572          = 285,
   CSP_SPI_BAUD_1_574          = 286,
   CSP_SPI_BAUD_1_576          = 287,
   CSP_SPI_BAUD_1_578          = 288,
   CSP_SPI_BAUD_1_580          = 289,
   CSP_SPI_BAUD_1_582          = 290,
   CSP_SPI_BAUD_1_584          = 291,
   CSP_SPI_BAUD_1_586          = 292,
   CSP_SPI_BAUD_1_588          = 293,
   CSP_SPI_BAUD_1_590          = 294,
   CSP_SPI_BAUD_1_592          = 295,
   CSP_SPI_BAUD_1_594          = 296,
   CSP_SPI_BAUD_1_596          = 297,
   CSP_SPI_BAUD_1_598          = 298,
   CSP_SPI_BAUD_1_600          = 299,
   CSP_SPI_BAUD_1_602          = 300,
   CSP_SPI_BAUD_1_604          = 301,
   CSP_SPI_BAUD_1_606          = 302,
   CSP_SPI_BAUD_1_608          = 303,
   CSP_SPI_BAUD_1_610          = 304,
   CSP_SPI_BAUD_1_612          = 305,
   CSP_SPI_BAUD_1_614          = 306,
   CSP_SPI_BAUD_1_616          = 307,
   CSP_SPI_BAUD_1_618          = 308,
   CSP_SPI_BAUD_1_620          = 309,
   CSP_SPI_BAUD_1_622          = 310,
   CSP_SPI_BAUD_1_624          = 311,
   CSP_SPI_BAUD_1_626          = 312,
   CSP_SPI_BAUD_1_628          = 313,
   CSP_SPI_BAUD_1_630          = 314,
   CSP_SPI_BAUD_1_632          = 315,
   CSP_SPI_BAUD_1_634          = 316,
   CSP_SPI_BAUD_1_636          = 317,
   CSP_SPI_BAUD_1_638          = 318,
   CSP_SPI_BAUD_1_640          = 319,
   CSP_SPI_BAUD_1_642          = 320,
   CSP_SPI_BAUD_1_644          = 321,
   CSP_SPI_BAUD_1_646          = 322,
   CSP_SPI_BAUD_1_648          = 323,
   CSP_SPI_BAUD_1_650          = 324,
   CSP_SPI_BAUD_1_652          = 325,
   CSP_SPI_BAUD_1_654          = 326,
   CSP_SPI_BAUD_1_656          = 327,
   CSP_SPI_BAUD_1_658          = 328,
   CSP_SPI_BAUD_1_660          = 329,
   CSP_SPI_BAUD_1_662          = 330,
   CSP_SPI_BAUD_1_664          = 331,
   CSP_SPI_BAUD_1_666          = 332,
   CSP_SPI_BAUD_1_668          = 333,
   CSP_SPI_BAUD_1_670          = 334,
   CSP_SPI_BAUD_1_672          = 335,
   CSP_SPI_BAUD_1_674          = 336,
   CSP_SPI_BAUD_1_676          = 337,
   CSP_SPI_BAUD_1_678          = 338,
   CSP_SPI_BAUD_1_680          = 339,
   CSP_SPI_BAUD_1_682          = 340,
   CSP_SPI_BAUD_1_684          = 341,
   CSP_SPI_BAUD_1_686          = 342,
   CSP_SPI_BAUD_1_688          = 343,
   CSP_SPI_BAUD_1_690          = 344,
   CSP_SPI_BAUD_1_692          = 345,
   CSP_SPI_BAUD_1_694          = 346,
   CSP_SPI_BAUD_1_696          = 347,
   CSP_SPI_BAUD_1_698          = 348,
   CSP_SPI_BAUD_1_700          = 349,
   CSP_SPI_BAUD_1_702          = 350,
   CSP_SPI_BAUD_1_704          = 351,
   CSP_SPI_BAUD_1_706          = 352,
   CSP_SPI_BAUD_1_708          = 353,
   CSP_SPI_BAUD_1_710          = 354,
   CSP_SPI_BAUD_1_712          = 355,
   CSP_SPI_BAUD_1_714          = 356,
   CSP_SPI_BAUD_1_716          = 357,
   CSP_SPI_BAUD_1_718          = 358,
   CSP_SPI_BAUD_1_720          = 359,
   CSP_SPI_BAUD_1_722          = 360,
   CSP_SPI_BAUD_1_724          = 361,
   CSP_SPI_BAUD_1_726          = 362,
   CSP_SPI_BAUD_1_728          = 363,
   CSP_SPI_BAUD_1_730          = 364,
   CSP_SPI_BAUD_1_732          = 365,
   CSP_SPI_BAUD_1_734          = 366,
   CSP_SPI_BAUD_1_736          = 367,
   CSP_SPI_BAUD_1_738          = 368,
   CSP_SPI_BAUD_1_740          = 369,
   CSP_SPI_BAUD_1_742          = 370,
   CSP_SPI_BAUD_1_744          = 371,
   CSP_SPI_BAUD_1_746          = 372,
   CSP_SPI_BAUD_1_748          = 373,
   CSP_SPI_BAUD_1_750          = 374,
   CSP_SPI_BAUD_1_752          = 375,
   CSP_SPI_BAUD_1_754          = 376,
   CSP_SPI_BAUD_1_756          = 377,
   CSP_SPI_BAUD_1_758          = 378,
   CSP_SPI_BAUD_1_760          = 379,
   CSP_SPI_BAUD_1_762          = 380,
   CSP_SPI_BAUD_1_764          = 381,
   CSP_SPI_BAUD_1_766          = 382,
   CSP_SPI_BAUD_1_768          = 383,
   CSP_SPI_BAUD_1_770          = 384,
   CSP_SPI_BAUD_1_772          = 385,
   CSP_SPI_BAUD_1_774          = 386,
   CSP_SPI_BAUD_1_776          = 387,
   CSP_SPI_BAUD_1_778          = 388,
   CSP_SPI_BAUD_1_780          = 389,
   CSP_SPI_BAUD_1_782          = 390,
   CSP_SPI_BAUD_1_784          = 391,
   CSP_SPI_BAUD_1_786          = 392,
   CSP_SPI_BAUD_1_788          = 393,
   CSP_SPI_BAUD_1_790          = 394,
   CSP_SPI_BAUD_1_792          = 395,
   CSP_SPI_BAUD_1_794          = 396,
   CSP_SPI_BAUD_1_796          = 397,
   CSP_SPI_BAUD_1_798          = 398,
   CSP_SPI_BAUD_1_800          = 399,
   CSP_SPI_BAUD_1_802          = 400,
   CSP_SPI_BAUD_1_804          = 401,
   CSP_SPI_BAUD_1_806          = 402,
   CSP_SPI_BAUD_1_808          = 403,
   CSP_SPI_BAUD_1_810          = 404,
   CSP_SPI_BAUD_1_812          = 405,
   CSP_SPI_BAUD_1_814          = 406,
   CSP_SPI_BAUD_1_816          = 407,
   CSP_SPI_BAUD_1_818          = 408,
   CSP_SPI_BAUD_1_820          = 409,
   CSP_SPI_BAUD_1_822          = 410,
   CSP_SPI_BAUD_1_824          = 411,
   CSP_SPI_BAUD_1_826          = 412,
   CSP_SPI_BAUD_1_828          = 413,
   CSP_SPI_BAUD_1_830          = 414,
   CSP_SPI_BAUD_1_832          = 415,
   CSP_SPI_BAUD_1_834          = 416,
   CSP_SPI_BAUD_1_836          = 417,
   CSP_SPI_BAUD_1_838          = 418,
   CSP_SPI_BAUD_1_840          = 419,
   CSP_SPI_BAUD_1_842          = 420,
   CSP_SPI_BAUD_1_844          = 421,
   CSP_SPI_BAUD_1_846          = 422,
   CSP_SPI_BAUD_1_848          = 423,
   CSP_SPI_BAUD_1_850          = 424,
   CSP_SPI_BAUD_1_852          = 425,
   CSP_SPI_BAUD_1_854          = 426,
   CSP_SPI_BAUD_1_856          = 427,
   CSP_SPI_BAUD_1_858          = 428,
   CSP_SPI_BAUD_1_860          = 429,
   CSP_SPI_BAUD_1_862          = 430,
   CSP_SPI_BAUD_1_864          = 431,
   CSP_SPI_BAUD_1_866          = 432,
   CSP_SPI_BAUD_1_868          = 433,
   CSP_SPI_BAUD_1_870          = 434,
   CSP_SPI_BAUD_1_872          = 435,
   CSP_SPI_BAUD_1_874          = 436,
   CSP_SPI_BAUD_1_876          = 437,
   CSP_SPI_BAUD_1_878          = 438,
   CSP_SPI_BAUD_1_880          = 439,
   CSP_SPI_BAUD_1_882          = 440,
   CSP_SPI_BAUD_1_884          = 441,
   CSP_SPI_BAUD_1_886          = 442,
   CSP_SPI_BAUD_1_888          = 443,
   CSP_SPI_BAUD_1_890          = 444,
   CSP_SPI_BAUD_1_892          = 445,
   CSP_SPI_BAUD_1_894          = 446,
   CSP_SPI_BAUD_1_896          = 447,
   CSP_SPI_BAUD_1_898          = 448,
   CSP_SPI_BAUD_1_900          = 449,
   CSP_SPI_BAUD_1_902          = 450,
   CSP_SPI_BAUD_1_904          = 451,
   CSP_SPI_BAUD_1_906          = 452,
   CSP_SPI_BAUD_1_908          = 453,
   CSP_SPI_BAUD_1_910          = 454,
   CSP_SPI_BAUD_1_912          = 455,
   CSP_SPI_BAUD_1_914          = 456,
   CSP_SPI_BAUD_1_916          = 457,
   CSP_SPI_BAUD_1_918          = 458,
   CSP_SPI_BAUD_1_920          = 459,
   CSP_SPI_BAUD_1_922          = 460,
   CSP_SPI_BAUD_1_924          = 461,
   CSP_SPI_BAUD_1_926          = 462,
   CSP_SPI_BAUD_1_928          = 463,
   CSP_SPI_BAUD_1_930          = 464,
   CSP_SPI_BAUD_1_932          = 465,
   CSP_SPI_BAUD_1_934          = 466,
   CSP_SPI_BAUD_1_936          = 467,
   CSP_SPI_BAUD_1_938          = 468,
   CSP_SPI_BAUD_1_940          = 469,
   CSP_SPI_BAUD_1_942          = 470,
   CSP_SPI_BAUD_1_944          = 471,
   CSP_SPI_BAUD_1_946          = 472,
   CSP_SPI_BAUD_1_948          = 473,
   CSP_SPI_BAUD_1_950          = 474,
   CSP_SPI_BAUD_1_952          = 475,
   CSP_SPI_BAUD_1_954          = 476,
   CSP_SPI_BAUD_1_956          = 477,
   CSP_SPI_BAUD_1_958          = 478,
   CSP_SPI_BAUD_1_960          = 479,
   CSP_SPI_BAUD_1_962          = 480,
   CSP_SPI_BAUD_1_964          = 481,
   CSP_SPI_BAUD_1_966          = 482,
   CSP_SPI_BAUD_1_968          = 483,
   CSP_SPI_BAUD_1_970          = 484,
   CSP_SPI_BAUD_1_972          = 485,
   CSP_SPI_BAUD_1_974          = 486,
   CSP_SPI_BAUD_1_976          = 487,
   CSP_SPI_BAUD_1_978          = 488,
   CSP_SPI_BAUD_1_980          = 489,
   CSP_SPI_BAUD_1_982          = 490,
   CSP_SPI_BAUD_1_984          = 491,
   CSP_SPI_BAUD_1_986          = 492,
   CSP_SPI_BAUD_1_988          = 493,
   CSP_SPI_BAUD_1_990          = 494,
   CSP_SPI_BAUD_1_992          = 495,
   CSP_SPI_BAUD_1_994          = 496,
   CSP_SPI_BAUD_1_996          = 497,
   CSP_SPI_BAUD_1_998          = 498,
   CSP_SPI_BAUD_1_1000         = 499,
   CSP_SPI_BAUD_1_1002         = 500,
   CSP_SPI_BAUD_1_1004         = 501,
   CSP_SPI_BAUD_1_1006         = 502,
   CSP_SPI_BAUD_1_1008         = 503,
   CSP_SPI_BAUD_1_1010         = 504,
   CSP_SPI_BAUD_1_1012         = 505,
   CSP_SPI_BAUD_1_1014         = 506,
   CSP_SPI_BAUD_1_1016         = 507,
   CSP_SPI_BAUD_1_1018         = 508,
   CSP_SPI_BAUD_1_1020         = 509,
   CSP_SPI_BAUD_1_1022         = 510,
   CSP_SPI_BAUD_1_1024         = 511,
};
// }}}


/* External functions prototypes */
/* ----------------------------- */

void        csp_spi_en(T_CSP_SPI_Reg *spi);
void        csp_spi_dis(T_CSP_SPI_Reg *spi);

void        csp_spi_conf_set(T_CSP_SPI_Reg *spi, U64 val);
U64         csp_spi_conf_get(T_CSP_SPI_Reg *spi);

//TODO
#if   CSP_SPI__SPI_ADVANCED_MODULE
#if 0
void            csp_spi_int_mode_set(T_CSP_SPI_Reg *spi, SPI_INT_MODE val);
SPI_INT_MODE    csp_spi_int_mode_get(T_CSP_SPI_Reg *spi);
#endif
#endif

void        csp_spi_baud_set(T_CSP_SPI_Reg *spi, enum E_CSP_SPI_BaudRatePrescaler val);
enum E_CSP_SPI_BaudRatePrescaler csp_spi_baud_get(T_CSP_SPI_Reg *spi);

enum E_CSP_Stat    csp_spi_stat_get(T_CSP_SPI_Reg *spi);
void        csp_spi_stat_clr(T_CSP_SPI_Reg *spi, enum E_CSP_Stat mask);

void        csp_spi_put(T_CSP_SPI_Reg *spi, UPWORD val);
void        csp_spi_put_bl(T_CSP_SPI_Reg *spi, UPWORD val);

UPWORD       csp_spi_get(T_CSP_SPI_Reg *spi);

UPWORD       csp_spi_exch(T_CSP_SPI_Reg *spi, UPWORD val);

//TODO
#if   CSP_SPI__SPI_ADVANCED_MODULE
#if 0
UPWORD       csp_spi_bufcnt_get(T_CSP_SPI_Reg *spi);
#endif
#endif


#if !defined(__CSP_BUILD_LIB)

/* Module API definition */

#if defined(__CSP_DEBUG_SPI)

//-- prototypes were here

#else

#include "csp_spi.c"

#endif

#endif

#endif /* #ifndef __CSP_SPI_H */
/* ********************************************************************************************** */
/* end of file: csp_spi.h */

