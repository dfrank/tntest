/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_i2c_aget.c
     Description:

   ********************************************************************************************** */

#include "csp_i2c.h"

//#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"

//#endif

#if defined(__CSP_BUILD_LIB)

#define _I2C1_BASE_VIRT_ADDR        (0xBF800000 + 0x5000)
#define _I2C2_BASE_VIRT_ADDR        (0xBF800000 + 0x5200)

T_CSP_I2C CSP_I2C1  __CSP_SFR((_I2C1_BASE_VIRT_ADDR));
T_CSP_I2C CSP_I2C2  __CSP_SFR((_I2C2_BASE_VIRT_ADDR));
#endif

__CSP_FUNC UPWORD  csp_i2c_addr_get (T_CSP_I2C *i2c)
{
    return (i2c->I2CADD);
}


__CSP_FUNC UPWORD  csp_i2c_addr_mask_get (T_CSP_I2C *i2c)
{
    return (i2c->I2CMSK);
}


__CSP_FUNC void  csp_i2c_addr_mask_set (T_CSP_I2C *i2c, UPWORD val)
{
    i2c->I2CMSK = val;
}


__CSP_FUNC void  csp_i2c_addr_set (T_CSP_I2C *i2c, UPWORD val)
{
    i2c->I2CADD = val;
}


__CSP_FUNC UPWORD  csp_i2c_baud_get (T_CSP_I2C *i2c)
{
    return (i2c->I2CBRG);
}


__CSP_FUNC void  csp_i2c_baud_set (T_CSP_I2C *i2c, UPWORD val)
{
    i2c->I2CBRG = val;
}

__CSP_FUNC void  csp_i2c_baud_set_auto (T_CSP_I2C *i2c, U32 periph_bus_clk_freq, U32 bitrate)
{
   //   Full formula from docs:
   //       (periph_bus_clk_freq * (1/(2 * bitrate) - Tpgd)) - 2
   //   Earlier, simplified formula was used:
   //       (periph_bus_clk_freq / 2 / bitrate)
   //   but its results were slightly less than needed.
   //
   //   Normal formula used in this module:
   //
   //       ((pbClk) * (_NANO_FACTOR/(2*bitrate) - Tpgd_in_ns)/_NANO_FACTOR) - 2
   //
   //   but to avoid overflow, we need to divide pbClk by _SCALE_FACTOR, which is 1000.
   //   and, of course, balance it later.

#define  _NANO_FACTOR                  1000000000
#define  _SCALE_FACTOR                 100000
#define  _DEFAULT_GOBBLER_DELAY__NS    104   //-- docs says that typical value of 
                                             //   Tpgd (Pulse Gobbler Delay) is 104 ns.
                                             //   I'm still not sure is it related to board hardware,
                                             //   or to specific MCU.
                                             //   If it is related to board hardware, then it should be passed
                                             //   as an argument, of course.

   i2c->I2CBRG = 
      CSP_INT_DIV(
            (
             CSP_INT_DIV(
                periph_bus_clk_freq,
                _SCALE_FACTOR
                )
             *
             (
              CSP_INT_DIV(
                 _NANO_FACTOR,
                 2 * bitrate
                 ) 
              - _DEFAULT_GOBBLER_DELAY__NS
             )
            )
            ,
            (_NANO_FACTOR / _SCALE_FACTOR)
            )
      - 2
      ;


}


__CSP_FUNC UPWORD  csp_i2c_conf_get (T_CSP_I2C *i2c)
{
    return (i2c->I2CCON);
}

__CSP_FUNC void  csp_i2c_comm (T_CSP_I2C *i2c, enum E_CSP_I2C_Command comm)
{
    switch (comm)
    {
        case CSP_I2C_COMM_START:
        case CSP_I2C_COMM_RSTART:
        case CSP_I2C_COMM_STOP:
        case CSP_I2C_COMM_RECEIVE:
        case CSP_I2C_COMM_ACK:
           __CSP_SET_REG(&i2c->I2CCON) = (1 << comm);
           //__CSP_BIT_VAL_SET(i2c->I2CCON, comm); 
           break;
        case CSP_I2C_COMM_ACK_EN:
           __CSP_CLR_REG(&i2c->I2CCON) = (1 << __I2C_ACK_DIS_BIT);
           //__CSP_BIT_VAL_CLR(i2c->I2CCON, __I2C_ACK_DIS_BIT);
           break;
        case CSP_I2C_COMM_ACK_DIS:
           __CSP_SET_REG(&i2c->I2CCON) = (1 << __I2C_ACK_DIS_BIT);
           //__CSP_BIT_VAL_SET(i2c->I2CCON, __I2C_ACK_DIS_BIT);
           break;
        case CSP_I2C_COMM_SCL_HOLD:
           __CSP_SET_REG(&i2c->I2CCON) = (1 << __I2C_SCL_RELEASE_BIT);
           //__CSP_BIT_VAL_SET(i2c->I2CCON, __I2C_SCL_RELEASE_BIT);
           break;
        case CSP_I2C_COMM_SCL_REL:
           __CSP_CLR_REG(&i2c->I2CCON) = (1 << __I2C_SCL_RELEASE_BIT);
           //__CSP_BIT_VAL_CLR(i2c->I2CCON, __I2C_SCL_RELEASE_BIT);
           break;
    }
}


__CSP_FUNC void  csp_i2c_conf_set (T_CSP_I2C *i2c, UPWORD val)
{
    i2c->I2CCON = val;
}

__CSP_FUNC void  csp_i2c_en (T_CSP_I2C *i2c)
{
   i2c->_I2CCON_SET = CSP_I2C_EN;
}

__CSP_FUNC void  csp_i2c_dis (T_CSP_I2C *i2c)
{
   i2c->_I2CCON_CLR = CSP_I2C_EN;
}


__CSP_FUNC U08  csp_i2c_get (T_CSP_I2C *i2c)
{
    return (i2c->I2CRCV);
}


__CSP_FUNC void  csp_i2c_put (T_CSP_I2C *i2c, U08 val)
{
   //do{
      i2c->I2CTRN = val;
   //} while (!(i2c->I2CSTAT & CSP_I2C_STAT_TR_BUF_FULL));
    
}


__CSP_FUNC void  csp_i2c_stat_clr (T_CSP_I2C *i2c, enum E_CSP_I2C_Stat mask)
{
   __CSP_CLR_REG(&i2c->I2CSTAT) = mask;
    //__CSP_ACC_MASK_VAL(BFA_CLR, i2c->I2CSTAT, mask, UINT_MAX);
}


__CSP_FUNC enum E_CSP_I2C_Stat  csp_i2c_stat_get (T_CSP_I2C *i2c)
{
    return (enum E_CSP_I2C_Stat)(i2c->I2CSTAT);
}

/* ********************************************************************************************** */
/* end of file: i2c.c */
