/* *************************************************************************************************
Project:         MCHP 16-bit CPU Support Library
Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

 *************************************************************************************************
Distribution:

MCHP 16-bit CPU Support Library
----------------------------------------
Copyright � 2006-2008 Alex B.

 *************************************************************************************************
 MCU Family:      PIC24F/PIC24H/dsPIC33
Compiler:        Microchip C30 3.11

 *************************************************************************************************
File:            csp_cn_disb.c
Description:

 ********************************************************************************************** */

#include "csp_cn.h"

T_CSP_CN  CSP_CN   __CSP_SFR((CN_BASE_VIRT_ADDR));


/**
 * @param val see csp_cn.h definitions CSP_CN_....
 */
__CSP_FUNC void  csp_cn_conf (T_CSP_CN *p_cn, UPWORD val)
{
   p_cn->CNCON = val;
}

__CSP_FUNC void  csp_cn_dis (T_CSP_CN *p_cn, enum E_CSP_CN_Pin mask)
{
   __CSP_CLR_REG(&p_cn->CNEN) = mask;
   //__CSP_ACC_MASK_VAL(BFA_CLR, p_cn->CNEN, mask, UINT_MAX);
}


__CSP_FUNC void  csp_cn_en (T_CSP_CN *p_cn, enum E_CSP_CN_Pin mask)
{
   __CSP_SET_REG(&p_cn->CNEN) = mask;
   //__CSP_ACC_MASK_VAL(BFA_SET, p_cn->CNEN, mask, UINT_MAX);
}


__CSP_FUNC enum E_CSP_CN_Pin  csp_cn_get (T_CSP_CN *p_cn)
{
   return (enum E_CSP_CN_Pin)p_cn->CNEN;
}


__CSP_FUNC void  csp_cn_pullup_dis (T_CSP_CN *p_cn, enum E_CSP_CN_Pin mask)
{
   __CSP_CLR_REG(&p_cn->CNPUE) = mask;
   //__CSP_ACC_MASK_VAL(BFA_CLR, p_cn->CNPUE, mask, UINT_MAX);
}

__CSP_FUNC void  csp_cn_pullup_en (T_CSP_CN *p_cn, enum E_CSP_CN_Pin mask)
{
   __CSP_SET_REG(&p_cn->CNPUE) = mask;
   //__CSP_ACC_MASK_VAL(BFA_SET, p_cn->CNPUE, mask, UINT_MAX);
}

__CSP_FUNC enum E_CSP_CN_Pin  csp_cn_pullup_get (T_CSP_CN *p_cn)
{
   return (enum E_CSP_CN_Pin)p_cn->CNPUE;
}

__CSP_FUNC void  csp_cn_pullup_wr (T_CSP_CN *p_cn, enum E_CSP_CN_Pin val)
{
   p_cn->CNPUE = val;
}

__CSP_FUNC void  csp_cn_wr (T_CSP_CN *p_cn, enum E_CSP_CN_Pin val)
{
   p_cn->CNEN = val;
}

/* ********************************************************************************************** */
/* end of file: csp_cn_wrte.c */

