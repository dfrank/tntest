/* *************************************************************************************************
Project:         MCHP 16-bit CPU Support Library
Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

 *************************************************************************************************
Distribution:

MCHP 16-bit CPU Support Library
----------------------------------------
Copyright � 2006-2008 Alex B.

 *************************************************************************************************
 MCU Family:      PIC24F/PIC24H/dsPIC33
Compiler:        Microchip C30 3.11

 *************************************************************************************************
File:            csp_uart.h
Description:

 ********************************************************************************************** */

#ifndef __CSP_UART_H
#define __CSP_UART_H

#include "../cspi.h"

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC32MX3XX_4XX)          \
    )

/* UART programming model */
/* ---------------------- */

typedef struct S_CSP_Uart
{
   volatile UPWORD UMODE;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD USTA;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD UTXREG;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD URXREG;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD UBRG;
} T_CSP_Uart;


#if ((__CSP_FAMILY == __CSP_FAMILY_PIC32MX3XX_4XX)          \
    )

/* UART base addresses */
/* ------------------- */
#define UART_BASE_VIRT_ADDR      (0xbf800000U)

#define UART1_BASE_VIRT_ADDR     (UART_BASE_VIRT_ADDR + 0x6000)
#define UART2_BASE_VIRT_ADDR     (UART_BASE_VIRT_ADDR + 0x6200)

/* Declare UART models */
/* ------------------- */

extern T_CSP_Uart CSP_UART1;
extern T_CSP_Uart CSP_UART2;

#endif

/* Definitions for UART API */
/* ------------------------ */

/* csp_uart_conf_set() and csp_uart_conf_get() parameters mask */

#define CSP_UART_EN                 (1ULL << 47) /* UART is enabled */
#define CSP_UART_DIS                (0ULL << 47) /* UART is disabled */

//-- ??? there's no such bit in docs, but it is present in watch window!
#define CSP_UART_DEBUG_FREEZE_EN    (1ULL << 46) /* When emulator is in Debug mode, module freezes operation */
#define CSP_UART_DEBUG_FREEZE_DIS   (0ULL << 46) /* When emulator is in Debug mode, module continues operation */

#define CSP_UART_IDLE_STOP          (1ULL << 45) /* Discontinue operation when device enters Idle mode */
#define CSP_UART_IDLE_CON           (0ULL << 45) /* Continue operation in Idle mode */

#define CSP_UART_IRDA_EN            (1ULL << 44) /* IrDA encoder and decoder enabled */
#define CSP_UART_IRDA_DIS           (0ULL << 44) /* IrDA encoder and decoder disabled */

#define CSP_UART_RTS_SIMPLEX        (1ULL << 43) /* URTS in Simplex mode */
#define CSP_UART_RTS_FLOW_CONTROL   (0ULL << 43) /* URTS in Flow Control mode */

#define CSP_UART_PINS_TX_RX_BCLK    (3ULL << 40) /* UTX, URX and BCLK pins are enabled and used; UCTS pin is controlled by port latches */
#define CSP_UART_PINS_TX_RX_RTS_CTS (2ULL << 40) /* UTX, URX, UCTS and URTS pins are enabled and used */
#define CSP_UART_PINS_TX_RX_RTS     (1ULL << 40) /* UTX, URX and URTS pins are enabled and used; UCTS pin is controlled by port latches */
#define CSP_UART_PINS_TX_RX         (0ULL << 40) /* Only UTX and URX pins are enabled and used */

//-- wakeup bit is skipped since it is controlled by csp_uart_comm()

#define CSP_UART_LOOPBACK_EN        (1ULL << 38) /* Enable Loopback mode */
#define CSP_UART_LOOPBACK_DIS       (0ULL << 38) /* Loopback mode is disabled */

#define CSP_UART_RX_IDLE_0          (1ULL << 36) /* UxRX Idle state is �0� */
#define CSP_UART_RX_IDLE_1          (0ULL << 36) /* UxRX Idle state is �1� */

#define CSP_UART_HIGH_BAUD_EN       (1ULL << 35) /* High speed (4x baud clock enabled) */
#define CSP_UART_HIGH_BAUD_DIS      (0ULL << 35) /* Standard speed (16x baud clock enabled)*/

#define CSP_UART_MODE_8_NO          (0ULL << 33) /* 8-bit data, no parity */
#define CSP_UART_MODE_8_EVEN        (1ULL << 33) /* 8-bit data, even parity */
#define CSP_UART_MODE_8_ODD         (2ULL << 33) /* 8-bit data, odd parity */
#define CSP_UART_MODE_9_NO          (3ULL << 33) /* 9-bit data, no parity */

#define CSP_UART_STOP_2             (1ULL << 32) /* 2 Stop bits */
#define CSP_UART_STOP_1             (0ULL << 32) /* 1 Stop bit */


#define CSP_UART_TX_INT_MODE_1      (2ULL << 14) /* Interrupt generated when the transmit buffer becomes empty */
#define CSP_UART_TX_INT_MODE_2      (1ULL << 14) /* Interrupt generated when all characters have been transmitted  */
#define CSP_UART_TX_INT_MODE_3      (0ULL << 14) /* Interrupt generated when the transmit buffer contains at least one empty space*/

#define CSP_UART_TX_IDLE_0          (1ULL << 13) /* UTX Idle state is �0� */
#define CSP_UART_TX_IDLE_1          (0ULL << 13) /* UTX Idle state is �1�*/

#define CSP_UART_RX_EN              (1ULL << 12) /* UART receiver is enabled. Rx pin is controlled by UART module (if CSP_UART_EN) */
#define CSP_UART_RX_DIS             (0ULL << 12) /* UART receiver is disbled. Rx pin is ignored by UART module, and is controlled by port. */

#define CSP_UART_TX_EN              (1ULL << 10) /* UART transmitter enabled */
#define CSP_UART_TX_DIS             (0ULL << 10) /* UART transmitter disabled, any pending transmission is aborted and buffer is reset */

#define CSP_UART_RX_INT_MODE_1      (3ULL <<  6) /* Interrupt flag bit is set when receive buffer is full (i.e. has 4 data chars)*/
#define CSP_UART_RX_INT_MODE_2      (2ULL <<  6) /* Interrupt flag bit is set when receive buffer is 3/4 full (i.e. has 3 data chars)*/
#define CSP_UART_RX_INT_MODE_3      (1ULL <<  6) /* Interrupt flag bit is set when a character is received */

#define CSP_UART_RX_ADDR_DET_EN     (1ULL <<  5) /* Address Detect mode enabled. If 9-bit mode is not selected, this control bit has no effect */
#define CSP_UART_RX_ADDR_DET_DIS    (0ULL <<  5) /* Address Detect mode disabled */


/* csp_uart_stat_get() and csp_uart_stat_clr() parameters type */

enum E_CSP_UartStat {
   CSP_UART_STAT_TX_BUF_FULL      = (1 <<  9),  /* RO  - Transmit buffer is full */
   CSP_UART_STAT_TX_SREG_EMPTY    = (1 <<  8),  /* RO  - Transmit Shift register is empty and transmit buffer is empty */
   CSP_UART_STAT_RX_IDLE          = (1 <<  4),  /* RO  - Receiver is Idle */
   CSP_UART_STAT_RX_BUF_NOT_EMPTY = (1 <<  0),  /* RO  - Receive buffer has data, at least one more character can be read */
   CSP_UART_STAT_ERR_PARITY       = (1 <<  3),  /* RO  - Parity error has been detected for the current character */
   CSP_UART_STAT_ERR_FRAME        = (1 <<  2),  /* RO  - Framing error has been detected for the current character */
   CSP_UART_STAT_ERR_OVF          = (1 <<  1)   /* R/C - Receive buffer has overflowed */
};

/* csp_uart_comm() parameter type */

enum E_CSP_UartCommand {
   CSP_UART_COMM_WAKEUP,
   CSP_UART_COMM_AUTOBAUD,
   CSP_UART_COMM_BREAK,
};

#endif


/* External functions prototypes */
/* ----------------------------- */

void      csp_uart_conf_set(T_CSP_Uart *uart, U64 val);
U64       csp_uart_conf_get(T_CSP_Uart *uart);

void      csp_uart_brg_set(T_CSP_Uart *uart, UPWORD val);
UPWORD     csp_uart_brg_get(T_CSP_Uart *uart);
void      csp_uart_baud_set_auto (T_CSP_Uart *uart, UPWORD periph_bus_clk_freq, UPWORD baudrate);

void      csp_uart_comm(T_CSP_Uart *uart, enum E_CSP_UartCommand comm);

enum E_CSP_UartStat csp_uart_stat_get(T_CSP_Uart *uart);
void      csp_uart_stat_clr(T_CSP_Uart *uart, enum E_CSP_UartStat mask);

void      csp_uart_put(T_CSP_Uart *uart, UPWORD val);
UPWORD     csp_uart_get(T_CSP_Uart *uart);

#if !defined(__CSP_BUILD_LIB)

/* Module API definition */

#if defined(__CSP_DEBUG_UART)

#else

#include "csp_uart.c"

#endif

#endif

#endif /* #ifndef __CSP_UART_H */
/* ********************************************************************************************** */
/* end of file: csp_uart.h */
