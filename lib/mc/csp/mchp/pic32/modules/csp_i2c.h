/* *************************************************************************************************
Project:         MCHP 16-bit CPU Support Library
Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

 *************************************************************************************************
Distribution:

MCHP 16-bit CPU Support Library
----------------------------------------
Copyright � 2006-2008 Alex B.

 *************************************************************************************************
 MCU Family:      PIC24F/PIC24H/dsPIC33
Compiler:        Microchip C30 3.11

 *************************************************************************************************
File:            csp_i2c.h
Description:

 ********************************************************************************************** */

#ifndef __CSP_I2C_H
#define __CSP_I2C_H

#include "../cspi.h"

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC32MX3XX_4XX)  \
    )

/* I2C programming model */
/* --------------------- */

typedef struct S_CSP_I2C
{
   __CSP_REG_DEF(I2CCON);
   __CSP_REG_DEF(I2CSTAT);
   __CSP_REG_DEF(I2CADD);
   __CSP_REG_DEF(I2CMSK);
   __CSP_REG_DEF(I2CBRG);
   __CSP_REG_DEF(I2CTRN);
   volatile UPWORD I2CRCV;
   __CSP_SKIP_BYTE(12);
} T_CSP_I2C;


#if ((__CSP_FAMILY == __CSP_FAMILY_PIC32MX3XX_4XX)  \
    )

/* I2C base addresses */
/* ------------------ */

/* Declare I2C models */
/* ------------------ */

extern T_CSP_I2C CSP_I2C1;
extern T_CSP_I2C CSP_I2C2;

#endif

/* Internal API definitions */
/* ------------------------ */

#define __I2C_SCL_RELEASE_BIT   (12)        /* I2CxCON<12> */
#define __I2C_ACK_DIS_BIT       (5)         /* I2CxCON<5>  */

/* Definitions for I2C API */
/* ----------------------- */

/* csp_i2c_conf_set() and csp_i2c_conf_get() parameters mask */

#define CSP_I2C_EN                  (1 << 15)   /* Enables the I2C module   */
#define CSP_I2C_DIS                 (0)         /* Disables the I2Cx module */

#define CSP_I2C_IDLE_STOP           (1 << 13)   /* Discontinue module operation when device enters an Idle mode */
#define CSP_I2C_IDLE_CON            (0)         /* Continue module operation in Idle mode */

//#define CSP_I2C_IPMI_EN             (1 << 11)   /* IPMI Support mode is enabled; all addresses Acknowledged */
//#define CSP_I2C_IPMI_DIS            (0)         /* IPMI Support mode disabled */

#define CSP_I2C_ADDR_10B            (1 << 10)   /* I2CxADD is a 10-bit slave address */
#define CSP_I2C_ADDR_7B             (0)         /* I2CxADD is a 7-bit slave address */

#define CSP_I2C_SLEW_RATE_DIS       (1 <<  9)   /* Slew rate control disabled */
#define CSP_I2C_SLEW_RATE_EN        (0)         /* Slew rate control enabled */

#define CSP_I2C_SMBUS_EN            (1 <<  8)   /* Enable I/O pin thresholds compliant with SMBus specification */
#define CSP_I2C_SMBUS_DIS           (0)         /* Disable SMBus input thresholds */

#define CSP_I2C_GEN_CALL_EN         (1 <<  7)   /* Enable interrupt when a general call address is received */
#define CSP_I2C_GEN_CALL_DIS        (0)         /* General call address disabled */

#define CSP_I2C_CLK_STRETCH_EN      (1 <<  6)   /* Enable software or receive clock stretching */
#define CSP_I2C_CLK_STRETCH_DIS     (0)         /* Disable software or receive clock stretching */

/* csp_i2c_comm() parameter type */

enum E_CSP_I2C_Command {
   CSP_I2C_COMM_START    = (0),  /* Initiate Start condition            */
   CSP_I2C_COMM_RSTART   = (1),  /* Initiate Repeated Start condition   */
   CSP_I2C_COMM_STOP     = (2),  /* Initiate Stop condition             */
   CSP_I2C_COMM_RECEIVE  = (3),  /* Enables Receive mode for I2C        */
   CSP_I2C_COMM_ACK      = (4),  /* Initiate Acknowledge sequence       */

   CSP_I2C_COMM_ACK_DIS  = (5),  /* Send NACK during Acknowledge        */
   CSP_I2C_COMM_ACK_EN   = (6),  /* Send ACK during Acknowledge         */

   CSP_I2C_COMM_SCL_HOLD = (7),  /* Hold SCLx clock low (clock stretch) */
   CSP_I2C_COMM_SCL_REL  = (8)   /* Release SCLx clock                  */

};

/* csp_i2c_stat_get() and csp_i2c_stat_clr() parameters type */

enum E_CSP_I2C_Stat {
   CSP_I2C_STAT_NACK           = (1 << 15),    /* RO  - NACK received from slave */
   CSP_I2C_STAT_TRANSMIT       = (1 << 14),    /* RO  - Master transmit is in progress (8 bits + ACK) */
   CSP_I2C_STAT_BUS_COLL       = (1 << 10),    /* R/C - A bus collision has been detected during a master operation */
   CSP_I2C_STAT_GEN_CALL       = (1 <<  9),    /* RO  - General call address was received */
   CSP_I2C_STAT_ADDR_10B_MATCH = (1 <<  8),    /* RO  - 10-bit address was matched */
   CSP_I2C_STAT_WRITE_COLL     = (1 <<  7),    /* R/C - An attempt to write the I2CxTRN register failed because the I2C module is busy */
   CSP_I2C_STAT_RC_OVF         = (1 <<  6),    /* R/C - A byte was received while the I2CxRCV register is still holding the previous byte */
   CSP_I2C_STAT_LAST_DATA      = (1 <<  5),    /* RO  - Indicates that the last byte received was data */
   CSP_I2C_STAT_STOP           = (1 <<  4),    /* R/C - Indicates that a Stop bit has been detected last */
   CSP_I2C_STAT_START          = (1 <<  3),    /* R/C - Indicates that a Start (or Repeated Start) bit has been detected last */
   CSP_I2C_STAT_SLAVE_READ     = (1 <<  2),    /* RO  - Read: data transfer is output from slave */
   CSP_I2C_STAT_RC_BUF_FULL    = (1 <<  1),    /* RO  - Receive complete, I2CxRCV is full */
   CSP_I2C_STAT_TR_BUF_FULL    = (1 <<  0)     /* RO  - Transmit in progress, I2CxTRN is full */

};

#endif


/* External functions prototypes */
/* ----------------------------- */


void     csp_i2c_conf_set(T_CSP_I2C *i2c, UPWORD val);
UPWORD    csp_i2c_conf_get(T_CSP_I2C *i2c);

void     csp_i2c_en (T_CSP_I2C *i2c);
void     csp_i2c_dis (T_CSP_I2C *i2c);

void     csp_i2c_baud_set(T_CSP_I2C *i2c, UPWORD val);
void     csp_i2c_baud_set_auto (T_CSP_I2C *i2c, U32 periph_bus_clk_freq, U32 bitrate);
UPWORD    csp_i2c_baud_get(T_CSP_I2C *i2c);

void     csp_i2c_comm(T_CSP_I2C *i2c, enum E_CSP_I2C_Command comm);

enum E_CSP_I2C_Stat csp_i2c_stat_get(T_CSP_I2C *i2c);
void     csp_i2c_stat_clr(T_CSP_I2C *i2c, enum E_CSP_I2C_Stat mask);

void     csp_i2c_addr_set(T_CSP_I2C *i2c, UPWORD val);
UPWORD    csp_i2c_addr_get(T_CSP_I2C *i2c);

void     csp_i2c_addr_mask_set(T_CSP_I2C *i2c, UPWORD val);
UPWORD    csp_i2c_addr_mask_get(T_CSP_I2C *i2c);

void     csp_i2c_put(T_CSP_I2C *i2c, U08 val);
U08      csp_i2c_get(T_CSP_I2C *i2c);

#if !defined(__CSP_BUILD_LIB)

/* Module API definition */

#if defined(__CSP_DEBUG_I2C)

//-- prototypes were here

#else

#include "csp_i2c.c"

#endif

#endif

#endif /* #ifndef __CSP_I2C_H */
/* ********************************************************************************************** */
/* end of file: csp_i2c.h */
