
#include "csp_ic.h"

#include "../cspi.h"

#if defined(__CSP_BUILD_LIB)

#define _IC1_BASE_VIRT_ADDR         (0xBF800000 + 0x2000)
#define _IC2_BASE_VIRT_ADDR         (0xBF800000 + 0x2200)
#define _IC3_BASE_VIRT_ADDR         (0xBF800000 + 0x2400)
#define _IC4_BASE_VIRT_ADDR         (0xBF800000 + 0x2600)
#define _IC5_BASE_VIRT_ADDR         (0xBF800000 + 0x2800)

T_CSP_InputCapture CSP_IC_1 __CSP_SFR((_IC1_BASE_VIRT_ADDR));
T_CSP_InputCapture CSP_IC_2 __CSP_SFR((_IC2_BASE_VIRT_ADDR));
T_CSP_InputCapture CSP_IC_3 __CSP_SFR((_IC3_BASE_VIRT_ADDR));
T_CSP_InputCapture CSP_IC_4 __CSP_SFR((_IC4_BASE_VIRT_ADDR));
T_CSP_InputCapture CSP_IC_5 __CSP_SFR((_IC5_BASE_VIRT_ADDR));

#endif

__CSP_FUNC void csp_ic__conf__set(T_CSP_InputCapture *p_ic, UPWORD val)
{
   p_ic->CON = val;
}

__CSP_FUNC UPWORD csp_ic__conf__get(T_CSP_InputCapture *p_ic)
{
   return p_ic->CON;
}

__CSP_FUNC void csp_ic__en(T_CSP_InputCapture *p_ic)
{
   p_ic->_CON_SET = (1 << _CSP_OFS_INP_CAP__ON);
}

__CSP_FUNC void csp_ic__dis(T_CSP_InputCapture *p_ic)
{
   p_ic->_CON_CLR = (1 << _CSP_OFS_INP_CAP__ON);
}

__CSP_FUNC enum E_CSP_IC_Status csp_ic__status__get(T_CSP_InputCapture *p_ic)
{
   return (p_ic->CON & CSP_IC_STATUS_MASK);
}

__CSP_FUNC bool csp_ic__data__is_available(T_CSP_InputCapture *p_ic)
{
   return ((csp_ic__status__get(p_ic) & CSP_IC_STATUS__BUF_NOT_EMPTY) != 0);
}

__CSP_FUNC bool csp_ic__is_overflow(T_CSP_InputCapture *p_ic)
{
   return ((csp_ic__status__get(p_ic) & CSP_IC_STATUS__OVERFLOW) != 0);
}

__CSP_FUNC UPWORD csp_ic__data__read(T_CSP_InputCapture *p_ic)
{
   UPWORD ret = 0;

   if (csp_ic__data__is_available(p_ic)){
      UPWORD conf = csp_ic__conf__get(p_ic);

      if (conf & (1 << _CSP_OFS_INP_CAP__C32)){
         //-- 32-bit timer src
         ret = p_ic->BUF;
      } else {
         //-- 16-bit timer src: then, we should care which timer is used for source:
         //   Timer2 provides lower 16 bits, and Timer3 provides upper 16 bits.
         if (conf & (1 << _CSP_OFS_INP_CAP__ICTMR)){
            //-- Timer3 is used
            ret = ((p_ic->BUF >> 16) & 0xffff);
         } else {
            //-- Timer2 is used
            ret = ((p_ic->BUF >> 0)  & 0xffff);
         }
      }
   }

   return ret;
}



