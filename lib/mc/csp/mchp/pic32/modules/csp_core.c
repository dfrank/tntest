/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_core_disg.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UPWORD csp_core_disi_get (void)
{
    return CORE.DISICNT;
}

/* ********************************************************************************************** */
/* end of file: csp_core_disg.c */

/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_core_prig.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC INT_PRI csp_core_priority_get (void)
{
    return __CSP_ACC_RANG_REG(BFA_RD, CORE.SR, __CORE_PRI_FB, __CORE_PRI_FE);
}

/* ********************************************************************************************** */
/* end of file: csp_core_prig.c */

/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_core_prip.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC INT_PRI csp_core_priority_set (INT_PRI val)
{
    UPWORD ret;
    ret = __CSP_ACC_RANG_REG(BFA_RD, CORE.SR, __CORE_PRI_FB, __CORE_PRI_FE);
    __CSP_ACC_RANG_REG(BFA_WR, CORE.SR, __CORE_PRI_FB, __CORE_PRI_FE, val);
    return ret;
}

/* ********************************************************************************************** */
/* end of file: csp_core_prip.c */

