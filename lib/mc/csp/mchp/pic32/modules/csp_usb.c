
#include "csp_usb.h"

//#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
//#endif


T_CSP_USB CSP_USB __CSP_SFR((USB_BASE_VIRT_ADDR));


__CSP_FUNC void csp_usb_pp_next (volatile USB_BDT_ENTRY **p)
{
   *(U08*)(p) ^= 4;
}


/* OTG STATUS */

__CSP_FUNC USB_OTGSTAT csp_usb_otg_stat_get (void)
{
   return CSP_USB.OTGSTAT;
}

/* USB INTERRUPTS */
__CSP_FUNC void csp_usb_int_en (USB_INT mask)
{
   __CSP_ACC_MASK_REG(BFA_SET, CSP_USB.IE, mask, UINT_MAX);
}
__CSP_FUNC void csp_usb_int_dis (USB_INT mask)
{
   __CSP_ACC_MASK_REG(BFA_CLR, CSP_USB.IE, mask, UINT_MAX);
}
__CSP_FUNC USB_INT csp_usb_int_get (void)
{
   return CSP_USB.IE;
}
__CSP_FUNC USB_INT csp_usb_int_flag_get (void)
{
   return CSP_USB.IR;
}
__CSP_FUNC void csp_usb_int_flag_clr (USB_INT mask)
{
   __CSP_ACC_MASK_REG(BFA_SET, CSP_USB.IR, mask, UINT_MAX);
}


/* USB ERRORS */
__CSP_FUNC void csp_usb_err_en (USB_ERR mask)
{
   __CSP_ACC_MASK_REG(BFA_SET, CSP_USB.EIE, mask, UINT_MAX);
}
__CSP_FUNC void csp_usb_err_dis (USB_ERR mask)
{
   __CSP_ACC_MASK_REG(BFA_CLR, CSP_USB.EIE, mask, UINT_MAX);
}
__CSP_FUNC USB_ERR csp_usb_err_flag_get (void)
{
   return CSP_USB.EIR;
}
__CSP_FUNC void csp_usb_err_flag_clr (USB_ERR mask)
{
   __CSP_ACC_MASK_REG(BFA_SET, CSP_USB.EIR, mask, UINT_MAX);
}


/* USB OTG INT */
__CSP_FUNC void csp_usb_otg_int_en (USB_OTG_INT mask)
{
   __CSP_ACC_MASK_REG(BFA_SET, CSP_USB.OTGIE, mask, UINT_MAX);
}
__CSP_FUNC void csp_usb_otg_int_dis (USB_OTG_INT mask)
{
   __CSP_ACC_MASK_REG(BFA_CLR, CSP_USB.OTGIE, mask, UINT_MAX);
}
__CSP_FUNC USB_OTG_INT csp_usb_otg_int_get (void)
{
   return CSP_USB.OTGIE;
}
__CSP_FUNC USB_OTG_INT csp_usb_otg_int_flag_get (void)
{
   return CSP_USB.OTGIR;
}
__CSP_FUNC void csp_usb_otg_int_flag_clr (USB_OTG_INT mask)
{
   __CSP_ACC_MASK_REG(BFA_SET, CSP_USB.OTGIR, mask, UINT_MAX);
}


/* OTG CONF */
__CSP_FUNC void csp_usb_otg_con_put (USB_OTG_CON val)
{
   CSP_USB.OTGCON = val;
}
__CSP_FUNC USB_OTG_CON csp_usb_otg_con_get (void)
{
   return (USB_OTG_CON)CSP_USB.OTGCON;
}
__CSP_FUNC void csp_usb_otg_con_set (USB_OTG_CON mask)
{
   __CSP_ACC_MASK_REG(BFA_SET, CSP_USB.OTGCON, mask, UINT_MAX);
}
__CSP_FUNC void csp_usb_otg_con_clr (USB_OTG_CON mask)
{
   __CSP_ACC_MASK_REG(BFA_CLR, CSP_USB.OTGCON, mask, UINT_MAX);
}



/* USB CONFIG */
__CSP_FUNC void csp_usb_cnfg_set (T_CSP_USB_Cfg val)
{
   CSP_USB.CNFG1  = (val >> 0) & 0xff;
   //CSP_USB.OTGCON = (val >> 8) & 0xff;
}
__CSP_FUNC T_CSP_USB_Cfg csp_usb_cnfg_get (void)
{
   return (T_CSP_USB_Cfg)(0
         //| ((CSP_USB.OTGCON & 0xff) << 8) 
         | ((CSP_USB.CNFG1 & 0xFF) << 0)
         );
}


/* USB CON */
__CSP_FUNC void csp_usb_con_put (USB_CON val)
{
   CSP_USB.CON = val;
}
__CSP_FUNC USB_CON csp_usb_con_get (void)
{
   return (USB_CON)CSP_USB.CON;
}
__CSP_FUNC void csp_usb_con_set (USB_CON mask)
{
   __CSP_ACC_MASK_REG(BFA_SET, CSP_USB.CON, mask, UINT_MAX);
}
__CSP_FUNC void csp_usb_con_clr (USB_CON mask)
{
   __CSP_ACC_MASK_REG(BFA_CLR, CSP_USB.CON, mask, UINT_MAX);
}


/* USB POWER */
__CSP_FUNC void csp_usb_power_conf_set (USB_POW_CON val)
{
   CSP_USB.PWRC = val;
}
__CSP_FUNC USB_POW_CON csp_usb_power_conf_get (void)
{
   return CSP_USB.PWRC;
}
__CSP_FUNC void csp_usb_en (void)
{
   __CSP_SET_REG(&CSP_USB.PWRC) = (1 << USB_EN_BIT);
   //__CSP_BIT_REG_SET(CSP_USB.PWRC, USB_EN_BIT);
}
__CSP_FUNC void csp_usb_dis (void)
{
   __CSP_CLR_REG(&CSP_USB.PWRC) = (1 << USB_EN_BIT);
   //__CSP_BIT_REG_CLR(CSP_USB.PWRC, USB_EN_BIT);
}


/* BDT */
__CSP_FUNC void csp_usb_bdt_set (void *pt)
{
   CSP_USB.BDTP1 = (UPWORD)(pt) / 256;
}


/* ADDR */
__CSP_FUNC void csp_usb_addr_set (UPWORD addr)
{
   CSP_USB.ADDR = addr;
}
__CSP_FUNC UPWORD csp_usb_addr_get (void)
{
   return CSP_USB.ADDR;
}


/* EP */
__CSP_FUNC void csp_usb_ep_conf_put (USB_EP_INDX ep, USB_EP_CONF val)
{
   volatile UPWORD *pep = &CSP_USB.EP0 + ep * 4/*4 because there's 0x10 bytes difference between EP0 and EP1. 10 bytes == 4 words*/;
   *pep = val;
}
__CSP_FUNC USB_EP_CONF csp_usb_ep_conf_get (USB_EP_INDX ep)
{
   volatile UPWORD *pep = &CSP_USB.EP0 + ep * 4;
   return *pep;
}
__CSP_FUNC void csp_usb_ep_conf_set (USB_EP_INDX ep, USB_EP_CONF mask)
{
   volatile UPWORD *pep = &CSP_USB.EP0 + ep * 4;
   __CSP_ACC_MASK_IND(BFA_SET, pep, mask, UINT_MAX);
}
__CSP_FUNC void csp_usb_ep_conf_clr (USB_EP_INDX ep, USB_EP_CONF mask)
{
   volatile UPWORD *pep = &CSP_USB.EP0 + ep * 4;
   __CSP_ACC_MASK_IND(BFA_CLR, pep, mask, UINT_MAX);
}
//__CSP_FUNC UPWORD *csp_usb_ep_ptr_get (USB_EP_INDX ep)
//{
//    return (UPWORD*)(&CSP_USB.EP0 + ep);
//}



/* STAT */
__CSP_FUNC USB_STAT csp_usb_stat_get (void)
{
   return (USB_STAT)CSP_USB.STAT;
}
__CSP_FUNC USB_EP_INDX csp_usb_stat_ep_get (void)
{
   return __CSP_ACC_RANG_REG(BFA_RD, CSP_USB.STAT, __USB_STAT_LAST_EP_FS, __USB_STAT_LAST_EP_FE);
}

