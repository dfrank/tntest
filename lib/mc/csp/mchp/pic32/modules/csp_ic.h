
#ifndef __CSP_IC_H
#define __CSP_IC_H

#include "../cspi.h"

/* Input Capture programming model */
/* -------------------------------- */

typedef struct S_CSP_InputCapture
{
   __CSP_REG_DEF(CON);
   volatile UPWORD BUF;
} T_CSP_InputCapture;

/* Input Capture base addresses */
/* ----------------------------- */

/* Declare Input Capture models */
/* ----------------------------- */

extern T_CSP_InputCapture CSP_IC_1;
extern T_CSP_InputCapture CSP_IC_2;
extern T_CSP_InputCapture CSP_IC_3;
extern T_CSP_InputCapture CSP_IC_4;
extern T_CSP_InputCapture CSP_IC_5;


/* Internal API definitions */
/* ------------------------ */

#define _CSP_OFS_INP_CAP__ON                 15
#define _CSP_OFS_INP_CAP__FRZ                14
#define _CSP_OFS_INP_CAP__SIDL               13
#define _CSP_OFS_INP_CAP__FEDGE              9
#define _CSP_OFS_INP_CAP__C32                8
#define _CSP_OFS_INP_CAP__ICTMR              7
#define _CSP_OFS_INP_CAP__ICI                5
#define _CSP_OFS_INP_CAP__ICOV               4
#define _CSP_OFS_INP_CAP__ICBNE              3
#define _CSP_OFS_INP_CAP__ICM                0




/* Definitions for Output compare API */
/* ---------------------------------- */

/* csp_ic__conf__set() and csp_ic__conf__get() parameters mask */

#define CSP_INP_CAP__EN               (1 << _CSP_OFS_INP_CAP__ON) /* Module enabled */
#define CSP_INP_CAP__DIS               (0) /* Disable and reset module, disable clocks,
                                              disable interrupt generation and allow SFR modifications.
                                              NOTE: When using 1:1 PBCLK divisor, the user�s
                                              software should not read/write the peripheral�s
                                              SFRs in the SYSCLK cycle immediately following the
                                              instruction that clears the module�s ON bit
                                              */

#define CSP_INP_CAP__DEB_STOP                   (1 << _CSP_OFS_INP_CAP__FRZ)   /* Freeze module operation when in Debug mode */
#define CSP_INP_CAP__DEB_CON                    (0)                            /* Do not freeze module operation when in debug mode */

#define CSP_INP_CAP__IDLE_STOP                  (1 << _CSP_OFS_INP_CAP__SIDL)  /* Halt in CPU idle mode */
#define CSP_INP_CAP__IDLE_CON                   (0)                            /* Continue to operate in CPU idle mode */

/* NOTE: this bit is used only in mode CSP_INP_CAP__MODE__EVERY_EDG_SP_FST) */
#define CSP_INP_CAP__FIRST_EDG_RIS              (1 << _CSP_OFS_INP_CAP__FEDGE) /* Capture rising edge first */
#define CSP_INP_CAP__FIRST_EDG_FALL             (0)                            /* Capture falling edge first */

#define CSP_INP_CAP__32BIT_TMR_SRC              (1 << _CSP_OFS_INP_CAP__C32)   /* 32-bit timer resource capture */
#define CSP_INP_CAP__16BIT_TMR_SRC              (0)                            /* 16-bit timer resource capture */

#define CSP_INP_CAP__SRC_TMR_3                  (1 << _CSP_OFS_INP_CAP__ICTMR) /* Timer3 is the counter source for capture */
#define CSP_INP_CAP__SRC_TMR_2                  (0)                            /* Timer2 is the counter source for capture */

#define CSP_INP_CAP__INT_EVERY_4                (3 << _CSP_OFS_INP_CAP__ICI)   /* Interrupt on every fourth capture event */
#define CSP_INP_CAP__INT_EVERY_3                (2 << _CSP_OFS_INP_CAP__ICI)   /* Interrupt on every third capture event */
#define CSP_INP_CAP__INT_EVERY_2                (1 << _CSP_OFS_INP_CAP__ICI)   /* Interrupt on every second capture event */
#define CSP_INP_CAP__INT_EVERY_1                (0 << _CSP_OFS_INP_CAP__ICI)   /* Interrupt on every capture event */


#define CSP_INP_CAP__MODE__DIS                  (0 << _CSP_OFS_INP_CAP__ICM)   /* Capture disable mode */
#define CSP_INP_CAP__MODE__EVERY_EDG            (1 << _CSP_OFS_INP_CAP__ICM)   /* Edge Detect mode - every edge (rising and falling) */
#define CSP_INP_CAP__MODE__EVERY_FALL           (2 << _CSP_OFS_INP_CAP__ICM)   /* Simple Capture Event mode - every falling edge */
#define CSP_INP_CAP__MODE__EVERY_RIS            (3 << _CSP_OFS_INP_CAP__ICM)   /* Simple Capture Event mode - every rising edge */
#define CSP_INP_CAP__MODE__EVERY_4_RIS_EDG      (4 << _CSP_OFS_INP_CAP__ICM)   /* Prescaled Capture Event mode - every fourth rising edge */
#define CSP_INP_CAP__MODE__EVERY_16_RIS_EDG     (5 << _CSP_OFS_INP_CAP__ICM)   /* Prescaled Capture Event mode - every sixteenth rising edge */
#define CSP_INP_CAP__MODE__EVERY_EDG_SP_FST     (6 << _CSP_OFS_INP_CAP__ICM)   /* Together with CSP_INP_CAP__FIRST_EDG_RIS/FALL: Simple Capture Event mode - every edge, specified edge first and every edge thereafter */
#define CSP_INP_CAP__MODE__INTERRUPT_ONLY       (7 << _CSP_OFS_INP_CAP__ICM)   /* Interrupt-Only mode (only supported while in Sleep mode or Idle mode) */


/* csp_oc_stat_get() return type */

enum E_CSP_IC_Status
{
   CSP_IC_STATUS__OVERFLOW       = (1 << _CSP_OFS_INP_CAP__ICOV),  /* Input capture overflow occured */
   CSP_IC_STATUS__BUF_NOT_EMPTY  = (1 << _CSP_OFS_INP_CAP__ICBNE), /* Input capture buffer is not empty; at least one more capture value can be read (from 4-level FIFO buffer) */
};

#define CSP_IC_STATUS_MASK  (0                                 \
      | CSP_IC_STATUS__OVERFLOW                                \
      | CSP_IC_STATUS__BUF_NOT_EMPTY                           \
      )




/* External functions prototypes */
/* ----------------------------- */

void                    csp_ic__conf__set          (T_CSP_InputCapture *p_ic, UPWORD val);
UPWORD                  csp_ic__conf__get          (T_CSP_InputCapture *p_ic);
void                    csp_ic__en                 (T_CSP_InputCapture *p_ic);
void                    csp_ic__dis                (T_CSP_InputCapture *p_ic);
enum E_CSP_IC_Status    csp_ic__status__get        (T_CSP_InputCapture *p_ic);
bool                    csp_ic__data__is_available (T_CSP_InputCapture *p_ic);
bool                    csp_ic__is_overflow        (T_CSP_InputCapture *p_ic);
UPWORD                  csp_ic__data__read         (T_CSP_InputCapture *p_ic);
#if !defined(__CSP_BUILD_LIB)

/* Module API definition */

#if defined(__CSP_DEBUG_OC)

//-- prototypes were here

#else

#include "csp_ic.c"

#endif

#endif

#endif /* #ifndef __CSP_IC_H */

