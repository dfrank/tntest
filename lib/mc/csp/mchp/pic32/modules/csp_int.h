/* *************************************************************************************************
Project:         MCHP 16-bit CPU Support Library
Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

 *************************************************************************************************
Distribution:

MCHP 16-bit CPU Support Library
----------------------------------------
Copyright � 2006-2008 Alex B.

 *************************************************************************************************
 MCU Family:      PIC24F/PIC24H/dsPIC33
Compiler:        Microchip C30 3.11

 *************************************************************************************************
File:            csp_int.h
Description:

 ********************************************************************************************** */

#ifndef __CSP_INT_H
#define __CSP_INT_H

#include "../cspi.h"

//-- these definitions are needed for parameter control for csp_int_....
//  TODO: implement it

#define CSP_INT_VECTOR__CSP_INT__INT_REQ_START    1000
#define CSP_INT_VECTOR__CSP_INT__INT_REQ_END      1999

#define CSP_INT_VECTOR__CSP_INT__INT_VECTOR_START 2000
#define CSP_INT_VECTOR__CSP_INT__INT_VECTOR_END   2999

//----------------------------------------------------------------------

#if defined(__32MX440F128H__)             \
   || defined(__32MX440F256H__)             \
   || defined(__32MX440F512H__)           \


/* Interrupt programming model */
/* --------------------------- */

typedef struct S_CSP_InterruptReg
{
   volatile UPWORD INTCON;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD INTSTAT;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD IPTMR;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD IFS0;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD IFS1;
   __CSP_SKIP_BYTE(12);

   __CSP_SKIP_BYTE(16);

   volatile UPWORD IEC0;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD IEC1;
   __CSP_SKIP_BYTE(12);

   __CSP_SKIP_BYTE(16);

   volatile UPWORD IPC0;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD IPC1;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD IPC2;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD IPC3;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD IPC4;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD IPC5;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD IPC6;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD IPC7;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD IPC8;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD IPC9;
   __CSP_SKIP_BYTE(12);

   __CSP_SKIP_BYTE(16);

   volatile UPWORD IPC11;

} T_CSP_InterruptReg;

/* Definitions for interrupt API */
/* ----------------------------- */

/* csp_int_ext_set() and csp_int_ext_set() parameters type */

enum E_CSP_ExternInterrupt {
   EINT0,      /* external interrupt INT0 */
   EINT1,      /* external interrupt INT1 */
   EINT2,      /* external interrupt INT2 */
   EINT3,      /* external interrupt INT3 */
   EINT4       /* external interrupt INT4 */
};

/* csp_int_en(), csp_int_dis() and other functions parameters type */

enum E_CSP_InterruptVector
{
#define CSP_INT_VECTOR__CORE_TIMER                       0
#define CSP_INT_VECTOR__CORE_SOFTWARE_0                  1
#define CSP_INT_VECTOR__CORE_SOFTWARE_1                  2
#define CSP_INT_VECTOR__EXTERNAL_0                       3
#define CSP_INT_VECTOR__TIMER_1                          4
#define CSP_INT_VECTOR__INPUT_CAPTURE_1                  5
#define CSP_INT_VECTOR__OUTPUT_COMPARE_1                 6
#define CSP_INT_VECTOR__EXTERNAL_1                       7
#define CSP_INT_VECTOR__TIMER_2                          8
#define CSP_INT_VECTOR__INPUT_CAPTURE_2                  9
#define CSP_INT_VECTOR__OUTPUT_COMPARE_2                 10
#define CSP_INT_VECTOR__EXTERNAL_2                       11
#define CSP_INT_VECTOR__TIMER_3                          12
#define CSP_INT_VECTOR__INPUT_CAPTURE_3                  13
#define CSP_INT_VECTOR__OUTPUT_COMPARE_3                 14
#define CSP_INT_VECTOR__EXTERNAL_3                       15
#define CSP_INT_VECTOR__TIMER_4                          16
#define CSP_INT_VECTOR__INPUT_CAPTURE_4                  17
#define CSP_INT_VECTOR__OUTPUT_COMPARE_4                 18
#define CSP_INT_VECTOR__EXTERNAL_4                       19
#define CSP_INT_VECTOR__TIMER_5                          20
#define CSP_INT_VECTOR__INPUT_CAPTURE_5                  21
#define CSP_INT_VECTOR__OUTPUT_COMPARE_5                 22
#define CSP_INT_VECTOR__UART_1                           24
#define CSP_INT_VECTOR__I2C_1                            25
#define CSP_INT_VECTOR__CHANGE_NOTICE                    26
#define CSP_INT_VECTOR__ADC                              27
#define CSP_INT_VECTOR__PMP                              28
#define CSP_INT_VECTOR__COMPARATOR_1                     29
#define CSP_INT_VECTOR__COMPARATOR_2                     30
#define CSP_INT_VECTOR__SPI_2                            31
#define CSP_INT_VECTOR__UART_2                           32
#define CSP_INT_VECTOR__I2C_2                            33
#define CSP_INT_VECTOR__FAIL_SAFE_MONITOR                34
#define CSP_INT_VECTOR__RTCC                             35
#define CSP_INT_VECTOR__DMA_0                            36
#define CSP_INT_VECTOR__DMA_1                            37
#define CSP_INT_VECTOR__DMA_2                            38
#define CSP_INT_VECTOR__DMA_3                            39
#define CSP_INT_VECTOR__FCE                              44
#define CSP_INT_VECTOR__USB_1                            45
   CSP_INT_VECTOR__DUMMY //-- needed to keep enum not empty
};

enum E_CSP_InterruptReq {
#define CSP_INT_REQ__CORE_TIMER                          0
#define CSP_INT_REQ__CORE_SOFTWARE_0                     1
#define CSP_INT_REQ__CORE_SOFTWARE_1                     2
#define CSP_INT_REQ__EXTERNAL_0                          3
#define CSP_INT_REQ__TIMER_1                             4
#define CSP_INT_REQ__INPUT_CAPTURE_1                     5
#define CSP_INT_REQ__OUTPUT_COMPARE_1                    6
#define CSP_INT_REQ__EXTERNAL_1                          7
#define CSP_INT_REQ__TIMER_2                             8
#define CSP_INT_REQ__INPUT_CAPTURE_2                     9
#define CSP_INT_REQ__OUTPUT_COMPARE_2                    10
#define CSP_INT_REQ__EXTERNAL_2                          11
#define CSP_INT_REQ__TIMER_3                             12
#define CSP_INT_REQ__INPUT_CAPTURE_3                     13
#define CSP_INT_REQ__OUTPUT_COMPARE_3                    14
#define CSP_INT_REQ__EXTERNAL_3                          15
#define CSP_INT_REQ__TIMER_4                             16
#define CSP_INT_REQ__INPUT_CAPTURE_4                     17
#define CSP_INT_REQ__OUTPUT_COMPARE_4                    18
#define CSP_INT_REQ__EXTERNAL_4                          19
#define CSP_INT_REQ__TIMER_5                             20
#define CSP_INT_REQ__INPUT_CAPTURE_5                     21
#define CSP_INT_REQ__OUTPUT_COMPARE_5                    22
#define CSP_INT_REQ__UART1_ERR                           26
#define CSP_INT_REQ__UART1_RX                            27
#define CSP_INT_REQ__UART1_TX                            28
#define CSP_INT_REQ__I2C1_COLLISION                      29
#define CSP_INT_REQ__I2C1_SLAVE                          30
#define CSP_INT_REQ__I2C1_MASTER                         31
#define CSP_INT_REQ__CHANGE_NOTICE                       32
#define CSP_INT_REQ__ADC                                 33
#define CSP_INT_REQ__PMP                                 34
#define CSP_INT_REQ__COMPARATOR_1                        35
#define CSP_INT_REQ__COMPARATOR_2                        36
#define CSP_INT_REQ__SPI2_ERR                            37
#define CSP_INT_REQ__SPI2_RX                             38
#define CSP_INT_REQ__SPI2_TX                             39
#define CSP_INT_REQ__UART2_ERR                           40
#define CSP_INT_REQ__UART2_RX                            41
#define CSP_INT_REQ__UART2_TX                            42
#define CSP_INT_REQ__I2C2_COLLISION                      43
#define CSP_INT_REQ__I2C2_SLAVE                          44
#define CSP_INT_REQ__I2C2_MASTER                         45
#define CSP_INT_REQ__FAIL_SAFE_MONITOR                   46
#define CSP_INT_REQ__RTCC                                47
#define CSP_INT_REQ__DMA0                                48
#define CSP_INT_REQ__DMA1                                49
#define CSP_INT_REQ__DMA2                                50
#define CSP_INT_REQ__DMA3                                51
#define CSP_INT_REQ__FLASH_CONTROL                       56
#define CSP_INT_REQ__USB                                 57
   CSP_INT_REQ__DUMMY //-- needed to keep enum not empty
};

#define  CSP_INT_CFG__MVEC_EN                (1 << 12)   /* Multi-vector mode is enabled */
#define  CSP_INT_CFG__MVEC_DIS               (0 << 12)   /* Multi-vector mode is disabled */

#define  CSP_INT_CFG__PROX_TIMER_DIS         (0 << 8)    /* Interrupt proximity timer is disabled */
#define  CSP_INT_CFG__PROX_TIMER_INT_PRI_1   (1 << 8)    /* Interrupts of group priority 1 start the Interrupt Proximity timer */
#define  CSP_INT_CFG__PROX_TIMER_INT_PRI_2   (2 << 8)    /* Interrupts of group priority 2 or lower start the Interrupt Proximity timer */
#define  CSP_INT_CFG__PROX_TIMER_INT_PRI_3   (3 << 8)    /* Interrupts of group priority 3 or lower start the Interrupt Proximity timer */
#define  CSP_INT_CFG__PROX_TIMER_INT_PRI_4   (4 << 8)    /* Interrupts of group priority 4 or lower start the Interrupt Proximity timer */
#define  CSP_INT_CFG__PROX_TIMER_INT_PRI_5   (5 << 8)    /* Interrupts of group priority 5 or lower start the Interrupt Proximity timer */
#define  CSP_INT_CFG__PROX_TIMER_INT_PRI_6   (6 << 8)    /* Interrupts of group priority 6 or lower start the Interrupt Proximity timer */
#define  CSP_INT_CFG__PROX_TIMER_INT_PRI_7   (7 << 8)    /* Interrupts of group priority 7 or lower start the Interrupt Proximity timer */

#define  CSP_INT_CFG__SINGLE_VECT_SHADOW_EN  (1 << 16)   /* Single vector is presented with a shadow register bit */
#define  CSP_INT_CFG__SINGLE_VECT_SHADOW_DIS (0 << 16)   /* Single vector is not presented with a shadow register bit */



#define  __CSP_INT_CFG_MASK                  ((1 << 12) | (7 << 8) | (1 << 16))


#else
#error chip is not supported by CSP Interrupt module
#endif



#if ((__CSP_FAMILY == __CSP_FAMILY_PIC32MX3XX_4XX)  \
    )

/* Interrupt base address */
/* ---------------------- */

#define INT_BASE_VIRT_ADDR    (0xbf880000 + 0x1000)

/* Declare interrupt model */
/* ----------------------- */

extern T_CSP_InterruptReg CSP_InterruptReg;

/* Internal API definitions */
/* ------------------------ */


/* Definitions for interrupt API */
/* ----------------------------- */

/* csp_int_trap_flag_set(),  csp_int_trap_flag_clr() and csp_int_trap_flag_check() parameters type*/

typedef enum __INT_TRAP
{
   INT_TRAP_OSCILLATOR =  1,           /* Oscillator Failure */
   INT_TRAP_STACK      =  2,           /* Stack error        */
   INT_TRAP_ADDRESS    =  3,           /* Address Error      */
   INT_TRAP_MATH       =  4            /* Math Error         */

} INT_TRAP;

/* csp_int_conf_set() and csp_int_conf_get() parameters mask */

#if 0
typedef enum __INT_CONF
{
   INT_NESTING_EN    = (1 << 0),       /* Enable nesting interrupts  */
   INT_NESTING_DIS   = (0),            /* Disable nesting interrupts */

   INT_ALT_TABLE_EN  = (1 << 1),       /* Enable alternative table  */
   INT_ALT_TABLE_DIS = (0),            /* Disable alternative table */

#if (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)
   INT_HOLD_HIGHEST  = (1 << 2),       /* VECNUM will contain the value of the highest priority pending interrupt, instead of the current interrupt */
   INT_HOLD_LAST     = (0),            /* VECNUM will contain the value of the last Acknowledged interrupt */
#endif

} INT_CONF;
#endif

/* csp_int_ext_set() and csp_int_ext_get() parameters type */

typedef enum __INT_EXT_POL
{
   INT_EXT_POL_POS,                /* Interrupt on rise edge    */
   INT_EXT_POL_NEG,                /* Interrupt on falling edge */
} INT_EXT_POL;

/* csp_int_vec_priority_set() and csp_int_vec_priority_get() parameters type */

/* see in csp.h */


/* Interrupt service routine qualifier */

#define CSP_ISR                 __attribute__((interrupt, nomips16, section(".isr")))

#endif

/* External functions prototypes */
/* ----------------------------- */

void        csp_int_conf_set(UPWORD val);
UPWORD       csp_int_conf_get(void);

void        csp_int_ext_set(enum E_CSP_ExternInterrupt ch, INT_EXT_POL pol);
INT_EXT_POL csp_int_ext_get(enum E_CSP_ExternInterrupt ch);

void        csp_int_vec_priority_set(enum E_CSP_InterruptVector int_vector, enum E_CSP_InterruptPri ipl);
enum E_CSP_InterruptPri     csp_int_vec_priority_get(enum E_CSP_InterruptVector int_vector);

void        csp_int_en(enum E_CSP_InterruptReq int_req);
void        csp_int_dis(enum E_CSP_InterruptReq int_req);
UPWORD       csp_int_is_enabled(enum E_CSP_InterruptReq int_req);

void        csp_int_flag_set(enum E_CSP_InterruptReq int_req);
void        csp_int_flag_clr(enum E_CSP_InterruptReq int_req);
UPWORD       csp_int_flag_check(enum E_CSP_InterruptReq int_req);

#if 0
#define     csp_int_flag_check(int_req)   (  \
      STATIC_ASSERT((int_req) >= __CSP_INT__INT_REQ_START), )

#define  
#endif


void        csp_int_trap_flag_set(INT_TRAP int_trap);
void        csp_int_trap_flag_clr(INT_TRAP int_trap);
UPWORD       csp_int_trap_flag_check(INT_TRAP int_trap);

#if !defined(__CSP_BUILD_LIB)

/* Module API definition */

#if defined(__CSP_DEBUG_INT)

//prototypes were here

#else

#include "csp_int.c"

#endif

#endif

#endif /* #ifndef __CSP_INT_H */
/* ********************************************************************************************** */
/* end of file: csp_int.h */
