/* *************************************************************************************************
Project:         MCHP 16-bit CPU Support Library
Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

 *************************************************************************************************
Distribution:

MCHP 16-bit CPU Support Library
----------------------------------------
Copyright � 2006-2008 Alex B.

 *************************************************************************************************
 MCU Family:      PIC24F/PIC24H/dsPIC33
Compiler:        Microchip C30 3.11

 *************************************************************************************************
File:            csp_oc_cget.c
Description:

 ********************************************************************************************** */

#include "csp_oc.h"

#include "../cspi.h"

#if defined(__CSP_BUILD_LIB)

#define _OC1_BASE_VIRT_ADDR         (0xBF800000 + 0x3000)
#define _OC2_BASE_VIRT_ADDR         (0xBF800000 + 0x3200)
#define _OC3_BASE_VIRT_ADDR         (0xBF800000 + 0x3400)
#define _OC4_BASE_VIRT_ADDR         (0xBF800000 + 0x3600)
#define _OC5_BASE_VIRT_ADDR         (0xBF800000 + 0x3800)

//#ifdef _OCMP1
T_CSP_OC CSP_OC1 __CSP_SFR((_OC1_BASE_VIRT_ADDR));
//#endif

//#ifdef _OCMP2
T_CSP_OC CSP_OC2 __CSP_SFR((_OC2_BASE_VIRT_ADDR));
//#endif

//#ifdef _OCMP3
T_CSP_OC CSP_OC3 __CSP_SFR((_OC3_BASE_VIRT_ADDR));
//#endif

//#ifdef _OCMP4
T_CSP_OC CSP_OC4 __CSP_SFR((_OC4_BASE_VIRT_ADDR));
//#endif

//#ifdef _OCMP5
T_CSP_OC CSP_OC5 __CSP_SFR((_OC5_BASE_VIRT_ADDR));
//#endif

#endif


__CSP_FUNC UPWORD  csp_oc_conf_get (T_CSP_OC *oc)
{
   return oc->OCCON;
}

__CSP_FUNC void  csp_oc_conf_set (T_CSP_OC *oc, UPWORD val)
{
   oc->OCCON = val;
}

__CSP_FUNC UPWORD  csp_oc_r_get (T_CSP_OC *oc)
{
   return (oc->OCR);
}

__CSP_FUNC void  csp_oc_r_set (T_CSP_OC *oc, UPWORD val)
{
   oc->OCR = val;
}

__CSP_FUNC UPWORD  csp_oc_rs_get (T_CSP_OC *oc)
{
   return (oc->OCRS);
}

__CSP_FUNC void  csp_oc_rs_set (T_CSP_OC *oc, UPWORD val)
{
   oc->OCRS = val;
}

__CSP_FUNC enum E_CSP_OS_Stat  csp_oc_stat_get (T_CSP_OC *oc)
{
   return (enum E_CSP_OS_Stat)__CSP_ACC_MASK_VAL(BFA_RD, oc->OCCON, (CSP_OC_STAT_FAULT));
}

/* ********************************************************************************************** */
/* end of file: csp_oc_tmst.c */
