/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_int_cfgt.c
     Description:

   ********************************************************************************************** */

#include "csp_int.h"

//#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
//#endif



#define __INT_PRI_MASK          (0x0007)

#if defined(__CSP_BUILD_LIB)
T_CSP_InterruptReg CSP_InterruptReg  __CSP_SFR((INT_BASE_VIRT_ADDR));
#endif




__CSP_FUNC UPWORD csp_int_conf_get (void)
{
   return __CSP_ACC_MASK_REG(BFA_RD, CSP_InterruptReg.INTCON, __CSP_INT_CFG_MASK);
}

__CSP_FUNC void  csp_int_conf_set (UPWORD val)
{
   __CSP_ACC_MASK_REG(BFA_WR, CSP_InterruptReg.INTCON, __CSP_INT_CFG_MASK, val);
}

__CSP_FUNC UPWORD  csp_int_is_enabled (enum E_CSP_InterruptReq int_req)
{
    volatile UPWORD *iec_reg;
    UPWORD bit;

    iec_reg = &CSP_InterruptReg.IEC0 + (((int_req) >> 5/* / (sizeof(UPWORD))*/) << 2/* * 4 (because of __CSP_SKIP_BYTE(12)) */);
    bit    = (int_req) & 0x1f;            //-- % 32

    return __CSP_BIT_VAL_GET(*iec_reg, bit);
}


__CSP_FUNC void  csp_int_dis (enum E_CSP_InterruptReq int_req)
{
    volatile UPWORD *iec_reg;
    UPWORD bit;

    iec_reg = &CSP_InterruptReg.IEC0 + (((int_req) >> 5/* / (sizeof(UPWORD))*/) << 2/* * 4 (because of __CSP_SKIP_BYTE(12)) */);
    bit    = (int_req) & 0x1f;

    __CSP_CLR_REG(iec_reg) = (1 << bit);
    //__CSP_BIT_VAL_CLR(*iec_reg, bit);
}


__CSP_FUNC void  csp_int_en (enum E_CSP_InterruptReq int_req)
{
    volatile UPWORD *iec_reg;
    UPWORD bit;

    iec_reg = &CSP_InterruptReg.IEC0 + (((int_req) >> 5/* / (sizeof(UPWORD))*/) << 2/* * 4 (because of __CSP_SKIP_BYTE(12)) */);
    bit    = (int_req) & 0x1f;

    __CSP_SET_REG(iec_reg) = (1 << bit);
    //__CSP_BIT_VAL_SET(*iec_reg, bit);
}


#if 0
__CSP_FUNC INT_EXT_POL  csp_int_ext_get (enum E_CSP_ExternInterrupt ch)
{
    return (INT_EXT_POL)__CSP_BIT_REG_GET(CSP_InterruptReg.INTCON1, ch);
}


__CSP_FUNC void  csp_int_ext_set (enum E_CSP_ExternInterrupt ch, INT_EXT_POL pol)
{
    __CSP_ACC_RANG_REG(BFA_WR, CSP_InterruptReg.INTCON2, ch, ch, pol);
}
#endif


__CSP_FUNC UPWORD  csp_int_flag_check (enum E_CSP_InterruptReq int_req)
{
    volatile UPWORD *ifs_reg;
    UPWORD bit;

    ifs_reg = &CSP_InterruptReg.IFS0 + (((int_req) >> 5/* / (sizeof(UPWORD))*/) << 2/* * 4 (because of __CSP_SKIP_BYTE(12)) */);
    bit     = (int_req) & 0x1f;

    return __CSP_BIT_VAL_GET(*ifs_reg, bit);
}


__CSP_FUNC void  csp_int_flag_clr (enum E_CSP_InterruptReq int_req)
{
    volatile UPWORD *ifs_reg;
    UPWORD bit;

    ifs_reg = &CSP_InterruptReg.IFS0 + (((int_req) >> 5/* / (sizeof(UPWORD))*/) << 2/* * 4 (because of __CSP_SKIP_BYTE(12)) */);
    bit     = (int_req) & 0x1f;

    __CSP_CLR_REG(ifs_reg) = (1 << bit);


    //__CSP_BIT_VAL_CLR(*ifs_reg, bit);
}


__CSP_FUNC void  csp_int_flag_set (enum E_CSP_InterruptReq int_req)
{
    volatile UPWORD *ifs_reg;
    UPWORD bit;

    ifs_reg = &CSP_InterruptReg.IFS0 + (((int_req) >> 5/* / (sizeof(UPWORD))*/) << 2/* * 4 (because of __CSP_SKIP_BYTE(12)) */);
    bit    = (int_req) & 0x1f;

    __CSP_SET_REG(ifs_reg) = (1 << bit);
    //__CSP_BIT_VAL_SET(*ifs_reg, bit);
}


__CSP_FUNC enum E_CSP_InterruptPri  csp_int_vec_priority_get (enum E_CSP_InterruptVector int_vec)
{
    volatile UPWORD *ipc_reg;
    UPWORD sh;

    ipc_reg = &CSP_InterruptReg.IPC0 + (((int_vec) >> 2/* / (4)*/) << 2/* * 4 (because of __CSP_SKIP_BYTE(12)) */);
    sh      = ((int_vec) & 0x03/* % 4*/) * 8 + 2;

    return __CSP_ACC_MASK_IND(BFA_RD, ipc_reg, (__INT_PRI_MASK << sh)) >> sh;
}


__CSP_FUNC void  csp_int_vec_priority_set (enum E_CSP_InterruptVector int_vec, enum E_CSP_InterruptPri ipl)
{
    volatile UPWORD *ipc_reg;
    UPWORD sh;

    ipc_reg = &CSP_InterruptReg.IPC0 + (((int_vec) >> 2/* / (4)*/) << 2/* * 4 (because of __CSP_SKIP_BYTE(12)) */);
    sh      = ((int_vec) & 0x03/* % 4*/) * 8 + 2;
    
    __CSP_ACC_MASK_IND(BFA_WR, ipc_reg, (__INT_PRI_MASK << sh), (ipl << sh));
}


#if 0
__CSP_FUNC BOOL  csp_int_trap_flag_check (INT_TRAP int_trap)
{
    volatile UPWORD *ifs_reg;
    UPWORD mask;

    ifs_reg = &CSP_InterruptReg.INTCON1 + (int_trap / 16);
    mask    = int_trap % 16;

    return __CSP_BIT_VAL_GET(*ifs_reg, mask);
}


__CSP_FUNC void  csp_int_trap_flag_clr (INT_TRAP int_trap)
{
    volatile UPWORD *ifs_reg;
    UPWORD mask;

    ifs_reg = &CSP_InterruptReg.INTCON1 + (int_trap / 16);
    mask    = int_trap % 16;

    __CSP_BIT_VAL_CLR(*ifs_reg, mask);
}


__CSP_FUNC void  csp_int_trap_flag_set (INT_TRAP int_trap)
{
    volatile UPWORD *ifs_reg;
    UPWORD mask;

    ifs_reg = &CSP_InterruptReg.INTCON1 + (int_trap / 16);
    mask    = int_trap % 16;

    __CSP_BIT_VAL_SET(*ifs_reg, mask);
}
#endif

