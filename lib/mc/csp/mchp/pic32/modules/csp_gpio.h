/* *************************************************************************************************
Project:         MCHP 16-bit CPU Support Library
Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

 *************************************************************************************************
Distribution:

MCHP 16-bit CPU Support Library
----------------------------------------
Copyright � 2006-2008 Alex B.

 *************************************************************************************************
 MCU Family:      PIC24F/PIC24H/dsPIC33
Compiler:        Microchip C30 3.11

 *************************************************************************************************
File:            csp_gpio.h
Description:

 ********************************************************************************************** */

#ifndef __CSP_GPIO_H
#define __CSP_GPIO_H

#include "../cspi.h"

#if (__CSP_FAMILY == __CSP_FAMILY_PIC32MX3XX_4XX)

/* GPIO programming model */
/* ---------------------- */

typedef struct __GPIO_TYPE
{
   volatile UPWORD TRIS;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD PORT;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD LAT;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD ODC;
} T_CSP_GPIO;

/* GPIO base address and GPIO model declare */
/* ---------------------------------------- */
#define GPIO_BASE_VIRT_ADDR     0xbf880000

#define GPIO_B_BASE_VIRT_ADDR        (GPIO_BASE_VIRT_ADDR + 0x6040)
#define GPIO_C_BASE_VIRT_ADDR        (GPIO_BASE_VIRT_ADDR + 0x6080)
#define GPIO_D_BASE_VIRT_ADDR        (GPIO_BASE_VIRT_ADDR + 0x60C0)
#define GPIO_E_BASE_VIRT_ADDR        (GPIO_BASE_VIRT_ADDR + 0x6100)
#define GPIO_F_BASE_VIRT_ADDR        (GPIO_BASE_VIRT_ADDR + 0x6140)
#define GPIO_G_BASE_VIRT_ADDR        (GPIO_BASE_VIRT_ADDR + 0x6180)

extern  T_CSP_GPIO CSP_PortB;
extern  T_CSP_GPIO CSP_PortC;
extern  T_CSP_GPIO CSP_PortD;
extern  T_CSP_GPIO CSP_PortE;
extern  T_CSP_GPIO CSP_PortF;
extern  T_CSP_GPIO CSP_PortG;

#if ((__CSP_PIN_COUNT == __CSP_PIN_COUNT_80)  ||  \
      (__CSP_PIN_COUNT == __CSP_PIN_COUNT_100)     \
    )

#define GPIO_A_BASE_VIRT_ADDR        0x6000
extern  T_CSP_GPIO CSP_PortA __CSP_SFR((GPIO_A_BASE_VIRT_ADDR));
#endif

#endif


#if (0                                                         \
      || (__CSP_FAMILY == __CSP_FAMILY_PIC32MX3XX_4XX)         \
      || (__CSP_FAMILY == __CSP_FAMILY_PIC32MX1XX_2XX)         \
    )

//TODO: PAD
#if 0
/* PAD programming model */
/* --------------------- */

typedef struct __PAD_TYPE
{
   volatile UPWORD PADCFG1;
} PAD_TYPE;

/* PAD base address */
/* ---------------- */

#define PAD_BASE_VIRT_ADDR       0x02FC

/* Declare PAD model */
/* ----------------- */

extern  PAD_TYPE PAD __CSP_SFR((PAD_BASE_VIRT_ADDR));
#endif

/* Internal API definitions */
/* ------------------------ */

/* Definitions for GPIO API */
/* ------------------------ */

/* csp_gpio_pin_set(), csp_gpio_pin_clr() and other functions parameters type */

enum E_CSP_GPIO_Pin {
   CSP_GPIO_0,
   CSP_GPIO_1,
   CSP_GPIO_2,
   CSP_GPIO_3,
   CSP_GPIO_4,
   CSP_GPIO_5,
   CSP_GPIO_6,
   CSP_GPIO_7,
   CSP_GPIO_8,
   CSP_GPIO_9,
   CSP_GPIO_10,
   CSP_GPIO_11,
   CSP_GPIO_12,
   CSP_GPIO_13,
   CSP_GPIO_14,
   CSP_GPIO_15,
};

/* csp_gpio_pin_dir_get() return type */

enum E_CSP_GPIO_Dir {
   GPIO_DIR_OUT,
   GPIO_DIR_IN
};

/* csp_gpio_pin_od_get() return type */

enum E_CSP_GPIO_Mode
{
   GPIO_MODE_NORMAL,
   GPIO_MODE_OD
};

//TODO: pad
#if 0
/* csp_gpio_pad_conf_set() and csp_gpio_pad_conf_set() parameters macro */

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC24FJ128GA010) ||  \
      (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ64GA004)  ||  \
      (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GA110) ||  \
      (__CSP_FAMILY == __CSP_FAMILY_PIC24FJ256GB110)     \
    )

typedef enum __GPIO_PAD_CONF
{
   GPIO_PAD_PMP_IN_TTL     = (1 << 0),     /* PADCFG1<0> */
   GPIO_PAD_PMP_IN_SCHM    = (0),

   GPIO_PAD_RTCC_OUT_CLK   = (1 << 1),     /* PADCFG1<1> */
   GPIO_PAD_RTCC_OUT_ALARM = (0)
} GPIO_PAD_CONF;

#elif (__CSP_FAMILY == __CSP_FAMILY_PIC24F16KA102)

typedef enum __GPIO_PAD_CONF
{
   /* PADCFG1<4> */
   GPIO_PAD_I2C_SMBUS      = (1 << 4),     /* The I2C module is configured for a longer SMBus input delay (nominal 300 ns delay)   */
   GPIO_PAD_I2C_NORMAL     = (0 << 4),     /* The I2C module is configured for a legacy input delay (nominal 150 ns delay)         */

   /* PADCFG1<3> */
   GPIO_PAD_OC1_DIS        = (1 << 3),     /* OC1 output will not be active on the pin; OCPWM1 can still be used for internal triggers  */
   GPIO_PAD_OC1_EN         = (0 << 3),     /* OC1 output will be active on the pin based on the OCPWM1 module settings     */

   /* PADCFG1<1:2> */
   GPIO_PAD_RTCC_OUT_SRC   = (2 << 1),     /* RTCC source clock is selected for the RTCC pin (can be LPRC or SOSC) */
   GPIO_PAD_RTCC_OUT_SEC   = (1 << 1),     /* RTCC seconds clock is selected for the RTCC pin  */
   GPIO_PAD_RTCC_OUT_ALARM = (0 << 1),     /* RTCC alarm pulse is selected for the RTCC pin    */
} GPIO_PAD_CONF;
#endif

#endif

#endif

/*
Definition of pair PORT-PIN

Instruction: usually, when you write BSP code you used following definition:

#define FOO_PORT        &PORTA
#define FOO_PIN         GPIO_1

then you used it like this:

csp_gpio_pin_dir_in(FOO_PORT, FOO_PIN);

Instead of repeat FOO, you can use more readable:

csp_gpio_pin_dir_in(CSP_GPIO_DEF(FOO));
*/
#define CSP_GPIO_DEF(a)                  a##_PORT, a##_PIN


/* External functions prototypes */
/* ----------------------------- */

void      csp_gpio_pin_od_en(T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin);
void      csp_gpio_pin_od_dis(T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin);
enum E_CSP_GPIO_Mode csp_gpio_pin_od_get(T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin);

void      csp_gpio_pin_dir_in(T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin);
void      csp_gpio_pin_dir_out(T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin);
void      csp_gpio_pin_dir_tgl(T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin);
enum E_CSP_GPIO_Dir  csp_gpio_pin_dir_get(T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin);

void      csp_gpio_pin_set(T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin);
void      csp_gpio_pin_clr(T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin);
void      csp_gpio_pin_tgl(T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin);
UPWORD     csp_gpio_pin_check(T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin);



void      csp_gpio_port_od_en(T_CSP_GPIO *gpio, UPWORD mask);
void      csp_gpio_port_od_dis(T_CSP_GPIO *gpio, UPWORD mask);
UPWORD     csp_gpio_port_od_get(T_CSP_GPIO *gpio);

void      csp_gpio_port_dir_in(T_CSP_GPIO *gpio, UPWORD mask);
void      csp_gpio_port_dir_out(T_CSP_GPIO *gpio, UPWORD mask);
void      csp_gpio_port_dir_tgl(T_CSP_GPIO *gpio, UPWORD mask);
UPWORD     csp_gpio_port_dir_get(T_CSP_GPIO *gpio);

void      csp_gpio_port_set(T_CSP_GPIO *gpio, UPWORD mask);
void      csp_gpio_port_clr(T_CSP_GPIO *gpio, UPWORD mask);
void      csp_gpio_port_tgl(T_CSP_GPIO *gpio, UPWORD mask);
UPWORD     csp_gpio_port_get(T_CSP_GPIO *gpio);
void      csp_gpio_port_wr(T_CSP_GPIO *gpio, UPWORD val);

//TODO: pad
#if 0
void          csp_gpio_pad_conf_set(PAD_TYPE *pad, GPIO_PAD_CONF val);
GPIO_PAD_CONF csp_gpio_pad_conf_get(PAD_TYPE *pad);
#endif

#if !defined(__CSP_BUILD_LIB)

/* Module API definition */

#if defined(__CSP_DEBUG_GPIO)

//-- prototypes were here

#else

#include "csp_gpio.c"

#endif

#endif

#endif /* #ifndef __CSP_GPIO_H */
/* ********************************************************************************************** */
/* end of file: csp_gpio.h */
