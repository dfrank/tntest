/* *************************************************************************************************
Project:         MCHP 16-bit CPU Support Library
Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

 *************************************************************************************************
Distribution:

MCHP 16-bit CPU Support Library
----------------------------------------
Copyright � 2006-2008 Alex B.

 *************************************************************************************************
 MCU Family:      PIC24F/PIC24H/dsPIC33
Compiler:        Microchip C30 3.11

 *************************************************************************************************
File:            csp_cn.h
Description:

 ********************************************************************************************** */

#ifndef __CSP_CN_H
#define __CSP_CN_H


#include "../cspi.h"

#if ((__CSP_FAMILY == __CSP_FAMILY_PIC32MX3XX_4XX) \
    )

/* CN programming model */
/* -------------------- */

typedef struct S_CSP_CN
{
   volatile UPWORD CNCON;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD CNEN;
   __CSP_SKIP_BYTE(12);
   volatile UPWORD CNPUE;
} T_CSP_CN;

#define CN_BASE_VIRT_ADDR       (0xbf880000 + 0x61c0)
extern T_CSP_CN  CSP_CN;

enum E_CSP_CN_Pin
{
   CSP_CN_PIN0  = (1 <<  0),
   CSP_CN_PIN1  = (1 <<  1),
   CSP_CN_PIN2  = (1 <<  2),
   CSP_CN_PIN3  = (1 <<  3),
   CSP_CN_PIN4  = (1 <<  4),
   CSP_CN_PIN5  = (1 <<  5),
   CSP_CN_PIN6  = (1 <<  6),
   CSP_CN_PIN7  = (1 <<  7),
   CSP_CN_PIN8  = (1 <<  8),
   CSP_CN_PIN9  = (1 <<  9),
   CSP_CN_PIN10 = (1 << 10),
   CSP_CN_PIN11 = (1 << 11),
   CSP_CN_PIN12 = (1 << 12),
   CSP_CN_PIN13 = (1 << 13),
   CSP_CN_PIN14 = (1 << 14),
   CSP_CN_PIN15 = (1 << 15),
   CSP_CN_PIN16 = (1 << 16),
   CSP_CN_PIN17 = (1 << 17),
   CSP_CN_PIN18 = (1 << 18),
#if ((0)        \
      || defined(__32MX320F128L__)      \
      || defined(__32MX340F128L__)      \
      || defined(__32MX360F256L__)      \
      || defined(__32MX360F512L__)      \
      || defined(__32MX440F128L__)      \
      || defined(__32MX460F256L__)      \
      || defined(__32MX460F512L__)      \
    )
   CSP_CN_PIN19 = (1 << 19),
   CSP_CN_PIN20 = (1 << 20),
   CSP_CN_PIN21 = (1 << 21),
#endif
};

#define CSP_CN_PIN_ALL  (0xffff)


/* Definitions for CN API */
/* ------------------------ */

//-- for csp_cn_conf() {{{

#define CSP_CN_EN                   (1 << 15) /* CN is enabled */
#define CSP_CN_DIS                  (0 << 15) /* CN is enabled */

#define CSP_CN_IDLE_STOP            (1 << 13) /* Discontinue operation when device enters Idle mode */
#define CSP_CN_IDLE_CON             (0 << 13) /* Continue operation in Idle mode */

// }}}


#else
"CN module type wrong definition"
#endif


/* External functions prototypes */
/* ----------------------------- */

void     csp_cn_conf (T_CSP_CN *p_cn, UPWORD val);

void     csp_cn_en(T_CSP_CN *p_cn, enum E_CSP_CN_Pin mask);
void     csp_cn_dis(T_CSP_CN *p_cn, enum E_CSP_CN_Pin mask);
void     csp_cn_wr(T_CSP_CN *p_cn, enum E_CSP_CN_Pin val);
enum E_CSP_CN_Pin csp_cn_get(T_CSP_CN *p_cn);

void     csp_cn_pullup_en(T_CSP_CN *p_cn, enum E_CSP_CN_Pin mask);
void     csp_cn_pullup_dis(T_CSP_CN *p_cn, enum E_CSP_CN_Pin mask);
void     csp_cn_pullup_wr(T_CSP_CN *p_cn, enum E_CSP_CN_Pin val);
enum E_CSP_CN_Pin csp_cn_pullup_get(T_CSP_CN *p_cn);

#if !defined(__CSP_BUILD_LIB)

   /* Module API definition */

#if   defined(__CSP_DEBUG_CN)

   //-- prototypes were here

#else

#include "csp_cn.c"

#endif

#endif

#endif /* #ifndef __CSP_CN_H */
   /* ********************************************************************************************** */
   /* end of file: csp_cn.h */
