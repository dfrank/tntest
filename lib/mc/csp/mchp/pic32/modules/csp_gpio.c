/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_pchk.c
     Description:

   ********************************************************************************************** */

#include "csp_gpio.h"

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif


#if defined(__CSP_BUILD_LIB)
T_CSP_GPIO CSP_PortB __CSP_SFR((GPIO_B_BASE_VIRT_ADDR));
T_CSP_GPIO CSP_PortC __CSP_SFR((GPIO_C_BASE_VIRT_ADDR));
T_CSP_GPIO CSP_PortD __CSP_SFR((GPIO_D_BASE_VIRT_ADDR));
T_CSP_GPIO CSP_PortE __CSP_SFR((GPIO_E_BASE_VIRT_ADDR));
T_CSP_GPIO CSP_PortF __CSP_SFR((GPIO_F_BASE_VIRT_ADDR));
T_CSP_GPIO CSP_PortG __CSP_SFR((GPIO_G_BASE_VIRT_ADDR));
#endif



__CSP_FUNC UPWORD  csp_gpio_pin_check (T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin)
{
    return __CSP_BIT_VAL_GET(gpio->PORT, pin);
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_pchk.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_pclr.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_pin_clr (T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin)
{
   __CSP_CLR_REG(&gpio->LAT) = (1 << pin);
    //__CSP_BIT_VAL_CLR(gpio->LAT, pin);
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_pclr.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_pdcg.c
     Description:

   ********************************************************************************************** */

//TODO: pad
#if 0
#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC GPIO_PAD_CONF csp_gpio_pad_conf_get (PAD_TYPE *pad)
{
    return pad->PADCFG1;
}


/* ********************************************************************************************** */
/* end of file: csp_gpio_pdcg.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_pdcs.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void csp_gpio_pad_conf_set (PAD_TYPE *pad, GPIO_PAD_CONF val)
{
    pad->PADCFG1 = val;
}
#endif

/* ********************************************************************************************** */
/* end of file: csp_gpio_pdcs.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_pdgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC enum E_CSP_GPIO_Dir  csp_gpio_pin_dir_get (T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin)
{
    return (enum E_CSP_GPIO_Dir)(__CSP_BIT_VAL_GET(gpio->TRIS, pin));
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_pdgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_pdin.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_pin_dir_in (T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin)
{
   __CSP_SET_REG(&gpio->TRIS) = (1 << pin);
    //__CSP_BIT_VAL_SET(gpio->TRIS, pin);
}


/* ********************************************************************************************** */
/* end of file: csp_gpio_pdin.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_pdou.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_pin_dir_out (T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin)
{
   __CSP_CLR_REG(&gpio->TRIS) = (1 << pin);
    //__CSP_BIT_VAL_CLR(gpio->TRIS, pin);
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_pdou.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_pdtg.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_pin_dir_tgl (T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin)
{
   __CSP_INV_REG(&gpio->TRIS) = (1 << pin);
    //__CSP_ACC_RANG_VAL(BFA_INV, gpio->TRIS, pin, pin, 1);
   //{
      //volatile unsigned int *addrpt;                                      
         //addrpt = (unsigned int*)&gpio->TRIS;                                       
      //__sync_fetch_and_xor((addrpt), 1);        
   //}
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_pdtg.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_pods.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_pin_od_dis (T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin)
{
   __CSP_CLR_REG(&gpio->ODC) = (1 << pin);
    //__CSP_BIT_VAL_CLR(gpio->ODC, pin);
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_pods.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_poen.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_pin_od_en (T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin)
{
   __CSP_SET_REG(&gpio->ODC) = (1 << pin);
    //__CSP_BIT_VAL_SET(gpio->ODC, pin);
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_poen.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_pogt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif
__CSP_FUNC enum E_CSP_GPIO_Mode  csp_gpio_pin_od_get (T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin)
{
    return (enum E_CSP_GPIO_Mode)(__CSP_BIT_VAL_GET(gpio->ODC, pin));
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_pogt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_pset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_pin_set (T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin)
{
   __CSP_SET_REG(&gpio->LAT) = (1 << pin);
    //__CSP_BIT_VAL_SET(gpio->LAT, pin);
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_pset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_ptgl.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_pin_tgl (T_CSP_GPIO *gpio, enum E_CSP_GPIO_Pin pin)
{
   __CSP_INV_REG(&gpio->LAT) = (1 << pin);
    //__CSP_ACC_RANG_VAL(BFA_INV, gpio->LAT, pin, pin, 1);
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_ptgl.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_tclr.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_port_clr (T_CSP_GPIO *gpio, UPWORD mask)
{
   __CSP_CLR_REG(&gpio->LAT) = mask;
    //__CSP_ACC_MASK_VAL(BFA_CLR, gpio->LAT, mask, 0xFFFF);
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_tclr.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_tdgt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UPWORD  csp_gpio_port_dir_get (T_CSP_GPIO *gpio)
{
    return gpio->TRIS;
}


/* ********************************************************************************************** */
/* end of file: csp_gpio_tdgt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_tdin.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_port_dir_in (T_CSP_GPIO *gpio, UPWORD mask)
{
   __CSP_SET_REG(&gpio->TRIS) = mask;
    //__CSP_ACC_MASK_VAL(BFA_SET, gpio->TRIS, mask, UINT_MAX);
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_tdin.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_tdou.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_port_dir_out (T_CSP_GPIO *gpio, UPWORD mask)
{
   __CSP_CLR_REG(&gpio->TRIS) = mask;
    //__CSP_ACC_MASK_VAL(BFA_CLR, gpio->TRIS, mask, UINT_MAX);
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_tdou.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_tdtg.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_port_dir_tgl (T_CSP_GPIO *gpio, UPWORD mask)
{
   __CSP_INV_REG(&gpio->TRIS) = mask;
    //__CSP_ACC_MASK_VAL(BFA_INV, gpio->TRIS, mask, UINT_MAX);
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_tdtg.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_tget.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UPWORD  csp_gpio_port_get (T_CSP_GPIO *gpio)
{
    return gpio->PORT;
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_tget.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_tods.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_port_od_dis (T_CSP_GPIO *gpio, UPWORD mask)
{
   __CSP_CLR_REG(&gpio->ODC) = mask;
    //__CSP_ACC_MASK_VAL(BFA_CLR, gpio->ODC, mask, UINT_MAX);
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_tods.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_toen.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_port_od_en (T_CSP_GPIO *gpio, UPWORD mask)
{
   __CSP_SET_REG(&gpio->ODC) = mask;
    //__CSP_ACC_MASK_VAL(BFA_SET, gpio->ODC, mask, UINT_MAX);
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_toen.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_togt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC UPWORD  csp_gpio_port_od_get (T_CSP_GPIO *gpio)
{
    return gpio->ODC;
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_togt.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_tset.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_port_set (T_CSP_GPIO *gpio, UPWORD mask)
{
   __CSP_SET_REG(&gpio->LAT) = mask;
    //__CSP_ACC_MASK_VAL(BFA_SET, gpio->LAT, mask, UINT_MAX);
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_tset.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_ttgl.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_port_tgl (T_CSP_GPIO *gpio, UPWORD mask)
{
   __CSP_INV_REG(&gpio->LAT) = mask;
    //__CSP_ACC_MASK_VAL(BFA_INV, gpio->LAT, mask, UINT_MAX);
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_ttgl.c */
/* *************************************************************************************************
     Project:         MCHP 16-bit CPU Support Library
     Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

   *************************************************************************************************
     Distribution:

     MCHP 16-bit CPU Support Library
     ----------------------------------------
     Copyright � 2006-2008 Alex B.

   *************************************************************************************************
     MCU Family:      PIC24F/PIC24H/dsPIC33
     Compiler:        Microchip C30 3.11

   *************************************************************************************************
     File:            csp_gpio_twrt.c
     Description:

   ********************************************************************************************** */

#if defined(__CSP_BUILD_LIB)
    #include "../cspi.h"
#endif

__CSP_FUNC void  csp_gpio_port_wr (T_CSP_GPIO *gpio, UPWORD val)
{
    gpio->LAT = val;
}

/* ********************************************************************************************** */
/* end of file: csp_gpio_twrt.c */
