/* *************************************************************************************************
Project:         MCHP 16-bit CPU Support Library
Author:          [GS] Alex B. (�) 2008, http://wiki.pic24.ru

 *************************************************************************************************
Distribution:

MCHP 16-bit CPU Support Library
----------------------------------------
Copyright � 2006-2008 Alex B.

 *************************************************************************************************
 MCU Family:      PIC24F/PIC24H/dsPIC33
Compiler:        Microchip C30 3.11

 *************************************************************************************************
File:            csp_uart_bget.c
Description:

 ********************************************************************************************** */



/* Internal API definitions */
/* ------------------------ */

#define __UART_WAKEUP_BIT       (7)
#define __UART_AUTOBAUD_BIT     (5)
#define __UART_BREAK_BIT        (11)

#define __UART_STAT_MASK        (CSP_UART_STAT_TX_BUF_FULL | CSP_UART_STAT_TX_SREG_EMPTY |  \
      CSP_UART_STAT_RX_IDLE | CSP_UART_STAT_RX_BUF_NOT_EMPTY |   \
      CSP_UART_STAT_ERR_PARITY | CSP_UART_STAT_ERR_FRAME |       \
      CSP_UART_STAT_ERR_OVF)



#if defined(__CSP_BUILD_LIB)
#include "../cspi.h"
#endif

#if defined(__CSP_BUILD_LIB)
T_CSP_Uart CSP_UART1 __CSP_SFR((UART1_BASE_VIRT_ADDR));
T_CSP_Uart CSP_UART2 __CSP_SFR((UART2_BASE_VIRT_ADDR));
#endif

#include "csp_uart.h"

__CSP_FUNC UPWORD  csp_uart_brg_get (T_CSP_Uart *uart)
{
   return (uart->UBRG);
}


__CSP_FUNC void  csp_uart_brg_set (T_CSP_Uart *uart, UPWORD val)
{
   uart->UBRG = val;
}

__CSP_FUNC void  csp_uart_baud_set_auto (T_CSP_Uart *uart, UPWORD periph_bus_clk_freq, UPWORD baudrate)
{
   if (csp_uart_conf_get(uart) & CSP_UART_HIGH_BAUD_EN){
      uart->UBRG = (CSP_INT_DIV((periph_bus_clk_freq >> 2), baudrate)) - 1;
   } else {
      uart->UBRG = (CSP_INT_DIV((periph_bus_clk_freq >> 4), baudrate)) - 1;
   }
}


__CSP_FUNC U64  csp_uart_conf_get (T_CSP_Uart *uart)
{
   AL_EXT_64 ret;

   ret.word_0 = uart->USTA;
   ret.word_1 = uart->UMODE;

   return ret.full;
}


__CSP_FUNC void  csp_uart_comm (T_CSP_Uart *uart, enum E_CSP_UartCommand comm)
{
   switch (comm)
   {
      case CSP_UART_COMM_WAKEUP:
         __CSP_SET_REG(&uart->UMODE) = (1 << __UART_WAKEUP_BIT);
         //__CSP_BIT_VAL_SET(uart->UMODE, __UART_WAKEUP_BIT);
         break;
      case CSP_UART_COMM_AUTOBAUD:
         __CSP_SET_REG(&uart->UMODE) = (1 << __UART_AUTOBAUD_BIT);
         //__CSP_BIT_VAL_SET(uart->UMODE, __UART_AUTOBAUD_BIT);
         break;
      case CSP_UART_COMM_BREAK:
         __CSP_SET_REG(&uart->UMODE) = (1 << __UART_BREAK_BIT);
         //__CSP_BIT_VAL_SET(uart->UMODE, __UART_BREAK_BIT);
         uart->UTXREG = 0;
         break;
   }
}


__CSP_FUNC void  csp_uart_conf_set (T_CSP_Uart *uart, U64 val)
{
   uart->UMODE = ((AL_EXT_64)val).word_1;
   uart->USTA  = ((AL_EXT_64)val).word_0;
}


__CSP_FUNC UPWORD  csp_uart_get (T_CSP_Uart *uart)
{
   return (uart->URXREG);
}


__CSP_FUNC void  csp_uart_put (T_CSP_Uart *uart, UPWORD val)
{
   uart->UTXREG = val;
}


__CSP_FUNC void  csp_uart_stat_clr (T_CSP_Uart *uart, enum E_CSP_UartStat mask)
{
   __CSP_CLR_REG(&uart->USTA) = mask;
   //__CSP_ACC_MASK_VAL(BFA_CLR, uart->USTA, __UART_STAT_MASK, mask);
}


__CSP_FUNC enum E_CSP_UartStat  csp_uart_stat_get (T_CSP_Uart *uart)
{
   return (enum E_CSP_UartStat)__CSP_ACC_MASK_VAL(BFA_RD, uart->USTA, __UART_STAT_MASK);
}


