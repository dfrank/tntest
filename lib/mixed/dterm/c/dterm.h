/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

#ifndef _DTERM_H
#define _DTERM_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include "dterm_types.h"
#include "bsp_dterm.h"

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

typedef U08 T_DTerm_MsgType;
enum E_DTerm_MsgType {
   DTERM_MSG_TYPE__TEXT,
   DTERM_MSG_TYPE__FILE,
   DTERM_MSG_TYPE__DATA,
   DTERM_MSG_TYPES_CNT,
};
#define DTERM_MSG_TYPE__NONE  DTERM_MSG_TYPES_CNT

typedef struct S_DTermRxBuf {
   U16 len;
   U08 data[ DTERM__RX_BUF_SIZE ];
} T_DTermRxBuf;

//typedef void (*T_pDTerm_RxHandler)(const U08 *p_data, U08 len);
typedef void (*T_pDTerm_RxHandler)(const T_DTermRxBuf *p_rx_buf);




enum E_DTerm_OutputSett {
   /*
    * "async" flag:
    *    if set, transmission happens asynchronously,
    *    by means of fifo tx buffer (size: DTERM__TX_BUF_SIZE)
    *
    *    if not set, transmission happens synchronously,
    *    waiting for all the data to be sent.
    */
   DTERM_SETT_FLAG__ASYNC = (1 << 0),

   /*
    * "wait" flag: 
    *    if "async" is set:                                     
    *       whether we should wait if fifo tx buffer is full 
    *    if "async" is not set:                                 
    *       before start synchronous transmission,
    *       whether we should wait until fifo tx buffer becomes empty                                    
    *
    */
   DTERM_SETT_FLAG__WAIT = (1 << 1),

   /*
    * "weak" flag:
    *    used if only "async" is set and "wait" is not,
    *    i.e., this is async_nowait mode.
    *
    *    If set, and there's no room in fifo tx buffer,
    *    new data is lost.
    *
    *    If not set, and there's no room in fifo tx buffer,
    *    old data is overwritten by new data (old data is lost)
    */
   DTERM_SETT_FLAG__WEAK = (1 << 2),

   /*
    * "sleep_while_transmitting" flag:
    * 
    * 
    *    We can wait in two cases:
    *       * "async" is not set - we wait while data is being transmitted
    *       * "async" is set and "wait" is set - we might wait if there's
    *         no room in fifo tx buffer
    *    
    *    If "sleep_while_transmitting" flag is set, 
    *    DTERM_TASK_SLEEP() is called for each character.
    *    Otherwise, it's busy-waiting.
    *
    *    Usually it makes little sense to set this flag for synchronous
    *    operation, because DTERM_TASK_SLEEP typically sleeps for 1 ms,
    *    and at 115200 baud there are 11.5 bytes can be transmitted each ms.
    *    It's probably better to busy-wait here.
    *
    *    In the latter case (i.e. for asynchronous operation),
    *    this flag is useful: while process is sleeping, several characters
    *    can be fetched from fifo tx buffer to UART thanks to interrupts.
    *
    *    But NOTE: for asynchronous operation, this flag is used if only 
    *    DTERM__ALLOW_RECURSION is 1.
    */
   DTERM_SETT_FLAG__SLEEP_WHILE_TRANSMITTING = (1 << 3),
};

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/


/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

/*
 * compile-time check
 */
#define __DTERM_DEB_ASSERT_STR(error_msg, A)    { typedef int error__##error_msg [(A) ? 1 : -1]; }


/*
 * Possible values for DTERM_TMSG_LVL__SYS and DTERM_TMSG_LVL__DEF option:
 */
#define  DTERM_TMSG_LVL__NONE       0  //-- no messages are echoed
#define  DTERM_TMSG_LVL__ERR        1  //-- only errors are echoed
#define  DTERM_TMSG_LVL__WRN        2  //-- errors and warnings are echoed
#define  DTERM_TMSG_LVL__INF        3  //-- everything but verbose messages is echoed
#define  DTERM_TMSG_LVL__VRB        4  //-- everything echoed
#define  DTERM_TMSG_LVLS_CNT        5  //-- count of levels. don't use it.
#define  DTERM_TMSG_LVLS_MASK       ((1 << DTERM_TMSG_LVLS_CNT) - 1) //- mask for levels

/*
 * Possible values for DTERM_TMSG_FMT__DEF option:
 * Message format used by macro DTMSG().
 */
#define  DTERM_TMSG_FMT__BARE       0
#define  DTERM_TMSG_FMT__SIMPLE     1  //-- only message level tag is echoed
#define  DTERM_TMSG_FMT__FUNC       2  //-- level, func.name and line
#define  DTERM_TMSG_FMT__FILE_FUNC  3  //-- level, file, func.name and line
#define  DTERM_TMSG_FMTS_CNT        4
#define  DTERM_TMSG_FMTS_MASK       ((1 << DTERM_TMSG_FMTS_CNT) - 1)

/*
 * Modes of operation: asynchronous, synchronous, etc.
 */
#define  DTERM_TMSG_MODE__ASYNC                    0
#define  DTERM_TMSG_MODE__ASYNC_NOWAIT             1
#define  DTERM_TMSG_MODE__ASYNC_NOWAIT_WEAK        2
#define  DTERM_TMSG_MODE__SYNC                     3
#define  DTERM_TMSG_MODE__SYNC_IGN_BUF             4
#define  DTERM_TMSG_MODES_CNT                      5
#define  DTERM_TMSG_MODES_MASK                     ((1 << DTERM_TMSG_MODES_CNT) - 1)

#define  DTERM_TMSG_LVLS_OFFSET     0
#define  DTERM_TMSG_FMTS_OFFSET     (DTERM_TMSG_LVLS_CNT + DTERM_TMSG_LVLS_OFFSET)
#define  DTERM_TMSG_MODES_OFFSET    (DTERM_TMSG_FMTS_CNT + DTERM_TMSG_FMTS_OFFSET)
#define  DTERM_TMSG_OPTS_OFFSET     (DTERM_TMSG_MODES_CNT + DTERM_TMSG_MODES_OFFSET)


/* -------------------------------------------------------
      Option flags for DTMSG() macro
   ------------------------------------------------------- */
#define  DT_ERR            __DT_LVL_FLAG(DTERM_TMSG_LVL__ERR)
#define  DT_WRN            __DT_LVL_FLAG(DTERM_TMSG_LVL__WRN)
#define  DT_INF            __DT_LVL_FLAG(DTERM_TMSG_LVL__INF)
#define  DT_VRB            __DT_LVL_FLAG(DTERM_TMSG_LVL__VRB)

#define  DT_BARE           __DT_FMT_FLAG(DTERM_TMSG_FMT__BARE)
#define  DT_SIMPLE         __DT_FMT_FLAG(DTERM_TMSG_FMT__SIMPLE)
#define  DT_FUNC           __DT_FMT_FLAG(DTERM_TMSG_FMT__FUNC)
#define  DT_FILE_FUNC      __DT_FMT_FLAG(DTERM_TMSG_FMT__FILE_FUNC)

#define  DT_ASYNC          __DT_MODE_FLAG(DTERM_TMSG_MODE__ASYNC)
#define  DT_NOWAIT         __DT_MODE_FLAG(DTERM_TMSG_MODE__ASYNC_NOWAIT)
#define  DT_NOWAIT_WEAK    __DT_MODE_FLAG(DTERM_TMSG_MODE__ASYNC_NOWAIT_WEAK)
#define  DT_SYNC           __DT_MODE_FLAG(DTERM_TMSG_MODE__SYNC)
#define  DT_SYNC_IGN_BUF   __DT_MODE_FLAG(DTERM_TMSG_MODE__SYNC_IGN_BUF)

/*
 * TODO: explain
 *
 * TODO: or, probably just remove it? It seems, it's useless.
 *       We should always behave like DT_FORCE is set.
 *
 * This flag can be used in high-priority ISRs that are NOT disabled
 * in DTERM_LOCK_PRIMARY()   (but are disabled in DTERM_LOCK_SECONDARY).
 *
 * Note that this flag is useless if you haven't
 * DTERM__ALLOW_RECURSION option set.
 *
 */
//#define  DT_FORCE          __DT_OPT_FLAG(0)

/*
 * If you need to call macro with default values, you can put
 * nice DT_DEF value instead of plain 0.
 */
#define  DT_DEF            0



/* -------------------------------------------------------
      Default settings
   ------------------------------------------------------- */

/*
 * DTERM_TMSG_LVL__SYS: system message level;
 * allows suppressing messages of certain levels.
 *
 * Higher levels include all lower levels.
 */
#ifndef DTERM_TMSG_LVL__SYS
#  define DTERM_TMSG_LVL__SYS DTERM_TMSG_LVL__VRB
#endif

/*
 * DTERM_TMSG_LVL__DEF: default message level;
 * will be used if you don't specify level
 * (DT_ERR, DT_INF, etc) for macro DTMSG()
 */
#ifndef DTERM_TMSG_LVL__DEF
#  define DTERM_TMSG_LVL__DEF DTERM_TMSG_LVL__INF
#endif

/*
 * DTERM_TMSG_FMT__DEF: default message format;
 * will be used if you don't specify format
 * (DT_SIMPLE, DT_FUNC, etc) for macro DTMSG()
 */
#ifndef DTERM_TMSG_FMT__DEF
#  define DTERM_TMSG_FMT__DEF DTERM_TMSG_FMT__FUNC
#endif

/*
 * DTERM_TMSG_MODE__DEF: default mode;
 * will be used if you don't specify mode
 * (DT_ASYNC, DT_NOWAIT, etc) for macro DTMSG() or DTPRINT()
 */
#ifndef DTERM_TMSG_MODE__DEF
#  define DTERM_TMSG_MODE__DEF DTERM_TMSG_MODE__ASYNC
#endif

/*
 * DTERM_TMSG_LINE_END: line ending that is appended to each message
 * echoed with DTMSG() (and therefore dterm__msg_text__send() too)
 */
#ifndef DTERM_TMSG_LINE_END
#  define DTERM_TMSG_LINE_END    "\r\n"
#endif

/*
 * DTERM_TMSG_AFTER_TAG: string that is appended after level string
 * before the rest of the message
 */
#ifndef DTERM_TMSG_AFTER_TAG
#  define DTERM_TMSG_AFTER_TAG     ": "
#endif

/*
 * DTERM__ALLOW_RECURSION: allow recursion.
 * that is, allow echoing of new message in the middle of another message.
 *
 * This is typically needed if some time-critical interrupt wants to
 * use dterm, but it's not acceptable to disable this interrupt in DTERM_LOCK_PRIMARY.
 *
 * NOTE: this flag is HIGHLY EXPERIMENTAL! Things may break!
 *
 * NOTE: if you set it to 1, make sure your DTERM_LOCK_PRIMARY / DTERM_UNLOCK_PRIMARY
 *       support recursive calls! Say, if you disable/enable interrupts there,
 *       and DTERM_LOCK_PRIMARY is called twice, then interrupts should be
 *       actually enabled only after second call to DTERM_UNLOCK_PRIMARY.
 *
 * NOTE: if you set it to 1, you must provide DTERM_LOCK_SECONDARY / DTERM_UNLOCK_SECONDARY
 *       which should _really_ disable all interrupts that may use dterm.
 *       This lock is obtained for much less time, so it's probably OK
 *       to just disable/enable all interrupts there.
 *
 * NOTE: in your time-critical interrupt that might happen in the middle of 
 *       another dterm msg generation, it's useless to call DTMSG(), because
 *       dterm__msg_text__send() will check that _state.state_tx != _STATE_TX__USUAL,
 *       and just won't do anything. So, your highly important message from
 *       time-critical interrupt will be just lost, which is not what you
 *       turned DTERM__ALLOW_RECURSION on for.
 *
 *       What you want to use there is DTPRINT(), which doesn't refuse to
 *       insert any string in the middle of another text message
 *       (but not file message though).
 *
 *       TODO: DEV_NOTE: is this good that we aren't able to use DTMSG()
 *       in this case? Probably we should allow this? Yeah, just like 
 *       DTERM_TMSG_MODE__SYNC_IGN_BUF: current message interrupted, new one
 *       started, then new one ended, and previous one continues as "invalid data".
 *       It's better than lost message.
 *
 *
 * DEV NOTE: how private static data is managed when some dterm function is
 *           interrupted in the middle? need to think about it carefully.
 *           See "TODO" marks about using DTERM_LOCK_SECONDARY/UNLOCK
 *           not only for fifobuf. (and therefore rename it to something differen)
 */
#ifndef DTERM__ALLOW_RECURSION
#  define  DTERM__ALLOW_RECURSION  0
#endif



/* -------------------------------------------------------
      Main macros
   ------------------------------------------------------- */

/**
 * DTMSG() - echo message. NOTE: some housekeeping information is added
 *           to data that given here: message start sequence,
 *           message end sequence, line ending is appended
 *           (see DTERM_TMSG_LINE_END)
 *
 * @param opts: there are various options available:
 *       * options for level (DT_ERR, DT_INF, etc),
 *       * options for message format (DT_SIMPLE, DT_FUNC, etc)
 *       * options for mode (DT_ASYNC, DT_SYNC, etc)
 *       * other options (DT_FORCE)
 *
 *       You can't specify multiple options in the same category:
 *       say, if you specify both (DT_ERR | DT_INF), you get
 *       compilation error. The same with other categories
 *       (message format and mode).
 *
 *       Options from different categories are allowed to mix, of course.
 *       Say, this is quite legal:
 *          (DT_WRN | DT_NOWAIT | DT_BARE)
 *
 *       If you specify no option for some category, default value is used.
 *       Default values for level, message format and mode are set with
 *       DTERM_TMSG_LVL__DEF, DTERM_TMSG_FMT__DEF and DTERM_TMSG_MODE__DEF,
 *       respectively.
 *
 *       So, you can specify just 0 here, and all defaults will be used.
 *
 *       NOTE: since most of the time defaults are used, it would be quite
 *       annoying to type 0 for so many DTMSG() calls, so, convenience macros
 *       are added for each level
 *       (because level is the option that varies most often) :
 *       DTMSG_E(), DTMSG_W(), DTMSG_I(), DTMSG_V().
 *
 *       See them below.
 *
 * @param fmt, ... - printf-like format string and its arguments.
 *
 * Usage examples:
 *    
 *       //-- echo int variable asynchronously as information message
 *       //   (message format is default)
 *       int i = 123;
 *       DTMSG(DT_INF, DT_ASYNC, "i=%d", i);
 *
 *       //-- echo warning bare message asynchronously without waiting
 *       DTMSG(DT_WRN | DT_BARE | DT_NOWAIT, "my warning message");
 *
 *       //-- echo error message synchronously (message format is default)
 *       DTMSG(DT_ERR | DT_SYNC, "my error message");
 *
 *       //-- echo message with all default settings
 *       DTMSG(0, "my message with %s settings", "default");
 *
 */
#define  DTMSG(opts, fmt, ...)   ({                                           \
                                                                              \
      /* check if exactly one level is specified, or none at all */           \
      __DTERM_DEB_ASSERT_STR(multiple_levels_specified,                       \
         (__DT_LVL_EXTR(opts) & (__DT_LVL_EXTR(opts) - 1)) == 0               \
         );                                                                   \
                                                                              \
      /* check if exactly one format is specified, or none at all */          \
      __DTERM_DEB_ASSERT_STR(multiple_formats_specified,                      \
         (__DT_FMT_EXTR(opts) & (__DT_FMT_EXTR(opts) - 1)) == 0               \
         );                                                                   \
                                                                              \
      /* check if exactly one mode is specified, or none at all */            \
      __DTERM_DEB_ASSERT_STR(multiple_modes_specified,                        \
         (__DT_MODE_EXTR(opts) & (__DT_MODE_EXTR(opts) - 1)) == 0             \
         );                                                                   \
                                                                              \
      /* check if level string is resolvable */                               \
      __DTERM_DEB_ASSERT_STR(dterm_bug__level_string_is_not_resolvable,       \
         __DT_LVL_STRING_GET(opts, "") != 0                                   \
         );                                                                   \
                                                                              \
      /* check if given message level <= system level */                      \
      if (__DT_LVL_VALUE_GET(opts) <= DTERM_TMSG_LVL__SYS){                   \
                                                                              \
         if (__DT_FMT_VALUE_GET(opts) == DTERM_TMSG_FMT__BARE){               \
            dterm__msg_text__send(                                            \
                  __DT_MODE_STRUCT_GET(opts),                                 \
                  fmt,                                                        \
                  ## __VA_ARGS__                                              \
                  );                                                          \
                                                                              \
         } else if (__DT_FMT_VALUE_GET(opts) == DTERM_TMSG_FMT__SIMPLE){      \
            dterm__msg_text__send(                                            \
                  __DT_MODE_STRUCT_GET(opts),                                 \
                  __DT_LVL_STRING_GET(opts, fmt),                             \
                  ## __VA_ARGS__                                              \
                  );                                                          \
                                                                              \
         } else if (__DT_FMT_VALUE_GET(opts) == DTERM_TMSG_FMT__FUNC){        \
            dterm__msg_text__send(                                            \
                  __DT_MODE_STRUCT_GET(opts),                                 \
                  __DT_LVL_STRING_GET(opts, "%s():%04d: " fmt),               \
                  __func__, __LINE__,                                         \
                  ## __VA_ARGS__                                              \
                  );                                                          \
                                                                              \
         } else if (__DT_FMT_VALUE_GET(opts) == DTERM_TMSG_FMT__FILE_FUNC){   \
            dterm__msg_text__send(                                            \
                  __DT_MODE_STRUCT_GET(opts),                                 \
                  __DT_LVL_STRING_GET(opts, "%s:%s():%04d: " fmt),            \
                  __FILE__, __func__, __LINE__,                               \
                  ## __VA_ARGS__                                              \
                  );                                                          \
                                                                              \
         } else {                                                             \
            /*should never be here*/BSP_DTERM__DEB_HALT();                           \
         }                                                                    \
      }                                                                       \
                                                                              \
   })


/**
 * DTPRINT() - echo raw formatted string without any housekeeping data.
 *             Note that if you need linebreak, you need to add it yourself.
 *
 * @param opts: only options for mode are allowed here (DT_ASYNC, DT_SYNC, etc)
 *
 *       You can't specify multiple options:
 *       say, if you specify both (DT_ASYNC | DT_SYNC), you get
 *       compilation error.
 *
 *       Messages from other categories (message format and level) 
 *       are not allowed here, since it makes no sense for DTPRINT().
 *       If you specify any of them, you get compilation error.
 *
 *       If you specify no option, default value is used.
 *       Default value for mode is set with DTERM_TMSG_MODE__DEF.
 *
 *       So, you can specify just 0 here, and default value will be used.
 *
 * @param fmt, ... - printf-like format string and its arguments.
 *
 *       //-- echo string with default mode
 *       DTPRINT(0, "my string\n");
 *
 *       //-- echo string synchronously
 *       DTPRINT(DT_ASYNC, "my string\n");
 *
 */
#define  DTPRINT(opts, fmt, ...)   ({                                         \
                                                                              \
      /* check if exactly one mode is specified, or none at all */            \
      __DTERM_DEB_ASSERT_STR(multiple_modes_specified,                        \
         (__DT_MODE_EXTR(opts) & (__DT_MODE_EXTR(opts) - 1)) == 0             \
         );                                                                   \
                                                                              \
      /* check if no fmt flags are specified */                               \
      __DTERM_DEB_ASSERT_STR(no_msg_format_options_allowed_in_DTPRINT,        \
         __DT_FMT_EXTR(opts) == 0                                             \
         );                                                                   \
                                                                              \
      /* check if no level flags are specified */                             \
      __DTERM_DEB_ASSERT_STR(no_level_allowed_in_DTPRINT,                     \
         __DT_LVL_EXTR(opts) == 0                                             \
         );                                                                   \
                                                                              \
      dterm__string__send(                                                    \
            __DT_MODE_STRUCT_GET(opts),                                       \
            DTERM_MSG_TYPE__TEXT,                                             \
            fmt,                                                              \
            ## __VA_ARGS__                                                    \
            );                                                                \
                                                                              \
   })


/**
 * Convenience macros for calling DTMSG() without opts argument.
 * Usage is just like classic printf().
 */
#define DTMSG_E(fmt, ...)            DTMSG((DT_ERR), fmt, ## __VA_ARGS__)
#define DTMSG_W(fmt, ...)            DTMSG((DT_WRN), fmt, ## __VA_ARGS__)
#define DTMSG_I(fmt, ...)            DTMSG((DT_INF), fmt, ## __VA_ARGS__)
#define DTMSG_V(fmt, ...)            DTMSG((DT_VRB), fmt, ## __VA_ARGS__)




/*
 * Macros for generating settings struct
 * (if you use only macros DTMSG() or DTPRINT() and don't use functions 
 * (dterm__msg_text__send()), you don't need them)
 */
#if DTERM__BUFFERED_TX

#define DTERM_SETT__ASYNC(...)                                       \
   (0                                                                \
    | DTERM_SETT_FLAG__ASYNC                                         \
    | DTERM_SETT_FLAG__WAIT                                          \
    | DTERM_SETT_FLAG__SLEEP_WHILE_TRANSMITTING                      \
    __VA_ARGS__                                                      \
    )

#define DTERM_SETT__ASYNC_NOWAIT(...)                                \
   (0                                                                \
    | DTERM_SETT_FLAG__ASYNC                                         \
    __VA_ARGS__                                                      \
   )

#define DTERM_SETT__ASYNC_NOWAIT_WEAK(...)                           \
   (0                                                                \
    | DTERM_SETT_FLAG__ASYNC                                         \
    | DTERM_SETT_FLAG__WEAK                                          \
    __VA_ARGS__                                                      \
   )

#define DTERM_SETT__SYNC(...)                                        \
   (0                                                                \
    | DTERM_SETT_FLAG__WAIT                                          \
    __VA_ARGS__                                                      \
   )

#else
#  define DTERM_SETT__ASYNC               DTERM_SETT__SYNC_IGN_BUF
#  define DTERM_SETT__ASYNC_NOWAIT        DTERM_SETT__SYNC_IGN_BUF
#  define DTERM_SETT__ASYNC_NOWAIT_WEAK   DTERM_SETT__SYNC_IGN_BUF
#  define DTERM_SETT__SYNC                DTERM_SETT__SYNC_IGN_BUF
#endif

#define DTERM_SETT__SYNC_IGN_BUF(...)                                \
   (0                                                                \
    __VA_ARGS__                                                      \
   )



#define  dterm__msg_file__end()   dterm__msg__end(DTERM_SETT__ASYNC(), DTERM_MSG_TYPE__FILE)
#define  dterm__msg_text__end()   dterm__msg__end(DTERM_SETT__ASYNC(), DTERM_MSG_TYPE__TEXT)




//-- helper macros {{{

/*
 * The __DT_..._FLAG() macros take option value, like DTERM_TMSG_LVL__ERR,
 * or DTERM_TMSG_FMT__BARE, etc; and return flag like DT_ERR, DT_BARE, etc.
 *
 * Returned flag may then be given to DTMSG() or DTPRINT() macro.
 */
#define  __DT_LVL_FLAG(level)    (1 << (level)  << DTERM_TMSG_LVLS_OFFSET)
#define  __DT_FMT_FLAG(fmt)      (1 << (fmt)    << DTERM_TMSG_FMTS_OFFSET)
#define  __DT_MODE_FLAG(mode)    (1 << (mode)   << DTERM_TMSG_MODES_OFFSET)
#define  __DT_OPT_FLAG(opt)      (1 << (opt)    << DTERM_TMSG_OPTS_OFFSET)

/**
 * Masks out everything but LEVEL options, like DT_ERR, or DT_WRN, etc.
 *
 * @param opts    options given to DTMSG() macro, like (DT_INF | DT_ASYNC)
 */
#define  __DT_LVL_EXTR(opts)     \
   ((opts)  & (DTERM_TMSG_LVLS_MASK    << DTERM_TMSG_LVLS_OFFSET))

/**
 * Masks out everything but FMT options, like DT_BARE, or DT_SIMPLE, etc.
 *
 * @param opts    options given to DTMSG() macro, like (DT_INF | DT_ASYNC)
 */
#define  __DT_FMT_EXTR(opts)     \
   ((opts)  & (DTERM_TMSG_FMTS_MASK    << DTERM_TMSG_FMTS_OFFSET))

/**
 * Masks out everything but MODE options, like DT_ASYNC, or DT_NOWAIT, etc.
 *
 * @param opts    options given to DTMSG() macro, like (DT_INF | DT_ASYNC)
 */
#define  __DT_MODE_EXTR(opts)    \
   ((opts)  & (DTERM_TMSG_MODES_MASK   << DTERM_TMSG_MODES_OFFSET))





/**
 * Returns DTERM_TMSG_LVL__... value depending on given options.
 *
 * @param opts    options given to DTMSG() macro, like (DT_INF | DT_ASYNC)
 */
#define  __DT_LVL_VALUE_GET(opts)                                             \
   (                                                                          \
     ((__DT_LVL_EXTR(opts) == DT_ERR)                                         \
     ? DTERM_TMSG_LVL__ERR                                                    \
     : ((__DT_LVL_EXTR(opts) == DT_WRN)                                       \
        ? DTERM_TMSG_LVL__WRN                                                 \
        : ((__DT_LVL_EXTR(opts) == DT_INF)                                    \
           ? DTERM_TMSG_LVL__INF                                              \
           : ((__DT_LVL_EXTR(opts) == DT_VRB)                                 \
              ? DTERM_TMSG_LVL__VRB                                           \
              : /* default */ DTERM_TMSG_LVL__DEF                             \
             )                                                                \
          )                                                                   \
       )                                                                      \
    )                                                                         \
   )

/**
 * Returns DTERM_TMSG_FMT__... value depending on given options.
 *
 * @param opts    options given to DTMSG() macro, like (DT_INF | DT_ASYNC)
 */
#define  __DT_FMT_VALUE_GET(opts)                                             \
   (                                                                          \
    ((__DT_FMT_EXTR(opts) == DT_BARE)                                         \
     ? DTERM_TMSG_FMT__BARE                                                   \
     : ((__DT_FMT_EXTR(opts) == DT_SIMPLE)                                    \
        ? DTERM_TMSG_FMT__SIMPLE                                              \
        : ((__DT_FMT_EXTR(opts) == DT_FUNC)                                   \
           ? DTERM_TMSG_FMT__FUNC                                             \
           : ((__DT_FMT_EXTR(opts) == DT_FILE_FUNC)                           \
              ? DTERM_TMSG_FMT__FILE_FUNC                                     \
              : /* default */ DTERM_TMSG_FMT__DEF                             \
             )                                                                \
          )                                                                   \
       )                                                                      \
    )                                                                         \
   )

/**
 * Returns DTERM_TMSG_MODE__... value depending on given options.
 *
 * @param opts    options given to DTMSG() macro, like (DT_INF | DT_ASYNC)
 */
#define  __DT_MODE_VALUE_GET(opts)                                            \
   (                                                                          \
    ((__DT_MODE_EXTR(opts) == DT_ASYNC)                                       \
     ? DTERM_TMSG_MODE__ASYNC                                                 \
     : ((__DT_MODE_EXTR(opts) == DT_NOWAIT)                                   \
        ? DTERM_TMSG_MODE__ASYNC_NOWAIT                                       \
        : ((__DT_MODE_EXTR(opts) == DT_NOWAIT_WEAK)                           \
           ? DTERM_TMSG_MODE__ASYNC_NOWAIT_WEAK                               \
           : ((__DT_MODE_EXTR(opts) == DT_SYNC)                               \
              ? DTERM_TMSG_MODE__SYNC                                         \
              : ((__DT_MODE_EXTR(opts) == DT_SYNC_IGN_BUF)                    \
                 ? DTERM_TMSG_MODE__SYNC_IGN_BUF                              \
                 : /* default */ DTERM_TMSG_MODE__DEF                         \
                )                                                             \
             )                                                                \
          )                                                                   \
       )                                                                      \
    )                                                                         \
   )

/**
 * Returns additional field initialization for enum E_DTerm_OutputSett
 * depending on given options.
 *
 * This additional field initialization could be given to macros
 * like DTERM_SETT__ASYNC(), DTERM_SETT__ASYNC_NOWAIT(), etc.
 *
 * @param opts    options given to DTMSG() macro, like (DT_INF | DT_ASYNC)
 */
#define  __DT_MODE_ADD_PARAMS_GET(opts)                                       \
   /*.force = (!!((opts) & DT_FORCE)),*/

/**
 * Returns enum E_DTerm_OutputSett by value
 *
 * @param opts    options given to DTMSG() macro, like (DT_INF | DT_ASYNC)
 */
#define  __DT_MODE_STRUCT_GET(opts)                                           \
   (                                                                          \
    ((__DT_MODE_VALUE_GET(opts) == DTERM_TMSG_MODE__ASYNC)                    \
     ? DTERM_SETT__ASYNC( __DT_MODE_ADD_PARAMS_GET(opts) )                    \
     : ((__DT_MODE_VALUE_GET(opts) == DTERM_TMSG_MODE__ASYNC_NOWAIT)          \
        ? DTERM_SETT__ASYNC_NOWAIT( __DT_MODE_ADD_PARAMS_GET(opts) )          \
        : ((__DT_MODE_VALUE_GET(opts) == DTERM_TMSG_MODE__ASYNC_NOWAIT_WEAK)  \
           ? DTERM_SETT__ASYNC_NOWAIT_WEAK( __DT_MODE_ADD_PARAMS_GET(opts) )  \
           : ((__DT_MODE_VALUE_GET(opts) == DTERM_TMSG_MODE__SYNC)            \
              ? DTERM_SETT__SYNC( __DT_MODE_ADD_PARAMS_GET(opts) )            \
              : ((__DT_MODE_VALUE_GET(opts) == DTERM_TMSG_MODE__SYNC_IGN_BUF) \
                 ? DTERM_SETT__SYNC_IGN_BUF( __DT_MODE_ADD_PARAMS_GET(opts) ) \
                 /* should never be here */                                   \
                 /* unfortunately I'm failed to generate compile-time error */\
                 /* so, at least, runtime error happens */                    \
                 : ({BSP_DTERM__DEB_HALT();}), DTERM_SETT__ASYNC()                   \
                )                                                             \
             )                                                                \
          )                                                                   \
       )                                                                      \
    )                                                                         \
   )

/**
 * Takes options given to DTMSG(), like (DT_INF | DT_ASYNC), and actual
 * format string, such as "my_value=%d".
 * Returns given format string prepended by level tag, e.g. "[I]: my_value=%d".
 *
 * The string between level tag and input string: DTERM_TMSG_AFTER_TAG,
 * default ": ".
 *
 * @param opts       options given to DTMSG() macro, like (DT_INF | DT_ASYNC)
 * @param rest_fmt   format string like "my_value=%d"
 */
#define  __DT_LVL_STRING_GET(opts, rest_fmt)                                  \
   (                                                                          \
     ((__DT_LVL_VALUE_GET(opts) == DTERM_TMSG_LVL__ERR)                       \
     ? "[E]" DTERM_TMSG_AFTER_TAG rest_fmt                                    \
     : ((__DT_LVL_VALUE_GET(opts) == DTERM_TMSG_LVL__WRN)                     \
        ? "[W]" DTERM_TMSG_AFTER_TAG rest_fmt                                 \
        : ((__DT_LVL_VALUE_GET(opts) == DTERM_TMSG_LVL__INF)                  \
           ? "[I]" DTERM_TMSG_AFTER_TAG rest_fmt                              \
           : ((__DT_LVL_VALUE_GET(opts) == DTERM_TMSG_LVL__VRB)               \
              ? "[V]" DTERM_TMSG_AFTER_TAG rest_fmt                           \
              : 0/* should never happen, checked in DTMSG() */                \
             )                                                                \
          )                                                                   \
       )                                                                      \
    )                                                                         \
   )

// }}}

/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

//-------------- Initialization ------------------------------

bool  dterm__init           (unsigned long periph_bus_clk_freq, unsigned long baudrate, T_pDTerm_RxHandler rx_handler);


//-------------- Send text message ------------------------------

size_t dterm__msg_text__send        (enum E_DTerm_OutputSett sett, char const *format, ...);


//-------------- Manually start and end messages ------------------------------

void     dterm__msg_file__start     (enum E_DTerm_OutputSett sett, const char *p_filename, U32 file_size);
void     dterm__msg_text__start     (enum E_DTerm_OutputSett sett);
void     dterm__msg__end            (enum E_DTerm_OutputSett sett, T_DTerm_MsgType msg_type);


//-------------- Manually send raw data ------------------------------

void     dterm__data__send          (enum E_DTerm_OutputSett sett, T_DTerm_MsgType msg_type, const char *p_data, size_t len);
size_t   dterm__string__send        (enum E_DTerm_OutputSett sett, T_DTerm_MsgType msg_type, const char *format, ...);


//-------------- Utils ------------------------------

void dterm__bytearr__echo(enum E_DTerm_OutputSett sett, const char *p_title, const U08 *p_data, size_t len);

//-------------- Status ------------------------------

bool     dterm__is_transmitting(void);
bool     dterm__is_transmitting__direct(void);
U16      dterm__tx_buf__max_used_size__get(void);
bool     dterm__is_waiting(void);


//-------------- System ------------------------------

void     dterm__interrupt_en(void);
void     dterm__interrupt_dis(void);

#endif // _DTERM_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


