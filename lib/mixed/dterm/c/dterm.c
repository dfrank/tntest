/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

/*
 *
 * Measured timing:
 *
 *    * xsprintf(_tmp_arr, "test %d test %d test %d", 1, 2, 3);
 *      1514 cycles
 *
 *    * dterm__string__send(DTERM__ASYNC_NOWAIT, DTERM_MSG_TYPE__TEXT, "test %d test %d test %d", 1, 2, 3);
 *      6900 cycles
 *
 *    * some of _lock_secondary timing:
 *       _lock()                       33 cycles
 *       _unlock()                     91 cycles
 *       _msg__start()                 19 cycles
 *       fifobuf__u08__put_check       95 - 100 cycles
 *
 */

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include <string.h>

#include "dterm.h"



/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

#define  _DT_ESC_SYMB   0x1e


#if DTERM__BUFFERED_TX
#   include <utils/fifobuf.h>
#endif




#if   ( defined(DTERM_LOCK_SECONDARY) && !defined(DTERM_UNLOCK_SECONDARY))\
   || (!defined(DTERM_LOCK_SECONDARY) &&  defined(DTERM_UNLOCK_SECONDARY))

#  error DTERM_LOCK_SECONDARY and DTERM_UNLOCK_SECONDARY must be both set or both not set
#endif

#if DTERM__ALLOW_RECURSION && !defined(DTERM_LOCK_SECONDARY)
#  error when DTERM__ALLOW_RECURSION is enabled, DTERM_LOCK_SECONDARY and DTERM_UNLOCK_SECONDARY must be set
#endif

#if defined(DTERM_LOCK_SECONDARY) && defined(DTERM_UNLOCK_SECONDARY)
#  define   _FIFOBUF_LOCK_MODE   FIFOBUF_LM__LOCKED
#else
#  define   _FIFOBUF_LOCK_MODE   FIFOBUF_LM__UNLOCKED
#endif

/***************************************************************************************************
 *                                  PRIVATE FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

enum _E_EscChar {
   _ESC_CHAR__MSG_END,
   _ESC_CHAR__MSG_START,
   _ESC_CHAR__DATA_LOST,
};

typedef enum _E_StateTx {
   _STATE_TX__NOT_INITIALIZED,
   _STATE_TX__WAITING,        //-- waiting for tx command
   _STATE_TX__TRANSMITTING,   //-- transmission is in progress
} _T_StateTx;

typedef enum _E_StateRx {
   _STATE_RX__NOT_INITIALIZED,
   _STATE_RX__OUT_OF_MSG,
   _STATE_RX__IN_MSG__WAITING_FOR_MSG_TYPE,
   _STATE_RX__IN_MSG__WAITING_FOR_LEN,
   _STATE_RX__IN_MSG__READING_DATA,
   _STATE_RX__IN_MSG__WAITING_FOR_MSG_END,
} _T_StateRx;

typedef struct {
   _T_StateTx        state_tx;
   _T_StateRx        state_rx;
   T_DTerm_MsgType   tx_msg_type;
   U08               rx_msg_bytes_left;
   U08              *p_rx_cur_pt;

   int               lock_cnt;

   //-- NOTE: flags are integers just to make them atomic
   struct {
      int too_long_msg;    //-- given message length is too large, so, completely ignore this msg.
      int escaped;         //-- just got escape symbol, so, next char has special meaning
      int old_data_lost;   //-- in the mode DTERM_OUT_MODE__ASYNC_NOWAIT, some old data
                           //   was overwritten, so we need to add _esc_seq__data_lost
   } flags;

} _T_State;

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

//static FBUF _crx;

#if DTERM__BUFFERED_TX
static U08        _tx_buf[ DTERM__TX_BUF_SIZE ];
static T_FIFOBuf  _fifobuf_tx;
static U16        _fifobuf_tx_max_used_size = 0;   //-- for statistics

#endif

#if DTERM__RX_ENABLED
static T_DTermRxBuf  _rx_buf = {};
#endif

static const char _esc_seq__msg_start[] = { _DT_ESC_SYMB, _ESC_CHAR__MSG_START };
static const char _esc_seq__msg_end[]   = { _DT_ESC_SYMB, _ESC_CHAR__MSG_END };
static const char _esc_seq__data_lost[] = { _DT_ESC_SYMB, _ESC_CHAR__DATA_LOST };

static T_pDTerm_RxHandler _rx_handler = NULL;

static _T_State _state = {
   .state_tx          = _STATE_TX__NOT_INITIALIZED,
   .state_rx          = _STATE_RX__NOT_INITIALIZED,
   .rx_msg_bytes_left = 0,

   .lock_cnt          = 0,

   .flags = {
   },
};

/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/


/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/


/***************************************************************************************************
 *                                        PRIVATE FUNCTIONS
 **************************************************************************************************/

//-- Private functions {{{

static void _uart_on(void)
{
   bsp_dterm__uart_on();
}

static void _uart_off(void)
{
   bsp_dterm__uart_off();
}

#if DTERM__ALLOW_RECURSION
static inline void _lock_secondary_if_recursion_allowed(void)
{
   DTERM_LOCK_SECONDARY();
}

static inline void _unlock_secondary_if_recursion_allowed(void)
{
   DTERM_UNLOCK_SECONDARY();
}
#else
static inline void _lock_secondary_if_recursion_allowed(void) {}
static inline void _unlock_secondary_if_recursion_allowed(void) {}
#endif

#if defined(DTERM_LOCK_SECONDARY) && defined(DTERM_UNLOCK_SECONDARY)
static inline void _lock_secondary(void)
{
   DTERM_LOCK_SECONDARY();
}

static inline void _unlock_secondary(void)
{
   DTERM_UNLOCK_SECONDARY();
}
#else
static inline void _lock_secondary(void) {}
static inline void _unlock_secondary(void) {}
#endif


#if DTERM__BUFFERED_TX

static inline void _tx_int_disable(void)
{
   bsp_dterm__tx_int_disable();
}

static inline void _tx_int_enable(void)
{
   bsp_dterm__tx_int_enable();
}

static void _finalize_tx__check_data_lost(void)
{
   _lock_secondary_if_recursion_allowed();

   if (_state.flags.old_data_lost){
      int i;
      for (i = 0; i < sizeof(_esc_seq__data_lost); i++){
         //-- NOTE: we pass FIFOBUF_LM__UNLOCKED here, because
         //   lock was acquired above
         fifobuf__u08__set_by_index(
               &_fifobuf_tx, FIFOBUF_LM__UNLOCKED, i, _esc_seq__data_lost[i]
               );
      }
      _state.flags.old_data_lost = false;
   }

   _unlock_secondary_if_recursion_allowed();
}

/**
 * While there is room in hardware UART Tx buffer, and there is
 * some data in fifo tx buffer, feed data from fifo to uart.
 */
static inline void _from_fifobuf_to_uart(void)
{
   while (!bsp_dterm__is_tx_buf_full()){

      //-- To avoid TOCTTOU problems, acquire a lock here,
      //   and use FIFOBUF_LM__UNLOCKED for fifobuf__u08__get().
#if DTERM__ALLOW_RECURSION
      _lock_secondary_if_recursion_allowed();

      //-- firstly, when we acquired a lock, check again
      //   that uart buffer is still isn't full

      if (!bsp_dterm__is_tx_buf_full()){

#endif
         //-- there is some room in hardware UART Tx buffer,
         //   let's check if we have more data to send

         if (fifobuf__is_empty(&_fifobuf_tx)){
            //-- all data is successfully sent, so, break.
            //   (don't foget to unlock firstly)

            _unlock_secondary_if_recursion_allowed();
            break;
         }

         //-- need to put next char from fifo buffer to UART
         //   NOTE: we pass FIFOBUF_LM__UNLOCKED here, because
         //   lock is already acquired to avoid TOCTTOU problems.
         bsp_dterm__uart_char_send(fifobuf__u08__get(&_fifobuf_tx, FIFOBUF_LM__UNLOCKED));

#if DTERM__ALLOW_RECURSION

      } else {
         //-- after we locked and re-checked UART status, its buffer is already full.
         //   It may happen if someone interrupted us after UART status is received
         //   first time, but before we acquired a lock.
         //   Just go on, nothing to do here.
      }

      _unlock_secondary_if_recursion_allowed();
#endif
   }

}

static inline void _busy_loop_until_fifobuf_is_empty(enum E_DTerm_OutputSett sett)
{
   while (!fifobuf__is_empty(&_fifobuf_tx)){
      _from_fifobuf_to_uart();
   }
}

//-- fifobuf callbacks {{{
#if DTERM__ALLOW_RECURSION
static void _fifobuf_tx_cb__lock(void *p_user_data)
{
   _lock_secondary_if_recursion_allowed();
}

static void _fifobuf_tx_cb__unlock(void *p_user_data)
{
   _unlock_secondary_if_recursion_allowed();
}
#endif

// }}}

static inline void _char__send_async(char s, const enum E_DTerm_OutputSett *p_sett)
{
   if (     fifobuf__is_empty(&_fifobuf_tx)
         && !bsp_dterm__is_tx_buf_full()
      )
   {
      //-- buffer is empty and UART is not busy, so, start transmission immediately
      bsp_dterm__uart_char_send(s);
   } else {

      if (*p_sett & DTERM_SETT_FLAG__WAIT){
         //-- we can wait
         bool bool_success = false;

         //-- loop until character is successfully put to the fifo tx buffer
         do {
            //-- try to put new char to buffer, bool_success reflects result of that try.
            fifobuf__u08__put_check(&_fifobuf_tx, _FIFOBUF_LOCK_MODE, s, &bool_success);

            //-- try to immediately feed the data from fifo buffer to uart
            //   NOTE that UART Tx interrupt is disabled now,
            //   so we need to do this job right here.
            //   It works much faster than in case with interrupts enabled.
            //   (if it fails - don't mention it, it will be done later from ISR)
            _from_fifobuf_to_uart();

            //-- NOTE: we check for DTERM__ALLOW_RECURSION here, because when we sleep,
            //         ISR gets called when dterm is already locked.
            //         ISR calls _lock() too, so, this is recursive call.
#if DTERM__ALLOW_RECURSION
            if (*p_sett * DTERM_SETT_FLAG__SLEEP_WHILE_TRANSMITTING && !bool_success){
               //-- before sleeping, we should enable UART tx interrupts,
               //   in order to make transmission continue fetching data
               //   from fifo tx buffer.
               _tx_int_enable();
               DTERM_TASK_SLEEP();
               _tx_int_disable();
            }
#endif

         } while (!bool_success);
      } else {
         //-- no wait

         if (*p_sett & DTERM_SETT_FLAG__WEAK){
            //-- in the "weak" mode, we just try to put new char to
            //   fifo tx buffer and we don't care if that fails.
            //   So, new data might be lost.
            //
            //   TODO: probably we should add data_lost sequence here, somehow?
            bool bool_success = false;
            fifobuf__u08__put_check(&_fifobuf_tx, _FIFOBUF_LOCK_MODE, s, &bool_success);

            //-- don't care if bool_success is false:
            //   it's "weak" mode
         } else {
            bool bool_success = false;

            do {
               //-- check if fifo tx buffer is full. If it is,
               //   then we need to throw old data away (so that it is lost forever),
               //   in order to make room for new data.
               if (fifobuf__is_full(&_fifobuf_tx)){
                  //-- old data is lost. Among other things, set old_data_lost flag,
                  //   so that when all new data is successfully written, we can
                  //   add special escape sequence indicating that some data is lost.
                  _state.flags.old_data_lost = true;
                  fifobuf__u08__get(&_fifobuf_tx, _FIFOBUF_LOCK_MODE);
               }

               //-- here, we're sure that there is room in the fifo tx buffer for new char
               {
                  fifobuf__u08__put_check(&_fifobuf_tx, _FIFOBUF_LOCK_MODE, s, &bool_success);

#if !DTERM__ALLOW_RECURSION
                  if (!bool_success){
                     BSP_DTERM__DEB_HALT();
                     //-- should NEVER be here
                     //   (this case should be caught in _lock())
                  }
#endif
               }

               //-- if DTERM__ALLOW_RECURSION is set to non-zero, we still may fail here.
               //   in this case, retry.
            } while (!bool_success);
         }

      }

   }

   //-- used for statistics: _fifobuf_tx_max_used_size
   {
      U16 used_size = fifobuf__count__get(&_fifobuf_tx);
      if (used_size > _fifobuf_tx_max_used_size){
         _fifobuf_tx_max_used_size = used_size;
      }
   }
}


#else

static void _finalize_tx__check_data_lost(void) {}
static inline void _busy_loop_until_fifobuf_is_empty(enum E_DTerm_OutputSett sett) {}

static inline void _tx_int_disable(void) {};
static inline void _tx_int_enable(void) {};

#endif

static inline void _char__send_sync(char s, const enum E_DTerm_OutputSett *p_sett)
{
   //-- wait while hardware UART TX buffer is full, and check if there's too long
   //   (when debugger stops execution, if UART is active in this moment, then
   //   flag TX_BUF_FULL will never be cleared, so,
   //   we have to create workaround here: if we wait too long, resart UART
   //   )
   {
#define  _TIMEOUT 400000   //TODO: implement timeout in more universal way (say, use some callback from 
      //      app to get tickcount in milliseconds)
      U32 counter = 0;
      while (bsp_dterm__is_tx_buf_full() && counter++ < _TIMEOUT){
         if (*p_sett & DTERM_SETT_FLAG__SLEEP_WHILE_TRANSMITTING){
            //-- probably not so useful: we will wait much longer than in case of busy-wait
            //   (on 115200 baud and 1-millisecond sleep, about 11.5 times longer)
            DTERM_TASK_SLEEP();
         }
      }

      if (counter >= _TIMEOUT){
         //-- restart UART
         _uart_off();
         _uart_on();
      }
   }

   bsp_dterm__uart_char_send(s);
}

static void _char__send(char s, void *pt)
{
   const enum E_DTerm_OutputSett *p_sett = pt;

#if DTERM__BUFFERED_TX
   if (*p_sett & DTERM_SETT_FLAG__ASYNC){
      _char__send_async(s, p_sett);
   } else {
      _char__send_sync(s, p_sett);
   }
#else
   //-- when compiled without support of buffered Tx, always 
   //   transmit data synchronously
   _char__send_sync(s, p_sett);
#endif
}


static bool _can_transmit(T_DTerm_MsgType msg_type)
{
   _lock_secondary_if_recursion_allowed();

   bool ret = 
      (
       _state.state_tx == _STATE_TX__WAITING 
       || 
       (_state.state_tx == _STATE_TX__TRANSMITTING && msg_type == _state.tx_msg_type)
      );

   _unlock_secondary_if_recursion_allowed();

   return ret;
}

/**
 * @param escape_magic_symbol should be set to false when sending some escape sequence
 *        (e.g. start_msg)
 *        otherwise it should be set to true
 */
static void _data__send(enum E_DTerm_OutputSett sett, const char *p_data, size_t len, bool escape_magic_symbol)
{
   while (len-- > 0){
      _char__send(*p_data, &sett);

      if (escape_magic_symbol && *p_data == _DT_ESC_SYMB){
         _char__send(*p_data, &sett);
      }

      p_data++;
   }
}

static bool _msg__start(enum E_DTerm_OutputSett sett, T_DTerm_MsgType msg_type)
{
   _lock_secondary_if_recursion_allowed();
   bool ret = (0
         || (_state.state_tx == _STATE_TX__WAITING)
         || !(sett & DTERM_SETT_FLAG__WEAK)
         );

   if (ret){

      _state.state_tx      = _STATE_TX__TRANSMITTING;
      _state.tx_msg_type   = msg_type;

      _unlock_secondary_if_recursion_allowed();

      _data__send(sett, _esc_seq__msg_start, sizeof(_esc_seq__msg_start), false);
      _char__send(msg_type, &sett);

   } else {
      _unlock_secondary_if_recursion_allowed();
   }

   return ret;
}

static void _msg__end(enum E_DTerm_OutputSett sett, T_DTerm_MsgType msg_type)
{

   bool state_match = false;
   _lock_secondary_if_recursion_allowed();
   state_match = (_state.state_tx == _STATE_TX__TRANSMITTING && msg_type == _state.tx_msg_type);
   _unlock_secondary_if_recursion_allowed();

   if (state_match){
      //-- send end_msg sequence
      _data__send(sett, _esc_seq__msg_end, sizeof(_esc_seq__msg_end), false);

      _lock_secondary_if_recursion_allowed();
      //-- after we acquired a lock again, make sure state wasn't changed yet
      state_match = (_state.state_tx == _STATE_TX__TRANSMITTING && msg_type == _state.tx_msg_type);

      if (state_match){
         //-- set state to _STATE_TX__WAITING
         _state.tx_msg_type   = DTERM_MSG_TYPE__NONE;
         _state.state_tx      = _STATE_TX__WAITING;
      }

      _unlock_secondary_if_recursion_allowed();
   }
}

static bool _lock(void)
{
   bool ret = true;
#ifdef DTERM_LOCK_PRIMARY
   DTERM_LOCK_PRIMARY();
#endif

   {
      _lock_secondary();

#if !DTERM__ALLOW_RECURSION
      if (_state.lock_cnt > 0){
         ret = false;
         BSP_DTERM__DEB_HALT();
         //-- IF YOU GOT HERE:
         //   it seems, you use dterm from some interrupt,
         //   and this interrupt isn't disabled 
         //   in user-provided lock function! It's your mistake.
         //
         //   You have two options:
         //       * modify your main lock function 
         //         (typically, bsp_dterm_mutex__lock())
         //         so that it disables interrupts that use dterm
         //
         //       * set DTERM__ALLOW_RECURSION to 1 and provide
         //         DTERM_LOCK_SECONDARY / DTERM_UNLOCK_SECONDARY
         //         which typically would disable / enable ALL interrupts
         //         
         //         This secondary lock is not acquired for long time,
         //         so, disabling / enabling all interrupts is usually OK
      }
#endif

      if (ret){
         _state.lock_cnt++;

         if (_state.lock_cnt == 1){
            _tx_int_disable();
         }
      }

      _unlock_secondary();
   }

   return ret;
}

static void _unlock(void)
{
   {
      _lock_secondary();

      if (_state.lock_cnt == 1){
         _tx_int_enable();
      }

      _state.lock_cnt--;

      _unlock_secondary();
   }

#ifdef DTERM_UNLOCK_PRIMARY
   DTERM_UNLOCK_PRIMARY();
#endif
}

static void _finalize_tx(void)
{
   _finalize_tx__check_data_lost();
}

// }}}

/***************************************************************************************************
 *    PROTECTED FUNCTIONS
 **************************************************************************************************/

#if DTERM__RX_ENABLED
void _dterm__isr_rx_handler(void)
{
   while (bsp_dterm__is_rx_buf_not_empty()){

      U08 cur_byte = (U08)bsp_dterm__uart_char_receive(); //U1RXREG;

      if (!_state.flags.escaped && cur_byte == _DT_ESC_SYMB){
         _state.flags.escaped = 1;
         continue;
      }

      if (_state.flags.escaped){
         _state.flags.escaped = 0;

         switch (cur_byte){
            case _ESC_CHAR__MSG_START:
               _state.state_rx = _STATE_RX__IN_MSG__WAITING_FOR_MSG_TYPE;
               continue;
               break;
            case _ESC_CHAR__MSG_END:
               if (_state.state_rx == _STATE_RX__IN_MSG__WAITING_FOR_MSG_END){

                  if (!_state.flags.too_long_msg){
                     if (_rx_handler != NULL){
                        //_rx_handler(&_crx);
                        _rx_handler(&_rx_buf);
                     }
                  }

               }

               _state.state_rx = _STATE_RX__OUT_OF_MSG;
               continue;
               break;
            case _DT_ESC_SYMB:
               //-- this is _DT_ESC_SYMB literally. just continue handle it.
               break;
            default:
               //-- unknown escaped characted. just continue handle it.
               break;
         }

      }


      switch (_state.state_rx){
         case _STATE_RX__NOT_INITIALIZED:
            break;

         case _STATE_RX__IN_MSG__WAITING_FOR_MSG_TYPE:
            if (cur_byte == DTERM_MSG_TYPE__DATA){
               _state.state_rx = _STATE_RX__IN_MSG__WAITING_FOR_LEN;
            } else {
               //-- message types other than DATA aren't supported here
               _state.state_rx = _STATE_RX__OUT_OF_MSG;
            }
            break;

         case _STATE_RX__IN_MSG__WAITING_FOR_LEN:

            _state.rx_msg_bytes_left = cur_byte;
            _rx_buf.len = cur_byte;
            _state.state_rx = _STATE_RX__IN_MSG__READING_DATA;
            _state.p_rx_cur_pt = _rx_buf.data;

            if (_rx_buf.len > DTERM__RX_BUF_SIZE){
               //-- error: buffer is not enough for this message
               _state.flags.too_long_msg = 1;
            } else {
               _state.flags.too_long_msg = 0;
            }
            break;

         case _STATE_RX__IN_MSG__READING_DATA:
            if (!_state.flags.too_long_msg){
               *_state.p_rx_cur_pt = cur_byte;
               _state.p_rx_cur_pt++;
            }

            if (--_state.rx_msg_bytes_left == 0){
               //-- this is last byte of the current message.
               _state.state_rx = _STATE_RX__IN_MSG__WAITING_FOR_MSG_END;
            }

            break;

         default:
            break;

      }

   }
}
#endif
   

#if DTERM__BUFFERED_TX
void _dterm__isr_tx_handler(void)
{
   if (_lock()){
      _from_fifobuf_to_uart();
      _unlock();
   }
}
#endif

/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

//-- Initialization {{{
bool dterm__init(unsigned long periph_bus_clk_freq, unsigned long baudrate, T_pDTerm_RxHandler rx_handler)
{
   bool ret = true;
   if (_state.state_tx != _STATE_TX__NOT_INITIALIZED){
      ret = false;
   } else {
#ifdef DTERM_USER_INIT
      DTERM_USER_INIT();
#endif
      
#if DTERM__BUFFERED_TX
      {
         T_FIFOBuf_CtorParams params = {
            .p_buf = _tx_buf,
            .size  = sizeof(_tx_buf),
            .callback = {
#if DTERM__ALLOW_RECURSION
               .p_lock           = _fifobuf_tx_cb__lock,
               .p_unlock         = _fifobuf_tx_cb__unlock,
#endif
               .p_user_data      = NULL,  //-- not used
            },
         };

         fifobuf__ctor(&_fifobuf_tx, &params);
      }
#endif

      ret = bsp_dterm__init(periph_bus_clk_freq, baudrate);

      if (ret){
         dterm__interrupt_en();

         _state.state_tx = _STATE_TX__WAITING;
         _state.state_rx = _STATE_RX__OUT_OF_MSG;
      }

      _rx_handler = rx_handler;

   }

   return ret;
}
// }}}

//-- Text message send {{{

/**
 * Send text message, with start_msg and end_msg sequences.
 */
size_t dterm__msg_text__send(enum E_DTerm_OutputSett sett, char const *format, ...)
{
   size_t ret = 0;

   if (_lock()){

      if (
            !(sett & DTERM_SETT_FLAG__ASYNC)
            &&
            (sett & DTERM_SETT_FLAG__WAIT)
         )
      {
         _busy_loop_until_fifobuf_is_empty(sett);
      }

      if (_msg__start(sett, DTERM_MSG_TYPE__TEXT)){

         //-- send message text
         {
            va_list ap;
            va_start(ap, format);
            ret += bsp_dterm__formatted_write(format, _char__send, &sett, ap);

            va_end(ap);
         }

         //-- send newline (to make it readable on usual terminals too)
         ret += bsp_dterm__formatted_write(DTERM_TMSG_LINE_END, _char__send, &sett, NULL);

         //-- send end_msg sequence
         _msg__end(sett, DTERM_MSG_TYPE__TEXT);

         _finalize_tx();
      }

      _unlock();
   }

   return ret;
}


// }}}

//-- Manually start and end messages {{{

void dterm__msg_file__start(enum E_DTerm_OutputSett sett, const char *p_filename, U32 file_size)
{
   if (_lock()){

      if (
            !(sett & DTERM_SETT_FLAG__ASYNC)
            &&
            (sett & DTERM_SETT_FLAG__WAIT)
         )
      {
         _busy_loop_until_fifobuf_is_empty(sett);
      }


      if (_msg__start(sett, DTERM_MSG_TYPE__FILE)){

         _data__send(sett, p_filename, strlen(p_filename) + 1/*including NULL-terminate*/, true);
         _data__send(sett, (char *)&file_size, sizeof(file_size), true);

         _finalize_tx();
      }

      _unlock();
   }
}

void dterm__msg_text__start(enum E_DTerm_OutputSett sett)
{
   if (_lock()){

      if (
            !(sett & DTERM_SETT_FLAG__ASYNC)
            &&
            (sett & DTERM_SETT_FLAG__WAIT)
         )
      {
         _busy_loop_until_fifobuf_is_empty(sett);
      }

      if (_msg__start(sett, DTERM_MSG_TYPE__TEXT)){
         _finalize_tx();
      }
      _unlock();
   }
}

void dterm__msg__end(enum E_DTerm_OutputSett sett, T_DTerm_MsgType msg_type)
{
   if (_lock()){

      if (
            !(sett & DTERM_SETT_FLAG__ASYNC)
            &&
            (sett & DTERM_SETT_FLAG__WAIT)
         )
      {
         _busy_loop_until_fifobuf_is_empty(sett);
      }
      _msg__end(sett, msg_type);
      _finalize_tx();
      _unlock();
   }
}

// }}}

//-- Manually send raw data {{{

void dterm__data__send(enum E_DTerm_OutputSett sett, T_DTerm_MsgType msg_type, const char *p_data, size_t len)
{
   if (_lock()){

      if (
            !(sett & DTERM_SETT_FLAG__ASYNC)
            &&
            (sett & DTERM_SETT_FLAG__WAIT)
         )
      {
         _busy_loop_until_fifobuf_is_empty(sett);
      }

      if (_can_transmit(msg_type)){
         _data__send(sett, p_data, len, true);
      }

      _finalize_tx();
      _unlock();
   }
}

size_t dterm__string__send(enum E_DTerm_OutputSett sett, T_DTerm_MsgType msg_type, const char *format, ...)
{
   size_t ret = 0;

   if (_lock()){

      if (
            !(sett & DTERM_SETT_FLAG__ASYNC)
            &&
            (sett & DTERM_SETT_FLAG__WAIT)
         )
      {
         _busy_loop_until_fifobuf_is_empty(sett);
      }

      if (_can_transmit(msg_type)){

         va_list ap;

         va_start (ap, format);
         ret = bsp_dterm__formatted_write(format, _char__send, &sett, ap);
         va_end (ap);

      }

      _finalize_tx();
      _unlock();
   }

   return ret;
}

// }}}

//-- Utils {{{

void dterm__bytearr__echo(enum E_DTerm_OutputSett sett, const char *p_title, const U08 *p_data, size_t len)
{
   dterm__msg_text__start(sett);

   dterm__string__send(sett, DTERM_MSG_TYPE__TEXT, "%s (len=%d) : ", p_title, len);
   size_t i;
   for (i = 0; i < len; i++){
      dterm__string__send(sett, DTERM_MSG_TYPE__TEXT, "0x%.2x", *p_data++);
      if ((i + 1) < len){
         dterm__string__send(sett, DTERM_MSG_TYPE__TEXT, ", ");
      }
   }

   dterm__msg__end(sett, DTERM_MSG_TYPE__TEXT);
}

// }}}

//-- Status {{{

bool dterm__is_transmitting(void)
{
   return (
         !bsp_dterm__is_transmitting__direct()
#if DTERM__BUFFERED_TX
         &&
         fifobuf__is_empty(&_fifobuf_tx)
#endif
         ) ? false : true;
}

bool dterm__is_transmitting__direct(void)
{
   return bsp_dterm__is_transmitting__direct();
}

U16 dterm__tx_buf__max_used_size__get(void)
{
#if DTERM__BUFFERED_TX
   return _fifobuf_tx_max_used_size;
#else
   return 0;
#endif
}

bool dterm__is_waiting(void)
{
#if DTERM__BUFFERED_TX
   return fifobuf__is_full(&_fifobuf_tx);
#else
   return dterm__is_transmitting();
#endif
}

// }}}

//-- System {{{

void dterm__interrupt_en(void)
{
   bsp_dterm__interrupt_en();
}

void dterm__interrupt_dis(void)
{
   bsp_dterm__interrupt_dis();
}

// }}}







/***************************************************************************************************
 *  end of file
 **************************************************************************************************/


