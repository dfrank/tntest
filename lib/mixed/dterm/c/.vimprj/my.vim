
" определяем путь к папке .vim
let s:sPath = expand('<sfile>:p:h')

let &tabstop = 3
let &shiftwidth = 3

" Путь до корня либ
let s:sEnvPath = $INDEXER_PROJECT_ROOT."/../../.."

" подключаем теги используемых библиотек
exec "set tags+=".s:sEnvPath."/c/utils/.vimprj/.indexer_files_tags/utils"

let g:indexer_indexerListFilename = s:sPath.'/.indexer_files'

let g:indexer_handlePath = 0

call envcontrol#set_previous()

