
#ifndef _DTERM_TYPES_H
#define _DTERM_TYPES_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#ifndef _U08_DEFINED
typedef uint8_t         U08;
#define _U08_DEFINED
#endif

#ifndef _U08_FAST_DEFINED
typedef uint_fast8_t    U08_FAST;
#define _U08_FAST_DEFINED
#endif

#ifndef _U16_DEFINED
typedef uint16_t        U16;
#define _U16_DEFINED
#endif

#ifndef _U16_FAST_DEFINED
typedef uint_fast16_t    U16_FAST;
#define _U16_FAST_DEFINED
#endif

#ifndef _U32_DEFINED
typedef uint32_t        U32;
#define _U32_DEFINED
#endif

#ifndef _U64_DEFINED
typedef uint64_t        U64;
#define _U64_DEFINED
#endif

#ifndef _S08_DEFINED
typedef int8_t          S08;
#define _S08_DEFINED
#endif

#ifndef _S08_FAST_DEFINED
typedef int_fast8_t     S08_FAST;
#define _S08_FAST_DEFINED
#endif

#ifndef _S16_DEFINED
typedef int16_t         S16;
#define _S16_DEFINED
#endif

#ifndef _S16_FAST_DEFINED
typedef int_fast16_t    S16_FAST;
#define _S16_FAST_DEFINED
#endif

#ifndef NULL
#define NULL  (void *)0
#endif


/**
 * Generates compile-time error if `cond` is zero
 */
#define DFC_STATIC_ASSERT(error_msg, cond)    { typedef int error__##error_msg [(cond) ? 1 : -1]; }



#endif /* _DTERM_TYPES_H */
/***************************************************************************************************
    end of file: types.h
 **************************************************************************************************/
