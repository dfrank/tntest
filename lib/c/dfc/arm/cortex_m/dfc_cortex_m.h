
#ifndef _DFC_CORTEX_M_H
#define _DFC_CORTEX_M_H

#include "common/dfc_types.h"

#define DFC_DEB_HALT()  {__asm__ volatile("bkpt #0");}

#ifndef _WORD_DEFINED
typedef          S32         PWORD;
#define _WORD_DEFINED
#endif

#ifndef _UWORD_DEFINED
typedef          U32         UPWORD;
#define _UWORD_DEFINED
#endif

#ifndef _SWORD_DEFINED
typedef          S32         SPWORD;
#define _SWORD_DEFINED
#endif

#endif  /* _DFC_CORTEX_M_H */

