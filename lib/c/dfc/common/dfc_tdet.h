

#ifndef _DFC_TDET_H
#define _DFC_TDET_H

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

//--  Microchip C32

#if defined(__XC32)
#  define DFC_COMPILER__MCHP_XC32

#  if defined(__PIC32MX__)
#     define DFC_ARCH__PIC32MX
#  else
#     error unknown architecture under microchip C32 compiler
#  endif


//-- Keil RealView

#elif defined(__ARMCC_VERSION)
#  define DFC_COMPILER__KEIL_REALVIEW

#  define DFC_ARCH__CORTEX_M

#  if ((__TARGET_ARCH_ARM == 0) && (__TARGET_ARCH_THUMB == 4))
#     define DFC_ARCH__CORTEX_M3
#  elif ((__TARGET_ARCH_ARM == 0) && (__TARGET_ARCH_THUMB == 3))
#     define DFC_ARCH__CORTEX_M0
#  else
#     error unknown architecture under keil realview compiler
#  endif

//-- GCC

#elif defined(__GNUC__)
#  define DFC_COMPILER__GCC

#  if defined(__ARM_ARCH)
#     define DFC_ARCH__CORTEX_M

#     if defined(__ARM_ARCH_6M__)
#        define DFC_ARCH__CORTEX_M0
#     elif defined(__ARM_ARCH_7M__)
#        define DFC_ARCH__CORTEX_M3
#     elif defined(__ARM_ARCH_7EM__)
#        if defined(__SOFTFP__)
#           define DFC_ARCH__CORTEX_M4
#        else
#           define DFC_ARCH__CORTEX_M4F
#        endif
#     else
#        error unknown ARM architecture for GCC compiler
#     endif
#  else
#     error unknown architecture for GCC compiler
#  endif
#endif


#endif /* _DFC_TDET_H */
/***************************************************************************************************
    end of file: tdet.h
 **************************************************************************************************/
