/***************************************************************************************************
 *   Project:       
 *   Author:        Alex Borisov
 ***************************************************************************************************
 *   Distribution:
 *
 ***************************************************************************************************
 *   MCU Family:    
 *   Compiler:      
 ***************************************************************************************************
 *   File:          types.h
 *   Description:   Types definition
 *
 ***************************************************************************************************
 *   History:       15.09.2010 - [Alex Borisov] - file created
 *
 **************************************************************************************************/

#ifndef _DFC_TYPES_H
#define _DFC_TYPES_H

#include "dfc_tdet.h"


/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

#if defined(DFC_COMPILER__MCHP_XC32)
#  include <stddef.h>
#  include <stdint.h>
#  include <stdbool.h>
#elif defined(DFC_COMPILER__KEIL_REALVIEW)
#  include <stddef.h>
#  include <stdint.h>
#  include <stdbool.h>
#else
#  include <stddef.h>
#  include <stdint.h>
#  include <stdbool.h>
#endif


/***************************************************************************************************
    Common user defined types 
 **************************************************************************************************/


#ifndef _U08_DEFINED
typedef uint8_t         U08;
#define _U08_DEFINED
#endif

#ifndef _U08_FAST_DEFINED
typedef uint_fast8_t    U08_FAST;
#define _U08_FAST_DEFINED
#endif

#ifndef _U16_DEFINED
typedef uint16_t        U16;
#define _U16_DEFINED
#endif

#ifndef _U16_FAST_DEFINED
typedef uint_fast16_t    U16_FAST;
#define _U16_FAST_DEFINED
#endif

#ifndef _U32_DEFINED
typedef uint32_t        U32;
#define _U32_DEFINED
#endif

#ifndef _U64_DEFINED
typedef uint64_t        U64;
#define _U64_DEFINED
#endif

#ifndef _S08_DEFINED
typedef int8_t          S08;
#define _S08_DEFINED
#endif

#ifndef _S08_FAST_DEFINED
typedef int_fast8_t     S08_FAST;
#define _S08_FAST_DEFINED
#endif

#ifndef _S16_DEFINED
typedef int16_t         S16;
#define _S16_DEFINED
#endif

#ifndef _S16_FAST_DEFINED
typedef int_fast16_t    S16_FAST;
#define _S16_FAST_DEFINED
#endif

#ifndef _S32_DEFINED
typedef int32_t         S32;
#define _S32_DEFINED
#endif

#ifndef _S64_DEFINED
typedef int64_t         S64;
#define _S64_DEFINED
#endif



#ifndef NULL
#define NULL  (void *)0
#endif




/***************************************************************************************************
    Useful definitions
 **************************************************************************************************/

#define DFC_SIZEOFMEMBER(s, m)         sizeof(((s *)0)->m)
#define DFC_OFFSETOF(s, m)             (size_t)&(((s *)0)->m)
#define DFC_COUNTOF(a)                 (sizeof(a) / sizeof(*(a)))
#define DFC_ALIGNOF(type)              DFC_OFFSETOF(struct { char c; type member; }, member)



#define DFC_INT_DIV(a, b)              ((a) >= 0 ? (((a) + ((b) >> 1)) / (b)) : (((a) - ((b) >> 1)) / (b)) )
#define DFC_INT_DIV__POS(a, b)         (((a) + ((b) >> 1)) / (b))
#define DFC_INT_DIV__NEG(a, b)         (((a) - ((b) >> 1)) / (b))

#define DFC_UINT_DIV(a, b)             DFC_INT_DIV__POS(a, b)

/**
 * Generates compile-time error if `cond` is zero
 */
#define DFC_STATIC_ASSERT(error_msg, cond)    { typedef int error__##error_msg [(cond) ? 1 : -1]; }



#endif /* _DFC_TYPES_H */
/***************************************************************************************************
    end of file: types.h
 **************************************************************************************************/
