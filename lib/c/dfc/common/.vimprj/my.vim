
" определяем путь к папке .vim
let s:sPath = expand('<sfile>:p:h')

"let g:proj_project_filename = s:sPath.'/.vimprojects'
"let g:indexer_projectsSettingsFilename = g:proj_project_filename

let &tabstop = 3
let &shiftwidth = 3



let g:indexer_handlePath = 0
let g:indexer_indexerListFilename = s:sPath.'/.indexer_files'

call envcontrol#set_previous()

