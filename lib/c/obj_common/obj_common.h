/***************************************************************************************************
 *   Author:        Dmitry Frank
 ***************************************************************************************************
 *   File:          
 *   Description:   
 **************************************************************************************************/

#ifndef _OBJ_COMMON_H
#define _OBJ_COMMON_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include "lib_cfg/obj_common_cfg.h"
#include "obj_common_types.h"

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

#define obj_common__subclass_pt_get_by_superclass_pt(_T_Subclass_, _superclass_field_name_, _superclass_pt_)           \
   ((_T_Subclass_ *)( (U08 *)(_superclass_pt_) - offsetof(_T_Subclass_, _superclass_field_name_) ))         \

#define EMPTY_FUNC            ((void *)obj_common__empty_func)
#define EMPTY_FUNC_WITH_ERR   ((void *)obj_common__empty_func_with_err)

#define OBJ_COMMON__CHECK_NULL(pt)  { if ((pt) == NULL){ APPL_DEB_HALT("NullPointerException"); } }

/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

void obj_common__empty_func(void);
#ifdef OBJ_COMMON__EMPTY_FUNC_ERR_ECHO
void obj_common__empty_func_with_err(void);
#endif

#endif // _OBJ_COMMON_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


