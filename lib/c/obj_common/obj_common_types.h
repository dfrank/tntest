
/*
 * Typedefs for main types.
 * You might want to change it depending on your operating system!
 */

#ifndef _OBJ_COMMON_TYPES
#define _OBJ_COMMON_TYPES

#include <limits.h>

#ifndef NULL
#	define NULL   (void *)0
#endif

#ifndef _U08_DEFINED
typedef     unsigned char       U08;
#define _U08_DEFINED
#endif

#ifndef _S08_DEFINED
typedef       signed char       S08;
#define _S08_DEFINED
#endif


#if UINT_MAX >= 0xffffffff
/*
 * 32- или 64-битная архитектура
 */

#ifndef _U16_DEFINED
typedef     unsigned short      U16;
#define _U16_DEFINED
#endif

#ifndef _S16_DEFINED
typedef       signed short      S16;
#define _S16_DEFINED
#endif

#ifndef _U32_DEFINED
typedef     unsigned int        U32;
#define _U32_DEFINED
#endif

#ifndef _S32_DEFINED
typedef       signed int        S32;
#define _S32_DEFINED
#endif

#ifndef _U64_DEFINED
typedef     unsigned long long  U64;
#define _U64_DEFINED
#endif

#ifndef _S64_DEFINED
typedef       signed long long  S64;
#define _S64_DEFINED
#endif

#ifndef CPU_WORD_SIZE
#  define CPU_WORD_SIZE    32
#endif

#else

/*
 * 8- или 16- битная архитектура
 */

#ifndef _U16_DEFINED
typedef     unsigned int        U16;
#define _U16_DEFINED
#endif

#ifndef _S16_DEFINED
typedef       signed int        S16;
#define _S16_DEFINED
#endif

#ifndef _U32_DEFINED
typedef     unsigned long       U32;
#define _U32_DEFINED
#endif

#ifndef _S32_DEFINED
typedef       signed long       S32;
#define _S32_DEFINED
#endif

#ifndef _U64_DEFINED
typedef     unsigned long long  U64;
#define _U64_DEFINED
#endif

#ifndef _S64_DEFINED
typedef       signed long long  S64;
#define _S64_DEFINED
#endif


#ifndef CPU_WORD_SIZE
#  define CPU_WORD_SIZE    16
#endif


#endif



#if 0
#ifndef _U16_DEFINED
#   if (CPU_WORD_SIZE == 8 || CPU_WORD_SIZE == 16)
typedef     unsigned int        U16;
#   else
typedef     unsigned short      U16;
#   endif
#define _U16_DEFINED
#endif

#ifndef _S16_DEFINED
#   if (CPU_WORD_SIZE == 8 || CPU_WORD_SIZE == 16)
typedef       signed int        S16;
#   else
typedef       signed short      S16;
#   endif
#define _S16_DEFINED
#endif

#ifndef _U32_DEFINED
#   if (CPU_WORD_SIZE == 8 || CPU_WORD_SIZE == 16)
typedef     unsigned long       U32;
#   else
typedef     unsigned int        U32;
#   endif
#define _U32_DEFINED
#endif

#ifndef _S32_DEFINED
#   if (CPU_WORD_SIZE == 8 || CPU_WORD_SIZE == 16)
typedef       signed long       S32;
#   else
typedef       signed int        S32;
#   endif
#define _S32_DEFINED
#endif

#ifndef _U64_DEFINED
#   if (CPU_WORD_SIZE == 8 || CPU_WORD_SIZE == 16)
typedef     unsigned long long  U64;
#   else
typedef     unsigned long long  U64;
#   endif
#define _U64_DEFINED
#endif

#ifndef _S64_DEFINED
#   if (CPU_WORD_SIZE == 8 || CPU_WORD_SIZE == 16)
typedef       signed long long  S64;
#   else
typedef       signed long long  S64;
#   endif
#define _S64_DEFINED
#endif

#endif


/*
 * following typedefs needed to optimize variables size.
 * It depends on CPU word size.
 */

#ifdef CPU_WORD_SIZE
#   if (CPU_WORD_SIZE == 8)
/*
 * CPU word size is 8 bits:
 */
#   ifndef _U08_FAST_DEFINED
typedef     U08                 U08_FAST;
#   define _U08_FAST_DEFINED
#   endif

#   ifndef _S08_FAST_DEFINED
typedef     S08                 S08_FAST;
#   define _S08_FAST_DEFINED
#   endif

#   ifndef _U16_FAST_DEFINED
typedef     U16                 U16_FAST;
#   define _U16_FAST_DEFINED
#   endif

#   ifndef _S16_FAST_DEFINED
typedef     S16                 S16_FAST;
#   define _S16_FAST_DEFINED
#   endif

#   ifndef _U32_FAST_DEFINED
typedef     U32                 U32_FAST;
#   define _U32_FAST_DEFINED
#   endif

#   ifndef _S32_FAST_DEFINED
typedef     S32                 S32_FAST;
#   define _S32_FAST_DEFINED
#   endif

#   ifndef _U64_FAST_DEFINED
typedef     U64                 U64_FAST;
#   define _U64_FAST_DEFINED
#   endif

#   ifndef _S64_FAST_DEFINED
typedef     S64                 S64_FAST;
#   define _S64_FAST_DEFINED
#   endif

#   elif CPU_WORD_SIZE == 16

/*
 * CPU word size is 16 bits:
 */
#   ifndef _U08_FAST_DEFINED
typedef     U16                 U08_FAST;
#   define _U08_FAST_DEFINED
#   endif

#   ifndef _S08_FAST_DEFINED
typedef     S16                 S08_FAST;
#   define _S08_FAST_DEFINED
#   endif

#   ifndef _U16_FAST_DEFINED
typedef     U16                 U16_FAST;
#   define _U16_FAST_DEFINED
#   endif

#   ifndef _S16_FAST_DEFINED
typedef     S16                 S16_FAST;
#   define _S16_FAST_DEFINED
#   endif

#   ifndef _U32_FAST_DEFINED
typedef     U32                 U32_FAST;
#   define _U32_FAST_DEFINED
#   endif

#   ifndef _S32_FAST_DEFINED
typedef     S32                 S32_FAST;
#   define _S32_FAST_DEFINED
#   endif

#   ifndef _U64_FAST_DEFINED
typedef     U64                 U64_FAST;
#   define _U64_FAST_DEFINED
#   endif

#   ifndef _S64_FAST_DEFINED
typedef     S64                 S64_FAST;
#   define _S64_FAST_DEFINED
#   endif

#   elif CPU_WORD_SIZE == 32

/*
 * CPU word size is 32 bits:
 */
#   ifndef _U08_FAST_DEFINED
typedef     U32                 U08_FAST;
#   define _U08_FAST_DEFINED
#   endif

#   ifndef _S08_FAST_DEFINED
typedef     S32                 S08_FAST;
#   define _S08_FAST_DEFINED
#   endif

#   ifndef _U16_FAST_DEFINED
typedef     U32                 U16_FAST;
#   define _U16_FAST_DEFINED
#   endif

#   ifndef _S16_FAST_DEFINED
typedef     S32                 S16_FAST;
#   define _S16_FAST_DEFINED
#   endif

#   ifndef _U32_FAST_DEFINED
typedef     U32                 U32_FAST;
#   define _U32_FAST_DEFINED
#   endif

#   ifndef _S32_FAST_DEFINED
typedef     S32                 S32_FAST;
#   define _S32_FAST_DEFINED
#   endif

#   ifndef _U64_FAST_DEFINED
typedef     U64                 U64_FAST;
#   define _U64_FAST_DEFINED
#   endif

#   ifndef _S64_FAST_DEFINED
typedef     S64                 S64_FAST;
#   define _S64_FAST_DEFINED
#   endif

#   elif CPU_WORD_SIZE == 64

/*
 * CPU word size is 64 bits:
 */
#   ifndef _U08_FAST_DEFINED
typedef     U64                 U08_FAST;
#   define _U08_FAST_DEFINED
#   endif

#   ifndef _S08_FAST_DEFINED
typedef     S64                 S08_FAST;
#   define _S08_FAST_DEFINED
#   endif

#   ifndef _U16_FAST_DEFINED
typedef     U64                 U16_FAST;
#   define _U16_FAST_DEFINED
#   endif

#   ifndef _S16_FAST_DEFINED
typedef     S64                 S16_FAST;
#   define _S16_FAST_DEFINED
#   endif

#   ifndef _U32_FAST_DEFINED
typedef     U64                 U32_FAST;
#   define _U32_FAST_DEFINED
#   endif

#   ifndef _S32_FAST_DEFINED
typedef     S64                 S32_FAST;
#   define _S32_FAST_DEFINED
#   endif

#   ifndef _U64_FAST_DEFINED
typedef     U64                 U64_FAST;
#   define _U64_FAST_DEFINED
#   endif

#   ifndef _S64_FAST_DEFINED
typedef     S64                 S64_FAST;
#   define _S64_FAST_DEFINED
#   endif

#   else
#      error CPU_WORD_SIZE have incorrect value.
#   endif /* CPU_WORD_SIZE */
#else
#   error CPU_WORD_SIZE is undefined
#endif



#endif /* _OBJ_COMMON_TYPES */

