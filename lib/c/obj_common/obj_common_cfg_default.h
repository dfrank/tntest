/*
 * Author: Dmitry Frank
 *
 * http://dfrank.ru
 */

#ifndef _OBJ_COMMON_CFG_H
#define _OBJ_COMMON_CFG_H

#define     OBJ_COMMON__MALLOC(size)      malloc(size)
#define     OBJ_COMMON__FREE(pt)          free(pt)
#define     OBJ_COMMON__DEB_HALT()        CSP_DEB_HALT()
//#define     OBJ_COMMON__DEB_HALT(...)     APPL_DEB_HALT(__VA_ARGS__)

#define     OBJ_COMMON__EMPTY_FUNC_ERR_ECHO()   //LOG_ERR("not implemented")

#endif /* _OBJ_COMMON_CFG_H */

