/***************************************************************************************************
 *   Project:       
 *   Author:        Alex Borisov
 ***************************************************************************************************
 *   Distribution:
 *
 ***************************************************************************************************
 *   MCU Family:    
 *   Compiler:      
 ***************************************************************************************************
 *   File:          dllist.c
 *   Description:   Double-linked circular lists
 *
 ***************************************************************************************************
 *   History:       14.09.2010 - [Alex Borisov] - file created
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "dllist.h"



/***************************************************************************************************
 * PRIVATE FUNCTIONS
 **************************************************************************************************/

static inline void _item__remove(T_DLListItem *p_item)
{
   p_item->p_prev->p_next = p_item->p_next;
   p_item->p_next->p_prev = p_item->p_prev;
}

static inline void _item__add_after(T_DLListItem *p_existing_item, T_DLListItem *p_new_item)
{
   p_new_item->p_next            = p_existing_item->p_next;
   p_new_item->p_prev            = p_existing_item;
   p_new_item->p_next->p_prev    = p_new_item;
   p_existing_item->p_next       = p_new_item;
}

static inline void _item__add_before(T_DLListItem *p_existing_item, T_DLListItem *p_new_item)
{
   p_new_item->p_next            = p_existing_item;
   p_new_item->p_prev            = p_existing_item->p_prev;
   p_new_item->p_prev->p_next    = p_new_item;
   p_existing_item->p_prev       = p_new_item;
}



/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

/**
 * 
 * 
 * 
 * @param me 
 */
void dllist__reset(T_DLList *me)
{
   me->zero_item.p_prev = me->zero_item.p_next = &me->zero_item;
}

/**
 * 
 * 
 * 
 * @param me 
 * 
 * @return bool 
 */
bool dllist__is_empty(const T_DLList *me)
{
   bool ret = false;

   if (me->zero_item.p_next == &me->zero_item && me->zero_item.p_prev == &me->zero_item){
      ret = true;
   }

   return ret;
}

int dllist__items_cnt__get(const T_DLList *me)
{
   int items_cnt = 0;
   const T_DLListItem *p_cur_item;

   for (
         p_cur_item =   me->zero_item.p_next, items_cnt = 0;
         p_cur_item != &me->zero_item;
         p_cur_item =   p_cur_item->p_next, items_cnt++
       );

   return items_cnt;
}


/**
 * 
 * 
 * 
 * @param me 
 * @param p_item 
 */
void dllist__item__add_head(T_DLList *me, T_DLListItem *p_item)
{
   _item__add_after(&me->zero_item, p_item);
}

/**
 * 
 * 
 * 
 * @param me 
 * @param p_item 
 */
void dllist__item__add_tail(T_DLList *me, T_DLListItem *p_item)
{
   _item__add_before(&me->zero_item, p_item);
}


void dllist__item__add_after(T_DLList *me, T_DLListItem *p_existing_item, T_DLListItem *p_new_item)
{
   //-- NOTE: actually, param me isn't needed for that, but let it be:
   //         probably we will store items_cnt in T_DLList one day,
   //         so, let API be more universal.

   _item__add_after(p_existing_item, p_new_item);
}

void dllist__item__add_before(T_DLList *me, T_DLListItem *p_existing_item, T_DLListItem *p_new_item)
{
   //-- NOTE: actually, param me isn't needed for that, but let it be:
   //         probably we will store items_cnt in T_DLList one day,
   //         so, let API be more universal.

   _item__add_before(p_existing_item, p_new_item);
}


/**
 * 
 * 
 * 
 * @param me 
 * 
 * @return T_DLListItem* 
 */
T_DLListItem *dllist__item__get_tail(const T_DLList *me)
{
   T_DLListItem *p_ret = NULL;

   if (me != NULL && me->zero_item.p_next != &me->zero_item){
      p_ret = me->zero_item.p_prev;
   }

   return p_ret;
}

/**
 * 
 * 
 * 
 * @param me 
 * 
 * @return T_DLListItem* 
 */
T_DLListItem *dllist__item__get_head(const T_DLList *me)
{
   T_DLListItem *p_ret = NULL;

   if (me != NULL && me->zero_item.p_prev != &me->zero_item){
      p_ret = me->zero_item.p_next;
   }

   return p_ret;
}

/**
 * 
 * 
 * 
 * @param me 
 * 
 * @return T_DLList* 
 */
T_DLListItem *dllist__item__remove_head (T_DLList *me)
{
   T_DLListItem *p_head = dllist__item__get_head(me);
   
   if (p_head != NULL){
      _item__remove(p_head);
   }

   return p_head;
}

/**
 * 
 * 
 * 
 * @param me 
 * 
 * @return T_DLListItem* 
 */
T_DLListItem *dllist__item__remove_tail (T_DLList *me)
{
   T_DLListItem *p_tail = dllist__item__get_tail(me);

   if (p_tail != NULL){
      _item__remove(p_tail);
   }

   return p_tail;
}

/**
 * 
 * 
 * 
 * @param p_item 
 */
void dllist__item__remove(T_DLList *me, T_DLListItem *p_item)
{
   //-- NOTE: actually, param me isn't needed for that, but let it be:
   //         probably we will store items_cnt in T_DLList one day,
   //         so, let API be more universal.
   _item__remove(p_item);
}

T_DLListItem *dllist__item__get_next(const T_DLList *me, const T_DLListItem *p_item, bool bool_circular)
{
   T_DLListItem *p_ret = NULL;

   if (&me->zero_item != p_item){
      p_ret = p_item->p_next;
      if (p_ret == &me->zero_item){
         p_ret = bool_circular
            ? p_ret->p_next
            : NULL;
      }
   }

   return p_ret;
}

T_DLListItem *dllist__item__get_prev(const T_DLList *me, const T_DLListItem *p_item, bool bool_circular)
{
   T_DLListItem *p_ret = NULL;

   if (&me->zero_item != p_item){
      p_ret = p_item->p_prev;
      if (p_ret == &me->zero_item){
         p_ret = bool_circular
            ? p_ret->p_prev
            : NULL;
      }
   }

   return p_ret;
}

/**
 * 
 * 
 * 
 * @param me 
 * @param p_item 
 * 
 * @return bool 
 */
bool dllist__item__is_contained (const T_DLList *me, const T_DLListItem *p_item)
{
   return (dllist__item_number__get_by_item(me, p_item) >= 0)
      ? true
      : false
      ;
}

T_DLListItem *dllist__item__get_by_number(const T_DLList *me, int item_num)
{
   T_DLListItem *p_cur_item, *p_ret = NULL;
   int cur_item_num;

   for (
         p_cur_item =   me->zero_item.p_next, cur_item_num = 0;
         p_cur_item != &me->zero_item;
         p_cur_item =   p_cur_item->p_next, cur_item_num++
       )
   {
      if (cur_item_num == item_num){
         //-- found an item: break and return its number.
         p_ret = p_cur_item;
         break;
      }
   }

   return p_ret;
}

int dllist__item_number__get_by_item(const T_DLList *me, const T_DLListItem *p_item)
{
   int result = 0;
   const T_DLListItem *p_cur_item;

   for (
         p_cur_item =   me->zero_item.p_next, result = 0;
         p_cur_item != &me->zero_item;
         p_cur_item =   p_cur_item->p_next, result++
       )
   {
      if (p_cur_item == p_item){
         //-- found an item: break and return its number.
         break;
      }
   }

   if (p_cur_item != p_item){
      //-- reached the end of the list, and haven't found item yet: return -1
      //   which means that list does not contain given item.
      result = -1;
   }

   return result;
}

/***************************************************************************************************
 *  end of file: dllist.c
 **************************************************************************************************/


