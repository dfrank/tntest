/***************************************************************************************************
 * TODO: description
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "ptarr.h"
#include <string.h>

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

/***************************************************************************************************
 *                                  PRIVATE METHODS PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                  EXTERNAL METHODS PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                      PROTECTED METHODS
 **************************************************************************************************/

/***************************************************************************************************
 *                                        PRIVATE METHODS
 **************************************************************************************************/

/**
 * NOTE: summands can be NULL: these values will be just ignored.
 */
static void _sum__ctor(T_PtArr *me, int summands_cnt, va_list argptr)
{
   void **pp_my_cur_pt = ptarr__data__get_pt(me);

   int cur_item_num = 0;

   for ( ; summands_cnt > 0; summands_cnt--){
      T_PtArr *p_cur_ptarr = va_arg(argptr, T_PtArr *);
      
      if (p_cur_ptarr != NULL){
         void **pp_cur_pt = ptarr__data__get_pt(p_cur_ptarr);
         int i;
         for (i = 0; i < ptarr__len__get(p_cur_ptarr); i++){
            if (cur_item_num < ptarr__len__get(me)){
               *(pp_my_cur_pt++) = *(pp_cur_pt++);
            } else {
               DTMSG_E("too many items: len=%d, cur_item_num=%d",
                     (PWORD)ptarr__len__get(me),
                     (PWORD)cur_item_num
                     );
            }
            cur_item_num++;
         }
      }
   }
}

/***************************************************************************************************
 *                                    CONSTRUCTOR, DESTRUCTOR                                      *
 **************************************************************************************************/

/**
 * Constructor
 *
 */
T_PtArr_Res ptarr__ctor(T_PtArr *me, const T_PtArr_CtorParams *p_params)
{
   T_PtArr_Res ret = PTARR_RES__OK;
   //memset(me, 0x00, sizeof(T_PtArr));

   //-- some construct code

   me->length = p_params->length;

   if (ret == PTARR_RES__OK){
      //me->flags |= _PTARR__STATE_FLAG__INIT;
      
   }
   return ret;
}

void ptarr__ctor__copy(T_PtArr *me, const T_PtArr *p_src)
{
   memcpy(me, p_src, sizeof(T_PtArr) + ptarr__len__get(p_src) * sizeofmember(T_PtArr, data[0]));
}


/**
 * Desctructor
 */
void ptarr__dtor(T_PtArr *me)
{
   //if (me->flags & _PTARR__STATE_FLAG__INIT){
      // some desctruct code
   //}
}

/***************************************************************************************************
 *                                          PUBLIC METHODS                                         *
 **************************************************************************************************/

/**
 * Checks if array contains specified value.
 * If it does, then its index is returned, otherwise -1 is returned.
 * @param check_items_cnt if not equal -1, then only first check_items_cnt items will be checked,
 *                        otherwise all me->length will be used
 */
S16 ptarr__find(T_PtArr *me, void *pt, size_t check_items_cnt)
{
   S16 ret = -1;

   if (check_items_cnt == -1){
      check_items_cnt = me->length;
   }

   S16 cur_item_num = 0;
   while (check_items_cnt-- > 0){
      if (me->data[cur_item_num] == pt){
         ret = cur_item_num;
         break;
      }
      cur_item_num++;
   }

   return ret;
}

/***************************************************************************************************
 *                                      ALLOCATOR, DEALLOCATOR                                     *
 **************************************************************************************************/

/*
 * Allocator and Deallocator
 *
 * Please NOTE: There's not necessary to use them! 
 * For instance, you can just allocate in stack or statically or manually from heap,
 * and use _ctor only.
 */
#if defined OBJ_COMMON__MALLOC && defined OBJ_COMMON__FREE

/* allocator */
T_PtArr *new_ptarr(const T_PtArr_CtorParams *p_params)
{
   T_PtArr *me = (T_PtArr *)OBJ_COMMON__MALLOC( sizeof(T_PtArr) + p_params->length * sizeofmember(T_PtArr, data[0]));
   ptarr__ctor(me, p_params);
   return me;
}

T_PtArr *new_ptarr__simple(size_t len)
{
   T_PtArr *me = (T_PtArr *)OBJ_COMMON__MALLOC( sizeof(T_PtArr) + len * sizeofmember(T_PtArr, data[0]));

   const T_PtArr_CtorParams par = {
      .length = len,
   };
   ptarr__ctor(me, &par);

   return me;
}

T_PtArr *new_ptarr__copy(const T_PtArr *p_src)
{
   T_PtArr *me = (T_PtArr *)OBJ_COMMON__MALLOC( sizeof(T_PtArr) + ptarr__len__get(p_src) * sizeofmember(T_PtArr, data[0]));
   ptarr__ctor__copy(me, p_src);
   return me;
}

/**
 * NOTE: summands can be NULL: these values will be just ignored.
 */
T_PtArr *new_ptarr__sum(int summands_cnt, ...)
{
   T_PtArr *p_ret = NULL;
   va_list argptr;


   {
      int items_cnt = 0;

      {
         va_start(argptr, summands_cnt);
         int cur_summand_num = 0;
         for (cur_summand_num = 0; cur_summand_num < summands_cnt; cur_summand_num++){
            T_PtArr *p_cur_ptarr = va_arg(argptr, T_PtArr *);
            if (p_cur_ptarr != NULL){
               items_cnt += ptarr__len__get(p_cur_ptarr);
            }
         }
         va_end(argptr);
      }

      if (items_cnt > 0){
         p_ret = new_ptarr__simple(items_cnt);
         va_start(argptr, summands_cnt);
         _sum__ctor(p_ret, summands_cnt, argptr);
         va_end(argptr);
      }
   }

   return p_ret;
}

/* deallocator */
void delete_ptarr(T_PtArr *me){
   ptarr__dtor(me);
   OBJ_COMMON__FREE(me);
}
#endif



/***************************************************************************************************
 *  end of file
 **************************************************************************************************/


