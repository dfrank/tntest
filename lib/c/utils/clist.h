/***************************************************************************************************
 *   Project:       P_0172_FW01 - 
 *   Author:        
 ***************************************************************************************************
 *   Distribution:
 *
 ***************************************************************************************************
 *   MCU Family:    
 *   Compiler:      
 ***************************************************************************************************
 *   File:          clist.h
 *   Description:   
 *
 ***************************************************************************************************
 *   History:       14.09.2010 - [] - file created
 *
 **************************************************************************************************/

#ifndef _CLIST_H
#define _CLIST_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include <csp_types.h>

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

typedef struct _CLIST
{
    struct _CLIST * prev;
    struct _CLIST * next;
} CLIST;

/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

void        clist_reset(CLIST *clist);
bool        clist_is_empty(const CLIST *clist);

void        clist_add_head(CLIST *clist, CLIST *node);
void        clist_add_tail(CLIST *clist, CLIST *node);

void        clist_add_after (CLIST *p_existing_node, CLIST *p_new_node);
void        clist_add_before (CLIST *p_existing_node, CLIST *p_new_node);

CLIST *     clist_get_head(const CLIST *clist);
CLIST *     clist_get_tail(const CLIST *clist);

CLIST *     clist_remove_head(CLIST *clist);
CLIST *     clist_remove_tail(CLIST *clist);

void        clist_remove_entry(CLIST *node);
bool        clist_is_contains (const CLIST *clist, const CLIST *node);
int         clist_get_entry_number(const CLIST *clist, const CLIST *node);

CLIST *clist_get_next(const CLIST *p_clist, const CLIST *p_node, bool bool_circular);
CLIST *clist_get_prev(const CLIST *p_clist, const CLIST *p_node, bool bool_circular);

#endif /* _CLIST_H */
/***************************************************************************************************
    end of file: clist.h
 **************************************************************************************************/
