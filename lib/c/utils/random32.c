/*
  This is a maximally equidistributed combined Tausworthe generator
  based on code from GNU Scientific Library 1.5 (30 Jun 2004)

   x_n = (s1_n ^ s2_n ^ s3_n)

   s1_{n+1} = (((s1_n & 4294967294) <<12) ^ (((s1_n <<13) ^ s1_n) >>19))
   s2_{n+1} = (((s2_n & 4294967288) << 4) ^ (((s2_n << 2) ^ s2_n) >>25))
   s3_{n+1} = (((s3_n & 4294967280) <<17) ^ (((s3_n << 3) ^ s3_n) >>11))

   The period of this generator is about 2^88.

   From: P. L'Ecuyer, "Maximally Equidistributed Combined Tausworthe
   Generators", Mathematics of Computation, 65, 213 (1996), 203--213.

   This is available on the net from L'Ecuyer's home page,

   http://www.iro.umontreal.ca/~lecuyer/myftp/papers/tausme.ps
   ftp://ftp.iro.umontreal.ca/pub/simulation/lecuyer/papers/tausme.ps

   There is an erratum in the paper "Tables of Maximally
   Equidistributed Combined LFSR Generators", Mathematics of
   Computation, 68, 225 (1999), 261--269:
   http://www.iro.umontreal.ca/~lecuyer/myftp/papers/tausme2.ps

        ... the k_j most significant bits of z_j must be non-
        zero, for each j. (Note: this restriction also applies to the
        computer code given in [4], but was mistakenly not mentioned in
        that paper.)

   This affects the seeding procedure by imposing the requirement
   s1 > 1, s2 > 7, s3 > 15.

*/


#include <stdio.h>
#include "random32.h"


typedef struct
{
        int32t s1, s2, s3;
} rnd_state;

rnd_state prnd;

/***************************************************************************************************
 *                                         PRIVATE FUNCTIONS
 **************************************************************************************************/

static int32t __random32(rnd_state *state)
{
    #define TAUSWORTHE(s,a,b,c,d) ((s&c)<<d) ^ (((s <<a) ^ s)>>b)

    state->s1 = TAUSWORTHE(state->s1, 13, 19, 4294967294UL, 12);
    state->s2 = TAUSWORTHE(state->s2,  2, 25, 4294967288UL,  4);
    state->s3 = TAUSWORTHE(state->s3,  3, 11, 4294967280UL, 17);
    return (state->s1 ^ state->s2 ^ state->s3);
}

/*
 * Handle minimum values for seeds
 */
static inline int32t __seed(int32t x, int32t m)
{
        return (x < m) ? x + m : x;
}

/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

/**
 *      random32 - pseudo random number generator
 *
 *      A 32 bit pseudo-random number is generated using a fast
 *      algorithm suitable for simulation.
 */
int32t random32 (void)
{
        return __random32(&prnd);
}

/**
 *      srandom32 - add entropy to pseudo random number generator
 *      @seed: seed value
 *
 *      Add some additional seeding to the random32() pool.
 */
void srandom32 (int32t entropy)
{
        prnd.s1 = __seed(prnd.s1 ^ entropy, 1);
}


/*
 *      Generate some initially weak seeding values to allow
 *      to start the random32() engine.
 */
void random32_init (int32t i)
{
        #define LCG(x)  ((x) * 69069)

        prnd.s1 = __seed(LCG(i),        1);
        prnd.s2 = __seed(LCG(prnd.s1),  7);
        prnd.s3 = __seed(LCG(prnd.s2), 15);

        /* "warm it up" */
        __random32(&prnd);
        __random32(&prnd);
        __random32(&prnd);
        __random32(&prnd);
        __random32(&prnd);
        __random32(&prnd);
}

/***************************************************************************************************
 *  end of file: random32.c
 **************************************************************************************************/

