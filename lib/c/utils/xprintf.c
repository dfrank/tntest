/***************************************************************************************************
 *   Project:       U_0148 - Formatted Output
 *   Author:        Alex B.
 ***************************************************************************************************
 *   Distribution:
 * 
 *   This file is part of BeRTOS.
 * 
 * Bertos is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
 * MA  02110-1301  USA
 *
 * As a special exception, you may use this file as part of a free software library without
 * restriction.  Specifically, if other files instantiate templates or use macros or inline
 * functions from this file, or you compile this file and link it with other files to produce
 * an executable, this file does not by itself cause the resulting executable to be covered by
 * the GNU General Public License.  This exception does not however invalidate any other reasons
 * why the executable file might be covered by the GNU General Public License.
 *
 * Copyright 2003, 2004, 2005, 2008 Develer S.r.l. (http://www.develer.com/)
 *
 ***************************************************************************************************
 *   MCU Family:    PIC24/dsPIC
 *   Compiler:      MCHP C30 3.12
 ***************************************************************************************************
 *   File:          xprintf.c
 *   Description:   Basic "printf", "sprintf" and "fprintf" formatter.
 * 
 * This module is 100% reentrant and can be adapted to user-defined routines that needs formatters
 * with special properties like different output channels or new format specifiers.
 *
 * To reduce size in applications not using real numbers or long integers the formatter may be
 * compiled to exclude certain parts.  This is controlled by giving a -D option a compilation time:
 *
 * \code
 *  -D CONFIG_PRINTF=PRINTF_FULL         Full ANSI printf formatter, with some C99 extensions
 *  -D CONFIG_PRINTF=PRINTF_NOFLOAT      Exclude support for floats
 *  -D CONFIG_PRINTF=PRINTF_REDUCED      Simplified formatter (see below)
 *  -D CONFIG_PRINTF=PRINTF_NOMODIFIERS  Exclude 'l', 'z' and 'h' modifiers in reduced version
 *  -D CONFIG_PRINTF=PRINTF_DISABLED     No formatter at all
 * \endcode
 *
 * Code size on AVR4 with GCC 3.4.1 (-O2):
 *   PRINTF_FULL        2912byte (0xB60)
 *   PRINTF_NOFLOAT     1684byte (0x694)
 *   PRINTF_REDUCED      924byte (0x39C)
 *   PRINTF_NOMODIFIERS  416byte (0x1A0)
 *
 * Code/data size in words on DSP56K with CodeWarrior 6.0:
 *   PRINTF_FULL         1493/45
 *   PRINTF_NOFLOAT      795/45
 *   PRINTF_REDUCED      482/0
 *   PRINTF_NOMODIFIERS  301/0
 *
 * The reduced version of formatter is suitable when program size is critical rather than formatting
 * power.  This routine uses less than 20 bytes of stack space which makes it practical even in
 * systems with less than 256 bytes of user RAM.
 *
 * The only formatting specifiers supported by the reduced formatter are:
 * \code
 *    %% %c %s %d %o %x %X and %hd %ho %hx %hX %ld %lo %lx %lX
 * \endcode
 *
 * It means that real variables are not supported as well as field width and precision arguments. 
 *
 ***************************************************************************************************
 *   History:       18.05.2009 - [Alex B.] - file created
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "xprintf.h"
#include "xprintf_conf.h"           /* Application Depended */

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

/* True if we must keep a count of the number of characters we print. */

#define CONFIG_PRINTF_COUNT_CHARS (CONFIG_PRINTF_RETURN_COUNT || CONFIG_PRINTF_N_FORMATTER)

#ifndef CONFIG_PRINTF_RETURN_COUNT
    #define CONFIG_PRINTF_RETURN_COUNT 0        /** Enable/disable _formatted_write return value */
#endif

#ifndef CONFIG_PRINTF_N_FORMATTER
    #define CONFIG_PRINTF_N_FORMATTER 0         /** Disable the arcane %n formatter. */
#endif

#ifndef CONFIG_PRINTF_OCTAL_FORMATTER
    #define CONFIG_PRINTF_OCTAL_FORMATTER 0     /** Disable the %o formatter. */
#endif

#ifndef CONFIG_PRINTF_FIXED_POINT_INT
    #define CONFIG_PRINTF_FIXED_POINT_INT 0     /** Disable the ',' formatter (fixed-point int). */
#endif


#if CONFIG_PRINTF

const char HEX_tab[16] = { '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F' };
const char hex_tab[16] = { '0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f' };

#if CONFIG_PRINTF > PRINTF_NOFLOAT

    /* Include float formatting */
    #include <float.h>

    /* Maximum precision for floating point values */
    typedef long double max_float_t;

    /*bernie: save some memory, who cares about floats with lots of decimals? */
    #define FRMWRI_BUFSIZE 64

    /* #warning 134 is too much, the code must be fixed to have a lower precision limit */
#else
    /*
      Conservative estimate. Should be (probably) 12 (which is the size necessary to represent
      (2^32-1) in octal plus the sign bit.
    */
    #define FRMWRI_BUFSIZE 16
#endif


#if CONFIG_PRINTF > PRINTF_NOMODIFIERS
    #define IS_SHORT (h_modifier || (sizeof(int) == 2 && !l_modifier))
#else
    #define IS_SHORT (sizeof(int) == 2)
#endif

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/
/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/
/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/
/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/
/***************************************************************************************************
 *                                  PRIVATE FUNCTION PROTOTYPES
 **************************************************************************************************/
/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/
/***************************************************************************************************
 *                                        PRIVATE FUNCTIONS
 **************************************************************************************************/

#if CONFIG_PRINTF > PRINTF_NOFLOAT

/**
 * 
 *
 * @param value 
 * @param nr_of_digits 
 * @param buf 
 * @param format_flag 
 * @param g_flag 
 * @param alternate_flag 
 * 
 * @return char* 
 */
static char *float_conversion(max_float_t value,
                              short nr_of_digits,
                              char *buf,
                              char format_flag,
                              char g_flag,
                              unsigned int alternate_flag
                             )
{
    char  *cp;
    char  *buf_pointer;
    short n, i, dec_point_pos, integral_10_log;

    buf_pointer = buf;
    integral_10_log = 0;

    if (value >= 1)
    {
        while (value >= 1e11)       /* To speed up things a bit */
        {
            value /= 1e10;
            integral_10_log += 10;
        }
        while (value >= 10)
        {
            value /= 10;
            integral_10_log++;
        }
    }
    else if (value)                 /* Not just 0.0 */
    {
        while (value <= 1e-10)      /* To speed up things a bit */
        {
            value *= 1e10;
            integral_10_log -= 10;
        }
        while (value < 1)
        {
            value *= 10;
            integral_10_log--;
        }
    }

    if (g_flag)
    {
        if (integral_10_log < nr_of_digits && integral_10_log >= -4)
        {
            format_flag = 0;
            nr_of_digits -= integral_10_log;
        }
        nr_of_digits--;
        if (alternate_flag)
            g_flag = 0;             /* %#G - No removal of trailing zeros */
        else
            alternate_flag = 1;     /* %G - Removal of trailing zeros */
    }

    /* %e or %E */

    if (format_flag)
    {
        dec_point_pos = 0;
    }
    else
    {
        /* Less than one... */
        if (integral_10_log < 0)
        {
            *buf_pointer++ = '0';
            if ((n = nr_of_digits) || alternate_flag)
                *buf_pointer++ = '.';
            i = 0;
            while (--i > integral_10_log && nr_of_digits)
            {
                *buf_pointer++ = '0';
                nr_of_digits--;
            }
            if (integral_10_log < (-n - 1))
                goto CLEAN_UP;      /* Nothing more to do */

            dec_point_pos = 1;
        }
        else
        {
            dec_point_pos = - integral_10_log;
        }
    }

    i = dec_point_pos;
    while (i <= nr_of_digits )
    {
        value -= (max_float_t)(n = (short)value);   /* n = Digit value = Remainder */
        value *= 10;                                /* Prepare for next shot */
        *buf_pointer++ = n + '0';

        if ( ! i++ && (nr_of_digits || alternate_flag))
            *buf_pointer++ = '.';
    }

    /* Rounding possible */

    if (value >= 5)
    {
        n  = 1; /* Carry */
        cp = buf_pointer - 1;

        do
        {
            if (*cp != '.')
            {
                if ( (*cp += n) == ('9' + 1) )
                {
                    *cp = '0';
                    n = 1;
                }
                else
                    n = 0;
            }
        } while (cp-- > buf);

        if (n)
        {
            /* %e or %E */
            if (format_flag)
            {
                cp = buf_pointer;
                while (cp > buf)
                {
                    if (*(cp - 1) == '.')
                    {
                        *cp = *(cp - 2);
                        cp--;
                    }
                    else
                        *cp = *(cp - 1);
                    cp--;
                }
                integral_10_log++;
            }
            else
            {
                cp = ++buf_pointer;
                while (cp > buf)
                {
                    *cp = *(cp - 1);
                    cp--;
                }
            }
            *buf = '1';
        }
    }

  CLEAN_UP:

    /* %G - Remove trailing zeros */
    if (g_flag)
    {
        while (*(buf_pointer - 1) == '0')
            buf_pointer--;
        if (*(buf_pointer - 1) == '.')
            buf_pointer--;
    }

    /* %e or %E */
    if (format_flag)
    {
        *buf_pointer++ = format_flag;
        if (integral_10_log < 0)
        {
            *buf_pointer++ = '-';
            integral_10_log = -integral_10_log;
        }
        else
            *buf_pointer++ = '+';
        n = 0;
        buf_pointer +=10;
        do
        {
            n++;
            *buf_pointer++ = (integral_10_log % 10) + '0';
            integral_10_log /= 10;
        } while ( integral_10_log || n < 2 );
        for ( i = n ; n > 0 ; n-- )
            *(buf_pointer - 11 - i + n) = *(buf_pointer - n);
        buf_pointer -= 10;
    }
    return(buf_pointer);
}

#endif /* CONFIG_PRINTF > PRINTF_NOFLOAT */

/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

/**
 * 
 *
 * @param format
 * @param put_one_char
 * @param secret_pointer 
 * @param ap 
 * 
 * @return int 
 */
size_t _formatted_write(const char * format,
                        void put_one_char(char, void *),
                        void *secret_pointer,
                        va_list ap)
{

    /* NORMAL FORMATTER */
    /* ---------------- */


#if CONFIG_PRINTF > PRINTF_REDUCED

    int           precision;
#if CONFIG_PRINTF_FIXED_POINT_INT
    int           fp_precision;
#endif
    int           n;
    int           field_width;
    char          format_flag;
    unsigned int  div_factor;
    unsigned long ulong;

    char          *buf_pointer;
    char          *ptr;
    const char    *hex;
    char          buf[FRMWRI_BUFSIZE];

#if CONFIG_PRINTF_COUNT_CHARS
    int  nr_of_chars = 0;
#endif

    enum PLUS_SPACE_FLAGS
    {
        PSF_NONE,
        PSF_PLUS,
        PSF_MINUS
    };

    struct
    {
        enum PLUS_SPACE_FLAGS plus_space_flag : 2;
        unsigned left_adjust                  : 1;
        unsigned l_L_modifier                 : 1;
        unsigned h_modifier                   : 1;
        unsigned alternate_flag               : 1;
        unsigned nonzero_value                : 1;
        unsigned zeropad                      : 1;
#if CPU_HARVARD
        unsigned progmem                      : 1;
#endif
    } flags;

#if CONFIG_PRINTF >  PRINTF_NOFLOAT
    max_float_t fvalue;
#endif

    /* Parsing */
    /* ------- */

    for (;;)    /* Until full format string read */
    {
        while (((format_flag = PGM_GET_CHAR(format++))) != '%')    /* Until '%' or '\0' */
        {
            if (!format_flag)
#if CONFIG_PRINTF_RETURN_COUNT
                return(nr_of_chars);
#else
                return 0;
#endif
            put_one_char(format_flag, secret_pointer);
#if CONFIG_PRINTF_COUNT_CHARS
            nr_of_chars++;
#endif
        }

        if (PGM_GET_CHAR(format) == '%')                         /* %% prints as % */
        {
            format++;
            put_one_char('%', secret_pointer);
#if CONFIG_PRINTF_COUNT_CHARS
            nr_of_chars++;
#endif
            continue;                               /* next symbol */
        }


        ptr = buf_pointer     = &buf[0];
        hex                   = HEX_tab;

        flags.left_adjust     = 0;
        flags.alternate_flag  = 0;
        flags.plus_space_flag = PSF_NONE;
        flags.zeropad         = 0;
#if CPU_HARVARD
        flags.progmem         = 0;
#endif

        /* check for leading '-', '+', ' ','#' or '0' flags  */

        for (;;)
        {
            switch (PGM_GET_CHAR(format))
            {
                case ' ':
                    if (flags.plus_space_flag != PSF_NONE)
                        goto NEXT_FLAG;
                case '+':
                    flags.plus_space_flag = PSF_PLUS;
                    goto NEXT_FLAG;
                case '-':
                    flags.left_adjust     = 1;
                    goto NEXT_FLAG;
                case '#':
                    flags.alternate_flag  = 1;
                    goto NEXT_FLAG;
                case '0':
                    flags.zeropad         = 1;
                    goto NEXT_FLAG;
            }
            break;
          NEXT_FLAG:
            format++;
        }

        /* Optional field width (may be '*') */

        if (PGM_GET_CHAR(format) == '*')
        {
            field_width = va_arg(ap, int);
            if (field_width < 0)
            {
                field_width = -field_width;
                flags.left_adjust = 1;
            }
            format++;
        }
        else
        {
            field_width = 0;
            while (PGM_GET_CHAR(format) >= '0' && PGM_GET_CHAR(format) <= '9')
                field_width = field_width * 10 + (PGM_GET_CHAR(format++) - '0');
        }

        if (flags.left_adjust)
            flags.zeropad = 0;

        /* Optional precision (or '*') */

        if (PGM_GET_CHAR(format) == '.')
        {
            if (PGM_GET_CHAR(++format) == '*')
            {
                precision = va_arg(ap, int);
                format++;
            }
            else
            {
                precision = 0;
                while (PGM_GET_CHAR(format) >= '0' && PGM_GET_CHAR(format) <= '9')
                    precision = precision * 10 + (PGM_GET_CHAR(format++) - '0');
            }
        }
        else
            precision = -1;

#if CONFIG_PRINTF_FIXED_POINT_INT
        if (PGM_GET_CHAR(format) == ',')
        {
            if (PGM_GET_CHAR(++format) == '*')
            {
                fp_precision = va_arg(ap, int);
                format++;
            }
            else
            {
                fp_precision = 0;
                while (PGM_GET_CHAR(format) >= '0' && PGM_GET_CHAR(format) <= '9')
                    fp_precision = fp_precision * 10 + (PGM_GET_CHAR(format++) - '0');
            }

            if (fp_precision == 0){
                fp_precision = -1;
            }

        }
        else
            fp_precision = -1;
#endif


        /* At this point, "left_adjust" is nonzero if there was a sign, "zeropad" is 1 if there
           was a leading zero and 0 otherwise, "field_width" and "precision" contain numbers
           corresponding to the digit strings before and after the decimal point, respectively,
           and "plus_space_flag" is either 0 (no flag) or contains a plus or space character.
           If there was no decimal point, "precision" will be -1.
         */

        flags.l_L_modifier = 0;
        flags.h_modifier   = 0;

        /* Optional 'l','L','z' or 'h' modifier? */

        switch (PGM_GET_CHAR(format))
        {
            case 'l':
            case 'L':
            case 'z':
                flags.l_L_modifier = 1;
                format++;
                break;
            case 'h':
                flags.h_modifier = 1;
                format++;
                break;
        }

        /*
           At exit from the following switch, we will emit the characters starting at "buf_pointer"
           and ending at "ptr"-1
         */

        switch (format_flag = PGM_GET_CHAR(format++))
        {

#if CONFIG_PRINTF_N_FORMATTER
            case 'n':
                if (sizeof(short) != sizeof(int))
                {
                    if (sizeof(int) != sizeof(long))
                    {
                        if (flags.h_modifier)
                            *va_arg(ap, short *) = nr_of_chars;
                        else if (flags.l_L_modifier)
                            *va_arg(ap, long *)  = nr_of_chars;
                        else
                            *va_arg(ap, int *)   = nr_of_chars;
                    }
                    else
                    {
                        if (flags.h_modifier)
                            *va_arg(ap, short *) = nr_of_chars;
                        else
                            *va_arg(ap, int *)   = nr_of_chars;
                    }
                }
                else
                {
                    if (flags.l_L_modifier)
                        *va_arg(ap, long *) = nr_of_chars;
                    else
                        *va_arg(ap, int *)  = nr_of_chars;
                }
                continue;
#endif
            case 'c':
                buf[0] = va_arg(ap, int);
                ptr++;
                break;

                /* Custom formatter for strings in program memory. */
            case 'S':
#if CPU_HARVARD
                flags.progmem = 1;
#endif
                /* Fall trough */

            case 's':
                if ( !(buf_pointer = va_arg(ap, char *)) )
                    buf_pointer = "<NULL>";//null_pointer;
                if (precision < 0)
                    precision = 10000;

                /* Move `ptr' to the last character of the string that will be actually printed. */

                ptr = buf_pointer;
#if CPU_HARVARD
                if (flags.progmem)
                {
                    for (n = 0; pgm_get_char(ptr) && n < precision; n++)
                        ++ptr;
                }
                else
#endif
                    for (n = 0; *ptr && n < precision; n++)
                        ++ptr;
                break;

#if CONFIG_PRINTF_OCTAL_FORMATTER
            case 'o':
                if (flags.alternate_flag && !precision)
                    precision++;
#endif
            case 'x':
                hex = hex_tab;
            case 'u':
            case 'p':
            case 'X':
                if (format_flag == 'p')
                {   
#if CPU_32BIT_POINTER
                    ulong = (unsigned long)va_arg(ap, char *);
#else
                    ulong = (unsigned long)(unsigned int)va_arg(ap, char *);
#endif
                }
                else if (flags.l_L_modifier)
                    ulong = va_arg(ap, unsigned long);
                else if (flags.h_modifier)
                    ulong = (unsigned long)(unsigned short)va_arg(ap, unsigned int);
                else
                    ulong = va_arg(ap, unsigned int);

                div_factor = 
#if CONFIG_PRINTF_OCTAL_FORMATTER
                (format_flag == 'o') ? 8 :
#endif
                (format_flag == 'u') ? 10 : 16;

                flags.plus_space_flag = PSF_NONE;
                goto INTEGRAL_CONVERSION;

            case 'd':
            case 'i':
                if (flags.l_L_modifier)
                    ulong = (unsigned long)(long)va_arg(ap, long);
                else
                    ulong = (unsigned long)(long)va_arg(ap, int);

                /* Extract sign */
                if ((signed long)ulong < 0)
                {
                    flags.plus_space_flag = PSF_MINUS;
                    ulong = (unsigned long)(-((signed long)ulong));
                }

                div_factor = 10;

                /* Now convert to digits */

              INTEGRAL_CONVERSION:
                ptr = buf_pointer = &buf[FRMWRI_BUFSIZE - 1];
                flags.nonzero_value = (ulong != 0);

                /* No char if zero and zero precision */
                if (precision != 0 || flags.nonzero_value)
                {
                    do {
                        *--buf_pointer = hex[ulong % div_factor];
#if CONFIG_PRINTF_FIXED_POINT_INT
                        if (fp_precision == (int)(ptr - buf_pointer)){
                            //-- write fixed point
                            *--buf_pointer = '.';
                        }
#endif
                    } while (ulong /= div_factor);
                }

                /* "precision" takes precedence */
                if (precision < 0)
                    if (flags.zeropad)
                        precision = field_width - (flags.plus_space_flag != PSF_NONE);
                while (
                        precision > (int)(ptr - buf_pointer)
#if CONFIG_PRINTF_FIXED_POINT_INT
                        || (fp_precision + 1 + 1/*for '.' char*/) > (int)(ptr - buf_pointer)
#endif
                        ){
                    *--buf_pointer = '0';
#if CONFIG_PRINTF_FIXED_POINT_INT
                    if (fp_precision == (int)(ptr - buf_pointer)){
                        //-- write fixed point
                        *--buf_pointer = '.';
                    }
#endif
                }

                if (flags.alternate_flag && flags.nonzero_value)
                {
                    if (format_flag == 'x' || format_flag == 'X')
                    {
                        *--buf_pointer = format_flag;
                        *--buf_pointer = '0';
                    }
#if CONFIG_PRINTF_OCTAL_FORMATTER
                    else if ((format_flag == 'o') && (*buf_pointer != '0'))
                    {
                        *--buf_pointer = '0';
                    }
#endif
                }
                break;

#if CONFIG_PRINTF > PRINTF_NOFLOAT
            case 'g':
            case 'G':
                n = 1;
                format_flag -= 2;
                if (! precision)
                {
                    precision = 1;
                }
                goto FLOATING_CONVERSION;

            case 'f':
                format_flag = 0;

            case 'e':
            case 'E':
                n = 0;
              FLOATING_CONVERSION:
                if (precision < 0)
                {
                    precision = 6;      /* Default */
                }

                if (sizeof(double) != sizeof(max_float_t))
                {
                    fvalue = flags.l_L_modifier ? va_arg(ap,max_float_t) : va_arg(ap, double);
                }
                else
                    fvalue = va_arg(ap,max_float_t);

                if (fvalue < 0)
                {
                    flags.plus_space_flag = PSF_MINUS;
                    fvalue                = -fvalue;
                }

                ptr = float_conversion (fvalue,
                                        (short)precision,
                                        buf_pointer += field_width,
                                        format_flag,
                                        (char)n,
                                        flags.alternate_flag);
                if (flags.zeropad)
                {
                    precision = field_width - (flags.plus_space_flag != PSF_NONE);
                    while (precision > ptr - buf_pointer)
                        *--buf_pointer = '0';
                }
                break;
#endif

            case '\0': /* Really bad place to find NUL in */
                format--;

            default:

                /* Undefined conversion! */
                ptr = buf_pointer = "???";      // bad_conversion;
                ptr += 3;
                break;

        }

        /*
         * This part emittes the formatted string to "put_one_char".
         */

        /* If field_width == 0 then nothing should be written. */

        precision = ptr - buf_pointer;

        if ( precision > field_width)
        {
            n = 0;
        }
        else
        {
            n = field_width - precision - (flags.plus_space_flag != PSF_NONE);
        }

        /* emit any leading pad characters */

        if (!flags.left_adjust)
            while (--n >= 0)
            {
                put_one_char(' ', secret_pointer);
#if CONFIG_PRINTF_COUNT_CHARS
                nr_of_chars++;
#endif
            }

        /* emit flag characters (if any) */

        if (flags.plus_space_flag)
        {
            put_one_char(flags.plus_space_flag == PSF_PLUS ? '+' : '-', secret_pointer);
#if CONFIG_PRINTF_COUNT_CHARS
            nr_of_chars++;
#endif
        }

#if CPU_HARVARD
        if (flags.progmem)
        {
            while (--precision >= 0)
            {
                put_one_char(pgm_get_char(buf_pointer++), secret_pointer);
#if CONFIG_PRINTF_COUNT_CHARS
                nr_of_chars++;
#endif
            }
        }
        else
#endif /* CPU_HARVARD */
        {
            /* emit the string itself */
            while (--precision >= 0)
            {
                put_one_char(*buf_pointer++, secret_pointer);
#if CONFIG_PRINTF_COUNT_CHARS
                nr_of_chars++;
#endif
            }
        }

        /* emit trailing space characters */

        if (flags.left_adjust)
            while (--n >= 0)
            {
                put_one_char(' ', secret_pointer);
#if CONFIG_PRINTF_COUNT_CHARS
                nr_of_chars++;
#endif
            }
    }

#else /* PRINTF_REDUCED starts here */


    /* REDUCED FORMATTER */
    /* ---------------- */

    char          format_flag;
    unsigned int  base;

    char          outChar;
    char          *ptr;

#if CONFIG_PRINTF > PRINTF_NOMODIFIERS
    unsigned int  l_modifier;
    unsigned int  h_modifier;
    unsigned long u_val, div_val;
#else
    unsigned int  u_val, div_val;
#endif

#if CONFIG_PRINTF_COUNT_CHARS
    unsigned int nr_of_chars = 0;
#endif


    for (;;)    /* Until full format string read */
    {
        while ((format_flag = PGM_GET_CHAR(format++)) != '%')    /* Until '%' or '\0' */
        {
            if (!format_flag)
#if CONFIG_PRINTF_COUNT_CHARS
                return(nr_of_chars);
#else
                return(0);
#endif
            put_one_char(format_flag, secret_pointer);
#if CONFIG_PRINTF_COUNT_CHARS
            nr_of_chars++;
#endif
        }

#if CONFIG_PRINTF > PRINTF_NOMODIFIERS
        /*
         * Optional 'l', 'z' or 'h' modifiers?
         */
        l_modifier = h_modifier = 0;

        switch (PGM_GET_CHAR(format))
        {
            case 'l':
            case 'z':
                /* for the 'z' modifier, we make this assumption */
                //STATIC_ASSERT(sizeof(size_t) == sizeof(long));
                l_modifier = 1;
                format++;
                break;

            case 'h':
                h_modifier = 1;
                format++;
                break;
        }
#endif

        switch (format_flag = PGM_GET_CHAR(format++))
        {
            case 'c':
                format_flag = va_arg(ap, int);
            default:
                put_one_char(format_flag, secret_pointer);
#if CONFIG_PRINTF_COUNT_CHARS
                nr_of_chars++;
#endif
                continue;

            case 's':
                ptr = va_arg(ap, char *);
                while ((format_flag = *ptr++))
                {
                    put_one_char(format_flag, secret_pointer);
#if CONFIG_PRINTF_COUNT_CHARS
                    nr_of_chars++;
#endif
                }
                continue;

            case 'o':
                base = 8;
                if (IS_SHORT)
                    div_val = 0x8000;
                else
#if CONFIG_PRINTF > PRINTF_NOMODIFIERS
                    div_val = 0x40000000UL;
#else
                    div_val = (unsigned int)0x40000000UL;
#endif
                goto CONVERSION_LOOP;

            case 'd':
                base = 10;
                if (IS_SHORT)
                    div_val = 10000;
                else
#if CONFIG_PRINTF > PRINTF_NOMODIFIERS
                    div_val = 1000000000UL;
#else
                    div_val = (unsigned int)1000000000UL;
#endif
                goto CONVERSION_LOOP;

            case 'X':
            case 'x':
                base = 16;
                if (IS_SHORT)
                    div_val = 0x1000;
                else
#if CONFIG_PRINTF > PRINTF_NOMODIFIERS
                    div_val = 0x10000000UL;
#else
                    div_val = (unsigned int)0x10000000UL;
#endif

              CONVERSION_LOOP:

#if CONFIG_PRINTF > PRINTF_NOMODIFIERS
                if (h_modifier)
                {
                    if (format_flag == 'd')
                        u_val = (short)va_arg(ap, int);
                    else
                        u_val = (unsigned short)va_arg(ap, int);
                }
                else if (l_modifier)
                    u_val = va_arg(ap, long);
                else
                {
                    if (format_flag == 'd')
                        u_val = va_arg(ap, int);
                    else
                        u_val = va_arg(ap, unsigned int);
                }

#else
                u_val = va_arg(ap,int);
#endif
                if (format_flag == 'd')
                {
                    if (((int)u_val) < 0)
                    {
                        u_val = - u_val;
                        put_one_char('-', secret_pointer);
#if CONFIG_PRINTF_COUNT_CHARS
                        nr_of_chars++;
#endif
                    }
                }
                while (div_val > 1 && div_val > u_val)
                {
                    div_val /= base;
                }
                do
                {
                    outChar = (u_val / div_val) + '0';
                    if (outChar > '9')
                    {
                        if (format_flag == 'x')
                            outChar += 'a'-'9'-1;
                        else
                            outChar += 'A'-'9'-1;
                    }
                    put_one_char(outChar, secret_pointer);
#if CONFIG_PRINTF_COUNT_CHARS
                    nr_of_chars++;
#endif
                    u_val %= div_val;
                    div_val /= base;
                }
                while (div_val);

        } /* end switch(format_flag...) */
    }
#endif /* else PRINTF_REDUCED*/
}



static void __str_put_char(char c, void *ptr)
{
    **((char **)ptr) = c;
    (*((char **)ptr))++;
}

static void __null_put_char (char c, void *ptr)
{

}


int xvsprintf (char *str, const char * fmt, va_list ap)
{
    int result;

    if (str)
    {
        result = _formatted_write(fmt, __str_put_char, &str, ap);
        /* Terminate string */
        *str = '\0';
    }
    else
        result = _formatted_write(fmt, __null_put_char, 0, ap);

    return result;
}


int xsprintf(char *str, const char * fmt, ...)
{
    int result;
    va_list ap;

    va_start(ap, fmt);
    result = xvsprintf(str, fmt, ap);
    va_end(ap);

    return result;
}


#endif /* #if CONFIG_PRINTF */


/***************************************************************************************************
 *  end of file: xprintf.c
 **************************************************************************************************/
