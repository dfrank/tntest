/***************************************************************************************************
 * FIFO Buffer API
 * header file
 **************************************************************************************************/

#ifndef _FIFOBUF_H
#define _FIFOBUF_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

//#define FIFOBUF_USE_DEF_CONFIG
#if 0
#if !defined FIFOBUF_USE_DEF_CONFIG
#  include "lib_cfg/fifobuf_cfg.h"
#endif

#if defined FIFOBUF_USE_DEF_CONFIG || !defined _FIFOBUF_CFG_H
#  include "fifobuf_cfg_default.h"
#endif
#endif

#include "obj_common.h"
#include <stddef.h>

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

#define  FIFOBUF_CFG__USE_MACRO_INSTEAD_OF_FUNC    0



#ifndef FIFOBUF_CFG__USE_MACRO_INSTEAD_OF_FUNC
#error FIFOBUF_CFG__USE_MACRO_INSTEAD_OF_FUNC is not defined
#endif

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

typedef struct S_FIFOBuf T_FIFOBuf;

/*
 * Result codes
 */
typedef enum E_FIFOBuf_Res {
   FIFOBUF_RES__OK,
} T_FIFOBuf_Res;

enum E_FIFOBuf_Stat {
   //TODO: FIFOBUF_STAT__ -> FIFOBUF_STAT__
   FIFOBUF_STAT__FULL  = (1 << 0),
   FIFOBUF_STAT__EMPTY = (1 << 1),
   FIFOBUF_STAT__ERR   = (1 << 2),

   FIFOBUF_ERR___OVF    = (1 << 3),
   FIFOBUF_ERR___UFL    = (1 << 4),
   FIFOBUF_ERR___HW     = (1 << 5)
};

enum E_FIFOBuf_LockMode
{
   //TODO: FIFOBUF_LM__ -> FIFOBUF_LM__
   FIFOBUF_LM__LOCKED,
   FIFOBUF_LM__UNLOCKED
};


typedef struct S_FIFOBuf_CtorParams {
   //-- add your ctor params here
   U08     *p_buf;   //-- if NULL, then will be auto-allocated by fifobuf.
   size_t   size;

   struct S_FIFOBuf_Callbacks {
      void   (*p_lock)(void *p_user_data);
      void   (*p_unlock)(void *p_user_data);
      void    *p_user_data;   //-- this pointer is passed to user callback functions
   } callback;

} T_FIFOBuf_CtorParams;

/***************************************************************************************************
 *                                         OBJECT CONTEXT                                          *
 **************************************************************************************************/

/*
 * Object context
 */
struct S_FIFOBuf {

   U08     *p_head;
   U08     *p_tail;
   U08     *p_begin;
   U08     *p_end;

   size_t   cnt;
   U16      stat;

   struct S_FIFOBuf_Callbacks callback;

   struct {
      unsigned    buf_allocated_by_fifobuf : 1;
   } flags;
};

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                CONSTRUCTOR, DESTRUCTOR PROTOTYPES
 **************************************************************************************************/

/**
 * Constructor
 *
 * @return FIFOBUF_RES__OK if everything is ok, otherwise returns error code
 */
T_FIFOBuf_Res fifobuf__ctor(T_FIFOBuf *me, const T_FIFOBuf_CtorParams *p_params);

/**
 * Desctructor
 */
void fifobuf__dtor(T_FIFOBuf *me);

/***************************************************************************************************
 *                                     PUBLIC METHODS PROTOTYPES
 **************************************************************************************************/

size_t      fifobuf__u08__put(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode, U08 data);
U08_FAST    fifobuf__u08__get(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode);

size_t      fifobuf__u08__put_check(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode, U08 data, bool *p_bool_success);
U08_FAST    fifobuf__u08__get_check(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode, bool *p_bool_success);

U08_FAST    fifobuf__u08__get_by_index(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode, int index);
void        fifobuf__u08__set_by_index(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode, int index, U08 data);

size_t      fifobuf__u16__put(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode, U16 data);
U16_FAST    fifobuf__u16__get(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode);

size_t      fifobuf__data__put(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode, const U08 *p_data, size_t len);

void        fifobuf__flush   (T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode);

size_t      fifobuf__size__get(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode);

/* Not blocked functions */

#define fifobuf__u08__put_nolock(fb, c)         fifobuf__u08__put((fb), FIFOBUF_LM__UNLOCKED, (c))
#define fifobuf__u08__get_nolock(fb)            fifobuf__u08__get((fb), FIFOBUF_LM__UNLOCKED)

#define fifobuf__u08__get_by_index_nolock(fb, index)   fifobuf__u08__get_by_index((fb), FIFOBUF_LM__UNLOCKED, (index))
#define fifobuf__u08__set_by_index_nolock(fb, index, data)   fifobuf__u08__set_by_index((fb), FIFOBUF_LM__UNLOCKED, (index), (data))

#define fifobuf__u16__put_nolock(fb, word)      fifobuf__u16__put((fb), FIFOBUF_LM__UNLOCKED, (word))
#define fifobuf__u16__get_nolock(fb)            fifobuf__u16__get((fb), FIFOBUF_LM__UNLOCKED)

#define fifobuf__data__put_nolock(fb, p_data, len) fifobuf__data__put((fb), FIFOBUF_LM__UNLOCKED, (p_data), (len))

#define fifobuf__flush_nolock(fb)               fifobuf__flush((fb), FIFOBUF_LM__UNLOCKED)

#define fifobuf__size__get_nolock(fb)           fifobuf__size__get((fb), FIFOBUF_LM__UNLOCKED)

/* Blocking functions */

#define fifobuf__u08__put_lock(fb, c)           fifobuf__u08__put((fb), FIFOBUF_LM__LOCKED, (c))
#define fifobuf__u08__get_lock(fb)              fifobuf__u08__get((fb), FIFOBUF_LM__LOCKED)

#define fifobuf__u08__get_by_index_lock(fb, index)   fifobuf__u08__get_by_index((fb), FIFOBUF_LM__LOCKED, (index))
#define fifobuf__u08__set_by_index_lock(fb, index, data)   fifobuf__u08__set_by_index((fb), FIFOBUF_LM__LOCKED, (index), (data))

#define fifobuf__u16__put_lock(fb, word)        fifobuf__u16__put((fb), FIFOBUF_LM__LOCKED, (word))
#define fifobuf__u16__get_lock(fb)              fifobuf__u16__get((fb), FIFOBUF_LM__LOCKED)

#define fifobuf__data__put_lock(fb, p_data, len) fifobuf__data__put((fb), FIFOBUF_LM__LOCKED, (p_data), (len))

#define fifobuf__flush_lock(fb)                 fifobuf__flush((fb), FIFOBUF_LM__LOCKED)

#define fifobuf__size__get_lock(fb)             fifobuf__size__get((fb), FIFOBUF_LM__LOCKED)

/* Service functions */

#if FIFOBUF_CFG__USE_MACRO_INSTEAD_OF_FUNC

#define fifobuf__count__get(fb)                 (((volatile T_FIFOBuf *)(fb))->cnt)

#define fifobuf__status__check(fb, x)           ((((volatile T_FIFOBuf *)(fb))->stat & (x)) != 0)
#define fifobuf__status__set(fb, x)             {((volatile T_FIFOBuf *)(fb))->stat |=  (x);}
#define fifobuf__status__clr(fb, x)             {((volatile T_FIFOBuf *)(fb))->stat &= ~(x);}

#else

#if 0
size_t         fifobuf__count__get              (T_FIFOBuf *me);
bool           fifobuf__status__check           (T_FIFOBuf *me, U16 val);
void           fifobuf__status__set             (T_FIFOBuf *me, U16 val);
void           fifobuf__status__clr             (T_FIFOBuf *me, U16 val);
#endif

static inline size_t fifobuf__count__get(T_FIFOBuf *me)
{
   return me->cnt;
}

static inline bool fifobuf__status__check(T_FIFOBuf *me, U16 val)
{
   return !!(me->stat & val);
}

static inline void fifobuf__status__set(T_FIFOBuf *me, U16 val)
{
   me->stat |= val;
}

static inline void fifobuf__status__clr(T_FIFOBuf *me, U16 val)
{
   me->stat &= ~val;
}

#endif

#define fifobuf__is_empty(fb)                   fifobuf__status__check((fb), FIFOBUF_STAT__EMPTY)
#define fifobuf__is_full(fb)                    fifobuf__status__check((fb), FIFOBUF_STAT__FULL)
#define fifobuf__is_err(fb)                     fifobuf__status__check((fb), FIFOBUF_STAT__ERR)



/***************************************************************************************************
 *                                ALLOCATOR, DEALLOCATOR PROTOTYPES
 **************************************************************************************************/

/*
 * Allocator and Deallocator
 *
 * Please NOTE: There's not necessary to use them! 
 * For instance, you can just allocate in stack or statically or manually from heap,
 * and use _ctor only.
 */
#if defined OBJ_COMMON__MALLOC && defined OBJ_COMMON__FREE

/* allocator */
T_FIFOBuf *new_fifobuf(const T_FIFOBuf_CtorParams *p_params);

/* deallocator */
void delete_fifobuf(T_FIFOBuf *me);

#endif


#endif // _FIFOBUF_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


