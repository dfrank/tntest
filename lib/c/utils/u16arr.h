/***************************************************************************************************
 * TODO: description
 * header file
 **************************************************************************************************/

#ifndef _U16ARR_H
#define _U16ARR_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

//#define U16ARR_USE_DEF_CONFIG
#if 0
#if !defined U16ARR_USE_DEF_CONFIG
#  include "lib_cfg/u16arr_cfg.h"
#endif

#if defined U16ARR_USE_DEF_CONFIG || !defined _U16ARR_CFG_H
#  include "u16arr_cfg_default.h"
#endif
#endif

#include "obj_common.h"
#include <stddef.h>

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

typedef struct S_U16Arr T_U16Arr;

/*
 * Result codes
 */
typedef enum E_U16Arr_Res {
   U16ARR_RES__OK,
} T_U16Arr_Res;

typedef struct S_U16Arr_CtorParams {
   size_t      length;
} T_U16Arr_CtorParams;

/***************************************************************************************************
 *                                         OBJECT CONTEXT                                          *
 **************************************************************************************************/

/*
 * Object context
 */
struct S_U16Arr {
   size_t   length;
   U16      data[];
};

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                CONSTRUCTOR, DESTRUCTOR PROTOTYPES
 **************************************************************************************************/

/**
 * Constructor
 *
 * @return U16ARR_RES__OK if everything is ok, otherwise returns error code
 */
T_U16Arr_Res u16arr__ctor(T_U16Arr *me, const T_U16Arr_CtorParams *p_params);
void          u16arr__ctor__copy(T_U16Arr *me, const T_U16Arr *p_src);

/**
 * Desctructor
 */
void u16arr__dtor(T_U16Arr *me);

/***************************************************************************************************
 *                                     PUBLIC METHODS PROTOTYPES
 **************************************************************************************************/

/**
 * @return size_t  length of byte array
 */
#define  u16arr__len__get(ba)            ((ba)->length)

/**
 * @return U16 *   pointer to data
 */
#define  u16arr__data__get_pt(ba)        ((ba)->data)

/**
 * Checks if array contains specified value.
 * If it does, then its index is returned, otherwise -1 is returned.
 * @param check_items_cnt if not null, then only first check_items_cnt items will be checked,
 *                        otherwise all me->length will be used
 */
S16 u16arr__find(T_U16Arr *me, U16 value, size_t check_items_cnt);

/***************************************************************************************************
 *                                ALLOCATOR, DEALLOCATOR PROTOTYPES
 **************************************************************************************************/

/*
 * Allocator and Deallocator
 *
 * Please NOTE: There's not necessary to use them! 
 * For instance, you can just allocate in stack or statically or manually from heap,
 * and use _ctor only.
 */
#if defined OBJ_COMMON__MALLOC && defined OBJ_COMMON__FREE

/* allocator */
T_U16Arr *new_u16arr(const T_U16Arr_CtorParams *p_params);
T_U16Arr *new_u16arr__simple(size_t len);
T_U16Arr *new_u16arr__copy(const T_U16Arr *p_src);

/* deallocator */
void delete_u16arr(T_U16Arr *me);

#endif


#endif // _U16ARR_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


