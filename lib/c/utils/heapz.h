/***************************************************************************************************
 *   Project:       P-0172 - Utilities Library
 *   Author:        Alex Borisov
 ***************************************************************************************************
 *   Distribution:  Copyright � zltigo
 *   http://electronix.ru/forum/index.php?showtopic=53623
 *
 ***************************************************************************************************
 *   MCU Family:    
 *   Compiler:      
 ***************************************************************************************************
 *   File:          heapz.h
 *   Description:   Easy Heap Management Header
 *
 ***************************************************************************************************
 *   History:       09.06.2010 - [Alex Borisov] - file created
 *
 **************************************************************************************************/

#ifndef __HEAPZ_H
#define __HEAPZ_H

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

typedef union type_size
{
    unsigned long mark;
    struct __attribute__((packed))
    {
        unsigned long size :24;
        unsigned long type  :8;
    };
} type_size;

/* Memory Control Block (MCB) */
typedef struct heap_mcb
{
    struct heap_mcb *next;      /* ��������� �� ��������� MCB mcb.next ���������� MCM ������ ��������� �� ������ MCB.   */
    struct heap_mcb *prev;      /* ��������� �� ���������� MCB. ��� ������� MCB ���� ��������� ��������� ��� �� ����.   */
    union type_size ts;         /* ������ ����� ������ (� ������)                                                       */
    void            *owner;     /* �������� ����� ������ (TCB ���������)                                                */
    /* ���������� �������������� ���� ������ ���������� ����� �� MCB */
} heap_mcb;

/* ���������-��������� ���� (���-��������� t_heap) */
typedef struct heap_t
{   
    struct heap_mcb *start;     /* ��������� �� ������ heap (������ MCB)    */
    struct heap_mcb *freem;     /* ��������� �� ������ ��������� MCB        */
    unsigned long   hsize;      /* RAW ������ ���� � ������                 */
} heap_t;


enum HEAP_MARK
{
    HEAP_MARK_FREE = 0,
    HEAP_MARK_SYSTEM,
    HEAP_MARK_TCB,
    HEAP_MARK_STACK,
    HEAP_MARK_QCB,
    HEAP_MARK_QUEUE,
    HEAP_MARK_CCB,
    HEAP_MARK_MUTEX,
    HEAP_MARK_8,
    HEAP_MARK_9,
    HEAP_MARK_A,
    HEAP_MARK_B,
    HEAP_MARK_C,
    HEAP_MARK_D,
    HEAP_MARK_E,
    HEAP_MARK_F
};

/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

/* ������������� ���������� */
void hz_init (void);

/* ������������� ������ � �������� heap */
void hz_heap_init (heap_t *heap, void *start, unsigned long size);

/* ����������� ������ � ���� 'heap', �������� 'size' ����.
   ���������� ��������� �� ����������������� ������. ���� � ���� �� ���������� ������,
   �� ���������� NULL
*/
void *hz_malloc (heap_t *heap, size_t size, int type, void *owner);

/* ����������� ����������������� ������, �� ������� ��������� 'ptr' � 'heap'.
   ���� ��������� ��������� �� ������, ������� �� ���� �������� �����
   ��� ��������� 'ptr' ����� 0, �� ������ �� ����������, �� �� ������� ����� �� exception :( */
void hz_free (heap_t *heap, void *ptr);

/* ���������� ������ ���������� ������ */
void hz_heap_list (heap_t *heap);


#endif /* __HEAPZ_H */
/***************************************************************************************************
    end of file: heapz.h
 **************************************************************************************************/

