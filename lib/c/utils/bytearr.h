/***************************************************************************************************
 * TODO: description
 * header file
 **************************************************************************************************/

#ifndef _BYTEARR_H
#define _BYTEARR_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

//#define BYTEARR_USE_DEF_CONFIG
#if 0
#if !defined BYTEARR_USE_DEF_CONFIG
#  include "lib_cfg/bytearr_cfg.h"
#endif

#if defined BYTEARR_USE_DEF_CONFIG || !defined _BYTEARR_CFG_H
#  include "bytearr_cfg_default.h"
#endif
#endif

#include "obj_common.h"
#include <stddef.h>

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

typedef struct S_ByteArr T_ByteArr;

/*
 * Result codes
 */
typedef enum E_ByteArr_Res {
   BYTEARR_RES__OK,
} T_ByteArr_Res;

typedef struct S_ByteArr_CtorParams {
   size_t      length;
} T_ByteArr_CtorParams;

/***************************************************************************************************
 *                                         OBJECT CONTEXT                                          *
 **************************************************************************************************/

/*
 * Object context
 */
struct S_ByteArr {
   size_t   length;
   U08      data[];
};

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                CONSTRUCTOR, DESTRUCTOR PROTOTYPES
 **************************************************************************************************/

/**
 * Constructor
 *
 * @return BYTEARR_RES__OK if everything is ok, otherwise returns error code
 */
T_ByteArr_Res bytearr__ctor(T_ByteArr *me, const T_ByteArr_CtorParams *p_params);
void          bytearr__ctor__copy(T_ByteArr *me, const T_ByteArr *p_src);

/**
 * Desctructor
 */
void bytearr__dtor(T_ByteArr *me);

/***************************************************************************************************
 *                                     PUBLIC METHODS PROTOTYPES
 **************************************************************************************************/

/**
 * @return size_t  length of byte array
 */
#define  bytearr__len__get(ba)            ((ba)->length)

/**
 * @return U08 *   pointer to data
 */
#define  bytearr__data__get_pt(ba)        ((ba)->data)

/***************************************************************************************************
 *                                ALLOCATOR, DEALLOCATOR PROTOTYPES
 **************************************************************************************************/

/*
 * Allocator and Deallocator
 *
 * Please NOTE: There's not necessary to use them! 
 * For instance, you can just allocate in stack or statically or manually from heap,
 * and use _ctor only.
 */
#if defined OBJ_COMMON__MALLOC && defined OBJ_COMMON__FREE

/* allocator */
T_ByteArr *new_bytearr(const T_ByteArr_CtorParams *p_params);
T_ByteArr *new_bytearr__simple(size_t len);
T_ByteArr *new_bytearr__copy(const T_ByteArr *p_src);

/* deallocator */
void delete_bytearr(T_ByteArr *me);

#endif


#endif // _BYTEARR_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


