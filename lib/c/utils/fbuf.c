/***************************************************************************************************
 *   Project:       FIFO Buffer API
 *   Author:        Alex B.
 ***************************************************************************************************
 *   Distribution:
 *
 ***************************************************************************************************
 *   MCU Family:    PIC24/dsPIC
 *   Compiler:      MCHP C30 3.12
 ***************************************************************************************************
 *   File:          fbuf.c
 *   Description:   ������ ������������ FIFO ������
 * 
 * 
 *
 ***************************************************************************************************
 *   History:       19.05.2009 - [Alex B.] - file created
 *
 **************************************************************************************************/
/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "fbuf.h"       

/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

/**
 * Config FIFO
 *
 * @param fb     - FIFO control block pointer 
 * @param buf    - FIFO buffer pointer
 * @param size   - size of FIFO buffer in bytes 
 * @param lock   - pointer to lock() function 
 * @param unlock - pointer to unlock() function
 */
void fbuf_config (volatile FBUF *fb, volatile unsigned char *buf, size_t size, void lock(void), void unlock(void))
{
    fb->begin  = fb->head = fb->tail = buf;
    fb->cnt    = 0;
    fb->end    = fb->head + size;
    fb->stat   = FBUF_STAT_EMPTY;

    fb->lock   = lock;
    fb->unlock = unlock;
}

/**
 * Put char to FIFO
 *
 * @param cb 
 * @param acc
 * @param c 
 * 
 * @return unsigned int 
 */
int _fbuf_put (volatile FBUF *fb, enum FBUF_LM acc, unsigned char c)
{
    if (acc == FBUF_ACC_LOCKED)
        fb->lock();                                 /* ���������� */


    if (fbuf_status_check(fb, FBUF_STAT_FULL))
    {   
        /* ���� ����� �������� - ��������� ����� ������ �� ������������ */
        fbuf_status_set(fb, FBUF_STAT_ERR | FBUF_ERR_OVF);
        if (acc == FBUF_ACC_LOCKED)
            fb->unlock();                           /* ������������� */
        return FBUF_RET_FULL;
    }

    fbuf_status_clr(fb, FBUF_STAT_EMPTY);

    *(fb->tail++) = c;
    if (fb->tail == fb->end)
        fb->tail = fb->begin;

    if (++fb->cnt >= (fb->end - fb->begin)) {
        fbuf_status_set(fb, FBUF_STAT_FULL);
    }

    if (acc == FBUF_ACC_LOCKED)
        fb->unlock();                               /* ������������� */

    return fb->cnt;                                 /* ���������� ������� ���������� �������� � ������ */
}

/**
 * Get char from FIFO
 *
 * @param cb 
 * @param acc 
 * 
 * @return unsigned char 
 */
int _fbuf_get (volatile FBUF *fb, enum FBUF_LM acc)
{
    int ret;

    if (acc == FBUF_ACC_LOCKED)
        fb->lock();                                 /* ���������� */

    if (fbuf_status_check(fb, FBUF_STAT_EMPTY))
    {
        /* ���� ����� ���� - ��������� ����� ������ �� ����������� */
        fbuf_status_set(fb, FBUF_STAT_ERR | FBUF_ERR_UFL);
        if (acc == FBUF_ACC_LOCKED)
            fb->unlock();                           /* ������������� */
        return FBUF_RET_EMPTY;
    }

    fbuf_status_clr(fb, FBUF_STAT_FULL);

    ret = (int)*(fb->head++);
    if (fb->head == fb->end)
        fb->head = fb->begin;

    if (!(--fb->cnt)) {    
        fbuf_status_set(fb, FBUF_STAT_EMPTY);
    }

    if (acc == FBUF_ACC_LOCKED)
        fb->unlock();                               /* ������������� */

    return ret;                                     /* ���������� ������ */
}

/**
 * Flush FIFO
 *
 * @param cb 
 * @param acc 
 */
void _fbuf_flush (volatile FBUF *fb, enum FBUF_LM acc)
{
    if (acc == FBUF_ACC_LOCKED)
        fb->lock();                                 /* ���������� */

    fb->head = fb->tail = fb->begin;
    fb->cnt  = 0;
    fb->stat = FBUF_STAT_EMPTY;

    if (acc == FBUF_ACC_LOCKED)
        fb->unlock();                               /* ������������� */
}

int _fbuf_u16_put(volatile FBUF *fb, enum FBUF_LM acc, unsigned short word)
{
    _fbuf_put(fb, acc, (unsigned char)(word >> 0));
    return _fbuf_put(fb, acc, (unsigned char)(word >> 8));
}

int _fbuf_u16_get (volatile FBUF *fb, enum FBUF_LM acc)
{
    unsigned short ret;
    *((unsigned char *)(&ret) + 0) = (unsigned char)_fbuf_get(fb, acc);
    *((unsigned char *)(&ret) + 1) = (unsigned char)_fbuf_get(fb, acc);
    return ret;
}




/***************************************************************************************************
 *  end of file: fbuf.c
 **************************************************************************************************/


