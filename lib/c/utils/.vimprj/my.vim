
" определяем путь к папке .vim
let s:sPath = expand('<sfile>:p:h')

let &tabstop = 3
let &shiftwidth = 3

let g:indexer_indexerListFilename = s:sPath.'/.indexer_files'

call envcontrol#set_previous()

