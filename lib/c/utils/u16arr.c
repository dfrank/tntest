/***************************************************************************************************
 * TODO: description
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "u16arr.h"
#include <string.h>

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

/***************************************************************************************************
 *                                  PRIVATE METHODS PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                  EXTERNAL METHODS PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                      PROTECTED METHODS
 **************************************************************************************************/

/***************************************************************************************************
 *                                        PRIVATE METHODS
 **************************************************************************************************/

/***************************************************************************************************
 *                                    CONSTRUCTOR, DESTRUCTOR                                      *
 **************************************************************************************************/

/**
 * Constructor
 *
 */
T_U16Arr_Res u16arr__ctor(T_U16Arr *me, const T_U16Arr_CtorParams *p_params)
{
   T_U16Arr_Res ret = U16ARR_RES__OK;
   //memset(me, 0x00, sizeof(T_U16Arr));

   //-- some construct code

   me->length = p_params->length;

   if (ret == U16ARR_RES__OK){
      //me->flags |= _U16ARR__STATE_FLAG__INIT;
      
   }
   return ret;
}

void u16arr__ctor__copy(T_U16Arr *me, const T_U16Arr *p_src)
{
   memcpy(me, p_src, sizeof(T_U16Arr) + u16arr__len__get(p_src) * sizeofmember(T_U16Arr, data[0]));
}


/**
 * Desctructor
 */
void u16arr__dtor(T_U16Arr *me)
{
   //if (me->flags & _U16ARR__STATE_FLAG__INIT){
      // some desctruct code
   //}
}

/***************************************************************************************************
 *                                          PUBLIC METHODS                                         *
 **************************************************************************************************/

/**
 * Checks if array contains specified value.
 * If it does, then its index is returned, otherwise -1 is returned.
 * @param check_items_cnt if not equal -1, then only first check_items_cnt items will be checked,
 *                        otherwise all me->length will be used
 */
S16 u16arr__find(T_U16Arr *me, U16 value, size_t check_items_cnt)
{
   S16 ret = -1;

   if (check_items_cnt == -1){
      check_items_cnt = me->length;
   }

   S16 cur_item_num = 0;
   while (check_items_cnt-- > 0){
      if (me->data[cur_item_num] == value){
         ret = cur_item_num;
         break;
      }
      cur_item_num++;
   }

   return ret;
}

/***************************************************************************************************
 *                                      ALLOCATOR, DEALLOCATOR                                     *
 **************************************************************************************************/

/*
 * Allocator and Deallocator
 *
 * Please NOTE: There's not necessary to use them! 
 * For instance, you can just allocate in stack or statically or manually from heap,
 * and use _ctor only.
 */
#if defined OBJ_COMMON__MALLOC && defined OBJ_COMMON__FREE

/* allocator */
T_U16Arr *new_u16arr(const T_U16Arr_CtorParams *p_params)
{
   T_U16Arr *me = (T_U16Arr *)OBJ_COMMON__MALLOC( sizeof(T_U16Arr) + p_params->length * sizeofmember(T_U16Arr, data[0]));
   u16arr__ctor(me, p_params);
   return me;
}

T_U16Arr *new_u16arr__simple(size_t len)
{
   T_U16Arr *me = (T_U16Arr *)OBJ_COMMON__MALLOC( sizeof(T_U16Arr) + len * sizeofmember(T_U16Arr, data[0]));

   const T_U16Arr_CtorParams par = {
      .length = len,
   };
   u16arr__ctor(me, &par);

   return me;
}

T_U16Arr *new_u16arr__copy(const T_U16Arr *p_src)
{
   T_U16Arr *me = (T_U16Arr *)OBJ_COMMON__MALLOC( sizeof(T_U16Arr) + u16arr__len__get(p_src) * sizeofmember(T_U16Arr, data[0]));
   u16arr__ctor__copy(me, p_src);
   return me;
}

/* deallocator */
void delete_u16arr(T_U16Arr *me){
   u16arr__dtor(me);
   OBJ_COMMON__FREE(me);
}
#endif



/***************************************************************************************************
 *  end of file
 **************************************************************************************************/


