/***************************************************************************************************
 *   Project:       
 *   Author:        Alex Borisov
 ***************************************************************************************************
 *   Distribution:  
 *
 ***************************************************************************************************
 *   MCU Family:    
 *   Compiler:      
 ***************************************************************************************************
 *   File:          debug.h
 *   Description:   ���������� ���������� ��������
 *
 ***************************************************************************************************
 *   History:       27.07.2009 - [Alex Borisov] - file created
 *
 **************************************************************************************************/

#ifndef __DEBUG_H
#define __DEBUG_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include "debug_conf.h"     /* Application Specific */

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

#define LOG_LVL_NONE        0       /* ���������� ���������� �� ���������                   */
#define LOG_LVL_ERR         1       /* ��������� ������ ������                              */
#define LOG_LVL_WARN        2       /* ��������� ������ � ��������������                    */
#define LOG_LVL_INFO        3       /* ��������� ������, �������������� � ���������         */

#define LOG_FMT_FILE_FUNC   2       /* � ��������� ��������� �������� ����� � �������       */
#define LOG_FMT_FUNC        1       /* � ��������� ��������� ������ �������� �������        */
#define LOG_FMT_SIMPLE      0       /* � ��������� ��������� ������ ���������� ����������   */

#define LOG_MTH_BUFF        0       /* ����� ����� ���������������� �������                 */
#define LOG_MTH_DIRECT      1       /* ���������������� ����� (��������, � �����������)     */


/* Use a default setting if nobody defined a log level */
#ifndef LOG_LEVEL
#define LOG_LEVEL           LOG_LVL_ERR
#endif
/* Use a default setting if nobody defined a log format */
#ifndef LOG_FORMAT
#define LOG_FORMAT          LOG_FMT_SIMPLE
#endif
/* Use a default setting if nobody defined a log method */
#ifndef LOG_METHOD
#define LOG_METHOD          LOG_MTH_DIRECT
#endif

//-- for backward compatibility
#ifndef LOG_OUT_MSG
#   ifdef LOG_OUT
#       define LOG_OUT_MSG    LOG_OUT
#   endif
#endif

#ifndef LOG_LINE_ENDING
#   define LOG_LINE_ENDING  "\r\n"
#endif


/* �������� ������� ���������� */
/* --------------------------- */

#define DEB_JOIN_2_H(A,B)   A##B
#define DEB_JOIN_2(A,B)     DEB_JOIN_2_H(A,B)
#define DEB_ASSERT_H(A)     typedef int DEB_JOIN_2(compile_time_error_in_line_,__LINE__) [(A) ? 1 : -1]
#define DEB_ASSERT(A)       DEB_ASSERT_H(A)

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

#if   LOG_FORMAT == LOG_FMT_FILE_FUNC
#  define LOG_PRINT(str_level, str,...)      LOG_OUT_MSG     ("%s: %s:%s():%04d: " str LOG_LINE_ENDING, str_level, __FILE__, __func__, __LINE__,  ## __VA_ARGS__)
#  define LOG_PRINT_I(str_level, str,...)    LOG_OUT_MSG_I   ("%s: %s:%s():%04d: " str LOG_LINE_ENDING, str_level, __FILE__, __func__, __LINE__,  ## __VA_ARGS__)
#  define LOG_PRINT_CRIT(str_level, str,...) LOG_OUT_MSG_CRIT("%s: %s:%s():%04d: " str LOG_LINE_ENDING, str_level, __FILE__, __func__, __LINE__,  ## __VA_ARGS__)
#elif LOG_FORMAT == LOG_FMT_FUNC
#  define LOG_PRINT(str_level, str,...)      LOG_OUT_MSG     ("%s: %s():%04d: "    str LOG_LINE_ENDING, str_level,           __func__, __LINE__,  ## __VA_ARGS__)
#  define LOG_PRINT_I(str_level, str,...)    LOG_OUT_MSG_I   ("%s: %s():%04d: "    str LOG_LINE_ENDING, str_level,           __func__, __LINE__,  ## __VA_ARGS__)
#  define LOG_PRINT_CRIT(str_level, str,...) LOG_OUT_MSG_CRIT("%s: %s():%04d: "    str LOG_LINE_ENDING, str_level,           __func__, __LINE__,  ## __VA_ARGS__)
#elif LOG_FORMAT == LOG_FMT_SIMPLE
#  define LOG_PRINT(str_level, str,...)      LOG_OUT_MSG     ("%s: "               str LOG_LINE_ENDING,                               str_level,  ## __VA_ARGS__)
#  define LOG_PRINT_I(str_level, str,...)    LOG_OUT_MSG_I   ("%s: "               str LOG_LINE_ENDING,                               str_level,  ## __VA_ARGS__)
#  define LOG_PRINT_CRIT(str_level, str,...) LOG_OUT_MSG_CRIT("%s: "               str LOG_LINE_ENDING,                               str_level,  ## __VA_ARGS__)
#endif



#if LOG_LEVEL >= LOG_LVL_ERR
#  define LOG_ERR(str,...)                LOG_PRINT   ("[E]", str, ## __VA_ARGS__)
#  define LOG_ERR_I(str,...)              LOG_PRINT_I ("[E]", str, ## __VA_ARGS__)
#  define LOG_ERR_CRIT(str,...)           LOG_PRINT_CRIT ("[E]", str, ## __VA_ARGS__)
#else
#  define LOG_ERR(str,...)                /* Nothing */
#  define LOG_ERR_I(str,...)              /* Nothing */
#  define LOG_ERR_CRIT(str,...)           /* Nothing */
#endif

#if LOG_LEVEL >= LOG_LVL_WARN
#  define LOG_WRN(str,...)                LOG_PRINT   ("[W]", str, ## __VA_ARGS__)
#  define LOG_WRN_I(str,...)              LOG_PRINT_I ("[W]", str, ## __VA_ARGS__)
#  define LOG_WRN_CRIT(str,...)           LOG_PRINT_CRIT ("[W]", str, ## __VA_ARGS__)
#else
#  define LOG_WRN(str,...)                /* Nothing */
#  define LOG_WRN_I(str,...)              /* Nothing */
#  define LOG_ERR_CRIT(str,...)           /* Nothing */
#endif

#if LOG_LEVEL >= LOG_LVL_INFO
#  define LOG_INF(str,...)                LOG_PRINT   ("[I]", str, ## __VA_ARGS__)
#  define LOG_INF_I(str,...)              LOG_PRINT_I ("[I]", str, ## __VA_ARGS__)
#  define LOG_INF_CRIT(str,...)           LOG_PRINT_CRIT ("[I]", str, ## __VA_ARGS__)
#else
#  define LOG_INF(str,...)                /* Nothing */
#  define LOG_INF_I(str,...)              /* Nothing */
#  define LOG_INF_CRIT(str,...)           /* Nothing */
#endif


#endif /* __DEBUG_H */
/***************************************************************************************************
    end of file: debug.h
 **************************************************************************************************/
