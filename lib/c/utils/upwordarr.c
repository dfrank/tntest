/***************************************************************************************************
 * TODO: description
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "upwordarr.h"
#include <string.h>

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

/***************************************************************************************************
 *                                  PRIVATE METHODS PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                  EXTERNAL METHODS PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                      PROTECTED METHODS
 **************************************************************************************************/

/***************************************************************************************************
 *                                        PRIVATE METHODS
 **************************************************************************************************/

/***************************************************************************************************
 *                                    CONSTRUCTOR, DESTRUCTOR                                      *
 **************************************************************************************************/

/**
 * Constructor
 *
 */
T_UPWordArr_Res upwordarr__ctor(T_UPWordArr *me, const T_UPWordArr_CtorParams *p_params)
{
   T_UPWordArr_Res ret = UPWORDARR_RES__OK;
   //memset(me, 0x00, sizeof(T_UPWordArr));

   //-- some construct code

   me->length = p_params->length;

   if (ret == UPWORDARR_RES__OK){
      //me->flags |= _UPWORDARR__STATE_FLAG__INIT;
      
   }
   return ret;
}

void upwordarr__ctor__copy(T_UPWordArr *me, const T_UPWordArr *p_src)
{
   memcpy(me, p_src, sizeof(T_UPWordArr) + upwordarr__len__get(p_src) * sizeofmember(T_UPWordArr, data[0]));
}


/**
 * Desctructor
 */
void upwordarr__dtor(T_UPWordArr *me)
{
   //if (me->flags & _UPWORDARR__STATE_FLAG__INIT){
      // some desctruct code
   //}
}

/***************************************************************************************************
 *                                          PUBLIC METHODS                                         *
 **************************************************************************************************/

/***************************************************************************************************
 *                                      ALLOCATOR, DEALLOCATOR                                     *
 **************************************************************************************************/

/*
 * Allocator and Deallocator
 *
 * Please NOTE: There's not necessary to use them! 
 * For instance, you can just allocate in stack or statically or manually from heap,
 * and use _ctor only.
 */
#if defined OBJ_COMMON__MALLOC && defined OBJ_COMMON__FREE

/* allocator */
T_UPWordArr *new_upwordarr(const T_UPWordArr_CtorParams *p_params)
{
   T_UPWordArr *me = (T_UPWordArr *)OBJ_COMMON__MALLOC( sizeof(T_UPWordArr) + p_params->length * sizeofmember(T_UPWordArr, data[0]));
   upwordarr__ctor(me, p_params);
   return me;
}

T_UPWordArr *new_upwordarr__simple(size_t len)
{
   T_UPWordArr *me = (T_UPWordArr *)OBJ_COMMON__MALLOC( sizeof(T_UPWordArr) + len * sizeofmember(T_UPWordArr, data[0]));

   const T_UPWordArr_CtorParams par = {
      .length = len,
   };
   upwordarr__ctor(me, &par);

   return me;
}

T_UPWordArr *new_upwordarr__copy(const T_UPWordArr *p_src)
{
   T_UPWordArr *me = (T_UPWordArr *)OBJ_COMMON__MALLOC( sizeof(T_UPWordArr) + upwordarr__len__get(p_src) * sizeofmember(T_UPWordArr, data[0]));
   upwordarr__ctor__copy(me, p_src);
   return me;
}

/* deallocator */
void delete_upwordarr(T_UPWordArr *me){
   upwordarr__dtor(me);
   OBJ_COMMON__FREE(me);
}
#endif



/***************************************************************************************************
 *  end of file
 **************************************************************************************************/


