/***************************************************************************************************
 *   Project:       
 *   Author:        Alex Borisov
 ***************************************************************************************************
 *   Distribution:
 *
 ***************************************************************************************************
 *   MCU Family:    
 *   Compiler:      
 ***************************************************************************************************
 *   File:          clist.c
 *   Description:   Double-linked circular lists
 *
 ***************************************************************************************************
 *   History:       14.09.2010 - [Alex Borisov] - file created
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "clist.h"

/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

/**
 * 
 * 
 * 
 * @param clist 
 */
void clist_reset (CLIST *clist)
{
    clist->prev = clist->next = clist;
}

/**
 * 
 * 
 * 
 * @param clist 
 * 
 * @return bool 
 */
bool clist_is_empty (const CLIST *clist)
{
    if (clist->next == clist && clist->prev == clist)
        return true;
    return false;
}

/**
 * 
 * 
 * 
 * @param clist 
 * @param node 
 */
void clist_add_head (CLIST *clist, CLIST *node)
{
    node->next       = clist->next;
    node->prev       = clist;
    node->next->prev = node;
    clist->next      = node;
}

/**
 * 
 * 
 * 
 * @param clist 
 * @param node 
 */
void clist_add_tail (CLIST *clist, CLIST *node)
{
    node->next       = clist;
    node->prev       = clist->prev;
    node->prev->next = node;
    clist->prev      = node;
}


void clist_add_after (CLIST *p_existing_node, CLIST *p_new_node)
{
   clist_add_head(p_existing_node, p_new_node);
}

void clist_add_before (CLIST *p_existing_node, CLIST *p_new_node)
{
   clist_add_tail(p_existing_node, p_new_node);
}


/**
 * 
 * 
 * 
 * @param clist 
 * 
 * @return CLIST* 
 */
CLIST * clist_get_tail (const CLIST *clist)
{
    CLIST *node;

    if (clist == NULL || clist->next == clist)
        return ((CLIST *)0);

    node = clist->prev;
    return node;
}

/**
 * 
 * 
 * 
 * @param clist 
 * 
 * @return CLIST* 
 */
CLIST *clist_get_head (const CLIST *clist)
{
    CLIST *node;
    if (clist == NULL || clist->prev == clist)
        return ((CLIST *)0);

    node = clist->next;
    return node;
}

/**
 * 
 * 
 * 
 * @param clist 
 * 
 * @return CLIST* 
 */
CLIST * clist_remove_head (CLIST *clist)
{
    CLIST *node;

    if (clist == NULL || clist->next == clist)
        return ((CLIST *)0);

    node             = clist->next;
    node->next->prev = clist;
    clist->next      = node->next;
    return node;
}

/**
 * 
 * 
 * 
 * @param clist 
 * 
 * @return CLIST* 
 */
CLIST * clist_remove_tail (CLIST *clist)
{
    CLIST *node;

    if (clist->prev == clist)
        return ((CLIST *)0);

    node             = clist->prev;
    node->prev->next = clist;
    clist->prev      = node->prev;
    return node;
}

/**
 * 
 * 
 * 
 * @param node 
 */
void clist_remove_entry (CLIST * node)
{
    node->prev->next = node->next;
    node->next->prev = node->prev;
}

CLIST *clist_get_next(const CLIST *p_clist, const CLIST *p_node, bool bool_circular)
{
   CLIST *p_ret = NULL;

   if (p_clist != p_node){
      p_ret = p_node->next;
      if (p_ret == p_clist){
         p_ret = bool_circular
            ? p_ret->next
            : NULL;
      }
   }

   return p_ret;
}

CLIST *clist_get_prev(const CLIST *p_clist, const CLIST *p_node, bool bool_circular)
{
   CLIST *p_ret = NULL;

   if (p_clist != p_node){
      p_ret = p_node->prev;
      if (p_ret == p_clist){
         p_ret = bool_circular
            ? p_ret->prev
            : NULL;
      }
   }

   return p_ret;
}

/**
 * 
 * 
 * 
 * @param clist 
 * @param node 
 * 
 * @return bool 
 */
bool clist_is_contains (const CLIST *clist, const CLIST *node)
{
   return (clist_get_entry_number(clist, node) >= 0)
      ? true
      : false
      ;

#if 0
    bool  result = false;
    CLIST *curr_que;

    curr_que = clist->next;

    for (;;)
    {
        if (curr_que == node)
        {
            result = true;
            break;
        }
        if (curr_que->next == clist)
            break;
        else
            curr_que = curr_que->next;
    }
    return result;
#endif
}

int clist_get_entry_number(const CLIST *clist, const CLIST *node)
{
   int result = 0;
   CLIST *curr_que;

   curr_que = clist->next;

   for (;;result++)
   {
      if (curr_que == node)
      {
         break;
      }
      if (curr_que->next == clist){
         result = -1;
         break;
      } else {
         curr_que = curr_que->next;
      }
   }
   return result;
}

/***************************************************************************************************
 *  end of file: clist.c
 **************************************************************************************************/


