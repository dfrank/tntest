/***************************************************************************************************
 * TODO: description
 * header file
 **************************************************************************************************/

#ifndef _PTARR_H
#define _PTARR_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

//#define PTARR_USE_DEF_CONFIG
#if 0
#if !defined PTARR_USE_DEF_CONFIG
#  include "lib_cfg/ptarr_cfg.h"
#endif

#if defined PTARR_USE_DEF_CONFIG || !defined _PTARR_CFG_H
#  include "ptarr_cfg_default.h"
#endif
#endif

#include "obj_common.h"
#include <stddef.h>

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

typedef struct S_PtArr T_PtArr;

/*
 * Result codes
 */
typedef enum E_PtArr_Res {
   PTARR_RES__OK,
} T_PtArr_Res;

typedef struct S_PtArr_CtorParams {
   size_t      length;
} T_PtArr_CtorParams;

/***************************************************************************************************
 *                                         OBJECT CONTEXT                                          *
 **************************************************************************************************/

/*
 * Object context
 */
struct S_PtArr {
   size_t   length;
   void    *data[];
};

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                CONSTRUCTOR, DESTRUCTOR PROTOTYPES
 **************************************************************************************************/

/**
 * Constructor
 *
 * @return PTARR_RES__OK if everything is ok, otherwise returns error code
 */
T_PtArr_Res    ptarr__ctor(T_PtArr *me, const T_PtArr_CtorParams *p_params);
void           ptarr__ctor__copy(T_PtArr *me, const T_PtArr *p_src);

/**
 * Desctructor
 */
void ptarr__dtor(T_PtArr *me);

/***************************************************************************************************
 *                                     PUBLIC METHODS PROTOTYPES
 **************************************************************************************************/

/**
 * @return size_t  length of byte array
 */
#define  ptarr__len__get(ba)            ((ba)->length)

/**
 * @return void **   pointer to data
 */
#define  ptarr__data__get_pt(ba)        ((ba)->data)

/**
 * Checks if array contains specified value.
 * If it does, then its index is returned, otherwise -1 is returned.
 * @param check_items_cnt if not null, then only first check_items_cnt items will be checked,
 *                        otherwise all me->length will be used
 */
S16 ptarr__find(T_PtArr *me, void *pt, size_t check_items_cnt);

/***************************************************************************************************
 *                                ALLOCATOR, DEALLOCATOR PROTOTYPES
 **************************************************************************************************/

/*
 * Allocator and Deallocator
 *
 * Please NOTE: There's not necessary to use them! 
 * For instance, you can just allocate in stack or statically or manually from heap,
 * and use _ctor only.
 */
#if defined OBJ_COMMON__MALLOC && defined OBJ_COMMON__FREE

/* allocator */
T_PtArr *new_ptarr(const T_PtArr_CtorParams *p_params);
T_PtArr *new_ptarr__simple(size_t len);
T_PtArr *new_ptarr__copy(const T_PtArr *p_src);
T_PtArr *new_ptarr__sum(int num, ...);

/* deallocator */
void delete_ptarr(T_PtArr *me);

#endif


#endif // _PTARR_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


