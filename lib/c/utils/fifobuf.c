/***************************************************************************************************
 * FIFO Buffer API
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "fifobuf.h"
#include <string.h>

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

/***************************************************************************************************
 *                                  PRIVATE METHODS PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                  EXTERNAL METHODS PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                      PROTECTED METHODS
 **************************************************************************************************/

/***************************************************************************************************
 *                                        PRIVATE METHODS
 **************************************************************************************************/

static void _u08__put(T_FIFOBuf *me, U08 data)
{
   if (fifobuf__status__check(me, FIFOBUF_STAT__FULL)){
      //-- set error flag if buffer is full
      fifobuf__status__set(me, FIFOBUF_STAT__ERR | FIFOBUF_ERR___OVF);
   } else {

      fifobuf__status__clr(me, FIFOBUF_STAT__EMPTY);

      *(me->p_tail++) = data;
      if (me->p_tail == me->p_end){
         me->p_tail = me->p_begin;
      }

      if (++me->cnt >= fifobuf__size__get_nolock(me)) {
         fifobuf__status__set(me, FIFOBUF_STAT__FULL);
      }
   }
}

static U08_FAST _u08__get(T_FIFOBuf *me)
{
   U08_FAST ret = 0;

   if (fifobuf__status__check(me, FIFOBUF_STAT__EMPTY)){
      //-- set error flag if buffer is empty
      fifobuf__status__set(me, FIFOBUF_STAT__ERR | FIFOBUF_ERR___UFL);
   } else {

      fifobuf__status__clr(me, FIFOBUF_STAT__FULL);

      ret = (int)*(me->p_head++);
      if (me->p_head == me->p_end){
         me->p_head = me->p_begin;
      }

      if (!(--me->cnt)) {    
         fifobuf__status__set(me, FIFOBUF_STAT__EMPTY);
      }
   }

   return ret;
}

static U08 *_u08_pointer__get_by_index(T_FIFOBuf *me, int index)
{
   U08 *p_ret = NULL;

   if (index >= me->cnt){
      //-- too large index
      //   TODO: set some error flag
      //fifobuf__status__set(me, FIFOBUF_STAT__ERR | FIFOBUF_ERR___UFL);
   } else {

      p_ret = me->p_head + index;
      if (p_ret >= me->p_end){
         p_ret -= fifobuf__size__get_nolock(me);
      }
   }

   return p_ret;
}

/***************************************************************************************************
 *                                    CONSTRUCTOR, DESTRUCTOR                                      *
 **************************************************************************************************/

/**
 * Constructor
 *
 */
T_FIFOBuf_Res fifobuf__ctor(T_FIFOBuf *me, const T_FIFOBuf_CtorParams *p_params)
{
   T_FIFOBuf_Res ret = FIFOBUF_RES__OK;
   memset(me, 0x00, sizeof(T_FIFOBuf));

   //-- some construct code
   {
      U08 *p_buf = p_params->p_buf;
      if (p_buf == NULL){
         p_buf = OBJ_COMMON__MALLOC(p_params->size);
         me->flags.buf_allocated_by_fifobuf = 1;
      }

      me->p_begin  = me->p_head = me->p_tail = p_buf;
   }

   me->cnt        = 0;
   me->p_end      = me->p_head + p_params->size;
   me->stat       = FIFOBUF_STAT__EMPTY;

   me->callback   = p_params->callback;

   if (me->callback.p_lock == NULL){
      me->callback.p_lock = EMPTY_FUNC;
   }

   if (me->callback.p_unlock == NULL){
      me->callback.p_unlock = EMPTY_FUNC;
   }

   return ret;
}


/**
 * Desctructor
 */
void fifobuf__dtor(T_FIFOBuf *me)
{
   if (me->flags.buf_allocated_by_fifobuf && me->p_begin != NULL){
      OBJ_COMMON__FREE(me->p_begin);
   }
}

/***************************************************************************************************
 *                                          PUBLIC METHODS                                         *
 **************************************************************************************************/

/**
 * Put byte to FIFO buffer
 *
 * @return count of bytes in the buffer.
 */
size_t fifobuf__u08__put(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode, U08 data)
{
   size_t ret;

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_lock( me->callback.p_user_data );
   }

   _u08__put(me, data);

   ret = me->cnt;

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_unlock( me->callback.p_user_data );
   }

   //-- return current buffer chars count
   return ret;
}

/**
 * Put byte to FIFO buffer
 *
 * @return count of bytes in the buffer.
 */
size_t fifobuf__u08__put_check(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode, U08 data, bool *p_bool_success)
{
   size_t ret;

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_lock( me->callback.p_user_data );
   }

   //-- clear OVF error so that we can check it later
   fifobuf__status__clr(me, (FIFOBUF_STAT__ERR | FIFOBUF_ERR___OVF));

   //-- actually put the data to buffer
   _u08__put(me, data);

   ret = me->cnt;

   //-- set p_bool_success depending on OVF flag
   *p_bool_success = !fifobuf__status__check(me, FIFOBUF_ERR___OVF);

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_unlock( me->callback.p_user_data );
   }

   //-- return current buffer chars count
   return ret;
}

/**
 * Get char from fifo
 */
U08_FAST fifobuf__u08__get(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode)
{
   U08_FAST ret = 0;

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_lock( me->callback.p_user_data );
   }

   ret = _u08__get(me);

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_unlock( me->callback.p_user_data );
   }

   return ret;
}

/**
 * Get char from fifo
 */
U08_FAST fifobuf__u08__get_check(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode, bool *p_bool_success)
{
   U08_FAST ret = 0;

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_lock( me->callback.p_user_data );
   }

   //-- clear UFL error so that we can check it later
   fifobuf__status__clr(me, (FIFOBUF_STAT__ERR | FIFOBUF_ERR___UFL));

   //-- actually fetch the data from buffer
   ret = _u08__get(me);

   //-- set p_bool_success depending on UFL flag
   *p_bool_success = !fifobuf__status__check(me, FIFOBUF_ERR___UFL);

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_unlock( me->callback.p_user_data );
   }

   return ret;
}

U08_FAST fifobuf__u08__get_by_index(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode, int index)
{
   U08_FAST ret = 0;

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_lock( me->callback.p_user_data );
   }

   {
      U08 *p_needed_pt = _u08_pointer__get_by_index(me, index);

      if (p_needed_pt != NULL){
         ret = (U08_FAST)*p_needed_pt;
      }
   }

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_unlock( me->callback.p_user_data );
   }

   return ret;
}

void fifobuf__u08__set_by_index(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode, int index, U08 data)
{
   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_lock( me->callback.p_user_data );
   }

   {
      U08 *p_needed_pt = _u08_pointer__get_by_index(me, index);

      if (p_needed_pt != NULL){
         *p_needed_pt = data;
      }
   }

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_unlock( me->callback.p_user_data );
   }
}

/**
 * Flush FIFO
 */
void fifobuf__flush (T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode)
{
   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_lock( me->callback.p_user_data );
   }

   me->p_head = me->p_tail = me->p_begin;
   me->cnt  = 0;
   me->stat = FIFOBUF_STAT__EMPTY;

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_unlock( me->callback.p_user_data );
   }
}

size_t fifobuf__u16__put(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode, U16 data)
{
   size_t ret;

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_lock( me->callback.p_user_data );
   }

   _u08__put(me, (U08)(data >> 0));
   _u08__put(me, (U08)(data >> 8));

   ret = me->cnt;

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_unlock( me->callback.p_user_data );
   }

   return ret;
}

size_t fifobuf__data__put(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode, const U08 *p_data, size_t len)
{
   //TODO: more efficient implementation (lock just once, and unlock at the end)

   size_t ret = fifobuf__count__get(me);

   if (ret + len <= fifobuf__size__get_lock(me)){
      while (ret < (ret + len)){
         fifobuf__u08__put(me, lock_mode, *p_data++);
         ret++;
      }
      if (fifobuf__count__get(me) != ret){
         //LOG_ERR("Self-check failed: byte count is wrong");
      }
   } else {
      //-- too many data
   }

   return ret;
}

U16_FAST fifobuf__u16__get(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode)
{
   U16_FAST ret = 0;

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_lock( me->callback.p_user_data );
   }

   *((U08 *)(&ret) + 0) = (U08)_u08__get(me);
   *((U08 *)(&ret) + 1) = (U08)_u08__get(me);

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_unlock( me->callback.p_user_data );
   }

   return ret;
}

size_t fifobuf__size__get(T_FIFOBuf *me, enum E_FIFOBuf_LockMode lock_mode)
{
   size_t ret;

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_lock( me->callback.p_user_data );
   }

   ret = (me->p_end - me->p_begin);

   if (lock_mode == FIFOBUF_LM__LOCKED){
      me->callback.p_unlock( me->callback.p_user_data );
   }

   return ret;
}

#if 0
size_t fifobuf__count__get(T_FIFOBuf *me)
{
   return me->cnt;
}

bool fifobuf__status__check(T_FIFOBuf *me, U16 val)
{
   return !!(me->stat & val);
}

void fifobuf__status__set(T_FIFOBuf *me, U16 val)
{
   me->stat |= val;
}

void fifobuf__status__clr(T_FIFOBuf *me, U16 val)
{
   me->stat &= ~val;
}
#endif


/***************************************************************************************************
 *                                      ALLOCATOR, DEALLOCATOR                                     *
 **************************************************************************************************/

/*
 * Allocator and Deallocator
 *
 * Please NOTE: There's not necessary to use them! 
 * For instance, you can just allocate in stack or statically or manually from heap,
 * and use _ctor only.
 */
#if defined OBJ_COMMON__MALLOC && defined OBJ_COMMON__FREE

/* allocator */
T_FIFOBuf *new_fifobuf(const T_FIFOBuf_CtorParams *p_params)
{
   T_FIFOBuf *me = (T_FIFOBuf *)OBJ_COMMON__MALLOC( sizeof(T_FIFOBuf) );
   fifobuf__ctor(me, p_params);
   return me;
}

/* deallocator */
void delete_fifobuf(T_FIFOBuf *me){
   fifobuf__dtor(me);
   OBJ_COMMON__FREE(me);
}
#endif



/***************************************************************************************************
 *  end of file
 **************************************************************************************************/


