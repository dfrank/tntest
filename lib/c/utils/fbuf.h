/***************************************************************************************************
 *   Project:       FIFO Buffer API
 *   Author:        Alex B.
 ***************************************************************************************************
 *   Distribution:
 *
 ***************************************************************************************************
 *   MCU Family:    PIC24/dsPIC
 *   Compiler:      MCHP C30 3.12
 ***************************************************************************************************
 *   File:          fbuf.h
 *   Description:   
 *
 ***************************************************************************************************
 *   History:       19.05.2009 - [Alex B.] - file created
 *                  20.05.2009 - [Alex B.] - change name
 *
 **************************************************************************************************/

#ifndef __FBUF_H
#define __FBUF_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include <stddef.h>

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

#define FBUF_RET_FULL       ( 0)
#define FBUF_RET_EMPTY      (-1)

enum FBUF_STAT
{
    FBUF_STAT_FULL  = (1 << 0),
    FBUF_STAT_EMPTY = (1 << 1),
    FBUF_STAT_ERR   = (1 << 2),
    
    FBUF_ERR_OVF    = (1 << 3),
    FBUF_ERR_UFL    = (1 << 4),
    FBUF_ERR_HW     = (1 << 5)
};

typedef struct __CB_FBUF
{
    volatile unsigned char * head;
    volatile unsigned char * tail;
    volatile unsigned char * begin;
    volatile unsigned char * end;

    volatile size_t          cnt;
    volatile unsigned int    stat;
    void (*lock)(void);
    void (*unlock)(void);
} FBUF;

enum FBUF_LM
{
    FBUF_ACC_LOCKED,
    FBUF_ACC_UNLOCKED
};

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

void    fbuf_config(volatile FBUF *fb, volatile unsigned char *buf, size_t size, void lock(void), void unlock(void));
int     _fbuf_put(volatile FBUF *fb, enum FBUF_LM acc, unsigned char c);
int     _fbuf_get(volatile FBUF *fb, enum FBUF_LM acc);
void    _fbuf_flush (volatile FBUF *fb, enum FBUF_LM acc);

int     _fbuf_u16_put(volatile FBUF *fb, enum FBUF_LM acc, unsigned short word);
int     _fbuf_u16_get(volatile FBUF *fb, enum FBUF_LM acc);

/* Not blocked functions (for calling from interrupt) */

#define fbuf_put(fb, c)             _fbuf_put((fb), FBUF_ACC_UNLOCKED, (c))
#define fbuf_get(fb)                _fbuf_get((fb), FBUF_ACC_UNLOCKED)
#define fbuf_flush(fb)              _fbuf_flush((fb), FBUF_ACC_UNLOCKED)

#define fbuf_u16_put(fb, word)          _fbuf_u16_put((fb), FBUF_ACC_UNLOCKED, (word))
#define fbuf_u16_get(fb)                _fbuf_u16_get((fb), FBUF_ACC_UNLOCKED)

/* Blocking functions */

#define fbuf_l_put(fb, c)           _fbuf_put((fb), FBUF_ACC_LOCKED, (c))
#define fbuf_l_get(fb)              _fbuf_get((fb), FBUF_ACC_LOCKED)
#define fbuf_l_flush(fb)            _fbuf_flush((fb), FBUF_ACC_LOCKED)

#define fbuf_l_u16_put(fb, word)          _fbuf_u16_put((fb), FBUF_ACC_LOCKED, (word))
#define fbuf_l_u16_get(fb)                _fbuf_u16_get((fb), FBUF_ACC_LOCKED)

/* Service functions */

#define fbuf_size_get(fb)           ((fb)->size)
#define fbuf_count_get(fb)          ((fb)->cnt)

#define fbuf_status_check(fb, x)    ((fb)->stat & (x))
#define fbuf_status_set(fb, x)      (fb)->stat |=  (x)
#define fbuf_status_clr(fb, x)      (fb)->stat &= ~(x)

#define fbuf_empty_check(fb)        fbuf_status_check((fb), FBUF_STAT_EMPTY)
#define fbuf_full_check(fb)         fbuf_status_check((fb), FBUF_STAT_FULL)
#define fbuf_err_check(fb)          fbuf_status_check((fb), FBUF_STAT_ERR)

#endif /* __FBUF_H */
/***************************************************************************************************
    end of file: fbuf.h
 **************************************************************************************************/
