/***************************************************************************************************
 * TODO: description
 * header file
 **************************************************************************************************/

#ifndef _UPWORDARR_H
#define _UPWORDARR_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

//#define UPWORDARR_USE_DEF_CONFIG
#if 0
#if !defined UPWORDARR_USE_DEF_CONFIG
#  include "lib_cfg/upwordarr_cfg.h"
#endif

#if defined UPWORDARR_USE_DEF_CONFIG || !defined _UPWORDARR_CFG_H
#  include "upwordarr_cfg_default.h"
#endif
#endif

#include "obj_common.h"
#include <stddef.h>

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

typedef struct S_UPWordArr T_UPWordArr;

/*
 * Result codes
 */
typedef enum E_UPWordArr_Res {
   UPWORDARR_RES__OK,
} T_UPWordArr_Res;

typedef struct S_UPWordArr_CtorParams {
   size_t      length;
} T_UPWordArr_CtorParams;

/***************************************************************************************************
 *                                         OBJECT CONTEXT                                          *
 **************************************************************************************************/

/*
 * Object context
 */
struct S_UPWordArr {
   size_t   length;
   UPWORD   data[];
};

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                CONSTRUCTOR, DESTRUCTOR PROTOTYPES
 **************************************************************************************************/

/**
 * Constructor
 *
 * @return UPWORDARR_RES__OK if everything is ok, otherwise returns error code
 */
T_UPWordArr_Res upwordarr__ctor(T_UPWordArr *me, const T_UPWordArr_CtorParams *p_params);
void          upwordarr__ctor__copy(T_UPWordArr *me, const T_UPWordArr *p_src);

/**
 * Desctructor
 */
void upwordarr__dtor(T_UPWordArr *me);

/***************************************************************************************************
 *                                     PUBLIC METHODS PROTOTYPES
 **************************************************************************************************/

/**
 * @return size_t  length of byte array
 */
#define  upwordarr__len__get(ba)            ((ba)->length)

/**
 * @return UPWORD *   pointer to data
 */
#define  upwordarr__data__get_pt(ba)        ((ba)->data)

/***************************************************************************************************
 *                                ALLOCATOR, DEALLOCATOR PROTOTYPES
 **************************************************************************************************/

/*
 * Allocator and Deallocator
 *
 * Please NOTE: There's not necessary to use them! 
 * For instance, you can just allocate in stack or statically or manually from heap,
 * and use _ctor only.
 */
#if defined OBJ_COMMON__MALLOC && defined OBJ_COMMON__FREE

/* allocator */
T_UPWordArr *new_upwordarr(const T_UPWordArr_CtorParams *p_params);
T_UPWordArr *new_upwordarr__simple(size_t len);
T_UPWordArr *new_upwordarr__copy(const T_UPWordArr *p_src);

/* deallocator */
void delete_upwordarr(T_UPWordArr *me);

#endif


#endif // _UPWORDARR_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


