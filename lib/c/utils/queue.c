/***************************************************************************************************
 *   Project:       P_0172_FW01
 *   Author:        Alex Borisov
 ***************************************************************************************************
 *   Distribution:
 *
 ***************************************************************************************************
 *   MCU Family:    -
 *   Compiler:      -
 ***************************************************************************************************
 *   File:          queue.c
 *   Description:   Circular Queue Lib
 *
 ***************************************************************************************************
 *   History:       28.07.2010 - [Alex Borisov] - file created
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "queue.h"

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

#ifndef NULL
#define NULL    0
#endif

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                  PRIVATE FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                        PRIVATE FUNCTIONS
 **************************************************************************************************/

/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

/**
 * Reset queue
 *
 * @param que
 */
void queue_reset (C_QUEUE *que)
{
    que->prev = que->next = que;
}

/**
 *
 * @param que
 *
 * @return TN_BOOL
 */
bool queue_is_empty (C_QUEUE *que)
{
    if (que->next == que && que->prev == que)
        return true;
    return false;
}

/**
 *
 * @param que
 * @param entry
 */
void queue_head_add (C_QUEUE *que, C_QUEUE *entry)
{
    entry->next       = que->next;
    entry->prev       = que;
    entry->next->prev = entry;
    que->next         = entry;
}

/**
 *
 * @param que
 * @param entry
 */
void queue_tail_add (C_QUEUE *que, C_QUEUE *entry)
{
    entry->next       = que;
    entry->prev       = que->prev;
    entry->prev->next = entry;
    que->prev         = entry;
}


C_QUEUE * queue_tail_get (C_QUEUE *que)
{
    C_QUEUE *entry;

    if (que == NULL || que->next == que)
        return ((C_QUEUE *)0);

    entry = que->prev;
    return entry;
}

/**
 *  
 * 
 * @param que 
 * 
 * @return WIDG_QUEUE* 
 */
C_QUEUE * queue_head_get (C_QUEUE *que)
{
    C_QUEUE *entry;
    if (que == NULL || que->prev == que)
        return ((C_QUEUE *)0);

    entry = que->next;
    return entry;
}

/**
 *
 * @param que
 *
 * @return WIDG_QUEUE*
 */
C_QUEUE * queue_head_remove (C_QUEUE *que)
{
    C_QUEUE *entry;

    if (que == NULL || que->next == que)
        return ((C_QUEUE *)0);

    entry             = que->next;
    entry->next->prev = que;
    que->next         = entry->next;
    return entry;
}

/**
 *
 * @param que
 *
 * @return WIDG_QUEUE*
 */
C_QUEUE * queue_tail_remove (C_QUEUE *que)
{
    C_QUEUE *entry;

    if (que->prev == que)
        return ((C_QUEUE *)0);

    entry             = que->prev;
    entry->prev->next = que;
    que->prev         = entry->prev;
    return entry;
}

/**
 *
 * @param entry
 */
void queue_entry_remove (C_QUEUE * entry)
{
    entry->prev->next = entry->next;
    entry->next->prev = entry->prev;
}

/**
 *
 * @param que
 * @param entry
 *
 * @return TN_BOOL
 */
bool queue_is_entry (C_QUEUE *que, C_QUEUE *entry)
{
    bool        result = false;
    C_QUEUE *curr_que;

    curr_que = que->next;

    for (;;)
    {
        if (curr_que == entry)
        {
            result = true;
            break;
        }
        if (curr_que->next == que)
            break;
        else
            curr_que = curr_que->next;
    }
    return result;
}

/***************************************************************************************************
 *  end of file: queue.c
 **************************************************************************************************/


