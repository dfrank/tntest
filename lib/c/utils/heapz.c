/***************************************************************************************************
 *   Project:       P-0172 - Utilities Library
 *   Author:        Alex Borisov
 ***************************************************************************************************
 *   Distribution:  Copyright � zltigo
 *   http://electronix.ru/forum/index.php?showtopic=53623
 *
 ***************************************************************************************************
 *   MCU Family:    
 *   Compiler:      
 ***************************************************************************************************
 *   File:          heapz.c
 *   Description:   Easy Heap Management Functions
 * 
 *   ��������� heap:
 *   {first_heap_mcb | memory part} {heap_mcb | memory part} ... {heap_mcb | memory part}
 *      heap_mcb    - ��������� �������� ������ (Memory Comtrol Block)
 *      memory part - ������� ������, ������� ����������� ��������������� MCB
 *  
 *   ���� mcb.next ��������� ���������� MCB ������ ��������� �� ������ MCB - ����������� ���������.
 *   ��������� mcb.prev ������� MCB ��������� ��� �� ����.
 *
 ***************************************************************************************************
 *   History:       09.06.2010 - [Alex Borisov] - file created
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include "heapz.h"
#include "heapz_conf.h"                 /* Application depended */

/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/

extern void hz_mutex_conf_cb(void);     /* ������������ ������ ���������� ��������� RTOS */
extern void hz_mutex_lock_cb(void);     /* ���������� ��������� RTOS        */
extern void hz_mutex_unlock_cb(void);   /* ������������� ��������� RTOS     */

/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

/**
 * 
 *
 */
void hz_init (void)
{
    hz_mutex_conf_cb();
}

/**
 * 
 *
 * @param heap 
 * @param start 
 * @param size 
 */
void hz_heap_init (heap_t *heap, void *start, unsigned long size)
{
    heap_mcb *fmcb;

    heap->start = (heap_mcb*)start;
    heap->hsize = size;
    heap->freem = heap->start;

    /* ���������������� ������ mcb */

    fmcb        = heap->start;
    fmcb->next  = fmcb;                                 // ����������� ���������
    fmcb->prev  = fmcb;                                 // ��������� �� ���������� MCB ��������� ��� �� ����
    fmcb->ts.size = heap->hsize - sizeof(heap_mcb);     // ������ ������� ������
    fmcb->ts.type = HEAP_MARK_FREE;                          // ������� ������ ��������
    fmcb->owner = 0;

    // ����� ������������� heap ������������ ����� ���� ��������� ����,
    // ������� ����� ������ heap ����� ������ MCB.
}


/**
 * 
 *
 * @param heap 
 * @param size 
 * @param type 
 * @param owner 
 * 
 * @return void* 
 */
void *hz_malloc (heap_t *heap, size_t size, int type, void *owner )
{
    heap_mcb    *tptr;
    void        *fptr;
    int         free_cnt = 0;

    hz_mutex_lock_cb();
    tptr = heap->freem;         /* ����� ���������� � ������� ���������� */

#if (HEAPZ_FULL_SCAN)
    heap_mcb *xptr = NULL;
#else
    heap_mcb *xptr;
#endif

#ifdef HEAPZ_ALIGN
    if(size & (HEAPZ_ALIGN - 1))
        size = size + ( HEAPZ_ALIGN - (size & ( HEAPZ_ALIGN - 1 )));
#endif

    for (;;)
    {   
        if (tptr->ts.type == HEAP_MARK_FREE)
        {
#if (HEAPZ_FULL_SCAN)
#else
            ++free_cnt;
#endif

            if (tptr->ts.size == size)                  // ��������� � ��������� ������� ������ �����?
            {   
                tptr->owner     = owner;
                tptr->ts.type   = type;                 // ����������� ����
                fptr = (unsigned char *)tptr+sizeof(heap_mcb);
#if (HEAPZ_FULL_SCAN)
                ++free_cnt;
#endif
                break;
            }
#if( HEAPZ_FULL_SCAN )
            else if (xptr == NULL)
            {
                if (tptr->ts.size >= (size + sizeof(heap_mcb))) // ������ ���������� ��� ���������� ����� � ��� MCB?
                    xptr = tptr;
                ++free_cnt;
            }
#else
            else if (tptr->ts.size >= (size + sizeof(heap_mcb))) // ������ ���������� ��� ���������� ����� � ��� MCB?
            {   
                // Create new free MCB in parent's MCB tail
                xptr = (heap_mcb *)( (unsigned char *)tptr + sizeof(heap_mcb) + size );

                xptr->next      = tptr->next;
                xptr->prev      = tptr;
                xptr->ts.size   = (tptr->ts.size - size - sizeof(heap_mcb));
                xptr->ts.type   = HEAP_MARK_FREE;

                // Reinit curent MCB

                tptr->next      = xptr;
                tptr->ts.size   = size;
                tptr->ts.type   = type;       // Mark block as used
                tptr->owner     = owner;

                // ���� ��������� MCB �� ���������, �� mcb.prev ���������� �� ���
                // ������ ������ ��������� �� ���������� (xptr) MCB

                if (xptr->next != heap->start)
                    (xptr->next)->prev = xptr;
                fptr = (unsigned char *)tptr + sizeof(heap_mcb);     // Valid pointer
                break;
            }
#endif
        }
        // Get ptr to next MCB
        tptr = tptr->next;
        if (tptr == heap->start)   // End of heap?
        {
#if (HEAPZ_FULL_SCAN)
            if (xptr != NULL)
            {   
                tptr = xptr;
                // Create new free MCB in parent's MCB tail
                xptr = (heap_mcb *)( (unsigned char *)tptr + sizeof(heap_mcb) + size );
                xptr->next = tptr->next;
                xptr->prev = tptr;
                xptr->ts.size = ( tptr->ts.size - size - sizeof(heap_mcb) );
                xptr->ts.type = HEAP_MARK_FREE;
                // Reinit curent MCB
                tptr->next = xptr;
                tptr->ts.size = size;
                tptr->ts.type = type;       // Mark block as used
                tptr->owner = owner;
                // ���� ��������� MCB �� ���������, �� mcb.prev ���������� �� ���
                // ������ ������ ��������� �� ���������� (xptr) MCB
                if( xptr->next != heap->start )
                    ( xptr->next )->prev = xptr;
                fptr = (unsigned char *)tptr + sizeof(heap_mcb);     // Valid pointer
                break;
            }
            else
#endif
            {   fptr = NULL;            // No Memory
                break;
            }
        }
    }

    if ((free_cnt == 1) && (fptr))      // ��� ����� ������ ��������� ���� ������?
        heap->freem = tptr->next;       // ��������� '������ ���������' �� ��������� MCB
                                        // �� ��� �������� ��� �� ������� ���� ����� � ���������� ����������
    hz_mutex_unlock_cb();

    return (fptr);
}

/**
 * 
 *
 * @param heap 
 * @param mem_ptr 
 */
void hz_free( heap_t *heap, void *mem_ptr )
{
    heap_mcb *xptr;
    heap_mcb *tptr;

    hz_mutex_lock_cb();

    tptr = (heap_mcb *)( (unsigned char *)mem_ptr - sizeof(heap_mcb) );

    // � ����� ���� �������������� _���_ :( ��������� �� ��������� � RAM, ����� ����� exception :(
    // ��� ������������ ����� ������� MCB � ���������� � mem_ptr
    // ����? ������ mem_ptr � �� �� ����� �������.
    // ������������ �������� ��� ����������� ����������
    xptr = tptr->prev;
    if (((xptr != tptr) && ( xptr->next != tptr )) || ( mem_ptr < (void*)heap->start ))
    {
        hz_mutex_unlock_cb();
        return;
    }
    // Valid pointer present ------------------------------------------------
    tptr->ts.type = HEAP_MARK_FREE;      // Mark as "free"
    // Check Next MCB
    xptr = tptr->next;
//  // ���� ��������� MCB �������� � �� ������ � heap..
//  if( ( xptr->ts.type == MARK_FREE )&&( xptr != heap->start ) )   // ��� ��������� ������� ������
    // ���� ��������� MCB �������� � ��������� � ��������
    if( ( xptr->ts.type == HEAP_MARK_FREE )&&( xptr == ( tptr + sizeof(heap_mcb) + tptr->ts.size ) ) )
    {   // ���������� ������� (tptr) � ��������� (xptr) MCB
        tptr->ts.size = tptr->ts.size + xptr->ts.size + sizeof(heap_mcb);
        tptr->next = xptr->next;
        // ���� ��������� MCB �� ���������, �� ������ � ��� mcb.prev �� �������
        xptr = xptr->next;
        if( xptr != heap->start )
            xptr->prev = tptr;
    }
    // Check previous MCB
    xptr = tptr->prev;
//  // ���� ���������� MCB �������� � ������� �� ������ � heap...
//  if( ( xptr->ts.type == MARK_FREE )&&( tptr != heap->start ) )   // ����������� ���
    // ���� ���������� MCB �������� � ��������� � ��������
    if( ( xptr->ts.type == HEAP_MARK_FREE )&&( tptr == ( xptr + sizeof(heap_mcb) + xptr->ts.size ) ) )
    {   // ���������� ������� (tptr) � ���������� (xptr) MCB
        xptr->ts.size = xptr->ts.size + tptr->ts.size + sizeof(heap_mcb);
        xptr->next = tptr->next;
        // ���� ��������� MCB �� ���������, �� ������ � ��� mcb.prev �� �������
        tptr = tptr->next;
        if( tptr != heap->start )
            tptr->prev = xptr;
        tptr = xptr;            // tptr ������ �� �������������� ����.
    }
    // ��������� heap->freem ��� ����� �������� ��������
    if( tptr < heap->freem )    // ������������ ���� ��������� ����� ����������� ������ ���������?
        heap->freem = tptr;     // ����� ��������� �� ������ 'free'

    hz_mutex_unlock_cb();

}

/**
 * 
 *
 * @param heap 
 */
void hz_heap_list( heap_t *heap )
{

}

/***************************************************************************************************
 *  end of file: heapz.c
 **************************************************************************************************/
