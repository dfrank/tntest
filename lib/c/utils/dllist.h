/***************************************************************************************************
 *   Project:       P_0172_FW01 - 
 *   Author:        
 ***************************************************************************************************
 *   Distribution:
 *
 ***************************************************************************************************
 *   MCU Family:    
 *   Compiler:      
 ***************************************************************************************************
 *   File:          dllist.h
 *   Description:   
 *
 ***************************************************************************************************
 *   History:       14.09.2010 - [] - file created
 *
 **************************************************************************************************/

#ifndef _DLLIST_H
#define _DLLIST_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include <csp_types.h>

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

typedef struct S_DLListItem
{
   struct S_DLListItem *p_prev;
   struct S_DLListItem *p_next;
} T_DLListItem;

typedef struct S_DLList
{
   T_DLListItem      zero_item;
} T_DLList;




/***************************************************************************************************
 *    DEFINITIONS
 **************************************************************************************************/

#if !defined(container_of)
/* given a pointer @ptr to the field @member embedded into type (usually
 * struct) @type, return pointer to the embedding instance of @type. */
#define container_of(ptr, type, member) \
   ((type *)((char *)(ptr)-(char *)(&((type *)0)->member)))
#endif




/*
 * NOTE: a lot of helper macros below were taken from Linux kernel source,
 *       from the file include/linux/list.h
 */


/**
 * dllist_entry - get the struct for this entry
 * @ptr:	the &struct TN_ListItem pointer.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the TN_ListItem member within the struct.
 */
#define dllist_entry(ptr, type, member) \
   container_of(ptr, type, member)

/**
 * dllist_for_each	-	iterate over a list
 * @pos:	the &struct TN_ListItem to use as a loop cursor.
 * @head:	the head for your list.
 */
#define dllist_for_each(pos, head) \
   for (pos = (head)->zero_item.p_next; pos != (head); pos = pos->p_next)


/**
 * dllist_first_entry - get the first element from a list
 * @ptr:	the list head to take the element from.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the TN_ListItem member within the struct.
 *
 * Note, that list is expected to be not empty.
 */
#define dllist_first_entry(ptr, type, member) \
   dllist_entry((ptr)->zero_item.p_next, type, member)

/**
 * dllist_first_entry_remove - remove the first element from a list
 * and return it
 * @ptr:	the list head to take the element from.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the TN_ListItem member within the struct.
 *
 * Note, that list is expected to be not empty.
 */
#if 0
#define dllist_first_entry_remove(ptr, type, member) \
   dllist_entry(dllist_remove_head(ptr), type, member)
#endif

/**
 * dllist_last_entry - get the last element from a list
 * @ptr:	the list head to take the element from.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the TN_ListItem member within the struct.
 *
 * Note, that list is expected to be not empty.
 */
#define dllist_last_entry(ptr, type, member) \
   dllist_entry((ptr)->p_prev, type, member)

/**
 * dllist_first_entry_or_null - get the first element from a list
 * @ptr:	the list head to take the element from.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the TN_ListItem member within the struct.
 *
 * Note that if the list is empty, it returns NULL.
 */
#define dllist_first_entry_or_null(ptr, type, member) \
   (!dllist_empty(ptr) ? dllist_first_entry(ptr, type, member) : NULL)

/**
 * dllist_next_entry - get the next element in list
 * @pos:	the type * to cursor
 * @member:	the name of the TN_ListItem member within the struct.
 */
#define dllist_next_entry(pos, member) \
   dllist_entry((pos)->member.p_next, typeof(*(pos)), member)

/**
 * dllist_prev_entry - get the prev element in list
 * @pos:	the type * to cursor
 * @member:	the name of the TN_ListItem member within the struct.
 */
#define dllist_prev_entry(pos, member) \
   dllist_entry((pos)->member.p_prev, typeof(*(pos)), member)


/**
 * dllist_for_each_prev	-	iterate over a list backwards
 * @pos:	the &struct TN_ListItem to use as a loop cursor.
 * @head:	the head for your list.
 */
#define dllist_for_each_prev(pos, head) \
   for (pos = (head)->p_prev; pos != (head); pos = pos->p_prev)

/**
 * dllist_for_each_safe - iterate over a list safe against removal of list entry
 * @pos:	the &struct TN_ListItem to use as a loop cursor.
 * @n:		another &struct TN_ListItem to use as temporary storage
 * @head:	the head for your list.
 */
#define dllist_for_each_safe(pos, n, head) \
   for (pos = (head)->p_next, n = pos->p_next; pos != (head); \
         pos = n, n = pos->p_next)

/**
 * dllist_for_each_prev_safe - iterate over a list backwards safe against removal of list entry
 * @pos:	the &struct TN_ListItem to use as a loop cursor.
 * @n:		another &struct TN_ListItem to use as temporary storage
 * @head:	the head for your list.
 */
#define dllist_for_each_prev_safe(pos, n, head) \
   for (pos = (head)->p_prev, n = pos->p_prev; \
         pos != (head); \
         pos = n, n = pos->p_prev)

/**
 * dllist_for_each_entry	-	iterate over list of given type
 * @pos:	the type * to use as a loop cursor.
 * @head:	the head for your list.
 * @member:	the name of the TN_ListItem member within the struct.
 */
#define dllist_for_each_entry(pos, head, member)				\
   for (pos = dllist_first_entry(head, typeof(*pos), member);	\
         &pos->member != &(head)->zero_item;					\
         pos = dllist_next_entry(pos, member))

/**
 * dllist_for_each_entry_reverse - iterate backwards over list of given type.
 * @pos:	the type * to use as a loop cursor.
 * @head:	the head for your list.
 * @member:	the name of the TN_ListItem member within the struct.
 */
#define dllist_for_each_entry_reverse(pos, head, member)			\
   for (pos = dllist_last_entry(head, typeof(*pos), member);		\
         &pos->member != (head); 					\
         pos = dllist_prev_entry(pos, member))

/**
 * dllist_prepare_entry - prepare a pos entry for use in dllist_for_each_entry_continue()
 * @pos:	the type * to use as a start point
 * @head:	the head of the list
 * @member:	the name of the TN_ListItem member within the struct.
 *
 * Prepares a pos entry for use as a start point in dllist_for_each_entry_continue().
 */
#define dllist_prepare_entry(pos, head, member) \
   ((pos) ? : dllist_entry(head, typeof(*pos), member))

/**
 * dllist_for_each_entry_continue - continue iteration over list of given type
 * @pos:	the type * to use as a loop cursor.
 * @head:	the head for your list.
 * @member:	the name of the TN_ListItem member within the struct.
 *
 * Continue to iterate over list of given type, continuing after
 * the current position.
 */
#define dllist_for_each_entry_continue(pos, head, member) 		\
   for (pos = dllist_next_entry(pos, member);			\
         &pos->member != (head);					\
         pos = dllist_next_entry(pos, member))

/**
 * dllist_for_each_entry_continue_reverse - iterate backwards from the given point
 * @pos:	the type * to use as a loop cursor.
 * @head:	the head for your list.
 * @member:	the name of the TN_ListItem member within the struct.
 *
 * Start to iterate over list of given type backwards, continuing after
 * the current position.
 */
#define dllist_for_each_entry_continue_reverse(pos, head, member)		\
   for (pos = dllist_prev_entry(pos, member);			\
         &pos->member != (head);					\
         pos = dllist_prev_entry(pos, member))

/**
 * dllist_for_each_entry_from - iterate over list of given type from the current point
 * @pos:	the type * to use as a loop cursor.
 * @head:	the head for your list.
 * @member:	the name of the TN_ListItem member within the struct.
 *
 * Iterate over list of given type, continuing from current position.
 */
#define dllist_for_each_entry_from(pos, head, member) 			\
   for (; &pos->member != (head);					\
         pos = dllist_next_entry(pos, member))

/**
 * dllist_for_each_entry_safe - iterate over list of given type safe against removal of list entry
 * @pos:	the type * to use as a loop cursor.
 * @n:		another type * to use as temporary storage
 * @head:	the head for your list.
 * @member:	the name of the TN_ListItem member within the struct.
 */
#define dllist_for_each_entry_safe(pos, n, head, member)			\
   for (pos = dllist_first_entry(head, typeof(*pos), member),	\
         n = dllist_next_entry(pos, member);			\
         &pos->member != (head); 					\
         pos = n, n = dllist_next_entry(n, member))

/**
 * dllist_for_each_entry_safe_continue - continue list iteration safe against removal
 * @pos:	the type * to use as a loop cursor.
 * @n:		another type * to use as temporary storage
 * @head:	the head for your list.
 * @member:	the name of the TN_ListItem member within the struct.
 *
 * Iterate over list of given type, continuing after current point,
 * safe against removal of list entry.
 */
#define dllist_for_each_entry_safe_continue(pos, n, head, member) 		\
   for (pos = dllist_next_entry(pos, member), 				\
         n = dllist_next_entry(pos, member);				\
         &pos->member != (head);						\
         pos = n, n = dllist_next_entry(n, member))

/**
 * dllist_for_each_entry_safe_from - iterate over list from current point safe against removal
 * @pos:	the type * to use as a loop cursor.
 * @n:		another type * to use as temporary storage
 * @head:	the head for your list.
 * @member:	the name of the TN_ListItem member within the struct.
 *
 * Iterate over list of given type from current point, safe against
 * removal of list entry.
 */
#define dllist_for_each_entry_safe_from(pos, n, head, member) 			\
   for (n = dllist_next_entry(pos, member);					\
         &pos->member != (head);						\
         pos = n, n = dllist_next_entry(n, member))

/**
 * dllist_for_each_entry_safe_reverse - iterate backwards over list safe against removal
 * @pos:	the type * to use as a loop cursor.
 * @n:		another type * to use as temporary storage
 * @head:	the head for your list.
 * @member:	the name of the TN_ListItem member within the struct.
 *
 * Iterate backwards over list of given type, safe against removal
 * of list entry.
 */
#define dllist_for_each_entry_safe_reverse(pos, n, head, member)		\
   for (pos = dllist_last_entry(head, typeof(*pos), member),		\
         n = dllist_prev_entry(pos, member);			\
         &pos->member != (head); 					\
         pos = n, n = dllist_prev_entry(n, member))

/**
 * dllist_safe_reset_next - reset a stale dllist_for_each_entry_safe loop
 * @pos:	the loop cursor used in the dllist_for_each_entry_safe loop
 * @n:		temporary storage used in dllist_for_each_entry_safe
 * @member:	the name of the TN_ListItem member within the struct.
 *
 * dllist_safe_reset_next is not safe to use in general if the list may be
 * modified concurrently (eg. the lock is dropped in the loop body). An
 * exception to this is if the cursor element (pos) is pinned in the list,
 * and dllist_safe_reset_next is called after re-taking the lock and before
 * completing the current iteration of the loop body.
 */
#define dllist_safe_reset_next(pos, n, member)				\
   n = dllist_next_entry(pos, member)



/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

void           dllist__reset(T_DLList *me);
bool           dllist__is_empty(const T_DLList *me);
int            dllist__items_cnt__get(const T_DLList *me);

void           dllist__item__add_head(T_DLList *me, T_DLListItem *item);
void           dllist__item__add_tail(T_DLList *me, T_DLListItem *item);

void           dllist__item__add_after (T_DLList *me, T_DLListItem *p_existing_item, T_DLListItem *p_new_item);
void           dllist__item__add_before (T_DLList *me, T_DLListItem *p_existing_item, T_DLListItem *p_new_item);

T_DLListItem  *dllist__item__get_head(const T_DLList *me);
T_DLListItem  *dllist__item__get_tail(const T_DLList *me);

T_DLListItem  *dllist__item__remove_head(T_DLList *me);
T_DLListItem  *dllist__item__remove_tail(T_DLList *me);

void           dllist__item__remove(T_DLList *me, T_DLListItem *item);
bool           dllist__item__is_contained (const T_DLList *me, const T_DLListItem *item);

T_DLListItem  *dllist__item__get_by_number(const T_DLList *me, int item_num);
int            dllist__item_number__get_by_item(const T_DLList *me, const T_DLListItem *item);

T_DLListItem  *dllist__item__get_next(const T_DLList *me, const T_DLListItem *p_item, bool bool_circular);
T_DLListItem  *dllist__item__get_prev(const T_DLList *me, const T_DLListItem *p_item, bool bool_circular);

#endif /* _DLLIST_H */
/***************************************************************************************************
    end of file: dllist.h
 **************************************************************************************************/
