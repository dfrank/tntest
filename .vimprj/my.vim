


" ���������� ���� � ����� .vimprj
let s:sVimprjPath  = expand('<sfile>:p:h')
let s:sProjectPath = simplify(s:sVimprjPath.'/..')

let $TRCOMP_BUNDLE__WIKI_NUM__PROJECT_COMMON    = 0
let $TRCOMP_BUNDLE__WIKI_NUM__TNKERNEL_DF_DEV_WIKI = 1
" NOTE: DON'T FORGET TO UPDATE WIKIS_CNT!!!
let $TRCOMP_BUNDLE__WIKIS_CNT                   = 2

let g:vimwiki_list = []
for i in range(0, ($TRCOMP_BUNDLE__WIKIS_CNT - 1))
   call add(g:vimwiki_list, {})
endfor

let g:vimwiki_list[$TRCOMP_BUNDLE__WIKI_NUM__PROJECT_COMMON] =
         \  {
         \     'maxhi': 0,
         \     'css_name': 'style.css',
         \     'auto_export': 0,
         \     'diary_index': 'diary',
         \     'template_default': '',
         \     'nested_syntaxes': {},
         \     'diary_sort': 'desc',
         \     'path': s:sProjectPath.'/project_common/stuff/vimwiki/',
         \     'diary_link_fmt': '%Y-%m-%d',
         \     'template_ext': '',
         \     'syntax': 'default',
         \     'custom_wiki2html': '',
         \     'index': 'index',
         \     'diary_header': 'Diary',
         \     'ext': '.wiki',
         \     'path_html': '',
         \     'temp': 0,
         \     'template_path': '',
         \     'list_margin': -1,
         \     'diary_rel_path': 'diary/'
         \  }

let g:vimwiki_list[$TRCOMP_BUNDLE__WIKI_NUM__TNKERNEL_DF_DEV_WIKI] =
         \  {
         \     'maxhi': 0,
         \     'css_name': 'style.css',
         \     'auto_export': 0,
         \     'diary_index': 'diary',
         \     'template_default': '',
         \     'nested_syntaxes': {},
         \     'diary_sort': 'desc',
         \     'path': s:sProjectPath.'/lib/mc/tnkernel_df/stuff/vimwiki/',
         \     'diary_link_fmt': '%Y-%m-%d',
         \     'template_ext': '',
         \     'syntax': 'default',
         \     'custom_wiki2html': '',
         \     'index': 'index',
         \     'diary_header': 'Diary',
         \     'ext': '.wiki',
         \     'path_html': '',
         \     'temp': 0,
         \     'template_path': '',
         \     'list_margin': -1,
         \     'diary_rel_path': 'diary/'
         \  }


" specify paths to subrepos

let $VIMPRJ_ENV__PATH__LIB_C_OBJ_COMMON    = s:sProjectPath."/lib/c/obj_common"
let $VIMPRJ_ENV__PATH__LIB_C_UTILS         = s:sProjectPath."/lib/c/utils"

let $VIMPRJ_ENV__PATH__LIB_MC_CSP__COMMON  = s:sProjectPath."/lib/mc/csp/common"
let $VIMPRJ_ENV__PATH__LIB_MC_CSP__PIC32   = s:sProjectPath."/lib/mc/csp/mchp/pic32"
let $VIMPRJ_ENV__PATH__LIB_MC_TNKERNEL_DF  = s:sProjectPath."/lib/mc/tnkernel_df/src"
let $VIMPRJ_ENV__PATH__LIB_MC_HARMONY      = s:sProjectPath."/lib/mc/harmony/framework"

let $VIMPRJ_ENV__PATH__UTIL__DM_CORE       = s:sProjectPath."/util/data_manager/core"
let $VIMPRJ_ENV__PATH__UTIL__DM_GUI        = s:sProjectPath."/util/data_manager/gui"

let $VIMPRJ_ENV__PATH__LIB_C_UTILS__TAGS                     = $VIMPRJ_ENV__PATH__LIB_C_UTILS."/.vimprj/.indexer_files_tags/utils"
let $VIMPRJ_ENV__PATH__LIB_C_OBJ_COMMON__TAGS                = $VIMPRJ_ENV__PATH__LIB_C_OBJ_COMMON."/.vimprj/.indexer_files_tags/obj_common"
let $VIMPRJ_ENV__PATH__LIB_MC_CSP__COMMON__TAGS              = $VIMPRJ_ENV__PATH__LIB_MC_CSP__COMMON."/.vimprj/.indexer_files_tags/csp__common"
let $VIMPRJ_ENV__PATH__LIB_MC_CSP__PIC32__TAGS               = $VIMPRJ_ENV__PATH__LIB_MC_CSP__PIC32."/.vimprj/.indexer_files_tags/csp__pic32"
let $VIMPRJ_ENV__PATH__LIB_MC_TNKERNEL_DF__TAGS              = $VIMPRJ_ENV__PATH__LIB_MC_TNKERNEL_DF."/.vimprj/.indexer_files_tags/tnkernel_df"
let $VIMPRJ_ENV__PATH__LIB_MC_HARMONY__TAGS                  = $VIMPRJ_ENV__PATH__LIB_MC_HARMONY."/.vimprj/.indexer_files_tags/harmony"

let $VIMPRJ_ENV__PATH__UTIL__DM_CORE__TAGS                   = $VIMPRJ_ENV__PATH__UTIL__DM_CORE."/.vimprj/.indexer_files_tags/data_manager__core"
let $VIMPRJ_ENV__PATH__UTIL__DM_GUI__TAGS                    = $VIMPRJ_ENV__PATH__UTIL__DM_GUI."/.vimprj/.indexer_files_tags/data_manager__gui"

let g:vimprj_env__paths = [
         \     $VIMPRJ_ENV__PATH__LIB_C_UTILS,
         \     $VIMPRJ_ENV__PATH__LIB_C_OBJ_COMMON,
         \     $VIMPRJ_ENV__PATH__LIB_MC_CSP__COMMON,
         \     $VIMPRJ_ENV__PATH__LIB_MC_CSP__PIC32,
         \     $VIMPRJ_ENV__PATH__LIB_MC_TNKERNEL_DF,
         \     $VIMPRJ_ENV__PATH__UTIL__DM_CORE,
         \     $VIMPRJ_ENV__PATH__UTIL__DM_GUI,
         \  ]

let g:FFS_paths = g:vimprj_env__paths


