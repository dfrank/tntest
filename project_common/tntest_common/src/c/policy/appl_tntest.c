/*******************************************************************************
 *    Description: 
 *
 *
 *
 * TODO: for some reason, with optimization 0, it gets stuck somewhere in _gen_exception!
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "appl_tntest.h"
#include "tntest_cfg.h"
#include "tntest.h"

#include "appl_tntest/appl_tntest_mutex.h"
#include "appl_tntest/appl_tntest_tasks.h"
#include "appl_tntest/appl_tntest_fmp.h"
#include "appl_tntest/appl_tntest_sem.h"
#include "appl_tntest/appl_tntest_event.h"
#include "appl_tntest/appl_tntest_dqueue.h"
#include "appl_tntest/appl_tntest_sys.h"
#include "appl_tntest/appl_tntest_timer.h"
#include "appl_tntest/appl_tntest_exch.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/


/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

extern struct TN_Task tn_idle_task;
extern struct TN_Task task_conf;

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

#if TN_PROFILER
static void _timing_echo(struct TN_TaskTiming *p_timing)
{
   TNT_DTMSG_I("total_run_time=%lu", (unsigned long)p_timing->total_run_time);
   TNT_DTMSG_I("got_running_cnt=%lu", (unsigned long)p_timing->got_running_cnt);
   TNT_DTMSG_I("max_consecutive_run_time=%ld", p_timing->max_consecutive_run_time);

#if TN_PROFILER_WAIT_TIME

   enum TN_WaitReason reason;

   for (reason = TN_WAIT_REASON_NONE; reason < TN_WAIT_REASONS_CNT; reason++){
      TNT_DTMSG_I("total_wait_time[%d]=%lu", reason, (unsigned long)p_timing->total_wait_time[reason]);
   }

   for (reason = TN_WAIT_REASON_NONE; reason < TN_WAIT_REASONS_CNT; reason++){
      TNT_DTMSG_I("max_consecutive_wait_time[%d]=%lu", reason, p_timing->max_consecutive_wait_time[reason]);
   }
#endif
}
#endif



/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void appl_tntest__task_body(void)
{

   for (;;){

      appl_tntest_exch();
      appl_tntest_timer();
      appl_tntest_sys();
      appl_tntest_dqueue();
      appl_tntest_event();
      appl_tntest_sem();
      appl_tntest_fmp();
      appl_tntest_mutex();
      appl_tntest_tasks();

#if TN_PROFILER
      {
         struct TN_TaskTiming timing;

         tn_task_profiler_timing_get(&tn_idle_task, &timing);
         TNT_DTMSG_I("timing of tn_idle_task:");
         _timing_echo(&timing);

         tn_task_profiler_timing_get(&task_conf, &timing);
         TNT_DTMSG_I("timing of task_conf:");
         _timing_echo(&timing);
      }
#endif

      TNT_DTMSG_I("---------------------------------------");
      TNT_DTMSG_I("passed!");
      TNT_DTMSG_I("---------------------------------------");

      //-- call arch-dependent callback when tests are passed
#if defined(TNT_TESTS_PASSED)
      TNT_TESTS_PASSED();
#endif

      tn_task_sleep(5000);
   }
}


/*******************************************************************************
 *    end of file
 ******************************************************************************/



