/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "appl_perfmeas.h"

#include "tntest_cfg.h"


/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

static U32 _appl_perfmeas__time_start = 0, _appl_perfmeas__time_end = 0;

static const char *_p_start_msg        = NULL;
static const char *_p_start_task_name  = NULL;
static const char *_p_end_msg          = NULL;
static const char *_p_end_task_name    = NULL;


/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void appl_perfmeas__init(void)
{
   
}

void appl_perfmeas__start(const char *p_start_msg, const char *p_task_name)
{
   _p_start_msg         = p_start_msg;
   _p_start_task_name   = p_task_name;
   _p_end_msg           = "--none--";

   _appl_perfmeas__time_start = TNT_PERFMEAS__CUR_VALUE__GET();
}

void appl_perfmeas__end(const char *p_end_msg, const char *p_task_name)
{
   _appl_perfmeas__time_end   = TNT_PERFMEAS__CUR_VALUE__GET();

   if (TN_IS_INT_DISABLED()){
      TNT_DEB_HALT("interrupts are disabled after exiting from previous kernel call!");
   }

   if (_appl_perfmeas__time_start != 0){
      _p_end_msg           = p_end_msg;
      _p_end_task_name     = p_task_name;

      //appl_perfmeas__echo();

      _appl_perfmeas__time_start          = 0;
   }
}

void appl_perfmeas__echo(void)
{
   U32 diff_cycles = TNT_PERFMEAS__DIFF__GET_CYCLES(_appl_perfmeas__time_start, _appl_perfmeas__time_end);
   U32 diff_ns     = TNT_PERFMEAS__DIFF__GET_NS    (_appl_perfmeas__time_start, _appl_perfmeas__time_end);

   TNT_DTMSG_I("From %s (by task %s) to %s (by task %s) %lu cycles (%lu ns) elapsed",
         _p_start_msg, _p_start_task_name,
         _p_end_msg, _p_end_task_name,
         diff_cycles, diff_ns
         );
}


/*******************************************************************************
 *    end of file
 ******************************************************************************/



