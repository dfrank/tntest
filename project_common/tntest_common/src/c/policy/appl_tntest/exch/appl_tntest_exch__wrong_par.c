/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/

#if 0

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "tntest.h"
#include "dterm.h"
#include "../appl_tntest_exch.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

TN_EXCH_DATA_BUF_DEF(test_exch_buf, struct TNTestExchData);




/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void appl_tntest_exch__wrong_par_create(void)
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("exchange: test of creation with wrong params");

   TNT_TEST_COMMENT("try to create exchange with data = NULL");
   TNT_ITEM__EXCH_MANAGE(CREATE, TNT_EXCH__1, NULL, TN_MAKE_ALIG_SIZE(sizeof(struct TNTestExchData)));
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__EXCH(TNT_EXCH__1, EXISTS, 0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("try to create exchange with size = 0");
   TNT_ITEM__EXCH_MANAGE(CREATE, TNT_EXCH__1, test_exch_buf, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__EXCH(TNT_EXCH__1, EXISTS, 0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("try to create exchange with size=(sizeof(int) - 1)");
   TNT_ITEM__EXCH_MANAGE(CREATE, TNT_EXCH__1, test_exch_buf, sizeof(int) - 1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__EXCH(TNT_EXCH__1, EXISTS, 0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("try to create exchange with size=(sizeof(int) + 1)");
   TNT_ITEM__EXCH_MANAGE(CREATE, TNT_EXCH__1, test_exch_buf, sizeof(int) + 1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__EXCH(TNT_EXCH__1, EXISTS, 0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("try to create exchange with size=(sizeof(int) * 10 + sizeof(int) / 2)");
   TNT_ITEM__EXCH_MANAGE(CREATE, TNT_EXCH__1, test_exch_buf, sizeof(int) * 10 + sizeof(int) / 2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__EXCH(TNT_EXCH__1, EXISTS, 0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

}

/*******************************************************************************
 *    end of file
 ******************************************************************************/


#endif

