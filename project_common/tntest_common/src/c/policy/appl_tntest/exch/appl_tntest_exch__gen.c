/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/

#if 0

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "tn.h"
#include "tntest.h"
#include "dterm.h"
#include "../appl_tntest_exch.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

TN_EXCH_DATA_BUF_DEF(test_exch_buf, struct TNTestExchData);




/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void appl_tntest_exch__gen_test(void)
{
   tnt_prev_check_item__reset();

   int priority_task_a = 5;
   int priority_task_b = 4;

   TNT_TEST_TITLE("exchange generic test");

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);

   TNT_TEST_COMMENT("create exchange, should be ok");
   TNT_ITEM__EXCH_MANAGE(CREATE, TNT_EXCH__1,
         test_exch_buf,
         TN_MAKE_ALIG_SIZE(sizeof(struct TNTestExchData))
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__EXCH(TNT_EXCH__1, EXISTS,          1);
         TNT_CHECK__EXCH(TNT_EXCH__1, SIZE,            sizeof(TN_UWord)/*see struct TNTestExchData*/);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );


   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__EXCH_MANAGE(DELETE, TNT_EXCH__1, /*the rest is not used*/ NULL, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__EXCH(TNT_EXCH__1, EXISTS,          0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);

}

/*******************************************************************************
 *    end of file
 ******************************************************************************/


#endif

