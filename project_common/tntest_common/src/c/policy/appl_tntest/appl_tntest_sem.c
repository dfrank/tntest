/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "appl_tntest_sem.h"
#include "tntest.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/




/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/



/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/



/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

static void _lock_full_count(enum TNT_TaskNum task_num, enum TNT_SemNum sem_num, int max_count, bool bool_polling)
{
   TNT_TEST_COMMENT("lock all count");

   int i;
   for (i = 0; i < max_count; i++){
      TNT_TEST_COMMENT("lock #%d", i);
      if (bool_polling){
         TNT_ITEM__SEND_CMD_SEM(task_num, SEM_ACQUIRE_POLLING, sem_num, TN_WAIT_INFINITE);
      } else {
         TNT_ITEM__SEND_CMD_SEM(task_num, SEM_ACQUIRE,         sem_num, TN_WAIT_INFINITE);
      }
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__SEM(sem_num, COUNT,  max_count - i - 1);

            TNT_CHECK__TASK(task_num, LAST_RETVAL, TERR_NO_ERR);
            );
   }
}

static void _signal_full_count(enum TNT_TaskNum task_num, enum TNT_SemNum sem_num, int max_count)
{
   TNT_TEST_COMMENT("signal all count");

   int i;
   for (i = 0; i < max_count; i++){
      TNT_TEST_COMMENT("signal #%d", i);
      TNT_ITEM__SEND_CMD_SEM(task_num, SEM_SIGNAL, sem_num, TN_WAIT_INFINITE);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__SEM(sem_num, COUNT,  i + 1);

            TNT_CHECK__TASK(task_num, LAST_RETVAL, TERR_NO_ERR);
            );
   }
}

static void _sem_test_wrong_params_create(void)   // {{{
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("sem test wrong params create");

#if TN_CHECK_PARAM
   TNT_TEST_COMMENT("try to create sem with max_count 0");
   TNT_ITEM__SEM_MANAGE(CREATE, TNT_SEM__1, 0, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__SEM(TNT_SEM__1, EXISTS, 0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("try to create sem with max_count -1");
   TNT_ITEM__SEM_MANAGE(CREATE, TNT_SEM__1, -1, -1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__SEM(TNT_SEM__1, EXISTS, 0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("try to create sem with start_count > max_count");
   TNT_ITEM__SEM_MANAGE(CREATE, TNT_SEM__1, 2, 1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__SEM(TNT_SEM__1, EXISTS, 0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("try to create sem with start_count -1");
   TNT_ITEM__SEM_MANAGE(CREATE, TNT_SEM__1, -1, 1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__SEM(TNT_SEM__1, EXISTS, 0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );
#endif

   TNT_TEST_COMMENT("try to create sem with start_count < max_count : should succeed");
   TNT_ITEM__SEM_MANAGE(CREATE, TNT_SEM__1, 1, 3);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__SEM(TNT_SEM__1, EXISTS, 1);
         TNT_CHECK__SEM(TNT_SEM__1, MAX_COUNT,        3);
         TNT_CHECK__SEM(TNT_SEM__1, COUNT,            1);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_ITEM__SEM_MANAGE(DELETE, TNT_SEM__1, /*the rest is not used*/ 0, 0);

}
// }}}

static void _sem_test(int max_count)   // {{{
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("sem test");

   int priority_task_a = 5;
   int priority_task_b = 4;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);

   TNT_TEST_COMMENT("create sem, should be ok");
   TNT_ITEM__SEM_MANAGE(CREATE, TNT_SEM__1, max_count, max_count);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__SEM(TNT_SEM__1, EXISTS,           1);
         TNT_CHECK__SEM(TNT_SEM__1, MAX_COUNT,        max_count);
         TNT_CHECK__SEM(TNT_SEM__1, COUNT,            max_count);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );


   _lock_full_count(TNT_TASK__A, TNT_SEM__1, max_count, false);

   TNT_TEST_COMMENT("B tries to acquire sem one more time and blocks");
   TNT_ITEM__SEND_CMD_SEM(TNT_TASK__B, SEM_ACQUIRE, TNT_SEM__1, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_SEM);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

   TNT_TEST_COMMENT("A signals -> B unblocks and gets it");
   TNT_ITEM__SEND_CMD_SEM(TNT_TASK__A, SEM_SIGNAL, TNT_SEM__1, 0/*not used*/);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("A tries to acquire one more item with zero timeout, and receives TERR_TIMEOUT immediately");
   TNT_ITEM__SEND_CMD_SEM(TNT_TASK__A, SEM_ACQUIRE, TNT_SEM__1, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_TIMEOUT);
         );

   TNT_TEST_COMMENT("A tries to acquire_polling one more item, and receives TERR_TIMEOUT immediately");
   TNT_ITEM__SEND_CMD_SEM(TNT_TASK__A, SEM_ACQUIRE_POLLING, TNT_SEM__1, TN_WAIT_INFINITE/*not used*/);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_TIMEOUT);
         );

   TNT_TEST_COMMENT("Interrupt tries to acquire_polling one more item, and receives TERR_TIMEOUT immediately");
   TNT_ITEM__INT_SEND_CMD_SEM(SEM_IACQUIRE_POLLING, TNT_SEM__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_TIMEOUT);
         );

   TNT_TEST_COMMENT("A signals sem");
   TNT_ITEM__SEND_CMD_SEM(TNT_TASK__A, SEM_SIGNAL, TNT_SEM__1, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__SEM(TNT_SEM__1, COUNT, 1);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   {
      TNT_TEST_COMMENT("Interrupt tries to acquire_polling one more item, and succeeds");
      TNT_ITEM__INT_SEND_CMD_SEM(SEM_IACQUIRE_POLLING, TNT_SEM__1);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__SEM(TNT_SEM__1, COUNT, 0);

            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
            );

      TNT_TEST_COMMENT("Interrupt signals sem");
      TNT_ITEM__INT_SEND_CMD_SEM(SEM_ISIGNAL, TNT_SEM__1);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__SEM(TNT_SEM__1, COUNT, 1);

            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
            );
   }

   {
      TNT_TEST_COMMENT("Interrupt tries to acquire_polling one more item, and succeeds");
      TNT_ITEM__INT_SEND_CMD_SEM(SEM_IACQUIRE_POLLING, TNT_SEM__1);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__SEM(TNT_SEM__1, COUNT, 0);

            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
            );

      TNT_TEST_COMMENT("B tries to acquire one more item and blocks");
      TNT_ITEM__SEND_CMD_SEM(TNT_TASK__B, SEM_ACQUIRE, TNT_SEM__1, TN_WAIT_INFINITE);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_SEM);

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );

      TNT_TEST_COMMENT("Interrupt signals sem -> B unblocks and gets it");
      TNT_ITEM__INT_SEND_CMD_SEM(SEM_ISIGNAL, TNT_SEM__1);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);

            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
            );
   }

   _signal_full_count(TNT_TASK__B, TNT_SEM__1, max_count);

   _lock_full_count(TNT_TASK__A, TNT_SEM__1, max_count, true);
   _signal_full_count(TNT_TASK__B, TNT_SEM__1, max_count);

   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__SEM_MANAGE(DELETE, TNT_SEM__1, /*the rest is not used*/ 0, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);

}
// }}}

static void _sem_test_deletion_and_release_wait(void)   // {{{
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("sem test: sem deletion while task is waiting for it, and release_wait");

   int priority_task_a = 5;
   int priority_task_b = 5;

   int max_count = 1;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);

   TNT_TEST_COMMENT("create sem, should be ok");
   TNT_ITEM__SEM_MANAGE(CREATE, TNT_SEM__1, max_count, max_count);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__SEM(TNT_SEM__1, EXISTS,           1);
         TNT_CHECK__SEM(TNT_SEM__1, MAX_COUNT,        max_count);
         TNT_CHECK__SEM(TNT_SEM__1, COUNT,            max_count);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );


   _lock_full_count(TNT_TASK__A, TNT_SEM__1, max_count, false);

   {
      TNT_TEST_COMMENT("A tries to get one more item and blocks");
      TNT_ITEM__SEND_CMD_SEM(TNT_TASK__A, SEM_ACQUIRE, TNT_SEM__1, TN_WAIT_INFINITE);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_SEM);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );

      TNT_TEST_COMMENT("B forcibly releases A from wait");
      TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_RELEASE_WAIT, TNT_TASK__A);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_FORCED);

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
            );
   }

   {
      TNT_TEST_COMMENT("A tries to get one more item and blocks");
      TNT_ITEM__SEND_CMD_SEM(TNT_TASK__A, SEM_ACQUIRE, TNT_SEM__1, TN_WAIT_INFINITE);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_SEM);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );

      TNT_TEST_COMMENT("sem gets deleted -> A receives TERR_DLT");
      TNT_ITEM__SEM_MANAGE(DELETE, TNT_SEM__1, /*the rest is not used*/ 0, 0);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__SEM(TNT_SEM__1, EXISTS, 0);

            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_DLT);
            );
   }

   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);

}
// }}}

static void _sem_wcontext_test(void)   // {{{
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("sem wrong context test");

   int priority_task_a = 5;

   int max_count = 1;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);

   TNT_TEST_COMMENT("create sem, should be ok");
   TNT_ITEM__SEM_MANAGE(CREATE, TNT_SEM__1, max_count, max_count);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__SEM(TNT_SEM__1, EXISTS,           1);
         TNT_CHECK__SEM(TNT_SEM__1, MAX_COUNT,        max_count);
         TNT_CHECK__SEM(TNT_SEM__1, COUNT,            max_count);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );


#if 0
   TNT_ITEM__SEND_CMD_SEM(TNT_TASK__A, SEM_ACQUIRE, TNT_SEM__1, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_SEM);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );
#endif

   TNT_TEST_COMMENT("Interrupt tries call task services");

   TNT_ITEM__INT_SEND_CMD_SEM(SEM_SIGNAL, TNT_SEM__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__INT_SEND_CMD_SEM(SEM_ACQUIRE, TNT_SEM__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__INT_SEND_CMD_SEM(SEM_ACQUIRE_POLLING, TNT_SEM__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );


   TNT_TEST_COMMENT("Task tries call interrupt services");

   TNT_ITEM__SEND_CMD_SEM(TNT_TASK__A, SEM_ISIGNAL, TNT_SEM__1, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__SEND_CMD_SEM(TNT_TASK__A, SEM_IACQUIRE_POLLING, TNT_SEM__1, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WCONTEXT);
         );



   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__SEM_MANAGE(DELETE, TNT_SEM__1, /*the rest is not used*/ 0, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);

}
// }}}


/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void appl_tntest_sem(void)
{
   _sem_test_wrong_params_create();
   _sem_test_deletion_and_release_wait();
   _sem_test(1);
   _sem_test(5);

   _sem_wcontext_test();
}

/*******************************************************************************
 *    end of file
 ******************************************************************************/



