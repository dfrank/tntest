/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "appl_tntest_mutex.h"
#include "tntest.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

#define  _TIMEOUT_FOR_DEADLOCK         (500)


/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/


static void _mutex_deletion_while_waiting_test(void) // {{{
{
   TNT_TEST_TITLE("mutex deletion while waiting for it");

   int priority_task_a = 6;
   int priority_task_b = 5;

   tnt_prev_check_item__reset();

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);

   TNT_ITEM__MUTEX_MANAGE(CREATE, TNT_MUTEX__1, TN_MUTEX_ATTR_INHERIT, 0);

   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER,   TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, EXISTS,   1);
         );

   TNT_TEST_COMMENT("A locks M1");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__A);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("B tries to lock M1 -> B blocks, A has priority of B");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, priority_task_b);
         );

   TNT_TEST_COMMENT("A deletes M1");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_DELETE, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, EXISTS, 0);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_DLT);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         );

   TNT_TEST_COMMENT("create M1 again");
   TNT_ITEM__MUTEX_MANAGE(CREATE, TNT_MUTEX__1, TN_MUTEX_ATTR_INHERIT, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, EXISTS, 1);
         );

   TNT_TEST_COMMENT("A locks M1");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__A);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("B tries to lock M1 -> B blocks, A has priority of B");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, priority_task_b);
         );

   TNT_TEST_COMMENT("A suspends B");
   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__A, TASK_SUSPEND, TNT_TASK__B);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, STATE, TSK_STATE_WAITSUSP);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("A deletes M1");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_DELETE, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, EXISTS, 0);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, TNT_BASE_PRIORITY);

         TNT_CHECK__TASK(TNT_TASK__B, STATE, TSK_STATE_SUSPEND);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_NONE);
         );

   TNT_TEST_COMMENT("A resumes B");
   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__A, TASK_RESUME, TNT_TASK__B);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

         TNT_CHECK__TASK(TNT_TASK__B, STATE, TSK_STATE_WAIT);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_DLT);
         );



   TNT_TEST_COMMENT("---------------");

   //TNT_ITEM__MUTEX_MANAGE(DELETE, TNT_MUTEX__1, 0, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);

}
// }}}


static void _mutex_tests__deadlock(void) // {{{
{
#if TN_MUTEX_DEADLOCK_DETECT
   TNT_TEST_TITLE("mutex deadlock check");

   int priority_task_a = 5;
   int priority_task_b = 5;
   int priority_task_c = 5;

   int priority_ceiling = 5;

   tnt_prev_check_item__reset();

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__C, priority_task_c);

   TNT_ITEM__MUTEX_MANAGE(CREATE, TNT_MUTEX__1, TN_MUTEX_ATTR_INHERIT, 0);
   TNT_ITEM__MUTEX_MANAGE(CREATE, TNT_MUTEX__2, TN_MUTEX_ATTR_CEILING, priority_ceiling);
   TNT_ITEM__MUTEX_MANAGE(CREATE, TNT_MUTEX__3, TN_MUTEX_ATTR_INHERIT, 0);

   TNT_TEST_COMMENT("--");

   TNT_TEST_COMMENT("initial state: all tasks has their base priority, no mutexes are held");
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__TASK(TNT_TASK__C, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);
         TNT_CHECK__MUTEX(TNT_MUTEX__2, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__2, LOCK_CNT, 0);
         TNT_CHECK__MUTEX(TNT_MUTEX__3, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__3, LOCK_CNT, 0);

         TNT_CHECK__SYS(DEADLOCK_CNT, 0);
         TNT_CHECK__SYS(DEADLOCK_FLAG, 0);
         );

         TNT_TEST_COMMENT("each task locks its own mutex");

         TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__A);
               TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);

               TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
               );

         TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_LOCK, TNT_MUTEX__2);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__MUTEX(TNT_MUTEX__2, HOLDER, TNT_TASK__B);
               TNT_CHECK__MUTEX(TNT_MUTEX__2, LOCK_CNT, 1);

               TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
               );

         TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__C, MUTEX_LOCK, TNT_MUTEX__3);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__MUTEX(TNT_MUTEX__3, HOLDER, TNT_TASK__C);
               TNT_CHECK__MUTEX(TNT_MUTEX__3, LOCK_CNT, 1);

               TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_NO_ERR);
               );

         TNT_TEST_COMMENT("each task tries to lock mutex owned by other task");

         TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__2);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_MUTEX_C);

               TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
               );

         TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_LOCK, TNT_MUTEX__3);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

               TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
               );

         TNT_TEST_COMMENT("the last task (that issues a deadlock) tries to lock with timeout");
         TNT_ITEM__SEND_CMD_MUTEX_TO(TNT_TASK__C, MUTEX_LOCK, TNT_MUTEX__1, _TIMEOUT_FOR_DEADLOCK);
         TNT_ITEM__WAIT(_TIMEOUT_FOR_DEADLOCK / 2);
         TNT_ITEM__CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

               TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
               TNT_CHECK__SYS(DEADLOCK_CNT, 1);
               TNT_CHECK__SYS(DEADLOCK_FLAG, 1);
               );

         TNT_ITEM__WAIT(_TIMEOUT_FOR_DEADLOCK);

         TNT_TEST_COMMENT("after timeout expired, check that C doesn't wait for mutex anymore");
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
               TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_TIMEOUT);

               TNT_CHECK__SYS(DEADLOCK_CNT, 0);
               TNT_CHECK__SYS(DEADLOCK_FLAG, 0);
               );


         TNT_TEST_COMMENT("release all locks");

         TNT_TEST_COMMENT("C unlocks M3 -> B locks it");
         TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__C, MUTEX_UNLOCK, TNT_MUTEX__3);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__MUTEX(TNT_MUTEX__3, HOLDER, TNT_TASK__B);
               TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
               TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);

               TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_NO_ERR);
               );

         TNT_TEST_COMMENT("B unlocks M3");
         TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_UNLOCK, TNT_MUTEX__3);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__MUTEX(TNT_MUTEX__3, HOLDER, TNT_TASK__NONE);
               TNT_CHECK__MUTEX(TNT_MUTEX__3, LOCK_CNT, 0);

               TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
               );

         TNT_TEST_COMMENT("B unlocks M2 -> A locks it");
         TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_UNLOCK, TNT_MUTEX__2);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__MUTEX(TNT_MUTEX__2, HOLDER, TNT_TASK__A);
               TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
               TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

               TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
               );

         TNT_TEST_COMMENT("A unlocks M2");
         TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_UNLOCK, TNT_MUTEX__2);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__MUTEX(TNT_MUTEX__2, HOLDER, TNT_TASK__NONE);
               TNT_CHECK__MUTEX(TNT_MUTEX__2, LOCK_CNT, 0);

               TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
               );

         TNT_TEST_COMMENT("A unlocks M1");
         TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_UNLOCK, TNT_MUTEX__1);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
               TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);

               TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
               );


         TNT_TEST_COMMENT("--");

         TNT_ITEM__MUTEX_MANAGE(DELETE, TNT_MUTEX__1, 0, 0);
         TNT_ITEM__MUTEX_MANAGE(DELETE, TNT_MUTEX__2, 0, 0);
         TNT_ITEM__MUTEX_MANAGE(DELETE, TNT_MUTEX__3, 0, 0);

         TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
         TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);
         TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__C, 0);

         TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
         TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);
         TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__C, 0);

#endif
}
// }}}

static void _mutex_tests__mutex_deletion(void) // {{{
{
   TNT_TEST_TITLE("mutex deletion check");

   int priority_task_a = 6;
   int priority_task_b = 5;
   int priority_task_c = 4;

   tnt_prev_check_item__reset();

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__C, priority_task_c);

   TNT_ITEM__MUTEX_MANAGE(CREATE, TNT_MUTEX__1, TN_MUTEX_ATTR_INHERIT, 0);

   //---

   TNT_TEST_COMMENT("initial state: all tasks has their base priority, no mutexes are held");
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__TASK(TNT_TASK__C, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, EXISTS, 1);
         );

   TNT_TEST_COMMENT("A locks M1");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__A);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TN_RC_OK);
         );

   TNT_TEST_COMMENT("B tries to lock M1 -> B blocks, A has priority of B");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, priority_task_b);
         );

   TNT_TEST_COMMENT("C tries to lock M1 -> B blocks, A has priority of C");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__C, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, priority_task_c);
         );

   TNT_TEST_COMMENT("A deleted M1 -> B and C become runnable and have retval TN_RC_DELETED, A has its base priority");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_DELETE, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TN_RC_DELETED);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TN_RC_DELETED);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);

         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, priority_task_a);

         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, EXISTS, 0);
         );


   TNT_TEST_COMMENT("--");

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__C, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__C, 0);

}
// }}}

static void _mutex_tests__task_deletion(bool bool_task_exit) // {{{
{
   TNT_TEST_TITLE("mutex test with task deletion, bool_task_exit=%d", bool_task_exit);

   int priority_task_a = 6;
   int priority_task_b = 5;

   tnt_prev_check_item__reset();

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);

   TNT_ITEM__MUTEX_MANAGE(CREATE, TNT_MUTEX__1, TN_MUTEX_ATTR_INHERIT, 0);

   //---

   TNT_TEST_COMMENT("initial state: all tasks has their base priority, no mutexes are held");
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, EXISTS, 1);
         );

   TNT_TEST_COMMENT("A locks M1");

   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__A);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("A locks M1 again");

   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 2);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("B tries to lock M1 -> B blocks, A has priority of B");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, priority_task_b);
         );

   TNT_TEST_COMMENT("A exits -> M1 gets unlocked and B locks it");
   if (bool_task_exit){
      TNT_TEST_COMMENT("use tn_task_exit()");
      TNT_ITEM__SEND_CMD_GENERAL(TNT_TASK__A, TASK_EXIT);
   } else {
      TNT_TEST_COMMENT("use tn_task_terminate()");
      TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   }

   //TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_UNLOCK, TNT_MUTEX__1);

   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_DORMANT);

         if (bool_task_exit){
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         }

         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);

         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__B);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);
         );

   TNT_TEST_COMMENT("B unlocks M1");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );


   TNT_TEST_COMMENT("--");

   TNT_ITEM__MUTEX_MANAGE(DELETE, TNT_MUTEX__1, 0, 0);

   TNT_TEST_COMMENT("NOTE: don't terminate A, it is already terminated");
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);

}
// }}}

//-- task is deleted while waiting for mutex
static void _mutex_tests__task_deletion_2(void) // {{{
{
   TNT_TEST_TITLE("task is deleted while waiting for mutex");

   int priority_task_a = 6;
   int priority_task_b = 5;

   tnt_prev_check_item__reset();

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);

   TNT_ITEM__MUTEX_MANAGE(CREATE, TNT_MUTEX__1, TN_MUTEX_ATTR_INHERIT, 0);

   //---

   TNT_TEST_COMMENT("initial state: all tasks has their base priority, no mutexes are held");
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, EXISTS, 1);
         );

   TNT_TEST_COMMENT("A locks M1");

   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__A);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("A locks M1 again");

   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 2);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("B tries to lock M1 -> B blocks, A has priority of B");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, priority_task_b);
         );

   TNT_TEST_COMMENT("terminate B -> A has its base priority");
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);

   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_DORMANT);

         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         );

   TNT_TEST_COMMENT("terminate A -> M1 gets unlocked");
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);

   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_DORMANT);

         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);
         );

   TNT_TEST_COMMENT("-------------------");

   TNT_TEST_COMMENT("NOTE: don't terminate tasks, they are already terminated");

   TNT_ITEM__MUTEX_MANAGE(DELETE, TNT_MUTEX__1, 0, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);

}
// }}}

static void _mutex_tests(bool bool_polling)  // {{{
{
   TNT_TEST_TITLE("mutex tests with bool_polling=%d", bool_polling);

   int priority_task_a = 6;
   int priority_task_b = 5;
   int priority_task_c = 4;


   int wait_reason_a = TSK_WAIT_REASON_DQUE_WRECEIVE;

   tnt_prev_check_item__reset();


   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__C, priority_task_c);

   TNT_ITEM__MUTEX_MANAGE(CREATE, TNT_MUTEX__1, TN_MUTEX_ATTR_INHERIT, 0);
   TNT_ITEM__MUTEX_MANAGE(CREATE, TNT_MUTEX__2, TN_MUTEX_ATTR_INHERIT, 0);

   TNT_TEST_COMMENT("initial state: all tasks have their base priority, no mutexes are held");
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, wait_reason_a);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__TASK(TNT_TASK__C, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);
         TNT_CHECK__MUTEX(TNT_MUTEX__2, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__2, LOCK_CNT, 0);
         );

   if (bool_polling){
      TNT_ITEM__SEND_CMD_EXMODE(TNT_TASK__A, POLLING);
      wait_reason_a = TSK_WAIT_REASON_NONE;

      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, wait_reason_a);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
            );
   }

   TNT_TEST_COMMENT("A locks M1 -> M1's holder is A");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__A);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("B tries to lock M1 -> B blocks, A has priority of B");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    priority_task_b);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

#if TN_MUTEX_REC
   TNT_TEST_COMMENT("---------- mutexes are recursive");
   TNT_TEST_COMMENT("A locks M1 again -> M1's lock_cnt is 2");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 2);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("A unlocks M1 -> M1's lock_cnt is 1");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );
#else
   TNT_TEST_COMMENT("---------- mutexes aren't recursive");
   TNT_TEST_COMMENT("A locks M1 again -> M1's lock_cnt is still 0, last_retval is TERR_ILUSE");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_ILUSE);
         );
#endif

   TNT_TEST_COMMENT("A locks M2 -> M2's holder is A");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__2, HOLDER, TNT_TASK__A);
         TNT_CHECK__MUTEX(TNT_MUTEX__2, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("C tries to lock M2 -> C blocks, A has priority of C");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__C, MUTEX_LOCK, TNT_MUTEX__2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    priority_task_c);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

   if (bool_polling){
      TNT_ITEM__SEND_CMD_EXMODE(TNT_TASK__A, WAIT);
      wait_reason_a = TSK_WAIT_REASON_DQUE_WRECEIVE;
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, wait_reason_a);
            );
   }


   TNT_TEST_COMMENT("A releases M1 -> B finishes waiting, M1's holder is B");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__B);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("B tries to lock M2 -> B blocks");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_LOCK, TNT_MUTEX__2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

   TNT_TEST_COMMENT("A releases M2 -> C finishes waiting, M2's holder is C, A has its base priority");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_UNLOCK, TNT_MUTEX__2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__MUTEX(TNT_MUTEX__2, HOLDER, TNT_TASK__C);
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, TNT_BASE_PRIORITY);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("A tries to lock M2 -> A blocks");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

   TNT_TEST_COMMENT("C releases M2 -> B finishes waiting, M2's holder is B");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__C, MUTEX_UNLOCK, TNT_MUTEX__2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__MUTEX(TNT_MUTEX__2, HOLDER, TNT_TASK__B);

         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("B releases M2 -> A finishes waiting, M2's holder is A");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_UNLOCK, TNT_MUTEX__2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, wait_reason_a);
         TNT_CHECK__MUTEX(TNT_MUTEX__2, HOLDER, TNT_TASK__A);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("C tries to lock M2 -> C blocks, A has priority of C");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__C, MUTEX_LOCK, TNT_MUTEX__2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, priority_task_c);

         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

   TNT_TEST_COMMENT("A tries to lock M1 -> A blocks, B has priority of C");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);
         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY, priority_task_c);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

   TNT_TEST_COMMENT("B releases M1 -> A finishes waiting, M1's holder is A, B has its base priority");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, wait_reason_a);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__A);
         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY, TNT_BASE_PRIORITY);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("B tries to lock M1 -> B blocks");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

   TNT_TEST_COMMENT("A releases M2 -> C finishes waiting, M2's holder is C, A has priority of B");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_UNLOCK, TNT_MUTEX__2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__MUTEX(TNT_MUTEX__2, HOLDER, TNT_TASK__C);
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, priority_task_b);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_NO_ERR);
         );

#if 0
   TNT_ITEM__MUTEX_MANAGE(DELETE, TNT_MUTEX__1, 0, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__LAST_RETVAL(TERR_ILUSE);
         );
#endif

   TNT_TEST_COMMENT("A releases M1 -> B finishes waiting, M1's holder is B, A has its base priority");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__B);
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, TNT_BASE_PRIORITY);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("C releases M2 -> M2's holder is none");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__C, MUTEX_UNLOCK, TNT_MUTEX__2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__2, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__2, LOCK_CNT, 0);

         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("A locks M2 -> M2's holder is A");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__2, HOLDER, TNT_TASK__A);
         TNT_CHECK__MUTEX(TNT_MUTEX__2, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("A tries to lock M1 -> A blocks");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

#if 1
   TNT_TEST_COMMENT("C tries to lock M2 with timeout -> C blocks, A has priority of C, B has priority of C");
   TNT_ITEM__SEND_CMD_MUTEX_TO(TNT_TASK__C, MUTEX_LOCK, TNT_MUTEX__2, TNT_TIMEOUT_AFTER_CMD * 2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, priority_task_c);
         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY, priority_task_c);

         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

   TNT_TEST_COMMENT("wait until timeout expires -> C releases, A and B have their base priority");
   TNT_ITEM__WAIT(TNT_TIMEOUT_AFTER_CMD * 2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY, TNT_BASE_PRIORITY);

         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_TIMEOUT);
         );
#endif


   TNT_TEST_COMMENT("C tries to lock M2 -> C blocks, A has priority of C, B has priority of C");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__C, MUTEX_LOCK, TNT_MUTEX__2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, priority_task_c);
         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY, priority_task_c);

         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );




   TNT_TEST_COMMENT("B releases M1 -> A finishes waiting, M1's holder is A, B has its base priority");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, wait_reason_a);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__A);
         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY, TNT_BASE_PRIORITY);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("A releases M2 -> C finishes waiting, M2's holder is C, A has its base priority");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_UNLOCK, TNT_MUTEX__2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__MUTEX(TNT_MUTEX__2, HOLDER, TNT_TASK__C);
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY, TNT_BASE_PRIORITY);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_NO_ERR);
         );


   TNT_TEST_COMMENT("---- release locks");

#if 1
   TNT_TEST_COMMENT("A releases M1 -> M1's holder is none");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("C releases M2 -> M2's holder is none");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__C, MUTEX_UNLOCK, TNT_MUTEX__2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__2, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__2, LOCK_CNT, 0);

         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_NO_ERR);
         );

#endif




   TNT_ITEM__MUTEX_MANAGE(DELETE, TNT_MUTEX__1, 0, 0);
   TNT_ITEM__MUTEX_MANAGE(DELETE, TNT_MUTEX__2, 0, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__C, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__C, 0);
}
// }}}

static void _mutex_unlock_order_test(void)  // {{{
{
   TNT_TEST_TITLE("mutex unlock order test");

   int priority_task_a = 6;
   int priority_task_b = 5;
   int priority_task_c = 4;

   tnt_prev_check_item__reset();

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__C, priority_task_c);

   TNT_ITEM__MUTEX_MANAGE(CREATE, TNT_MUTEX__1, TN_MUTEX_ATTR_INHERIT, 0);

   TNT_TEST_COMMENT("initial state: all tasks have their base priority, no mutexes are held");
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__TASK(TNT_TASK__C, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);
         );

   TNT_TEST_COMMENT("A locks M1 -> M1's holder is A");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__A);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("B tries to lock M1 -> B blocks, A has priority of B");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    priority_task_b);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

   TNT_TEST_COMMENT("C tries to lock M1 -> C blocks, A has priority of C");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__C, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    priority_task_c);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);

         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

   TNT_TEST_COMMENT("A releases M1 -> B unblocks and has priority of C, A has its base priority");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__B);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);

         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    priority_task_c);
         );

   TNT_TEST_COMMENT("B releases M1 -> C unblocks, B has its base priority");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__C);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);

         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("C releases M1");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__C, MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(

         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);


         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_NO_ERR);
         );


   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__MUTEX_MANAGE(DELETE, TNT_MUTEX__1, 0, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__C, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__C, 0);
}
// }}}

static void _mutex_ceil_and_illegal_stuff(void)  // {{{
{
   TNT_TEST_TITLE("mutex: ceiling mutexes, plus doing some illegal stuff");

   int priority_task_a = 6;
   int priority_task_b = 5;
   int priority_task_c = 3;
   int priority_task_d = 2;

   int m1_ceil_priority = 3;

   tnt_prev_check_item__reset();

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__C, priority_task_c);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__D, priority_task_d);

   TNT_ITEM__MUTEX_MANAGE(CREATE, TNT_MUTEX__1, TN_MUTEX_ATTR_CEILING, m1_ceil_priority);

   TNT_TEST_COMMENT("initial state: all tasks have their base priority, no mutexes are held");
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__TASK(TNT_TASK__C, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__TASK(TNT_TASK__D, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__D, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__D, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);
         );

   TNT_TEST_COMMENT("A locks M1 -> M1's holder is A, A has ceiled priority");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__A);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    m1_ceil_priority);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("B tries to unlock M1 -> TERR_ILUSE should be returned");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_ILUSE);
         );

   TNT_TEST_COMMENT("B tries to delete M1 -> TERR_ILUSE should be returned");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_DELETE, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_ILUSE);
         );

   TNT_TEST_COMMENT("D tries to lock M1 -> TERR_ILUSE should be returned, because D's priority is higher than ceil priority");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__D, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__D, LAST_RETVAL, TERR_ILUSE);
         );

   TNT_TEST_COMMENT("A releases M1 -> A has its base priority");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);

         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         );

   TNT_TEST_COMMENT("D tries to lock M1 again -> TERR_ILUSE should be returned again");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__D, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__D, LAST_RETVAL, TERR_ILUSE);
         );

   TNT_TEST_COMMENT("B locks M1");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__B);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    m1_ceil_priority);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("C tries to lock M1 -> C blocks");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__C, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_MUTEX_C);
         );


   TNT_TEST_COMMENT("B releases M1 -> B has its base priority, C locks M1");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__C);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         );

   TNT_TEST_COMMENT("C releases M1");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__C, MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);

         TNT_CHECK__TASK(TNT_TASK__C, PRIORITY,    TNT_BASE_PRIORITY);
         );

   TNT_TEST_COMMENT("Try to delete non-terminated task: TERR_WSTATE should be returned");
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WSTATE);
         );


   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__MUTEX_MANAGE(DELETE, TNT_MUTEX__1, 0, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__C, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__D, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__C, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__D, 0);
}
// }}}

static void _mutex_wcontext_test(void) // {{{
{
   TNT_TEST_TITLE("mutex wrong context test");

   int priority_task_a = 6;

   tnt_prev_check_item__reset();

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);

   TNT_ITEM__MUTEX_MANAGE(CREATE, TNT_MUTEX__1, TN_MUTEX_ATTR_INHERIT, 0);

   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);

         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER,   TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, EXISTS,   1);
         );

   TNT_TEST_COMMENT("Interrupt tries call task services");

   TNT_ITEM__INT_SEND_CMD_MUTEX(MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__INT_SEND_CMD_MUTEX(MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__INT_SEND_CMD_MUTEX(MUTEX_DELETE, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );


   TNT_TEST_COMMENT("---------------");

   TNT_ITEM__MUTEX_MANAGE(DELETE, TNT_MUTEX__1, 0, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);

}
// }}}


/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void appl_tntest_mutex(void)
{
   _mutex_unlock_order_test();
   _mutex_deletion_while_waiting_test();
   _mutex_tests__task_deletion_2();
   _mutex_tests__task_deletion(false);
   _mutex_tests__task_deletion(true);
   _mutex_tests__mutex_deletion();
   _mutex_tests__deadlock();
   _mutex_tests(false);
   _mutex_tests(true);
   _mutex_ceil_and_illegal_stuff();

   _mutex_wcontext_test();
}

/*******************************************************************************
 *    end of file
 ******************************************************************************/



