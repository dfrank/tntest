/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "appl_tntest_fmp.h"
#include "tntest.h"

#include <stdio.h>
#include <string.h>

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

#define  _FMP_BLOCKS_CNT               (3)
#define  _POISON                    (0x5a5a)

#define  _TIMEOUT_FOR_WAIT             (500)


/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

struct _FmpBlock {
   int a;
   int b;
};



/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

static struct _FmpBlock _fmp_block[ _FMP_BLOCKS_CNT ];

//static TN_FMEM_BUF_DEF(_fmp_block, struct _FmpBlock, _FMP_BLOCKS_CNT);



/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

static int _get_block_num(struct _FmpBlock *p_block)
{
   int ret = -1;

   int i;
   for (i = 0; i < _FMP_BLOCKS_CNT; i++){
      if (p_block == &_fmp_block[i]){
         ret = i;
         break;
      }
   }

   return ret;
}

static void _value_pt_reset(struct _FmpBlock **p_block)
{
   *p_block = (struct _FmpBlock *)_POISON;
}

static void _value_pt_received(enum TNT_FmpNum fmp_num, struct _FmpBlock *p_block, bool bool_should_succeed)
{
   int block_num = _get_block_num(p_block);

   TNT_DTMSG_I("Checking received pt: 0x%x (block index=%d), should %s",
         (unsigned int)p_block,
         block_num,
         (bool_should_succeed ? "be ok" : "not be changed (i.e. should be poisoned)")
         );

   if (bool_should_succeed){
      if (block_num == -1){
         TNT_DEB_HALT("received value wrong: it should be valid, but it is not (0x%x (block index=%d))",
               (unsigned int)p_block,
               block_num
               );
      }
   } else {
      if (p_block != (struct _FmpBlock *)_POISON){
         TNT_DEB_HALT("received value wrong: it should be poisoned, but it is not: 0x%x", 
               (unsigned int)p_block
               );
      }
   }

}

static void _get_all_items(enum TNT_TaskNum task_num, enum TNT_FmpNum fmp_num, bool bool_polling)
{
   TNT_TEST_COMMENT("get all items, checking that we got correct items");
   struct _FmpBlock  *p_block = NULL;

   U32 mask_all = ((1 << _FMP_BLOCKS_CNT) - 1);
   U32 mask_cur = 0;

   int i;
   for (i = 0; i < _FMP_BLOCKS_CNT; i++){
      TNT_TEST_COMMENT("get item #%d from fmp", i);
      _value_pt_reset(&p_block);
      if (bool_polling){
         TNT_ITEM__SEND_CMD_FMP(task_num, FMP_GET_POLLING, fmp_num, &p_block, TN_WAIT_INFINITE);
      } else {
         TNT_ITEM__SEND_CMD_FMP(task_num, FMP_GET, fmp_num, &p_block, TN_WAIT_INFINITE);
      }
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__FMP(fmp_num, FREE_BLOCKS_CNT,  _FMP_BLOCKS_CNT - i - 1);
            TNT_CHECK__FMP(fmp_num, USED_BLOCKS_CNT,  i + 1);

            TNT_CHECK__TASK(task_num, LAST_RETVAL, TERR_NO_ERR);
            );
      _value_pt_received(TNT_FMP__1, p_block, true);

      //-- check that address is one of our blocks
      //   NOTE: since _value_pt_received() was added, this check is now redundant.
      //   But, let it be: it's probably better to over-check.
      int block_num = _get_block_num(p_block);
      if (block_num == -1){
         TNT_DEB_HALT("p_block=0x%x, address is wrong", (unsigned int)p_block);
      }

      //-- memset the whole received block with poison
      memset(p_block, 0xa5, sizeof(*p_block));

      mask_cur |= (1 << block_num);
   }

   if (mask_cur != mask_all){
      TNT_DEB_HALT("mask_cur=0x%lx, mask_all=0x%lx", mask_cur, mask_all);
   }
}

static void _release_all_items(enum TNT_TaskNum task_num, enum TNT_FmpNum fmp_num)
{
   TNT_TEST_COMMENT("release all items");
   int i;
   for (i = 0; i < _FMP_BLOCKS_CNT; i++){
      TNT_TEST_COMMENT("release item #%d to fmp", i);
      TNT_ITEM__SEND_CMD_FMP(task_num, FMP_RELEASE, fmp_num, &_fmp_block[i], TN_WAIT_INFINITE);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__FMP(fmp_num, FREE_BLOCKS_CNT,  i + 1);
            TNT_CHECK__FMP(fmp_num, USED_BLOCKS_CNT,  _FMP_BLOCKS_CNT - i - 1);

            TNT_CHECK__TASK(task_num, LAST_RETVAL, TERR_NO_ERR);
            );
   }
}


static void _fmp_test_wrong_params_create(void)   // {{{
{
#if TN_CHECK_PARAM
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("fmp test wrong params create");

   TNT_TEST_COMMENT("try to create fmp with start_addr = NULL");
   TNT_ITEM__FMP_MANAGE(CREATE, TNT_FMP__1, NULL, sizeof(_fmp_block[0]), _FMP_BLOCKS_CNT);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__FMP(TNT_FMP__1, EXISTS, 0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("try to create fmp with block_size=(sizeof(int) - 1)");
   TNT_ITEM__FMP_MANAGE(CREATE, TNT_FMP__1, _fmp_block, sizeof(int) - 1, _FMP_BLOCKS_CNT);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__FMP(TNT_FMP__1, EXISTS, 0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("try to create fmp with block_size=(sizeof(int) + 1)");
   TNT_ITEM__FMP_MANAGE(CREATE, TNT_FMP__1, _fmp_block, sizeof(int) + 1, _FMP_BLOCKS_CNT);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__FMP(TNT_FMP__1, EXISTS, 0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("try to create fmp with block_size=(sizeof(int) * 10 + sizeof(int) / 2)");
   TNT_ITEM__FMP_MANAGE(CREATE, TNT_FMP__1, _fmp_block, sizeof(int) * 10 + sizeof(int) / 2, _FMP_BLOCKS_CNT);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__FMP(TNT_FMP__1, EXISTS, 0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("try to create fmp with blocks_cnt=0 (minimum is 2)");
   TNT_ITEM__FMP_MANAGE(CREATE, TNT_FMP__1, _fmp_block, sizeof(_fmp_block[0]), 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__FMP(TNT_FMP__1, EXISTS, 0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("try to create fmp with blocks_cnt=1 (minimum is 2)");
   TNT_ITEM__FMP_MANAGE(CREATE, TNT_FMP__1, _fmp_block, sizeof(_fmp_block[0]), 1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__FMP(TNT_FMP__1, EXISTS, 0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

#endif
}
// }}}

static void _fmp_test(void)   // {{{
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("fmp test");

   int priority_task_a = 5;
   int priority_task_b = 4;

   struct _FmpBlock  *p_block = NULL;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);

   TNT_TEST_COMMENT("create fmp, should be ok");
   TNT_ITEM__FMP_MANAGE(CREATE, TNT_FMP__1, _fmp_block, sizeof(_fmp_block[0]), _FMP_BLOCKS_CNT);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__FMP(TNT_FMP__1, EXISTS,           1);
         TNT_CHECK__FMP(TNT_FMP__1, BLOCKS_CNT,       _FMP_BLOCKS_CNT);
         TNT_CHECK__FMP(TNT_FMP__1, BLOCK_SIZE,       sizeof(_fmp_block[0]));
         TNT_CHECK__FMP(TNT_FMP__1, FREE_BLOCKS_CNT,  _FMP_BLOCKS_CNT);
         TNT_CHECK__FMP(TNT_FMP__1, USED_BLOCKS_CNT,  0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );


   _get_all_items(TNT_TASK__A, TNT_FMP__1, false);

   _value_pt_reset(&p_block);
   TNT_TEST_COMMENT("B tries to get one more item and blocks");
   TNT_ITEM__SEND_CMD_FMP(TNT_TASK__B, FMP_GET, TNT_FMP__1, &p_block, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_WFIXMEM);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);

   _value_pt_reset(&p_block);
   TNT_TEST_COMMENT("A releases item [0] -> B unblocks and gets it");
   TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_RELEASE, TNT_FMP__1, &_fmp_block[0], TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );
   _value_pt_received(TNT_FMP__1, p_block, true);

   _value_pt_reset(&p_block);
   TNT_TEST_COMMENT("A tries to get one more item with zero timeout, and receives TERR_TIMEOUT immediately");
   TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_GET, TNT_FMP__1, &p_block, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_TIMEOUT);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);

   _value_pt_reset(&p_block);
   TNT_TEST_COMMENT("A tries to get_polling one more item, and receives TERR_TIMEOUT immediately");
   TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_GET_POLLING, TNT_FMP__1, &p_block, TN_WAIT_INFINITE/*not used*/);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_TIMEOUT);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);

   _value_pt_reset(&p_block);
   TNT_TEST_COMMENT("Interrupt tries to get_polling one more item, and receives TERR_TIMEOUT immediately");
   TNT_ITEM__INT_SEND_CMD_FMP(FMP_GET_IPOLLING, TNT_FMP__1, &p_block);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_TIMEOUT);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);

   _value_pt_reset(&p_block);
   TNT_TEST_COMMENT("A tries to get one more item with non-zero and non-infinite timeout");
   TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_GET, TNT_FMP__1, &p_block, _TIMEOUT_FOR_WAIT);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_WFIXMEM);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);

   TNT_TEST_COMMENT("Wait until A wakes up");
   TNT_ITEM__WAIT(_TIMEOUT_FOR_WAIT * 2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_TIMEOUT);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);


   TNT_TEST_COMMENT("A releases item [2]");
   TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_RELEASE, TNT_FMP__1, &_fmp_block[2], TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__FMP(TNT_FMP__1, FREE_BLOCKS_CNT, 1);
         TNT_CHECK__FMP(TNT_FMP__1, USED_BLOCKS_CNT, _FMP_BLOCKS_CNT - 1);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   {
      _value_pt_reset(&p_block);
      TNT_TEST_COMMENT("Interrupt tries to get_polling one more item, and succeeds");
      TNT_ITEM__INT_SEND_CMD_FMP(FMP_GET_IPOLLING, TNT_FMP__1, &p_block);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__FMP(TNT_FMP__1, FREE_BLOCKS_CNT, 0);
            TNT_CHECK__FMP(TNT_FMP__1, USED_BLOCKS_CNT, _FMP_BLOCKS_CNT);

            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
            );
      _value_pt_received(TNT_FMP__1, p_block, true);

      TNT_TEST_COMMENT("Interrupt releases item [2]");
      TNT_ITEM__INT_SEND_CMD_FMP(FMP_IRELEASE, TNT_FMP__1, &_fmp_block[2]);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__FMP(TNT_FMP__1, FREE_BLOCKS_CNT, 1);
            TNT_CHECK__FMP(TNT_FMP__1, USED_BLOCKS_CNT, _FMP_BLOCKS_CNT - 1);

            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
            );
   }

   {
      _value_pt_reset(&p_block);
      TNT_TEST_COMMENT("Interrupt tries to get_polling one more item, and succeeds");
      TNT_ITEM__INT_SEND_CMD_FMP(FMP_GET_IPOLLING, TNT_FMP__1, &p_block);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__FMP(TNT_FMP__1, FREE_BLOCKS_CNT, 0);
            TNT_CHECK__FMP(TNT_FMP__1, USED_BLOCKS_CNT, _FMP_BLOCKS_CNT);

            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
            );
      _value_pt_received(TNT_FMP__1, p_block, true);

      _value_pt_reset(&p_block);
      TNT_TEST_COMMENT("B tries to get one more item and blocks");
      TNT_ITEM__SEND_CMD_FMP(TNT_TASK__B, FMP_GET, TNT_FMP__1, &p_block, TN_WAIT_INFINITE);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_WFIXMEM);

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );
      _value_pt_received(TNT_FMP__1, p_block, false);

      TNT_TEST_COMMENT("Interrupt releases item [2] -> B unblocks and gets it");
      TNT_ITEM__INT_SEND_CMD_FMP(FMP_IRELEASE, TNT_FMP__1, &_fmp_block[2]);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);

            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
            );
      _value_pt_received(TNT_FMP__1, p_block, true);
   }

   _release_all_items(TNT_TASK__B, TNT_FMP__1);

   TNT_TEST_COMMENT("A tries to release already released item [2], and gets TN_RC_OVERFLOW");
   TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_RELEASE, TNT_FMP__1, &_fmp_block[2], TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TN_RC_OVERFLOW);
         );

   TNT_TEST_COMMENT("Interrupt tries to release already released item [2], and gets TN_RC_OVERFLOW");
   TNT_ITEM__INT_SEND_CMD_FMP(FMP_IRELEASE, TNT_FMP__1, &_fmp_block[2]);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TN_RC_OVERFLOW);
         );


   _get_all_items(TNT_TASK__A, TNT_FMP__1, true);
   _release_all_items(TNT_TASK__B, TNT_FMP__1);

   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__FMP_MANAGE(DELETE, TNT_FMP__1, /*the rest is not used*/ NULL, 0, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);

}
// }}}

static void _fmp_test_deletion_and_release_wait(void)   // {{{
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("fmp test: fmp deletion while task is waiting for it, and release_wait");

   int priority_task_a = 5;
   int priority_task_b = 5;

   struct _FmpBlock  *p_block = NULL;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);

   TNT_TEST_COMMENT("create fmp, should be ok");
   TNT_ITEM__FMP_MANAGE(CREATE, TNT_FMP__1, _fmp_block, sizeof(_fmp_block[0]), _FMP_BLOCKS_CNT);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__FMP(TNT_FMP__1, EXISTS,           1);
         TNT_CHECK__FMP(TNT_FMP__1, BLOCKS_CNT,       _FMP_BLOCKS_CNT);
         TNT_CHECK__FMP(TNT_FMP__1, BLOCK_SIZE,       sizeof(_fmp_block[0]));
         TNT_CHECK__FMP(TNT_FMP__1, FREE_BLOCKS_CNT,  _FMP_BLOCKS_CNT);
         TNT_CHECK__FMP(TNT_FMP__1, USED_BLOCKS_CNT,  0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );


   _get_all_items(TNT_TASK__A, TNT_FMP__1, false);

   {
      _value_pt_reset(&p_block);
      TNT_TEST_COMMENT("A tries to get one more item and blocks");
      TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_GET, TNT_FMP__1, &p_block, TN_WAIT_INFINITE);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_WFIXMEM);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );
      _value_pt_received(TNT_FMP__1, p_block, false);

      TNT_TEST_COMMENT("B forcibly releases A from wait");
      TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_RELEASE_WAIT, TNT_TASK__A);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_FORCED);

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
            );
      _value_pt_received(TNT_FMP__1, p_block, false);
   }

   {
      _value_pt_reset(&p_block);
      TNT_TEST_COMMENT("A tries to get one more item and blocks");
      TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_GET, TNT_FMP__1, &p_block, TN_WAIT_INFINITE);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_WFIXMEM);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );
      _value_pt_received(TNT_FMP__1, p_block, false);

      TNT_TEST_COMMENT("fmp gets deleted -> A receives TERR_DLT");
      TNT_ITEM__FMP_MANAGE(DELETE, TNT_FMP__1, /*the rest is not used*/ NULL, 0, 0);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__FMP(TNT_FMP__1, EXISTS, 0);
#if TN_CHECK_PARAM
            TNT_CHECK__FMP(TNT_FMP__1, FREE_BLOCKS_CNT,  -1);
            TNT_CHECK__FMP(TNT_FMP__1, USED_BLOCKS_CNT,  -1);
#endif

            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_DLT);
            );
      _value_pt_received(TNT_FMP__1, p_block, false);
   }

   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);

}
// }}}

static void _fmp_test_wrong_params_all(void)   // {{{
{
#if TN_CHECK_PARAM
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("fmp test wrong params all");

   int priority_task_a = 5;

   struct _FmpBlock  *p_block = NULL;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);

   TNT_TEST_COMMENT("create fmp, should be ok");
   TNT_ITEM__FMP_MANAGE(CREATE, TNT_FMP__1, _fmp_block, sizeof(_fmp_block[0]), _FMP_BLOCKS_CNT);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__FMP(TNT_FMP__1, EXISTS,           1);
         TNT_CHECK__FMP(TNT_FMP__1, BLOCKS_CNT,       _FMP_BLOCKS_CNT);
         TNT_CHECK__FMP(TNT_FMP__1, BLOCK_SIZE,       sizeof(_fmp_block[0]));
         TNT_CHECK__FMP(TNT_FMP__1, FREE_BLOCKS_CNT,  _FMP_BLOCKS_CNT);
         TNT_CHECK__FMP(TNT_FMP__1, USED_BLOCKS_CNT,  0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("get with NULL p_data");
   TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_GET, TNT_FMP__1, NULL, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_GET_POLLING, TNT_FMP__1, NULL, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_ITEM__INT_SEND_CMD_FMP(FMP_GET_IPOLLING, TNT_FMP__1, NULL);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("release with NULL p_data");
   TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_RELEASE, TNT_FMP__1, NULL, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_ITEM__INT_SEND_CMD_FMP(FMP_IRELEASE, TNT_FMP__1, NULL);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("now, check operations on deleted object");
   TNT_ITEM__FMP_MANAGE(DELETE, TNT_FMP__1, /*the rest is not used*/ NULL, 0, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__FMP(TNT_FMP__1, EXISTS,           0);
         TNT_CHECK__FMP(TNT_FMP__1, FREE_BLOCKS_CNT,  -1);
         TNT_CHECK__FMP(TNT_FMP__1, USED_BLOCKS_CNT,  -1);
         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   _value_pt_reset(&p_block);
   TNT_TEST_COMMENT("get with non-existing fmp");
   TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_GET, TNT_FMP__1, &p_block, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NOEXS);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);

   _value_pt_reset(&p_block);
   TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_GET_POLLING, TNT_FMP__1, &p_block, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NOEXS);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);

   _value_pt_reset(&p_block);
   TNT_ITEM__INT_SEND_CMD_FMP(FMP_GET_IPOLLING, TNT_FMP__1, &p_block);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NOEXS);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);

   _value_pt_reset(&p_block);
   TNT_TEST_COMMENT("release with non-existing fmp");
   TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_RELEASE, TNT_FMP__1, &p_block, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NOEXS);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);

   _value_pt_reset(&p_block);
   TNT_ITEM__INT_SEND_CMD_FMP(FMP_IRELEASE, TNT_FMP__1, &p_block);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NOEXS);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);


   TNT_TEST_COMMENT("---- done, delete objects");


   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);

#endif
}
// }}}

static void _fmp_wcontext_test(void)   // {{{
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("fmp wrong context test");

   int priority_task_a = 5;

   struct _FmpBlock  *p_block = NULL;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);

   TNT_TEST_COMMENT("create fmp, should be ok");
   TNT_ITEM__FMP_MANAGE(CREATE, TNT_FMP__1, _fmp_block, sizeof(_fmp_block[0]), _FMP_BLOCKS_CNT);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__FMP(TNT_FMP__1, EXISTS,           1);
         TNT_CHECK__FMP(TNT_FMP__1, BLOCKS_CNT,       _FMP_BLOCKS_CNT);
         TNT_CHECK__FMP(TNT_FMP__1, BLOCK_SIZE,       sizeof(_fmp_block[0]));
         TNT_CHECK__FMP(TNT_FMP__1, FREE_BLOCKS_CNT,  _FMP_BLOCKS_CNT);
         TNT_CHECK__FMP(TNT_FMP__1, USED_BLOCKS_CNT,  0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );


   TNT_TEST_COMMENT("Interrupt tries call task services");

   _value_pt_reset(&p_block);
   TNT_ITEM__INT_SEND_CMD_FMP(FMP_GET, TNT_FMP__1, &p_block);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);

   _value_pt_reset(&p_block);
   TNT_ITEM__INT_SEND_CMD_FMP(FMP_GET_POLLING, TNT_FMP__1, &p_block);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);

   _value_pt_reset(&p_block);
   TNT_ITEM__INT_SEND_CMD_FMP(FMP_RELEASE, TNT_FMP__1, &p_block);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);

   TNT_TEST_COMMENT("Task tries call interrupt services");

   _value_pt_reset(&p_block);
   TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_GET_IPOLLING, TNT_FMP__1, &p_block, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WCONTEXT);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);

   _value_pt_reset(&p_block);
   TNT_ITEM__SEND_CMD_FMP(TNT_TASK__A, FMP_IRELEASE, TNT_FMP__1, &p_block, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WCONTEXT);
         );
   _value_pt_received(TNT_FMP__1, p_block, false);



   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__FMP_MANAGE(DELETE, TNT_FMP__1, /*the rest is not used*/ NULL, 0, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);

}
// }}}


/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void appl_tntest_fmp(void)
{
   _fmp_test();
   _fmp_test_wrong_params_create();
   _fmp_test_wrong_params_all();
   _fmp_test_deletion_and_release_wait();

   _fmp_wcontext_test();
}

/*******************************************************************************
 *    end of file
 ******************************************************************************/



