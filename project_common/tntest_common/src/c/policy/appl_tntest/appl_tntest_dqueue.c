/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "appl_tntest_dqueue.h"
#include "tntest.h"

#include <string.h>

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

#define  _DQUEUE_SIZE               (3)

#define  _TIMEOUT_FOR_WAIT          (500)
#define  _POISON                    (0x5a5a)


#define  _PATTERN_POISON               (0x5a5a)

#ifdef __XC16__
//-- XC16 seems buggy: if there is too much data, all the data gets mixed up.
//   so, if we remove some of the comments, it works again some of the comments, it works again.
#  undef TNT_TEST_COMMENT
#  define  TNT_TEST_COMMENT(str, ...)     TNT_DTMSG_COMMENT("comment removed on PIC24/dsPIC"); tnt_file_line_echo(__LINE__, __FILE__)
#endif

/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

#if 0
enum _ItemValue {
   _ITEM_VALUE__1 = 0x9471,
   _ITEM_VALUE__2 = 0x1050,
   _ITEM_VALUE__3 = 0x8574,
   _ITEM_VALUE__4 = 0x4841,
   _ITEM_VALUE__5 = 0x6832,
};
#endif

struct _SendReceiveValue {
   int send;
   int receive;
};

enum _QueueFlag {
   _QUEUE_FLAG__1 = (1 << 0),
   _QUEUE_FLAG__2 = (1 << 1),
};


/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

static int _dqueue_buf[ TNT_DQUEUES_CNT ][ _DQUEUE_SIZE ];

//-- array for keeping track of values that we've sent
//   and that we expect to receive
static struct _SendReceiveValue _srvalue[ TNT_DQUEUES_CNT ];

static unsigned int _flags_pattern_a = 0;

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

static void _pattern_reset(unsigned int *p_pattern)
{
   if (p_pattern != NULL){
      *p_pattern = _PATTERN_POISON;
   }
}

static void _pattern_check(unsigned int *p_pattern, unsigned int value)
{
   if (p_pattern != NULL){
      if (*p_pattern == value){
         TNT_DTMSG_BARE("returned pattern is correct: 0x%x", *p_pattern);
      } else {
         TNT_DEB_HALT("p_pattern should be 0x%x, but it is 0x%x",
               value, *p_pattern
               );
      }
   }
}


static void _srvalues_reset(void)
{
   //-- reset sent/received values
   memset(_srvalue, 0x00, sizeof(_srvalue));
}

static void _value_reset(int *p_item_value)
{
   *p_item_value = _POISON;
}

/*
 * @param task_num : if TNT_TASK__NONE, then send from interrupt, and timeout is ignored
 */
static void _send(enum TNT_TaskNum task_num, enum TNT_DQueueNum dqueue_num, TN_Timeout timeout, bool bool_should_succeed)
{
   TNT_DTMSG_I("Sending value %d..", _srvalue[dqueue_num].send);

   if (task_num != TNT_TASK__NONE){
      TNT_ITEM__SEND_CMD_DQUEUE(
            task_num,
            DQUEUE_SEND,
            dqueue_num,
            (void *)(_srvalue[dqueue_num].send),
            timeout
            );
   } else {
      TNT_ITEM__INT_SEND_CMD_DQUEUE(
            DQUEUE_ISEND_POLLING,
            dqueue_num,
            (void *)(_srvalue[dqueue_num].send)
            );
   }

   if (bool_should_succeed){
      _srvalue[dqueue_num].send++;
   }
}

static void _value_check_received(enum TNT_DQueueNum dqueue_num, int item_value, bool bool_should_succeed)
{
   int expected_value = bool_should_succeed
      ? _srvalue[dqueue_num].receive++
      : _POISON;

   TNT_DTMSG_I("Checking received value: 0x%x (expected: 0x%x)", item_value, expected_value);

   if (item_value != expected_value){
      TNT_DEB_HALT("received value wrong: it should be 0x%x, but it is 0x%x",
            expected_value,
            item_value
            );
   }

}

static void _send_all(enum TNT_TaskNum task_num, enum TNT_DQueueNum dqueue_num, TN_Timeout timeout)
{
   int i;
   for (i = 0; i < _DQUEUE_SIZE; i++){
      TNT_TEST_COMMENT("Write item #%d..", i);
      _send(task_num, dqueue_num, timeout, true);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__DQUEUE(dqueue_num, WRITTEN_ITEMS_CNT, i + 1);
            TNT_CHECK__DQUEUE(dqueue_num, FREE_ITEMS_CNT, _DQUEUE_SIZE - i - 1);

            TNT_CHECK__TASK(task_num, LAST_RETVAL, TERR_NO_ERR);
            );
   }
}

static void _receive_all(enum TNT_TaskNum task_num, enum TNT_DQueueNum dqueue_num, TN_Timeout timeout)
{
   int i;
   int item_value = 0;

   for (i = 0; i < _DQUEUE_SIZE; i++){
      _value_reset(&item_value);
      TNT_TEST_COMMENT("Receive item #%d..", i);
      TNT_ITEM__SEND_CMD_DQUEUE(task_num, DQUEUE_RECEIVE, dqueue_num, &item_value, timeout);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__DQUEUE(dqueue_num, WRITTEN_ITEMS_CNT, _DQUEUE_SIZE - i - 1);
            TNT_CHECK__DQUEUE(dqueue_num, FREE_ITEMS_CNT, i + 1);

            TNT_CHECK__TASK(task_num, LAST_RETVAL, TERR_NO_ERR);
            );
      _value_check_received(TNT_DQUEUE__1, item_value, true);
   }
}

static void _dqueue_deletion_test(void)   // {{{
{
   _srvalues_reset();
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("dqueue deletion test");

   int priority_task_a = 5;
   int priority_task_b = 4;

   int item_value = 0;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);

   TNT_TEST_COMMENT("create dqueue, should be ok");
   TNT_ITEM__DQUEUE_MANAGE(CREATE, TNT_DQUEUE__1, NULL, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, EXISTS,              1);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, ITEMS_CNT,           0);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, FREE_ITEMS_CNT,      0);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, WRITTEN_ITEMS_CNT,   0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("A tries to read from dqueue and blocks");
   TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_RECEIVE, TNT_DQUEUE__1, &item_value, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

   TNT_TEST_COMMENT("dqueue gets deleted -> A unblocks with TERR_DLT");
   TNT_ITEM__DQUEUE_MANAGE(DELETE, TNT_DQUEUE__1, /*the rest is not used*/ 0, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_DLT);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, EXISTS, 0);
#if TN_CHECK_PARAM
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, FREE_ITEMS_CNT, -1);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, WRITTEN_ITEMS_CNT, -1);
#endif

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("Create dqueue again");
   TNT_ITEM__DQUEUE_MANAGE(CREATE, TNT_DQUEUE__1, NULL, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, EXISTS,              1);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, FREE_ITEMS_CNT,      0);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, WRITTEN_ITEMS_CNT,   0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );


   TNT_TEST_COMMENT("A tries to write to dqueue and blocks");
   _send(TNT_TASK__A, TNT_DQUEUE__1, TN_WAIT_INFINITE, false);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WSEND);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

   TNT_TEST_COMMENT("dqueue gets deleted -> A unblocks with TERR_DLT");
   TNT_ITEM__DQUEUE_MANAGE(DELETE, TNT_DQUEUE__1, /*the rest is not used*/ 0, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_DLT);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, EXISTS, 0);
#if TN_CHECK_PARAM
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, FREE_ITEMS_CNT, -1);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, WRITTEN_ITEMS_CNT, -1);
#endif

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );


   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);

}
// }}}

static void _dqueue_no_buffer_test(void)   // {{{
{
   _srvalues_reset();
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("dqueue zero size test");

   int priority_task_a = 5;
   int priority_task_b = 4;

   int item_value = 0;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);

   TNT_TEST_COMMENT("create dqueue, should be ok");
   TNT_ITEM__DQUEUE_MANAGE(CREATE, TNT_DQUEUE__1, NULL, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, EXISTS,              1);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, ITEMS_CNT,           0);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, WRITTEN_ITEMS_CNT,   0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("A exmode is polling");
   TNT_ITEM__SEND_CMD_EXMODE(TNT_TASK__A, POLLING);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );


   _value_reset(&item_value);
   TNT_TEST_COMMENT("Interrupt tries to read polling from dqueue");
   TNT_ITEM__INT_SEND_CMD_DQUEUE(DQUEUE_IRECEIVE_POLLING, TNT_DQUEUE__1, &item_value);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_TIMEOUT);
         );
   _value_check_received(TNT_DQUEUE__1, item_value, false);

   TNT_TEST_COMMENT("Interrupt tries to write polling to dqueue");
   TNT_ITEM__INT_SEND_CMD_DQUEUE(DQUEUE_ISEND_POLLING, TNT_DQUEUE__1, &item_value);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_TIMEOUT);
         );

   _value_reset(&item_value);
   TNT_TEST_COMMENT("A tries to read polling from dqueue");
   TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_RECEIVE_POLLING, TNT_DQUEUE__1, &item_value, TN_WAIT_INFINITE/*not used*/);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_TIMEOUT);
         );
   _value_check_received(TNT_DQUEUE__1, item_value, false);

   {
      _value_reset(&item_value);
      TNT_TEST_COMMENT("A tries to read from dqueue and blocks");
      TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_RECEIVE, TNT_DQUEUE__1, &item_value, TN_WAIT_INFINITE);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );
      _value_check_received(TNT_DQUEUE__1, item_value, false);

      TNT_TEST_COMMENT("B writes to dqueue -> A unblocks");
      _send(TNT_TASK__B, TNT_DQUEUE__1, TN_WAIT_INFINITE, true);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
            );
      _value_check_received(TNT_DQUEUE__1, item_value, true);
   }
   {
      _value_reset(&item_value);
      TNT_TEST_COMMENT("A tries to read from dqueue and blocks");
      TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_RECEIVE, TNT_DQUEUE__1, &item_value, TN_WAIT_INFINITE);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );
      _value_check_received(TNT_DQUEUE__1, item_value, false);

      TNT_TEST_COMMENT("Interrupt writes to dqueue -> A unblocks");
      _send(TNT_TASK__NONE, TNT_DQUEUE__1, 0/*not used*/, true);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
            );
      _value_check_received(TNT_DQUEUE__1, item_value, true);
   }

   //--

   {
      _value_reset(&item_value);
      TNT_TEST_COMMENT("B tries to write to dqueue and blocks");
      _send(TNT_TASK__B, TNT_DQUEUE__1, TN_WAIT_INFINITE, true);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WSEND);

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );

      _value_reset(&item_value);
      TNT_TEST_COMMENT("A reads from dqueue -> B unblocks");
      TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_RECEIVE, TNT_DQUEUE__1, &item_value, TN_WAIT_INFINITE);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
            );
      _value_check_received(TNT_DQUEUE__1, item_value, true);
   }

   {
      _value_reset(&item_value);
      TNT_TEST_COMMENT("B tries to write to dqueue and blocks");
      _send(TNT_TASK__B, TNT_DQUEUE__1, TN_WAIT_INFINITE, true);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WSEND);

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );

      _value_reset(&item_value);
      TNT_TEST_COMMENT("Interrupt reads from dqueue -> B unblocks");
      TNT_ITEM__INT_SEND_CMD_DQUEUE(DQUEUE_IRECEIVE_POLLING, TNT_DQUEUE__1, &item_value);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);

            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
            );
      _value_check_received(TNT_DQUEUE__1, item_value, true);
   }

   


   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__DQUEUE_MANAGE(DELETE, TNT_DQUEUE__1, /*the rest is not used*/ 0, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);

}
// }}}

static void _dqueue_test(void)   // {{{
{
   _srvalues_reset();
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("dqueue test");

   int priority_task_a = 5;
   int priority_task_b = 4;
   int priority_task_c = 3;

   int item_value = 0;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__C, priority_task_c);

#if TN_CHECK_PARAM
   TNT_TEST_COMMENT("create dqueue with negative items_cnt, should fail");
   TNT_ITEM__DQUEUE_MANAGE(CREATE, TNT_DQUEUE__1, (void **)_dqueue_buf[ TNT_DQUEUE__1 ], -1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, EXISTS,              0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );
#endif

   TNT_TEST_COMMENT("create dqueue, should be ok");
   TNT_ITEM__DQUEUE_MANAGE(CREATE, TNT_DQUEUE__1, (void **)_dqueue_buf[ TNT_DQUEUE__1 ], _DQUEUE_SIZE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__C, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__C, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, EXISTS,              1);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, ITEMS_CNT,           _DQUEUE_SIZE);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, WRITTEN_ITEMS_CNT,   0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );


   TNT_TEST_COMMENT("A exmode is polling");
   TNT_ITEM__SEND_CMD_EXMODE(TNT_TASK__A, POLLING);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );


   _value_reset(&item_value);
   TNT_TEST_COMMENT("Interrupt tries to read polling from dqueue");
   TNT_ITEM__INT_SEND_CMD_DQUEUE(DQUEUE_IRECEIVE_POLLING, TNT_DQUEUE__1, &item_value);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_TIMEOUT);
         );
   _value_check_received(TNT_DQUEUE__1, item_value, false);

   _value_reset(&item_value);
   TNT_TEST_COMMENT("A tries to read polling from dqueue");
   TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_RECEIVE_POLLING, TNT_DQUEUE__1, &item_value, TN_WAIT_INFINITE/*not used*/);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_TIMEOUT);
         );
   _value_check_received(TNT_DQUEUE__1, item_value, false);

   _value_reset(&item_value);
   TNT_TEST_COMMENT("A tries to read from dqueue with non-zero and non-infinite timeout");
   TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_RECEIVE, TNT_DQUEUE__1, &item_value, _TIMEOUT_FOR_WAIT);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );
   _value_check_received(TNT_DQUEUE__1, item_value, false);

   TNT_TEST_COMMENT("Wait until A wakes up");
   TNT_ITEM__WAIT(_TIMEOUT_FOR_WAIT * 2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_TIMEOUT);
         );
   _value_check_received(TNT_DQUEUE__1, item_value, false);


   _value_reset(&item_value);
   TNT_TEST_COMMENT("A tries to read from dqueue and blocks");
   TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_RECEIVE, TNT_DQUEUE__1, &item_value, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );
   _value_check_received(TNT_DQUEUE__1, item_value, false);

   TNT_TEST_COMMENT("B writes to dqueue -> A unblocks");
   _send(TNT_TASK__B, TNT_DQUEUE__1, TN_WAIT_INFINITE, true);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );
   _value_check_received(TNT_DQUEUE__1, item_value, true);

   TNT_TEST_COMMENT("B writes to dqueue until it is full");
   _send_all(TNT_TASK__B, TNT_DQUEUE__1, TN_WAIT_INFINITE);

   {
      TNT_TEST_COMMENT("Interrupt tries to write polling to dqueue");
      _send(TNT_TASK__NONE, TNT_DQUEUE__1, 0/*not used*/, false);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_TIMEOUT);
            );

      TNT_TEST_COMMENT("A tries to write polling to dqueue");
      _send(TNT_TASK__A, TNT_DQUEUE__1, 0, false);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_TIMEOUT);
            );

      TNT_TEST_COMMENT("A tries to polling to dqueue with non-zero and non-infinite timeout");
      _send(TNT_TASK__A, TNT_DQUEUE__1, _TIMEOUT_FOR_WAIT, false);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WSEND);
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );

      TNT_TEST_COMMENT("Wait until A wakes up");
      TNT_ITEM__WAIT(_TIMEOUT_FOR_WAIT * 2);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_TIMEOUT);
            );

      TNT_TEST_COMMENT("A tries to write to dqueue and blocks");
      _send(TNT_TASK__A, TNT_DQUEUE__1, TN_WAIT_INFINITE, true);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WSEND);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );
   }

#if TN_CHECK_PARAM
   TNT_TEST_COMMENT("B tries to read to NULL destination");
   TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__B, DQUEUE_RECEIVE, TNT_DQUEUE__1, NULL, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("Interrupt tries to read to NULL destination");
   TNT_ITEM__INT_SEND_CMD_DQUEUE(DQUEUE_IRECEIVE_POLLING, TNT_DQUEUE__1, NULL);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WRONG_PARAM);
         );
#endif


   {
      TNT_TEST_COMMENT("Interrupt tries to write polling to dqueue");
      _send(TNT_TASK__NONE, TNT_DQUEUE__1, 0/*not used*/, false);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_TIMEOUT);
            );

      TNT_TEST_COMMENT("C tries to write polling to dqueue");
      _send(TNT_TASK__C, TNT_DQUEUE__1, 0, false);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_TIMEOUT);
            );

      TNT_TEST_COMMENT("C tries to write to dqueue and blocks");
      _send(TNT_TASK__C, TNT_DQUEUE__1, TN_WAIT_INFINITE, true);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WSEND);
            TNT_CHECK__TASK(TNT_TASK__C, STATE, TSK_STATE_WAIT);

            TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );
   }

   _value_reset(&item_value);
   TNT_TEST_COMMENT("B reads polling from dqueue -> A unblocks");
   TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__B, DQUEUE_RECEIVE, TNT_DQUEUE__1, &item_value, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );
   _value_check_received(TNT_DQUEUE__1, item_value, true);

   _value_reset(&item_value);
   TNT_TEST_COMMENT("Interrupts reads polling from dqueue -> C unblocks");
   TNT_ITEM__INT_SEND_CMD_DQUEUE(DQUEUE_IRECEIVE_POLLING, TNT_DQUEUE__1, &item_value);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_NO_ERR);

         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
         );
   _value_check_received(TNT_DQUEUE__1, item_value, true);

   TNT_TEST_COMMENT("C reads from dqueue until it is empty");
   _receive_all(TNT_TASK__C, TNT_DQUEUE__1, TN_WAIT_INFINITE);


   _send_all(TNT_TASK__A, TNT_DQUEUE__1, 0);
   _receive_all(TNT_TASK__C, TNT_DQUEUE__1, 0);


#if TN_CHECK_PARAM
   TNT_TEST_COMMENT("A tries to read to NULL destination");
   TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_RECEIVE, TNT_DQUEUE__1, NULL, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("Interrupt tries to read to NULL destination");
   TNT_ITEM__INT_SEND_CMD_DQUEUE(DQUEUE_IRECEIVE_POLLING, TNT_DQUEUE__1, NULL);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WRONG_PARAM);
         );
#endif


   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__DQUEUE_MANAGE(DELETE, TNT_DQUEUE__1, /*the rest is not used*/ 0, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__C, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__C, 0);

}
// }}}

static void _dqueue_wcontext_test(void)   // {{{
{
   _srvalues_reset();
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("dqueue wrong context test");

   int priority_task_a = 5;

   int item_value = 0;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);

   TNT_TEST_COMMENT("create dqueue, should be ok");
   TNT_ITEM__DQUEUE_MANAGE(CREATE, TNT_DQUEUE__1, NULL, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, EXISTS,              1);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, ITEMS_CNT,           0);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, WRITTEN_ITEMS_CNT,   0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("Interrupt tries call task services");

   TNT_ITEM__INT_SEND_CMD_DQUEUE(DQUEUE_SEND, TNT_DQUEUE__1, &item_value);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__INT_SEND_CMD_DQUEUE(DQUEUE_SEND_POLLING, TNT_DQUEUE__1, &item_value);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__INT_SEND_CMD_DQUEUE(DQUEUE_RECEIVE, TNT_DQUEUE__1, &item_value);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__INT_SEND_CMD_DQUEUE(DQUEUE_RECEIVE_POLLING, TNT_DQUEUE__1, &item_value);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );



   TNT_TEST_COMMENT("Task tries call interrupt services");

   TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_ISEND_POLLING, TNT_DQUEUE__1, &item_value, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_IRECEIVE_POLLING, TNT_DQUEUE__1, &item_value, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WCONTEXT);
         );





   TNT_TEST_COMMENT("---- done, delete objects");
   TNT_ITEM__DQUEUE_MANAGE(DELETE, TNT_DQUEUE__1, NULL, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);

}
// }}}

static void _dqueue_multiple_test(void)   // {{{
{
   _srvalues_reset();
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("multiple dqueue test");

   int priority_task_a = 5;
   int priority_task_b = 4;
   int priority_task_c = 3;

   int item_value = 0;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__C, priority_task_c);

   TNT_TEST_COMMENT("create objects, should be ok");
   TNT_ITEM__DQUEUE_MANAGE(CREATE, TNT_DQUEUE__1, (void **)_dqueue_buf[ TNT_DQUEUE__1 ], _DQUEUE_SIZE);
   TNT_ITEM__DQUEUE_MANAGE(CREATE, TNT_DQUEUE__2, (void **)_dqueue_buf[ TNT_DQUEUE__2 ], _DQUEUE_SIZE);
   TNT_ITEM__EVENT_MANAGE(CREATE, TNT_EVENT__1, TNT_EVENT_NORMAL_ATTR, (0));
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__C, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__C, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, EXISTS,              1);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, ITEMS_CNT,           _DQUEUE_SIZE);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, WRITTEN_ITEMS_CNT,   0);

         TNT_CHECK__DQUEUE(TNT_DQUEUE__2, EXISTS,              1);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__2, ITEMS_CNT,           _DQUEUE_SIZE);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__2, WRITTEN_ITEMS_CNT,   0);

         TNT_CHECK__EVENT(TNT_EVENT__1, EXISTS,              1);
         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,             (0));

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("connecting E1 with flag 0 to DQ1");
   TNT_ITEM__SEND_CMD_DQUEUE_EVENT(TNT_TASK__A, DQUEUE_EVENTGRP_CONNECT, TNT_DQUEUE__1, TNT_EVENT__1, _QUEUE_FLAG__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, CONNECTED_EVENT, TNT_EVENT__1);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, CONNECTED_EVENT_PATTERN, _QUEUE_FLAG__1);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("connecting E1 with flag 1 to DQ2");
   TNT_ITEM__SEND_CMD_DQUEUE_EVENT(TNT_TASK__A, DQUEUE_EVENTGRP_CONNECT, TNT_DQUEUE__2, TNT_EVENT__1, _QUEUE_FLAG__2);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__DQUEUE(TNT_DQUEUE__2, CONNECTED_EVENT, TNT_EVENT__1);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__2, CONNECTED_EVENT_PATTERN, _QUEUE_FLAG__2);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   {
      _pattern_reset(&(_flags_pattern_a));
      TNT_TEST_COMMENT("A tries to wait messages from two queues and blocks");
      TNT_ITEM__SEND_CMD_EVENT(
            TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_QUEUE_FLAG__1 | _QUEUE_FLAG__2),
            TN_EVENTGRP_WMODE_OR, &_flags_pattern_a, TN_WAIT_INFINITE, 0
            );
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_EVENT);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );
      _pattern_check(&(_flags_pattern_a), _PATTERN_POISON);

      TNT_TEST_COMMENT("B writes to dqueue 1 -> A unblocks");
      _send(TNT_TASK__B, TNT_DQUEUE__1, TN_WAIT_INFINITE, true);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN, (_QUEUE_FLAG__1));
            TNT_CHECK__DQUEUE(TNT_DQUEUE__1, WRITTEN_ITEMS_CNT,   1);

            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
            );
      _pattern_check(&(_flags_pattern_a), (_QUEUE_FLAG__1));

      _value_reset(&item_value);
      TNT_TEST_COMMENT("A receives polling message from DQ1");
      TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_RECEIVE, TNT_DQUEUE__1, &item_value, 0);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN, (0));
            TNT_CHECK__DQUEUE(TNT_DQUEUE__1, WRITTEN_ITEMS_CNT, 0);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TN_RC_OK);
            );
      _value_check_received(TNT_DQUEUE__1, item_value, true);
   }

   {
      _pattern_reset(&(_flags_pattern_a));
      TNT_TEST_COMMENT("A tries to wait messages from two queues and blocks");
      TNT_ITEM__SEND_CMD_EVENT(
            TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_QUEUE_FLAG__1 | _QUEUE_FLAG__2),
            TN_EVENTGRP_WMODE_OR, &_flags_pattern_a, TN_WAIT_INFINITE, 0
            );
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_EVENT);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );
      _pattern_check(&(_flags_pattern_a), _PATTERN_POISON);

      TNT_TEST_COMMENT("Interrupt writes to dqueue 2 -> A unblocks");
      _send(TNT_TASK__NONE, TNT_DQUEUE__2, 0/*not used*/, true);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN, (_QUEUE_FLAG__2));
            TNT_CHECK__DQUEUE(TNT_DQUEUE__2, WRITTEN_ITEMS_CNT,   1);

            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
            );
      _pattern_check(&(_flags_pattern_a), (_QUEUE_FLAG__2));

      _value_reset(&item_value);
      TNT_TEST_COMMENT("A receives polling message from DQ2");
      TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_RECEIVE, TNT_DQUEUE__2, &item_value, 0);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN, (0));
            TNT_CHECK__DQUEUE(TNT_DQUEUE__2, WRITTEN_ITEMS_CNT, 0);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TN_RC_OK);
            );
      _value_check_received(TNT_DQUEUE__2, item_value, true);
   }

   {
      _pattern_reset(&(_flags_pattern_a));
      TNT_TEST_COMMENT("A tries to wait messages from two queues and blocks");
      TNT_ITEM__SEND_CMD_EVENT(
            TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_QUEUE_FLAG__1 | _QUEUE_FLAG__2),
            TN_EVENTGRP_WMODE_OR, &_flags_pattern_a, TN_WAIT_INFINITE, 0
            );
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_EVENT);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );
      _pattern_check(&(_flags_pattern_a), _PATTERN_POISON);

      TNT_TEST_COMMENT("B writes to dqueue 2 -> A unblocks");
      _send(TNT_TASK__B, TNT_DQUEUE__2, TN_WAIT_INFINITE, true);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN, (_QUEUE_FLAG__2));
            TNT_CHECK__DQUEUE(TNT_DQUEUE__2, WRITTEN_ITEMS_CNT,   1);

            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
            );
      _pattern_check(&(_flags_pattern_a), (_QUEUE_FLAG__2));

      TNT_TEST_COMMENT("B writes to dqueue 2 again");
      _send(TNT_TASK__B, TNT_DQUEUE__2, TN_WAIT_INFINITE, true);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__DQUEUE(TNT_DQUEUE__2, WRITTEN_ITEMS_CNT,   2);

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
            );

      TNT_TEST_COMMENT("B writes to dqueue 1");
      _send(TNT_TASK__B, TNT_DQUEUE__1, TN_WAIT_INFINITE, true);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN, (_QUEUE_FLAG__1 | _QUEUE_FLAG__2));
            TNT_CHECK__DQUEUE(TNT_DQUEUE__1, WRITTEN_ITEMS_CNT,   1);

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
            );

      _value_reset(&item_value);
      TNT_TEST_COMMENT("A receives polling message from DQ2");
      TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_RECEIVE, TNT_DQUEUE__2, &item_value, 0);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__DQUEUE(TNT_DQUEUE__2, WRITTEN_ITEMS_CNT, 1);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TN_RC_OK);
            );
      _value_check_received(TNT_DQUEUE__2, item_value, true);

      _pattern_reset(&(_flags_pattern_a));
      TNT_TEST_COMMENT("A tries to wait messages from two queues and doesn't block");
      TNT_ITEM__SEND_CMD_EVENT(
            TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_QUEUE_FLAG__1 | _QUEUE_FLAG__2),
            TN_EVENTGRP_WMODE_OR, &_flags_pattern_a, TN_WAIT_INFINITE, 0
            );
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
            );
      _pattern_check(&(_flags_pattern_a), (_QUEUE_FLAG__1 | _QUEUE_FLAG__2));

      _value_reset(&item_value);
      TNT_TEST_COMMENT("A receives polling message from DQ1");
      TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_RECEIVE, TNT_DQUEUE__1, &item_value, 0);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN, (_QUEUE_FLAG__2));
            TNT_CHECK__DQUEUE(TNT_DQUEUE__1, WRITTEN_ITEMS_CNT, 0);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TN_RC_OK);
            );
      _value_check_received(TNT_DQUEUE__1, item_value, true);

      _pattern_reset(&(_flags_pattern_a));
      TNT_TEST_COMMENT("A tries to wait messages from two queues and doesn't block");
      TNT_ITEM__SEND_CMD_EVENT(
            TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_QUEUE_FLAG__1 | _QUEUE_FLAG__2),
            TN_EVENTGRP_WMODE_OR, &_flags_pattern_a, TN_WAIT_INFINITE, 0
            );
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
            );
      _pattern_check(&(_flags_pattern_a), (_QUEUE_FLAG__2));

      _value_reset(&item_value);
      TNT_TEST_COMMENT("A receives polling message from DQ2");
      TNT_ITEM__SEND_CMD_DQUEUE(TNT_TASK__A, DQUEUE_RECEIVE, TNT_DQUEUE__2, &item_value, 0);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN, (0));
            TNT_CHECK__DQUEUE(TNT_DQUEUE__2, WRITTEN_ITEMS_CNT, 0);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TN_RC_OK);
            );
      _value_check_received(TNT_DQUEUE__2, item_value, true);

   }


   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__EVENT_MANAGE(DELETE, TNT_EVENT__1, /*the rest is not used*/ TNT_EVENT_NORMAL_ATTR, (0));
   TNT_ITEM__DQUEUE_MANAGE(DELETE, TNT_DQUEUE__1, /*the rest is not used*/ 0, 0);
   TNT_ITEM__DQUEUE_MANAGE(DELETE, TNT_DQUEUE__2, /*the rest is not used*/ 0, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__C, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__C, 0);

}
// }}}

static void _dqueue_multiple_wrong_par_test(void)   // {{{
{
   _srvalues_reset();
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("multiple dqueue test: wrong params");

   int priority_task_a = 5;


   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

#if TN_CHECK_PARAM
   TNT_TEST_COMMENT("create DQ1");
   TNT_ITEM__DQUEUE_MANAGE(CREATE, TNT_DQUEUE__1, (void **)_dqueue_buf[ TNT_DQUEUE__1 ], _DQUEUE_SIZE);

   TNT_TEST_COMMENT("connecting E1 with flag 0 to DQ1, but E1 isn't created: should fail");
   TNT_ITEM__SEND_CMD_DQUEUE_EVENT(TNT_TASK__A, DQUEUE_EVENTGRP_CONNECT, TNT_DQUEUE__1, TNT_EVENT__1, _QUEUE_FLAG__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, CONNECTED_EVENT, TNT_EVENT__NONE);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, CONNECTED_EVENT_PATTERN, (0));

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TN_RC_INVALID_OBJ);
         );

   TNT_TEST_COMMENT("delete DQ1, create E1");
   TNT_ITEM__DQUEUE_MANAGE(DELETE, TNT_DQUEUE__1, /*the rest is not used*/ 0, 0);
   TNT_ITEM__EVENT_MANAGE(CREATE, TNT_EVENT__1, TNT_EVENT_NORMAL_ATTR, (0));

   TNT_TEST_COMMENT("connecting E1 with flag 0 to DQ1, but DQ1 isn't created: should fail");
   TNT_ITEM__SEND_CMD_DQUEUE_EVENT(TNT_TASK__A, DQUEUE_EVENTGRP_CONNECT, TNT_DQUEUE__1, TNT_EVENT__1, _QUEUE_FLAG__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, CONNECTED_EVENT, TNT_EVENT__NONE);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, CONNECTED_EVENT_PATTERN, (0));

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TN_RC_INVALID_OBJ);
         );

   TNT_TEST_COMMENT("delete E1");
   TNT_ITEM__EVENT_MANAGE(DELETE, TNT_EVENT__1, /*the rest is not used*/ TNT_EVENT_NORMAL_ATTR, (0));
#endif


   TNT_TEST_COMMENT("create both DQ1 and E1");
   TNT_ITEM__DQUEUE_MANAGE(CREATE, TNT_DQUEUE__1, (void **)_dqueue_buf[ TNT_DQUEUE__1 ], _DQUEUE_SIZE);
   TNT_ITEM__EVENT_MANAGE(CREATE, TNT_EVENT__1, TNT_EVENT_NORMAL_ATTR, (0));

   TNT_TEST_COMMENT("connecting E1 with zero pattern to DQ1: should fail");
   TNT_ITEM__SEND_CMD_DQUEUE_EVENT(TNT_TASK__A, DQUEUE_EVENTGRP_CONNECT, TNT_DQUEUE__1, TNT_EVENT__1, (0));
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, CONNECTED_EVENT, TNT_EVENT__NONE);
         TNT_CHECK__DQUEUE(TNT_DQUEUE__1, CONNECTED_EVENT_PATTERN, (0));

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TN_RC_WPARAM);
         );



   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__EVENT_MANAGE(DELETE, TNT_EVENT__1, /*the rest is not used*/ TNT_EVENT_NORMAL_ATTR, (0));
   TNT_ITEM__DQUEUE_MANAGE(DELETE, TNT_DQUEUE__1, /*the rest is not used*/ 0, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);

}
// }}}



/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void appl_tntest_dqueue(void)
{
   _dqueue_no_buffer_test();
   _dqueue_deletion_test();
   _dqueue_test();
   _dqueue_wcontext_test();

   _dqueue_multiple_test();
   _dqueue_multiple_wrong_par_test();
}

/*******************************************************************************
 *    end of file
 ******************************************************************************/



