/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

/*
 * TODO: 
 *    - two timers with the same timeout, both trying to cancel each other:
 *      only one should fire
 *    - two timers with the same timeout, both trying to re-start each other
 *
 *
 */

#include "appl_tntest_timer.h"
#include "tntest.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

#define  _MAX_TIMER_FAILED_TIMEOUT     100
#define  _TIMER_INACTIVE               0xffffff00
#define  _NEXT_TIMEOUT_UNCHANGED       0xffffff01


#define  _ITERATIONS_CNT               500

#define  _MAX_RUNNING_TIMEOUT          1000
#define  _MAX_STOPPING_TIMEOUT         100

#define  _TIMEOUT_RESTART              5

#ifdef __XC16__
//-- TODO: if we don't remove that title, timer test is failed to execute.
//         Need to understand, why.
#  undef TNT_TEST_TITLE
#  define  TNT_TEST_TITLE(str, ...)     TNT_DTMSG_COMMENT("comment removed on PIC24/dsPIC"); tnt_file_line_echo(__LINE__, __FILE__)
#endif



/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

enum _TestFlag {
   _TIMER_TEST_RUNNING  = (1 << 0),
   _TIMER_TEST_STOPPING  = (1 << 1),
};

enum _TimerNum {
   _TIMER_NUM_0,
   _TIMER_NUM_1,
   _TIMER_NUM_2,
   _TIMER_NUM_3,
   _TIMER_NUM_4,
   _TIMER_NUM_5,
   _TIMER_NUM_6,
   _TIMER_NUM_7,
   _TIMER_NUM_8,
   _TIMER_NUM_9,
   _TIMER_NUM_10,
   _TIMER_NUM_11,
   _TIMER_NUM_12,
   _TIMER_NUM_13,
   _TIMER_NUM_14,
   _TIMER_NUM_15,

   _TIMERS_CNT
};

#define _TIMERS_ALL  (0                \
      | (1 << _TIMER_NUM_0)            \
      | (1 << _TIMER_NUM_1)            \
      | (1 << _TIMER_NUM_2)            \
      | (1 << _TIMER_NUM_3)            \
      | (1 << _TIMER_NUM_4)            \
      | (1 << _TIMER_NUM_5)            \
      | (1 << _TIMER_NUM_6)            \
      | (1 << _TIMER_NUM_7)            \
      | (1 << _TIMER_NUM_8)            \
      | (1 << _TIMER_NUM_9)            \
      | (1 << _TIMER_NUM_10)           \
      | (1 << _TIMER_NUM_11)           \
      | (1 << _TIMER_NUM_12)           \
      | (1 << _TIMER_NUM_13)           \
      | (1 << _TIMER_NUM_14)           \
      | (1 << _TIMER_NUM_15)           \
      )
   

struct _TimerFuncCtx {
   enum _TimerNum    timer_num;

   //-- NOTE: don't use TN_TickCnt here because it might be negative
   long              timeout_left;
   long              next_timeout;
};


/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

static struct _TimerFuncCtx _timer_ctx[ _TIMERS_CNT ];
static struct TN_EventGrp   _events;
static struct TN_EventGrp   _timer_fired_events;

static struct TN_Timer _timer[ _TIMERS_CNT ];
static int _iteration_num;
static int _used_timers_cnt;

static int _last_fire_tickcount[ _TIMERS_CNT ];

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

static void _set_timeout_from_next(enum _TimerNum timer_num)
{
   if (_timer_ctx[ timer_num ].next_timeout != _NEXT_TIMEOUT_UNCHANGED){
      _timer_ctx[ timer_num ].timeout_left = _timer_ctx[ timer_num ].next_timeout;
      _timer_ctx[ timer_num ].next_timeout = _NEXT_TIMEOUT_UNCHANGED;
   }
}

static TN_TickCnt _timeout_value_get(enum _TimerNum timer_num)
{
   return timer_num + 1;
}

static void _timer_start(enum _TimerNum timer_num, TN_TickCnt timeout)
{
   SYSRETVAL_DATA;
   _timer_ctx[ timer_num ].timer_num = timer_num;
   _timer_ctx[ timer_num ].next_timeout = timeout;
   if (tn_sys_context_get() == TN_CONTEXT_TASK){
      SYSRETVAL_CHECK(tn_timer_start(&_timer[ timer_num ], timeout));
   } else {
      SYSRETVAL_CHECK(tn_timer_start(&_timer[ timer_num ], timeout));
   }
}

static void _timer_cancel(enum _TimerNum timer_num)
{
   SYSRETVAL_DATA;
   _timer_ctx[ timer_num ].timer_num = timer_num;
   _timer_ctx[ timer_num ].next_timeout = _TIMER_INACTIVE;
   if (tn_sys_context_get() == TN_CONTEXT_TASK){
      SYSRETVAL_CHECK(tn_timer_cancel(&_timer[ timer_num ]));
   } else {
      SYSRETVAL_CHECK(tn_timer_cancel(&_timer[ timer_num ]));
   }
}

static void _timer_func__restart_itself(struct TN_Timer *timer, void *p_user_data)
{
   SYSRETVAL_DATA;
   struct _TimerFuncCtx *p_ctx = p_user_data;

   if (p_ctx->timer_num == _TIMER_NUM_0){
      _iteration_num++;
      if (_iteration_num >= _ITERATIONS_CNT){
         SYSRETVAL_CHECK(tn_eventgrp_imodify(&_events, TN_EVENTGRP_OP_CLEAR, _TIMER_TEST_RUNNING));
         SYSRETVAL_CHECK(tn_eventgrp_imodify(&_events, TN_EVENTGRP_OP_SET, _TIMER_TEST_STOPPING));
      }
   }

   SYSRETVAL_CHECK(tn_eventgrp_imodify(&_timer_fired_events, TN_EVENTGRP_OP_SET, (1 << p_ctx->timer_num)));


   if (
         SYSRETVAL_CHECK_TOUT(tn_eventgrp_iwait_polling(
               &_events, 
               _TIMER_TEST_STOPPING, 
               TN_EVENTGRP_WMODE_AND, 
               NULL
               ))
         !=
         TN_RC_OK
      )
   {
      //-- _TIMER_TEST_STOPPING flag isn't set, so, go on again.
      _timer_start(p_ctx->timer_num, _timeout_value_get(p_ctx->timer_num));
   }

}

static void _timer_func__cancel_other(struct TN_Timer *timer, void *p_user_data)
{
   SYSRETVAL_DATA;
   struct _TimerFuncCtx *p_ctx = p_user_data;
   p_ctx->next_timeout = _TIMER_INACTIVE;

   //-- get pointer to another timer
   enum _TimerNum timer_to_cancel_num = (timer == &_timer[ _TIMER_NUM_0 ])
      ? _TIMER_NUM_1
      : _TIMER_NUM_0;

   //-- cancel another timer
   _timer_cancel(timer_to_cancel_num);

   //-- set flag that timer has fired
   SYSRETVAL_CHECK(tn_eventgrp_imodify(&_timer_fired_events, TN_EVENTGRP_OP_SET, (1 << p_ctx->timer_num)));

}

static void _timer_func__restart_other(struct TN_Timer *timer, void *p_user_data)
{
   SYSRETVAL_DATA;
   struct _TimerFuncCtx *p_ctx = p_user_data;
   p_ctx->next_timeout = _TIMER_INACTIVE;

   //-- get pointer to another timer
   int timer_to_restart_num = (timer == &_timer[ _TIMER_NUM_0 ])
      ? _TIMER_NUM_1
      : _TIMER_NUM_0;

   if (timer_to_restart_num == _TIMER_NUM_1){
      //-- restart another timer
      _timer_start(timer_to_restart_num, _TIMEOUT_RESTART);

      //-- change func
      tn_timer_set_func(&_timer[ timer_to_restart_num ], _timer_func__restart_other, &_timer_ctx[ timer_to_restart_num ]);
   }

   //-- set flag that timer has fired
   SYSRETVAL_CHECK(tn_eventgrp_imodify(&_timer_fired_events, TN_EVENTGRP_OP_SET, (1 << p_ctx->timer_num)));

   _last_fire_tickcount[ p_ctx->timer_num ] = tn_sys_time_get();
}


static void _timers_cyclic_test(void)   // {{{
{
   SYSRETVAL_DATA;
   TNT_TEST_TITLE("timers cyclic test");

   TN_INTSAVE_DATA;

   enum _TimerNum timer_num;
   _used_timers_cnt = _TIMERS_CNT;

   //-- create objects: event group, timers
   SYSRETVAL_CHECK(tn_eventgrp_create(&_timer_fired_events, (0)));

   //-- create timers with wrong function,
   //   it will be modified later by tn_timer_set_func()
   //   just in order to test `tn_timer_set_func()`.
   for (timer_num = 0; timer_num < _used_timers_cnt; timer_num++){
      //-- create
      SYSRETVAL_CHECK(tn_timer_create(&_timer[ timer_num ], _timer_func__cancel_other, &_timer_ctx[ timer_num ]));

      //-- modify
      SYSRETVAL_CHECK(tn_timer_set_func(&_timer[ timer_num ], _timer_func__restart_itself, &_timer_ctx[ timer_num ]));
   }

   TNT_TEST_COMMENT("starting test..");
   TN_INT_DIS_SAVE();
   {

      //-- set flag that timer test is in progress
      SYSRETVAL_CHECK(tn_eventgrp_modify(&_events, TN_EVENTGRP_OP_SET, _TIMER_TEST_RUNNING));
      SYSRETVAL_CHECK(tn_eventgrp_modify(&_events, TN_EVENTGRP_OP_CLEAR, _TIMER_TEST_STOPPING));

      _iteration_num = 0;

      //-- schedule timers
      for (timer_num = 0; timer_num < _used_timers_cnt; timer_num++){
         _timer_start(timer_num, _timeout_value_get(timer_num));
         _set_timeout_from_next(timer_num);
      }

      //-- just for additional test, re-start timer 5
      _timer_start(5, _timeout_value_get(5));
      _set_timeout_from_next(5);

   }

   TN_INT_RESTORE();

   TNT_TEST_COMMENT("waiting for test to be done..");

   if (
         SYSRETVAL_CHECK_TOUT(tn_eventgrp_wait(
               &_events, 
               _TIMER_TEST_STOPPING, 
               TN_EVENTGRP_WMODE_AND, 
               NULL,
               _MAX_RUNNING_TIMEOUT
               ))
         !=
         TN_RC_OK
      )
   {
      TNT_DEB_HALT("failed wait for STOPPING flag to be set");
   }

   TNT_TEST_COMMENT("waiting all the timers to fire last time..");

   if (
         SYSRETVAL_CHECK_TOUT(tn_eventgrp_wait(
               &_timer_fired_events, 
               _TIMERS_ALL, 
               TN_EVENTGRP_WMODE_AND, 
               NULL,
               _MAX_STOPPING_TIMEOUT
               ))
         !=
         TN_RC_OK
      )
   {
      TNT_DEB_HALT("failed wait for all timers to stop");
   }

   SYSRETVAL_CHECK(tn_eventgrp_modify(&_events, TN_EVENTGRP_OP_CLEAR, _TIMER_TEST_STOPPING));
   SYSRETVAL_CHECK(tn_eventgrp_modify(&_timer_fired_events, TN_EVENTGRP_OP_CLEAR, _TIMERS_ALL));

   TNT_TEST_COMMENT("done, deleting objects.");

   //-- delete objects
   SYSRETVAL_CHECK(tn_eventgrp_delete(&_timer_fired_events));

   //-- delete timers
   for (timer_num = 0; timer_num < _used_timers_cnt; timer_num++){
      SYSRETVAL_CHECK(tn_timer_delete(&_timer[ timer_num ]));
   }

}
// }}}

static void _timers_cancel_same_timeout_test(int timeout)   // {{{
{
   SYSRETVAL_DATA;
   TNT_TEST_TITLE("timers test: cancel another timer with the same timeout (timeout=%d)", timeout);

   TN_INTSAVE_DATA;

   enum _TimerNum timer_num;
   _used_timers_cnt = 2;

   //-- create objects: event group, timers
   SYSRETVAL_CHECK(tn_eventgrp_create(&_timer_fired_events, (0)));

   //-- create timers
   for (timer_num = 0; timer_num < _used_timers_cnt; timer_num++){
      SYSRETVAL_CHECK(tn_timer_create(&_timer[ timer_num ], _timer_func__cancel_other, &_timer_ctx[ timer_num ]));
   }

   TN_INT_DIS_SAVE();
   {

      //-- set flag that timer test is in progress
      SYSRETVAL_CHECK(tn_eventgrp_modify(&_events, TN_EVENTGRP_OP_SET, _TIMER_TEST_RUNNING));

      //-- schedule timers
      for (timer_num = 0; timer_num < _used_timers_cnt; timer_num++){
         _timer_start(timer_num, timeout);
         _set_timeout_from_next(timer_num);
      }

   }

   TN_INT_RESTORE();

   if (
         SYSRETVAL_CHECK_TOUT(tn_eventgrp_wait(
               &_timer_fired_events, 
               ((1 << _TIMER_NUM_0) | (1 << _TIMER_NUM_1)), 
               TN_EVENTGRP_WMODE_OR, 
               NULL,
               _MAX_RUNNING_TIMEOUT
               ))
         !=
         TN_RC_OK
      )
   {
      TNT_DEB_HALT("failed wait for at least one timer to fire");
   }

   //-- just to be safe, sleep the timeout before checking flags of fired timers
   SYSRETVAL_CHECK_TOUT(tn_task_sleep(timeout));

   if (
         SYSRETVAL_CHECK_TOUT(tn_eventgrp_wait(
               &_timer_fired_events, 
               ((1 << _TIMER_NUM_0) | (1 << _TIMER_NUM_1)), 
               TN_EVENTGRP_WMODE_AND, 
               NULL,
               0
               ))
         ==
         TN_RC_OK
      )
   {
      TNT_DEB_HALT("only one timer should fire, but all of them has fired");
   }


   SYSRETVAL_CHECK(tn_eventgrp_modify(&_events, TN_EVENTGRP_OP_CLEAR, _TIMER_TEST_RUNNING));
   SYSRETVAL_CHECK(tn_eventgrp_modify(&_timer_fired_events, TN_EVENTGRP_OP_CLEAR, _TIMERS_ALL));

   //-- delete objects
   SYSRETVAL_CHECK(tn_eventgrp_delete(&_timer_fired_events));

   //-- delete timers
   for (timer_num = 0; timer_num < _used_timers_cnt; timer_num++){
      SYSRETVAL_CHECK(tn_timer_delete(&_timer[ timer_num ]));
   }

}
// }}}

static void _timers_restart_same_timeout_test(int timeout, int delta_timeout)   // {{{
{
   SYSRETVAL_DATA;
   TNT_TEST_TITLE(
         "timers test: restart another timer from timer function, timeout=%d, delta_timeout=%d",
         timeout,
         delta_timeout
         );

   TN_INTSAVE_DATA;

   enum _TimerNum timer_num;
   _used_timers_cnt = 2;

   //-- create objects: event group, timers
   SYSRETVAL_CHECK(tn_eventgrp_create(&_timer_fired_events, (0)));

   //-- create timers
   SYSRETVAL_CHECK(tn_timer_create(&_timer[ _TIMER_NUM_0 ], _timer_func__restart_other, &_timer_ctx[ _TIMER_NUM_0 ]));
   //-- set wrong function, it will be changed by the first timer
   SYSRETVAL_CHECK(tn_timer_create(&_timer[ _TIMER_NUM_1 ], _timer_func__restart_itself, &_timer_ctx[ _TIMER_NUM_1 ]));


   TN_INT_DIS_SAVE();
   {

      //-- set flag that timer test is in progress
      SYSRETVAL_CHECK(tn_eventgrp_modify(&_events, TN_EVENTGRP_OP_SET, _TIMER_TEST_RUNNING));

      _timer_start(_TIMER_NUM_0, timeout);
      _set_timeout_from_next(_TIMER_NUM_0);
      _timer_start(_TIMER_NUM_1, timeout + delta_timeout);
      _set_timeout_from_next(_TIMER_NUM_1);

   }

   TN_INT_RESTORE();

   if (
         SYSRETVAL_CHECK_TOUT(tn_eventgrp_wait(
               &_timer_fired_events, 
               ((1 << _TIMER_NUM_0)), 
               TN_EVENTGRP_WMODE_AND, 
               NULL,
               _MAX_RUNNING_TIMEOUT
               ))
         !=
         TN_RC_OK
      )
   {
      TNT_DEB_HALT("failed wait for timer #0 to fire");
   }

   if (
         SYSRETVAL_CHECK_TOUT(tn_eventgrp_wait(
               &_timer_fired_events, 
               ((1 << _TIMER_NUM_1)), 
               TN_EVENTGRP_WMODE_AND, 
               NULL,
               _MAX_RUNNING_TIMEOUT
               ))
         !=
         TN_RC_OK
      )
   {
      TNT_DEB_HALT("failed wait for timer #1 to fire");
   }

   {
      unsigned int diff = _last_fire_tickcount[ _TIMER_NUM_1 ] - _last_fire_tickcount[ _TIMER_NUM_0 ];
      if (diff != _TIMEOUT_RESTART){
         TNT_DEB_HALT("second timer fired with wrong timeout: delta is %d, but expected %d",
               diff, _TIMEOUT_RESTART
               );
      }
   }


   SYSRETVAL_CHECK(tn_eventgrp_modify(&_events, TN_EVENTGRP_OP_CLEAR, _TIMER_TEST_RUNNING));
   SYSRETVAL_CHECK(tn_eventgrp_modify(&_timer_fired_events, TN_EVENTGRP_OP_CLEAR, _TIMERS_ALL));

   //-- delete objects
   SYSRETVAL_CHECK(tn_eventgrp_delete(&_timer_fired_events));

   //-- delete timers
   for (timer_num = 0; timer_num < _used_timers_cnt; timer_num++){
      SYSRETVAL_CHECK(tn_timer_delete(&_timer[ timer_num ]));
   }

}
// }}}


/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void appl_tntest_timer(void)
{
   //-- init
   {
      int i;
      for (i = 0; i < _TIMERS_CNT; i++){
         _timer_ctx[i].next_timeout = _NEXT_TIMEOUT_UNCHANGED;
      }
   }

   _timers_cyclic_test();

   {
      int timeout;
      for (timeout = 1; timeout <= (TN_TICK_LISTS_CNT * 2) + 1; timeout++){
         _timers_cancel_same_timeout_test(timeout);
      }
   }

   {
      int timeout, delta;
      for (timeout = 1; timeout <= (TN_TICK_LISTS_CNT * 2) + 1; timeout++){
         for (delta = 0; delta <= (_TIMEOUT_RESTART * 2); delta++){
            _timers_restart_same_timeout_test(timeout, delta);
         }
      }
   }

   
}

void appl_tntest_timer__init(void)
{
   SYSRETVAL_DATA;
   SYSRETVAL_CHECK(tn_eventgrp_create(&_events, (0)));
}

void appl_tntest_timer__sys_int_check(void)
{
   SYSRETVAL_DATA;
   if (
         SYSRETVAL_CHECK_TOUT(tn_eventgrp_iwait_polling(
               &_events, 
               _TIMER_TEST_RUNNING, 
               TN_EVENTGRP_WMODE_AND, 
               NULL
               ))
         ==
         TN_RC_OK
      )
   {
      enum _TimerNum timer_num;

      //-- check timers
      for (timer_num = 0; timer_num < _used_timers_cnt; timer_num++){

         if (_timer_ctx[ timer_num ].timeout_left != _TIMER_INACTIVE){
            if (_timer_ctx[ timer_num ].timeout_left > -_MAX_TIMER_FAILED_TIMEOUT){
               _timer_ctx[ timer_num ].timeout_left--;

               if (
                     SYSRETVAL_CHECK_TOUT(tn_eventgrp_iwait_polling(
                           &_timer_fired_events, 
                           (1 << timer_num), 
                           TN_EVENTGRP_WMODE_AND, 
                           NULL
                           ))
                     ==
                     TN_RC_OK
                  )
               {
                  //-- timer just fired

                  //-- clear that event
                  SYSRETVAL_CHECK(tn_eventgrp_imodify(
                           &_timer_fired_events,
                           TN_EVENTGRP_OP_CLEAR,
                           (1 << timer_num)
                           ));

                  if (_timer_ctx[ timer_num ].timeout_left != 0){
                     //-- timer fired at wrong moment
                     TNT_DEB_HALT("timer #%d fired at wrong moment: timeout_left=%ld",
                           timer_num,
                           _timer_ctx[ timer_num ].timeout_left
                           );
                  } else {
                     //-- timer fired at right moment. Nothing to do now.

                  }
               }


            } else {
               TNT_DEB_HALT("timer #%d haven't fired (waited for %ld extra ticks)",
                     timer_num,
                     (-_timer_ctx[ timer_num ].timeout_left)
                     );
            }
         }

         //-- if new timeout was set for the timer, get it
         _set_timeout_from_next(timer_num);

         if (_timer_ctx[ timer_num ].timeout_left != _TIMER_INACTIVE){
            //-- test tn_timer_time_left() {{{
            TN_TickCnt timeout_left = 0;
            SYSRETVAL_CHECK(tn_timer_time_left(&_timer[timer_num], &timeout_left));

            if (timeout_left != _timer_ctx[ timer_num ].timeout_left){
               TNT_DEB_HALT("timer #%d: error in tn_timer_time_left(): returned value is %d, expected %d",
                     timer_num,
                     timeout_left,
                     _timer_ctx[ timer_num ].timeout_left
                     );
            }

            // }}}
         }

         //-- test tn_timer_is_active() {{{
         {
            TN_BOOL is_active = 0;
            SYSRETVAL_CHECK(tn_timer_is_active(&_timer[timer_num], &is_active));

            if (is_active != (_timer_ctx[ timer_num ].timeout_left != _TIMER_INACTIVE)){
               TNT_DEB_HALT("timer #%d: error in tn_timer_is_active(): returned value is %d, expected %d",
                     timer_num,
                     is_active,
                     (_timer_ctx[ timer_num ].timeout_left != _TIMER_INACTIVE)
                     );
            }
         }
         // }}}

      }
   }

}

/*******************************************************************************
 *    end of file
 ******************************************************************************/



