/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "appl_tntest_sys.h"
#include "tntest.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/




/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

enum _TestFlag {
   _A_WAS_RUNNING = (1 << 0),
   _B_WAS_RUNNING = (1 << 1),
};



/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/



/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

static void _tslice_test(void)   // {{{
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("time slice test");

   int priority_task_a = 5;
   int priority_task_b = 5;
   int priority_task_c = 4;

   int tslice          = 5;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__C, priority_task_c);

   TNT_TEST_COMMENT("create event, should be ok");
   TNT_ITEM__EVENT_MANAGE(CREATE, TNT_EVENT__1, TNT_EVENT_NORMAL_ATTR, (0));
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__C, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__C, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__EVENT(TNT_EVENT__1, EXISTS,    1);
         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (0));

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("C sets time slice for priority of A and B to %d..", tslice);
   TNT_ITEM__SEND_CMD_SYS(TNT_TASK__C, SYS_TSLICE_SET, 
         priority_task_a, tslice, 0/*not used*/
         );
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TN_RC_OK);
         );

   TNT_TEST_COMMENT("A becomes always runnable");
   TNT_ITEM__SEND_CMD_EXMODE(TNT_TASK__A, POLLING);
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TN_RC_OK);
         );

   TNT_TEST_COMMENT("B becomes always runnable");
   TNT_ITEM__SEND_CMD_EXMODE(TNT_TASK__B, POLLING);
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__B, STATE, TSK_STATE_RUNNABLE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TN_RC_OK);
         );

   TNT_TEST_COMMENT("A should set flag that it was running");
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_MODIFY, TNT_EVENT__1, (_A_WAS_RUNNING),
         /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
         );

   TNT_TEST_COMMENT("B should set flag that it was running");
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__B, EVENT_MODIFY, TNT_EVENT__1, (_B_WAS_RUNNING),
         /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
         );

   TNT_TEST_COMMENT("Let A and B do their job..");
   TNT_ITEM__WAIT(TNT_TIMEOUT_AFTER_CMD);

   TNT_TEST_COMMENT("C checks flags: should be both set");
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__C, EVENT_WAIT_POLLING, TNT_EVENT__1, (_A_WAS_RUNNING | _B_WAS_RUNNING),
         TN_EVENTGRP_WMODE_AND, NULL, TN_WAIT_INFINITE/*not used*/, 0
         );
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TN_RC_OK);
         );

   TNT_TEST_COMMENT("C clears these flags");
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__C, EVENT_MODIFY, TNT_EVENT__1, (_A_WAS_RUNNING | _B_WAS_RUNNING),
         /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_CLEAR
         );
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TN_RC_OK);
         );

   TNT_TEST_COMMENT("C sets time slice for priority of A and B to TN_NO_TIME_SLICE");
   TNT_ITEM__SEND_CMD_SYS(TNT_TASK__C, SYS_TSLICE_SET, 
         priority_task_a, TN_NO_TIME_SLICE, 0/*not used*/
         );
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TN_RC_OK);
         );


   TNT_TEST_COMMENT("A should set flag that it was running");
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_MODIFY, TNT_EVENT__1, (_A_WAS_RUNNING),
         /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
         );

   TNT_TEST_COMMENT("B should set flag that it was running");
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__B, EVENT_MODIFY, TNT_EVENT__1, (_B_WAS_RUNNING),
         /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
         );

   TNT_TEST_COMMENT("Let A and B do their job..");
   TNT_ITEM__WAIT(TNT_TIMEOUT_AFTER_CMD);

   TNT_TEST_COMMENT("C checks flags: should be set just one of them");
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__C, EVENT_WAIT_POLLING, TNT_EVENT__1, (_A_WAS_RUNNING | _B_WAS_RUNNING),
         TN_EVENTGRP_WMODE_AND, NULL, TN_WAIT_INFINITE/*not used*/, 0
         );
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TN_RC_TIMEOUT);
         );

   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__C, EVENT_WAIT_POLLING, TNT_EVENT__1, (_A_WAS_RUNNING | _B_WAS_RUNNING),
         TN_EVENTGRP_WMODE_OR, NULL, TN_WAIT_INFINITE/*not used*/, 0
         );
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TN_RC_OK);
         );

   TNT_TEST_COMMENT("C clears these flags");
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__C, EVENT_MODIFY, TNT_EVENT__1, (_A_WAS_RUNNING | _B_WAS_RUNNING),
         /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_CLEAR
         );
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TN_RC_OK);
         );


   TNT_TEST_COMMENT("Make tasks A and B waiting again, in order to make them finish their job");

   TNT_ITEM__SEND_CMD_EXMODE(TNT_TASK__A, WAIT);
   TNT_ITEM__SEND_CMD_EXMODE(TNT_TASK__B, WAIT);

   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TN_RC_OK);

         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, STATE, TSK_STATE_WAIT);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TN_RC_OK);
         );




   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__EVENT_MANAGE(DELETE, TNT_EVENT__1, /*the rest is not used*/ TNT_EVENT_NORMAL_ATTR, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__C, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__C, 0);

}
// }}}

static void _tslice_wrong_param_test(void)   // {{{
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("time slice wrong param test");

   int priority_task_c = 4;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__C, priority_task_c);

   TNT_TEST_COMMENT("create event, should be ok");
   TNT_ITEM__EVENT_MANAGE(CREATE, TNT_EVENT__1, TNT_EVENT_NORMAL_ATTR, (0));
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__C, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__C, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__EVENT(TNT_EVENT__1, EXISTS,    1);
         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (0));

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("C tries to set time slice for priority 5 to -1");
   TNT_ITEM__SEND_CMD_SYS(TNT_TASK__C, SYS_TSLICE_SET, 
         5, -1, 0/*not used*/
         );
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TN_RC_WPARAM);
         );

   TNT_TEST_COMMENT("C tries to set time slice for priority 5 to (TN_MAX_TIME_SLICE + 1)");
   TNT_ITEM__SEND_CMD_SYS(TNT_TASK__C, SYS_TSLICE_SET, 
         5, (TN_MAX_TIME_SLICE + 1), 0/*not used*/
         );
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TN_RC_WPARAM);
         );

   TNT_TEST_COMMENT("C tries to set time slice for priority 0 to 5: should succeed");
   TNT_ITEM__SEND_CMD_SYS(TNT_TASK__C, SYS_TSLICE_SET, 
         0, 5, 0/*not used*/
         );
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TN_RC_OK);
         );

   TNT_TEST_COMMENT("C tries to set time slice for priority 0 to TN_NO_TIME_SLICE: should succeed");
   TNT_ITEM__SEND_CMD_SYS(TNT_TASK__C, SYS_TSLICE_SET, 
         0, TN_NO_TIME_SLICE, 0/*not used*/
         );
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TN_RC_OK);
         );

   TNT_TEST_COMMENT("C tries to set time slice for priority (TN_PRIORITIES_CNT - 1) to 5");
   TNT_ITEM__SEND_CMD_SYS(TNT_TASK__C, SYS_TSLICE_SET, 
         (TN_PRIORITIES_CNT - 1), 5, 0/*not used*/
         );
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TN_RC_WPARAM);
         );

   TNT_TEST_COMMENT("Interrupt tries to set time slice for priority 3 to 5");
   TNT_ITEM__INT_SEND_CMD_SYS(SYS_TSLICE_SET, 
         3, 5, 0/*not used*/
         );
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TN_RC_WCONTEXT);
         );


   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__EVENT_MANAGE(DELETE, TNT_EVENT__1, /*the rest is not used*/ TNT_EVENT_NORMAL_ATTR, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__C, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__C, 0);

}
// }}}


static void _sched_dis_test(void)   // {{{
{

   const int to = 10;
   TN_TickCnt time_before, time_after, time_after_en;

   TNT_TEST_COMMENT("sched disable test");

   //-- disable scheduler
   TN_UWord sched_stat = tn_sched_dis_save();

   //-- remember starting time and try to sleep N ticks
   time_before = tn_sys_time_get();
   tn_task_sleep(to);

   //-- since scheduler is disabled, we shouldn't actually sleep, so, 
   //   time should not change. Remember current time.
   time_after = tn_sys_time_get();

   //-- now, enable scheduler. Since context switch should be pended,
   //   current task should go to sleep immediately
   tn_sched_restore(sched_stat);

   //-- and here, time should differ by N ticks
   time_after_en = tn_sys_time_get();


   TNT_TEST_COMMENT("time_before=%d, time_after=%d, time_after_en=%d",
         time_before, time_after, time_after_en
         );
   
   //-- check all of this
   if (time_after > (time_before + 1)){
      TNT_DEB_HALT("error: time after too large");
   } else if (time_after_en < (time_before + to) || time_after_en > (time_before + to + 1)){
      TNT_DEB_HALT("error: time_after_en is wrong");
   } else {
      TNT_TEST_COMMENT("OK");
   }


}
// }}}


   /*******************************************************************************
    *    PUBLIC FUNCTIONS
    ******************************************************************************/

void appl_tntest_sys(void)
{
#if !TN_DYNAMIC_TICK
   _tslice_test();
   _tslice_wrong_param_test();
#endif
   _sched_dis_test();
}

/*******************************************************************************
 *    end of file
 ******************************************************************************/



