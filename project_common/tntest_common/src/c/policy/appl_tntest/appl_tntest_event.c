/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "appl_tntest_event.h"
#include "tntest.h"
#include "dterm.h"
#include "appl_cfg__dbg__deb_halt.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

#define  _TIMEOUT_FOR_WAIT             (500)
#define  _PATTERN_POISON               (0x5a5a)


#define  _FLAGS_PT(v)   (bool_flags_pattern ? (&(v)) : NULL)

#ifdef __XC16__
//-- XC16 seems buggy: if there is too much data, all the data gets mixed up.
//   so, if we remove some of the comments, it works again some of the comments, it works again.
#  undef TNT_TEST_COMMENT
#  define  TNT_TEST_COMMENT(str, ...)     TNT_DTMSG_COMMENT("comment removed on PIC24/dsPIC"); tnt_file_line_echo(__LINE__, __FILE__)
#endif

//TODO: remove
#define  _X96_HACKS  0


/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

enum _TestFlag {
   _TEST_FLAG__F1 = (1 << 0),
   _TEST_FLAG__F2 = (1 << 1),
   _TEST_FLAG__F3 = (1 << 2),
};



/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

static unsigned int _flags_pattern_a = 0;
static unsigned int _flags_pattern_b = 0;
static unsigned int _flags_pattern_c = 0;
static unsigned int _flags_pattern_d = 0;
static unsigned int _flags_pattern_int = 0;




/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

#if 0
static void _event_test(void)   // {{{
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("event test");

   int priority_task_a = 5;
   int priority_task_b = 4;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);

   TNT_TEST_COMMENT("create event, should be ok");
   TNT_ITEM__EVENT_MANAGE(CREATE, TNT_EVENT__1, TNT_EVENT_NORMAL_ATTR, (0));
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__EVENT(TNT_EVENT__1, EXISTS,    1);
         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (0));

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );


   //-- tests here


   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__EVENT_MANAGE(DELETE, TNT_EVENT__1, /*the rest is not used*/ TNT_EVENT_NORMAL_ATTR, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);

}
// }}}
#endif

static void _pattern_reset(unsigned int *p_pattern)
{
   if (p_pattern != NULL){
      *p_pattern = _PATTERN_POISON;
   }
}

static void _pattern_check(unsigned int *p_pattern, unsigned int value)
{
   if (p_pattern != NULL){
      if (*p_pattern == value){
         TNT_DTMSG_BARE("returned pattern is correct: 0x%x", *p_pattern);
      } else {
         TNT_DEB_HALT("p_pattern should be 0x%x, but it is 0x%x",
               value, *p_pattern
               );
      }
   }
}

static void _eventgrp_wrong_params_test(void)   // {{{
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("event test wrong params");

   int priority_task_a = 5;
   int priority_task_b = 4;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);

   TNT_TEST_COMMENT("create event, should be ok");
   TNT_ITEM__EVENT_MANAGE(CREATE, TNT_EVENT__1, TNT_EVENT_NORMAL_ATTR, (3));
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__EVENT(TNT_EVENT__1, EXISTS,    1);
         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (3));

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );


#if TN_CHECK_PARAM
   bool bool_flags_pattern = true;

   TNT_TEST_COMMENT("A tries to wait for 0 pattern: should fail");
   _pattern_reset(_FLAGS_PT(_flags_pattern_a));
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (0),
         TN_EVENTGRP_WMODE_AND, _FLAGS_PT(_flags_pattern_a), TN_WAIT_INFINITE, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WRONG_PARAM);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_a), _PATTERN_POISON);

   TNT_TEST_COMMENT("A tries to wait OR and AND: should fail");
   _pattern_reset(_FLAGS_PT(_flags_pattern_a));
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F1),
         (TN_EVENTGRP_WMODE_AND | TN_EVENTGRP_WMODE_OR), _FLAGS_PT(_flags_pattern_a), TN_WAIT_INFINITE, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WRONG_PARAM);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_a), _PATTERN_POISON);

   TNT_TEST_COMMENT("A tries to wait without mode specified: should fail");
   _pattern_reset(_FLAGS_PT(_flags_pattern_a));
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F1),
         (0), _FLAGS_PT(_flags_pattern_a), TN_WAIT_INFINITE, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WRONG_PARAM);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_a), _PATTERN_POISON);

   TNT_TEST_COMMENT("A tries to wait polling for 0 pattern: should fail");
   _pattern_reset(_FLAGS_PT(_flags_pattern_a));
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_WAIT_POLLING, TNT_EVENT__1, (0),
         TN_EVENTGRP_WMODE_AND, _FLAGS_PT(_flags_pattern_a), TN_WAIT_INFINITE, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WRONG_PARAM);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_a), _PATTERN_POISON);

   TNT_TEST_COMMENT("Interrupt tries to wait polling for 0 pattern: should fail");
   _pattern_reset(_FLAGS_PT(_flags_pattern_int));
   TNT_ITEM__INT_SEND_CMD_EVENT(
         EVENT_IWAIT, TNT_EVENT__1, (0),
         TN_EVENTGRP_WMODE_AND, _FLAGS_PT(_flags_pattern_int), 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WRONG_PARAM);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_int), _PATTERN_POISON);
#endif




   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__EVENT_MANAGE(DELETE, TNT_EVENT__1, /*the rest is not used*/ TNT_EVENT_NORMAL_ATTR, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__EVENT(TNT_EVENT__1, EXISTS,    0);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

#if TN_CHECK_PARAM
   TNT_TEST_COMMENT("A tries to wait for F1 in deleted eventgrp: should fail");
   _pattern_reset(_FLAGS_PT(_flags_pattern_a));
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F1),
         TN_EVENTGRP_WMODE_AND, _FLAGS_PT(_flags_pattern_a), TN_WAIT_INFINITE, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TN_RC_INVALID_OBJ);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_a), _PATTERN_POISON);

   TNT_TEST_COMMENT("Interrupt tries to wait polling for F1 in deleted eventgrp: should fail");
   _pattern_reset(_FLAGS_PT(_flags_pattern_int));
   TNT_ITEM__INT_SEND_CMD_EVENT(
         EVENT_IWAIT, TNT_EVENT__1, (_TEST_FLAG__F1),
         TN_EVENTGRP_WMODE_AND, _FLAGS_PT(_flags_pattern_int), 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TN_RC_INVALID_OBJ);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_int), _PATTERN_POISON);
#endif

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);

}
// }}}


static void _eventgrp_test(bool bool_interrupt, bool bool_flags_pattern)   // {{{
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("event test multi");

   int priority_task_a = 7;
   int priority_task_b = 6;
   int priority_task_c = 5;
   int priority_task_d = 4;
   int priority_task_e = 3;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__C, priority_task_c);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__D, priority_task_d);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__E, priority_task_e);

   TNT_TEST_COMMENT("create event, should be ok");
   TNT_ITEM__EVENT_MANAGE(CREATE, TNT_EVENT__1, TNT_EVENT_NORMAL_ATTR, (0));
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__C, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__C, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__D, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__D, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__D, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__D, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__E, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__E, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__E, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__EVENT(TNT_EVENT__1, EXISTS,    1);
         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (0));

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("A waits for flags F1 and F2");
   _pattern_reset(_FLAGS_PT(_flags_pattern_a));
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
         TN_EVENTGRP_WMODE_AND, _FLAGS_PT(_flags_pattern_a), TN_WAIT_INFINITE, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_EVENT);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_a), _PATTERN_POISON);


   TNT_TEST_COMMENT("B waits for flags F1 or F2");
   _pattern_reset(_FLAGS_PT(_flags_pattern_b));
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__B, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
         TN_EVENTGRP_WMODE_OR, _FLAGS_PT(_flags_pattern_b), TN_WAIT_INFINITE, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_EVENT);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_b), _PATTERN_POISON);

   TNT_TEST_COMMENT("C waits for flags F2 and F3");
   _pattern_reset(_FLAGS_PT(_flags_pattern_c));
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__C, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F2 | _TEST_FLAG__F3),
         TN_EVENTGRP_WMODE_AND, _FLAGS_PT(_flags_pattern_c), TN_WAIT_INFINITE, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_EVENT);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_c), _PATTERN_POISON);

   TNT_TEST_COMMENT("D waits for flags F2 or F3");
   _pattern_reset(_FLAGS_PT(_flags_pattern_d));
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__D, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F2 | _TEST_FLAG__F3),
         TN_EVENTGRP_WMODE_OR, _FLAGS_PT(_flags_pattern_d), TN_WAIT_INFINITE, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__D, WAIT_REASON, TSK_WAIT_REASON_EVENT);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_d), _PATTERN_POISON);

   TNT_TEST_COMMENT("E sets F2 -> B and D wake up");
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__E, EVENT_MODIFY, TNT_EVENT__1, (_TEST_FLAG__F2),
         /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         TNT_CHECK__TASK(TNT_TASK__D, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__D, LAST_RETVAL, TERR_NO_ERR);

         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F2));

         TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_b), _TEST_FLAG__F2);
   _pattern_check(_FLAGS_PT(_flags_pattern_d), _TEST_FLAG__F2);

   if (!bool_interrupt){
      TNT_TEST_COMMENT("E clears F2");
      TNT_ITEM__SEND_CMD_EVENT(
            TNT_TASK__E, EVENT_MODIFY, TNT_EVENT__1, (_TEST_FLAG__F2),
            /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_CLEAR
            );
   } else {
      TNT_TEST_COMMENT("Interrupt clears F2");
      TNT_ITEM__INT_SEND_CMD_EVENT(
            EVENT_IMODIFY, TNT_EVENT__1, (_TEST_FLAG__F2),
            /* the rest is not used */ 0, 0, TN_EVENTGRP_OP_CLEAR
            );
   }
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (0));

         if (!bool_interrupt){
            TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
         } else {
            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
         }
         );

   if (!bool_interrupt){
      TNT_TEST_COMMENT("E sets F1");
      TNT_ITEM__SEND_CMD_EVENT(
            TNT_TASK__E, EVENT_MODIFY, TNT_EVENT__1, (_TEST_FLAG__F1),
            /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
            );
   } else {
      TNT_TEST_COMMENT("Interrupt sets F1");
      TNT_ITEM__INT_SEND_CMD_EVENT(
            EVENT_IMODIFY, TNT_EVENT__1, (_TEST_FLAG__F1),
            /* the rest is not used */ 0, 0, TN_EVENTGRP_OP_SET
            );
   }
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F1));

         if (!bool_interrupt){
            TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
         } else {
            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
         }
         );

   if (!bool_interrupt){
      TNT_TEST_COMMENT("E sets F2 -> A wakes up");
      TNT_ITEM__SEND_CMD_EVENT(
            TNT_TASK__E, EVENT_MODIFY, TNT_EVENT__1, (_TEST_FLAG__F2),
            /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
            );
   } else {
      TNT_TEST_COMMENT("Interrupt sets F2 -> A wakes up");
      TNT_ITEM__INT_SEND_CMD_EVENT(
            EVENT_IMODIFY, TNT_EVENT__1, (_TEST_FLAG__F2),
            /* the rest is not used */ 0, 0, TN_EVENTGRP_OP_SET
            );
   }
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F1 | _TEST_FLAG__F2));

         if (!bool_interrupt){
            TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
         } else {
            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
         }
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_a), (_TEST_FLAG__F1 | _TEST_FLAG__F2));

   TNT_TEST_COMMENT("E suspends C");
   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__E, TASK_SUSPEND, TNT_TASK__C);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__C, STATE, TSK_STATE_WAITSUSP);

         TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("E sets F3 and F1 -> C stops waiting but is still suspended");
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__E, EVENT_MODIFY, TNT_EVENT__1, (_TEST_FLAG__F3 | _TEST_FLAG__F1),
         /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__C, STATE, TSK_STATE_SUSPEND);

         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F1 | _TEST_FLAG__F2 | _TEST_FLAG__F3));

         TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
         );
   
   //-- pattern should not yet be updated, because task is suspended..
   _pattern_check(_FLAGS_PT(_flags_pattern_c), _PATTERN_POISON);

   TNT_TEST_COMMENT("E resumes C");
   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__E, TASK_RESUME, TNT_TASK__C);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__C, STATE, TSK_STATE_WAIT);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TERR_NO_ERR);

         TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
         );

   //-- now, pattern should be updated.
   _pattern_check(
         _FLAGS_PT(_flags_pattern_c),
#if _X96_HACKS
         (_TEST_FLAG__F2 |                  _TEST_FLAG__F3)
#else
         (_TEST_FLAG__F1 | _TEST_FLAG__F2 | _TEST_FLAG__F3)
#endif
         );





   TNT_TEST_COMMENT("A waits for flags F1 and F2: should not wait");
   _pattern_reset(_FLAGS_PT(_flags_pattern_a));
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
         TN_EVENTGRP_WMODE_AND, _FLAGS_PT(_flags_pattern_a), TN_WAIT_INFINITE, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         /*TODO: check that A didn't wait*/
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );
   _pattern_check(
         _FLAGS_PT(_flags_pattern_a),
#if _X96_HACKS
         (_TEST_FLAG__F1 | _TEST_FLAG__F2)
#else
         (_TEST_FLAG__F1 | _TEST_FLAG__F2 | _TEST_FLAG__F3)
#endif
         );

   TNT_TEST_COMMENT("E clears F2");
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__E, EVENT_MODIFY, TNT_EVENT__1, (_TEST_FLAG__F2),
         /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_CLEAR
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F1 | _TEST_FLAG__F3));

         TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("A waits for flags F1 and F2: should wait for some time");
   _pattern_reset(_FLAGS_PT(_flags_pattern_a));
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
         TN_EVENTGRP_WMODE_AND, _FLAGS_PT(_flags_pattern_a), _TIMEOUT_FOR_WAIT, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_EVENT);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_a), _PATTERN_POISON);

   TNT_TEST_COMMENT("Wait until A wakes up");
   TNT_ITEM__WAIT(_TIMEOUT_FOR_WAIT * 2);

   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_TIMEOUT);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_a), _PATTERN_POISON);

   TNT_TEST_COMMENT("A waits for flags F1 and F2 with zero timeout");
   _pattern_reset(_FLAGS_PT(_flags_pattern_a));
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
         TN_EVENTGRP_WMODE_AND, _FLAGS_PT(_flags_pattern_a), 0, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_TIMEOUT);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_a), _PATTERN_POISON);

   TNT_TEST_COMMENT("A waits for flags F1 and F2 in polling mode");
   _pattern_reset(_FLAGS_PT(_flags_pattern_a));
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_WAIT_POLLING, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
         TN_EVENTGRP_WMODE_AND, _FLAGS_PT(_flags_pattern_a), TN_WAIT_INFINITE/*not used*/, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_TIMEOUT);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_a), _PATTERN_POISON);

   TNT_TEST_COMMENT("A waits for flags F1 or F2: should not wait");
   _pattern_reset(_FLAGS_PT(_flags_pattern_a));
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
         TN_EVENTGRP_WMODE_OR, _FLAGS_PT(_flags_pattern_a), TN_WAIT_INFINITE, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         /*TODO: check that A didn't wait*/
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );
   _pattern_check(
         _FLAGS_PT(_flags_pattern_a),
#if _X96_HACKS
         (_TEST_FLAG__F1)
#else
         (_TEST_FLAG__F1 | _TEST_FLAG__F3)
#endif
         );

   TNT_TEST_COMMENT("Interrupt waits for flags F1 and F2 in polling mode");
   _pattern_reset(_FLAGS_PT(_flags_pattern_int));
   TNT_ITEM__INT_SEND_CMD_EVENT(
         EVENT_IWAIT, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
         TN_EVENTGRP_WMODE_AND, _FLAGS_PT(_flags_pattern_int), 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_TIMEOUT);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_int), _PATTERN_POISON);

   TNT_TEST_COMMENT("Interrupt waits for flags F1 or F2 in polling mode");
   _pattern_reset(_FLAGS_PT(_flags_pattern_int));
   TNT_ITEM__INT_SEND_CMD_EVENT(
         EVENT_IWAIT, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
         TN_EVENTGRP_WMODE_OR, _FLAGS_PT(_flags_pattern_int), 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
         );
   _pattern_check(
         _FLAGS_PT(_flags_pattern_int),
#if _X96_HACKS
         (_TEST_FLAG__F1)
#else
         (_TEST_FLAG__F1 | _TEST_FLAG__F3)
#endif
         );

   TNT_TEST_COMMENT("A waits for F2");
   _pattern_reset(_FLAGS_PT(_flags_pattern_a));
   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F2),
         TN_EVENTGRP_WMODE_OR, _FLAGS_PT(_flags_pattern_a), TN_WAIT_INFINITE, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_EVENT);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );
   _pattern_check(_FLAGS_PT(_flags_pattern_a), (_PATTERN_POISON));

   if (!bool_interrupt){
      TNT_TEST_COMMENT("E toggles F1 and F2");
      TNT_ITEM__SEND_CMD_EVENT(
            TNT_TASK__E, EVENT_MODIFY, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
            /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_TOGGLE
            );
   } else {
      TNT_TEST_COMMENT("Interrupt toggles F1 and F2");
      TNT_ITEM__INT_SEND_CMD_EVENT(
            EVENT_IMODIFY, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
            /* the rest is not used */ 0, 0, TN_EVENTGRP_OP_TOGGLE
            );
   }
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F2 | _TEST_FLAG__F3));
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

         if (!bool_interrupt){
            TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
         } else {
            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
         }
         );
   _pattern_check(
         _FLAGS_PT(_flags_pattern_a),
#if _X96_HACKS
         (_TEST_FLAG__F2)
#else
         (_TEST_FLAG__F2 | _TEST_FLAG__F3)
#endif
         );





   TNT_TEST_COMMENT("---- done, delete objects");

   TNT_ITEM__EVENT_MANAGE(DELETE, TNT_EVENT__1, /*the rest is not used*/ TNT_EVENT_NORMAL_ATTR, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__C, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__D, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__E, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__C, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__D, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__E, 0);

}
// }}}

static void _eventgrp_test_autoclr(bool bool_interrupt, bool bool_flags_pattern)   // {{{
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("event test multi");

   int priority_task_a = 7;
   int priority_task_b = 6;
   int priority_task_c = 5;
   int priority_task_d = 4;
   int priority_task_e = 3;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__C, priority_task_c);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__D, priority_task_d);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__E, priority_task_e);

   TNT_TEST_COMMENT("create event, should be ok");
   TNT_ITEM__EVENT_MANAGE(CREATE, TNT_EVENT__1, TNT_EVENT_NORMAL_ATTR, (0));
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__C, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__C, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__C, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__C, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__D, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__D, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__D, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__D, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__E, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__E, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__E, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__EVENT(TNT_EVENT__1, EXISTS,    1);
         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (0));

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );



         TNT_TEST_COMMENT("A waits for flags F1 and F2");
         _pattern_reset(_FLAGS_PT(_flags_pattern_a));
         TNT_ITEM__SEND_CMD_EVENT(
               TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
               (TN_EVENTGRP_WMODE_AND | TN_EVENTGRP_WMODE_AUTOCLR), _FLAGS_PT(_flags_pattern_a), TN_WAIT_INFINITE, 0
               );
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_EVENT);
               );
         _pattern_check(_FLAGS_PT(_flags_pattern_a), _PATTERN_POISON);


         if (!bool_interrupt){
            TNT_TEST_COMMENT("E sets F2 and F3");
            TNT_ITEM__SEND_CMD_EVENT(
                  TNT_TASK__E, EVENT_MODIFY, TNT_EVENT__1, (_TEST_FLAG__F2 | _TEST_FLAG__F3),
                  /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
                  );
         } else {
            TNT_TEST_COMMENT("Interrupt sets F2 and F3");
            TNT_ITEM__INT_SEND_CMD_EVENT(
                  EVENT_IMODIFY, TNT_EVENT__1, (_TEST_FLAG__F2 | _TEST_FLAG__F3),
                  /* the rest is not used */ 0, 0, TN_EVENTGRP_OP_SET
                  );
         }
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F2 | _TEST_FLAG__F3));

               if (!bool_interrupt){
                  TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
               } else {
                  TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
               }
               );

         if (!bool_interrupt){
            TNT_TEST_COMMENT("E toggles F1: A wakes up, F1 and F2 get cleared");
            TNT_ITEM__SEND_CMD_EVENT(
                  TNT_TASK__E, EVENT_MODIFY, TNT_EVENT__1, (_TEST_FLAG__F1),
                  /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_TOGGLE
                  );
         } else {
            TNT_TEST_COMMENT("Interrupt toggles F1: A wakes up, F1 and F2 get cleared");
            TNT_ITEM__INT_SEND_CMD_EVENT(
                  EVENT_IMODIFY, TNT_EVENT__1, (_TEST_FLAG__F1),
                  /* the rest is not used */ 0, 0, TN_EVENTGRP_OP_TOGGLE
                  );
         }
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
               TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F3));

               if (!bool_interrupt){
                  TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
               } else {
                  TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
               }
               );
         _pattern_check(_FLAGS_PT(_flags_pattern_a), (_TEST_FLAG__F1 | _TEST_FLAG__F2 | _TEST_FLAG__F3));


         TNT_TEST_COMMENT("E sets F1");
         TNT_ITEM__SEND_CMD_EVENT(
               TNT_TASK__E, EVENT_MODIFY, TNT_EVENT__1, (_TEST_FLAG__F1),
               /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
               );
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F1 | _TEST_FLAG__F3));
               TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
               );


         TNT_TEST_COMMENT("A waits for flags F1 or F2: should not wait");
         _pattern_reset(_FLAGS_PT(_flags_pattern_a));
         TNT_ITEM__SEND_CMD_EVENT(
               TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
               (TN_EVENTGRP_WMODE_OR | TN_EVENTGRP_WMODE_AUTOCLR), _FLAGS_PT(_flags_pattern_a), TN_WAIT_INFINITE, 0
               );
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
               TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F3));

               );
         _pattern_check(_FLAGS_PT(_flags_pattern_a), (_TEST_FLAG__F1 | _TEST_FLAG__F3));


         TNT_TEST_COMMENT("---- done, delete objects");

         TNT_ITEM__EVENT_MANAGE(DELETE, TNT_EVENT__1, /*the rest is not used*/ TNT_EVENT_NORMAL_ATTR, 0);

         TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
         TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);
         TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__C, 0);
         TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__D, 0);
         TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__E, 0);

         TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
         TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);
         TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__C, 0);
         TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__D, 0);
         TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__E, 0);

}
// }}}


static void _eventgrp_wcontext_test(void)   // {{{
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("eventgrp wrong context test");

   int priority_task_a = 5;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);

   TNT_TEST_COMMENT("create event, should be ok");
   TNT_ITEM__EVENT_MANAGE(CREATE, TNT_EVENT__1, TNT_EVENT_NORMAL_ATTR, (0));
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__EVENT(TNT_EVENT__1, EXISTS,    1);
         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (0));

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("Interrupt tries call task services");

   TNT_ITEM__INT_SEND_CMD_EVENT(
         EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F1),
         TN_EVENTGRP_WMODE_AND, NULL, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__INT_SEND_CMD_EVENT(
         EVENT_WAIT_POLLING, TNT_EVENT__1, (_TEST_FLAG__F1),
         TN_EVENTGRP_WMODE_AND, NULL, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__INT_SEND_CMD_EVENT(
         EVENT_MODIFY, TNT_EVENT__1, (_TEST_FLAG__F1),
         TN_EVENTGRP_WMODE_AND, NULL, TN_EVENTGRP_OP_SET
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );


   TNT_TEST_COMMENT("Task tries call interrupt services");

   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_IWAIT, TNT_EVENT__1, (_TEST_FLAG__F1),
         TN_EVENTGRP_WMODE_AND, NULL, TN_WAIT_INFINITE, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__SEND_CMD_EVENT(
         TNT_TASK__A, EVENT_IMODIFY, TNT_EVENT__1, (_TEST_FLAG__F1),
         TN_EVENTGRP_WMODE_AND, NULL, TN_WAIT_INFINITE, 0
         );
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WCONTEXT);
         );






   TNT_TEST_COMMENT("---- done, delete objects");
   TNT_ITEM__EVENT_MANAGE(DELETE, TNT_EVENT__1, /*the rest is not used*/ TNT_EVENT_NORMAL_ATTR, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);

}
// }}}

#if TN_OLD_EVENT_API
static void _eventgrp_old_api_test(bool bool_interrupt)   // {{{
{
   tnt_prev_check_item__reset();

   TNT_TEST_TITLE("event old api test");

   int priority_task_a = 7;
   int priority_task_b = 6;
   int priority_task_e = 5;
   bool bool_flags_pattern = true;

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__E, priority_task_e);

   TNT_TEST_COMMENT("try to create event with attr=0");
   TNT_ITEM__EVENT_MANAGE(CREATE, TNT_EVENT__1, (0), (0));
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__EVENT(TNT_SEM__1, EXISTS, 0);
         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("try to create event with attr=(MULTI | CLR)");
   TNT_ITEM__EVENT_MANAGE(CREATE, TNT_EVENT__1, (TN_EVENTGRP_ATTR_CLR | TN_EVENTGRP_ATTR_MULTI), (0));
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__EVENT(TNT_SEM__1, EXISTS, 0);
         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WRONG_PARAM);
         );

   TNT_TEST_COMMENT("create event, should be ok");
   TNT_ITEM__EVENT_MANAGE(CREATE, TNT_EVENT__1, (TN_EVENTGRP_ATTR_CLR | TN_EVENTGRP_ATTR_SINGLE), (0));
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__E, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__E, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__E, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__EVENT(TNT_EVENT__1, EXISTS,    1);
         TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (0));

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

         TNT_TEST_COMMENT("A waits for flags F1 and F2");
         _pattern_reset(_FLAGS_PT(_flags_pattern_a));
         TNT_ITEM__SEND_CMD_EVENT(
               TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
               TN_EVENTGRP_WMODE_AND, _FLAGS_PT(_flags_pattern_a), TN_WAIT_INFINITE, 0
               );
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_EVENT);
               );
         _pattern_check(_FLAGS_PT(_flags_pattern_a), _PATTERN_POISON);


         TNT_TEST_COMMENT("B waits for flags F1 or F2 and fails because of SINGLE flag");
         _pattern_reset(_FLAGS_PT(_flags_pattern_b));
         TNT_ITEM__SEND_CMD_EVENT(
               TNT_TASK__B, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
               TN_EVENTGRP_WMODE_OR, _FLAGS_PT(_flags_pattern_b), TN_WAIT_INFINITE, 0
               );
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TN_RC_ILLEGAL_USE);
               );
         _pattern_check(_FLAGS_PT(_flags_pattern_b), _PATTERN_POISON);

         TNT_TEST_COMMENT("E sets F2 -> nothing happens");
         TNT_ITEM__SEND_CMD_EVENT(
               TNT_TASK__E, EVENT_MODIFY_OLD_TNKERNEL, TNT_EVENT__1, (_TEST_FLAG__F2),
               /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
               );
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F2));

               TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
               );

         if (!bool_interrupt){
            TNT_TEST_COMMENT("E clears F2");
            TNT_ITEM__SEND_CMD_EVENT(
                  TNT_TASK__E, EVENT_MODIFY_OLD_TNKERNEL, TNT_EVENT__1, ~(_TEST_FLAG__F2),
                  /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_CLEAR
                  );
         } else {
            TNT_TEST_COMMENT("Interrupt clears F2");
            TNT_ITEM__INT_SEND_CMD_EVENT(
                  EVENT_IMODIFY_OLD_TNKERNEL, TNT_EVENT__1, ~(_TEST_FLAG__F2),
                  /* the rest is not used */ 0, 0, TN_EVENTGRP_OP_CLEAR
                  );
         }
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (0));

               if (!bool_interrupt){
               TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
               } else {
               TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
               }
               );

         if (!bool_interrupt){
            TNT_TEST_COMMENT("E sets F1");
            TNT_ITEM__SEND_CMD_EVENT(
                  TNT_TASK__E, EVENT_MODIFY_OLD_TNKERNEL, TNT_EVENT__1, (_TEST_FLAG__F1),
                  /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
                  );
         } else {
            TNT_TEST_COMMENT("Interrupt sets F1");
            TNT_ITEM__INT_SEND_CMD_EVENT(
                  EVENT_IMODIFY_OLD_TNKERNEL, TNT_EVENT__1, (_TEST_FLAG__F1),
                  /* the rest is not used */ 0, 0, TN_EVENTGRP_OP_SET
                  );
         }
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F1));

               if (!bool_interrupt){
               TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
               } else {
               TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
               }
               );

         TNT_TEST_COMMENT("E sets F3");
         TNT_ITEM__SEND_CMD_EVENT(
               TNT_TASK__E, EVENT_MODIFY_OLD_TNKERNEL, TNT_EVENT__1, (_TEST_FLAG__F3),
               /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
               );
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F1 | _TEST_FLAG__F3));

               TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
               );

         if (!bool_interrupt){
            TNT_TEST_COMMENT("E sets F2 -> A wakes up");
            TNT_ITEM__SEND_CMD_EVENT(
                  TNT_TASK__E, EVENT_MODIFY_OLD_TNKERNEL, TNT_EVENT__1, (_TEST_FLAG__F2),
                  /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
                  );
         } else {
            TNT_TEST_COMMENT("Interrupt sets F2 -> A wakes up, the whole pattern gets cleared");
            TNT_ITEM__INT_SEND_CMD_EVENT(
                  EVENT_IMODIFY_OLD_TNKERNEL, TNT_EVENT__1, (_TEST_FLAG__F2),
                  /* the rest is not used */ 0, 0, TN_EVENTGRP_OP_SET
                  );
         }
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
               TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

#if _X96_HACKS
               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F3));
#else
               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (0));
#endif

               if (!bool_interrupt){
               TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
               } else {
               TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
               }
               );
         _pattern_check(
               _FLAGS_PT(_flags_pattern_a),
#if _X96_HACKS
               (_TEST_FLAG__F1 | _TEST_FLAG__F2)
#else
               (_TEST_FLAG__F1 | _TEST_FLAG__F2 | _TEST_FLAG__F3)
#endif
               );


         TNT_TEST_COMMENT("E sets F2 and F3");
         TNT_ITEM__SEND_CMD_EVENT(
               TNT_TASK__E, EVENT_MODIFY_OLD_TNKERNEL, TNT_EVENT__1, (_TEST_FLAG__F2 | _TEST_FLAG__F3),
               /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
               );
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F2 | _TEST_FLAG__F3));

               TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
               );

         TNT_TEST_COMMENT("Interrupt waits for flags F1 and F2 in polling mode: failed, nothing changes");
         _pattern_reset(_FLAGS_PT(_flags_pattern_int));
         TNT_ITEM__INT_SEND_CMD_EVENT(
               EVENT_IWAIT, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
               TN_EVENTGRP_WMODE_AND, _FLAGS_PT(_flags_pattern_int), 0
               );
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_TIMEOUT);
               );
         _pattern_check(_FLAGS_PT(_flags_pattern_int), _PATTERN_POISON);

         TNT_TEST_COMMENT("Interrupt waits for flags F1 or F2 in polling mode: succeeds, pattern gets cleared");
         _pattern_reset(_FLAGS_PT(_flags_pattern_int));
         TNT_ITEM__INT_SEND_CMD_EVENT(
               EVENT_IWAIT, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
               TN_EVENTGRP_WMODE_OR, _FLAGS_PT(_flags_pattern_int), 0
               );
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
#if _X96_HACKS
               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F3));
#else
               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (0));
#endif
               );
         _pattern_check(
               _FLAGS_PT(_flags_pattern_int),
#if _X96_HACKS
               (_TEST_FLAG__F2)
#else
               (_TEST_FLAG__F2 | _TEST_FLAG__F3)
#endif
               );

         TNT_TEST_COMMENT("E sets F2 and F3");
         TNT_ITEM__SEND_CMD_EVENT(
               TNT_TASK__E, EVENT_MODIFY_OLD_TNKERNEL, TNT_EVENT__1, (_TEST_FLAG__F2 | _TEST_FLAG__F3),
               /* the rest is not used */ 0, 0, 0, TN_EVENTGRP_OP_SET
               );
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F2 | _TEST_FLAG__F3));

               TNT_CHECK__TASK(TNT_TASK__E, LAST_RETVAL, TERR_NO_ERR);
               );

         TNT_TEST_COMMENT("A waits for flags F1 or F2: should not wait");
         _pattern_reset(_FLAGS_PT(_flags_pattern_a));
         TNT_ITEM__SEND_CMD_EVENT(
               TNT_TASK__A, EVENT_WAIT, TNT_EVENT__1, (_TEST_FLAG__F1 | _TEST_FLAG__F2),
               TN_EVENTGRP_WMODE_OR, _FLAGS_PT(_flags_pattern_a), TN_WAIT_INFINITE, 0
               );
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               /*TODO: check that A didn't wait*/
               TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
#if _X96_HACKS
               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (_TEST_FLAG__F3));
#else
               TNT_CHECK__EVENT(TNT_EVENT__1, PATTERN,   (0));
#endif
               );
         _pattern_check(
               _FLAGS_PT(_flags_pattern_a),
#if _X96_HACKS
               (_TEST_FLAG__F2)
#else
               (_TEST_FLAG__F2 | _TEST_FLAG__F3)
#endif
               );




         TNT_TEST_COMMENT("---- done, delete objects");

         TNT_ITEM__EVENT_MANAGE(DELETE, TNT_EVENT__1, /*the rest is not used*/ TNT_EVENT_NORMAL_ATTR, 0);

         TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
         TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);
         TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__E, 0);

         TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
         TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);
         TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__E, 0);

}
// }}}
#endif

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void appl_tntest_event(void)
{
   _eventgrp_wrong_params_test();

   _eventgrp_test(false, true);
   _eventgrp_test(true,  true);
   _eventgrp_test(false, false);
   _eventgrp_test(true,  false);

   _eventgrp_test_autoclr(false, true);
   _eventgrp_test_autoclr(true,  true);
   _eventgrp_test_autoclr(false, false);
   _eventgrp_test_autoclr(true,  false);


   _eventgrp_wcontext_test();

#if TN_OLD_EVENT_API
   _eventgrp_old_api_test(false);
   _eventgrp_old_api_test(true);
#endif
}

/*******************************************************************************
 *    end of file
 ******************************************************************************/



