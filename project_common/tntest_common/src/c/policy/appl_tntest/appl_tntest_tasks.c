/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "appl_tntest_tasks.h"
#include "tntest.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

#define  _TIMEOUT_FOR_DEADLOCK         (500)
#define  _TIMEOUT_FOR_SLEEP            (500)


/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/


static void _task_terminate_tests(void) // {{{
{
   TNT_TEST_TITLE("task termination test");

   int priority_task_a = 5;

   tnt_prev_check_item__reset();

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);
         //TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

   TNT_TEST_COMMENT("tn_task_exit() and then re-activate");
   TNT_ITEM__SEND_CMD_GENERAL(TNT_TASK__A, TASK_EXIT);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_DORMANT);
         );

   TNT_ITEM__TASK_MANAGE(ACTIVATE, TNT_TASK__A, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);
         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("return from task function and then re-activate");
   TNT_ITEM__SEND_CMD_GENERAL(TNT_TASK__A, TASK_RETURN);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_DORMANT);
         );

   TNT_ITEM__TASK_MANAGE(ACTIVATE, TNT_TASK__A, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);
         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("tn_task_terminate() and then re-activate");
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_DORMANT);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("tn_task_terminate() again: TERR_WSTATE should be returned");
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WSTATE);
         );

   TNT_TEST_COMMENT("tn_task_activate() should succeed");
   TNT_ITEM__TASK_MANAGE(ACTIVATE, TNT_TASK__A, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("tn_task_activate() again: TERR_WSTATE should be returned");
   TNT_ITEM__TASK_MANAGE(ACTIVATE, TNT_TASK__A, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WSTATE);
         );

   TNT_TEST_COMMENT("tn_task_terminate() and then re-activate from interrupt");
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_DORMANT);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_ITEM__INT_SEND_CMD_TASK(TASK_IACTIVATE, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);

         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("try to terminate task again: TERR_WSTATE should be returned");
   TNT_ITEM__INT_SEND_CMD_TASK(TASK_IACTIVATE, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);

         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WSTATE);
         );

   TNT_TEST_COMMENT("A sleeps");
   TNT_ITEM__SEND_CMD_SLEEP(TNT_TASK__A, TASK_SLEEP, _TIMEOUT_FOR_SLEEP);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_SLEEP);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

   TNT_TEST_COMMENT("tn_task_terminate() sleeping task and then re-activate");
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_DORMANT);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("tn_task_terminate() again: TERR_WSTATE should be returned");
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_WSTATE);
         );

   TNT_TEST_COMMENT("tn_task_activate() should succeed");
   TNT_ITEM__TASK_MANAGE(ACTIVATE, TNT_TASK__A, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );



   TNT_TEST_COMMENT("done; delete task");

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_DORMANT);

         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__WAIT_AND_CHECK(
         TNT_CHECK__DIRECTOR(LAST_RETVAL, TERR_NO_ERR);
         );

}
// }}}

static void _task_suspend_test(void) // {{{
{
   TNT_TEST_TITLE("task suspension check");

   int priority_task_a = 6;
   int priority_task_b = 5;

   tnt_prev_check_item__reset();

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);

   TNT_TEST_COMMENT("initial state: all tasks has their base priority, no mutexes are held");
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);
         );


   TNT_TEST_COMMENT("A suspends itself");
   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__A, TASK_SUSPEND, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_SUSPEND);
         );

   TNT_TEST_COMMENT("B resumes A");
   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_RESUME, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("B suspends A");
   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_SUSPEND, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAITSUSP);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("B tries to suspend A again");
   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_SUSPEND, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_WSTATE);
         );

   TNT_TEST_COMMENT("send a message to A to switch it to polling mode,");
   TNT_TEST_COMMENT("but it should be done only when A is resumed");
   TNT_ITEM__SEND_CMD_EXMODE(TNT_TASK__A, POLLING);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_SUSPEND);
         );

   TNT_TEST_COMMENT("B resumes A, A receives message and switches to polling mode");
   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_RESUME, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("B tries to resume A again");
   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_RESUME, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_WSTATE);
         );

   TNT_TEST_COMMENT("B suspends A");
   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_SUSPEND, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_SUSPEND);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("B resumes A");
   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_RESUME, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);
         //TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

   TNT_TEST_COMMENT("A suspends itself");
   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__A, TASK_SUSPEND, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_SUSPEND);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         );

   TNT_TEST_COMMENT("now tasks are terminated, A is terminated from suspended state");


   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);

}
// }}}

static void _task_sleep_test(bool bool_suspend) // {{{
{
   TNT_TEST_TITLE("task sleep check, bool_suspend=%d", bool_suspend);

   int priority_task_a = 6;
   int priority_task_b = 5;

   tnt_prev_check_item__reset();

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);
   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__B, priority_task_b);

   TNT_ITEM__MUTEX_MANAGE(CREATE, TNT_MUTEX__1, TN_MUTEX_ATTR_INHERIT, 0);

   TNT_TEST_COMMENT("initial state: all tasks has their base priority, no mutexes are held");
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__TASK(TNT_TASK__B, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__B, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__B, STATE,       TSK_STATE_WAIT);

         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);
         );


   TNT_ITEM__SEND_CMD_EXMODE(TNT_TASK__A, POLLING);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
         TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);

         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);
         );

   {
      TNT_TEST_COMMENT("A sleeps");
      TNT_ITEM__SEND_CMD_SLEEP(TNT_TASK__A, TASK_SLEEP, _TIMEOUT_FOR_SLEEP);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_SLEEP);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );

      if (bool_suspend){
         TNT_TEST_COMMENT("B suspends A");
         TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_SUSPEND, TNT_TASK__A);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAITSUSP);

               TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
               );

      }

      TNT_TEST_COMMENT("B wakes A up");
      TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_WAKEUP, TNT_TASK__A);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, (bool_suspend ? TSK_STATE_SUSPEND : TSK_STATE_RUNNABLE));
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, (bool_suspend 
                  ? TWORKER_MAN__LAST_RETVAL__UNKNOWN 
                  : TERR_NO_ERR)
               );

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
            );

      TNT_TEST_COMMENT("B tries to wake A up again, TERR_WSTATE should be returned");
      TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_WAKEUP, TNT_TASK__A);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_WSTATE);
            );

      if (bool_suspend){
         TNT_TEST_COMMENT("B resumes A");
         TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_RESUME, TNT_TASK__A);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);
               TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

               TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
               );

      }

   }

   {
      TNT_TEST_COMMENT("A sleeps");
      TNT_ITEM__SEND_CMD_SLEEP(TNT_TASK__A, TASK_SLEEP, _TIMEOUT_FOR_SLEEP);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_SLEEP);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );

      TNT_TEST_COMMENT("B forcibly releases A from wait");
      TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_RELEASE_WAIT, TNT_TASK__A);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_FORCED);

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
            );

      TNT_TEST_COMMENT("B tries to forcibly release A from wait again: should fail");
      TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_RELEASE_WAIT, TNT_TASK__A);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_WSTATE);
            );
   }

   {
      TNT_TEST_COMMENT("A sleeps");
      TNT_ITEM__SEND_CMD_SLEEP(TNT_TASK__A, TASK_SLEEP, _TIMEOUT_FOR_SLEEP);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_SLEEP);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );

      if (bool_suspend){
         TNT_TEST_COMMENT("B suspends A");
         TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_SUSPEND, TNT_TASK__A);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAITSUSP);

               TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
               );

      }

      TNT_TEST_COMMENT("Interrupt wakes A up");
      TNT_ITEM__INT_SEND_CMD_TASK(TASK_IWAKEUP, TNT_TASK__A);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, (bool_suspend ? TSK_STATE_SUSPEND : TSK_STATE_RUNNABLE));
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, (bool_suspend 
                  ? TWORKER_MAN__LAST_RETVAL__UNKNOWN 
                  : TERR_NO_ERR)
               );

            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
            );

      TNT_TEST_COMMENT("Interrupt tries to wake A up again, should fail");
      TNT_ITEM__INT_SEND_CMD_TASK(TASK_IWAKEUP, TNT_TASK__A);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WSTATE);
            );

      if (bool_suspend){
         TNT_TEST_COMMENT("B resumes A");
         TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_RESUME, TNT_TASK__A);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);
               TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_NO_ERR);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

      }

   }

   {
      TNT_TEST_COMMENT("A sleeps");
      TNT_ITEM__SEND_CMD_SLEEP(TNT_TASK__A, TASK_SLEEP, _TIMEOUT_FOR_SLEEP);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_SLEEP);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );

      if (bool_suspend){
         TNT_TEST_COMMENT("B suspends A");
         TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_SUSPEND, TNT_TASK__A);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAITSUSP);

               TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
               );

      }

      TNT_TEST_COMMENT("Interrupt forcibly releases A from wait");
      TNT_ITEM__INT_SEND_CMD_TASK(TASK_IRELEASE_WAIT, TNT_TASK__A);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, (bool_suspend ? TSK_STATE_SUSPEND : TSK_STATE_RUNNABLE));
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, (bool_suspend 
                  ? TWORKER_MAN__LAST_RETVAL__UNKNOWN 
                  : TERR_FORCED)
               );

            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
            );

      TNT_TEST_COMMENT("Interrupt tries to forcibly release A from wait again: should fail");
      TNT_ITEM__INT_SEND_CMD_TASK(TASK_IRELEASE_WAIT, TNT_TASK__A);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WSTATE);
            );

      if (bool_suspend){
         TNT_TEST_COMMENT("B resumes A");
         TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_RESUME, TNT_TASK__A);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);
               TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_FORCED);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

      }
   }

   {
      TNT_TEST_COMMENT("A sleeps");
      TNT_ITEM__SEND_CMD_SLEEP(TNT_TASK__A, TASK_SLEEP, _TIMEOUT_FOR_SLEEP);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_SLEEP);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);

            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            );

      if (bool_suspend){
         TNT_TEST_COMMENT("B suspends A");
         TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_SUSPEND, TNT_TASK__A);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAITSUSP);

               TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
               );

      }

      TNT_TEST_COMMENT("Wait until A wakes up");
      TNT_ITEM__WAIT(_TIMEOUT_FOR_SLEEP * 2);
      TNT_ITEM__CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, (bool_suspend ? TSK_STATE_SUSPEND : TSK_STATE_RUNNABLE));
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, (bool_suspend 
                  ? TWORKER_MAN__LAST_RETVAL__UNKNOWN 
                  : TERR_TIMEOUT)
               );
            );

      TNT_TEST_COMMENT("Interrupt tries to forcibly release A from wait again: should fail");
      TNT_ITEM__INT_SEND_CMD_TASK(TASK_IRELEASE_WAIT, TNT_TASK__A);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WSTATE);
            );

      if (bool_suspend){
         TNT_TEST_COMMENT("B resumes A");
         TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_RESUME, TNT_TASK__A);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);
               TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_TIMEOUT);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

      }
   }



   TNT_TEST_COMMENT("B locks M1");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_LOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__B);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 1);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

   {
      TNT_TEST_COMMENT("A tries to lock M1 -> A blocks");
      TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);
            );

      if (bool_suspend){
         TNT_TEST_COMMENT("B suspends A");
         TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_SUSPEND, TNT_TASK__A);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAITSUSP);

               TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
               );

      }

      TNT_TEST_COMMENT("B tries to wake A up, should fail because A waits for mutex, not just sleeps");
      TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_WAKEUP, TNT_TASK__A);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_WSTATE);
            );

      TNT_TEST_COMMENT("B releases A from wait");
      TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_RELEASE_WAIT, TNT_TASK__A);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, (bool_suspend ? TSK_STATE_SUSPEND : TSK_STATE_RUNNABLE));
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, (bool_suspend 
                  ? TWORKER_MAN__LAST_RETVAL__UNKNOWN 
                  : TERR_FORCED)
               );

            TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
            );

      if (bool_suspend){
         TNT_TEST_COMMENT("B resumes A");
         TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_RESUME, TNT_TASK__A);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);
               TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_FORCED);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

      }
   }

   {
      TNT_TEST_COMMENT("A tries to lock M1 -> A blocks");
      TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__A, MUTEX_LOCK, TNT_MUTEX__1);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAIT);
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_MUTEX_I);
            );

      if (bool_suspend){
         TNT_TEST_COMMENT("B suspends A");
         TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_SUSPEND, TNT_TASK__A);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_WAITSUSP);

               TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
               );

      }

      TNT_TEST_COMMENT("Interrupt tries to wake A up, should fail because A waits for mutex, not just sleeps");
      TNT_ITEM__INT_SEND_CMD_TASK(TASK_IWAKEUP, TNT_TASK__A);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WSTATE);
            );

      TNT_TEST_COMMENT("Interrupt releases A from wait");
      TNT_ITEM__INT_SEND_CMD_TASK(TASK_IRELEASE_WAIT, TNT_TASK__A);
      TNT_ITEM__WAIT_AND_CHECK_DIFF(
            TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_NONE);
            TNT_CHECK__TASK(TNT_TASK__A, STATE, (bool_suspend ? TSK_STATE_SUSPEND : TSK_STATE_RUNNABLE));
            TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, (bool_suspend 
                  ? TWORKER_MAN__LAST_RETVAL__UNKNOWN 
                  : TERR_FORCED)
               );

            TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_NO_ERR);
            );

      if (bool_suspend){
         TNT_TEST_COMMENT("B resumes A");
         TNT_ITEM__SEND_CMD_TASK(TNT_TASK__B, TASK_RESUME, TNT_TASK__A);
         TNT_ITEM__WAIT_AND_CHECK_DIFF(
               TNT_CHECK__TASK(TNT_TASK__A, STATE, TSK_STATE_RUNNABLE);
               TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_FORCED);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );

      }
   }


   TNT_TEST_COMMENT("B unlocks M1");
   TNT_ITEM__SEND_CMD_MUTEX(TNT_TASK__B, MUTEX_UNLOCK, TNT_MUTEX__1);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__MUTEX(TNT_MUTEX__1, HOLDER, TNT_TASK__NONE);
         TNT_CHECK__MUTEX(TNT_MUTEX__1, LOCK_CNT, 0);

         TNT_CHECK__TASK(TNT_TASK__B, LAST_RETVAL, TERR_NO_ERR);
         );


   TNT_TEST_COMMENT("---- done, delete objects");


   TNT_ITEM__MUTEX_MANAGE(DELETE, TNT_MUTEX__1, 0, 0);

   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__B, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);
   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__B, 0);

}
// }}}

static void _task_wcontext_test(void) // {{{
{
   TNT_TEST_TITLE("task wrong context test");

   int priority_task_a = 6;

   tnt_prev_check_item__reset();

   TNT_ITEM__TASK_MANAGE(CREATE, TNT_TASK__A, priority_task_a);

   TNT_TEST_COMMENT("initial state: all tasks has their base priority, no mutexes are held");
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, PRIORITY,    TNT_BASE_PRIORITY);
         TNT_CHECK__TASK(TNT_TASK__A, WAIT_REASON, TSK_WAIT_REASON_DQUE_WRECEIVE);
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TWORKER_MAN__LAST_RETVAL__UNKNOWN);
         TNT_CHECK__TASK(TNT_TASK__A, STATE,       TSK_STATE_WAIT);
         );


   TNT_TEST_COMMENT("Interrupt tries call task services");

   TNT_ITEM__INT_SEND_CMD_TASK(TASK_SUSPEND, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__INT_SEND_CMD_TASK(TASK_RESUME, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__INT_SEND_CMD_SLEEP(TASK_SLEEP, TN_WAIT_INFINITE);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__INT_SEND_CMD_TASK(TASK_WAKEUP, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );

#if 0
   TNT_ITEM__INT_SEND_CMD_TASK(TASK_ACTIVATE, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );
#endif

   TNT_ITEM__INT_SEND_CMD_TASK(TASK_RELEASE_WAIT, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );

#if 0
   TNT_ITEM__INT_SEND_CMD_TASK(TASK_TERMINATE, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__INTERRUPT(LAST_RETVAL, TERR_WCONTEXT);
         );
#endif


   TNT_TEST_COMMENT("Task tries call interrupt services");

   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__A, TASK_IWAKEUP, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__A, TASK_IACTIVATE, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WCONTEXT);
         );

   TNT_ITEM__SEND_CMD_TASK(TNT_TASK__A, TASK_IRELEASE_WAIT, TNT_TASK__A);
   TNT_ITEM__WAIT_AND_CHECK_DIFF(
         TNT_CHECK__TASK(TNT_TASK__A, LAST_RETVAL, TERR_WCONTEXT);
         );




   TNT_TEST_COMMENT("---- done, delete objects");


   TNT_ITEM__TASK_MANAGE(TERMINATE, TNT_TASK__A, 0);

   TNT_ITEM__TASK_MANAGE(DELETE, TNT_TASK__A, 0);

}
// }}}


/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void appl_tntest_tasks(void)
{
   _task_sleep_test(false);
   _task_sleep_test(true);
   _task_terminate_tests();
   _task_suspend_test();

   _task_wcontext_test();
}

/*******************************************************************************
 *    end of file
 ******************************************************************************/



