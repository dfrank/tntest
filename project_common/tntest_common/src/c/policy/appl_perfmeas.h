/*******************************************************************************
 *   Description:   TODO
 *
 ******************************************************************************/

#ifndef _APPL_PERFMEAS_H
#define _APPL_PERFMEAS_H

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC TYPES
 ******************************************************************************/

/*******************************************************************************
 *    GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTION PROTOTYPES
 ******************************************************************************/

void appl_perfmeas__init(void);
void appl_perfmeas__start(const char *p_start_msg, const char *p_task_name);
void appl_perfmeas__end(const char *p_end_msg, const char *p_task_name);

void appl_perfmeas__echo(void);

#endif // _APPL_PERFMEAS_H


/*******************************************************************************
 *    end of file
 ******************************************************************************/


