
/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "tntest_cfg.h"
#include "tworker_man.h"
#include "appl_perfmeas.h"

#include <stdio.h>
#include <string.h>


/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

#define TASK_WORKER_STACK_SIZE_DEF        (200/* + TASK_STACK_INTERRUPT_ADD*/)

#define  _TASK_WORKER_QUEUE_SIZE          4
#define  _TASK_NAME                       p_task_data->p_task_param->p_task_name

#define  _INTERRUPT_NAME                  "int"

#define  _LOG_INF(is_task, str, ...)    \
   if (is_task){\
      TNT_DTMSG_SIMP_I("[Task %s]: "    str, p_task_data->p_task_param->p_task_name, ## __VA_ARGS__);\
   } else {\
      TNT_DTMSG_SIMP_I_INT("int: " str,                                         ## __VA_ARGS__);\
   }

//#define  _LOG_INF(str, ...)


#define CMD_FMP_MSG_BUF_SIZE           (4)
#define INT_CMD_FMP_MSG_BUF_SIZE       (4)

#define _INT1_QUEUE_SIZE               4



/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

static T_DLList _tasks_list;

static struct TN_DQueue          _int1_queue;
static struct TWorkMan_TaskCmd   _int1_cmd;
void                            *_int1_queue_buf[_INT1_QUEUE_SIZE];
enum TN_RCode                    _int1_last_retval = TWORKER_MAN__LAST_RETVAL__UNKNOWN;

static struct TN_FMem   fmp_cmd_msg;
static TN_FMEM_BUF_DEF(
      fmp_cmd_msg_buf,
      struct TWorkMan_TaskCmd,
      CMD_FMP_MSG_BUF_SIZE
      );

static struct TN_FMem   fmp_int_cmd_msg;
static TN_FMEM_BUF_DEF(
      fmp_int_cmd_msg_buf,
      struct TWorkMan_TaskCmd,
      INT_CMD_FMP_MSG_BUF_SIZE
      );


/***************************************************************************************************
 *                                         PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                        EXTERNAL DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                  PRIVATE FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                       PRIVATE FUNCTIONS
 **************************************************************************************************/

static void _cmd_handle(struct TWorkMan_TaskCmd *p_cmd, struct TWorkMan_TaskData *p_task_data)
{
   bool bool_handled = false;
   bool is_task = (p_task_data != NULL);

   enum TN_RCode *p_rc;
   const char *p_task_name;

   if (is_task){
      p_rc = &p_task_data->last_retval;
      p_task_name = _TASK_NAME;
   } else {
      p_rc = &_int1_last_retval;
      p_task_name = _INTERRUPT_NAME;
   }

   switch (p_cmd->cmd){
      case TWORKMAN_TASK_CMD__MUTEX_LOCK:
         bool_handled = true;
         _LOG_INF(is_task, "locking mutex (0x%x)..", p_cmd->data.mutex.p_mutex);

         appl_perfmeas__start("calling tn_mutex_lock", p_task_name);
         *p_rc = tn_mutex_lock(p_cmd->data.mutex.p_mutex, p_cmd->data.mutex.timeout);
         appl_perfmeas__end("exiting from tn_mutex_lock", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "mutex (0x%x) locked", p_cmd->data.mutex.p_mutex);
         } else {
            _LOG_INF(is_task, "mutex (0x%x) locking failed with err=%d", p_cmd->data.mutex.p_mutex, *p_rc);
         }

         break;
      case TWORKMAN_TASK_CMD__MUTEX_UNLOCK:
         bool_handled = true;
         _LOG_INF(is_task, "unlocking mutex (0x%x)..", p_cmd->data.mutex.p_mutex);

         appl_perfmeas__start("calling tn_mutex_unlock", p_task_name);
         *p_rc = tn_mutex_unlock(p_cmd->data.mutex.p_mutex);
         appl_perfmeas__end("exiting from tn_mutex_unlock", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "mutex (0x%x) unlocked", p_cmd->data.mutex.p_mutex);
         } else {
            _LOG_INF(is_task, "mutex (0x%x) unlock failed with err=%d", p_cmd->data.mutex.p_mutex, *p_rc);
         }

         break;
      case TWORKMAN_TASK_CMD__MUTEX_DELETE:
         bool_handled = true;
         _LOG_INF(is_task, "deleting mutex (0x%x)..", p_cmd->data.mutex.p_mutex);

         appl_perfmeas__start("calling tn_mutex_delete", p_task_name);
         *p_rc = tn_mutex_delete(p_cmd->data.mutex.p_mutex);
         appl_perfmeas__end("exiting from tn_mutex_delete", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "mutex (0x%x) deleted", p_cmd->data.mutex.p_mutex);
         } else {
            _LOG_INF(is_task, "mutex (0x%x) deletion failed with err=%d", p_cmd->data.mutex.p_mutex, *p_rc);
         }
         break;
      case TWORKMAN_TASK_CMD__EXMODE_SET:
         bool_handled = true;
         p_task_data->exmode = p_cmd->data.exmode.exmode;
         *p_rc = TERR_NO_ERR;
         _LOG_INF(is_task, "set exmode=%d", p_task_data->exmode);
         break;
      case TWORKMAN_TASK_CMD__TASK_EXIT:
         bool_handled = true;
         _LOG_INF(is_task, "calling tn_task_exit()");
         appl_perfmeas__start("calling tn_task_exit", p_task_name);
         tn_task_exit((enum TN_TaskExitOpt)0);
         appl_perfmeas__end("exiting from tn_task_exit", p_task_name);
         _LOG_INF(is_task, "done");
         break;
      case TWORKMAN_TASK_CMD__TASK_RETURN:
         bool_handled = true;
         _LOG_INF(is_task, "setting bool_need_return flag");
         p_task_data->bool_need_return = true;
         break;


      case TWORKMAN_TASK_CMD__TASK_SUSPEND:
         bool_handled = true;
         _LOG_INF(is_task, "suspending task (0x%x)..", p_cmd->data.task.p_task);

         appl_perfmeas__start("calling tn_task_suspend", p_task_name);
         *p_rc = tn_task_suspend(p_cmd->data.task.p_task);
         appl_perfmeas__end("exiting from tn_task_suspend", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "task (0x%x) suspended", p_cmd->data.task.p_task);
         } else {
            _LOG_INF(is_task, "task (0x%x) suspension failed with err=%d", p_cmd->data.task.p_task, *p_rc);
         }
         break;

      case TWORKMAN_TASK_CMD__TASK_RESUME:
         bool_handled = true;
         _LOG_INF(is_task, "resuming task (0x%x)..", p_cmd->data.task.p_task);

         appl_perfmeas__start("calling tn_task_resume", p_task_name);
         *p_rc = tn_task_resume(p_cmd->data.task.p_task);
         appl_perfmeas__end("exiting from tn_task_resume", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "task (0x%x) resumed", p_cmd->data.task.p_task);
         } else {
            _LOG_INF(is_task, "task (0x%x) resuming failed with err=%d", p_cmd->data.task.p_task, *p_rc);
         }
         break;

      case TWORKMAN_TASK_CMD__TASK_WAKEUP:
         bool_handled = true;
         _LOG_INF(is_task, "wake task (0x%x) up..", p_cmd->data.task.p_task);

         appl_perfmeas__start("calling tn_task_wakeup", p_task_name);
         *p_rc = tn_task_wakeup(p_cmd->data.task.p_task);
         appl_perfmeas__end("exiting from tn_task_wakeup", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "task (0x%x) woken up", p_cmd->data.task.p_task);
         } else {
            _LOG_INF(is_task, "task (0x%x) wake up failed with err=%d", p_cmd->data.task.p_task, *p_rc);
         }
         break;

      case TWORKMAN_TASK_CMD__TASK_RELEASE_WAIT:
         bool_handled = true;
         _LOG_INF(is_task, "releasing task (0x%x) from wait..", p_cmd->data.task.p_task);

         appl_perfmeas__start("calling tn_task_release_wait", p_task_name);
         *p_rc = tn_task_release_wait(p_cmd->data.task.p_task);
         appl_perfmeas__end("exiting from tn_task_release_wait", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "task (0x%x) released from wait", p_cmd->data.task.p_task);
         } else {
            _LOG_INF(is_task, "task (0x%x) releasing failed with err=%d", p_cmd->data.task.p_task, *p_rc);
         }
         break;

      case TWORKMAN_TASK_CMD__TASK_SLEEP:
         bool_handled = true;
         _LOG_INF(is_task, "calling tn_task_sleep(%d)", p_cmd->data.sleep.timeout);
         appl_perfmeas__start("calling tn_task_sleep", p_task_name);
         *p_rc = tn_task_sleep(p_cmd->data.sleep.timeout);
         appl_perfmeas__end("exiting from tn_task_sleep", p_task_name);
         _LOG_INF(is_task, "exited from tn_task_sleep");
         break;


      case TWORKMAN_TASK_CMD__FMP_GET:
         bool_handled = true;
         _LOG_INF(is_task, "getting mem from fmp (0x%x)..", p_cmd->data.fmp.p_fmp);

         appl_perfmeas__start("calling tn_fmem_get", p_task_name);
         *p_rc = tn_fmem_get(p_cmd->data.fmp.p_fmp, p_cmd->data.fmp.p_data, p_cmd->data.fmp.timeout);
         appl_perfmeas__end("exiting from tn_fmem_get", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "got mem (0x%x) from fmp (0x%x)", *(unsigned int *)p_cmd->data.fmp.p_data, p_cmd->data.fmp.p_fmp);
         } else {
            _LOG_INF(is_task, "failed to get mem from fmp (0x%x), err=%d", p_cmd->data.fmp.p_fmp, *p_rc);
         }

         break;

      case TWORKMAN_TASK_CMD__FMP_GET_POLLING:
         bool_handled = true;
         _LOG_INF(is_task, "getting mem from fmp, polling (0x%x)..", p_cmd->data.fmp.p_fmp);

         appl_perfmeas__start("calling tn_fmem_get_polling", p_task_name);
         *p_rc = tn_fmem_get_polling(p_cmd->data.fmp.p_fmp, p_cmd->data.fmp.p_data);
         appl_perfmeas__end("exiting from tn_fmem_get_polling", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "got mem (0x%x) from fmp (0x%x)", *(unsigned int *)p_cmd->data.fmp.p_data, p_cmd->data.fmp.p_fmp);
         } else {
            _LOG_INF(is_task, "failed to get mem from fmp (0x%x), err=%d", p_cmd->data.fmp.p_fmp, *p_rc);
         }

         break;

      case TWORKMAN_TASK_CMD__FMP_RELEASE:
         bool_handled = true;
         _LOG_INF(is_task, "release mem to fmp (0x%x)..", p_cmd->data.fmp.p_fmp);

         appl_perfmeas__start("calling tn_fmem_release", p_task_name);
         *p_rc = tn_fmem_release(p_cmd->data.fmp.p_fmp, p_cmd->data.fmp.p_data);
         appl_perfmeas__end("exiting from tn_fmem_release", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "released mem (0x%x) to fmp (0x%x)", p_cmd->data.fmp.p_data, p_cmd->data.fmp.p_fmp);
         } else {
            _LOG_INF(is_task, "failed to release mem to fmp (0x%x), err=%d", p_cmd->data.fmp.p_fmp, *p_rc);
         }
         break;

      case TWORKMAN_TASK_CMD__DQUEUE_SEND:
         bool_handled = true;
         _LOG_INF(is_task, "send data to dqueue (0x%x)..", p_cmd->data.dqueue.p_dqueue);

         appl_perfmeas__start("calling tn_queue_send", p_task_name);
         *p_rc = tn_queue_send(
               p_cmd->data.dqueue.p_dqueue,
               p_cmd->data.dqueue.p_data,
               p_cmd->data.dqueue.timeout
               );
         appl_perfmeas__end("exiting from tn_queue_send", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "data (0x%x) sent to dqueue (0x%x)", p_cmd->data.dqueue.p_data, p_cmd->data.dqueue.p_dqueue);
         } else {
            _LOG_INF(is_task, "failed to send data to dqueue (0x%x), err=%d", p_cmd->data.dqueue.p_dqueue, *p_rc);
         }
         break;
      case TWORKMAN_TASK_CMD__DQUEUE_SEND_POLLING:
         bool_handled = true;
         _LOG_INF(is_task, "send_polling data to dqueue (0x%x)..", p_cmd->data.dqueue.p_dqueue);

         appl_perfmeas__start("calling tn_queue_send_polling", p_task_name);
         *p_rc = tn_queue_send_polling(
               p_cmd->data.dqueue.p_dqueue,
               p_cmd->data.dqueue.p_data
               );
         appl_perfmeas__end("exiting from tn_queue_send_polling", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "data (0x%x) sent_polling to dqueue (0x%x)", p_cmd->data.dqueue.p_data, p_cmd->data.dqueue.p_dqueue);
         } else {
            _LOG_INF(is_task, "failed to send_polling data to dqueue (0x%x), err=%d", p_cmd->data.dqueue.p_dqueue, *p_rc);
         }
         break;
      case TWORKMAN_TASK_CMD__DQUEUE_RECEIVE:
         bool_handled = true;
         _LOG_INF(is_task, "receive data from dqueue (0x%x)..", p_cmd->data.dqueue.p_dqueue);

         appl_perfmeas__start("calling tn_queue_receive", p_task_name);
         *p_rc = tn_queue_receive(
               p_cmd->data.dqueue.p_dqueue,
               p_cmd->data.dqueue.p_data,
               p_cmd->data.dqueue.timeout
               );
         appl_perfmeas__end("exiting from tn_queue_receive", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "data (0x%x) received from dqueue (0x%x)", *(unsigned int *)p_cmd->data.dqueue.p_data, p_cmd->data.dqueue.p_dqueue);
         } else {
            _LOG_INF(is_task, "failed to receive data from dqueue (0x%x), err=%d", p_cmd->data.dqueue.p_dqueue, *p_rc);
         }
         break;
      case TWORKMAN_TASK_CMD__DQUEUE_RECEIVE_POLLING:
         bool_handled = true;
         _LOG_INF(is_task, "receive_polling data from dqueue (0x%x)..", p_cmd->data.dqueue.p_dqueue);

         appl_perfmeas__start("calling tn_queue_receive_polling", p_task_name);
         *p_rc = tn_queue_receive_polling(
               p_cmd->data.dqueue.p_dqueue,
               p_cmd->data.dqueue.p_data
               );
         appl_perfmeas__end("exiting from tn_queue_receive_polling", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "data (0x%x) received_polling from dqueue (0x%x)", *(unsigned int *)p_cmd->data.dqueue.p_data, p_cmd->data.dqueue.p_dqueue);
         } else {
            _LOG_INF(is_task, "failed to receive_polling data from dqueue (0x%x), err=%d", p_cmd->data.dqueue.p_dqueue, *p_rc);
         }
         break;
      case TWORKMAN_TASK_CMD__DQUEUE_EVENTGRP_CONNECT:
         bool_handled = true;
         _LOG_INF(is_task, "connecting eventgrp (0x%x) with pattern 0x%x to dqueue (0x%x)..",
               p_cmd->data.dqueue.p_eventgrp,
               p_cmd->data.dqueue.pattern,
               p_cmd->data.dqueue.p_dqueue
               );

         appl_perfmeas__start("calling tn_queue_eventgrp_connect", p_task_name);
         *p_rc = tn_queue_eventgrp_connect(
               p_cmd->data.dqueue.p_dqueue,
               p_cmd->data.dqueue.p_eventgrp,
               p_cmd->data.dqueue.pattern
               );
         appl_perfmeas__end("exiting from tn_queue_eventgrp_connect", p_task_name);

         if (*p_rc == TN_RC_OK){
            _LOG_INF(is_task, "eventgrp (0x%x) with pattern 0x%x connected to dqueue (0x%x)",
                  *(unsigned int *)p_cmd->data.dqueue.p_eventgrp,
                  p_cmd->data.dqueue.pattern,
                  *(unsigned int *)p_cmd->data.dqueue.p_dqueue 
                  );
         } else {
            _LOG_INF(is_task, "failed to connect eventgrp (0x%x) with pattern 0x%x to dqueue (0x%x)",
                  *(unsigned int *)p_cmd->data.dqueue.p_eventgrp,
                  p_cmd->data.dqueue.pattern,
                  *(unsigned int *)p_cmd->data.dqueue.p_dqueue 
                  );
         }
         break;
      case TWORKMAN_TASK_CMD__DQUEUE_EVENTGRP_DISCONNECT:
         bool_handled = true;
         _LOG_INF(is_task, "disconnecting eventgrp (if any) from dqueue (0x%x)..",
               p_cmd->data.dqueue.p_dqueue
               );

         appl_perfmeas__start("calling tn_queue_eventgrp_disconnect", p_task_name);
         *p_rc = tn_queue_eventgrp_disconnect(
               p_cmd->data.dqueue.p_dqueue
               );
         appl_perfmeas__end("exiting from tn_queue_eventgrp_disconnect", p_task_name);

         if (*p_rc == TN_RC_OK){
            _LOG_INF(is_task, "eventgrp (if any) disconnected from dqueue (0x%x)",
                  *(unsigned int *)p_cmd->data.dqueue.p_dqueue 
                  );
         } else {
            _LOG_INF(is_task, "failed to disconnect eventgrp (if any) from dqueue (0x%x)",
                  *(unsigned int *)p_cmd->data.dqueue.p_dqueue 
                  );
         }
         break;



      case TWORKMAN_TASK_CMD__SEM_SIGNAL:
         bool_handled = true;
         _LOG_INF(is_task, "signaling sem (0x%x)..", p_cmd->data.sem.p_sem);

         appl_perfmeas__start("calling tn_sem_signal", p_task_name);
         *p_rc = tn_sem_signal(p_cmd->data.sem.p_sem);
         appl_perfmeas__end("exiting from tn_sem_signal", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "sem (0x%x) signaled", p_cmd->data.sem.p_sem);
         } else {
            _LOG_INF(is_task, "sem (0x%x) signaling failed with err=%d", p_cmd->data.sem.p_sem, *p_rc);
         }
         break;
      case TWORKMAN_TASK_CMD__SEM_ACQUIRE:
         bool_handled = true;
         _LOG_INF(is_task, "acquiring sem (0x%x)..", p_cmd->data.sem.p_sem);

         appl_perfmeas__start("calling tn_sem_acquire", p_task_name);
         *p_rc = tn_sem_acquire(p_cmd->data.sem.p_sem, p_cmd->data.sem.timeout);
         appl_perfmeas__end("exiting from tn_sem_acquire", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "sem (0x%x) acquired", p_cmd->data.sem.p_sem);
         } else {
            _LOG_INF(is_task, "sem (0x%x) acquiring failed with err=%d", p_cmd->data.sem.p_sem, *p_rc);
         }
         break;
      case TWORKMAN_TASK_CMD__SEM_ACQUIRE_POLLING:
         bool_handled = true;
         _LOG_INF(is_task, "acquiring_polling sem (0x%x)..", p_cmd->data.sem.p_sem);

         appl_perfmeas__start("calling tn_sem_polling", p_task_name);
         *p_rc = tn_sem_polling(p_cmd->data.sem.p_sem);
         appl_perfmeas__end("exiting from tn_sem_polling", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "sem (0x%x) acquired_polling", p_cmd->data.sem.p_sem);
         } else {
            _LOG_INF(is_task, "sem (0x%x) acquiring_polling failed with err=%d", p_cmd->data.sem.p_sem, *p_rc);
         }
         break;




      case TWORKMAN_TASK_CMD__EVENT_WAIT:
         bool_handled = true;
         _LOG_INF(is_task, "waiting event (0x%x)..", p_cmd->data.event.p_event);

         appl_perfmeas__start("calling tn_eventgrp_wait", p_task_name);
         *p_rc = tn_eventgrp_wait(
               p_cmd->data.event.p_event,
               p_cmd->data.event.wait_pattern,
               p_cmd->data.event.wait_mode,
               p_cmd->data.event.p_flags_pattern,
               p_cmd->data.event.timeout
               );
         appl_perfmeas__end("exiting from tn_eventgrp_wait", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "done waiting for event (0x%x)", p_cmd->data.event.p_event);
         } else {
            _LOG_INF(is_task, "waiting for event (0x%x) failed with err=%d", p_cmd->data.event.p_event, *p_rc);
         }
         break;
      case TWORKMAN_TASK_CMD__EVENT_WAIT_POLLING:
         bool_handled = true;
         _LOG_INF(is_task, "waiting_polling event (0x%x)..", p_cmd->data.event.p_event);

         appl_perfmeas__start("calling tn_eventgrp_wait_polling", p_task_name);
         *p_rc = tn_eventgrp_wait_polling(
               p_cmd->data.event.p_event,
               p_cmd->data.event.wait_pattern,
               p_cmd->data.event.wait_mode,
               p_cmd->data.event.p_flags_pattern
               );
         appl_perfmeas__end("exiting from tn_eventgrp_wait_polling", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "done waiting_polling for event (0x%x)", p_cmd->data.event.p_event);
         } else {
            _LOG_INF(is_task, "waiting_polling for event (0x%x) failed with err=%d", p_cmd->data.event.p_event, *p_rc);
         }
         break;
      case TWORKMAN_TASK_CMD__EVENT_MODIFY:
         bool_handled = true;
         _LOG_INF(is_task, "modifying event (0x%x) : op=%d, pattern=%d",
               p_cmd->data.event.p_event,
               p_cmd->data.event.operation,
               p_cmd->data.event.wait_pattern
               );

         appl_perfmeas__start("calling tn_eventgrp_modify", p_task_name);
         *p_rc = tn_eventgrp_modify(
               p_cmd->data.event.p_event,
               p_cmd->data.event.operation,
               p_cmd->data.event.wait_pattern
               );
         appl_perfmeas__end("exiting from tn_eventgrp_modify", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "done modifying event (0x%x)", p_cmd->data.event.p_event);
         } else {
            _LOG_INF(is_task, "modifying event (0x%x) failed with err=%d", p_cmd->data.event.p_event, *p_rc);
         }
         break;
#if TN_OLD_EVENT_API
      case TWORKMAN_TASK_CMD__EVENT_MODIFY_OLD_TNKERNEL:
         bool_handled = true;
         _LOG_INF(is_task, "modifying event (in old fashion) (0x%x) : op=%d, pattern=%d",
               p_cmd->data.event.p_event,
               p_cmd->data.event.operation,
               p_cmd->data.event.wait_pattern
               );


         switch (p_cmd->data.event.operation){
            case TN_EVENTGRP_OP_CLEAR:
               appl_perfmeas__start("calling tn_event_clear", p_task_name);
               *p_rc = tn_event_clear(
                     p_cmd->data.event.p_event,
                     p_cmd->data.event.wait_pattern
                     );
               break;
            case TN_EVENTGRP_OP_SET:
               appl_perfmeas__start("calling tn_event_set", p_task_name);
               *p_rc = tn_event_set(
                     p_cmd->data.event.p_event,
                     p_cmd->data.event.wait_pattern
                     );
               break;
            default:
               _LOG_INF(is_task, "ERROR: wrong op=%d", p_cmd->data.event.operation);
               break;
         }

         appl_perfmeas__end("exiting from tn_event_..", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "done modifying event (0x%x)", p_cmd->data.event.p_event);
         } else {
            _LOG_INF(is_task, "modifying event (0x%x) failed with err=%d", p_cmd->data.event.p_event, *p_rc);
         }
         break;
#endif




      case TWORKMAN_TASK_CMD__TASK_IACTIVATE:
         bool_handled = true;
         _LOG_INF(is_task, "activate task (0x%x)..", p_cmd->data.task.p_task);

         appl_perfmeas__start("calling tn_task_iactivate", p_task_name);
         *p_rc = tn_task_iactivate(p_cmd->data.task.p_task);
         appl_perfmeas__end("exiting from tn_task_iactivate", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "task (0x%x) activated", p_cmd->data.task.p_task);
         } else {
            _LOG_INF(is_task, "task (0x%x) activation failed with err=%d", p_cmd->data.task.p_task, *p_rc);
         }
         break;
      case TWORKMAN_TASK_CMD__TASK_IWAKEUP:
         bool_handled = true;
         _LOG_INF(is_task, "wake task (0x%x) up..", p_cmd->data.task.p_task);

         appl_perfmeas__start("calling tn_task_iwakeup", p_task_name);
         *p_rc = tn_task_iwakeup(p_cmd->data.task.p_task);
         appl_perfmeas__end("exiting from tn_task_iwakeup", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "task (0x%x) woken up", p_cmd->data.task.p_task);
         } else {
            _LOG_INF(is_task, "task (0x%x) wake up failed with err=%d", p_cmd->data.task.p_task, *p_rc);
         }
         break;
      case TWORKMAN_TASK_CMD__TASK_IRELEASE_WAIT:
         bool_handled = true;
         _LOG_INF(is_task, "releasing task (0x%x) from wait..", p_cmd->data.task.p_task);

         appl_perfmeas__start("calling tn_task_irelease_wait", p_task_name);
         *p_rc = tn_task_irelease_wait(p_cmd->data.task.p_task);
         appl_perfmeas__end("exiting from tn_task_irelease_wait", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "task (0x%x) released from wait", p_cmd->data.task.p_task);
         } else {
            _LOG_INF(is_task, "task (0x%x) releasing failed with err=%d", p_cmd->data.task.p_task, *p_rc);
         }
         break;

      case TWORKMAN_TASK_CMD__FMP_GET_IPOLLING:
         bool_handled = true;
         _LOG_INF(is_task, "getting mem from fmp, polling (0x%x)..", p_cmd->data.fmp.p_fmp);

         appl_perfmeas__start("calling tn_fmem_iget_polling", p_task_name);
         *p_rc = tn_fmem_iget_polling(p_cmd->data.fmp.p_fmp, p_cmd->data.fmp.p_data);
         appl_perfmeas__end("exiting from tn_fmem_iget_polling", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "got mem (0x%x) from fmp (0x%x)", *(unsigned int *)p_cmd->data.fmp.p_data, p_cmd->data.fmp.p_fmp);
         } else {
            _LOG_INF(is_task, "failed to get mem from fmp (0x%x), err=%d", p_cmd->data.fmp.p_fmp, *p_rc);
         }
         break;
      case TWORKMAN_TASK_CMD__FMP_IRELEASE:
         bool_handled = true;
         _LOG_INF(is_task, "release mem to fmp (0x%x)..", p_cmd->data.fmp.p_fmp);

         appl_perfmeas__start("calling tn_fmem_irelease", p_task_name);
         *p_rc = tn_fmem_irelease(p_cmd->data.fmp.p_fmp, p_cmd->data.fmp.p_data);
         appl_perfmeas__end("exiting from tn_fmem_irelease", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "released mem (0x%x) to fmp (0x%x)", p_cmd->data.fmp.p_data, p_cmd->data.fmp.p_fmp);
         } else {
            _LOG_INF(is_task, "failed to release mem to fmp (0x%x), err=%d", p_cmd->data.fmp.p_fmp, *p_rc);
         }
         break;

      case TWORKMAN_TASK_CMD__DQUEUE_ISEND_POLLING:
         bool_handled = true;
         _LOG_INF(is_task, "send_polling data to dqueue (0x%x)..", p_cmd->data.dqueue.p_dqueue);

         appl_perfmeas__start("calling tn_queue_isend_polling", p_task_name);
         *p_rc = tn_queue_isend_polling(
               p_cmd->data.dqueue.p_dqueue,
               p_cmd->data.dqueue.p_data
               );
         appl_perfmeas__end("exiting from tn_queue_isend_polling", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "data (0x%x) sent_polling to dqueue (0x%x)", p_cmd->data.dqueue.p_data, p_cmd->data.dqueue.p_dqueue);
         } else {
            _LOG_INF(is_task, "failed to send_polling data to dqueue (0x%x), err=%d", p_cmd->data.dqueue.p_dqueue, *p_rc);
         }
         break;
      case TWORKMAN_TASK_CMD__DQUEUE_IRECEIVE_POLLING:
         bool_handled = true;
         _LOG_INF(is_task, "receive_polling data from dqueue (0x%x)..", p_cmd->data.dqueue.p_dqueue);

         appl_perfmeas__start("calling tn_queue_ireceive_polling", p_task_name);
         *p_rc = tn_queue_ireceive_polling(
               p_cmd->data.dqueue.p_dqueue,
               p_cmd->data.dqueue.p_data
               );
         appl_perfmeas__end("exiting from tn_queue_ireceive_polling", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "data (0x%x) received_polling from dqueue (0x%x)", *(unsigned int *)p_cmd->data.dqueue.p_data, p_cmd->data.dqueue.p_dqueue);
         } else {
            _LOG_INF(is_task, "failed to receive_polling data from dqueue (0x%x), err=%d", p_cmd->data.dqueue.p_dqueue, *p_rc);
         }
         break;

      case TWORKMAN_TASK_CMD__SEM_ISIGNAL:
         bool_handled = true;
         _LOG_INF(is_task, "signaling sem (0x%x)..", p_cmd->data.sem.p_sem);

         appl_perfmeas__start("calling tn_sem_isignal", p_task_name);
         *p_rc = tn_sem_isignal(p_cmd->data.sem.p_sem);
         appl_perfmeas__end("exiting from tn_sem_isignal", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "sem (0x%x) signaled", p_cmd->data.sem.p_sem);
         } else {
            _LOG_INF(is_task, "sem (0x%x) signaling failed with err=%d", p_cmd->data.sem.p_sem, *p_rc);
         }
         break;
      case TWORKMAN_TASK_CMD__SEM_IACQUIRE_POLLING:
         bool_handled = true;
         _LOG_INF(is_task, "acquiring sem (0x%x)..", p_cmd->data.sem.p_sem);

         appl_perfmeas__start("calling tn_sem_iacquire", p_task_name);
         *p_rc = tn_sem_iacquire_polling(p_cmd->data.sem.p_sem);
         appl_perfmeas__end("exiting from tn_sem_iacquire", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "sem (0x%x) acquired", p_cmd->data.sem.p_sem);
         } else {
            _LOG_INF(is_task, "sem (0x%x) acquiring failed with err=%d", p_cmd->data.sem.p_sem, *p_rc);
         }
         break;

      case TWORKMAN_TASK_CMD__EVENT_IWAIT:
         bool_handled = true;
         _LOG_INF(is_task, "waiting event (0x%x)..", p_cmd->data.event.p_event);

         appl_perfmeas__start("calling tn_eventgrp_iwait", p_task_name);
         *p_rc = tn_eventgrp_iwait_polling(
               p_cmd->data.event.p_event,
               p_cmd->data.event.wait_pattern,
               p_cmd->data.event.wait_mode,
               p_cmd->data.event.p_flags_pattern
               );
         appl_perfmeas__end("exiting from tn_eventgrp_iwait", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "done waiting_polling for event (0x%x)", p_cmd->data.event.p_event);
         } else {
            _LOG_INF(is_task, "waiting_polling for event (0x%x) failed with err=%d", p_cmd->data.event.p_event, *p_rc);
         }
         break;
      case TWORKMAN_TASK_CMD__EVENT_IMODIFY:
         bool_handled = true;
         _LOG_INF(is_task, "modifying event (0x%x) : op=%d, pattern=%d",
               p_cmd->data.event.p_event,
               p_cmd->data.event.operation,
               p_cmd->data.event.wait_pattern
               );

         appl_perfmeas__start("calling tn_eventgrp_imodify", p_task_name);
         *p_rc = tn_eventgrp_imodify(
               p_cmd->data.event.p_event,
               p_cmd->data.event.operation,
               p_cmd->data.event.wait_pattern
               );
         appl_perfmeas__end("exiting from tn_eventgrp_imodify", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "done modifying event (0x%x)", p_cmd->data.event.p_event);
         } else {
            _LOG_INF(is_task, "modifying event (0x%x) failed with err=%d", p_cmd->data.event.p_event, *p_rc);
         }
         break;

#if TN_OLD_EVENT_API
      case TWORKMAN_TASK_CMD__EVENT_IMODIFY_OLD_TNKERNEL:
         bool_handled = true;
         _LOG_INF(is_task, "modifying event (in old fashion) (0x%x) : op=%d, pattern=%d",
               p_cmd->data.event.p_event,
               p_cmd->data.event.operation,
               p_cmd->data.event.wait_pattern
               );


         switch (p_cmd->data.event.operation){
            case TN_EVENTGRP_OP_CLEAR:
               appl_perfmeas__start("calling tn_event_iclear", p_task_name);
               *p_rc = tn_event_iclear(
                     p_cmd->data.event.p_event,
                     p_cmd->data.event.wait_pattern
                     );
               break;
            case TN_EVENTGRP_OP_SET:
               appl_perfmeas__start("calling tn_event_iset", p_task_name);
               *p_rc = tn_event_iset(
                     p_cmd->data.event.p_event,
                     p_cmd->data.event.wait_pattern
                     );
               break;
            default:
               _LOG_INF(is_task, "ERROR: wrong op=%d", p_cmd->data.event.operation);
               break;
         }

         appl_perfmeas__end("exiting from tn_event_..", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "done modifying event (0x%x)", p_cmd->data.event.p_event);
         } else {
            _LOG_INF(is_task, "modifying event (0x%x) failed with err=%d", p_cmd->data.event.p_event, *p_rc);
         }
         break;
#endif

      case TWORKMAN_TASK_CMD__SYS_TSLICE_SET:
         bool_handled = true;
         _LOG_INF(is_task, "setting time slice: priority=%ld, value=%ld",
               p_cmd->data.sys.p1,
               p_cmd->data.sys.p2
               );

         appl_perfmeas__start("calling tn_sys_tslice_set", p_task_name);
         *p_rc = tn_sys_tslice_set(
               p_cmd->data.sys.p1,
               p_cmd->data.sys.p2
               );
         appl_perfmeas__end("exiting from tn_sys_tslice_set", p_task_name);

         if (*p_rc == TERR_NO_ERR){
            _LOG_INF(is_task, "time slice is set");
         } else {
            _LOG_INF(is_task, "failed to set time slice (err=%d)", *p_rc);
         }
         break;
   }

   if (!bool_handled){
      TNT_DEB_HALT("cmd not handled, cmd=%d", (int)p_cmd->cmd);
   }
}


static void _task_worker (void *p_param)
{
   SYSRETVAL_DATA;
   struct TWorkMan_TaskData *p_task_data = (struct TWorkMan_TaskData *)p_param;

   struct TWorkMan_TaskCmd cmd;

   struct TWorkMan_TaskCmd *p_cmd;

   enum TN_RCode tn_res = TERR_NO_ERR;

   _LOG_INF(true, "started");

   //-- first of all, clear 'bool_need_return' flag
   //   (without this, if task exited and then activated, it exits 
   //   again immediately)
   p_task_data->bool_need_return = false;

   while (!p_task_data->bool_need_return){
      _LOG_INF(true, "waiting for command..");

      switch (p_task_data->exmode){
         case TWORKMAN_EXMODE__WAIT:

            appl_perfmeas__start("calling tn_queue_receive", _TASK_NAME);
            SYSRETVAL_CHECK(
                  tn_res = tn_queue_receive(&p_task_data->msg_queue, (void *)&p_cmd, TN_WAIT_INFINITE)
                  );
            appl_perfmeas__end("exiting from tn_queue_receive", _TASK_NAME);

            break;
         case TWORKMAN_EXMODE__POLLING:

            do {
               SYSRETVAL_CHECK_TOUT(
                     tn_res = tn_queue_receive_polling(
                        &p_task_data->msg_queue, (void *)&p_cmd
                        )
                     );
            } while (tn_res == TERR_TIMEOUT);

            break;
         default:
            TNT_DEB_HALT("wrong exmode=%d", p_task_data->exmode);
            break;
      }

      if (tn_res == TERR_NO_ERR){
         //-- message received

         memcpy(&cmd, p_cmd, sizeof(cmd));
         tworker_man__cmd_mem__free(p_cmd);

         p_task_data->last_retval = TWORKER_MAN__LAST_RETVAL__UNKNOWN;
         
         _cmd_handle(&cmd, p_task_data);


      }
   }

   _LOG_INF(true, "returning from task function..");

}


/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/



void tworker_man__init(void)
{
   SYSRETVAL_DATA;
   dllist__reset(&_tasks_list);

   //-- create memory pool for struct TWorkMan_TaskCmd

   SYSRETVAL_CHECK(tn_fmem_create(
            &fmp_cmd_msg,
            (void *)fmp_cmd_msg_buf,
            TN_MAKE_ALIG_SIZE(sizeof(struct TWorkMan_TaskCmd)),
            CMD_FMP_MSG_BUF_SIZE
            ));
   SYSRETVAL_CHECK(tn_fmem_create(
            &fmp_int_cmd_msg,
            (void *)fmp_int_cmd_msg_buf,
            TN_MAKE_ALIG_SIZE(sizeof(struct TWorkMan_TaskCmd)),
            INT_CMD_FMP_MSG_BUF_SIZE
            ));

   //-- create message queue for int1
   SYSRETVAL_CHECK(
         tn_queue_create(&_int1_queue, _int1_queue_buf, _INT1_QUEUE_SIZE)
         );

   //-- init architecture-dependent part
   TNT_TWORKER_MAN_ARCH_INIT();
}

void tworker_man__int_handler(void)
{
   SYSRETVAL_DATA;
   struct TWorkMan_TaskCmd *p_cmd = NULL;

   if (SYSRETVAL_CHECK_TOUT(tn_queue_ireceive(&_int1_queue, (void *)&p_cmd))
         == TERR_NO_ERR)
   {
      //-- message received

      _int1_cmd = *p_cmd;
      tworker_man__int_cmd_mem__ifree(p_cmd);


      TNT_DTMSG_I_INT("int: msg received");

      _cmd_handle(&_int1_cmd, NULL);
   }
}

struct TN_DQueue *tworker_man__int_queue__get(void)
{
   return &_int1_queue;
}

struct TWorkMan_TaskCmd *tworker_man__cmd_mem__alloc(void)
{
   SYSRETVAL_DATA;
   struct TWorkMan_TaskCmd *p_ret = NULL;

   SYSRETVAL_CHECK(tn_fmem_get(&fmp_cmd_msg, (void*)&p_ret, 0));

   return p_ret;
}

struct TWorkMan_TaskCmd *tworker_man__int_cmd_mem__alloc(void)
{
   SYSRETVAL_DATA;
   struct TWorkMan_TaskCmd *p_ret = NULL;

   SYSRETVAL_CHECK(tn_fmem_get(&fmp_int_cmd_msg, (void*)&p_ret, 0));

   return p_ret;
}

int tworker_man__int_attr__get(enum TNT_InterruptAttr interrupt_attr)
{
   int ret = 0;
   bool bool_handled = false;

   switch (interrupt_attr){
      case TNT_INTERRUPT_ATTR__LAST_RETVAL:
         bool_handled = true;
         ret = _int1_last_retval;
         break;

      case TNT_INTERRUPT_ATTRS_CNT:
         break;
   }

   if (!bool_handled){
      TNT_DEB_HALT("bool_handled should be true");
   }

   return ret;
}

void tworker_man__cmd_mem__free(struct TWorkMan_TaskCmd *p_cmd)
{
   SYSRETVAL_DATA;
   SYSRETVAL_CHECK(tn_fmem_release(&fmp_cmd_msg, p_cmd));
}

void tworker_man__cmd_mem__ifree(struct TWorkMan_TaskCmd *p_cmd)
{
   SYSRETVAL_DATA;
   SYSRETVAL_CHECK(tn_fmem_irelease(&fmp_cmd_msg, p_cmd));
}

void tworker_man__int_cmd_mem__free(struct TWorkMan_TaskCmd *p_cmd)
{
   SYSRETVAL_DATA;
   SYSRETVAL_CHECK(tn_fmem_release(&fmp_int_cmd_msg, p_cmd));
}

void tworker_man__int_cmd_mem__ifree(struct TWorkMan_TaskCmd *p_cmd)
{
   SYSRETVAL_DATA;
   SYSRETVAL_CHECK(tn_fmem_irelease(&fmp_int_cmd_msg, p_cmd));
}

struct TWorkMan_TaskData *tworker_man__worker__create(
      struct TWorkMan_TaskParam *p_task_param,
      int priority,
      int stack_size       //-- if 0, default value will be used
      )
{
   SYSRETVAL_DATA;

   //-- allocate task data and zero it
   //   (at least, struct TN_Task should be zeroed before calling tn_task_create)
   struct TWorkMan_TaskData *p_tdata = TNT_MALLOC(sizeof(*p_tdata));
   memset(p_tdata, 0x00, sizeof(*p_tdata));

   //-- add it to common list
   dllist__item__add_tail(&_tasks_list, &p_tdata->list_node);

   //-- if stack_size isn't specified, use default value
   if (stack_size == 0){
      stack_size = TASK_WORKER_STACK_SIZE_DEF;
   }

   //-- fill task data with given params
   p_tdata->stack_size     = stack_size;
   p_tdata->priority       = priority;
   p_tdata->p_task_param   = p_task_param;

   //-- allocate stack for that task
   p_tdata->p_stack        = TNT_MALLOC(stack_size * sizeof(p_tdata->p_stack[0]));

   //-- allocate buffer for msg queue
   p_tdata->pp_msg_queue_buf = TNT_MALLOC(_TASK_WORKER_QUEUE_SIZE * sizeof(p_tdata->pp_msg_queue_buf[0]));

   //-- create message queue
   SYSRETVAL_CHECK(tn_queue_create(&p_tdata->msg_queue, p_tdata->pp_msg_queue_buf, _TASK_WORKER_QUEUE_SIZE));

   p_tdata->last_retval = TWORKER_MAN__LAST_RETVAL__UNKNOWN;
   p_tdata->exmode      = TWORKMAN_EXMODE__WAIT;

   //-- create task, after all
   SYSRETVAL_CHECK(tn_task_create(
            &p_tdata->task,
            _task_worker,
            p_tdata->priority,
            p_tdata->p_stack,
            p_tdata->stack_size,
            p_tdata,
            TN_TASK_START_ON_CREATION
            ));
   //-- set task name for debug

#if TN_DEBUG
   p_tdata->task.name = p_task_param->p_task_name;
#endif


   return p_tdata;

}

enum TN_RCode tworker_man__worker__terminate(
      struct TWorkMan_TaskData *p_tdata
      )
{
   return tn_task_terminate(&p_tdata->task);
}

enum TN_RCode tworker_man__worker__activate(
      struct TWorkMan_TaskData *p_tdata
      )
{
   return tn_task_activate(&p_tdata->task);
}

enum TN_RCode tworker_man__worker__delete(
      struct TWorkMan_TaskData *p_tdata
      )
{
   enum TN_RCode ret;
   ret = tn_task_delete(&p_tdata->task);

   if (ret == TERR_NO_ERR){
      if (p_tdata->p_task_param != NULL){
         TNT_FREE(p_tdata->p_task_param);
         p_tdata->p_task_param = NULL;
      }
      TNT_FREE(p_tdata->p_stack);
      TNT_FREE(p_tdata->pp_msg_queue_buf);
      TNT_FREE(p_tdata);
   }

   return ret;
}

/***************************************************************************************************
 *  end of file: task_dbg.c
 **************************************************************************************************/


