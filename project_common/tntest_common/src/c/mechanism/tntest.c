/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "tntest_cfg.h"
#include "tworker_man.h"
#include "tntest.h"
#include "utils/xprintf.h"

#include <string.h>
#include <stdarg.h>


#ifndef TN_MUTEX_REC
#  error TN_MUTEX_REC is not defined
#endif

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/


#define  _EXPL_STRING_BUF_SIZE         (100)//TODO: remove?
#define  _STRING_BUF_SIZE              (300)

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

struct _Data {
   struct TWorkMan_TaskData *p_worker_task[ TNT_TASKS_CNT ];
   enum TN_RCode last_retval;
};

struct _CheckInt {
   int         bool_check;
   int         value;
};

struct TNT_TestItem {

   T_DLListItem   list_node;  

   enum TNT_Action   action;

   union _TestItemData {

      //-- params for TNT_ACT__TASK_MANAGE
      struct _TestItemData_TaskManage {
         enum TNT_ObjAction   obj_action;
         enum TNT_TaskNum     task_num;
         int               priority;
      } task_manage;

      //-- params for TNT_ACT__MUTEX_MANAGE
      struct _TestItemData_MutexManage {
         enum TNT_ObjAction   obj_action;
         enum TNT_MutexNum    mutex_num;
         int               attribute;
         int               ceil_priority;
      } mutex_manage;

      //-- params for TNT_ACT__FMP_MANAGE
      struct _TestItemData_FmpManage {
         enum TNT_ObjAction   obj_action;
         enum TNT_FmpNum      fmp_num;
         void                *start_addr;
         unsigned int         block_size;
         int                  blocks_cnt;
      } fmp_manage;

      //-- params for TNT_ACT__DQUEUE_MANAGE
      struct _TestItemData_DQueueManage {
         enum TNT_ObjAction   obj_action;
         enum TNT_DQueueNum   dqueue_num;
         void               **data_fifo;
         int                  items_cnt;
      } dqueue_manage;

      //-- params for TNT_ACT__SEM_MANAGE
      struct _TestItemData_SemManage {
         enum TNT_ObjAction   obj_action;
         enum TNT_SemNum      sem_num;
         int                  start_value;
         int                  max_val;
      } sem_manage;

      //-- params for TNT_ACT__EVENT_MANAGE
      struct _TestItemData_EventManage {
         enum TNT_ObjAction   obj_action;
         enum TNT_EventNum    event_num;
         enum TN_EGrpAttr     attr;
         unsigned int         pattern;
      } event_manage;

#if 0
      //-- params for TNT_ACT__EXCH_MANAGE
      struct _TestItemData_ExchManage {
         enum TNT_ObjAction   obj_action;
         enum TNT_ExchNum     exch_num;
         TN_UWord            *data;
         unsigned int         size;
      } exch_manage;
#endif

      //-- params for TNT_ACT__SEND_CMD_TO_TASK
      struct _TestItemData_SendCmd {
         enum TNT_TaskNum              task_num;
         struct TWorkMan_TaskCmd    cmd;
      } send_cmd;

      //-- params for TNT_ACT__SEND_CMD_TO_INT
      struct _TestItemData_IntSendCmd {
         struct TWorkMan_TaskCmd     cmd;
      } int_send_cmd;

      //-- params for TNT_ACT__WAIT
      struct _TestItemData_Wait {
         TN_Timeout timeout;
         bool bool_ensure_dterm_finished;
      } wait;

      //-- params for TNT_ACT__CHECK
      struct _TestItemData_Check {
         struct {
            struct _CheckInt  attr[ TNT_TASK_ATTRS_CNT ];
         } task[ TNT_TASKS_CNT ];

         struct {
            struct _CheckInt  attr[ TNT_MUTEX_ATTRS_CNT ];
         } mutex[ TNT_MUTEXES_CNT ];

         struct {
            struct _CheckInt  attr[ TNT_FMP_ATTRS_CNT ];
         } fmp[ TNT_FMPS_CNT ];

         struct {
            struct _CheckInt  attr[ TNT_DQUEUE_ATTRS_CNT ];
         } dqueue[ TNT_DQUEUES_CNT ];

         struct {
            struct _CheckInt  attr[ TNT_SEM_ATTRS_CNT ];
         } sem[ TNT_SEMS_CNT ];

         struct {
            struct _CheckInt  attr[ TNT_EVENT_ATTRS_CNT ];
         } event[ TNT_EVENTS_CNT ];

#if 0
         struct {
            struct _CheckInt  attr[ TNT_EXCH_ATTRS_CNT ];
         } exch[ TNT_EXCHS_CNT ];
#endif

         struct {
            struct _CheckInt  attr[ TNT_SYS_ATTRS_CNT ];
         } sys;

         struct {
            struct _CheckInt  attr[ TNT_DIRECTOR_ATTRS_CNT ];
         } director;

         struct {
            struct _CheckInt  attr[ TNT_INTERRUPT_ATTRS_CNT ];
         } interrupt;

      } check;

   } data;
};



/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/



/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

static struct _Data _me;

static struct TN_Mutex  _mutex[ TNT_MUTEXES_CNT ];
static struct TN_FMem    _fmp[ TNT_FMPS_CNT ];
static struct TN_DQueue _dqueue[ TNT_DQUEUES_CNT ];
static struct TN_Sem    _sem[ TNT_SEMS_CNT ];
static struct TN_EventGrp _event[ TNT_EVENTS_CNT ];

#if 0
static struct TN_Exch      _exch[ TNT_EXCHS_CNT ];
#endif

static const char *_p_task_name[ TNT_TASKS_CNT + 1/*for 'none'*/ ] = {
   [ TNT_TASK__A ]        = "A",
   [ TNT_TASK__B ]        = "B",
   [ TNT_TASK__C ]        = "C",
   [ TNT_TASK__D ]        = "D",
   [ TNT_TASK__E ]        = "E",
   [ TNT_TASK__NONE ]     = "NONE",
};

static const char *_p_mutex_name[ TNT_MUTEXES_CNT + 1/*for 'none'*/ ] = {
   [ TNT_MUTEX__1 ]       = "M1",
   [ TNT_MUTEX__2 ]       = "M2",
   [ TNT_MUTEX__3 ]       = "M3",
   [ TNT_MUTEX__NONE ]    = "NONE",
};

static const char *_p_fmp_name[ TNT_FMPS_CNT + 1/*for 'none'*/ ] = {
   [ TNT_FMP__1 ]          = "FMP1",
   [ TNT_FMP__NONE ]       = "NONE",
};

static const char *_p_dqueue_name[ TNT_DQUEUES_CNT + 1/*for 'none'*/ ] = {
   [ TNT_DQUEUE__1 ]          = "DQ1",
   [ TNT_DQUEUE__2 ]          = "DQ2",
   [ TNT_DQUEUE__NONE ]       = "NONE",
};

static const char *_p_sem_name[ TNT_SEMS_CNT + 1/*for 'none'*/ ] = {
   [ TNT_SEM__1 ]          = "S1",
   [ TNT_SEM__NONE ]       = "NONE",
};

static const char *_p_event_name[ TNT_EVENTS_CNT + 1/*for 'none'*/ ] = {
   [ TNT_EVENT__1 ]          = "E1",
   [ TNT_EVENT__NONE ]       = "NONE",
};

#if 0
static const char *_p_exch_name[ TNT_EXCHS_CNT + 1/*for 'none'*/ ] = {
   [ TNT_EXCH__1 ]          = "Exch1",
   [ TNT_EXCH__NONE ]       = "NONE",
};
#endif

static const char *_p_exmode_name[ TWORKMAN_EXMODES_CNT ] = {
   [ TWORKMAN_EXMODE__POLLING ]  = "POLLING",
   [ TWORKMAN_EXMODE__WAIT ]     = "WAIT",
};

//static T_DLList _test_list = {};

static struct TNT_TestItem _prev_check_item;

static char _string_buf[ _STRING_BUF_SIZE ];
static int  _string_buf_index = 0;

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

const char *title_mark = "===========================";


/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

#if TN_MUTEX_DEADLOCK_DETECT
extern volatile int _tn_deadlocks_cnt;   //-- Count of active deadlocks
#endif



/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

//-- functions to work with static string buffer

static void _log_flush(void)
{
   _string_buf[0] = 0;
   _string_buf_index = 0;
}

static void _log_write(const char *fmt, ...)
{
   va_list ap;

   va_start(ap, fmt);
   _string_buf_index += xvsprintf((_string_buf + _string_buf_index), fmt, ap);
   va_end(ap);
}

static bool _log_echo_value(const char *name, int value_actual, int value_expected, 
      const char *(*p_int_to_string)(int value)
      )
{
   //-- test equality
   bool bool_ret = (value_actual == value_expected);

   //-- add separator if needed
   if (_string_buf_index > 0){
      _log_write(", ");
   }

   //-- write value
   if (p_int_to_string == NULL){
      //-- write plain int value
      _log_write("%s=%d ", name, value_actual);
   } else {
      //-- write string representation of int value
      _log_write("%s=%s ", name, p_int_to_string(value_actual));
   }

   //-- write about our expectations
   if (bool_ret){
      _log_write("(as expected)");
   } else {
      if (p_int_to_string == NULL){
         //-- write plain int value
         _log_write("(ERROR: expected=%d)", value_expected);
      } else {
         //-- write string representation of int value
         _log_write("(ERROR: expected=%s)", p_int_to_string(value_expected));
      }
   }

   return bool_ret;
}




//----------------------------


static const char *_task_state_name_get(int task_state)
{
   const char *p_ret = NULL;

   switch (task_state){
      case TSK_STATE_RUNNABLE:
         p_ret = "RUNNABLE";
         break;
      case TSK_STATE_WAIT:
         p_ret = "WAIT";
         break;
      case TSK_STATE_SUSPEND:
         p_ret = "SUSPEND";
         break;
      case TSK_STATE_WAITSUSP:
         p_ret = "SUSPENDED+WAIT";
         break;
      case TSK_STATE_DORMANT:
         p_ret = "DORMANT";
         break;
      default:
         TNT_DTMSG_W("unknown task_state=%d", task_state);
         p_ret = "--unknown--";
         break;
   }

   return p_ret;
}

static const char *_wait_reason_name_get(int /*enum TN_WaitReason*/ wait_reason)
{
   const char *p_ret = NULL;

   switch (wait_reason){
      case TSK_WAIT_REASON_NONE:
         p_ret = "NONE";
         break;
      case TSK_WAIT_REASON_SLEEP:
         p_ret = "SLEEP";
         break;
      case TSK_WAIT_REASON_SEM:
         p_ret = "SEM";
         break;
      case TSK_WAIT_REASON_EVENT:
         p_ret = "EVENT";
         break;
      case TSK_WAIT_REASON_DQUE_WSEND:
         p_ret = "DQUE_WSEND";
         break;
      case TSK_WAIT_REASON_DQUE_WRECEIVE:
         p_ret = "DQUE_WRECEIVE";
         break;
      case TSK_WAIT_REASON_MUTEX_C:
         p_ret = "MUTEX_C";
         break;
      case TSK_WAIT_REASON_MUTEX_I:
         p_ret = "MUTEX_I";
         break;
      case TSK_WAIT_REASON_WFIXMEM:
         p_ret = "WFIXMEM";
         break;
      default:
         TNT_DTMSG_W("unknown wait_reason=%d", wait_reason);
         p_ret = "--unknown--";
         break;
   }

   return p_ret;
}

static const char *_retval_name_get(int /*enum TN_RCode*/ retval)
{
   const char *p_ret = NULL;

   switch (retval){
      case TN_RC_OK:
         p_ret = "TN_RC_OK";
         break;
      case TN_RC_OVERFLOW:
         p_ret = "TN_RC_OVERFLOW";
         break;
      case TN_RC_WCONTEXT:
         p_ret = "TN_RC_WCONTEXT";
         break;
      case TN_RC_WSTATE:
         p_ret = "TN_RC_WSTATE";
         break;
      case TN_RC_TIMEOUT:
         p_ret = "TN_RC_TIMEOUT";
         break;
      case TN_RC_WPARAM:
         p_ret = "TN_RC_WPARAM";
         break;
      case TN_RC_ILLEGAL_USE:
         p_ret = "TN_RC_ILLEGAL_USE";
         break;
      case TN_RC_INVALID_OBJ:
         p_ret = "TN_RC_INVALID_OBJ";
         break;
      case TN_RC_DELETED:
         p_ret = "TN_RC_DELETED";
         break;
      case TN_RC_FORCED:
         p_ret = "TN_RC_FORCED";
         break;
      case TN_RC_INTERNAL:
         p_ret = "TN_RC_INTERNAL";
         break;
      case TWORKER_MAN__LAST_RETVAL__UNKNOWN:
         p_ret = "NOT-YET-RECEIVED";
         break;

      default:
         TNT_DTMSG_W("unknown retval=%d", retval);
         p_ret = "--unknown--";
         break;
   }

   return p_ret;
}

static struct TNT_TestItem *_test_item_create(void)
{
   struct TNT_TestItem *p_item = TNT_MALLOC(sizeof(*p_item));
   memset(p_item, 0x00, sizeof(*p_item));

   //dllist__item__add_tail(&_test_list, &p_item->list_node);

   return p_item;
}

static enum TNT_TaskNum _task_pt_to_task_num(const struct TN_Task *p_task)
{
   enum TNT_TaskNum ret = TNT_TASK__NONE;

   enum TNT_TaskNum i;
   for (i = (enum TNT_TaskNum)0; i < TNT_TASKS_CNT; i++){
      if (p_task == &_me.p_worker_task[i]->task){
         ret = i;
      }
   }

   return ret;
}

static enum TNT_MutexNum _mutex_pt_to_mutex_num(const struct TN_Mutex *p_mutex)
{
   enum TNT_MutexNum ret = TNT_MUTEX__NONE;

   enum TNT_MutexNum i;
   for (i = (enum TNT_MutexNum)0; i < TNT_MUTEXES_CNT; i++){
      if (p_mutex == &_mutex[i]){
         ret = i;
      }
   }

   return ret;
}

static enum TNT_FmpNum _fmp_pt_to_fmp_num(const struct TN_FMem *p_fmp)
{
   enum TNT_FmpNum ret = TNT_FMP__NONE;

   enum TNT_FmpNum i;
   for (i = (enum TNT_FmpNum)0; i < TNT_FMPS_CNT; i++){
      if (p_fmp == &_fmp[i]){
         ret = i;
      }
   }

   return ret;
}

static enum TNT_DQueueNum _dqueue_pt_to_dqueue_num(const struct TN_DQueue *p_dqueue)
{
   enum TNT_DQueueNum ret = TNT_DQUEUE__NONE;

   enum TNT_DQueueNum i;
   for (i = (enum TNT_DQueueNum)0; i < TNT_DQUEUES_CNT; i++){
      if (p_dqueue == &_dqueue[i]){
         ret = i;
      }
   }

   return ret;
}

static enum TNT_SemNum _sem_pt_to_sem_num(const struct TN_Sem *p_sem)
{
   enum TNT_SemNum ret = TNT_SEM__NONE;

   enum TNT_SemNum i;
   for (i = (enum TNT_SemNum)0; i < TNT_SEMS_CNT; i++){
      if (p_sem == &_sem[i]){
         ret = i;
      }
   }

   return ret;
}

static enum TNT_EventNum _event_pt_to_event_num(const struct TN_EventGrp *p_event)
{
   enum TNT_EventNum ret = TNT_EVENT__NONE;

   enum TNT_EventNum i;
   for (i = (enum TNT_EventNum)0; i < TNT_EVENTS_CNT; i++){
      if (p_event == &_event[i]){
         ret = i;
      }
   }

   return ret;
}

static const char *_task_name_get_by_task_num(int/*enum TNT_TaskNum*/ task_num)
{
   return _p_task_name[task_num];
}

static const char *_yes_no_get_by_bool(int/*BOOL*/ bool_value)
{
   return bool_value ? "yes" : "no";
}

static const char *_mutex_name_get_by_mutex_num(int/*enum TNT_MutexNum*/ mutex_num)
{
   return _p_mutex_name[mutex_num];
}

static const char *_fmp_name_get_by_fmp_num(int/*enum TNT_FmpNum*/ fmp_num)
{
   return _p_fmp_name[fmp_num];
}

static const char *_dqueue_name_get_by_dqueue_num(int/*enum TNT_DQueueNum*/ dqueue_num)
{
   return _p_dqueue_name[dqueue_num];
}

static const char *_sem_name_get_by_sem_num(int/*enum TNT_SemNum*/ sem_num)
{
   return _p_sem_name[sem_num];
}

static const char *_event_name_get_by_event_num(int/*enum TNT_EventNum*/ event_num)
{
   return _p_event_name[event_num];
}

static const char *_mutex_name_get(const struct TN_Mutex *p_mutex)
{
   return _mutex_name_get_by_mutex_num(
         _mutex_pt_to_mutex_num(p_mutex)
         );
}

static const char *_fmp_name_get(const struct TN_FMem *p_fmp)
{
   return _fmp_name_get_by_fmp_num(
         _fmp_pt_to_fmp_num(p_fmp)
         );
}

static const char *_dqueue_name_get(const struct TN_DQueue *p_dqueue)
{
   return _dqueue_name_get_by_dqueue_num(
         _dqueue_pt_to_dqueue_num(p_dqueue)
         );
}

static const char *_sem_name_get(const struct TN_Sem *p_sem)
{
   return _sem_name_get_by_sem_num(
         _sem_pt_to_sem_num(p_sem)
         );
}

static const char *_event_name_get(const struct TN_EventGrp *p_event)
{
   return _event_name_get_by_event_num(
         _event_pt_to_event_num(p_event)
         );
}


/**
 * Returns pointer to statically allocated buffer!
 */
static const char *_task_cmd_explain(const struct TWorkMan_TaskCmd *p_task_cmd)
{
   static char _expl_string_buf[ _EXPL_STRING_BUF_SIZE ];

   switch (p_task_cmd->cmd){
      case TWORKMAN_TASK_CMD__MUTEX_LOCK:
         xsprintf(_expl_string_buf, "lock mutex %s (0x%x)",
               _mutex_name_get(p_task_cmd->data.mutex.p_mutex),
               (unsigned int)p_task_cmd->data.mutex.p_mutex
               );
         break;
      case TWORKMAN_TASK_CMD__MUTEX_UNLOCK:
         xsprintf(_expl_string_buf, "unlock mutex %s (0x%x)",
               _mutex_name_get(p_task_cmd->data.mutex.p_mutex),
               (unsigned int)p_task_cmd->data.mutex.p_mutex
               );
         break;
      case TWORKMAN_TASK_CMD__MUTEX_DELETE:
         xsprintf(_expl_string_buf, "delete mutex %s (0x%x)",
               _mutex_name_get(p_task_cmd->data.mutex.p_mutex),
               (unsigned int)p_task_cmd->data.mutex.p_mutex
               );
         break;
      case TWORKMAN_TASK_CMD__EXMODE_SET:
         
         xsprintf(_expl_string_buf, "set exmode=%s",
               _p_exmode_name[p_task_cmd->data.exmode.exmode]
               );
         break;
      case TWORKMAN_TASK_CMD__TASK_EXIT:
         xsprintf(_expl_string_buf, "task exit");
         break;
      case TWORKMAN_TASK_CMD__TASK_RETURN:
         xsprintf(_expl_string_buf, "return from task function");
         break;

      case TWORKMAN_TASK_CMD__TASK_SUSPEND:
         xsprintf(_expl_string_buf, "suspend task %s",
               _task_name_get_by_task_num(
                  _task_pt_to_task_num( p_task_cmd->data.task.p_task )
                  )
               );
         break;
      case TWORKMAN_TASK_CMD__TASK_RESUME:
         xsprintf(_expl_string_buf, "resume task %s",
               _task_name_get_by_task_num(
                  _task_pt_to_task_num( p_task_cmd->data.task.p_task )
                  )
               );
         break;

      case TWORKMAN_TASK_CMD__TASK_SLEEP:
         xsprintf(_expl_string_buf, "task sleep");
         break;

      case TWORKMAN_TASK_CMD__TASK_WAKEUP:
         xsprintf(_expl_string_buf, "wake up task %s",
               _task_name_get_by_task_num(
                  _task_pt_to_task_num( p_task_cmd->data.task.p_task )
                  )
               );
         break;

      case TWORKMAN_TASK_CMD__TASK_RELEASE_WAIT:
         xsprintf(_expl_string_buf, "release from wait task %s",
               _task_name_get_by_task_num(
                  _task_pt_to_task_num( p_task_cmd->data.task.p_task )
                  )
               );
         break;

      case TWORKMAN_TASK_CMD__TASK_IACTIVATE:
         xsprintf(_expl_string_buf, "iactivate task %s",
               _task_name_get_by_task_num(
                  _task_pt_to_task_num( p_task_cmd->data.task.p_task )
                  )
               );
         break;

      case TWORKMAN_TASK_CMD__TASK_IWAKEUP:
         xsprintf(_expl_string_buf, "iwakeup task %s",
               _task_name_get_by_task_num(
                  _task_pt_to_task_num( p_task_cmd->data.task.p_task )
                  )
               );
         break;

      case TWORKMAN_TASK_CMD__TASK_IRELEASE_WAIT:
         xsprintf(_expl_string_buf, "irelease_wait task %s",
               _task_name_get_by_task_num(
                  _task_pt_to_task_num( p_task_cmd->data.task.p_task )
                  )
               );
         break;

      case TWORKMAN_TASK_CMD__FMP_GET:
         xsprintf(_expl_string_buf, "get mem from fmp %s (0x%x)",
               _fmp_name_get(p_task_cmd->data.fmp.p_fmp),
               (unsigned int)p_task_cmd->data.fmp.p_fmp
               );
         break;
      case TWORKMAN_TASK_CMD__FMP_GET_POLLING:
         xsprintf(_expl_string_buf, "get_polling mem to fmp %s (0x%x)",
               _fmp_name_get(p_task_cmd->data.fmp.p_fmp),
               (unsigned int)p_task_cmd->data.fmp.p_fmp
               );
         break;
      case TWORKMAN_TASK_CMD__FMP_RELEASE:
         xsprintf(_expl_string_buf, "release mem to fmp %s (0x%x)",
               _fmp_name_get(p_task_cmd->data.fmp.p_fmp),
               (unsigned int)p_task_cmd->data.fmp.p_fmp
               );
         break;
      case TWORKMAN_TASK_CMD__FMP_GET_IPOLLING:
         xsprintf(_expl_string_buf, "get_ipolling mem to fmp %s (0x%x)",
               _fmp_name_get(p_task_cmd->data.fmp.p_fmp),
               (unsigned int)p_task_cmd->data.fmp.p_fmp
               );
         break;
      case TWORKMAN_TASK_CMD__FMP_IRELEASE:
         xsprintf(_expl_string_buf, "irelease mem to fmp %s (0x%x)",
               _fmp_name_get(p_task_cmd->data.fmp.p_fmp),
               (unsigned int)p_task_cmd->data.fmp.p_fmp
               );
         break;


      case TWORKMAN_TASK_CMD__DQUEUE_SEND:
         xsprintf(_expl_string_buf, "send data to dqueue %s (0x%x)",
               _dqueue_name_get(p_task_cmd->data.dqueue.p_dqueue),
               (unsigned int)p_task_cmd->data.dqueue.p_dqueue
               );
         break;
      case TWORKMAN_TASK_CMD__DQUEUE_SEND_POLLING:
         xsprintf(_expl_string_buf, "send_polling data to dqueue %s (0x%x)",
               _dqueue_name_get(p_task_cmd->data.dqueue.p_dqueue),
               (unsigned int)p_task_cmd->data.dqueue.p_dqueue
               );
         break;
      case TWORKMAN_TASK_CMD__DQUEUE_ISEND_POLLING:
         xsprintf(_expl_string_buf, "isend_polling data to dqueue %s (0x%x)",
               _dqueue_name_get(p_task_cmd->data.dqueue.p_dqueue),
               (unsigned int)p_task_cmd->data.dqueue.p_dqueue
               );
         break;
      case TWORKMAN_TASK_CMD__DQUEUE_RECEIVE:
         xsprintf(_expl_string_buf, "receive data from dqueue %s (0x%x)",
               _dqueue_name_get(p_task_cmd->data.dqueue.p_dqueue),
               (unsigned int)p_task_cmd->data.dqueue.p_dqueue
               );
         break;
      case TWORKMAN_TASK_CMD__DQUEUE_RECEIVE_POLLING:
         xsprintf(_expl_string_buf, "receive_polling data to dqueue %s (0x%x)",
               _dqueue_name_get(p_task_cmd->data.dqueue.p_dqueue),
               (unsigned int)p_task_cmd->data.dqueue.p_dqueue
               );
         break;
      case TWORKMAN_TASK_CMD__DQUEUE_IRECEIVE_POLLING:
         xsprintf(_expl_string_buf, "ireceive_polling data to dqueue %s (0x%x)",
               _dqueue_name_get(p_task_cmd->data.dqueue.p_dqueue),
               (unsigned int)p_task_cmd->data.dqueue.p_dqueue
               );
         break;
      case TWORKMAN_TASK_CMD__DQUEUE_EVENTGRP_CONNECT:
         xsprintf(_expl_string_buf, "connect eventgrp %s (0x%x) with pattern 0x%x to dqueue %s (0x%x)",
               _event_name_get(p_task_cmd->data.dqueue.p_eventgrp),
               (unsigned int)p_task_cmd->data.dqueue.p_eventgrp,
               (unsigned int)p_task_cmd->data.dqueue.pattern,
               _dqueue_name_get(p_task_cmd->data.dqueue.p_dqueue),
               (unsigned int)p_task_cmd->data.dqueue.p_dqueue
               );
         break;
      case TWORKMAN_TASK_CMD__DQUEUE_EVENTGRP_DISCONNECT:
         xsprintf(_expl_string_buf, "disconnect eventgrp (if any) from dqueue %s (0x%x)",
               _dqueue_name_get(p_task_cmd->data.dqueue.p_dqueue),
               (unsigned int)p_task_cmd->data.dqueue.p_dqueue
               );
         break;


      case TWORKMAN_TASK_CMD__SEM_SIGNAL:
         xsprintf(_expl_string_buf, "signal sem %s (0x%x)",
               _sem_name_get(p_task_cmd->data.sem.p_sem),
               (unsigned int)p_task_cmd->data.sem.p_sem
               );
         break;
      case TWORKMAN_TASK_CMD__SEM_ISIGNAL:
         xsprintf(_expl_string_buf, "isignal sem %s (0x%x)",
               _sem_name_get(p_task_cmd->data.sem.p_sem),
               (unsigned int)p_task_cmd->data.sem.p_sem
               );
         break;
      case TWORKMAN_TASK_CMD__SEM_ACQUIRE:
         xsprintf(_expl_string_buf, "acquire sem %s (0x%x)",
               _sem_name_get(p_task_cmd->data.sem.p_sem),
               (unsigned int)p_task_cmd->data.sem.p_sem
               );
         break;
      case TWORKMAN_TASK_CMD__SEM_ACQUIRE_POLLING:
         xsprintf(_expl_string_buf, "acquire_polling sem %s (0x%x)",
               _sem_name_get(p_task_cmd->data.sem.p_sem),
               (unsigned int)p_task_cmd->data.sem.p_sem
               );
         break;
      case TWORKMAN_TASK_CMD__SEM_IACQUIRE_POLLING:
         xsprintf(_expl_string_buf, "iacquire_polling sem %s (0x%x)",
               _sem_name_get(p_task_cmd->data.sem.p_sem),
               (unsigned int)p_task_cmd->data.sem.p_sem
               );
         break;

      case TWORKMAN_TASK_CMD__EVENT_WAIT:
         xsprintf(_expl_string_buf, "wait event %s (0x%x)",
               _event_name_get(p_task_cmd->data.event.p_event),
               (unsigned int)p_task_cmd->data.event.p_event
               );
         break;
      case TWORKMAN_TASK_CMD__EVENT_WAIT_POLLING:
         xsprintf(_expl_string_buf, "wait_polling event %s (0x%x)",
               _event_name_get(p_task_cmd->data.event.p_event),
               (unsigned int)p_task_cmd->data.event.p_event
               );
         break;
      case TWORKMAN_TASK_CMD__EVENT_IWAIT:
         xsprintf(_expl_string_buf, "iwait_polling event %s (0x%x)",
               _event_name_get(p_task_cmd->data.event.p_event),
               (unsigned int)p_task_cmd->data.event.p_event
               );
         break;
      case TWORKMAN_TASK_CMD__EVENT_MODIFY:
         xsprintf(_expl_string_buf, "modify event grp %s (0x%x)",
               _event_name_get(p_task_cmd->data.event.p_event),
               (unsigned int)p_task_cmd->data.event.p_event
               );
         break;
      case TWORKMAN_TASK_CMD__EVENT_IMODIFY:
         xsprintf(_expl_string_buf, "imodify event grp %s (0x%x)",
               _event_name_get(p_task_cmd->data.event.p_event),
               (unsigned int)p_task_cmd->data.event.p_event
               );
         break;
#if TN_OLD_EVENT_API
      case TWORKMAN_TASK_CMD__EVENT_MODIFY_OLD_TNKERNEL:
         xsprintf(_expl_string_buf, "modify event grp (as old tnkernel) %s (0x%x)",
               _event_name_get(p_task_cmd->data.event.p_event),
               (unsigned int)p_task_cmd->data.event.p_event
               );
         break;
      case TWORKMAN_TASK_CMD__EVENT_IMODIFY_OLD_TNKERNEL:
         xsprintf(_expl_string_buf, "imodify event grp (as old tnkernel) %s (0x%x)",
               _event_name_get(p_task_cmd->data.event.p_event),
               (unsigned int)p_task_cmd->data.event.p_event
               );
         break;
#endif

      case TWORKMAN_TASK_CMD__SYS_TSLICE_SET:
         xsprintf(_expl_string_buf, "set time slice %d for priority %d",
               (unsigned int)p_task_cmd->data.sys.p2,
               (unsigned int)p_task_cmd->data.sys.p1
               );
         break;

   }

   return _expl_string_buf;
}



/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

//-- tnt_item_create__... (normally you use only tnt_item_create__check() from them) {{{

//-- check {{{

struct TNT_TestItem *tnt_item_create__check(struct TNT_TestItem *p_prev_item)
{
   struct TNT_TestItem *p_item;

   p_item = _test_item_create();
   p_item->action = TNT_ACT__CHECK;

   //-- if previous item specified, copy data from it to new item
   if (p_prev_item != NULL){
      memcpy(&p_item->data, &p_prev_item->data, sizeof(p_item->data));
   }

   return p_item;
}


//}}}

//-- wait {{{

struct TNT_TestItem *tnt_item_create__wait(TN_Timeout timeout, bool bool_ensure_dterm_finished)
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__WAIT
   p_item = _test_item_create();
   p_item->action = TNT_ACT__WAIT;
   p_item->data.wait.timeout = timeout;
   p_item->data.wait.bool_ensure_dterm_finished = bool_ensure_dterm_finished;

   return p_item;
}

//}}}

//-- Objects management by test director: ..._manage {{{

struct TNT_TestItem *tnt_item_create__task_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_TaskNum     task_num,
      int                  priority
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__TASK_MANAGE
   p_item = _test_item_create();
   p_item->action = TNT_ACT__TASK_MANAGE;
   p_item->data.task_manage.obj_action = obj_action;
   p_item->data.task_manage.task_num   = task_num;
   p_item->data.task_manage.priority   = priority;

   return p_item;
} 

struct TNT_TestItem *tnt_item_create__fmp_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_FmpNum      fmp_num,
      void                *start_addr,
      unsigned int         block_size,
      int                  blocks_cnt
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__FMP_MANAGE
   p_item = _test_item_create();
   p_item->action = TNT_ACT__FMP_MANAGE;
   p_item->data.fmp_manage.obj_action     = obj_action;
   p_item->data.fmp_manage.fmp_num        = fmp_num;
   p_item->data.fmp_manage.start_addr     = start_addr;
   p_item->data.fmp_manage.block_size     = block_size;
   p_item->data.fmp_manage.blocks_cnt     = blocks_cnt;

   return p_item;
}


struct TNT_TestItem *tnt_item_create__mutex_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_MutexNum    mutex_num,
      int                  attribute,
      int                  ceil_priority
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__MUTEX_MANAGE
   p_item = _test_item_create();
   p_item->action = TNT_ACT__MUTEX_MANAGE;
   p_item->data.mutex_manage.obj_action      = obj_action;
   p_item->data.mutex_manage.mutex_num       = mutex_num;
   p_item->data.mutex_manage.attribute       = attribute;
   p_item->data.mutex_manage.ceil_priority   = ceil_priority;

   return p_item;
}

struct TNT_TestItem *tnt_item_create__dqueue_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_DQueueNum   dqueue_num,
      void               **data_fifo,
      int                  items_cnt
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__DQUEUE_MANAGE
   p_item = _test_item_create();
   p_item->action = TNT_ACT__DQUEUE_MANAGE;
   p_item->data.dqueue_manage.obj_action     = obj_action;
   p_item->data.dqueue_manage.dqueue_num     = dqueue_num;
   p_item->data.dqueue_manage.data_fifo      = data_fifo;
   p_item->data.dqueue_manage.items_cnt      = items_cnt;

   return p_item;
}

struct TNT_TestItem *tnt_item_create__sem_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_SemNum      sem_num,
      int                  start_value,
      int                  max_val
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEM_MANAGE
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEM_MANAGE;
   p_item->data.sem_manage.obj_action     = obj_action;
   p_item->data.sem_manage.sem_num        = sem_num;
   p_item->data.sem_manage.start_value    = start_value;
   p_item->data.sem_manage.max_val        = max_val;

   return p_item;
}

struct TNT_TestItem *tnt_item_create__event_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_EventNum    event_num,
      enum TN_EGrpAttr     attr,
      unsigned int         pattern
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__EVENT_MANAGE
   p_item = _test_item_create();
   p_item->action = TNT_ACT__EVENT_MANAGE;
   p_item->data.event_manage.obj_action     = obj_action;
   p_item->data.event_manage.event_num      = event_num;
   p_item->data.event_manage.attr           = attr;
   p_item->data.event_manage.pattern        = pattern;

   return p_item;
}

#if 0
struct TNT_TestItem *tnt_item_create__exch_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_ExchNum     exch_num,
      TN_UWord            *data,
      unsigned int         size
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__EXCH_MANAGE
   p_item = _test_item_create();
   p_item->action = TNT_ACT__EXCH_MANAGE;
   p_item->data.exch_manage.obj_action      = obj_action;
   p_item->data.exch_manage.exch_num        = exch_num;
   p_item->data.exch_manage.data            = data;
   p_item->data.exch_manage.size            = size;

   return p_item;
}
#endif



//}}}

//-- Sending commands to worker tasks: send_cmd_... {{{

struct TNT_TestItem *tnt_item_create__send_cmd_general(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEND_CMD_TO_TASK
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_TASK;
   p_item->data = (union _TestItemData){
      .send_cmd = {
         .task_num = task_num,
         .cmd = {
            .cmd = cmd,
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__send_cmd_fmp(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_FmpNum fmp_num,
      void *p_data,
      TN_Timeout timeout
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEND_CMD_TO_TASK
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_TASK;
   p_item->data = (union _TestItemData){
      .send_cmd = {
         .task_num = task_num,
         .cmd = {
            .cmd = cmd,
            .data = { .fmp = {
               .p_fmp = &_fmp[ fmp_num ],
               .timeout = timeout,
               .p_data = p_data,
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__send_cmd_dqueue(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_DQueueNum dqueue_num,
      void *p_data,
      TN_Timeout timeout
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEND_CMD_TO_TASK
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_TASK;
   p_item->data = (union _TestItemData){
      .send_cmd = {
         .task_num = task_num,
         .cmd = {
            .cmd = cmd,
            .data = { .dqueue = {
               .p_dqueue = &_dqueue[ dqueue_num ],
               .timeout = timeout,
               .p_data = p_data,
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__send_cmd_dqueue_event(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_DQueueNum dqueue_num,
      enum TNT_EventNum eventgrp_num,
      TN_UWord pattern
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEND_CMD_TO_TASK
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_TASK;
   p_item->data = (union _TestItemData){
      .send_cmd = {
         .task_num = task_num,
         .cmd = {
            .cmd = cmd,
            .data = { .dqueue = {
               .p_dqueue   = &_dqueue[ dqueue_num ],
               .p_eventgrp = &_event[ eventgrp_num ],
               .pattern    = pattern,
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__send_cmd_sem(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_SemNum sem_num,
      TN_Timeout timeout
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEND_CMD_TO_TASK
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_TASK;
   p_item->data = (union _TestItemData){
      .send_cmd = {
         .task_num = task_num,
         .cmd = {
            .cmd = cmd,
            .data = { .sem = {
               .p_sem = &_sem[ sem_num ],
               .timeout = timeout,
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__send_cmd_event(
      enum TNT_TaskNum     task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_EventNum    event_num,
      unsigned int         wait_pattern,
      enum TN_EGrpWaitMode wait_mode,
      unsigned int        *p_flags_pattern,
      unsigned long        timeout,
      enum TN_EGrpOp       operation
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEND_CMD_TO_TASK
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_TASK;
   p_item->data = (union _TestItemData){
      .send_cmd = {
         .task_num = task_num,
         .cmd = {
            .cmd = cmd,
            .data = { .event = {
               .p_event          = &_event[ event_num ],
               .wait_pattern     = wait_pattern,
               .wait_mode        = wait_mode,
               .p_flags_pattern  = p_flags_pattern,
               .operation        = operation,
               .timeout          = timeout,
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__send_cmd_mutex(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_MutexNum mutex_num,
      TN_Timeout timeout
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEND_CMD_TO_TASK
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_TASK;
   p_item->data = (union _TestItemData){
      .send_cmd = {
         .task_num = task_num,
         .cmd = {
            .cmd = cmd,
            .data = { .mutex = {
               .p_mutex = &_mutex[ mutex_num ],
               .timeout = timeout,
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__send_cmd_task(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_TaskNum tgt_task_num
      )
{
   struct TNT_TestItem *p_item;

   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_TASK;
   p_item->data = (union _TestItemData){
      .send_cmd = {
         .task_num = task_num,
         .cmd = {
            .cmd = cmd,
            .data = { .task = {
               .p_task = &_me.p_worker_task[ tgt_task_num ]->task,
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__send_cmd_exmode(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_ExMode exmode
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEND_CMD_TO_TASK
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_TASK;
   p_item->data = (union _TestItemData){
      .send_cmd = {
         .task_num = task_num,
         .cmd = {
            .cmd = TWORKMAN_TASK_CMD__EXMODE_SET,
            .data = { .exmode = {
               .exmode = exmode,
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__send_cmd_sleep(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      TN_Timeout timeout
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEND_CMD_TO_TASK
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_TASK;
   p_item->data = (union _TestItemData){
      .send_cmd = {
         .task_num = task_num,
         .cmd = {
            .cmd = cmd,
            .data = { .sleep = {
               .timeout = timeout,
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__send_cmd_sys(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      long p1,
      long p2,
      long p3
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEND_CMD_TO_TASK
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_TASK;
   p_item->data = (union _TestItemData){
      .send_cmd = {
         .task_num = task_num,
         .cmd = {
            .cmd = cmd,
            .data = { .sys = {
               .p1 = p1,
               .p2 = p2,
               .p3 = p3,
            }, },
         },
      },
   };

   return p_item;
}


// }}}

//-- Sending commands to worker interrupt: int_send_cmd_... {{{

struct TNT_TestItem *tnt_item_create__int_send_cmd_task(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_TaskNum tgt_task_num
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEND_CMD_TO_TASK
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_INT;
   p_item->data = (union _TestItemData){
      .int_send_cmd = {
         .cmd = {
            .cmd = cmd,
            .data = { .task = {
               .p_task = &_me.p_worker_task[ tgt_task_num ]->task,
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__int_send_cmd_fmp(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_FmpNum fmp_num,
      void *p_data
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEND_CMD_TO_TASK
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_INT;
   p_item->data = (union _TestItemData){
      .int_send_cmd = {
         .cmd = {
            .cmd = cmd,
            .data = { .fmp = {
               .p_fmp = &_fmp[ fmp_num ],
               .p_data = p_data,
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__int_send_cmd_dqueue(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_DQueueNum dqueue_num,
      void *p_data
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEND_CMD_TO_TASK
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_INT;
   p_item->data = (union _TestItemData){
      .int_send_cmd = {
         .cmd = {
            .cmd = cmd,
            .data = { .dqueue = {
               .p_dqueue = &_dqueue[ dqueue_num ],
               .p_data = p_data,
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__int_send_cmd_sem(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_SemNum sem_num
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEND_CMD_TO_TASK
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_INT;
   p_item->data = (union _TestItemData){
      .int_send_cmd = {
         .cmd = {
            .cmd = cmd,
            .data = { .sem = {
               .p_sem = &_sem[ sem_num ],
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__int_send_cmd_event(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_EventNum event_num,
      unsigned int         wait_pattern,
      enum TN_EGrpWaitMode   wait_mode,
      unsigned int        *p_flags_pattern,
      enum TN_EGrpOp       operation
      )
{
   struct TNT_TestItem *p_item;

   //-- add TNT_ACT__SEND_CMD_TO_TASK
   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_INT;
   p_item->data = (union _TestItemData){
      .int_send_cmd = {
         .cmd = {
            .cmd = cmd,
            .data = { .event = {
               .p_event = &_event[ event_num ],
               .wait_pattern     = wait_pattern,
               .wait_mode        = wait_mode,
               .p_flags_pattern  = p_flags_pattern,
               .operation        = operation,
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__int_send_cmd_mutex(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_MutexNum mutex_num
      )
{
   struct TNT_TestItem *p_item;

   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_INT;
   p_item->data = (union _TestItemData){
      .int_send_cmd = {
         .cmd = {
            .cmd = cmd,
            .data = { .mutex = {
               .p_mutex = &_mutex[ mutex_num ],
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__int_send_cmd_sleep(
      enum E_TWorkMan_TaskCmd cmd,
      TN_Timeout timeout
      )
{
   struct TNT_TestItem *p_item;

   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_INT;
   p_item->data = (union _TestItemData){
      .int_send_cmd = {
         .cmd = {
            .cmd = cmd,
            .data = { .sleep = {
               .timeout = timeout,
            }, },
         },
      },
   };

   return p_item;
}

struct TNT_TestItem *tnt_item_create__int_send_cmd_sys(
      enum E_TWorkMan_TaskCmd cmd,
      long p1,
      long p2,
      long p3
      )
{
   struct TNT_TestItem *p_item;

   p_item = _test_item_create();
   p_item->action = TNT_ACT__SEND_CMD_TO_INT;
   p_item->data = (union _TestItemData){
      .int_send_cmd = {
         .cmd = {
            .cmd = cmd,
            .data = { .sys = {
               .p1 = p1,
               .p2 = p2,
               .p3 = p3,
            }, },
         },
      },
   };

   return p_item;
}



// }}}

// }}}

//-- tnt_item_proceed__... {{{

//-- wait {{{

void tnt_item_proceed__wait(TN_Timeout timeout, bool bool_ensure_dterm_finished)
{
   struct TNT_TestItem *p_item = tnt_item_create__wait(timeout, bool_ensure_dterm_finished);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

//}}}

//-- Objects management by test director: ..._manage {{{

void tnt_item_proceed__task_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_TaskNum     task_num,
      int                  priority
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__task_manage(obj_action, task_num, priority);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}


void tnt_item_proceed__fmp_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_FmpNum      fmp_num,
      void                *start_addr,
      unsigned int         block_size,
      int                  blocks_cnt
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__fmp_manage(obj_action, fmp_num, start_addr, block_size, blocks_cnt);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__dqueue_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_DQueueNum   dqueue_num,
      void               **data_fifo,
      int                  items_cnt
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__dqueue_manage(obj_action, dqueue_num, data_fifo, items_cnt);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__sem_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_SemNum      sem_num,
      int                  start_value,
      int                  max_val
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__sem_manage(obj_action, sem_num, start_value, max_val);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__event_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_EventNum    event_num,
      enum TN_EGrpAttr     attr,
      unsigned int         pattern
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__event_manage(obj_action, event_num, attr, pattern);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__mutex_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_MutexNum    mutex_num,
      int               attribute,
      int               ceil_priority
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__mutex_manage(obj_action, mutex_num, attribute, ceil_priority);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

#if 0
void tnt_item_proceed__exch_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_ExchNum     exch_num,
      TN_UWord            *data,
      unsigned int         size
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__exch_manage(obj_action, exch_num, data, size);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}
#endif


//}}}

//-- Sending commands to worker tasks: send_cmd_... {{{

void tnt_item_proceed__send_cmd_general(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__send_cmd_general(task_num, cmd);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__send_cmd_fmp(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_FmpNum fmp_num,
      void *p_data,
      TN_Timeout timeout
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__send_cmd_fmp(task_num, cmd, fmp_num, p_data, timeout);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__send_cmd_dqueue(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_DQueueNum dqueue_num,
      void *p_data,
      TN_Timeout timeout
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__send_cmd_dqueue(task_num, cmd, dqueue_num, p_data, timeout);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__send_cmd_dqueue_event(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_DQueueNum dqueue_num,
      enum TNT_EventNum eventgrp_num,
      TN_UWord pattern
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__send_cmd_dqueue_event(task_num, cmd, dqueue_num, eventgrp_num, pattern);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__send_cmd_sem(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_SemNum sem_num,
      TN_Timeout timeout
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__send_cmd_sem(task_num, cmd, sem_num, timeout);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__send_cmd_event(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_EventNum event_num,
      unsigned int         wait_pattern,
      enum TN_EGrpWaitMode   wait_mode,
      unsigned int        *p_flags_pattern,
      unsigned long        timeout,
      enum TN_EGrpOp       operation
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__send_cmd_event(task_num, cmd, event_num, wait_pattern, wait_mode, p_flags_pattern, timeout, operation);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__send_cmd_mutex(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_MutexNum mutex_num,
      TN_Timeout timeout
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__send_cmd_mutex(task_num, cmd, mutex_num, timeout);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__send_cmd_task(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_TaskNum tgt_task_num
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__send_cmd_task(task_num, cmd, tgt_task_num);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__send_cmd_exmode(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_ExMode exmode
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__send_cmd_exmode(task_num, exmode);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__send_cmd_sleep(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      TN_Timeout timeout
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__send_cmd_sleep(task_num, cmd, timeout);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__send_cmd_sys(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      long p1,
      long p2,
      long p3
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__send_cmd_sys(
         task_num, cmd, p1, p2, p3
         );
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}


//}}}

//-- Sending commands to worker interrupt: int_send_cmd_... {{{

void tnt_item_proceed__int_send_cmd_fmp(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_FmpNum fmp_num,
      void *p_data
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__int_send_cmd_fmp(cmd, fmp_num, p_data);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__int_send_cmd_dqueue(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_DQueueNum dqueue_num,
      void *p_data
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__int_send_cmd_dqueue(cmd, dqueue_num, p_data);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__int_send_cmd_sem(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_SemNum sem_num
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__int_send_cmd_sem(cmd, sem_num);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__int_send_cmd_event(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_EventNum event_num,
      unsigned int         wait_pattern,
      enum TN_EGrpWaitMode wait_mode,
      unsigned int        *p_flags_pattern,
      enum TN_EGrpOp       operation
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__int_send_cmd_event(cmd, event_num, wait_pattern, wait_mode, p_flags_pattern, operation);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__int_send_cmd_task(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_TaskNum tgt_task_num
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__int_send_cmd_task(cmd, tgt_task_num);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__int_send_cmd_mutex(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_MutexNum mutex_num
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__int_send_cmd_mutex(cmd, mutex_num);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__int_send_cmd_sleep(
      enum E_TWorkMan_TaskCmd cmd,
      TN_Timeout timeout
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__int_send_cmd_sleep(cmd, timeout);
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}

void tnt_item_proceed__int_send_cmd_sys(
      enum E_TWorkMan_TaskCmd cmd,
      long p1,
      long p2,
      long p3
      )
{
   struct TNT_TestItem *p_item = tnt_item_create__int_send_cmd_sys(
         cmd, p1, p2, p3
         );
   tnt_item_proceed(p_item);
   tnt_item_delete(p_item);
}


//}}}

// }}}

//-- Check constraints: tnt_check__... {{{

void tnt_check__set_fmp_constr(
      struct TNT_TestItem *p_item,
      enum TNT_FmpNum fmp_num,
      enum TNT_FmpAttr fmp_attr,
      int attr_value
      )
{
   p_item->data.check.fmp[ fmp_num ].attr[ fmp_attr ] = (struct _CheckInt){
      .bool_check = true,
      .value = attr_value,
   };
}

void tnt_check__set_dqueue_constr(
      struct TNT_TestItem *p_item,
      enum TNT_DQueueNum dqueue_num,
      enum TNT_DQueueAttr dqueue_attr,
      int attr_value
      )
{
   p_item->data.check.dqueue[ dqueue_num ].attr[ dqueue_attr ] = (struct _CheckInt){
      .bool_check = true,
      .value = attr_value,
   };
}

void tnt_check__set_sem_constr(
      struct TNT_TestItem *p_item,
      enum TNT_SemNum sem_num,
      enum TNT_SemAttr sem_attr,
      int attr_value
      )
{
   p_item->data.check.sem[ sem_num ].attr[ sem_attr ] = (struct _CheckInt){
      .bool_check = true,
      .value = attr_value,
   };
}

void tnt_check__set_event_constr(
      struct TNT_TestItem *p_item,
      enum TNT_EventNum event_num,
      enum TNT_EventAttr event_attr,
      int attr_value
      )
{
   p_item->data.check.event[ event_num ].attr[ event_attr ] = (struct _CheckInt){
      .bool_check = true,
      .value = attr_value,
   };
}

void tnt_check__set_task_constr(
      struct TNT_TestItem *p_item,
      enum TNT_TaskNum task_num,
      enum TNT_TaskAttr task_attr,
      int attr_value
      )
{
   p_item->data.check.task[ task_num ].attr[ task_attr ] = (struct _CheckInt){
      .bool_check = true,
      .value = attr_value,
   };
}

void tnt_check__set_mutex_constr(
      struct TNT_TestItem *p_item,
      enum TNT_MutexNum mutex_num,
      enum TNT_MutexAttr mutex_attr,
      int attr_value
      )
{

#if !TN_MUTEX_REC
   //-- if mutexes aren't recursive, lock_cnt is always 0
   if (mutex_attr == TNT_MUTEX_ATTR__LOCK_CNT){
      attr_value = 0;
   }
#endif

   p_item->data.check.mutex[ mutex_num ].attr[ mutex_attr ] = (struct _CheckInt){
      .bool_check = true,
      .value = attr_value,
   };
}

void tnt_check__set_sys_constr(
      struct TNT_TestItem *p_item,
      enum TNT_SysAttr sys_attr,
      int attr_value
      )
{

   p_item->data.check.sys.attr[ sys_attr ] = (struct _CheckInt){
      .bool_check = true,
      .value = attr_value,
   };
}

void tnt_check__set_director_constr(
      struct TNT_TestItem *p_item,
      enum TNT_DirectorAttr director_attr,
      int attr_value
      )
{

   p_item->data.check.director.attr[ director_attr ] = (struct _CheckInt){
      .bool_check = true,
      .value = attr_value,
   };
}

void tnt_check__set_interrupt_constr(
      struct TNT_TestItem *p_item,
      enum TNT_InterruptAttr interrupt_attr,
      int attr_value
      )
{

   p_item->data.check.interrupt.attr[ interrupt_attr ] = (struct _CheckInt){
      .bool_check = true,
      .value = attr_value,
   };
}

#if 0
void tnt_check__set_exch_constr(
      struct TNT_TestItem *p_item,
      enum TNT_ExchNum exch_num,
      enum TNT_ExchAttr exch_attr,
      int attr_value
      )
{
   p_item->data.check.exch[ exch_num ].attr[ exch_attr ] = (struct _CheckInt){
      .bool_check = true,
      .value = attr_value,
   };
}
#endif


// }}}





void tnt_file_line_echo(int line, const char *file)
{
   TNT_DTMSG_BARE("//   (line %d in %s)", line, file);
}







struct TNT_TestItem *tnt_prev_check_item__get(void)
{
   return &_prev_check_item;
}

void tnt_prev_check_item__reset(void)
{
   memset(&_prev_check_item, 0x00, sizeof(_prev_check_item));
}




void tnt_item_proceed(const struct TNT_TestItem *p_item)
{
   SYSRETVAL_DATA;

   switch (p_item->action){
      case TNT_ACT__TASK_MANAGE:
         {
            const struct _TestItemData_TaskManage *p_data = &p_item->data.task_manage;

            switch (p_data->obj_action){
               case TNT_OBJ_ACT__CREATE:
                  {
                     TNT_DTMSG_I("Create task %s", _p_task_name[ p_data->task_num ]);
                     struct TWorkMan_TaskParam *p_task_param = TNT_MALLOC(sizeof(*p_task_param));
                     p_task_param->p_task_name = _p_task_name[ p_data->task_num ];

                     _me.p_worker_task[ p_data->task_num ] = tworker_man__worker__create(
                           p_task_param, p_data->priority, 0
                           );
                  }
                  break;
               case TNT_OBJ_ACT__TERMINATE:
                  {
                     TNT_DTMSG_I("Terminate task %s", _p_task_name[ p_data->task_num ]);
                     _me.last_retval = tworker_man__worker__terminate(_me.p_worker_task[ p_data->task_num ]);
                  }
                  break;
               case TNT_OBJ_ACT__ACTIVATE:
                  {
                     TNT_DTMSG_I("Activate task %s", _p_task_name[ p_data->task_num ]);
                     _me.last_retval = tworker_man__worker__activate(_me.p_worker_task[ p_data->task_num ]);
                  }
                  break;
               case TNT_OBJ_ACT__DELETE:
                  {
                     TNT_DTMSG_I("Delete task %s", _p_task_name[ p_data->task_num ]);
                     _me.last_retval = tworker_man__worker__delete(_me.p_worker_task[ p_data->task_num ]);
                     if (_me.last_retval == TN_RC_OK){
                        _me.p_worker_task[ p_data->task_num ] = NULL;
                     }
                  }
                  break;
               default:
                  TNT_DTMSG_E("wrong obj_action=%d", p_data->obj_action);
                  break;
            }
         }
         break;
      case TNT_ACT__MUTEX_MANAGE:
         {
            const struct _TestItemData_MutexManage *p_data = &p_item->data.mutex_manage;

            switch (p_data->obj_action){
               case TNT_OBJ_ACT__CREATE:
                  {
                     TNT_DTMSG_I("Create mutex %s", _p_mutex_name[ p_data->mutex_num ]);
                     SYSRETVAL_CHECK(
                           tn_mutex_create(
                              &_mutex[p_data->mutex_num],
                              p_data->attribute,
                              p_data->ceil_priority)
                           );
                  }
                  break;
               case TNT_OBJ_ACT__DELETE:
                  {
                     TNT_DTMSG_I("Delete mutex %s", _p_mutex_name[ p_data->mutex_num ]);
                     SYSRETVAL_CHECK(
                           tn_mutex_delete(&_mutex[ p_data->mutex_num ])
                           );
                  }
                  break;
               default:
                  TNT_DTMSG_E("wrong obj_action=%d", p_data->obj_action);
                  break;
            }
         }
         break;
      case TNT_ACT__FMP_MANAGE:
         {
            const struct _TestItemData_FmpManage *p_data = &p_item->data.fmp_manage;

            switch (p_data->obj_action){
               case TNT_OBJ_ACT__CREATE:
                  {
                     TNT_DTMSG_I("Create fmp %s: start_addr=0x%x, block_size=%d, blocks_cnt=%d",
                           _p_fmp_name[ p_data->fmp_num ],
                           p_data->start_addr,
                           p_data->block_size,
                           p_data->blocks_cnt
                           );
                     _me.last_retval = tn_fmem_create(
                           &_fmp[p_data->fmp_num],
                           p_data->start_addr,
                           p_data->block_size,
                           p_data->blocks_cnt
                           );
                  }
                  break;
               case TNT_OBJ_ACT__DELETE:
                  {
                     TNT_DTMSG_I("Delete fmp %s", _p_fmp_name[ p_data->fmp_num ]);
                     SYSRETVAL_CHECK(
                           tn_fmem_delete(&_fmp[ p_data->fmp_num ])
                           );
                  }
                  break;
               default:
                  TNT_DTMSG_E("wrong obj_action=%d", p_data->obj_action);
                  break;
            }
         }
         break;
      case TNT_ACT__DQUEUE_MANAGE:
         {
            const struct _TestItemData_DQueueManage *p_data = &p_item->data.dqueue_manage;

            switch (p_data->obj_action){
               case TNT_OBJ_ACT__CREATE:
                  {
                     TNT_DTMSG_I("Create dqueue %s: data_fifo=0x%x, items_cnt=%d",
                           _p_dqueue_name[ p_data->dqueue_num ],
                           p_data->data_fifo,
                           p_data->items_cnt
                           );
                     _me.last_retval = tn_queue_create(
                           &_dqueue[p_data->dqueue_num],
                           p_data->data_fifo,
                           p_data->items_cnt
                           );
                  }
                  break;
               case TNT_OBJ_ACT__DELETE:
                  {
                     TNT_DTMSG_I("Delete dqueue %s", _p_dqueue_name[ p_data->dqueue_num ]);
                     SYSRETVAL_CHECK(
                           tn_queue_delete(&_dqueue[ p_data->dqueue_num ])
                           );
                  }
                  break;
               default:
                  TNT_DTMSG_E("wrong obj_action=%d", p_data->obj_action);
                  break;
            }
         }
         break;
      case TNT_ACT__SEM_MANAGE:
         {
            const struct _TestItemData_SemManage *p_data = &p_item->data.sem_manage;

            switch (p_data->obj_action){
               case TNT_OBJ_ACT__CREATE:
                  {
                     TNT_DTMSG_I("Create sem %s: start_value=%d, max_val=%d",
                           _p_sem_name[ p_data->sem_num ],
                           p_data->start_value,
                           p_data->max_val
                           );
                     _me.last_retval = tn_sem_create(
                           &_sem[p_data->sem_num],
                           p_data->start_value,
                           p_data->max_val
                           );
                  }
                  break;
               case TNT_OBJ_ACT__DELETE:
                  {
                     TNT_DTMSG_I("Delete sem %s", _p_sem_name[ p_data->sem_num ]);
                     SYSRETVAL_CHECK(
                           tn_sem_delete(&_sem[ p_data->sem_num ])
                           );
                  }
                  break;
               default:
                  TNT_DTMSG_E("wrong obj_action=%d", p_data->obj_action);
                  break;
            }
         }
         break;
      case TNT_ACT__EVENT_MANAGE:
         {
            const struct _TestItemData_EventManage *p_data = &p_item->data.event_manage;

            switch (p_data->obj_action){
               case TNT_OBJ_ACT__CREATE:
                  {
                     TNT_DTMSG_I("Create event %s: pattern=0x%x",
                           _p_event_name[ p_data->event_num ],
                           p_data->pattern
                           );
                     _me.last_retval = tn_eventgrp_create_wattr(
                           &_event[p_data->event_num],
                           p_data->attr,
                           p_data->pattern
                           );
                  }
                  break;
               case TNT_OBJ_ACT__DELETE:
                  {
                     TNT_DTMSG_I("Delete event %s", _p_event_name[ p_data->event_num ]);
                     SYSRETVAL_CHECK(
                           tn_eventgrp_delete(&_event[ p_data->event_num ])
                           );
                  }
                  break;
               default:
                  TNT_DTMSG_E("wrong obj_action=%d", p_data->obj_action);
                  break;
            }
         }
         break;
#if 0
      case TNT_ACT__EXCH_MANAGE:
         {
            const struct _TestItemData_ExchManage *p_data = &p_item->data.exch_manage;
            

            switch (p_data->obj_action){
               case TNT_OBJ_ACT__CREATE:
                  {
                     TNT_DTMSG_I("Create exchange %s, data=0x%x, size=%u",
                           _p_exch_name[ p_data->exch_num ],
                           (unsigned int)p_data->data,
                           (unsigned int)p_data->size
                           );
                     _me.last_retval = tn_exch_create(
                           &_exch[p_data->exch_num],
                           p_data->data,
                           p_data->size
                           );
                  }
                  break;
               case TNT_OBJ_ACT__DELETE:
                  {
                     TNT_DTMSG_I("Delete exchange %s", _p_exch_name[ p_data->exch_num ]);
                     SYSRETVAL_CHECK(
                           tn_exch_delete(&_exch[ p_data->exch_num ])
                           );
                  }
                  break;
               default:
                  TNT_DTMSG_E("wrong obj_action=%d", p_data->obj_action);
                  break;
            }
         }
         break;
#endif
      case TNT_ACT__SEND_CMD_TO_TASK:
         {
            const struct _TestItemData_SendCmd *p_data = &p_item->data.send_cmd;

            if (!((int)p_data->task_num >= 0 && p_data->task_num < TNT_TASKS_CNT)){
               TNT_DEB_HALT("wrong task_num=%d", p_data->task_num);
            } else {

               //-- explain what we're about to do
               TNT_DTMSG_I("----- Command to task %s: %s",
                     _p_task_name[p_data->task_num],
                     _task_cmd_explain(&p_data->cmd)
                     );

               //-- allocate mem for command and copy data there
               struct TWorkMan_TaskCmd *p_cmd = tworker_man__cmd_mem__alloc();
               //struct TWorkMan_TaskCmd *p_cmd = TNT_MALLOC(sizeof(*p_cmd));
               memcpy(p_cmd, &p_data->cmd, sizeof(*p_cmd));

               //-- send message to target task
               enum TN_RCode tn_res;
               SYSRETVAL_CHECK(tn_res = tn_queue_send(
                        &_me.p_worker_task[ p_data->task_num ]->msg_queue,
                        p_cmd,
                        TN_WAIT_INFINITE
                        )
                     );

               if (tn_res != TN_RC_OK){
                  //-- there was some error, so, free memory
                  TNT_DTMSG_E("tn_res=%d", tn_res);
                  tworker_man__cmd_mem__free(p_cmd);
                  //TNT_FREE(p_cmd);
               }
            }
         }
         break;

      case TNT_ACT__SEND_CMD_TO_INT:
         {
            const struct _TestItemData_IntSendCmd *p_data = &p_item->data.int_send_cmd;

            //-- explain what we're about to do
            TNT_DTMSG_I("----- Command to interrupt: %s",
                  _task_cmd_explain(&p_data->cmd)
                  );

            //-- allocate mem for command and copy data there
            struct TWorkMan_TaskCmd *p_cmd = tworker_man__int_cmd_mem__alloc();
            //struct TWorkMan_TaskCmd *p_cmd = TNT_MALLOC(sizeof(*p_cmd));
            *p_cmd = p_data->cmd;

            //-- send message to target task
            enum TN_RCode tn_res;
            SYSRETVAL_CHECK(tn_res = tn_queue_send(
                     tworker_man__int_queue__get(),
                     p_cmd,
                     TN_WAIT_INFINITE
                     )
                  );

            if (tn_res != TN_RC_OK){
               //-- there was some error, so, free memory
               TNT_DTMSG_E("tn_res=%d", tn_res);
               tworker_man__int_cmd_mem__free(p_cmd);
               //TNT_FREE(p_cmd);
            }
         }
         break;

      case TNT_ACT__WAIT:
         TNT_DTMSG_I("Wait %ld ticks", p_item->data.wait.timeout);
         tn_task_sleep(p_item->data.wait.timeout);

         if (p_item->data.wait.bool_ensure_dterm_finished){
            if (dterm__is_waiting()){
               while (dterm__is_waiting()){
                  tn_task_sleep(1);
               }
               tn_task_sleep(2);
            }
         }
         break;

      case TNT_ACT__CHECK:
         {
            const struct _TestItemData_Check *p_data = &p_item->data.check;
            bool bool_ok = true;

            TNT_DTMSG_I("Checking:");

            //-- check tasks
            int task_num;
            for (task_num = 0; task_num < TNT_TASKS_CNT; task_num++){
               bool bool_cur_ok = true;
               _log_flush();

               if (p_data->task[task_num].attr[TNT_TASK_ATTR__PRIORITY].bool_check){
                  int value_expected = p_data->task[task_num].attr[TNT_TASK_ATTR__PRIORITY].value;
                  int  expected_priority = 
                     (value_expected == TNT_BASE_PRIORITY)
                     ? _me.p_worker_task[task_num]->task.base_priority
                     : value_expected;

                  bool_cur_ok = _log_echo_value("priority",
                        _me.p_worker_task[task_num]->task.priority,
                        expected_priority,
                        NULL
                        ) && bool_cur_ok;

               }

               if (p_data->task[task_num].attr[TNT_TASK_ATTR__WAIT_REASON].bool_check){
                  int value_expected = p_data->task[task_num].attr[TNT_TASK_ATTR__WAIT_REASON].value;
                  bool_cur_ok = _log_echo_value("wait_reason",
                        _me.p_worker_task[task_num]->task.task_wait_reason,
                        value_expected,
                        _wait_reason_name_get
                        ) && bool_cur_ok;
               }

               if (p_data->task[task_num].attr[TNT_TASK_ATTR__LAST_RETVAL].bool_check){
                  int value_expected = p_data->task[task_num].attr[TNT_TASK_ATTR__LAST_RETVAL].value;
                  bool_cur_ok = _log_echo_value("last_retval",
                        _me.p_worker_task[task_num]->last_retval,
                        value_expected,
                        _retval_name_get
                        ) && bool_cur_ok;
               }

               if (p_data->task[task_num].attr[TNT_TASK_ATTR__STATE].bool_check){
                  int value_expected = p_data->task[task_num].attr[TNT_TASK_ATTR__STATE].value;
                  bool_cur_ok = _log_echo_value("state",
                        _me.p_worker_task[task_num]->task.task_state,
                        value_expected,
                        _task_state_name_get
                        ) && bool_cur_ok;
               }

               if (_string_buf_index > 0){
                  if (bool_cur_ok){
                     TNT_DTMSG_SIMP_I( "* Task %s: %s\n", _p_task_name[task_num], _string_buf);
                  } else {
                     TNT_DTMSG_SIMP_E( "* Task %s: %s\n", _p_task_name[task_num], _string_buf);
                  }
               } else if (!bool_cur_ok){
                  TNT_DEB_HALT("should never be here: nothing echoed but bool_cur_ok is false");
               }

               bool_ok = bool_cur_ok && bool_ok;
            }

            //-- check mutexes
            int mutex_num;
            for (mutex_num = 0; mutex_num < TNT_MUTEXES_CNT; mutex_num++){
               _log_flush();
               bool bool_cur_ok = true;

               if (p_data->mutex[mutex_num].attr[TNT_MUTEX_ATTR__HOLDER].bool_check){
                  int value_expected = p_data->mutex[mutex_num].attr[TNT_MUTEX_ATTR__HOLDER].value;

                  bool_cur_ok = _log_echo_value("holder",
                        _task_pt_to_task_num(_mutex[mutex_num].holder),
                        value_expected,
                        _task_name_get_by_task_num
                        ) && bool_cur_ok;

               }

               if (p_data->mutex[mutex_num].attr[TNT_MUTEX_ATTR__LOCK_CNT].bool_check){
                  int value_expected = p_data->mutex[mutex_num].attr[TNT_MUTEX_ATTR__LOCK_CNT].value;

                  bool_cur_ok = _log_echo_value("lock_cnt",
                        _mutex[mutex_num].cnt,
                        value_expected,
                        NULL
                        ) && bool_cur_ok;

               }

               if (p_data->mutex[mutex_num].attr[TNT_MUTEX_ATTR__EXISTS].bool_check){
                  int value_expected = p_data->mutex[mutex_num].attr[TNT_MUTEX_ATTR__EXISTS].value;

                  bool_cur_ok = _log_echo_value("exists",
                        (_mutex[mutex_num].id_mutex == TN_ID_MUTEX),
                        value_expected,
                        _yes_no_get_by_bool
                        ) && bool_cur_ok;

               }

               if (_string_buf_index > 0){
                  if (bool_cur_ok){
                     TNT_DTMSG_SIMP_I( "* Mutex %s: %s\n", _p_mutex_name[mutex_num], _string_buf);
                  } else {
                     TNT_DTMSG_SIMP_E( "* Mutex %s: %s\n", _p_mutex_name[mutex_num], _string_buf);
                  }
               } else if (!bool_cur_ok){
                  TNT_DEB_HALT("should never be here: nothing echoed but bool_cur_ok is false");
               }

               bool_ok = bool_cur_ok && bool_ok;
            }

            //-- check fmps
            int fmp_num;
            for (fmp_num = 0; fmp_num < TNT_FMPS_CNT; fmp_num++){
               _log_flush();
               bool bool_cur_ok = true;

               if (p_data->fmp[fmp_num].attr[TNT_FMP_ATTR__BLOCKS_CNT].bool_check){
                  int value_expected = p_data->fmp[fmp_num].attr[TNT_FMP_ATTR__BLOCKS_CNT].value;

                  bool_cur_ok = _log_echo_value("blocks_cnt",
                        _fmp[fmp_num].blocks_cnt,
                        value_expected,
                        NULL
                        ) && bool_cur_ok;

               }

               if (p_data->fmp[fmp_num].attr[TNT_FMP_ATTR__BLOCK_SIZE].bool_check){
                  int value_expected = p_data->fmp[fmp_num].attr[TNT_FMP_ATTR__BLOCK_SIZE].value;

                  bool_cur_ok = _log_echo_value("block_size",
                        _fmp[fmp_num].block_size,
                        value_expected,
                        NULL
                        ) && bool_cur_ok;

               }

               if (p_data->fmp[fmp_num].attr[TNT_FMP_ATTR__FREE_BLOCKS_CNT].bool_check){
                  int value_expected = p_data->fmp[fmp_num].attr[TNT_FMP_ATTR__FREE_BLOCKS_CNT].value;

                  bool_cur_ok = _log_echo_value("free_blocks_cnt",
                        tn_fmem_free_blocks_cnt_get(&_fmp[fmp_num]),
                        value_expected,
                        NULL
                        ) && bool_cur_ok;

               }

               if (p_data->fmp[fmp_num].attr[TNT_FMP_ATTR__USED_BLOCKS_CNT].bool_check){
                  int value_expected = p_data->fmp[fmp_num].attr[TNT_FMP_ATTR__USED_BLOCKS_CNT].value;

                  bool_cur_ok = _log_echo_value("used_blocks_cnt",
                        tn_fmem_used_blocks_cnt_get(&_fmp[fmp_num]),
                        value_expected,
                        NULL
                        ) && bool_cur_ok;

               }

               if (p_data->fmp[fmp_num].attr[TNT_FMP_ATTR__EXISTS].bool_check){
                  int value_expected = p_data->fmp[fmp_num].attr[TNT_FMP_ATTR__EXISTS].value;

                  bool_cur_ok = _log_echo_value("exists",
                        (_fmp[fmp_num].id_fmp == TN_ID_FSMEMORYPOOL),
                        value_expected,
                        _yes_no_get_by_bool
                        ) && bool_cur_ok;

               }


               if (_string_buf_index > 0){
                  if (bool_cur_ok){
                     TNT_DTMSG_SIMP_I( "* FMP %s: %s\n", _p_fmp_name[fmp_num], _string_buf);
                  } else {
                     TNT_DTMSG_SIMP_E( "* FMP %s: %s\n", _p_fmp_name[fmp_num], _string_buf);
                  }
               } else if (!bool_cur_ok){
                  TNT_DEB_HALT("should never be here: nothing echoed but bool_cur_ok is false");
               }

               bool_ok = bool_cur_ok && bool_ok;
            }

            //-- check dqueues
            int dqueue_num;
            for (dqueue_num = 0; dqueue_num < TNT_DQUEUES_CNT; dqueue_num++){
               _log_flush();
               bool bool_cur_ok = true;

               if (p_data->dqueue[dqueue_num].attr[TNT_DQUEUE_ATTR__ITEMS_CNT].bool_check){
                  int value_expected = p_data->dqueue[dqueue_num].attr[TNT_DQUEUE_ATTR__ITEMS_CNT].value;

                  bool_cur_ok = _log_echo_value("items_cnt",
                        _dqueue[dqueue_num].items_cnt,
                        value_expected,
                        NULL
                        ) && bool_cur_ok;

               }

               if (p_data->dqueue[dqueue_num].attr[TNT_DQUEUE_ATTR__WRITTEN_ITEMS_CNT].bool_check){
                  int value_expected = p_data->dqueue[dqueue_num].attr[TNT_DQUEUE_ATTR__WRITTEN_ITEMS_CNT].value;
                  int written_items_cnt = tn_queue_used_items_cnt_get(&_dqueue[dqueue_num]);

                  bool_cur_ok = _log_echo_value("written_items_cnt",
                        written_items_cnt,
                        value_expected,
                        NULL
                        ) && bool_cur_ok;

               }

               if (p_data->dqueue[dqueue_num].attr[TNT_DQUEUE_ATTR__FREE_ITEMS_CNT].bool_check){
                  int value_expected = p_data->dqueue[dqueue_num].attr[TNT_DQUEUE_ATTR__FREE_ITEMS_CNT].value;
                  int free_items_cnt = tn_queue_free_items_cnt_get(&_dqueue[dqueue_num]);

                  bool_cur_ok = _log_echo_value("free_items_cnt",
                        free_items_cnt,
                        value_expected,
                        NULL
                        ) && bool_cur_ok;

               }

               if (p_data->dqueue[dqueue_num].attr[TNT_DQUEUE_ATTR__EXISTS].bool_check){
                  int value_expected = p_data->dqueue[dqueue_num].attr[TNT_DQUEUE_ATTR__EXISTS].value;

                  bool_cur_ok = _log_echo_value("exists",
                        (_dqueue[dqueue_num].id_dque == TN_ID_DATAQUEUE),
                        value_expected,
                        _yes_no_get_by_bool
                        ) && bool_cur_ok;

               }

               if (p_data->dqueue[dqueue_num].attr[TNT_DQUEUE_ATTR__CONNECTED_EVENT].bool_check){
                  int value_expected = p_data->dqueue[dqueue_num].attr[TNT_DQUEUE_ATTR__CONNECTED_EVENT].value;

                  bool_cur_ok = _log_echo_value("connected_event",
                        _event_pt_to_event_num(_dqueue[dqueue_num].eventgrp_link.eventgrp),
                        value_expected,
                        _event_name_get_by_event_num
                        ) && bool_cur_ok;

               }

               if (p_data->dqueue[dqueue_num].attr[TNT_DQUEUE_ATTR__CONNECTED_EVENT_PATTERN].bool_check){
                  int value_expected = p_data->dqueue[dqueue_num].attr[TNT_DQUEUE_ATTR__CONNECTED_EVENT_PATTERN].value;

                  bool_cur_ok = _log_echo_value("connected_event_pattern",
                        _dqueue[dqueue_num].eventgrp_link.pattern,
                        value_expected,
                        NULL
                        ) && bool_cur_ok;
               }


               if (_string_buf_index > 0){
                  if (bool_cur_ok){
                     TNT_DTMSG_SIMP_I( "* DQueue %s: %s\n", _p_dqueue_name[dqueue_num], _string_buf);
                  } else {
                     TNT_DTMSG_SIMP_E( "* DQueue %s: %s\n", _p_dqueue_name[dqueue_num], _string_buf);
                  }
               } else if (!bool_cur_ok){
                  TNT_DEB_HALT("should never be here: nothing echoed but bool_cur_ok is false");
               }

               bool_ok = bool_cur_ok && bool_ok;
            }

            //-- check sems
            int sem_num;
            for (sem_num = 0; sem_num < TNT_SEMS_CNT; sem_num++){
               _log_flush();
               bool bool_cur_ok = true;

               if (p_data->sem[sem_num].attr[TNT_SEM_ATTR__COUNT].bool_check){
                  int value_expected = p_data->sem[sem_num].attr[TNT_SEM_ATTR__COUNT].value;

                  bool_cur_ok = _log_echo_value("count",
                        _sem[sem_num].count,
                        value_expected,
                        NULL
                        ) && bool_cur_ok;

               }

               if (p_data->sem[sem_num].attr[TNT_SEM_ATTR__MAX_COUNT].bool_check){
                  int value_expected = p_data->sem[sem_num].attr[TNT_SEM_ATTR__MAX_COUNT].value;

                  bool_cur_ok = _log_echo_value("max_count",
                        _sem[sem_num].max_count,
                        value_expected,
                        NULL
                        ) && bool_cur_ok;

               }

               if (p_data->sem[sem_num].attr[TNT_SEM_ATTR__EXISTS].bool_check){
                  int value_expected = p_data->sem[sem_num].attr[TNT_SEM_ATTR__EXISTS].value;

                  bool_cur_ok = _log_echo_value("exists",
                        (_sem[sem_num].id_sem == TN_ID_SEMAPHORE),
                        value_expected,
                        _yes_no_get_by_bool
                        ) && bool_cur_ok;

               }


               if (_string_buf_index > 0){
                  if (bool_cur_ok){
                     TNT_DTMSG_SIMP_I( "* Sem %s: %s\n", _p_sem_name[sem_num], _string_buf);
                  } else {
                     TNT_DTMSG_SIMP_E( "* Sem %s: %s\n", _p_sem_name[sem_num], _string_buf);
                  }
               } else if (!bool_cur_ok){
                  TNT_DEB_HALT("should never be here: nothing echoed but bool_cur_ok is false");
               }

               bool_ok = bool_cur_ok && bool_ok;
            }

            //-- check events
            int event_num;
            for (event_num = 0; event_num < TNT_EVENTS_CNT; event_num++){
               _log_flush();
               bool bool_cur_ok = true;

               if (p_data->event[event_num].attr[TNT_EVENT_ATTR__PATTERN].bool_check){
                  int value_expected = p_data->event[event_num].attr[TNT_EVENT_ATTR__PATTERN].value;

                  bool_cur_ok = _log_echo_value("pattern",
                        _event[event_num].pattern,
                        value_expected,
                        NULL
                        ) && bool_cur_ok;

               }

               if (p_data->event[event_num].attr[TNT_EVENT_ATTR__EXISTS].bool_check){
                  int value_expected = p_data->event[event_num].attr[TNT_EVENT_ATTR__EXISTS].value;

                  bool_cur_ok = _log_echo_value("exists",
                        (_event[event_num].id_event == TN_ID_EVENTGRP),
                        value_expected,
                        _yes_no_get_by_bool
                        ) && bool_cur_ok;

               }


               if (_string_buf_index > 0){
                  if (bool_cur_ok){
                     TNT_DTMSG_SIMP_I( "* Event %s: %s\n", _p_event_name[event_num], _string_buf);
                  } else {
                     TNT_DTMSG_SIMP_E( "* Event %s: %s\n", _p_event_name[event_num], _string_buf);
                  }
               } else if (!bool_cur_ok){
                  TNT_DEB_HALT("should never be here: nothing echoed but bool_cur_ok is false");
               }

               bool_ok = bool_cur_ok && bool_ok;
            }

#if 0
            //-- check exchange objects
            int exch_num;
            for (exch_num = 0; exch_num < TNT_EXCHS_CNT; exch_num++){
               _log_flush();
               bool bool_cur_ok = true;

               if (p_data->exch[exch_num].attr[TNT_EXCH_ATTR__SIZE].bool_check){
                  int value_expected = p_data->exch[exch_num].attr[TNT_EXCH_ATTR__SIZE].value;

                  bool_cur_ok = _log_echo_value("size",
                        _exch[exch_num].size,
                        value_expected,
                        NULL
                        ) && bool_cur_ok;

               }

               if (p_data->exch[exch_num].attr[TNT_EXCH_ATTR__EXISTS].bool_check){
                  int value_expected = p_data->exch[exch_num].attr[TNT_EXCH_ATTR__EXISTS].value;

                  bool_cur_ok = _log_echo_value("exists",
                        (_exch[exch_num].id_exch == TN_ID_EXCHANGE),
                        value_expected,
                        _yes_no_get_by_bool
                        ) && bool_cur_ok;

               }


               if (_string_buf_index > 0){
                  if (bool_cur_ok){
                     TNT_DTMSG_SIMP_I( "* Exchange %s: %s\n", _p_exch_name[exch_num], _string_buf);
                  } else {
                     TNT_DTMSG_SIMP_E( "* Exchange %s: %s\n", _p_exch_name[exch_num], _string_buf);
                  }
               } else if (!bool_cur_ok){
                  TNT_DEB_HALT("should never be here: nothing echoed but bool_cur_ok is false");
               }

               bool_ok = bool_cur_ok && bool_ok;
            }
#endif

            //-- check sys
            {
               _log_flush();
               bool bool_cur_ok = true;

#if TN_MUTEX_DEADLOCK_DETECT
               if (p_data->sys.attr[TNT_SYS_ATTR__DEADLOCK_CNT].bool_check){
                  int value_expected = p_data->sys.attr[TNT_SYS_ATTR__DEADLOCK_CNT].value;

                  bool_cur_ok = _log_echo_value("deadlock_cnt",
                        _tn_deadlocks_cnt,
                        value_expected,
                        NULL
                        ) && bool_cur_ok;

               }

               if (p_data->sys.attr[TNT_SYS_ATTR__DEADLOCK_FLAG].bool_check){
                  int value_expected = p_data->sys.attr[TNT_SYS_ATTR__DEADLOCK_FLAG].value;

                  bool_cur_ok = _log_echo_value("deadlock_flag",
                        !!(tn_sys_state_flags_get() & TN_STATE_FLAG__DEADLOCK),
                        value_expected,
                        NULL
                        ) && bool_cur_ok;

               }
#endif

               if (_string_buf_index > 0){
                  if (bool_cur_ok){
                     TNT_DTMSG_SIMP_I( "* Sys: %s\n", _string_buf);
                  } else {
                     TNT_DTMSG_SIMP_E( "* Sys: %s\n", _string_buf);
                  }
               } else if (!bool_cur_ok){
                  TNT_DEB_HALT("should never be here: nothing echoed but bool_cur_ok is false");
               }

               bool_ok = bool_cur_ok && bool_ok;
            }

            //-- check director task stuff
            {
               _log_flush();
               bool bool_cur_ok = true;

               if (p_data->director.attr[TNT_DIRECTOR_ATTR__LAST_RETVAL].bool_check){
                  int value_expected = p_data->director.attr[TNT_DIRECTOR_ATTR__LAST_RETVAL].value;

                  bool_cur_ok = _log_echo_value("last_retval",
                        _me.last_retval,
                        value_expected,
                        _retval_name_get
                        ) && bool_cur_ok;

               }

               if (_string_buf_index > 0){
                  if (bool_cur_ok){
                     TNT_DTMSG_SIMP_I( "* Director: %s\n", _string_buf);
                  } else {
                     TNT_DTMSG_SIMP_E( "* Director: %s\n", _string_buf);
                  }
               } else if (!bool_cur_ok){
                  TNT_DEB_HALT("should never be here: nothing echoed but bool_cur_ok is false");
               }

               bool_ok = bool_cur_ok && bool_ok;
            }

            //-- check interrupt stuff
            {
               _log_flush();
               bool bool_cur_ok = true;

               if (p_data->interrupt.attr[TNT_INTERRUPT_ATTR__LAST_RETVAL].bool_check){
                  int value_expected = p_data->interrupt.attr[TNT_INTERRUPT_ATTR__LAST_RETVAL].value;

                  bool_cur_ok = _log_echo_value("last_retval",
                        tworker_man__int_attr__get(TNT_INTERRUPT_ATTR__LAST_RETVAL),
                        value_expected,
                        _retval_name_get
                        ) && bool_cur_ok;

               }

               if (_string_buf_index > 0){
                  if (bool_cur_ok){
                     TNT_DTMSG_SIMP_I("* Interrupt: %s\n", _string_buf);
                  } else {
                     TNT_DTMSG_SIMP_E( "* Interrupt: %s\n", _string_buf);
                  }
               } else if (!bool_cur_ok){
                  TNT_DEB_HALT("should never be here: nothing echoed but bool_cur_ok is false");
               }

               bool_ok = bool_cur_ok && bool_ok;
            }

            if (!bool_ok){
               TNT_DTMSG_E("Stop tests because of error.");
               for (;;){
                  tn_task_sleep(100);
               }
            }

            //-- remember this check item as 'previous' one
            memcpy(&_prev_check_item, p_item, sizeof(_prev_check_item));
         }
         break;
   }
}

void tnt_item_delete(struct TNT_TestItem *p_item)
{
   TNT_FREE(p_item);
}





/*******************************************************************************
 *    end of file
 ******************************************************************************/



