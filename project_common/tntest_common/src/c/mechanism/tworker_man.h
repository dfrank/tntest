/***************************************************************************************************
 *   Project:       P-0173 - ��-90 - ��������� ��� ��-100
 *   Author:        Dmitry Frank
 ***************************************************************************************************
 *   Distribution:  Copyright � 2009-2010 �����
 *
 ***************************************************************************************************
 *   MCU Family:    PIC24FJ256GB106
 *   Compiler:      Microchip C30 3.22
 ***************************************************************************************************
 *   File:          
 *   Description:   
 *
 ***************************************************************************************************
 *
 **************************************************************************************************/

#ifndef _TWORKER_MAN_H
#define _TWORKER_MAN_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include "utils/dllist.h"
#include "tntest_types.h"
#include "tn.h"

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

enum E_TWorkMan_TaskCmd {
   TWORKMAN_TASK_CMD__MUTEX_LOCK,
   TWORKMAN_TASK_CMD__MUTEX_UNLOCK,
   TWORKMAN_TASK_CMD__MUTEX_DELETE,

   TWORKMAN_TASK_CMD__EXMODE_SET,

   TWORKMAN_TASK_CMD__TASK_EXIT,    //-- no param, i.e. exit current task
   TWORKMAN_TASK_CMD__TASK_RETURN,  //-- no param, i.e. return from current task
                                    //   function (should be the same as 
                                    //   task_exit)

   TWORKMAN_TASK_CMD__TASK_SUSPEND, //-- suspend any task
   TWORKMAN_TASK_CMD__TASK_RESUME,  //-- resume any task
   TWORKMAN_TASK_CMD__TASK_WAKEUP, //-- wake any task up
   TWORKMAN_TASK_CMD__TASK_RELEASE_WAIT, //-- release from wait any task

   TWORKMAN_TASK_CMD__TASK_SLEEP,   //-- no param, i.e. sleep current task

   TWORKMAN_TASK_CMD__FMP_GET,
   TWORKMAN_TASK_CMD__FMP_GET_POLLING,
   TWORKMAN_TASK_CMD__FMP_RELEASE,

   TWORKMAN_TASK_CMD__DQUEUE_SEND,
   TWORKMAN_TASK_CMD__DQUEUE_SEND_POLLING,
   TWORKMAN_TASK_CMD__DQUEUE_RECEIVE,
   TWORKMAN_TASK_CMD__DQUEUE_RECEIVE_POLLING,
   TWORKMAN_TASK_CMD__DQUEUE_EVENTGRP_CONNECT,
   TWORKMAN_TASK_CMD__DQUEUE_EVENTGRP_DISCONNECT,

   TWORKMAN_TASK_CMD__SEM_SIGNAL,
   TWORKMAN_TASK_CMD__SEM_ACQUIRE,
   TWORKMAN_TASK_CMD__SEM_ACQUIRE_POLLING,

   TWORKMAN_TASK_CMD__EVENT_WAIT,
   TWORKMAN_TASK_CMD__EVENT_WAIT_POLLING,
   TWORKMAN_TASK_CMD__EVENT_MODIFY,
#if TN_OLD_EVENT_API
   TWORKMAN_TASK_CMD__EVENT_MODIFY_OLD_TNKERNEL,
#endif


   TWORKMAN_TASK_CMD__TASK_IACTIVATE,  //-- activate (bring from DORMANT state to RUNNABLE)
   TWORKMAN_TASK_CMD__TASK_IWAKEUP,
   TWORKMAN_TASK_CMD__TASK_IRELEASE_WAIT,
   TWORKMAN_TASK_CMD__FMP_GET_IPOLLING,
   TWORKMAN_TASK_CMD__FMP_IRELEASE,

   TWORKMAN_TASK_CMD__DQUEUE_ISEND_POLLING,
   TWORKMAN_TASK_CMD__DQUEUE_IRECEIVE_POLLING,

   TWORKMAN_TASK_CMD__SEM_ISIGNAL,
   TWORKMAN_TASK_CMD__SEM_IACQUIRE_POLLING,

   TWORKMAN_TASK_CMD__EVENT_IWAIT,
   TWORKMAN_TASK_CMD__EVENT_IMODIFY,
#if TN_OLD_EVENT_API
   TWORKMAN_TASK_CMD__EVENT_IMODIFY_OLD_TNKERNEL,
#endif

   TWORKMAN_TASK_CMD__SYS_TSLICE_SET,
};

enum E_TWorkMan_ExMode {
   TWORKMAN_EXMODE__WAIT,
   TWORKMAN_EXMODE__POLLING,
   TWORKMAN_EXMODES_CNT,
};

struct TWorkMan_TaskParam {
   const char *p_task_name;
};

struct TWorkMan_TaskData {

   //-- list to keep track of all created tasks
   T_DLListItem      list_node;  

   //-- task stuff
   struct TN_Task    task;
   TN_UWord         *p_stack;
   int               stack_size;
   int               priority;
   struct TWorkMan_TaskParam *p_task_param;

   //-- queue stuff
   struct TN_DQueue  msg_queue;
   void            **pp_msg_queue_buf;

   enum TN_RCode    last_retval;
   enum E_TWorkMan_ExMode  exmode;

   unsigned          bool_need_return : 1;
};


struct TWorkMan_TaskCmd {
   enum E_TWorkMan_TaskCmd    cmd;

   union {
      struct {
         struct TN_Mutex     *p_mutex;
         int                  timeout;
      } mutex;

      struct {
         struct TN_FMem       *p_fmp;
         int                  timeout;
         void                *p_data;
      } fmp;

      struct {
         struct TN_DQueue    *p_dqueue;
         int                  timeout;
         void                *p_data;

         struct TN_EventGrp  *p_eventgrp;
         TN_UWord             pattern;

      } dqueue;

      struct {
         struct TN_Sem       *p_sem;
         int                  timeout;
      } sem;

      struct {
         struct TN_EventGrp  *p_event;
         int                  timeout;
         unsigned int         wait_pattern;
         enum TN_EGrpWaitMode wait_mode;
         unsigned int        *p_flags_pattern;
         enum TN_EGrpOp       operation;
      } event;

      struct {
         enum E_TWorkMan_ExMode  exmode;
      } exmode;

      struct {
         struct TN_Task      *p_task;
      } task;

      struct {
         int                  timeout;
      } sleep;

      struct {
         long                 p1;
         long                 p2;
         long                 p3;
      } sys;

   } data;
};

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

#define  TWORKER_MAN__LAST_RETVAL__UNKNOWN   ((enum TN_RCode)(-100))//-- when retval isn't yet received
                                                   //   this value should not interfere
                                                   //   with TERR_... values



/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

void tworker_man__init(void);

void tworker_man__int_handler(void);

struct TWorkMan_TaskData *tworker_man__worker__create(
      struct TWorkMan_TaskParam *p_task_param,
      int priority,
      int stack_size       //-- if 0, default value will be used
      );

enum TN_RCode tworker_man__worker__terminate(
      struct TWorkMan_TaskData *p_tdata
      );

enum TN_RCode tworker_man__worker__activate(
      struct TWorkMan_TaskData *p_tdata
      );

enum TN_RCode tworker_man__worker__delete(
      struct TWorkMan_TaskData *p_tdata
      );



struct TN_DQueue *tworker_man__int_queue__get(void);
struct TWorkMan_TaskCmd *tworker_man__cmd_mem__alloc(void);
struct TWorkMan_TaskCmd *tworker_man__int_cmd_mem__alloc(void);
int tworker_man__int_attr__get(enum TNT_InterruptAttr interrupt_attr);
void tworker_man__cmd_mem__free(struct TWorkMan_TaskCmd *p_cmd);
void tworker_man__cmd_mem__ifree(struct TWorkMan_TaskCmd *p_cmd);
void tworker_man__int_cmd_mem__free(struct TWorkMan_TaskCmd *p_cmd);
void tworker_man__int_cmd_mem__ifree(struct TWorkMan_TaskCmd *p_cmd);

#endif // _TWORKER_MAN_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


