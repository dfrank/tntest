/*******************************************************************************
 *   Description:   TODO
 *
 ******************************************************************************/

#ifndef _TNTEST_TYPES_H
#define _TNTEST_TYPES_H

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC TYPES
 ******************************************************************************/

enum TNT_TaskNum {
   TNT_TASK__A,
   TNT_TASK__B,
   TNT_TASK__C,
   TNT_TASK__D,
   TNT_TASK__E,

   TNT_TASKS_CNT,
};
#define TNT_TASK__NONE TNT_TASKS_CNT

enum TNT_MutexNum {
   TNT_MUTEX__1,
   TNT_MUTEX__2,
   TNT_MUTEX__3,

   TNT_MUTEXES_CNT,
};
#define TNT_MUTEX__NONE TNT_MUTEXES_CNT

enum TNT_FmpNum {
   TNT_FMP__1,

   TNT_FMPS_CNT,
};
#define TNT_FMP__NONE TNT_FMPS_CNT

enum TNT_DQueueNum {
   TNT_DQUEUE__1,
   TNT_DQUEUE__2,

   TNT_DQUEUES_CNT,
};
#define TNT_DQUEUE__NONE TNT_DQUEUES_CNT

enum TNT_SemNum {
   TNT_SEM__1,

   TNT_SEMS_CNT,
};
#define TNT_SEM__NONE TNT_SEMS_CNT

enum TNT_EventNum {
   TNT_EVENT__1,

   TNT_EVENTS_CNT,
};
#define TNT_EVENT__NONE TNT_EVENTS_CNT

enum TNT_ExchNum {
   TNT_EXCH__1,

   TNT_EXCHS_CNT,
};
#define TNT_EXCH__NONE TNT_EXCHS_CNT

enum TNT_ObjAction {
   TNT_OBJ_ACT__CREATE,
   TNT_OBJ_ACT__DELETE,
   TNT_OBJ_ACT__TERMINATE, //-- for tasks only
   TNT_OBJ_ACT__ACTIVATE,  //-- for tasks only

   TNT_OBJ_ACTS_CNT,
};

enum TNT_Action {
   TNT_ACT__TASK_MANAGE,
   TNT_ACT__MUTEX_MANAGE,
   TNT_ACT__FMP_MANAGE,
   TNT_ACT__SEM_MANAGE,
   TNT_ACT__DQUEUE_MANAGE,
   TNT_ACT__EVENT_MANAGE,
#if 0
   TNT_ACT__EXCH_MANAGE,
#endif
   TNT_ACT__SEND_CMD_TO_TASK,
   TNT_ACT__SEND_CMD_TO_INT,
   TNT_ACT__WAIT,
   TNT_ACT__CHECK,
};

enum TNT_TaskAttr {
   TNT_TASK_ATTR__PRIORITY,
   TNT_TASK_ATTR__STATE,
   TNT_TASK_ATTR__WAIT_REASON,
   TNT_TASK_ATTR__LAST_RETVAL,

   TNT_TASK_ATTRS_CNT,
};

enum TNT_MutexAttr {
   TNT_MUTEX_ATTR__HOLDER,
   TNT_MUTEX_ATTR__LOCK_CNT,
   TNT_MUTEX_ATTR__EXISTS,

   TNT_MUTEX_ATTRS_CNT,
};

enum TNT_FmpAttr {
   TNT_FMP_ATTR__BLOCKS_CNT,
   TNT_FMP_ATTR__BLOCK_SIZE,
   TNT_FMP_ATTR__FREE_BLOCKS_CNT,
   TNT_FMP_ATTR__USED_BLOCKS_CNT,
   TNT_FMP_ATTR__EXISTS,

   TNT_FMP_ATTRS_CNT,
};

enum TNT_DQueueAttr {
   TNT_DQUEUE_ATTR__ITEMS_CNT,
   TNT_DQUEUE_ATTR__WRITTEN_ITEMS_CNT,
   TNT_DQUEUE_ATTR__FREE_ITEMS_CNT,
   TNT_DQUEUE_ATTR__EXISTS,
   TNT_DQUEUE_ATTR__CONNECTED_EVENT,
   TNT_DQUEUE_ATTR__CONNECTED_EVENT_PATTERN,

   TNT_DQUEUE_ATTRS_CNT,
};

enum TNT_SemAttr {
   TNT_SEM_ATTR__COUNT,
   TNT_SEM_ATTR__MAX_COUNT,
   TNT_SEM_ATTR__EXISTS,

   TNT_SEM_ATTRS_CNT,
};

enum TNT_EventAttr {
   TNT_EVENT_ATTR__PATTERN,
   TNT_EVENT_ATTR__EXISTS,

   TNT_EVENT_ATTRS_CNT,
};

enum TNT_ExchAttr {
   TNT_EXCH_ATTR__SIZE,
   TNT_EXCH_ATTR__EXISTS,

   TNT_EXCH_ATTRS_CNT,
};

enum TNT_SysAttr {
   TNT_SYS_ATTR__DEADLOCK_CNT,
   TNT_SYS_ATTR__DEADLOCK_FLAG,

   TNT_SYS_ATTRS_CNT,
};

//-- attrubutes of director task
enum TNT_DirectorAttr {
   TNT_DIRECTOR_ATTR__LAST_RETVAL,

   TNT_DIRECTOR_ATTRS_CNT,
};

//-- attrubutes of interrupt
enum TNT_InterruptAttr {
   TNT_INTERRUPT_ATTR__LAST_RETVAL,

   TNT_INTERRUPT_ATTRS_CNT,
};


/*******************************************************************************
 *    GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTION PROTOTYPES
 ******************************************************************************/


#endif // _TNTEST_TYPES_H


/*******************************************************************************
 *    end of file
 ******************************************************************************/


