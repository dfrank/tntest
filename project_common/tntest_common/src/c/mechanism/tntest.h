/*******************************************************************************
 *   Description:   TODO
 *
 ******************************************************************************/

#ifndef _TNTEST_H
#define _TNTEST_H

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "tworker_man.h"
#include "tntest_types.h"
#include "tntest_cfg.h"

/*******************************************************************************
 *    PUBLIC TYPES
 ******************************************************************************/


/*******************************************************************************
 *    GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

extern const char *title_mark;


/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

#define  TNT_TIMEOUT_AFTER_CMD            (80)

//-- used in tests to indicate task's base priority
#define  TNT_BASE_PRIORITY                (0xfff0)


#if TN_OLD_EVENT_API
#  define TNT_EVENT_NORMAL_ATTR TN_EVENTGRP_ATTR_MULTI
#else
#  define TNT_EVENT_NORMAL_ATTR (0)
#endif



#define  TNT_TEST_TITLE(str, ...)       TNT_DTMSG_COMMENT("%s " str " %s", title_mark, ## __VA_ARGS__, title_mark)
#define  TNT_TEST_COMMENT(str, ...)     TNT_DTMSG_COMMENT("//-- " str "", ## __VA_ARGS__); tnt_file_line_echo(__LINE__, __FILE__)


//-- Waiting {{{

#define  TNT_ITEM__WAIT(_timeout_)                                         \
   tnt_item_proceed__wait((_timeout_), false)

#define  TNT_ITEM__WAIT_STD_DTERM()                                        \
   tnt_item_proceed__wait(TNT_TIMEOUT_AFTER_CMD, true)

//}}}

//-- Objects management by test director: ..._MANAGE() {{{

#define  TNT_ITEM__TASK_MANAGE(_act_, _task_, _priority_)                  \
   tnt_item_proceed__task_manage(TNT_OBJ_ACT__##_act_, (_task_), (_priority_))

#define  TNT_ITEM__FMP_MANAGE(_act_, _fmp_, _start_addr_, _block_size_, _blocks_cnt_)   \
   tnt_item_proceed__fmp_manage(TNT_OBJ_ACT__##_act_, (_fmp_), (_start_addr_), (_block_size_), (_blocks_cnt_))

#define  TNT_ITEM__DQUEUE_MANAGE(_act_, _dqueue_, _data_fifo_, _items_cnt_)   \
   tnt_item_proceed__dqueue_manage(TNT_OBJ_ACT__##_act_, (_dqueue_), (_data_fifo_), (_items_cnt_))

#define  TNT_ITEM__SEM_MANAGE(_act_, _sem_, _start_value_, _max_val_)   \
   tnt_item_proceed__sem_manage(TNT_OBJ_ACT__##_act_, (_sem_), (_start_value_), (_max_val_))

#define  TNT_ITEM__EVENT_MANAGE(_act_, _event_, _attr_, _pattern_)   \
   tnt_item_proceed__event_manage(TNT_OBJ_ACT__##_act_, (_event_), (_attr_), (_pattern_))

#define  TNT_ITEM__MUTEX_MANAGE(_act_, _mutex_, _attr_, _ceil_priority_)   \
   tnt_item_proceed__mutex_manage(TNT_OBJ_ACT__##_act_, (_mutex_), (_attr_), (_ceil_priority_))

#if 0
#define  TNT_ITEM__EXCH_MANAGE(_act_, _exch_, _data_, _size_)   \
   tnt_item_proceed__exch_manage(TNT_OBJ_ACT__##_act_, (_exch_), (_data_), (_size_))
#endif

// }}}

//-- Sending commands to worker tasks: SEND_CMD_... {{{

#define  TNT_ITEM__SEND_CMD_GENERAL(_task_, _cmd_)                         \
   tnt_item_proceed__send_cmd_general((_task_), TWORKMAN_TASK_CMD__##_cmd_)

#define  TNT_ITEM__SEND_CMD_FMP(_task_, _cmd_, _fmp_, _p_data_, _timeout_)             \
   tnt_item_proceed__send_cmd_fmp((_task_), TWORKMAN_TASK_CMD__##_cmd_, (_fmp_), (_p_data_), (_timeout_))

#define  TNT_ITEM__SEND_CMD_DQUEUE(_task_, _cmd_, _dqueue_, _p_data_, _timeout_)             \
   tnt_item_proceed__send_cmd_dqueue((_task_), TWORKMAN_TASK_CMD__##_cmd_, (_dqueue_), (_p_data_), (_timeout_))

#define  TNT_ITEM__SEND_CMD_DQUEUE_EVENT(_task_, _cmd_, _dqueue_, _event_, _pattern_)             \
   tnt_item_proceed__send_cmd_dqueue_event((_task_), TWORKMAN_TASK_CMD__##_cmd_, (_dqueue_), (_event_), (_pattern_))

#define  TNT_ITEM__SEND_CMD_SEM(_task_, _cmd_, _sem_, _timeout_)             \
   tnt_item_proceed__send_cmd_sem((_task_), TWORKMAN_TASK_CMD__##_cmd_, (_sem_), (_timeout_))

#define  TNT_ITEM__SEND_CMD_EVENT(_task_, _cmd_, _event_, _wait_pattern_, _wait_mode_, _p_flags_pattern_, _timeout_, _operation_)         \
   tnt_item_proceed__send_cmd_event((_task_), TWORKMAN_TASK_CMD__##_cmd_, (_event_), (_wait_pattern_), (_wait_mode_), (_p_flags_pattern_), (_timeout_), (_operation_))

#define  TNT_ITEM__SEND_CMD_MUTEX(_task_, _cmd_, _mutex_)                  \
   tnt_item_proceed__send_cmd_mutex((_task_), TWORKMAN_TASK_CMD__##_cmd_, (_mutex_), TN_WAIT_INFINITE)

#define  TNT_ITEM__SEND_CMD_MUTEX_TO(_task_, _cmd_, _mutex_, _timeout_)    \
   tnt_item_proceed__send_cmd_mutex((_task_), TWORKMAN_TASK_CMD__##_cmd_, (_mutex_), (_timeout_))

#define  TNT_ITEM__SEND_CMD_TASK(_task_, _cmd_, _tgt_task_)                \
   tnt_item_proceed__send_cmd_task((_task_), TWORKMAN_TASK_CMD__##_cmd_, (_tgt_task_))

#define  TNT_ITEM__SEND_CMD_EXMODE(_task_, _exmode_)                       \
   tnt_item_proceed__send_cmd_exmode((_task_), TWORKMAN_EXMODE__##_exmode_)

#define  TNT_ITEM__SEND_CMD_SLEEP(_task_, _cmd_, _timeout_)                         \
   tnt_item_proceed__send_cmd_sleep((_task_), TWORKMAN_TASK_CMD__##_cmd_, _timeout_)

#define  TNT_ITEM__SEND_CMD_SYS(_task_, _cmd_, _p1_, _p2_, _p3_)                         \
   tnt_item_proceed__send_cmd_sys((_task_), TWORKMAN_TASK_CMD__##_cmd_, (_p1_), (_p2_), (_p3_))

// }}}

//-- Sending commands to worker interrupt: INT_SEND_CMD_... {{{

#define  TNT_ITEM__INT_SEND_CMD_FMP(_cmd_, _fmp_, _p_data_)                  \
   tnt_item_proceed__int_send_cmd_fmp(TWORKMAN_TASK_CMD__##_cmd_, (_fmp_), (_p_data_))

#define  TNT_ITEM__INT_SEND_CMD_DQUEUE(_cmd_, _dqueue_, _p_data_)             \
   tnt_item_proceed__int_send_cmd_dqueue(TWORKMAN_TASK_CMD__##_cmd_, (_dqueue_), (_p_data_))

#define  TNT_ITEM__INT_SEND_CMD_SEM(_cmd_, _sem_)             \
   tnt_item_proceed__int_send_cmd_sem(TWORKMAN_TASK_CMD__##_cmd_, (_sem_))

#define  TNT_ITEM__INT_SEND_CMD_EVENT(_cmd_, _event_, _wait_pattern_, _wait_mode_, _p_flags_pattern_, _operation_)         \
   tnt_item_proceed__int_send_cmd_event(TWORKMAN_TASK_CMD__##_cmd_, (_event_), (_wait_pattern_), (_wait_mode_), (_p_flags_pattern_), (_operation_))

#define  TNT_ITEM__INT_SEND_CMD_TASK(_cmd_, _task_)                  \
   tnt_item_proceed__int_send_cmd_task(TWORKMAN_TASK_CMD__##_cmd_, (_task_))

#define  TNT_ITEM__INT_SEND_CMD_MUTEX(_cmd_, _mutex_)                  \
   tnt_item_proceed__int_send_cmd_mutex(TWORKMAN_TASK_CMD__##_cmd_, (_mutex_))

#define  TNT_ITEM__INT_SEND_CMD_SLEEP(_cmd_, _timeout_)                         \
   tnt_item_proceed__int_send_cmd_sleep(TWORKMAN_TASK_CMD__##_cmd_, _timeout_)

#define  TNT_ITEM__INT_SEND_CMD_SYS(_cmd_, _p1_, _p2_, _p3_)                         \
   tnt_item_proceed__int_send_cmd_sys(TWORKMAN_TASK_CMD__##_cmd_, (_p1_), (_p2_), (_p3_))

// }}}

//-- Check: CHECK(), CHECK_DIFF(), WAIT_AND_CHECK(), WAIT_AND_CHECK_DIFF() {{{

#define  TNT_ITEM__CHECK_DIFF(_code_) {                                    \
   struct TNT_TestItem *p_item;                                            \
   p_item = tnt_item_create__check(tnt_prev_check_item__get());            \
   { _code_ }                                                              \
   tnt_item_proceed(p_item);                                               \
   tnt_item_delete(p_item);                                                \
}

#define  TNT_ITEM__CHECK(_code_) {                                         \
   struct TNT_TestItem *p_item;                                            \
   p_item = tnt_item_create__check(NULL);                                  \
   { _code_ }                                                              \
   tnt_item_proceed(p_item);                                               \
   tnt_item_delete(p_item);                                                \
}

#define  TNT_ITEM__WAIT_AND_CHECK(_code_) {                                \
   TNT_ITEM__WAIT(TNT_TIMEOUT_AFTER_CMD);                                  \
   TNT_ITEM__CHECK(_code_);                                                \
}

#define  TNT_ITEM__WAIT_AND_CHECK_DIFF(_code_) {                           \
   TNT_ITEM__WAIT_STD_DTERM();                                             \
   TNT_ITEM__CHECK_DIFF(_code_);                                           \
}

//}}}

//-- Check constraints (to be used inside CHECK macros): TNT_CHECK__... {{{

#define  TNT_CHECK__FMP(_fmp_, _attr_, _value_)                          \
   tnt_check__set_fmp_constr(p_item, (_fmp_), TNT_FMP_ATTR__##_attr_, (_value_))

#define  TNT_CHECK__DQUEUE(_dqueue_, _attr_, _value_)                          \
   tnt_check__set_dqueue_constr(p_item, (_dqueue_), TNT_DQUEUE_ATTR__##_attr_, (_value_))

#define  TNT_CHECK__SEM(_sem_, _attr_, _value_)                          \
   tnt_check__set_sem_constr(p_item, (_sem_), TNT_SEM_ATTR__##_attr_, (_value_))

#define  TNT_CHECK__EVENT(_event_, _attr_, _value_)                          \
   tnt_check__set_event_constr(p_item, (_event_), TNT_EVENT_ATTR__##_attr_, (_value_))

#define  TNT_CHECK__TASK(_task_, _attr_, _value_)                          \
   tnt_check__set_task_constr(p_item, (_task_), TNT_TASK_ATTR__##_attr_, (_value_))

#define  TNT_CHECK__MUTEX(_mutex_, _attr_, _value_)                        \
   tnt_check__set_mutex_constr(p_item, (_mutex_), TNT_MUTEX_ATTR__##_attr_, (_value_))

#define  TNT_CHECK__SYS(_attr_, _value_)                                   \
   tnt_check__set_sys_constr(p_item, TNT_SYS_ATTR__##_attr_, (_value_))

#define  TNT_CHECK__DIRECTOR(_attr_, _value_)                              \
   tnt_check__set_director_constr(p_item, TNT_DIRECTOR_ATTR__##_attr_, (_value_))

#define  TNT_CHECK__INTERRUPT(_attr_, _value_)                              \
   tnt_check__set_interrupt_constr(p_item, TNT_INTERRUPT_ATTR__##_attr_, (_value_))

#if 0
#define  TNT_CHECK__EXCH(_exch_, _attr_, _value_)                          \
   tnt_check__set_exch_constr(p_item, (_exch_), TNT_EXCH_ATTR__##_attr_, (_value_))
#endif

// }}}



/*******************************************************************************
 *    PUBLIC FUNCTION PROTOTYPES
 ******************************************************************************/

//-- tnt_item_create__... (normally you use only tnt_item_create__check() from them) {{{

//-- check {{{

struct TNT_TestItem *tnt_item_create__check(
      struct TNT_TestItem *p_prev_item
      );

//}}}

//-- wait {{{

struct TNT_TestItem *tnt_item_create__wait(
      TN_Timeout timeout,
      bool bool_ensure_dterm_finished
      );

//}}}

//-- Objects management by test director: ..._manage {{{

struct TNT_TestItem *tnt_item_create__task_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_TaskNum     task_num,
      int                  priority
      );

struct TNT_TestItem *tnt_item_create__fmp_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_FmpNum      fmp_num,
      void                *start_addr,
      unsigned int         block_size,
      int                  blocks_cnt
      );

struct TNT_TestItem *tnt_item_create__mutex_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_MutexNum    mutex_num,
      int                  attribute,
      int                  ceil_priority
      );

struct TNT_TestItem *tnt_item_create__dqueue_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_DQueueNum   dqueue_num,
      void               **data_fifo,
      int                  items_cnt
      );

struct TNT_TestItem *tnt_item_create__sem_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_SemNum      sem_num,
      int                  start_value,
      int                  max_val
      );

struct TNT_TestItem *tnt_item_create__event_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_EventNum    event_num,
      enum TN_EGrpAttr     attr,
      unsigned int         pattern
      );

#if 0
struct TNT_TestItem *tnt_item_create__exch_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_ExchNum     exch_num,
      TN_UWord            *data,
      unsigned int         size
      );
#endif

//}}}

//-- Sending commands to worker tasks: send_cmd_... {{{

struct TNT_TestItem *tnt_item_create__send_cmd_general(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd
      );

struct TNT_TestItem *tnt_item_create__send_cmd_fmp(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_FmpNum fmp_num,
      void *p_data,
      TN_Timeout timeout
      );

struct TNT_TestItem *tnt_item_create__send_cmd_dqueue(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_DQueueNum dqueue_num,
      void *p_data,
      TN_Timeout timeout
      );

struct TNT_TestItem *tnt_item_create__send_cmd_dqueue_event(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_DQueueNum dqueue_num,
      enum TNT_EventNum eventgrp_num,
      TN_UWord pattern
      );

struct TNT_TestItem *tnt_item_create__send_cmd_sem(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_SemNum sem_num,
      TN_Timeout timeout
      );

struct TNT_TestItem *tnt_item_create__send_cmd_event(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_EventNum event_num,
      unsigned int         wait_pattern,
      enum TN_EGrpWaitMode wait_mode,
      unsigned int        *p_flags_pattern,
      unsigned long        timeout,
      enum TN_EGrpOp       operation
      );

struct TNT_TestItem *tnt_item_create__send_cmd_mutex(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_MutexNum mutex_num,
      TN_Timeout timeout
      );

struct TNT_TestItem *tnt_item_create__send_cmd_task(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_TaskNum tgt_task_num
      );

struct TNT_TestItem *tnt_item_create__send_cmd_exmode(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_ExMode exmode
      );

struct TNT_TestItem *tnt_item_create__send_cmd_sleep(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      TN_Timeout timeout
      );

struct TNT_TestItem *tnt_item_create__send_cmd_sys(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      long p1,
      long p2,
      long p3
      );

// }}}

//-- Sending commands to worker interrupt: int_send_cmd_... {{{

struct TNT_TestItem *tnt_item_create__int_send_cmd_task(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_TaskNum tgt_task_num
      );

struct TNT_TestItem *tnt_item_create__int_send_cmd_fmp(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_FmpNum fmp_num,
      void *p_data
      );

struct TNT_TestItem *tnt_item_create__int_send_cmd_dqueue(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_DQueueNum dqueue_num,
      void *p_data
      );

struct TNT_TestItem *tnt_item_create__int_send_cmd_sem(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_SemNum sem_num
      );

struct TNT_TestItem *tnt_item_create__int_send_cmd_event(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_EventNum event_num,
      unsigned int         wait_pattern,
      enum TN_EGrpWaitMode   wait_mode,
      unsigned int        *p_flags_pattern,
      enum TN_EGrpOp       operation
      );

struct TNT_TestItem *tnt_item_create__int_send_cmd_mutex(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_MutexNum mutex_num
      );

struct TNT_TestItem *tnt_item_create__int_send_cmd_sleep(
      enum E_TWorkMan_TaskCmd cmd,
      TN_Timeout timeout
      );

struct TNT_TestItem *tnt_item_create__int_send_cmd_sys(
      enum E_TWorkMan_TaskCmd cmd,
      long p1,
      long p2,
      long p3
      );

// }}}

// }}}

//-- tnt_item_proceed__... {{{

//-- wait {{{

void tnt_item_proceed__wait(TN_Timeout timeout, bool bool_ensure_dterm_finished);

//}}}

//-- Objects management by test director: ..._manage {{{

void tnt_item_proceed__task_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_TaskNum     task_num,
      int                  priority
      );


void tnt_item_proceed__fmp_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_FmpNum      fmp_num,
      void                *start_addr,
      unsigned int         block_size,
      int                  blocks_cnt
      );

void tnt_item_proceed__dqueue_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_DQueueNum   dqueue_num,
      void               **data_fifo,
      int                  items_cnt
      );

void tnt_item_proceed__sem_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_SemNum      sem_num,
      int                  start_value,
      int                  max_val
      );

void tnt_item_proceed__event_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_EventNum    event_num,
      enum TN_EGrpAttr     attr,
      unsigned int         pattern
      );

void tnt_item_proceed__mutex_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_MutexNum    mutex_num,
      int                  attribute,
      int                  ceil_priority
      );

#if 0
void tnt_item_proceed__exch_manage(
      enum TNT_ObjAction   obj_action,
      enum TNT_ExchNum     exch_num,
      TN_UWord            *data,
      unsigned int         size
      );
#endif

//}}}

//-- Sending commands to worker tasks: send_cmd_... {{{

void tnt_item_proceed__send_cmd_general(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd
      );

void tnt_item_proceed__send_cmd_fmp(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_FmpNum fmp_num,
      void *p_data,
      TN_Timeout timeout
      );

void tnt_item_proceed__send_cmd_dqueue(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_DQueueNum dqueue_num,
      void *p_data,
      TN_Timeout timeout
      );

void tnt_item_proceed__send_cmd_dqueue_event(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_DQueueNum dqueue_num,
      enum TNT_EventNum eventgrp_num,
      TN_UWord pattern
      );

void tnt_item_proceed__send_cmd_sem(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_SemNum sem_num,
      TN_Timeout timeout
      );

void tnt_item_proceed__send_cmd_event(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_EventNum event_num,
      unsigned int         wait_pattern,
      enum TN_EGrpWaitMode   wait_mode,
      unsigned int        *p_flags_pattern,
      unsigned long        timeout,
      enum TN_EGrpOp       operation
      );

void tnt_item_proceed__send_cmd_mutex(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_MutexNum mutex_num,
      TN_Timeout timeout
      );

void tnt_item_proceed__send_cmd_task(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_TaskNum tgt_task_num
      );

void tnt_item_proceed__send_cmd_exmode(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_ExMode exmode
      );

void tnt_item_proceed__send_cmd_sleep(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      TN_Timeout timeout
      );

void tnt_item_proceed__send_cmd_sys(
      enum TNT_TaskNum task_num,
      enum E_TWorkMan_TaskCmd cmd,
      long p1,
      long p2,
      long p3
      );

//}}}

//-- Sending commands to worker interrupt: int_send_cmd_... {{{

void tnt_item_proceed__int_send_cmd_fmp(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_FmpNum fmp_num,
      void *p_data
      );

void tnt_item_proceed__int_send_cmd_task(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_TaskNum tgt_task_num
      );

void tnt_item_proceed__int_send_cmd_dqueue(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_DQueueNum dqueue_num,
      void *p_data
      );

void tnt_item_proceed__int_send_cmd_sem(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_SemNum sem_num
      );

void tnt_item_proceed__int_send_cmd_event(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_EventNum event_num,
      unsigned int         wait_pattern,
      enum TN_EGrpWaitMode   wait_mode,
      unsigned int        *p_flags_pattern,
      enum TN_EGrpOp       operation
      );

void tnt_item_proceed__int_send_cmd_mutex(
      enum E_TWorkMan_TaskCmd cmd,
      enum TNT_MutexNum mutex_num
      );

void tnt_item_proceed__int_send_cmd_sleep(
      enum E_TWorkMan_TaskCmd cmd,
      TN_Timeout timeout
      );

void tnt_item_proceed__int_send_cmd_sys(
      enum E_TWorkMan_TaskCmd cmd,
      long p1,
      long p2,
      long p3
      );

//}}}

// }}}

//-- Check constraints: tnt_check__... {{{

void tnt_check__set_fmp_constr(
      struct TNT_TestItem *p_item,
      enum TNT_FmpNum fmp_num,
      enum TNT_FmpAttr fmp_attr,
      int attr_value
      );

void tnt_check__set_dqueue_constr(
      struct TNT_TestItem *p_item,
      enum TNT_DQueueNum dqueue_num,
      enum TNT_DQueueAttr dqueue_attr,
      int attr_value
      );

void tnt_check__set_sem_constr(
      struct TNT_TestItem *p_item,
      enum TNT_SemNum sem_num,
      enum TNT_SemAttr sem_attr,
      int attr_value
      );

void tnt_check__set_event_constr(
      struct TNT_TestItem *p_item,
      enum TNT_EventNum event_num,
      enum TNT_EventAttr event_attr,
      int attr_value
      );

void tnt_check__set_task_constr(
      struct TNT_TestItem *p_item,
      enum TNT_TaskNum task_num,
      enum TNT_TaskAttr task_attr,
      int attr_value
      );

void tnt_check__set_mutex_constr(
      struct TNT_TestItem *p_item,
      enum TNT_MutexNum mutex_num,
      enum TNT_MutexAttr mutex_attr,
      int attr_value
      );

void tnt_check__set_sys_constr(
      struct TNT_TestItem *p_item,
      enum TNT_SysAttr sys_attr,
      int attr_value
      );

void tnt_check__set_director_constr(
      struct TNT_TestItem *p_item,
      enum TNT_DirectorAttr director_attr,
      int attr_value
      );

void tnt_check__set_interrupt_constr(
      struct TNT_TestItem *p_item,
      enum TNT_InterruptAttr interrupt_attr,
      int attr_value
      );

#if 0
void tnt_check__set_exch_constr(
      struct TNT_TestItem *p_item,
      enum TNT_ExchNum exch_num,
      enum TNT_ExchAttr exch_attr,
      int attr_value
      );
#endif

// }}}

void tnt_item_proceed(const struct TNT_TestItem *p_item);
void tnt_item_delete(struct TNT_TestItem *p_item);


void tnt_file_line_echo(int line, const char *file);

struct TNT_TestItem *tnt_prev_check_item__get(void);
void tnt_prev_check_item__reset(void);





#endif // _TNTEST_H


/*******************************************************************************
 *    end of file
 ******************************************************************************/


