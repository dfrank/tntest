
#include "stm32f4xx.h"
#include <stdio.h>

#define LED0 (1 << 12)
#define LED1 (1 << 13)
#define LED2 (1 << 14)
#define LED3 (1 << 15)


volatile int a;

/**
 * @brief  Implementation of fputc using the DBGU as the standard output. Required
 *         for printf().
 *
 * @param c        Character to write.
 * @param pStream  Output stream.
 * @param The character written if successful, or -1 if the output stream is
 *        not stdout or stderr.
 */
signed int fputc(signed int c, FILE *pStream)
{
   // Функции отладочного вывода описаны где-то в CMSIS
   return ITM_SendChar(c);

#if 0
   if ((pStream == stdout) || (pStream == stderr)) {

      PrintChar(c);

      return c;
   }
   else {

      return EOF;
   }
#endif
}


int main(void)
{

	   //-- configure LED port pins
	   {
	      RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN; // Enable Port D clock

	      //-- Set pin 12, 13, 14, 15 as general purpose output mode (pull-push)
	      GPIOD->MODER |= (0
	            | GPIO_MODER_MODER12_0
	            | GPIO_MODER_MODER13_0
	            | GPIO_MODER_MODER14_0
	            | GPIO_MODER_MODER15_0
	            ) ;

	      // GPIOD->OTYPER |= 0; //-- No need to change - use pull-push output

	      GPIOD->OSPEEDR |= (0
	            | GPIO_OSPEEDER_OSPEEDR12 // 100MHz operations
	            | GPIO_OSPEEDER_OSPEEDR13
	            | GPIO_OSPEEDER_OSPEEDR14
	            | GPIO_OSPEEDER_OSPEEDR15
	            );

	      GPIOD->PUPDR = 0; // No pull up, no pull down
	   }


    while(1)
    {

    	a = 0;
    	a = 1;

    	int i = 0;
    	for (i = 0; i < 10000; i++){
    	      if (GPIOD->ODR & LED3){
    	         GPIOD->BSRRH = LED3;
    	      } else {
    	         GPIOD->BSRRL = LED3;
    	      }

    	      printf("switch!\n");
    	}
    }
}
