/*******************************************************************************
 *   Description:   TODO
 *
 ******************************************************************************/

#ifndef _TNTEST_CFG_H
#define _TNTEST_CFG_H

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "tmalloc.h"
#include "dterm.h"
#include "appl_cfg__dbg__deb_halt.h"

#include "perfmeas.h"

#include "tworker_man_arch.h"
#include "bsp_tnkernel.h"

#include "bsp_led.h"


/*******************************************************************************
 *    PUBLIC TYPES
 ******************************************************************************/

/*******************************************************************************
 *    GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

#define  LOG_ON      1

#if LOG_ON
#  define  LOG_DEFINE(...)   bsp_led__toggle_process_pin(); __VA_ARGS__
#else
#  define  LOG_DEFINE(...)   bsp_led__toggle_process_pin();
#endif

//-- heap -------------------

#define  TNT_MALLOC(...)      tmalloc(__VA_ARGS__)
#define  TNT_FREE(...)        tfree(__VA_ARGS__)




//-- log -------------------

#define  TNT_DTMSG_BARE(str, ...)     LOG_DEFINE(printf(str "\n", ## __VA_ARGS__))

#define  TNT_DTMSG_I(str, ...)     LOG_DEFINE(printf("[I] " str "\n", ## __VA_ARGS__))
#define  TNT_DTMSG_W(str, ...)     LOG_DEFINE(printf("[W] " str "\n", ## __VA_ARGS__))
#define  TNT_DTMSG_E(str, ...)     printf("[E] " str "\n", ## __VA_ARGS__)

#define  TNT_DTMSG_SIMP_I(str, ...)     LOG_DEFINE(printf("[I] " str "\n", ## __VA_ARGS__))
#define  TNT_DTMSG_SIMP_E(str, ...)     printf("[E] " str "\n", ## __VA_ARGS__)

#define  TNT_DTMSG_I_INT(str, ...)     //printf("[I] " str "\n", ## __VA_ARGS__)
#define  TNT_DTMSG_W_INT(str, ...)     //printf("[W] " str "\n", ## __VA_ARGS__)
#define  TNT_DTMSG_E_INT(str, ...)     printf("[E] " str "\n", ## __VA_ARGS__)

#define  TNT_DTMSG_SIMP_I_INT(str, ...)     //printf("[I] " str "\n", ## __VA_ARGS__)
#define  TNT_DTMSG_SIMP_E_INT(str, ...)     printf("[E] " str "\n", ## __VA_ARGS__)


//-- log comments --------------------

#define  TNT_DTMSG_COMMENT(str, ...)       LOG_DEFINE(printf(str "\n", ## __VA_ARGS__))



//-- performance measurements -------------------

#define  TNT_PERFMEAS__CUR_VALUE__GET(...)            0
#define  TNT_PERFMEAS__DIFF__GET_CYCLES(start, end)   0
#define  TNT_PERFMEAS__DIFF__GET_NS(start, end)       0


//-- debugger halt -------------------

#define  TNT_DEB_HALT(...)     APPL_DEB_HALT(__VA_ARGS__)


//-- arch-dependent part of tworker man -------------------

#define  TNT_TWORKER_MAN_ARCH_INIT()   tworker_man_arch__init();


#define  TNT_TESTS_PASSED()            bsp_led__tests_passed()

/*******************************************************************************
 *    PUBLIC FUNCTION PROTOTYPES
 ******************************************************************************/


#endif // _TNTEST_CFG_H


/*******************************************************************************
 *    end of file
 ******************************************************************************/


