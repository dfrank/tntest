/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "tworker_man_arch.h"
#include "bsp_mcu_config.h"
#include "bsp_tnkernel.h"
#include "tworker_man.h"

#include "stm32f4xx.h"
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_tim.h>

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

#define TWORKMAN_TMR                   TIM4
#define TWORKMAN_TMR_INT_REQ           TIM4_IRQn
#define TWORKMAN_TMR_INT_HNDL__SYS()   void TIM4_IRQHandler()
#define TWORKMAN_TMR_APB_CLOCK_CMD     RCC_APB1PeriphClockCmd
#define TWORKMAN_TMR_APB_PERIPH        RCC_APB1Periph_TIM4

#define TWORKMAN_TMR_PRESCALER         (16800 / 4)
#define TWORKMAN_TMR_PR                5

//#define TWORKMAN_TMR_BUS               TIM4_IRQn

#if 0
#define TWORKMAN_TMR_PRESCALER         TMR_PRESCALE_VALUE_8
#define TWORKMAN_PRESCALER_HUMAN_VALUE 8

#define TWORKMAN_TMR_PR                (BSP_FPB / TWORKMAN_PRESCALER_HUMAN_VALUE / 12345/*times per second*/)
#if (TWORKMAN_TMR_PR > 0xffff)
#  error TWORKMAN_TMR_PR is too large
#endif

#if (TWORKMAN_TMR_PR < 2)
#  error TWORKMAN_TMR_PR is too small
#endif

#endif




/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

TIM_TimeBaseInitTypeDef timer;

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

int tworker_man_init_tmp = 0;

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void tworker_man_arch__init(void)
{
   //-- enable clocking of needed bus
   TWORKMAN_TMR_APB_CLOCK_CMD(TWORKMAN_TMR_APB_PERIPH, ENABLE);

   //-- fill structure timer with default values
   TIM_TimeBaseStructInit(&timer);
   timer.TIM_Prescaler = TWORKMAN_TMR_PRESCALER;
   timer.TIM_Period = TWORKMAN_TMR_PR;

   //-- init timer
   TIM_TimeBaseInit(TWORKMAN_TMR, &timer);


   //-- set interrupt to fire on timer overflow
   TIM_ITConfig(TWORKMAN_TMR, TIM_IT_Update, ENABLE);

   //-- start timer
   TIM_Cmd(TWORKMAN_TMR, ENABLE);

   //-- set highest interrupt priority
   NVIC_SetPriority(TWORKMAN_TMR_INT_REQ, 0xff);

   //-- enable timer interrupt
   NVIC_EnableIRQ(TWORKMAN_TMR_INT_REQ);

   tworker_man_init_tmp = 1;
}

void test(void)
{
   tworker_man_init_tmp = 1;
}

TWORKMAN_TMR_INT_HNDL__SYS()
{
   TIM_ClearITPendingBit(TWORKMAN_TMR, TIM_IT_Update);

   test();
   tworker_man__int_handler();
}


/*******************************************************************************
 *    end of file
 ******************************************************************************/



