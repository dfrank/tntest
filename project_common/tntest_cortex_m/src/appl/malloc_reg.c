/***************************************************************************************************
 *   Project:       P-0173 - БК-90 - интерфейс для БК-100
 *   Author:        Dmitry Frank
 ***************************************************************************************************
 *   Distribution:  Copyright © 2009-2010 Орион
 *
 ***************************************************************************************************
 *   MCU Family:    PIC24FJ256GB106
 *   Compiler:      Microchip C30 3.22
 ***************************************************************************************************
 *   File:          
 *   Description:   
 *
 ***************************************************************************************************
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include <csp_types.h>
#include "dterm.h"
#include "appl_cfg/appl_cfg__dbg__malloc_reg.h"
#include "appl_cfg/appl_cfg__dbg__deb_halt.h"

#include "malloc_reg.h"
#include "dfc.h"


#include "tmalloc.h"

#ifndef APPL_MALLOC_REG_ENABLED
#  error APPL_MALLOC_REG_ENABLED should be defined
#endif

#if APPL_MALLOC_REG_ENABLED

#ifndef APPL_MALLOC_REG__SAVE_FILE
#  error APPL_MALLOC_REG__SAVE_FILE should be defined
#endif

#ifndef APPL_MALLOC_REG__CHECK_MAX_BLOCK_SIZE
#  error APPL_MALLOC_REG__CHECK_MAX_BLOCK_SIZE should be defined
#endif



#include "umm_malloc/umm_malloc.h"
#include "appl_cfg__malloc.h"
#include "appl_cfg.h"

#ifndef APPL_CFG__MALLOC
#  error APPL_CFG__MALLOC is not defined
#endif


/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

#define MEM_ALLOC_REGISTER_CNT               160 //-- max number of allocations

#define _MEM_CELL_OVERHEAD                   4  //-- amount of memory used for each malloc


/***************************************************************************************************
 *                                  PRIVATE FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

typedef struct S_MemAllocReg {
    U08 *pt;
    U16 memCnt;
#if APPL_MALLOC_REG__SAVE_FILE
    const char *file;
    U16 line;
#endif
} T_MemAllocReg;

typedef struct S_MemAllocRegAll {
    T_MemAllocReg aMemAllocReg[ MEM_ALLOC_REGISTER_CNT ];
    S16 allocatedSize;
    S16 used_cells;
} T_MemAllocRegAll;

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

T_MemAllocRegAll memAllocRegAll;

static S16 _max_allocated_size = 0;
static S16 _max_used_cells     = 0;

#if APPL_MALLOC_REG__CHECK_MAX_BLOCK_SIZE
static S16 _min_avail_block_size = 0x7fff;
#endif

/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                        PRIVATE FUNCTIONS
 **************************************************************************************************/

/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

enum E_MemAllocRegRes MemAllocRegister(
      U08_FAST pwMode, U08 * pt, U16 memCnt
#if APPL_MALLOC_REG__SAVE_FILE
      , const char *file, U16 line
#endif
      )
{
    enum E_MemAllocRegRes ret = MEM_ALLOC_REG_RES__OK;

    U16 i;
    T_MemAllocReg *curPt = &memAllocRegAll.aMemAllocReg[0];
    U08 bool_ok = 0;

    switch (pwMode){
        case MEM_ALLOC_REG_MODE_ALLOC:
            {
               //PRINTD_I("alloc %u bytes (%s : %u)", memCnt, file, line);
                for (i = 0; i < MEM_ALLOC_REGISTER_CNT;     i++, curPt++){
                    if (curPt->pt == 0){
                        memAllocRegAll.allocatedSize += memCnt;
                        memAllocRegAll.used_cells++;
                        curPt->pt = pt;
                        curPt->memCnt = memCnt;
#if APPL_MALLOC_REG__SAVE_FILE
                        curPt->file = file;
                        curPt->line = line;
#endif

                        bool_ok = 1;

                        if (memAllocRegAll.allocatedSize > _max_allocated_size){
                           _max_allocated_size = memAllocRegAll.allocatedSize;
                        }

                        if (memAllocRegAll.used_cells > _max_used_cells){
                           _max_used_cells = memAllocRegAll.used_cells;
                        }

                        break;
                    }
                }

#if APPL_MALLOC_REG__CHECK_MAX_BLOCK_SIZE
                {
                   int max_mem_part_size = tmalloc__get_max_mem_part();
                   if (max_mem_part_size < _min_avail_block_size){
                      _min_avail_block_size = max_mem_part_size;
                   }
                }
#endif


                if (!bool_ok){
                   ret = MEM_ALLOC_REG_RES__ERR_NO_FREE_CELLS;
                   DTMSG(DT_ERR | DT_SYNC, "No space for memory alloc reg!");
                   malloc_reg__mem_usage__report();
#if APPL_CFG__DEBUG
                   DFC_DEB_HALT();
#endif
                }
            }
            break;
        case MEM_ALLOC_REG_MODE_FREE:
            for (i = 0; i < MEM_ALLOC_REGISTER_CNT;     i++, curPt++){
                if (curPt->pt == pt){
                    memAllocRegAll.allocatedSize -= curPt->memCnt;
                    memAllocRegAll.used_cells--;
#if APPL_MALLOC_REG__SAVE_FILE
                    //PRINTD_I("free %u bytes (%s : %u)", curPt->memCnt, curPt->file, curPt->line);
#else
                    //PRINTD_I("free %u bytes", curPt->memCnt);
#endif
                    curPt->memCnt = 0;
                    curPt->pt = 0;
#if APPL_MALLOC_REG__SAVE_FILE
                    curPt->file = 0;
                    curPt->line = 0;
#endif
                    bool_ok = 1;
                    break;
                }
            }
            if (!bool_ok){
               ret = MEM_ALLOC_REG_RES__ERR_NOT_FOUND_REG;
               DTMSG(DT_ERR | DT_SYNC, "Not found reg! pt=0x%x", (uintptr_t)pt);
#if APPL_CFG__DEBUG
               DFC_DEB_HALT();
#endif
            }
            break;
        default:
            //TODO:error

            curPt->pt = NULL;
            curPt->memCnt = 0;
#if APPL_MALLOC_REG__SAVE_FILE
            curPt->file = file;
            curPt->line = line;
#endif
            break; 
    }

    return ret;
}

S16 malloc_reg__allocated_size__get(void)
{
   return memAllocRegAll.allocatedSize;
}

S16 malloc_reg__max_allocated_size__get(void)
{
   return _max_allocated_size;
}

S16 malloc_reg__max_used_cells__get(void)
{
   return _max_used_cells;
}

void malloc_reg__mem_usage__report(void)
{
   size_t i;
   T_MemAllocReg *p_cur_mem_alloc = memAllocRegAll.aMemAllocReg;
   DTMSG(DT_BARE, "== mem report ==");
   for (
         i = 0, p_cur_mem_alloc = memAllocRegAll.aMemAllocReg;
         i < MEM_ALLOC_REGISTER_CNT;
         i++, p_cur_mem_alloc++
         )
   {
      if (p_cur_mem_alloc->memCnt > 0){

         DTMSG(DT_BARE, "%d bytes, pt=0x%x, cell_num=%d"
#if APPL_MALLOC_REG__SAVE_FILE
               ", %s:%d"
#endif
               "\n"
               , p_cur_mem_alloc->memCnt
               , (unsigned int)p_cur_mem_alloc->pt
               , i
#if APPL_MALLOC_REG__SAVE_FILE
               , p_cur_mem_alloc->file, p_cur_mem_alloc->line
#endif
               );

      }
   }
   DTMSG(DT_BARE, "----\n");
   DTMSG(DT_BARE, "total allocated: %d (%d) bytes\n",
         (int)memAllocRegAll.allocatedSize,
         (int)(memAllocRegAll.allocatedSize + memAllocRegAll.used_cells * _MEM_CELL_OVERHEAD)
         );
   DTMSG(DT_BARE, "total cells used: %d\n", (int)memAllocRegAll.used_cells);
   DTMSG(DT_BARE, "max mem part size: %d\n", (int)tmalloc__get_max_mem_part());
   DTMSG(DT_BARE, "max allocated: %d (%d) bytes\n",
         (int)_max_allocated_size,
         (int)(_max_allocated_size + _max_used_cells * _MEM_CELL_OVERHEAD)
         );
   DTMSG(DT_BARE, "max cells used: %d\n", (int)_max_used_cells);
#if APPL_MALLOC_REG__CHECK_MAX_BLOCK_SIZE
   DTMSG(DT_BARE, "min max-mem-part-size: %d\n", (int)_min_avail_block_size);
#endif

#if APPL_CFG__MALLOC == APPL_CFG__MALLOC__UMM_MALLOC
   umm_info(NULL, 0);
   DTMSG(DT_BARE, "---- umm heap info ----\n");
   DTMSG(DT_BARE, "totalEntries=%d\n", (int)heapInfo.totalEntries);
   DTMSG(DT_BARE, "usedEntries=%d\n",  (int)heapInfo.usedEntries);
   DTMSG(DT_BARE, "freeEntries=%d\n",  (int)heapInfo.freeEntries);
   DTMSG(DT_BARE, "totalBlocks=%d\n",  (int)heapInfo.totalBlocks);
   DTMSG(DT_BARE, "usedBlocks=%d\n",   (int)heapInfo.usedBlocks);
   DTMSG(DT_BARE, "freeBlocks=%d\n",   (int)heapInfo.freeBlocks);
#endif
}


/***************************************************************************************************
 *  end of file
 **************************************************************************************************/

#endif // APPL_MALLOC_REG_ENABLED

