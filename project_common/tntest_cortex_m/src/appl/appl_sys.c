/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "dfc.h"
#include "csp_types.h"
#include "bsp_tnkernel.h"
#include "task_conf.h"
#include "bsp/bsp.h"

#include "dterm.h"

#include "appl_sys.h"
#include "appl_cfg/appl_cfg.h"
#include "appl_cfg/appl_cfg__dbg__deb_halt.h"

#include "appl_tntest/appl_tntest_timer.h"

#include "tntest_cfg.h" //-- for LOG_DEFINE()

#include "stm32f4xx.h"

#ifndef APPL_CFG__DEBUG
#  error APPL_CFG__DEBUG is not defined
#endif

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/



#define IDLE_TASK_STACK_SIZE           (TN_MIN_STACK_SIZE + 10)
#define INT_STACK_SIZE                 (TN_MIN_STACK_SIZE + 200)

//-- system timer cfg {{{
#define TN_SYS_TMR_INT_HNDL__SYS()     void SysTick_Handler(void)

#define TN_SYS_TMR_PRESCALER_VALUE     1

#define TN_SYS_TMR_PR                  (BSP_FOSC / TN_SYS_TMR_PRESCALER_VALUE / 1000/*Hz*/)

#define SYS_TIME_MS(a)                 (TN_TIMEOUT)((a) / TN_SYS_TMR_PR_MS)
// }}}


/***************************************************************************************************
 *                                  PRIVATE FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

TN_TASK_STACK_DEF(idle_task_stack,  IDLE_TASK_STACK_SIZE);
TN_TASK_STACK_DEF(interrupt_stack,  INT_STACK_SIZE);

volatile TN_Timeout appl_sys_tick_count = 0;
volatile TN_Timeout next_tick_count = 0;


/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

extern TN_UWORD task_conf_stack[];
extern TN_UWORD task_input_stack[];


/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/



/***************************************************************************************************
 *    INTERRUPT HANDLERS
 **************************************************************************************************/

extern int tworker_man_init_tmp;//TODO: remove
TN_SYS_TMR_INT_HNDL__SYS()
{
#if TN_DYNAMIC_TICK

   appl_sys_tick_count++;

#if 1
   if (next_tick_count > 0 && next_tick_count != TN_WAIT_INFINITE){
      next_tick_count--;

      if (next_tick_count == 0){
         tn_tick_int_processing();
      }
   }
#else
   tn_tick_int_processing();
#endif

#else
   tn_tick_int_processing();
#endif

   if (tworker_man_init_tmp){
      //tworker_man__int_handler();   //TODO: move to separate int
   }
   appl_tntest_timer__sys_int_check();
}


/***************************************************************************************************
 *                                        PRIVATE FUNCTIONS
 **************************************************************************************************/

#if TN_DYNAMIC_TICK

void _tick_schedule(TN_Timeout timeout)
{
   next_tick_count = timeout;
}

TN_TickCnt _tick_cnt_get(void)
{
   return appl_sys_tick_count;
}

#endif


static void _user_task_create (void)
{
   //-- create task_conf that will configure the system and create all other tasks
   task_conf__create();
}

/**
 *  Idle task hook function
 */
static void _idle_task_callback (void)
{
}

static void _tn_deadlock_callback(
      TN_BOOL active, struct TN_Mutex *mutex, struct TN_Task *task
      )
{
   //-- NOTE: called from interrupt with other interrupts disabled

   if (active){
      LOG_DEFINE(DTMSG(DT_WRN | DT_NOWAIT | DT_SIMPLE, "deadlock"));
   } else {
      LOG_DEFINE(DTMSG(DT_WRN | DT_NOWAIT | DT_SIMPLE, "deadlock off"));
   }
}

/**
 * System timer initialization
 */
static void _sys_timer_init(void)
{
   //-- set priority 1 - lower than priority for tworker_man
   //   (tworker_man_arch__init())
   NVIC_SetPriority(SysTick_IRQn, 0);

   //-- init system timer
   SysTick_Config(TN_SYS_TMR_PR);
}


/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

int fputc(int c, FILE *stream)
{
   return ITM_SendChar(c);
}

int main (void)
{
   //-- disable interrupts globally
   tn_arch_int_dis();

   SCB->SHCSR |= SCB_SHCSR_MEMFAULTENA_Msk; //Set bit 16
   SCB->SHCSR |= SCB_SHCSR_BUSFAULTENA_Msk; //Set bit 17
   SCB->SHCSR |= SCB_SHCSR_USGFAULTENA_Msk; //Set bit 18


   appl_tntest_timer__init();


   _sys_timer_init();

   tn_callback_deadlock_set(_tn_deadlock_callback);

#if TN_DYNAMIC_TICK
   tn_callback_dyn_tick_set(_tick_schedule, _tick_cnt_get);
#endif

   tn_sys_start(
         idle_task_stack,
         IDLE_TASK_STACK_SIZE,
         interrupt_stack,
         INT_STACK_SIZE,
         _user_task_create,
         _idle_task_callback
         );

   return 0;
}




//TODO: move to appl_ex.c

void HardFault_Handler(void)
{
   volatile int i;
   //DFC_DEB_HALT();
   for (;i != 100;){
      i = 1;
   }
   i = 2;
}

void BusFault_Handler(void)
{
   volatile int i;
   //DFC_DEB_HALT();
   for (;i != 100;){
      i = 1;
   }
   i = 2;
}

void UsageFault_Handler(void)
{
   volatile int i;
   //DFC_DEB_HALT();
   for (;i != 100;){
      i = 1;
   }
   i = 2;
}

void MemManage_Handler(void)
{
   volatile int i;
   //DFC_DEB_HALT();
   for (;i != 100;){
      i = 1;
   }
   i = 2;
}

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{ 
   /* User can add his own implementation to report the file name and line number,
ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

   /* Infinite loop */
   while (1)
   {
   }
}

/***************************************************************************************************
 *  end of file
 **************************************************************************************************/


