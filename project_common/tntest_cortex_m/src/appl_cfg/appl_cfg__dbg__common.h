/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

#ifndef _APPL_CFG__DBG__COMMON_H
#define _APPL_CFG__DBG__COMMON_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include "appl_cfg__dbg__malloc_reg.h"
#include "appl_cfg__dbg__deb_halt.h"
//#include "appl_cfg__dbg_uart.h"

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/


//#   define static_dbg  static
#   define static_dbg                  /*nothing*/


/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

#endif // _APPL_CFG__DBG__COMMON_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


