/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

#ifndef _BSP_TNKERNEL_H
#define _BSP_TNKERNEL_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/


#include "tn.h"


extern void appl__echo_err_halt__tn_err(enum TN_RCode tnret, int line);

#define SYSRETVAL_DATA        enum TN_RCode __sysret_check

#define SYSRETVAL_CHECK(x) \
   (((__sysret_check = (x)) != TN_RC_OK) ? (appl__echo_err_halt__tn_err(__sysret_check, __LINE__), __sysret_check) : __sysret_check)
   //{enum TN_RCode __rv = (x); if (__rv != TERR_NO_ERR){appl__echo_err_halt__tn_err(__rv, __LINE__);} }

#define SYSRETVAL_CHECK_TOUT(x) \
   (((__sysret_check = (x)) != TN_RC_OK && __sysret_check != TN_RC_TIMEOUT) ? (appl__echo_err_halt__tn_err(__sysret_check, __LINE__), __sysret_check) : __sysret_check)
   //{enum TN_RCode __rv = (x); if (__rv != TERR_NO_ERR && __rv != TERR_TIMEOUT){appl__echo_err_halt__tn_err(__rv, __LINE__);} }



/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

#define  TN_RETVAL   enum TN_RCode
#define  TN_UWORD    TN_UWord
#define  TN_TASK
#define  TN_DATA

#define  TASK_STACK_INTERRUPT_ADD   0


#define  BSP_TNKERNEL__MUTEX_PROTOCOL     TN_MUTEX_PROT_INHERIT
#define  BSP_TNKERNEL__CEIL_PRIORITY      2

/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

#endif // _BSP_TNKERNEL_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


