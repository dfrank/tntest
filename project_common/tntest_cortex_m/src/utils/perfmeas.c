/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "csp_types.h"
#include "perfmeas.h"
#include "bsp_perfmeas.h"
#include "bsp_mcu_config.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void perfmeas__init(void)
{

}

U32 perfmeas__diff__get_cycles(U32 start, U32 end/*if 0, current value is used*/)
{
   return 0;
}

U32 perfmeas__diff__get_ns(U32 start, U32 end/*if 0, current value is used*/)
{
   return 0;
}




U32 perfmeas__value_to_cycles(U32 value)
{
   return (U64)value * BSP_FOSC / BSP_FPB;
}

U32 perfmeas__value_to_ns(U32 value)
{
   return (U64)value * _BSP__NS_PER_SECOND / BSP_FPB;
}


/*******************************************************************************
 *    end of file
 ******************************************************************************/



