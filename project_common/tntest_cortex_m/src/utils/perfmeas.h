/*******************************************************************************
 *   Description:   TODO
 *
 ******************************************************************************/

#ifndef _PERFMEAS_H
#define _PERFMEAS_H

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

//-- these includes are needed for static inline functions
#include "csp_types.h"
#include "bsp_perfmeas.h"


/*******************************************************************************
 *    PUBLIC TYPES
 ******************************************************************************/

/*******************************************************************************
 *    GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTION PROTOTYPES
 ******************************************************************************/


void perfmeas__init(void);

static inline U32 perfmeas__cur_value__get(void)
{
   return 0;
}

U32 perfmeas__diff__get_cycles(U32 start, U32 end/*if 0, current value is used*/);
U32 perfmeas__diff__get_ns(U32 start, U32 end/*if 0, current value is used*/);


U32 perfmeas__value_to_cycles(U32 value);
U32 perfmeas__value_to_ns(U32 value);

#endif // _PERFMEAS_H


/*******************************************************************************
 *    end of file
 ******************************************************************************/


