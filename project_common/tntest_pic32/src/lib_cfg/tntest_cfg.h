/*******************************************************************************
 *   Description:   TODO
 *
 ******************************************************************************/

#ifndef _TNTEST_CFG_H
#define _TNTEST_CFG_H

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "tmalloc.h"
#include "dterm.h"
#include "appl_cfg__dbg__deb_halt.h"

#include "perfmeas.h"

#include "tworker_man_arch.h"



/*******************************************************************************
 *    PUBLIC TYPES
 ******************************************************************************/

/*******************************************************************************
 *    GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

//-- heap -------------------

#define  TNT_MALLOC(...)      tmalloc(__VA_ARGS__)
#define  TNT_FREE(...)        tfree(__VA_ARGS__)




//-- log -------------------

#define  TNT_DTMSG_BARE(...)     DTMSG(DT_BARE, __VA_ARGS__)

#define  TNT_DTMSG_I(...)     DTMSG_I(__VA_ARGS__)
#define  TNT_DTMSG_W(...)     DTMSG_W(__VA_ARGS__)
#define  TNT_DTMSG_E(...)     DTMSG_E(__VA_ARGS__)

#define  TNT_DTMSG_SIMP_I(...)     DTMSG(DT_SIMPLE | DT_INF, __VA_ARGS__)
#define  TNT_DTMSG_SIMP_E(...)     DTMSG(DT_SIMPLE | DT_ERR, __VA_ARGS__)

#define  TNT_DTMSG_I_INT(...)     DTMSG(DT_INF | DT_NOWAIT, __VA_ARGS__)
#define  TNT_DTMSG_W_INT(...)     DTMSG(DT_WRN | DT_NOWAIT, __VA_ARGS__)
#define  TNT_DTMSG_E_INT(...)     DTMSG(DT_ERR | DT_NOWAIT, __VA_ARGS__)

#define  TNT_DTMSG_SIMP_I_INT(...)     DTMSG(DT_SIMPLE | DT_INF | DT_NOWAIT, __VA_ARGS__)
#define  TNT_DTMSG_SIMP_E_INT(...)     DTMSG(DT_SIMPLE | DT_ERR | DT_NOWAIT, __VA_ARGS__)


//-- log comments --------------------

#define  TNT_DTMSG_COMMENT(...)       DTMSG(DT_BARE, __VA_ARGS__)



//-- performance measurements -------------------

#define  TNT_PERFMEAS__CUR_VALUE__GET(...)            perfmeas__cur_value__get()
#define  TNT_PERFMEAS__DIFF__GET_CYCLES(start, end)   perfmeas__diff__get_cycles(start, end)
#define  TNT_PERFMEAS__DIFF__GET_NS(start, end)       perfmeas__diff__get_ns(start, end)


//-- debugger halt -------------------

#define  TNT_DEB_HALT(...)     APPL_DEB_HALT(__VA_ARGS__)


//-- arch-dependent part of tworker man -------------------

#define  TNT_TWORKER_MAN_ARCH_INIT()   tworker_man_arch__init();



/*******************************************************************************
 *    PUBLIC FUNCTION PROTOTYPES
 ******************************************************************************/


#endif // _TNTEST_CFG_H


/*******************************************************************************
 *    end of file
 ******************************************************************************/


