/*
 * Author: Dmitry Frank
 *
 * http://dfrank.ru
 */

#ifndef _OBJ_COMMON_CFG_H
#define _OBJ_COMMON_CFG_H

#define     OBJ_COMMON__MALLOC(size)      tmalloc(size)
#define     OBJ_COMMON__FREE(pt)          tfree(pt)
#define     OBJ_COMMON__DEB_HALT(...)     APPL_DEB_HALT(__VA_ARGS__)

#define     OBJ_COMMON__EMPTY_FUNC_ERR_ECHO()   DTMSG_E("not implemented")

#include "tmalloc.h"
#include "dfc.h"
#include "dterm.h"
#include "appl_cfg__dbg__deb_halt.h"

#endif /* _OBJ_COMMON_CFG_H */

