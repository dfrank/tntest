/***************************************************************************************************
 *   Project:       P-0173 - ��-90 - ��������� ��� ��-100
 *   Author:        Alex Borisov
 ***************************************************************************************************
 *   Distribution:  Copyright � 2009-2010 �����
 *
 ***************************************************************************************************
 *   MCU Family:    PIC24FJ256GB106
 *   Compiler:      Microchip C30 3.22
 ***************************************************************************************************
 *   File:          appl_ex.c
 *   Description:   ������� ��������� ����������
 *
 ***************************************************************************************************
 *   History:       24.06.2010 - [Alex Borisov] - file created
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "bsp.h"                    /* Board Support Package        */

#include "bsp_tnkernel.h"

#include "dterm.h"

#include "appl_ex.h"
#include "mips_call_stack.h"

/***************************************************************************************************
 * DEFINITIONS
 **************************************************************************************************/


/***************************************************************************************************
 * PRIVATE TYPES
 **************************************************************************************************/

//    Exception handler: 
enum E_ApplEx_ExceptionCode {
   EXCEP_IRQ = 0,                // interrupt 
   EXCEP_AdEL = 4,               // address error exception (load or ifetch) 
   EXCEP_AdES,                   // address error exception (store) 
   EXCEP_IBE,                    // bus error (ifetch) 
   EXCEP_DBE,                    // bus error (load/store) 
   EXCEP_Sys,                    // syscall 
   EXCEP_Bp,                     // breakpoint 
   EXCEP_RI,                     // reserved instruction 
   EXCEP_CpU,                    // coprocessor unusable 
   EXCEP_Overflow,               // arithmetic overflow 
   EXCEP_Trap,                   // trap (possible divide by zero) 
   EXCEP_IS1 = 16,               // implementation specfic 1 
   EXCEP_CEU,                    // CorExtend Unuseable 
   EXCEP_C2E                     // coprocessor 2 
}; 





/***************************************************************************************************
 * PRIVATE DATA
 **************************************************************************************************/

static enum E_ApplEx_ExceptionCode _excep_code; 
static unsigned int  _excep_EPC; 
static unsigned int  _excep_BadVAddr;
static unsigned int  _excep_ReturnAddr;
static unsigned int  _excep_RIPL;   //-- requested interrupt priority level, from Cause register

static char *_excep_descr[] = {
   [ EXCEP_IRQ ]        = "interrupt",
   [ EXCEP_AdEL ]       = "Addr error exception (load or ifetch)",
   [ EXCEP_AdES ]       = "Addr error exception (store)",
   [ EXCEP_IBE ]        = "bus error (ifetch)",
   [ EXCEP_DBE ]        = "bus error (load/store)",
   [ EXCEP_Sys ]        = "syscall",
   [ EXCEP_Bp ]         = "breakpoint",
   [ EXCEP_RI ]         = "reserved instruction",
   [ EXCEP_CpU ]        = "coprocessor unusable",
   [ EXCEP_Overflow ]   = "arithmetic overflow",
   [ EXCEP_Trap ]       = "trap (possible divide by zero)",
   [ EXCEP_IS1 ]        = "implem. specific",
   [ EXCEP_CEU ]        = "CorExtend unusable",
   [ EXCEP_C2E ]        = "coprocessor 2",
};

//-- needed for the case when exception is occured in the call stack retrieving routine
static bool _bool_tried_to_calc_stack = false;




/***************************************************************************************************
 * PRIVATE FUNCTIONS
 **************************************************************************************************/

/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

/************************************************************
 *    Replace the normal _weak_ generic exception handler. 
 *    So there may be a message and a breakpoint.
 ***********************************************************/
void appl_ex__for_deb_halt(void)
{
   volatile int i = 0;
   i = 1;
   i = 2;

#ifndef __DEBUG
   while(1);
#endif
}

void __attribute__((nomips16)) _general_exception_handler(void) 
{
   unsigned int mod_addr; 
   asm volatile("mfc0 %0,$8" : "=r" (_excep_BadVAddr)); 
   asm volatile("mfc0 %0,$13" : "=r" (_excep_code)); 
   asm volatile("mfc0 %0,$14" : "=r" (_excep_EPC)); 
   asm volatile("move %0,$ra" : "=r" (_excep_ReturnAddr)); 

   _excep_code = ((_excep_code & 0x7c) >> 2);


   DTMSG(DT_ERR | DT_SYNC, "EXCEPTION code=%d, descr=\"%s\", EPC=0x%x, BadVAddr=0x%x, ret_addr=0x%x",
         _excep_code,
         _excep_descr[_excep_code],
         _excep_EPC,
         _excep_BadVAddr,
         _excep_ReturnAddr
         );

   if (!_bool_tried_to_calc_stack){
      _bool_tried_to_calc_stack = true;
      mips_call_stack__call_stack__print_err();
   } else {
      //-- error during retrieving call stack!
      DTMSG(DT_ERR | DT_SYNC, "exception during retrieving call stack");
   }

   DTMSG(DT_ERR | DT_SYNC, "-----------");


#ifdef __DEBUG

   DFC_DEB_HALT();
   //appl_ex__for_deb_halt();
#else
   while(1);                // Stay here.
#endif

   // Skip instruction causing the exception. 
   _excep_code = (_excep_code & 0x0000007C) >> 2; 
   mod_addr = _excep_EPC + 4; 
   asm volatile("mtc0 %0,$14" :: "r" (mod_addr)); 
}

void __attribute__((nomips16)) _general_exception_context(void) 
{
   _general_exception_handler();
}

void __attribute__((nomips16)) _DefaultInterrupt(void)
{
   asm volatile("mfc0 %0,$13" : "=r" (_excep_RIPL)); 
   _excep_RIPL = ((_excep_RIPL >> 10) & 0x3f);

   DTMSG(DT_ERR | DT_SYNC, "No ISR defined for vector=%d (see file %%compiler_dir%%/pic32mx/include/proc/%%proc%%.h for vector numbers)", _excep_RIPL);

#ifdef __DEBUG
   DFC_DEB_HALT();
#else
   while(1);                // Stay here.
#endif
}


/***************************************************************************************************
 *  end of file: appl_ex.c
 **************************************************************************************************/


