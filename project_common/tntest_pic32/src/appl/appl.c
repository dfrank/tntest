/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "dfc.h"
#include "dterm.h"
#include "appl.h"
#include "bsp_mcu_config.h"
#include "appl_sys.h"
#include "dterm.h"
#include "malloc_reg.h"

#include "appl_dterm_commands.h"
#include "task_input.h"

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

/***************************************************************************************************
 *                                  PRIVATE FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

enum E_Appl_DtermCmdGeneral {
   APPL_DTERM_CMD_GEN__STOP_ALL,           //-- params: none
   APPL_DTERM_CMD_GEN__MEM_USAGE_REPORT,   //-- params: none
};



/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                        PRIVATE FUNCTIONS
 **************************************************************************************************/

static void _dterm_rx_handler(intptr_t tmp_p_rx_buf)
{
   const T_DTermRxBuf *p_rx_buf = (const T_DTermRxBuf *)tmp_p_rx_buf;

   U16 bytes_left = p_rx_buf->len;
   const U08 *p_data    = p_rx_buf->data;

   if (bytes_left-- > 0){
      switch (*p_data++){

         case APPL_DTERM__ADDRESSEE__GENERAL:
            if (bytes_left-- > 0){
               switch (*p_data++){
                  case APPL_DTERM_CMD_GEN__MEM_USAGE_REPORT:
                     appl__mem_usage__report();
                     break;

               }
            }
            break;
      }
   }

}




/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

void appl__dterm_rx_handler(const T_DTermRxBuf *p_rx_buf)
{
   task_input__invoke_later_1par_i(_dterm_rx_handler, (intptr_t)p_rx_buf);
}

void appl__mem_usage__report(void)
{
   malloc_reg__mem_usage__report();
   DTMSG_I("dterm tx buf max usage: %d bytes", 
         dterm__tx_buf__max_used_size__get()
         );
}




void appl__fatal_error__report(enum E_ApplFatalError fatal_error_num)
{
   DTMSG_E("Appl fatal error %d", fatal_error_num);
   for (;;);
}

void appl__out_of_memory_msg_show(void)
{
   appl__fatal_error__report(APPL_FATAL_ERROR__OUT_OF_MEMORY);
}



void appl__echo_err__should_never_be_here(int line)
{
   DTMSG_E("should never be here, line=%d", (int)line);
}

void appl__echo_err__tn_err(TN_RETVAL tnret, int line)
{
   DTMSG_E("tn_retval=%d, line=%d", (int)tnret, (int)line);
}

void appl__echo_err_halt__tn_err(TN_RETVAL tnret, int line)
{
   DTMSG(DT_ERR | DT_SYNC, "tn_retval=%d, line=%d", (int)tnret, (int)line);
   DTMSG(DT_ERR | DT_SYNC, "tn_retval=%d, line=%d", (int)tnret, (int)line);
   DFC_DEB_HALT();
}


/***************************************************************************************************
 *  end of file
 **************************************************************************************************/


