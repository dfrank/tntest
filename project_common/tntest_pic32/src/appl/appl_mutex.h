/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

#ifndef _APPL_MUTEX_H
#define _APPL_MUTEX_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include "bsp_tnkernel.h"

#if 0
#include "tn_mutexrec.h"

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

typedef TN_MUTEXREC T_ApplMutex;

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

TN_RETVAL appl_mutex__create        (T_ApplMutex *me, TN_UWORD attribute, TN_UWORD ceil_priority);
TN_RETVAL appl_mutex__delete        (T_ApplMutex *me);
TN_RETVAL appl_mutex__lock          (T_ApplMutex *me, unsigned long timeout);
TN_RETVAL appl_mutex__lock_polling  (T_ApplMutex *me);
TN_RETVAL appl_mutex__unlock        (T_ApplMutex *me);

#else

typedef TN_MUTEX T_ApplMutex;

#define     appl_mutex__create(...)             SYSRETVAL_CHECK(tn_mutex_create(__VA_ARGS__))
#define     appl_mutex__delete(...)             SYSRETVAL_CHECK(tn_mutex_delete(__VA_ARGS__))
#define     appl_mutex__lock(...)               SYSRETVAL_CHECK(tn_mutex_lock(__VA_ARGS__))
#define     appl_mutex__lock_polling(...)       SYSRETVAL_CHECK(tn_mutex_lock_polling(__VA_ARGS__))
#define     appl_mutex__unlock(...)             SYSRETVAL_CHECK(tn_mutex_unlock(__VA_ARGS__))

#endif



#endif // _APPL_MUTEX_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


