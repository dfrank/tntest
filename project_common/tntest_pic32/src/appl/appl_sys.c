/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include <sys/attribs.h>
#include "bsp_tnkernel.h"
#include "task_conf.h"
#include "bsp/bsp.h"

#include "dterm.h"

#include "appl_sys.h"
#include "appl_cfg/appl_cfg.h"
#include "appl_cfg/appl_cfg__dbg__deb_halt.h"

#include "appl_tntest/appl_tntest_timer.h"

#include "peripheral/peripheral.h"

#ifndef APPL_CFG__DEBUG
#  error APPL_CFG__DEBUG is not defined
#endif

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/



#define IDLE_TASK_STACK_SIZE           (TN_MIN_STACK_SIZE + 32 + TASK_STACK_INTERRUPT_ADD)
#define INT_STACK_SIZE                 (TN_MIN_STACK_SIZE + 200)

//-- system timer cfg {{{
#define TN_SYS_TMR                     TMR_ID_5
#define TN_SYS_TMR_INT_REQ             INT_SOURCE_TIMER_5
#define TN_SYS_TMR_INT_VECTOR          INT_VECTOR_T5

#define TN_SYS_TMR_INT_HNDL__SYS()     tn_sys_interrupt(_TIMER_5_VECTOR)

#define TN_SYS_TMR_PRESCALER           TMR_PRESCALE_VALUE_8
#define TN_SYS_TMR_PRESCALER_VALUE     8

#define TN_SYS_TMR_PR                  (BSP_FPB / TN_SYS_TMR_PRESCALER_VALUE / 1000/*Hz*/)

#define SYS_TIME_MS(a)                 (TN_TickCnt)((a) / TN_SYS_TMR_PR_MS)
// }}}


/***************************************************************************************************
 *                                  PRIVATE FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

TN_TASK_STACK_DEF(idle_task_stack,  IDLE_TASK_STACK_SIZE);
TN_TASK_STACK_DEF(interrupt_stack,  INT_STACK_SIZE);

volatile TN_TickCnt appl_sys_tick_count = 0xffffff01;
volatile TN_TickCnt next_tick_count = 0;


/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

extern TN_UWORD task_conf_stack[];
extern TN_UWORD task_input_stack[];


/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/



/***************************************************************************************************
 *    INTERRUPT HANDLERS
 **************************************************************************************************/

TN_SYS_TMR_INT_HNDL__SYS()
{
   if (
         PLIB_INT_SourceIsEnabled(INT_ID_0, TN_SYS_TMR_INT_REQ)
         &&
         PLIB_INT_SourceFlagGet(INT_ID_0, TN_SYS_TMR_INT_REQ)
      )
   {
      PLIB_INT_SourceFlagClear(INT_ID_0, TN_SYS_TMR_INT_REQ);


      //-- COMMENTED because tneo now has built-in stack overflow
      //   software control
#if 0
#if APPL_CFG__DEBUG
      //-- check for stack corruption
      {
         const char *p_corrupted_task_name = NULL;
         if (task_conf_stack[0] != TN_FILL_STACK_VAL){
            p_corrupted_task_name = "task_conf";
         } else if (task_input_stack[0] != TN_FILL_STACK_VAL){
            p_corrupted_task_name = "task_input";
         }

         if (idle_task_stack[0] != TN_FILL_STACK_VAL){
            p_corrupted_task_name = "idle";
         } else if (interrupt_stack[0] != TN_FILL_STACK_VAL){
            p_corrupted_task_name = "interrupt";
         }

         if (p_corrupted_task_name != NULL){
            APPL_DEB_HALT(
                  "!!!!!!!!!! Stack overflow, task: %s", 
                  p_corrupted_task_name
                  );
         }
      }
#endif
#endif


#if TN_DYNAMIC_TICK

      appl_sys_tick_count++;

#if 1
      if (next_tick_count > 0 && next_tick_count != TN_WAIT_INFINITE){
         next_tick_count--;
         
         if (next_tick_count == 0){
            tn_tick_int_processing();
         }
      }
#else
      tn_tick_int_processing();
#endif

#else
      tn_tick_int_processing();
#endif



      appl_tntest_timer__sys_int_check();
   }
}


/***************************************************************************************************
 *                                        PRIVATE FUNCTIONS
 **************************************************************************************************/

#if TN_DYNAMIC_TICK

void _tick_schedule(TN_TickCnt timeout)
{
   next_tick_count = timeout;
}

TN_TickCnt _tick_cnt_get(void)
{
   return appl_sys_tick_count;
}

#endif

static void _user_task_create (void)
{
   //-- create task_conf that will configure the system and create all other tasks
   task_conf__create();
}

/**
 *  Idle task hook function
 */
static void _idle_task_callback (void)
{
}

static void _tn_deadlock_callback(
      TN_BOOL active, struct TN_Mutex *mutex, struct TN_Task *task
      )
{
   //-- NOTE: called from interrupt with other interrupts disabled

   if (active){
      DTMSG(DT_WRN | DT_NOWAIT | DT_SIMPLE, "deadlock");
   } else {
      DTMSG(DT_WRN | DT_NOWAIT | DT_SIMPLE, "deadlock off");
   }
}

static void _tn_stack_overflow_callback(struct TN_Task *task)
{
   //-- NOTE: called from interrupt with other interrupts disabled

   APPL_DEB_HALT(
         "!!!!!!!!!! Stack overflow, task='%s'", 
         task->name
         );
}

/**
 * System timer initialization
 */
static void _sys_timer_init(void)
{
   //-- disable ADC function on all pins
   AD1PCFG = 0xffffffff;

   //-- disable CN
   PLIB_PORTS_ChangeNoticeDisable(PORTS_ID_0);

   //-- turn off JTAG
   PLIB_DEVCON_JTAGPortDisable(DEVCON_ID_0);

   //-- multi-vectored interrupts enabled
   PLIB_INT_MultiVectorSelect(INT_ID_0);

   //-- setup system timer
   PLIB_TMR_StopInIdleDisable(TN_SYS_TMR);
   PLIB_TMR_PrescaleSelect(TN_SYS_TMR, TN_SYS_TMR_PRESCALER);
   PLIB_TMR_Period16BitSet(TN_SYS_TMR, TN_SYS_TMR_PR - 1);

   //-- setup system timer interrupt
   PLIB_INT_VectorPrioritySet(INT_ID_0, TN_SYS_TMR_INT_VECTOR, INT_PRIORITY_LEVEL1);
   PLIB_INT_SourceFlagClear(INT_ID_0, TN_SYS_TMR_INT_REQ);
   PLIB_INT_SourceEnable(INT_ID_0, TN_SYS_TMR_INT_REQ);

   //-- start system timer
   PLIB_TMR_Start(TN_SYS_TMR);
}

static void _system_config(void)
{
   //-- NOTE: in legacy plib, the following call did the trick:
   //
   //    SYSTEMConfig(BSP_FOSC, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);
   //   
   //   it confugures prefetch cache and PFM wait states (PFM is PreFetch Module),
   //   more about it read in DS61119B (Section 4. Prefetch Cache Module)
   //
   //   But in the Harmony, we have only SYS_DEVCON_PerformanceConfig(),
   //   which also affects FPB (peripheral bus frequency). So, I've taken the code
   //   from SYS_DEVCON_PerformanceConfig(), and modified it so that FPB isn't changed.
   //

   bool int_flag = false;

   if (PLIB_INT_IsEnabled (INT_ID_0)){
      int_flag = true;
      PLIB_INT_Disable(INT_ID_0);
   }

   if (PLIB_PCACHE_ExistsWaitState(PCACHE_ID_0)){
      int ws;
      if (BSP_FOSC <= 30000000)
         ws = 0;
      else if (BSP_FOSC <= 60000000)
         ws = 1;
      else
         ws = 2;

      PLIB_PCACHE_WaitStateSet(PCACHE_ID_0, ws);
   }

   if (PLIB_PCACHE_ExistsPrefetchEnable(PCACHE_ID_0)){
      PLIB_PCACHE_PrefetchEnableSet(PCACHE_ID_0, PLIB_PCACHE_PREFETCH_ENABLE_ALL);
   }

   if (PLIB_BMX_ExistsDataRamWaitState(BMX_ID_0)){
      PLIB_BMX_DataRamWaitStateSet(BMX_ID_0, PLIB_BMX_DATA_RAM_WAIT_ZERO);
   }            

   if (int_flag){
      PLIB_INT_Enable(INT_ID_0);
      int_flag = false;
   }

}


/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

int main (void)
{
   //-- disable interrupts globally
   tn_arch_int_dis();

   // Setup configuration
   _system_config();

   //turn off ADC function for all pins
   //TODO: plib
   AD1PCFG = 0xffffffff;

   //turn all pins into input mode
   TRISB = 0xffffffff;
   TRISC = 0xffffffff;
   TRISD = 0xffffffff;
   TRISE = 0xffffffff;
   TRISF = 0xffffffff;
   TRISG = 0xffffffff;

   appl_tntest_timer__init();

   _sys_timer_init();

   tn_callback_deadlock_set(_tn_deadlock_callback);
   tn_callback_stack_overflow_set(_tn_stack_overflow_callback);

#if TN_DYNAMIC_TICK
   tn_callback_dyn_tick_set(_tick_schedule, _tick_cnt_get);
#endif

   tn_sys_start(
         idle_task_stack,
         IDLE_TASK_STACK_SIZE,
         interrupt_stack,
         INT_STACK_SIZE,
         _user_task_create,
         _idle_task_callback
         );

   return 0;
}



/***************************************************************************************************
 *  end of file
 **************************************************************************************************/


