
/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "bsp/bsp.h"
#include "dterm.h"
#include "appl.h"
#include "appl_sys.h"

#include "bsp_tnkernel.h"
#include "tmalloc.h"

#include "dterm.h"

#include "malloc_reg.h"
#include "tworker_man.h"

#include "appl_cfg__dbg__deb_halt.h"

#include "task_input.h"
#include "appl_tntest.h"
#include "perfmeas.h"

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

#define TASK_CONF_STACK_SIZE           (470 + TASK_STACK_INTERRUPT_ADD)
#define TASK_CONF_PRIORITY             (1)   //-- highest user priority


/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/

struct TN_Task task_conf;

//-- NOTE: we need to explicitly initialize it to TN_FILL_STACK_VAL in order to make
//   stack corruption check work (in appl_sys.c)
TN_TASK_STACK_DEF(task_conf_stack,  TASK_CONF_STACK_SIZE) = {
   [0 ... (TASK_CONF_STACK_SIZE - 1)] = TN_FILL_STACK_VAL
};




/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                  PRIVATE FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                        PRIVATE FUNCTIONS
 **************************************************************************************************/

/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

void TN_TASK task_conf_body (void *param)
{
   SYSRETVAL_CHECK_TOUT(tn_task_sleep(10));

   dterm__init(BSP_FPB, 115200, appl__dterm_rx_handler);
   
   //NOTE: this sleep is needed to make UART work normally. Without this sleep,
   //      UART speed is weird for some time.
   SYSRETVAL_CHECK_TOUT(tn_task_sleep(100));

   //-- check reset condition {{{
   {
      DTMSG((0), "sdf");
      if (RCONbits.CMR){
         DTMSG_W("CMR");
      }
      if (RCONbits.BOR){
         DTMSG_W("BOR");
      }
      if (RCONbits.EXTR){
         DTMSG_W("EXTR");
      }
      if (RCONbits.IDLE){
         DTMSG_W("IDLE");
      }
      if (RCONbits.POR){
         DTMSG_W("POR");
      }
      if (RCONbits.SLEEP){
         DTMSG_W("SLEEP");
      }
      if (RCONbits.SWR){
         DTMSG_W("SWR");
      }
      if (RCONbits.VREGS){
         DTMSG_W("VREGS");
      }
      if (RCONbits.WDTO){
         DTMSG_W("WDTO");
      }
      RCON = 0;
   }
   // }}}

   //-- init various program modules

   tmalloc__init();
   tworker_man__init();
   perfmeas__init();

   //-- create other tasks
   task_input__create();

   //-- final little sleep before going to work
   SYSRETVAL_CHECK_TOUT(tn_task_sleep(10));

#if 0
   //-- test perfmeas {{{
   {
      U32 prev_value = 0, cur_value = 0;
      for (;;){
         tn_task_sleep(1000);
         cur_value = perfmeas__cur_value__get();
         DTMSG_I("cur=%lu", perfmeas__diff__get_cycles(prev_value, cur_value));
         prev_value = cur_value;
      }
   }
   // }}}
#endif

   DTMSG_I("system started!");

#if APPL_MALLOC_REG_ENABLED
   DTMSG_I("allocated size=%d", malloc_reg__allocated_size__get());
#endif

   //-- never returns
   appl_tntest__task_body();
}

void task_conf__create(void)
{
   SYSRETVAL_CHECK(tn_task_create(
            &task_conf,
            task_conf_body,
            TASK_CONF_PRIORITY,
            task_conf_stack,
            TASK_CONF_STACK_SIZE,
            0,
            TN_TASK_START_ON_CREATION
            ));

   //-- set task name for debug
#if TN_DEBUG
   task_conf.name = "task_conf";
#endif
}



/***************************************************************************************************
 *  end of file: task_conf.c
 **************************************************************************************************/


