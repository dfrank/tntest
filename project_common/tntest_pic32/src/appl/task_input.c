
/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "dfc.h"
#include "appl.h"
#include "task_input.h"
#include "dterm.h"

#include "utils/random32.h"

#include "appl_cfg/appl_cfg__tnkernel.h"
#include "appl_cfg/appl_cfg__dbg__deb_halt.h"

#ifndef APPL_CFG__TNKERNEL
#  error APPL_CFG__TNKERNEL is not defined
#endif

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

#define TASK_INPUT_STACK_SIZE             (200 + TASK_STACK_INTERRUPT_ADD)
#define TASK_INPUT_PRIORITY               (1)

#define TASK_INPUT_MSG_BUF_SIZE           (8)



/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

struct TaskInputMsg {
   enum E_TaskInputCmd  cmd;
   uintptr_t            par1;
   uintptr_t            par2;
};



/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

struct TN_Task   tcb_task_input;

//-- NOTE: we need to explicitly initialize it to TN_FILL_STACK_VAL in order to make
//   stack corruption check work (in appl_sys.c)
TN_TASK_STACK_DEF(task_input_stack, TASK_INPUT_STACK_SIZE) = {
   [0 ... (TASK_INPUT_STACK_SIZE - 1)] = TN_FILL_STACK_VAL
};


TN_DQUE  que_input_msg;
void     *que_input_msg_buf[TASK_INPUT_MSG_BUF_SIZE];
struct TN_FMem   fmp_input_msg;
TN_FMEM_BUF_DEF(fmp_input_msg_buf, struct TaskInputMsg, TASK_INPUT_MSG_BUF_SIZE);



/***************************************************************************************************
 *                                         PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                        EXTERNAL DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                  PRIVATE FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                       PRIVATE FUNCTIONS
 **************************************************************************************************/



/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

bool task_input__msg__send_i(enum E_TaskInputCmd cmd, intptr_t par1, intptr_t par2)
{
   bool ret = false;
   struct TaskInputMsg *p_msg;
   TN_RETVAL tn_res;

   tn_res = SYSRETVAL_CHECK(tn_fmem_get_ipolling(&fmp_input_msg, (void*)&p_msg));
   if (tn_res == TN_RC_OK){
      p_msg->cmd     = cmd;
      p_msg->par1    = par1;
      p_msg->par2    = par2;

      tn_res = SYSRETVAL_CHECK(tn_queue_isend_polling(&que_input_msg, (void*)p_msg));
      if (tn_res == TN_RC_OK){
         ret = true;
      } else {
         SYSRETVAL_CHECK(tn_fmem_irelease(&fmp_input_msg, (void*)p_msg)); //-- free buf
         DTMSG(DT_WRN | DT_NOWAIT, "tn_queue_isend_polling error: %d", tn_res);
      }
   } else {
      DTMSG(DT_WRN | DT_NOWAIT, "tn_fmem_get_ipolling error: %d", tn_res);
   }
   return ret;
}


bool task_input__invoke_later_0par_i (T_TaskInput_FuncToCall_0par p_func)
{
   return task_input__msg__send_i(
         TASK_INPUT_CMD__INVOKE_LATER_1PAR, 
         (uintptr_t)p_func, 
         0  //-- not used
         );
}


bool task_input__invoke_later_1par_i (T_TaskInput_FuncToCall_1par p_func, uintptr_t par1)
{
   return task_input__msg__send_i(
         TASK_INPUT_CMD__INVOKE_LATER_1PAR, 
         (uintptr_t)p_func, 
         par1
         );
}



/**
 *  
 *  
 * @param param 
 * 
 * @return void TN_TASK 
 */
void TN_TASK task_input (void *param)
{
   SYSRETVAL_CHECK(tn_fmem_create (&fmp_input_msg, (void *)fmp_input_msg_buf, TN_MAKE_ALIG_SIZE(sizeof(struct TaskInputMsg)), TASK_INPUT_MSG_BUF_SIZE));
   SYSRETVAL_CHECK(tn_queue_create(&que_input_msg, (void *)que_input_msg_buf,                    TASK_INPUT_MSG_BUF_SIZE));

   struct TaskInputMsg *p_msg;

   for (;;){
      {
         TN_RETVAL tn_res = SYSRETVAL_CHECK_TOUT(tn_queue_receive(&que_input_msg, (void*)&p_msg, 10));
         if (tn_res == TN_RC_TIMEOUT){

            //-- maybe do something periodically

            continue;
         } else if (tn_res != TN_RC_OK){
            APPL_DEB_HALT("tn_queue_receive error=%d", tn_res);
            continue;
         }

         switch (p_msg->cmd){
            case TASK_INPUT_CMD__INVOKE_LATER_0PAR:
               {
                  T_TaskInput_FuncToCall_0par p_func = (T_TaskInput_FuncToCall_0par)p_msg->par1;
                  p_func();
               }
               break;
            case TASK_INPUT_CMD__INVOKE_LATER_1PAR:
               {
                  T_TaskInput_FuncToCall_1par p_func = (T_TaskInput_FuncToCall_1par)p_msg->par1;
                  p_func(p_msg->par2);
               }
               break;
         }

         SYSRETVAL_CHECK(tn_fmem_release(&fmp_input_msg, (void*)p_msg));
      }

   }

}

void task_input__create(void)
{
   SYSRETVAL_CHECK(tn_task_create(&tcb_task_input,
         task_input,
         TASK_INPUT_PRIORITY,
         task_input_stack,
         TASK_INPUT_STACK_SIZE,
         NULL,
         TN_TASK_START_ON_CREATION
         ));

   //-- set task name for debug
#if TN_DEBUG
   tcb_task_input.name = "task_input";
#endif

}

/***************************************************************************************************
 *  end of file: task_input.c
 **************************************************************************************************/


