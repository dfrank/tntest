/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "tworker_man_arch.h"
#include "bsp_mcu_config.h"
#include "bsp_tnkernel.h"
#include "tworker_man.h"

#include "peripheral/peripheral.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

#define TWORKMAN_TMR                   TMR_ID_1
#define TWORKMAN_TMR_INT_REQ           INT_SOURCE_TIMER_1
#define TWORKMAN_TMR_INT_VECTOR        INT_VECTOR_T1
#define TWORKMAN_TMR_INT_HNDL__SYS()   tn_sys_interrupt(_TIMER_1_VECTOR)

#define TWORKMAN_TMR_PRESCALER         TMR_PRESCALE_VALUE_8
#define TWORKMAN_PRESCALER_HUMAN_VALUE 8

#define TWORKMAN_TMR_PR                (BSP_FPB / TWORKMAN_PRESCALER_HUMAN_VALUE / 12345/*Hz*/)
#if (TWORKMAN_TMR_PR > 0xffff)
#  error TWORKMAN_TMR_PR is too large
#endif

#if (TWORKMAN_TMR_PR < 2)
#  error TWORKMAN_TMR_PR is too small
#endif






/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void tworker_man_arch__init(void)
{
   //-- setup timer
   PLIB_TMR_StopInIdleDisable(TWORKMAN_TMR);
   PLIB_TMR_PrescaleSelect(TWORKMAN_TMR, TWORKMAN_TMR_PRESCALER);
   PLIB_TMR_Period16BitSet(TWORKMAN_TMR, TWORKMAN_TMR_PR - 1);

   //-- setup timer interrupt
   PLIB_INT_VectorPrioritySet(INT_ID_0, TWORKMAN_TMR_INT_VECTOR, INT_PRIORITY_LEVEL1);
   PLIB_INT_SourceFlagClear(INT_ID_0, TWORKMAN_TMR_INT_REQ);
   PLIB_INT_SourceEnable(INT_ID_0, TWORKMAN_TMR_INT_REQ);

   //-- start timer
   PLIB_TMR_Start(TWORKMAN_TMR);
}


TWORKMAN_TMR_INT_HNDL__SYS()
   //void __ISR(_TIMER_1_VECTOR) tworker_isr(void)
{

   if (
         PLIB_INT_SourceIsEnabled(INT_ID_0, TWORKMAN_TMR_INT_REQ)
         &&
         PLIB_INT_SourceFlagGet(INT_ID_0, TWORKMAN_TMR_INT_REQ)
      )
   {
      PLIB_INT_SourceFlagClear(INT_ID_0, TWORKMAN_TMR_INT_REQ);

      //-- call actual handler
      tworker_man__int_handler();

   }
}


/*******************************************************************************
 *    end of file
 ******************************************************************************/



