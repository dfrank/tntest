/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

#ifndef _MCU_CONFIG_H
#define _MCU_CONFIG_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include "bsp_mcu_config.h"

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

// Oscillator installed on the board: 16MHz.

//-- Possible values:
//
//    FPLLIDIV: 1 .. 6, 10, 12
//    FPLLMUL:  15 .. 21, 24
//    FPLLODIV: 1, 2, 4, 8, 16, 32, 64, 256
//
//    FPBDIV:   1, 2, 4, 8

#if   (BSP_FOSC == 80000000)
#  pragma config FPLLIDIV = DIV_4,  FPLLMUL = MUL_20, FPLLODIV = DIV_1
#elif (BSP_FOSC == 72000000)
#  pragma config FPLLIDIV = DIV_4,  FPLLMUL = MUL_18, FPLLODIV = DIV_1
#elif (BSP_FOSC == 60000000)
#  pragma config FPLLIDIV = DIV_4,  FPLLMUL = MUL_15, FPLLODIV = DIV_1
#elif (BSP_FOSC == 32000000)
#  pragma config FPLLIDIV = DIV_4,  FPLLMUL = MUL_16, FPLLODIV = DIV_2
#elif (BSP_FOSC == 30000000)
#  pragma config FPLLIDIV = DIV_4,  FPLLMUL = MUL_15, FPLLODIV = DIV_2
#elif (BSP_FOSC == 16000000)
#  pragma config FPLLIDIV = DIV_4,  FPLLMUL = MUL_16, FPLLODIV = DIV_4
#elif (BSP_FOSC == 8000000)
#  pragma config FPLLIDIV = DIV_4,  FPLLMUL = MUL_16, FPLLODIV = DIV_8
#elif (BSP_FOSC == 1000000)
#  pragma config FPLLIDIV = DIV_4,  FPLLMUL = MUL_16, FPLLODIV = DIV_64
#else
#  error wrong BSP_FOSC
#endif

#if (BSP_FPB == BSP_FOSC)
#  pragma config FPBDIV = DIV_1
#elif (BSP_FPB == (BSP_FOSC / 2))
#  pragma config FPBDIV = DIV_2
#elif (BSP_FPB == (BSP_FOSC / 4))
#  pragma config FPBDIV = DIV_4
#elif (BSP_FPB == (BSP_FOSC / 8))
#  pragma config FPBDIV = DIV_8
#else
#  error wrong BSP_FPB
#endif


#pragma config POSCMOD = HS
#pragma config FNOSC = PRIPLL

#pragma config FWDTEN = OFF
#pragma config WDTPS = PS8192 //-- a bit more than 8 seconds

/**
 * USB PLL configuration.
 * We need to provide 4 MHz to the USB PLL, so, since our oscillator is 16MHz,
 * we need to divide it by 4.
 */
#pragma config UPLLEN   = OFF        // USB PLL Disabled
//#pragma config UPLLIDIV = DIV_4     // USB PLL Input Divider = Divide by 4





#pragma config DEBUG    = OFF           // Background Debugger disabled
#pragma config BWP      = OFF           // Boot write protect: OFF

// For PIC32MX3xx, PIC32MX4xx, PIC32MX5xx, PIC32MX6xx and PIC32MX7xx 
// devices the ICE connection is on PGx2. .
#pragma config ICESEL = ICS_PGx2    // ICE pins configured on PGx2, Boot write protect OFF.

#pragma config FSOSCEN      = OFF //-- will be turned off later

//-- code protection
#ifdef __DEBUG
#  pragma config CP = OFF       
#else
#  pragma config CP = ON
#endif



/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

#endif // _MCU_CONFIG_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


