/***************************************************************************************************
 *   Project:       P-0173 - ��-90 - ��������� ��� ��-100
 *   Author:        Alex Borisov
 ***************************************************************************************************
 *   Distribution:  Copyright � 2009-2010 �����
 *
 ***************************************************************************************************
 *   MCU Family:    PIC24FJ256GB106
 *   Compiler:      Microchip C30 3.22
 ***************************************************************************************************
 *   File:          bsp.h
 *   Description:   ������ ��������� ���������
 *
 ***************************************************************************************************
 *   History:       02.02.2010 - [Alex Borisov] - file created
 *
 **************************************************************************************************/

#ifndef _BSP_COMMON__MCU_CONFIG_H
#define _BSP_COMMON__MCU_CONFIG_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

/***************************************************************************************************
 * DEFINITIONS
 **************************************************************************************************/

#if 1
#define BSP_FOSC        80000000U     /* Hz */  //-- if changing this, look for "BSP_FOSC_CHANGED"
#define BSP_FPB         40000000U     /* Hz */  //-- if changing this, look for "BSP_FPB_CHANGED"
#endif

#if 0
#define BSP_FOSC        16000000U     /* Hz */  //-- if changing this, look for "BSP_FOSC_CHANGED"
#define BSP_FPB         8000000U      /* Hz */  //-- if changing this, look for "BSP_FPB_CHANGED"
#endif

#if 0
#define BSP_FOSC        1000000U     /* Hz */  //-- if changing this, look for "BSP_FOSC_CHANGED"
#define BSP_FPB         500000U      /* Hz */  //-- if changing this, look for "BSP_FPB_CHANGED"
#endif

#if 0
#define BSP_FOSC        80000000      /* Hz */  //-- if changing this, look for "BSP_FOSC_CHANGED"
#define BSP_FPB         80000000      /* Hz */  //-- if changing this, look for "BSP_FPB_CHANGED"
#endif

#if 0
#define BSP_FOSC        32000000      /* Hz */  //-- if changing this, look for "BSP_FOSC_CHANGED"
#define BSP_FPB         32000000      /* Hz */  //-- if changing this, look for "BSP_FPB_CHANGED"
#endif

#define  BSP_CORE_TIMER_PRESCALER  2   //-- it is fixed, we can't change it
#define  BSP_CORE_TIMER_MS    (BSP_FOSC / 1000 / BSP_CORE_TIMER_PRESCALER)





#define  _BSP__NS_PER_SECOND    1000000000ULL

//-- NOTE: the following 2 definitions should correspond!
#define  BSP__T_NS_SCALE_FACTOR     100      //-- time in nanoseconds scale factor
                                             //   i.e. value 2777 means 27.77 ns
#define  BSP__T_NS_SCALE_DPP        2        //-- 

#define /*T_BSP_T_ns*/BSP_TPB   ((_BSP__NS_PER_SECOND * BSP__T_NS_SCALE_FACTOR + BSP_FPB/2) / BSP_FPB)
#define /*T_BSP_T_ns*/BSP_TOSC  ((_BSP__NS_PER_SECOND * BSP__T_NS_SCALE_FACTOR + BSP_FPB/2) / BSP_FOSC)

typedef unsigned int/*U32*/   T_BSP_T_ns;  //-- time in nanoseconds multiplied by BSP__T_NS_SCALE_FACTOR

#endif /* _BSP_COMMON__MCU_CONFIG_H */
/***************************************************************************************************
    end of file: bsp.h
 **************************************************************************************************/

