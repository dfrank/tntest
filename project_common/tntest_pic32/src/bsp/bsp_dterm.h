/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

#ifndef _BSP_DTERM_H
#define _BSP_DTERM_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include "bsp_tnkernel.h"
#include "dfc.h"

#include "utils/xprintf.h"
#include "peripheral/peripheral.h"


/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/


#define  DTERM__BUFFERED_TX   1
#define  DTERM__TX_BUF_SIZE   4096   //-- Size of buffer for Tx

#define  DTERM__RX_ENABLED    1
#define  DTERM__RX_BUF_SIZE   32

/*
 * Allow recursion
 * (see detailed explanation in dterm.h)
 */
#define  DTERM__ALLOW_RECURSION  1



#define  DTERM_USER_INIT            bsp_dterm__user_init

/*
 * NOTE: these lock/unlock functions can be called from
 * task and interrupt, so, they should lock/unlock mutex
 * if only there is task context; otherwise,
 * only interrupts in which dterm is used should be
 * disabled/enabled.
 */
#define  DTERM_LOCK_PRIMARY         bsp_dterm__lock_primary
#define  DTERM_UNLOCK_PRIMARY       bsp_dterm__unlock_primary


/*
 * Secondary lock/unlock: they are called not for long time, but for critical
 * sections.
 *
 * Typically, it makes sense to just disable all interrupts here.
 * Or, you can disable/enable interrupts that aren't affected by DTERM_LOCK_PRIMARY,
 * but then some day you can forget to add some new interrupt here, which will
 * lead to bug.
 *
 *
 * These definitions are mandatory if you set DTERM__ALLOW_RECURSION to 1.
 *
 * But even if your DTERM__ALLOW_RECURSION is 0, these definitions are useful
 * to catch bug if your DTERM_LOCK_PRIMARY / DTERM_UNLOCK_PRIMARY are buggy
 * (typically, if you add dterm echo to some interrupt but forgot to 
 * disable this interrupt in DTERM_LOCK_PRIMARY)
 */
#define  DTERM_LOCK_SECONDARY       bsp_dterm__lock_secondary
#define  DTERM_UNLOCK_SECONDARY     bsp_dterm__unlock_secondary


/*
 * NOTE: this DTERM_TASK_SLEEP function can be called from task and interrupt,
 *       so, in the case of interrupt, it typically should just do nothing
 */
#define  DTERM_TASK_SLEEP           bsp_dterm_task_sleep






#define DTERM_UART                   USART_ID_1

#define DTERM_TX_INT_REQ             INT_SOURCE_USART_1_TRANSMIT
#define DTERM_RX_INT_REQ             INT_SOURCE_USART_1_RECEIVE
#define DTERM_ERR_INT_REQ            INT_SOURCE_USART_1_ERROR

#define DTERM_UART_INT_VECTOR_FOR_ISR  _UART_1_VECTOR
#define DTERM_UART_INT_VECTOR        INT_VECTOR_UART1
#define DTERM_UART_INT_PRI           INT_PRIORITY_LEVEL7 //TN_INTERRUPT_LEVEL
#define DTERM_UART_INT_SUBPRI        INT_SUBPRIORITY_LEVEL0 //TN_INTERRUPT_LEVEL

#define DTERM_INT_HANDLER__SYS()     tn_p32_srs_isr(_UART_1_VECTOR)

#define DTERM_TX_PORT                PORT_CHANNEL_D
#define DTERM_TX_PIN                 PORTS_BIT_POS_3


#define DTERM_RX_PORT                PORT_CHANNEL_D
#define DTERM_RX_PIN                 PORTS_BIT_POS_2


#if defined(__DEBUG)
#  define BSP_DTERM__DEB_HALT()      DFC_DEB_HALT()
#else
#  define BSP_DTERM__DEB_HALT()
#endif

/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/



void bsp_dterm__user_init(void);
void bsp_dterm__lock_primary(void);
void bsp_dterm__unlock_primary(void);
void bsp_dterm__lock_secondary(void);
void bsp_dterm__unlock_secondary(void);
void bsp_dterm_task_sleep(void);


static inline size_t bsp_dterm__formatted_write(
      const char * format,
      void put_one_char(char, void *),
      void *secret_pointer,
      va_list ap
      )
{
   return _formatted_write(format, put_one_char, secret_pointer, ap);
}





//---- controller-dependent stuff --------------------------------------


void bsp_dterm__uart_on(void);
void bsp_dterm__uart_off(void);
int bsp_dterm__init(unsigned long periph_bus_clk_freq, unsigned long baudrate);
void bsp_dterm__interrupt_en(void);
void bsp_dterm__interrupt_dis(void);


static inline int bsp_dterm__is_transmitting__direct(void)
{
   return (
         (PLIB_USART_TransmitterIsEmpty(DTERM_UART))
         ||
         !(PLIB_USART_IsEnabled(DTERM_UART))
         ) ? false : true;
}

static inline void bsp_dterm__tx_int_disable(void)
{
   PLIB_INT_SourceDisable(INT_ID_0, DTERM_TX_INT_REQ);
}

static inline void bsp_dterm__tx_int_enable(void)
{
   PLIB_INT_SourceEnable(INT_ID_0, DTERM_TX_INT_REQ);
}

static inline int bsp_dterm__is_tx_buf_full(void)
{
   return PLIB_USART_TransmitterBufferIsFull(DTERM_UART);
}

static inline int bsp_dterm__is_rx_buf_not_empty(void)
{
   return PLIB_USART_ReceiverDataIsAvailable(DTERM_UART);
}

static inline void bsp_dterm__uart_char_send(char c)
{
   PLIB_USART_TransmitterByteSend (DTERM_UART, c);
}

static inline char bsp_dterm__uart_char_receive(void)
{
   return PLIB_USART_ReceiverByteReceive(DTERM_UART);
}


#endif // _BSP_DTERM_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


