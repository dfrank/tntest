/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "bsp_dterm.h"

#include "appl_mutex.h"
#include "peripheral/peripheral.h"

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

/***************************************************************************************************
 *                                  PRIVATE FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

static T_ApplMutex _mutex;

static int _status_register = 0;

//-- for self-check: secondary lock should never be locked recursively,
//   since secondary lock just disables interrupts
static bool _secondary_locked = false;



/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/

extern void _dterm__isr_tx_handler(void);
extern void _dterm__isr_rx_handler(void);


/***************************************************************************************************
 *                                        PRIVATE FUNCTIONS
 **************************************************************************************************/

/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/


void bsp_dterm__user_init(void)
{
   appl_mutex__create(&_mutex, BSP_TNKERNEL__MUTEX_PROTOCOL, BSP_TNKERNEL__CEIL_PRIORITY);
}

void bsp_dterm__lock_primary(void)
{
   //-- if in the task context, lock mutex
   if (tn_sys_context_get() == TN_CONTEXT_TASK){
      appl_mutex__lock(&_mutex, TN_WAIT_INFINITE);
   }

   //-- here we should disable all interrupts in which dterm might be used
   //   (well, you're able to not disable some interrupt that uses dterm, 
   //   but then you should set DTERM__ALLOW_RECURSION to 1, which is 
   //   HIGHLY EXPERIMENTAL. See comments for DTERM__ALLOW_RECURSION)

}

void bsp_dterm__unlock_primary(void)
{
   //-- here we should re-enable all interrupts in which dterm might be used


   //-- if in the task context, unlock mutex
   if (tn_sys_context_get() == TN_CONTEXT_TASK){
      appl_mutex__unlock(&_mutex);
   }
}

void bsp_dterm__lock_secondary(void)
{
   _status_register = tn_arch_sr_save_int_dis();
   if (_secondary_locked){
      BSP_DTERM__DEB_HALT();
   }
   _secondary_locked = true;
   //asm volatile("di %0" : "=r" (_status_register));
}

void bsp_dterm__unlock_secondary(void)
{
   if (!_secondary_locked){
      BSP_DTERM__DEB_HALT();
   }
   _secondary_locked = false;
   tn_arch_sr_restore(_status_register);
   //asm volatile("mtc0 %0, $12" : : "r" (_status_register));
}


void bsp_dterm_task_sleep(void)
{
   //-- if in the task context, sleep
   if (tn_sys_context_get() == TN_CONTEXT_TASK){
      tn_task_sleep(1);
   }
}


//---- controller-dependent stuff --------------------------------------


void bsp_dterm__uart_on(void)
{
   PLIB_USART_Enable(DTERM_UART);
}

void bsp_dterm__uart_off(void)
{
   PLIB_USART_Disable(DTERM_UART);
}

int bsp_dterm__init(unsigned long periph_bus_clk_freq, unsigned long baudrate)
{
   /* Config UART hardware */

   PLIB_USART_InitializeModeGeneral(
         DTERM_UART, 
         false,   //-- autobaud
         false,   //-- loopback
         false,   //-- wake from sleep
         false,   //-- irda mode
         false    //-- stop when CPU is in idle mode
         );

   PLIB_USART_InitializeOperation(
         DTERM_UART, 
         USART_RECEIVE_FIFO_ONE_CHAR,
         USART_TRANSMIT_FIFO_NOT_FULL, 
         USART_ENABLE_TX_RX_USED
         );

   PLIB_USART_TransmitterEnable(DTERM_UART);
   PLIB_USART_ReceiverEnable(DTERM_UART);

   PLIB_USART_BaudRateHighEnable(DTERM_UART);

   PLIB_USART_BaudRateHighSet(
         DTERM_UART, 
         periph_bus_clk_freq, 
         baudrate
         );

   //-- UART's 0 is logical 1 (high), for both Rx and Tx.
   PLIB_USART_ReceiverIdleStateLowDisable(DTERM_UART);
   PLIB_USART_TransmitterIdleIsLowDisable(DTERM_UART);

   //-- Config UART interrupts
   PLIB_INT_VectorPrioritySet   (INT_ID_0, DTERM_UART_INT_VECTOR, DTERM_UART_INT_PRI);
   PLIB_INT_VectorSubPrioritySet(INT_ID_0, DTERM_UART_INT_VECTOR, DTERM_UART_INT_SUBPRI);

   //-- Config GPIO
   PLIB_PORTS_PinDirectionOutputSet(
         PORTS_ID_0, 
         DTERM_TX_PORT, 
         DTERM_TX_PIN
         );

   PLIB_PORTS_PinDirectionInputSet(
         PORTS_ID_0, 
         DTERM_RX_PORT, 
         DTERM_RX_PIN
         );


   //-- Enable UART
   bsp_dterm__uart_on();

   return true;
}


void bsp_dterm__interrupt_en(void)
{
   PLIB_INT_SourceFlagClear(INT_ID_0, DTERM_TX_INT_REQ);

#if DTERM__BUFFERED_TX
   PLIB_INT_SourceEnable(INT_ID_0, DTERM_TX_INT_REQ);
#else
   PLIB_INT_SourceDisable(INT_ID_0, DTERM_TX_INT_REQ);
#endif

   PLIB_INT_SourceFlagClear(INT_ID_0, DTERM_RX_INT_REQ);

#if DTERM__RX_ENABLED
   PLIB_INT_SourceEnable(INT_ID_0, DTERM_RX_INT_REQ);
#else
   PLIB_INT_SourceDisable(INT_ID_0, DTERM_RX_INT_REQ);
#endif

   PLIB_INT_SourceFlagClear(INT_ID_0, DTERM_ERR_INT_REQ);
   PLIB_INT_SourceDisable  (INT_ID_0, DTERM_ERR_INT_REQ);
}

void bsp_dterm__interrupt_dis(void)
{
   PLIB_INT_SourceDisable(INT_ID_0, DTERM_TX_INT_REQ);
   PLIB_INT_SourceDisable(INT_ID_0, DTERM_RX_INT_REQ);
   PLIB_INT_SourceDisable(INT_ID_0, DTERM_ERR_INT_REQ);
}




//-- Interrupt handlers {{{



DTERM_INT_HANDLER__SYS()
   //void __ISR(DTERM_UART_INT_VECTOR_FOR_ISR) dterm__uart_int_handler(void)
{
#if DTERM__RX_ENABLED
   //-- is this Rx interrupt?
   if (
         PLIB_INT_SourceIsEnabled(INT_ID_0, DTERM_RX_INT_REQ)
         &&
         PLIB_INT_SourceFlagGet(INT_ID_0, DTERM_RX_INT_REQ)
      )
   {
      PLIB_INT_SourceFlagClear(INT_ID_0, DTERM_RX_INT_REQ);
      //IFS0CLR = (1 << 27);


      //-- from Errata: "The RXDA bit does not correctly reflect the RX FIFO status
      //   after an overrun event"
      //
      //   So, we check overrun flag here, and if it is set, then just clear it.
      if (PLIB_USART_ReceiverOverrunHasOccurred(DTERM_UART)){
         PLIB_USART_ReceiverOverrunErrorClear(DTERM_UART);
      }

      _dterm__isr_rx_handler();
   }
#endif

#if DTERM__BUFFERED_TX
   if (
         PLIB_INT_SourceIsEnabled(INT_ID_0, DTERM_TX_INT_REQ)
         &&
         PLIB_INT_SourceFlagGet(INT_ID_0, DTERM_TX_INT_REQ)
      )
   {
      PLIB_INT_SourceFlagClear(INT_ID_0, DTERM_TX_INT_REQ);

      _dterm__isr_tx_handler();
   }
#endif

}


// }}}



/***************************************************************************************************
 *  end of file
 **************************************************************************************************/


