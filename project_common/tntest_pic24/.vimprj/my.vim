
" определяем путь к папке .vimprj
let s:sPath = expand('<sfile>:p:h')

" указываем плагину project.tar.gz, какой файл проектов использовать
"
" ВНИМАНИЕ! 
"     для того, чтобы это работало, необходимо немного подправить
"     плагин project.tar.gz. См. http://habrahabr.ru/blogs/vim/102954/
let g:proj_project_filename=s:sPath.'/.vimprojects'

" Путь до основного окружения
let s:sEnvPath = $INDEXER_PROJECT_ROOT."/../.."

"let g:indexer_ctagsDontSpecifyFilesIfPossible = 0
"let g:indexer_enableWhenProjectDirFound = 0
"let g:indexer_ctagsJustAppendTagsAtFileSave = 1
" указываем файл проекта, который лежит в папке .vim
let g:indexer_indexerListFilename = s:sPath.'/.indexer_files'
"let g:indexer_useSedWhenAppend = 0
"let g:indexer_projectsSettingsFilename = s:sPath.'/.vimprojects'
"let g:indexer_projectName = 'Osa'

let g:indexer_getAllSubdirsFromIndexerListFile = 1

" подключаем теги используемых библиотек
"exec "set tags+=".s:sEnvPath."/lib/mc/csp/.vimprj/.indexer_files_tags/mc_csp"
"exec "set tags+=".s:sEnvPath."/lib/mc/tnkernel/.vimprj/.indexer_files_tags/mc_tnkernel"
"exec "set tags+=".s:sEnvPath."/lib/c/utils/.vimprj/.indexer_files_tags/utils"
"exec "set tags+=".s:sEnvPath."/lib/mixed/fldata/c/.vimprj/.indexer_files_tags/flash_addressed_list_c"

exec "set tags+=".$VIMPRJ_ENV__PATH__LIB_MC_CSP__COMMON__TAGS
exec "set tags+=".$VIMPRJ_ENV__PATH__LIB_MC_CSP__PIC32__TAGS
exec "set tags+=".$VIMPRJ_ENV__PATH__LIB_MC_TNKERNEL_DF__TAGS
exec "set tags+=".$VIMPRJ_ENV__PATH__LIB_C_UTILS__TAGS

" check microchip peripheral lib tags
let periph_include_path = $MACHINE_LOCAL_CONF__PATH__MICROCHIP_XC32."/pic32-libs/include/peripheral"
let periph_include_tags = periph_include_path."/tags"
if (!filereadable(periph_include_tags))
   echo "creating microchip peripheral lib include tags ".periph_include_tags." ..."
   let s:sResp = system("ctags -R --exclude=legacy -f ".periph_include_tags." ".periph_include_path."")
   echo s:sResp
   if !empty(s:sResp)
      call confirm("error creating microchip tags (".periph_include_tags."): ".s:sResp)
   endif
endif

exec "set tags+=".periph_include_tags

let periph_source_path = $MACHINE_LOCAL_CONF__PATH__MICROCHIP_XC32."/pic32-libs/peripheral"
let periph_source_tags = periph_source_path."/tags"
if (!filereadable(periph_source_tags))
   echo "creating microchip peripheral lib source tags ".periph_source_tags." ..."
   let s:sResp = system("ctags -R --exclude=legacy -f ".periph_source_tags." ".periph_source_path."")
   echo s:sResp
   if !empty(s:sResp)
      call confirm("error creating microchip peripheral lib tags (".periph_source_tags.") : ".s:sResp)
   endif
endif

exec "set tags+=".periph_source_tags


" добавляем пути библиотек в path
"exec "set path+=".s:sEnvPath."/lib"
"exec "set path+=".s:sEnvPath."/common/bk90"

" указываем специфические для данного проекта настройки форматирования
let &tabstop = 3
let &shiftwidth = 3

" указываем прочие настройки для данного проекта
"let g:indexer_ctagsCommandLineOptions = '--c-kinds=+l --fields=+iaS --extra=+q'

let s:o_dir = $INDEXER_PROJECT_ROOT.'/output/tmp/compiled_in_vim'

if !isdirectory(s:o_dir)
   call mkdir(s:o_dir, "p")
endif

"call confirm("src appl/my.vim")

"let &makeprg = MakeprgGenerate($INDEXER_PROJECT_ROOT.'/'.s:sMcpProject, 'MPLAB_8_mcp', {
         "\ '-o': s:o_dir.'/%:t:r.o',
         "\ })

let g:indexer_handlePath = 0

call CheckNeededSymbols(
         \  "The following items needs to be defined in your vimfiles/machine_local_conf/current/variables.vim file to make things work: ",
         \  "",
         \  [
         \     '$MACHINE_LOCAL_CONF__PATH__MICROCHIP_XC16',
         \  ]
         \  )

let s:sCompilerExecutable = ''
if has('win32') || has('win64')
   let s:sCompilerExecutable = 'xc16-gcc.exe'
else
   let s:sCompilerExecutable = 'xc16-gcc'
endif

" Path to MPLAB .mcp project
let s:sProject = 'tntest_pic24.X'
call envcontrol#set_project_file($INDEXER_PROJECT_ROOT.'/'.s:sProject, 'MPLAB_X', {
         \        'parser_params': {
         \           'compiler_command_without_includes' : ''
         \              .'"'.s:sCompilerExecutable.'" '
         \              .' -g -D__DEBUG -x c -c -mcpu=24FJ256GB106 -Wall -msmart-io=1 -msfr-warn=off '
         \              .' -mlarge-code -mlarge-data -mconst-in-code '
         \              .' -Os -MMD -MF "'.s:o_dir.'/%:t:r.o.d" -o "'.s:o_dir.'/%:t:r.o"',
         \        },
         \        'handle_clang' : 1,
         \        'add_paths' : [
         \           $MACHINE_LOCAL_CONF__PATH__MICROCHIP_XC16.'/include',
         \           $MACHINE_LOCAL_CONF__PATH__MICROCHIP_XC16.'/support/PIC24F/h',
         \           $MACHINE_LOCAL_CONF__PATH__MICROCHIP_XC16.'/support/generic/h',
         \        ],
         \        'clang_add_params' : ''
         \              .'  -D __C30__'
         \              .'  -D __CLANG_FOR_COMPLETION__'
         \              .'  -D __PIC24F__'
         \              .'  -D __PIC24FJ256GB106__'
         \              .'  -Wno-builtin-requires-header'
         \              .'  -Wno-unused-value'
         \              .'  -Wno-implicit-function-declaration'
         \              .'  -Wno-attributes'
         \              .'  -Wno-invalid-source-encoding'
         \              ,
         \ })

         "\        'compiler_executable' : '"C:\Program Files\Microchip\mplabc30\v3.25\bin\pic30-gcc.exe"',

"let &makeprg = 'pic30-gcc.exe -mcpu=24FJ256GB106 -x c -c   "%:p" -o"'.s:o_dir.'/%:t:r.o" -I"'.s:sEnvPath.'\lib\c" -I"'.s:sEnvPath.'\lib\mc" -I"'.$INDEXER_PROJECT_ROOT.'\source" -I"'.$INDEXER_PROJECT_ROOT.'\source\appl" -I"'.$INDEXER_PROJECT_ROOT.'\source\conf" -I"'.$INDEXER_PROJECT_ROOT.'\source\utils\dtp" -I"'.$INDEXER_PROJECT_ROOT.'\source\utils" -I"'.$INDEXER_PROJECT_ROOT.'\source\displays" -I"'.$INDEXER_PROJECT_ROOT.'\source\bsp" -I"'.$INDEXER_PROJECT_ROOT.'\source\param" -I"'.s:sEnvPath.'\common\bk100" -I"'.s:sEnvPath.'\common\bk90" -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -g -Wall -mlarge-code -mlarge-data -mconst-in-code -Os'

"call confirm("appl")

