/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include <csp.h>
#include "bsp_tnkernel.h"
#include "task_conf.h"
#include "bsp/bsp.h"

#include "dterm.h"

#include "appl_sys.h"
#include "appl_cfg/appl_cfg.h"
#include "appl_cfg/appl_cfg__dbg__deb_halt.h"

#include "appl_tntest/appl_tntest_timer.h"


#ifndef APPL_CFG__DEBUG
#  error APPL_CFG__DEBUG is not defined
#endif

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/



#define IDLE_TASK_STACK_SIZE           (TN_MIN_STACK_SIZE + 32 + TASK_STACK_INTERRUPT_ADD)
#define INT_STACK_SIZE                 (TN_MIN_STACK_SIZE + 200)

//-- system timer cfg {{{
#define TN_SYS_TMR                     &TMR1
#define TN_SYS_TMR_INT_REQ             INT_TMR1

//#define TN_SYS_TMR_INT_HNDL__SYS()     void __attribute__((__interrupt__, auto_psv)) _T1Interrupt(void)
#define TN_SYS_TMR_INT_HNDL__SYS()     tn_p24_soft_isr(_T1Interrupt, auto_psv)

#define TN_SYS_TMR_PRESCALER           TMR_PS_1_64
#define TN_SYS_TMR_PRESCALER_VALUE     64

#define TN_SYS_TMR_PR                  (BSP_FPB / TN_SYS_TMR_PRESCALER_VALUE / 1000/*ms*/)

#define SYS_TIME_MS(a)                 (TN_TIMEOUT)((a) / TN_SYS_TMR_PR_MS)
// }}}


/***************************************************************************************************
 *                                  PRIVATE FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

TN_TASK_STACK_DEF(idle_task_stack,  IDLE_TASK_STACK_SIZE);
TN_TASK_STACK_DEF(interrupt_stack,  INT_STACK_SIZE);


volatile TN_TickCnt appl_sys_tick_count = 0xffffff01;
volatile TN_TickCnt next_tick_count = 0;


/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

extern TN_UWORD task_conf_stack[];
extern TN_UWORD task_input_stack[];


/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/



/***************************************************************************************************
 *    INTERRUPT HANDLERS
 **************************************************************************************************/

TN_SYS_TMR_INT_HNDL__SYS()
{
   if (csp_int_flag_check(TN_SYS_TMR_INT_REQ)){
      csp_int_flag_clr(TN_SYS_TMR_INT_REQ);

#if TN_DYNAMIC_TICK

      appl_sys_tick_count++;

#if 1
      if (next_tick_count > 0 && next_tick_count != TN_WAIT_INFINITE){
         next_tick_count--;

         if (next_tick_count == 0){
            tn_tick_int_processing();
         }
      }
#else
      tn_tick_int_processing();
#endif

#else
      tn_tick_int_processing();
#endif

      appl_tntest_timer__sys_int_check();
   }
}


/***************************************************************************************************
 *                                        PRIVATE FUNCTIONS
 **************************************************************************************************/

#if TN_DYNAMIC_TICK

void _tick_schedule(TN_TickCnt timeout)
{
   next_tick_count = timeout;
}

TN_TickCnt _tick_cnt_get(void)
{
   return appl_sys_tick_count;
}

#endif

static void _user_task_create (void)
{
   //-- create task_conf that will configure the system and create all other tasks
   task_conf__create();
}

/**
 *  Idle task hook function
 */
static void _idle_task_callback (void)
{
}

static void _tn_deadlock_callback(
      TN_BOOL active, struct TN_Mutex *mutex, struct TN_Task *task
      )
{
   //-- NOTE: called from interrupt with other interrupts disabled

   if (active){
      DTMSG(DT_WRN | DT_NOWAIT | DT_SIMPLE, "deadlock");
   } else {
      DTMSG(DT_WRN | DT_NOWAIT | DT_SIMPLE, "deadlock off");
   }
}

/**
 * System timer initialization
 */
static void _sys_timer_init(void)
{
   csp_adc_an_clr(&ADC1, ADC_AN_ALL);

   csp_cn_dis(&CN1, CN_PIN_ALL);
   csp_cn_dis(&CN2, CN_PIN_ALL);
   csp_cn_dis(&CN3, CN_PIN_ALL);
   csp_cn_dis(&CN4, CN_PIN_ALL);
   csp_cn_dis(&CN5, CN_PIN_ALL);
   csp_cn_dis(&CN6, CN_PIN_ALL);



   //-- system timer
   csp_tmr_a_conf_set  (TN_SYS_TMR, TMR_DIS | TMR_IDLE_CON | TMR_GATE_DIS | TN_SYS_TMR_PRESCALER | TMR_SOURCE_INT);
   csp_tmr_a_period_set(TN_SYS_TMR, TN_SYS_TMR_PR - 1);

   //-- setup system timer interrupt
   csp_int_priority_set(TN_SYS_TMR_INT_REQ, 4);
   csp_int_flag_clr    (TN_SYS_TMR_INT_REQ);
   csp_int_en          (TN_SYS_TMR_INT_REQ);

   csp_tmr_a_en(TN_SYS_TMR);
}


/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

int main (void)
{
   //-- disable interrupts globally
   tn_arch_int_dis();

   appl_tntest_timer__init();

   _sys_timer_init();


   tn_callback_deadlock_set(_tn_deadlock_callback);

#if TN_DYNAMIC_TICK
   tn_callback_dyn_tick_set(_tick_schedule, _tick_cnt_get);
#endif

   tn_sys_start(
         idle_task_stack,
         IDLE_TASK_STACK_SIZE,
         interrupt_stack,
         INT_STACK_SIZE,
         _user_task_create,
         _idle_task_callback
         );

   return 0;
}



/***************************************************************************************************
 *  end of file
 **************************************************************************************************/


