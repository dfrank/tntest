/***************************************************************************************************
 *   Project:       P-0173 - ��-90 - ��������� ��� ��-100
 *   Author:        Alex Borisov
 ***************************************************************************************************
 *   Distribution:  Copyright � 2009-2010 �����
 *
 ***************************************************************************************************
 *   MCU Family:    PIC24FJ256GB106
 *   Compiler:      Microchip C30 3.22
 ***************************************************************************************************
 *   File:          appl_ex.c
 *   Description:   ������� ��������� ����������
 *
 ***************************************************************************************************
 *   History:       24.06.2010 - [Alex Borisov] - file created
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include <csp.h>

/***************************************************************************************************
 * DEFINITIONS
 **************************************************************************************************/


/***************************************************************************************************
 * PRIVATE TYPES
 **************************************************************************************************/


/***************************************************************************************************
 * PRIVATE DATA
 **************************************************************************************************/

/***************************************************************************************************
 * PRIVATE FUNCTIONS
 **************************************************************************************************/

/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

void __attribute((__interrupt__, auto_psv)) _AddressError (void)
{
   CSP_DEB_HALT();
   //for(;;);
}

/**
 *  
 */
void __attribute((__interrupt__, auto_psv)) _StackError (void)
{
   CSP_DEB_HALT();
   //    for(;;);
}

/**
 *  
 */
void __attribute((__interrupt__, auto_psv)) _MathError (void)
{
   CSP_DEB_HALT();
   //for(;;);
}

/**
 *  
 */
void __attribute((__interrupt__, auto_psv)) _OscillatorFail (void)
{
   CSP_DEB_HALT();
   //for(;;);
}

/**
 * 
 *
 * 
 */
void __attribute((__interrupt__, auto_psv)) _DefaultInterrupt (void)
{
   CSP_DEB_HALT();
   //for(;;);
}

/***************************************************************************************************
 *  end of file: appl_ex.c
 **************************************************************************************************/


