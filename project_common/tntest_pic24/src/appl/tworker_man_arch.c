/*******************************************************************************
 *    Description: TODO
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include <csp.h>
#include "tworker_man_arch.h"
#include "bsp_mcu_config.h"
#include "bsp_tnkernel.h"
#include "tworker_man.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

#define TWORKMAN_TMR                   &TMR2
#define TWORKMAN_TMR_INT_REQ           INT_TMR2
#define TWORKMAN_TMR_INT_HNDL__SYS()   tn_p24_soft_isr(_T2Interrupt, auto_psv)


#define TWORKMAN_TMR_PRESCALER         TMR_PS_1_64
#define TWORKMAN_PRESCALER_HUMAN_VALUE 64

//???
#define TWORKMAN_TMR_PR                (BSP_FPB / TWORKMAN_PRESCALER_HUMAN_VALUE / 100/*ms*/)





/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void tworker_man_arch__init(void)
{
   //-- setup int1
   csp_tmr_b_conf_set  (TWORKMAN_TMR, TMR_DIS | TMR_IDLE_CON | TMR_GATE_DIS | TWORKMAN_TMR_PRESCALER | TMR_SOURCE_INT);
   csp_tmr_b_period_set(TWORKMAN_TMR, TWORKMAN_TMR_PR - 1);

   csp_tmr_b_en(TWORKMAN_TMR);

   //-- setup system timer interrupt
   csp_int_priority_set(TWORKMAN_TMR_INT_REQ, 2);
   csp_int_flag_clr    (TWORKMAN_TMR_INT_REQ);
   csp_int_en          (TWORKMAN_TMR_INT_REQ);

}


TWORKMAN_TMR_INT_HNDL__SYS()
   //void __ISR(CSP_INT_VECTOR__TIMER_1) tworker_isr(void)
{

   if (csp_int_flag_check(TWORKMAN_TMR_INT_REQ)){
      csp_int_flag_clr(TWORKMAN_TMR_INT_REQ);

      //-- call actual handler
      tworker_man__int_handler();

   }
}


/*******************************************************************************
 *    end of file
 ******************************************************************************/



