/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include <stdlib.h>
#include "bsp.h"                    /* Board Support Package        */

#include "bsp_tnkernel.h"
#include "tmalloc.h"
#include "malloc_reg.h"
#include "dterm.h"

#include "appl.h"
#include "appl_mutex.h"

#include "umm_malloc/umm_malloc.h"

#include "appl_cfg__malloc.h"

#ifndef APPL_CFG__MALLOC
#  error APPL_CFG__MALLOC is not defined
#endif

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

#define  _MAX_MEM_PART_SIZE      10000

#if APPL_CFG__MALLOC == APPL_CFG__MALLOC__BUILTIN
#  define  _MALLOC     malloc
#  define  _FREE       free
#elif APPL_CFG__MALLOC == APPL_CFG__MALLOC__UMM_MALLOC
#  define  _MALLOC     umm_malloc
#  define  _FREE       umm_free
#endif



/***************************************************************************************************
 *                                  PRIVATE FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

static T_ApplMutex TN_DATA _mutex;

/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                        PRIVATE FUNCTIONS
 **************************************************************************************************/

/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

void tmalloc__init()
{
   appl_mutex__create(&_mutex , BSP_TNKERNEL__MUTEX_PROTOCOL, BSP_TNKERNEL__CEIL_PRIORITY);
}

void *tmalloc_func(size_t size IF_MALLOC_REG_SAVE_FILE(, const char *file, U16 line)){
   void *ret = NULL;
   appl_mutex__lock(&_mutex, TN_WAIT_INFINITE);

   ret = _MALLOC(size);
   if (size > 0 && ret == NULL){
      DTMSG(DT_ERR | DT_SYNC, 
            "OUT OF MEMORY: failed to allocate %d bytes"
            IF_MALLOC_REG_SAVE_FILE(" in %s:%d")
            ,
            size
            IF_MALLOC_REG_SAVE_FILE(, file, line)
            );

      appl__mem_usage__report();

#ifdef __DEBUG
      CSP_DEB_HALT();
#else
      appl__out_of_memory_msg_show();
      for (;;){}
#endif
      SYSRETVAL_CHECK_TOUT(tn_task_sleep(1));
   }
#if APPL_MALLOC_REG_ENABLED
   if (ret){
      MemAllocRegister(MEM_ALLOC_REG_MODE_ALLOC, ret, size IF_MALLOC_REG_SAVE_FILE(, file, line));
   }
#endif
   appl_mutex__unlock(&_mutex);

   return ret;
}


void tfree(void *ptr){
   appl_mutex__lock(&_mutex, TN_WAIT_INFINITE);
#if APPL_MALLOC_REG_ENABLED
   enum E_MemAllocRegRes reg_res = MEM_ALLOC_REG_RES__OK;
   if (ptr != NULL){
      reg_res = MemAllocRegister(MEM_ALLOC_REG_MODE_FREE, ptr, 0 IF_MALLOC_REG_SAVE_FILE(, NULL, 0));
   } else {
   }
#endif
   _FREE(ptr);
   appl_mutex__unlock(&_mutex);

#if APPL_MALLOC_REG_ENABLED
   if (reg_res != MEM_ALLOC_REG_RES__OK){
      CSP_DEB_HALT();
   }
#endif
}


void *tmalloc_wait_func(size_t size IF_MALLOC_REG_SAVE_FILE(, const char *file, U16 line)){
   void *ret = 0;
   do {
      ret = tmalloc_func(size IF_MALLOC_REG_SAVE_FILE(, file, line));
   } while (ret == 0);
   return ret;
}


int tmalloc__get_max_mem_part(void)
{
   appl_mutex__lock(&_mutex, TN_WAIT_INFINITE);

   int cur_part_size = _MAX_MEM_PART_SIZE;
   void *p_pt = NULL;
   int step   = 1000;   //-- starting step, it will be divided by 10 several times until it is 1
   bool bool_decrement_step = false;

   while (cur_part_size > 0 || step > 1){

      if (bool_decrement_step){
         if (step > 1){
            //-- step is more 1 now, so, divide it and try again.
            cur_part_size += step;
            step /= 10;
         } else {
            break;
         }
         bool_decrement_step = false;
      }

      p_pt = _MALLOC(cur_part_size);

      if (p_pt == NULL){
         cur_part_size -= step;

         if (cur_part_size <= 0){
            bool_decrement_step = true;
         }

      } else {
         _FREE(p_pt);

         bool_decrement_step = true;
      }
   }

   appl_mutex__unlock(&_mutex);
   return cur_part_size;
}

int tmalloc__echo_max_mem_part(void)
{
   int max_mem_part = tmalloc__get_max_mem_part();
   DTMSG_I("max_mem_part=%d bytes", max_mem_part);
   return max_mem_part;
}


/***************************************************************************************************
 *  end of file
 **************************************************************************************************/

