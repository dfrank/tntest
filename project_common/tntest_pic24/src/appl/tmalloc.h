/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/


#ifndef _TMALLOC_H
#define _TMALLOC_H


/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include <csp_types.h>
#include "appl_cfg/appl_cfg__dbg__malloc_reg.h"

#ifndef APPL_MALLOC_REG_ENABLED
#error APPL_MALLOC_REG_ENABLED should be defined
#endif

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/



/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

#if APPL_MALLOC_REG_ENABLED
#   define tmalloc(size)        tmalloc_func        ((size)IF_MALLOC_REG_SAVE_FILE(, __FILE__, __LINE__))
#   define tmalloc_wait(size)   tmalloc_wait_func   ((size)IF_MALLOC_REG_SAVE_FILE(, __FILE__, __LINE__))
#else
#   define tmalloc(size)        tmalloc_func        ((size))
#   define tmalloc_wait(size)   tmalloc_wait_func   ((size))
#endif



/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

extern void    tmalloc__init     ();
extern void   *tmalloc_func      (size_t size IF_MALLOC_REG_SAVE_FILE(, const char *file, U16 line));
extern void   *tmalloc_wait_func (size_t size IF_MALLOC_REG_SAVE_FILE(, const char *file, U16 line));
extern void    tfree             (void *ptr);

int tmalloc__get_max_mem_part(void);
int tmalloc__echo_max_mem_part(void);

#endif // _TMALLOC_H

/***************************************************************************************************
  end of file
 **************************************************************************************************/

