/***************************************************************************************************
 *   Project:       U_0150_FW03 - LED-������ B65/B95
 *   Author:        Alex Borisov
 ***************************************************************************************************
 *   Distribution:  Copyright � 2010 ������ ����
 *
 ***************************************************************************************************
 *   MCU Family:    PIC24F16KA101     
 *   Compiler:      Microchip C30 3.22
 ***************************************************************************************************
 *   File:          xprint_conf.h
 *   Description:   ������������ ������ ���������������� ������
 *
 ***************************************************************************************************
 *   History:       16.06.2010 - [Alex Borisov] - file created
 *
 **************************************************************************************************/

#ifndef __XPRINTF_CONF_H
#define __XPRINTF_CONF_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

#define PGM_GET_CHAR(s) (*(s))                  /* Can be redeclared for Harvard CPU with special const access */
#define pgm_get_char(s) (*(s))                  /* Can be redeclared for Harvard CPU with special const access */

/* Formatter option */

/*
 * To reduce size in applications not using real numbers or long integers the formatter may be
 * compiled to exclude certain parts.  This is controlled by giving a -D option a compilation time:
 *
 * \code
 *  -D CONFIG_PRINTF=PRINTF_FULL         Full ANSI printf formatter, with some C99 extensions
 *  -D CONFIG_PRINTF=PRINTF_NOFLOAT      Exclude support for floats
 *  -D CONFIG_PRINTF=PRINTF_REDUCED      Simplified formatter (see below)
 *  -D CONFIG_PRINTF=PRINTF_NOMODIFIERS  Exclude 'l', 'z' and 'h' modifiers in reduced version
 *  -D CONFIG_PRINTF=PRINTF_DISABLED     No formatter at all
 * \endcode
 *
 * Code size on AVR4 with GCC 3.4.1 (-O2):
 *   PRINTF_FULL        2912byte (0xB60)
 *   PRINTF_NOFLOAT     1684byte (0x694)
 *   PRINTF_REDUCED      924byte (0x39C)
 *   PRINTF_NOMODIFIERS  416byte (0x1A0)
 *
 * Code/data size in words on DSP56K with CodeWarrior 6.0:
 *   PRINTF_FULL         1493/45
 *   PRINTF_NOFLOAT      795/45
 *   PRINTF_REDUCED      482/0
 *   PRINTF_NOMODIFIERS  301/0
 *
 * Code size on PIC24 with MCHP C30 3.22 (-Os):
 *   PRINTF_FULL         4932 (with float lib)
 *   PRINTF_NOFLOAT      1410
 *   PRINTF_REDUCED      837
 *   PRINTF_NOMODIFIERS  492
 *   
 *
 * The reduced version of formatter is suitable when program size is critical rather than formatting
 * power.  This routine uses less than 20 bytes of stack space which makes it practical even in
 * systems with less than 256 bytes of user RAM.
 *
 * The only formatting specifiers supported by the reduced formatter are:
 * \code
 *    %% %c %s %d %o %x %X and %hd %ho %hx %hX %ld %lo %lx %lX
 * \endcode
 *
 * It means that real variables are not supported as well as field width and precision arguments. 
 */

#define CONFIG_PRINTF_OCTAL_FORMATTER 0         /* 1 - include Octal formatters */
#define CONFIG_PRINTF_RETURN_COUNT    1         /* 1 - formatter funcion return number of output symbols */
#define CONFIG_PRINTF_N_FORMATTER     0         /* 1 - include %n formatter */
#define CONFIG_PRINTF_FIXED_POINT_INT 1         /* 1 - include ',' formatter, usage like '.' : "%,2d" */

#define CONFIG_PRINTF               PRINTF_NOFLOAT

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/
/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/
/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

#endif /* __XPRINTF_CONF_H */
/***************************************************************************************************
    end of file: xprintf_conf.h
 **************************************************************************************************/
