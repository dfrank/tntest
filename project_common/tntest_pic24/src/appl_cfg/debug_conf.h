/***************************************************************************************************
 *   Project:       U_0150_FW03 - LED-������ B65/B95
 *   Author:        Alex Borisov
 ***************************************************************************************************
 *   Distribution:  Copyright � 2010 ������ ����
 *
 ***************************************************************************************************
 *   MCU Family:    PIC24F16KA101     
 *   Compiler:      Microchip C30 3.22
 ***************************************************************************************************
 *   File:          debug_conf.h
 *   Description:   
 *
 ***************************************************************************************************
 *   History:       16.06.2010 - [Alex Borisov] - file created
 *
 **************************************************************************************************/

#ifndef _DEBUG_CONF_H
#define _DEBUG_CONF_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include "utils/dterm.h"

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

#if DTERM__BUFFERED_TX
#  define      DTERM__USUAL_SETT    DTERM__ASYNC
#  define      DTERM__INT_SETT      DTERM__ASYNC_NOWAIT
#  define      DTERM__CRITICAL_SETT DTERM__SYNC
#else
#  define      DTERM__USUAL_SETT    DTERM__SYNC_IGN_BUF
#  define      DTERM__INT_SETT      DTERM__SYNC_IGN_BUF
#  define      DTERM__CRITICAL_SETT DTERM__SYNC_IGN_BUF
#endif



#if 1
#if defined(__DEBUG)
#  define  LOG_LEVEL      LOG_LVL_INFO
#else
#  define  LOG_LEVEL      LOG_LVL_INFO  //LOG_LVL_NONE
#endif
#else
#  define  LOG_LEVEL      LOG_LVL_NONE
#endif

#define  LOG_FORMAT     LOG_FMT_FUNC
//#define  LOG_METHOD     LOG_MTH_BUFF
#define  LOG_METHOD     LOG_MTH_DIRECT

#if 0
#if LOG_METHOD ==  LOG_MTH_BUFF
#  define LOG_OUT                         bsp_dcom_put
#elif LOG_METHOD ==  LOG_MTH_DIRECT
#  define LOG_OUT                         bsp_dcom_critical_put
#endif
#endif

#define LOG_LINE_ENDING    /* nothing */

#  define LOG_OUT_MSG_ADV(dterm_sett, ...)   dterm__msg_text__send(dterm_sett, ## __VA_ARGS__)
#  define LOG_OUT_RAW_ADV(dterm_sett, ...)   dterm__string__send(dterm_sett, DTERM_MSG_TYPE__TEXT, ## __VA_ARGS__)

#  define LOG_OUT_MSG(...)                dterm__msg_text__send(DTERM__USUAL_SETT, ## __VA_ARGS__)
#  define LOG_OUT_RAW(...)                dterm__string__send(DTERM__USUAL_SETT, DTERM_MSG_TYPE__TEXT, ## __VA_ARGS__)

#  define LOG_OUT_MSG_I(...)              dterm__msg_text__send(DTERM__INT_SETT, ## __VA_ARGS__)
#  define LOG_OUT_RAW_I(...)              dterm__string__send(DTERM__INT_SETT, DTERM_MSG_TYPE__TEXT, ## __VA_ARGS__)

#  define LOG_OUT_MSG_CRIT(...)           dterm__msg_text__send(DTERM__CRITICAL_SETT, ## __VA_ARGS__)
#  define LOG_OUT_RAW_CRIT(...)           dterm__string__send(DTERM__CRITICAL_SETT, DTERM_MSG_TYPE__TEXT, ## __VA_ARGS__)

//#  define LOG_OUT_BIN                     bsp_dcom_critical_put_binary  //-- TODO: isn't used now
#  define LOG_USE_DTERMINAL               1

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/


#endif /* _DEBUG_CONF_H */
/***************************************************************************************************
  end of file: debug_conf.h
 **************************************************************************************************/
