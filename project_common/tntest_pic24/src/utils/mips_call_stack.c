/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "mips_call_stack.h"
#include <csp.h>
#include "dterm.h"

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

#define  _MAX_CALL_STACK_SIZE    7

#define  _LOG_INF(...)    //DTMSG_I(__VA_ARGS__)

/***************************************************************************************************
 *                                  PRIVATE FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

static unsigned int _call_stack_addr[ _MAX_CALL_STACK_SIZE ];


/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                        PRIVATE FUNCTIONS
 **************************************************************************************************/

static unsigned int _get_ra()
{
   unsigned int p_ret;
   asm volatile("move %0, $ra" : "=r" (p_ret)); 
   return p_ret;
}

static unsigned int _get_sp()
{
   unsigned int p_ret;
   asm volatile("move %0, $sp" : "=r" (p_ret)); 
   return p_ret;
}

static int _is_program_addr_valid(unsigned int addr)
{
   return (addr >= 0x9d000000 && addr <= (0x9fc00000));
}

static int _is_ram_addr_valid(unsigned int addr)
{
   return (addr >= 0xa0000000 && addr <= (0xafffffff));
}

/* 
 * get previous stack pointer and return address given the current ones
 *
 * NOTE: we should NOT make it static, because then it will be inlined into mips_call_stack__call_stack__get(),
 * so mips_call_stack__call_stack__get() doesn't even allocate stack frame (and therefore doesn't store $ra),
 * so, this leads to wrong behavior.
 *
 * If this function is non-static, then it will not be inlined, stack frame allocated and $ra is stored,
 * and everything works.
 *
 **/
/*NOT static*/int _mips_call_stack__get_prev_sp_ra(unsigned int *prev_sp, unsigned int *prev_ra, unsigned int sp, unsigned int ra)
{
   unsigned *wra = (unsigned *)ra;
   int spofft;

   if (_is_ram_addr_valid(sp) && _is_program_addr_valid(ra)){

      /* scan towards the beginning of the function -
         addui sp,sp,spofft should be the first command */
      _LOG_INF("sp=0x%x, ra=0x%x, looking for addiu sp, sp, spofft...", sp, ra);
      _LOG_INF("test");
      while(_is_program_addr_valid((unsigned int)wra)) {
         //_LOG_INF("wra=0x%x, value=0x%x ...", wra, (*wra >> 16));
         if ((*wra >> 16) == 0x27bd){
            _LOG_INF("match! wra=0x%x, value=0x%x", wra, (*wra >> 16));
            break;
         }
         /* test for "scanned too much" elided */
         wra--;
      }

      if (_is_program_addr_valid((unsigned int)wra)){
         spofft = ((int)*wra << 16) >> 16; /* sign-extend */
         *prev_sp = sp - spofft;

         _LOG_INF("spofft=%d, set prev_sp=0x%x", spofft, *prev_sp);

         _LOG_INF("looking for sw $ra, raofft(sp)...");

         /* now scan forward for sw r31, raofft(sp) */
         while(wra < (unsigned *)ra) {
            _LOG_INF("wra=0x%x, value=0x%x", wra, (*wra >> 16));
            if((*wra >> 16) == 0xafbf) {
               int raofft = ((int)*wra << 16) >> 16; /* sign-extend */
               *prev_ra = *(unsigned int *)(sp + raofft);
               _LOG_INF("match! wra=0x%x, sp=0x%x, raofft=0x%x, set prev_ra=0x%x", wra, sp, raofft, *prev_ra);
               return 1;
            }
            wra++;
         }
      } else {
         _LOG_INF("wra is wrong: 0x%x", wra);
      }
   }

   _LOG_INF("failed to find where $ra is saved");
   return 0; /* failed to find where ra is saved */
}


/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/

int mips_call_stack__call_stack__get(unsigned int *retaddrs, int max_size)
{
   unsigned int sp = _get_sp(); /* stack pointer register */
   unsigned int ra = _get_ra(); /* return address register */
   /* adjust sp by the offset by which this function
    *      has just decremented it */
   int *funcbase = (int *)(int)&mips_call_stack__call_stack__get;

   /* funcbase points to an addiu sp,sp,spofft command */
   int spofft = (*funcbase << 16) >> 16; /* 16 LSBs */
   int i = 0;

   //-- TODO: without this (or some other LOG_INF_I) stack retrieving routine doesn't work
   //         (exception is thrown)
   _LOG_INF("sp=0x%x, ra=0x%x, funcbase=0x%x, spofft=%d", sp, ra, funcbase, spofft);

   sp -= spofft;

   _LOG_INF("new sp=0x%x", sp);

   do {
      _LOG_INF("ra=0x%x", ra);
      if(i < max_size) {
         retaddrs[i++] = ra;
      }
   } while(_mips_call_stack__get_prev_sp_ra(&sp, &ra, sp, ra));

   return i; /* stack size */
}

void mips_call_stack__call_stack__print_err(void)
{
   int call_stack_size = mips_call_stack__call_stack__get(_call_stack_addr, countof(_call_stack_addr));
   int i;
   DTMSG(DT_ERR | DT_SYNC, "call_stack_size=%d", call_stack_size);

   for (i = 0; i < call_stack_size; i++){
      DTMSG(DT_ERR | DT_SYNC, "call stack item #%d: 0x%x", i, _call_stack_addr[i]);
   }
}



/***************************************************************************************************
 *  end of file
 **************************************************************************************************/


