/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

#ifndef _MCU_CONFIG_H
#define _MCU_CONFIG_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include <csp.h>
#include "bsp_mcu_config.h"

/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

__CSP_CONFIG_1(JTAG_DIS                     |   /* JTAG disabled                            */
#if defined(__DEBUG)
      CODE_PROTECT_DIS             |   /* Code Protect disabled                    */
#else
      CODE_PROTECT_EN              |   /* TODO:change Code Protect enabled                     */
#endif
      CODE_WRITE_EN                |   /* Code write enabled                       */
#if defined(__DEBUG)
      BACKGROUND_DEBUG_EN          |   /* Debug enabled                            */
      EMULATION_EN                 |   /* Emulation enabled                        */
#else
      BACKGROUND_DEBUG_DIS         |   /* Debug enabled                            */
      EMULATION_DIS                |   /* Emulation enabled                        */
#endif
      ICD_PIN_PGX2                 |   /* PGC2/PGD2 pins used for debug            */
#if defined(__DEBUG)
      WDT_DIS                      |   /* WDT disabled                             */
#else
      WDT_DIS                      |   /* WDT disabled                             */
#endif
      WDT_WINDOW_DIS               |   /* WDT mode is not-windowed                 */
WDT_PRESCALE_32              |   /* WDT prescale is 1:32                     */
WDT_POSTSCALE_1                  /* WDT postscale is 1:1                     */
);

__CSP_CONFIG_2(TWO_SPEED_STARTUP_DIS        |   /* Two-speed startup disabled               */
      USB_PLL_PRESCALE_4           |
      OSC_STARTUP_PRIMARY_PLL      |   /* Startup oscillator is Primary with PLL   */
      CLK_SW_DIS_CLK_MON_DIS       |   /* Clock switch disabled, monitor disabled  */
      OSCO_PIN_CLKO                |   /* OSCO pin is clockout                     */
      PPS_REG_PROTECT_DIS          |   /* PPS runtime change enabled               */
      USB_REG_DIS                  |
      PRIMARY_OSC_HS                   /* Primary oscillator mode is HS            */
      );

__CSP_CONFIG_3(CODE_WRITE_PROT_MEM_START    |   /* Code write protect block from start      */
      CODE_WRITE_CONF_MEM_DIS      |   /* Code config memory block not protected   */
      CODE_WRITE_BLOCK_DIS             /* Code block protect disabled              */
      );


/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

#endif // _MCU_CONFIG_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


