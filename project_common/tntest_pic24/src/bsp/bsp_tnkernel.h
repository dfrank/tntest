/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

#ifndef _BSP_TNKERNEL_H
#define _BSP_TNKERNEL_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/



#include "tn.h"

#define TN_DATA
#define TN_UWORD unsigned int
#define TN_RETVAL int
#define TN_TASK

#define BSP_TNKERNEL__MAKE_ALIG(a)      MAKE_ALIG(sizeof(a))
#define BSP_TNKERNEL__MUTEX_PROTOCOL    TN_MUTEX_ATTR_INHERIT//TN_MUTEX_ATTR_CEILING
#define BSP_TNKERNEL__CEIL_PRIORITY     7
#define TASK_STACK_INTERRUPT_ADD        0
#define BSP_TNKERNEL__DI()              tn_cpu_int_disable()
#define BSP_TNKERNEL__EI()              tn_cpu_int_enable()
#define tn_sys_interrupt(x)             tn_p24_soft_isr(x)
#define TN_INTERRUPT_LEVEL              6


extern void appl__echo_err_halt__tn_err(TN_RETVAL tnret, int line);

#define SYSRETVAL_DATA  /* nothing */

#define SYSRETVAL_CHECK(x) \
   ({int __rv = (x); if (__rv != TERR_NO_ERR){appl__echo_err_halt__tn_err(__rv, __LINE__);} __rv;})

#define SYSRETVAL_CHECK_TOUT(x) \
   ({int __rv = (x); if (__rv != TERR_NO_ERR && __rv != TERR_TIMEOUT){appl__echo_err_halt__tn_err(__rv, __LINE__);} __rv;})



/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/

#endif // _BSP_TNKERNEL_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


