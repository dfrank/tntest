/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         INCLUDED FILES
 **************************************************************************************************/

#include "bsp_dterm.h"

#include "appl_mutex.h"

/***************************************************************************************************
 *                                           DEFINITIONS
 **************************************************************************************************/

/***************************************************************************************************
 *                                  PRIVATE FUNCTION PROTOTYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                          PRIVATE TYPES
 **************************************************************************************************/

/***************************************************************************************************
 *                                           PRIVATE DATA
 **************************************************************************************************/

static T_ApplMutex _mutex;

static int _status_register = 0;

//-- for self-check: secondary lock should never be locked recursively,
//   since secondary lock just disables interrupts
static bool _secondary_locked = false;



/***************************************************************************************************
 *                                           PUBLIC DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                          EXTERNAL DATA
 **************************************************************************************************/

/***************************************************************************************************
 *                                  EXTERNAL FUNCTION PROTOTYPES
 **************************************************************************************************/

extern void _dterm__isr_tx_handler(void);
extern void _dterm__isr_rx_handler(void);


/***************************************************************************************************
 *                                        PRIVATE FUNCTIONS
 **************************************************************************************************/

/***************************************************************************************************
 *                                         PUBLIC FUNCTIONS
 **************************************************************************************************/


void bsp_dterm__user_init(void)
{
   appl_mutex__create(&_mutex, BSP_TNKERNEL__MUTEX_PROTOCOL, BSP_TNKERNEL__CEIL_PRIORITY);
}

void bsp_dterm__lock_primary(void)
{
   //-- if in the task context, lock mutex
   if (tn_sys_context_get() == TN_CONTEXT_TASK){
      appl_mutex__lock(&_mutex, TN_WAIT_INFINITE);
   }

   //-- here we should disable all interrupts in which dterm might be used
   //   (well, you're able to not disable some interrupt that uses dterm, 
   //   but then you should set DTERM__ALLOW_RECURSION to 1, which is 
   //   HIGHLY EXPERIMENTAL. See comments for DTERM__ALLOW_RECURSION)

}

void bsp_dterm__unlock_primary(void)
{
   //-- here we should re-enable all interrupts in which dterm might be used


   //-- if in the task context, unlock mutex
   if (tn_sys_context_get() == TN_CONTEXT_TASK){
      appl_mutex__unlock(&_mutex);
   }
}

void bsp_dterm__lock_secondary(void)
{
   _status_register = tn_arch_sr_save_int_dis();
   if (_secondary_locked){
      DTERM_DEB_HALT();
   }
   _secondary_locked = true;
   //asm volatile("di %0" : "=r" (_status_register));
}

void bsp_dterm__unlock_secondary(void)
{
   if (!_secondary_locked){
      DTERM_DEB_HALT();
   }
   _secondary_locked = false;
   tn_arch_sr_restore(_status_register);
   //asm volatile("mtc0 %0, $12" : : "r" (_status_register));
}


void bsp_dterm_task_sleep(void)
{
   //-- if in the task context, sleep
   if (tn_sys_context_get() == TN_CONTEXT_TASK){
      tn_task_sleep(1);
   }
}


//---- controller-dependent stuff --------------------------------------


void bsp_dterm__uart_on(void)
{
   csp_uart_conf_set(&DTERM_UART,

         UART_EN               |
         UART_TX_EN            |
         //UART_RX_EN            |
         UART_DEBUG_FREEZE_EN  |
         UART_IDLE_STOP        |
         UART_IRDA_DIS         |
         UART_RTS_SIMPLEX      |
         UART_PINS_TX_RX       |
         UART_LOOPBACK_DIS     |
         UART_RX_IDLE_1        |
         UART_TX_IDLE_1        |
         UART_HIGH_BAUD_EN     |
         UART_MODE_8_NO        |
         UART_STOP_1           |
         UART_TX_INT_MODE_3    |   /* Interrupt generated when the transmit buffer contains at least one empty space*/
         UART_RX_INT_MODE_3    |   /* Прерывание генерируется по каждому принятому символу */
         UART_RX_ADDR_DET_DIS
         );
}

void bsp_dterm__uart_off(void)
{
   csp_uart_conf_set(&DTERM_UART, UART_DIS);
}

int bsp_dterm__init(unsigned long periph_bus_clk_freq, unsigned long baudrate)
{
   /* Config UART hardware */

   //-- Enable UART
   //   (NOTE: we should do it before calling csp_uart_baud_set_auto() because 
   //   csp_uart_baud_set_auto() checks if UART_HIGH_BAUD_EN is set)
   bsp_dterm__uart_on();

   //-- set baud rate
   csp_uart_baud_set_auto(&DTERM_UART, periph_bus_clk_freq, baudrate);

   /* Config PPS module for UART TX */
   csp_gpio_pin_dir_out(GPIO_DEF(DTERM_TX));
   csp_gpio_pin_dir_in (GPIO_DEF(DTERM_RX));

   csp_pps_unlock();
   csp_pps_out_set(DTERM_TX_PPS_FUNC, DTERM_TX_PPS_PIN);   /* TX */
   csp_pps_inp_set(DTERM_RX_PPS_FUNC, DTERM_RX_PPS_PIN);   /* RX */
   csp_pps_lock();

   csp_int_priority_set(DTERM_TX_INT_REQ,  DTERM_UART_INT_PRI);
   csp_int_priority_set(DTERM_RX_INT_REQ,  DTERM_UART_INT_PRI);
   csp_int_priority_set(DTERM_ERR_INT_REQ, DTERM_UART_INT_PRI);

   return true;
}


void bsp_dterm__interrupt_en(void)
{
   csp_int_flag_clr    (DTERM_TX_INT_REQ);

#if DTERM__BUFFERED_TX
   csp_int_en          (DTERM_TX_INT_REQ);
#else
   csp_int_dis         (DTERM_TX_INT_REQ);
#endif

   //IPC6bits.U1IP = TN_INTERRUPT_LEVEL;
   //IPC6bits.U1IS = 0;
   csp_int_flag_clr    (DTERM_RX_INT_REQ);

#if DTERM__RX_ENABLED
   csp_int_en          (DTERM_RX_INT_REQ);
#else
   csp_int_dis         (DTERM_RX_INT_REQ);
#endif

   csp_int_flag_clr    (DTERM_ERR_INT_REQ);
   //csp_int_en          (DTERM_ERR_INT);
   csp_int_dis         (DTERM_ERR_INT_REQ);
}

void bsp_dterm__interrupt_dis(void)
{
   csp_int_dis         (DTERM_TX_INT_REQ);
   csp_int_dis         (DTERM_RX_INT_REQ);
   csp_int_dis         (DTERM_ERR_INT_REQ);
}




//-- Interrupt handlers {{{



DTERM_INT_RX_HANDLER__SYS()
   //void __ISR(_UART_1_VECTOR) dterm__uart_int_handler(void)
{
#if DTERM__RX_ENABLED
   //-- is this Rx interrupt?
   if (csp_int_flag_check(DTERM_RX_INT_REQ) && csp_int_check(DTERM_RX_INT_REQ)){

      csp_int_flag_clr(DTERM_RX_INT_REQ);
      //IFS0CLR = (1 << 27);


      //-- from Errata: "The RXDA bit does not correctly reflect the RX FIFO status
      //   after an overrun event"
      //
      //   So, we check overrun flag here, and if it is set, then just clear it.
      if (csp_uart_stat_get(&DTERM_UART) & UART_STAT_ERR_OVF){
         csp_uart_stat_clr(&DTERM_UART, UART_STAT_ERR_OVF);
      }

      _dterm__isr_rx_handler();
   }
#endif

}

DTERM_INT_TX_HANDLER__SYS()
{

#if DTERM__BUFFERED_TX
   if (csp_int_flag_check(DTERM_TX_INT_REQ) && csp_int_check(DTERM_TX_INT_REQ)){
      csp_int_flag_clr(DTERM_TX_INT_REQ);

      _dterm__isr_tx_handler();
   }
#endif

}



// }}}



/***************************************************************************************************
 *  end of file
 **************************************************************************************************/


