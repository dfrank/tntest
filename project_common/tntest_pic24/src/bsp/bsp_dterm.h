/***************************************************************************************************
 *   Description:   TODO
 *
 **************************************************************************************************/

#ifndef _BSP_DTERM_H
#define _BSP_DTERM_H

/***************************************************************************************************
 *                                         INCLUDED FILES                                          *
 **************************************************************************************************/

#include "bsp_tnkernel.h"

#include "utils/xprintf.h"
#include <csp.h>


/***************************************************************************************************
 *                                          PUBLIC TYPES                                           *
 **************************************************************************************************/

/***************************************************************************************************
 *                                         GLOBAL VARIABLES                                        *
 **************************************************************************************************/

/***************************************************************************************************
 *                                           DEFINITIONS                                           *
 **************************************************************************************************/


#define  DTERM__BUFFERED_TX   1
#define  DTERM__TX_BUF_SIZE   1024   //-- Size of buffer for Tx

#define  DTERM__RX_ENABLED    1
#define  DTERM__RX_BUF_SIZE   32

/*
 * Allow recursion
 * (see detailed explanation in dterm.h)
 */
#define  DTERM__ALLOW_RECURSION  1



#define  DTERM_USER_INIT            bsp_dterm__user_init

/*
 * NOTE: these lock/unlock functions can be called from
 * task and interrupt, so, they should lock/unlock mutex
 * if only there is task context; otherwise,
 * only interrupts in which dterm is used should be
 * disabled/enabled.
 */
#define  DTERM_LOCK_PRIMARY         bsp_dterm__lock_primary
#define  DTERM_UNLOCK_PRIMARY       bsp_dterm__unlock_primary


/*
 * Secondary lock/unlock: they are called not for long time, but for critical
 * sections.
 *
 * Typically, it makes sense to just disable all interrupts here.
 * Or, you can disable/enable interrupts that aren't affected by DTERM_LOCK_PRIMARY,
 * but then some day you can forget to add some new interrupt here, which will
 * lead to bug.
 *
 *
 * These definitions are mandatory if you set DTERM__ALLOW_RECURSION to 1.
 *
 * But even if your DTERM__ALLOW_RECURSION is 0, these definitions are useful
 * to catch bug if your DTERM_LOCK_PRIMARY / DTERM_UNLOCK_PRIMARY are buggy
 * (typically, if you add dterm echo to some interrupt but forgot to 
 * disable this interrupt in DTERM_LOCK_PRIMARY)
 */
#define  DTERM_LOCK_SECONDARY       bsp_dterm__lock_secondary
#define  DTERM_UNLOCK_SECONDARY     bsp_dterm__unlock_secondary


/*
 * NOTE: this DTERM_TASK_SLEEP function can be called from task and interrupt,
 *       so, in the case of interrupt, it typically should just do nothing
 */
#define  DTERM_TASK_SLEEP           bsp_dterm_task_sleep






#define DTERM_UART                   UART2

#define DTERM_TX_INT_REQ             INT_UART2_TX
#define DTERM_RX_INT_REQ             INT_UART2_RX
#define DTERM_ERR_INT_REQ            INT_UART2_ERR

#define DTERM_UART_INT_PRI           INT_PRI_2 //TN_INTERRUPT_LEVEL

#if 0
#define DTERM_INT_TX_HANDLER__SYS()     void __attribute__((__interrupt__, auto_psv)) _U2TXInterrupt(void)
#define DTERM_INT_RX_HANDLER__SYS()     void __attribute__((__interrupt__, auto_psv)) _U2RXInterrupt(void)
#define DTERM_INT_ERR_HANDLER__SYS()    void __attribute__((__interrupt__, auto_psv)) _U2ErrInterrupt(void)
#else
#define DTERM_INT_TX_HANDLER__SYS()     tn_p24_soft_isr(_U2TXInterrupt, auto_psv)
#define DTERM_INT_RX_HANDLER__SYS()     tn_p24_soft_isr(_U2RXInterrupt, auto_psv)
#define DTERM_INT_ERR_HANDLER__SYS()    tn_p24_soft_isr(_U2ErrInterrupt, auto_psv)
#endif



#define DTERM_TX_PORT                &PORTD
#define DTERM_TX_PIN                 GPIO_2
#define DTERM_TX_PPS_FUNC            PPS_OUT_UART2_TX
#define DTERM_TX_PPS_PIN             PPS_PIN_23


#define DTERM_RX_PORT                &PORTD
#define DTERM_RX_PIN                 GPIO_3
#define DTERM_RX_PPS_FUNC            PPS_INP_UART2_RX
#define DTERM_RX_PPS_PIN             PPS_PIN_22


#if defined(__DEBUG)
#  define DTERM_DEB_HALT()            {__asm__ volatile(".pword 0xDA4000"); __asm__ volatile ("nop");}
#else
#  define DTERM_DEB_HALT()
#endif

/***************************************************************************************************
 *                                    PUBLIC FUNCTION PROTOTYPES                                   *
 **************************************************************************************************/



void bsp_dterm__user_init(void);
void bsp_dterm__lock_primary(void);
void bsp_dterm__unlock_primary(void);
void bsp_dterm__lock_secondary(void);
void bsp_dterm__unlock_secondary(void);
void bsp_dterm_task_sleep(void);


static inline size_t bsp_dterm__formatted_write(
      const char * format,
      void put_one_char(char, void *),
      void *secret_pointer,
      va_list ap
      )
{
   return _formatted_write(format, put_one_char, secret_pointer, ap);
}





//---- controller-dependent stuff --------------------------------------


void bsp_dterm__uart_on(void);
void bsp_dterm__uart_off(void);
int bsp_dterm__init(unsigned long periph_bus_clk_freq, unsigned long baudrate);
void bsp_dterm__interrupt_en(void);
void bsp_dterm__interrupt_dis(void);


static inline int bsp_dterm__is_transmitting__direct(void)
{
   return (
         (csp_uart_stat_get(&DTERM_UART) & UART_STAT_TX_SREG_EMPTY)
         ||
         !(csp_uart_conf_get(&DTERM_UART) & UART_EN)
         ) ? false : true;
}

static inline void bsp_dterm__tx_int_disable(void)
{
   csp_int_dis(DTERM_TX_INT_REQ);
}

static inline void bsp_dterm__tx_int_enable(void)
{
   csp_int_en(DTERM_TX_INT_REQ);
}

static inline int bsp_dterm__is_tx_buf_full(void)
{
   return !!(csp_uart_stat_get(&DTERM_UART) & UART_STAT_TX_BUF_FULL);
}

static inline int bsp_dterm__is_rx_buf_not_empty(void)
{
   return !!(csp_uart_stat_get(&DTERM_UART) & UART_STAT_RX_BUF_NOT_EMPTY);
}

static inline void bsp_dterm__uart_char_send(char c)
{
   csp_uart_put(&DTERM_UART, c);
}

static inline char bsp_dterm__uart_char_receive(void)
{
   return (char)csp_uart_get(&DTERM_UART);
}


#endif // _BSP_DTERM_H
/***************************************************************************************************
  end of file
 **************************************************************************************************/


