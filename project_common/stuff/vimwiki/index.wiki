
= tntest / dev =

[[used_peripheral]]
[[wiki$TRCOMP_BUNDLE__WIKI_NUM__TNKERNEL_DF_DEV_WIKI:index|tneo_dev]]

tests:

   * [ ] test BFA macros:
      * [ ] 1-bit field
      * [ ] 2 and more bit fields
      * [ ] all commands
      * [ ] TN_BFAR with lower and upper arguments swapped

   * [ ] bin folder: add readme in which is stated that tn_cfg.h with which these binaries were built is tn_cfg_default.h
   * [ ] CHECK that default configuration is actually used
   * [ ] check "isolate each function in a section" for each pre-built platform
   * [ ] check optimization for each pre-built platform
   * [X] Cortex-M port: use separate interrupt for `tworker_man__int_handler()`
   * [ ] config: try to make some runtime check that tn_cfg.h matches the one that was used for building the kernel

cortex_m:

   * [ ] set up separate interrupt for tworker_man, and make it different priority! (it seems, it's better to make sys tick interrupt higher priority)
   * [ ] move tworker_man__int_handler() to separate interrupt

pic24:

   * [X] Very strange bugs when TNT_DTMSG_COMMENT is not commented:
      * It fails to pass timers test. But, if we comment out TNT_TEST_COMMENT, and comment out timers test, all the rest test pass.
      * If TNT_TEST_COMMENT is not commented out, the whole log becomes crap: logger shows garbage. 
   * [X] Separate interrupt stack
      * [X] Implement
      * [X] Check nested interrupts
   * [X] TN_IPL is specified twice
   * [X] probably make pic24 interrupt customizable (now, it is always INT0)
   * [X] interrupts doc page: remove some details that are pic32-specific, move them to pic32 details page.
   * [X] implement tn_arch_int_en()
   * [X] tn_arch_pic24_c.c : non-atomic way to set INT0IP
   * [X] make returning from task function calling tn_task_exit()
   * [X] pic32: handle CS0 by the kernel.
   * [X] warnings when building fmp: size of pointer isn't equal to size of unsigned long
   * [X] specify MCU on which the kernel is tested
   * [X] rename pic32 project to tneo_pic32?
   * [X] check other PIC24F CPUs in q-kernel (check `__HAS_EDS__` macro)
   * [X] examples: clean basic example up, create other examples for pic24.
   * [X] create_version_archive.sh : add pic24/dspic binary
   * [X] pic24: add separate project for chips with EDC
   * [ ] run in simulator
   * [ ] strange bug: if tntest_pic24 is built with XC16 v1.23, timers test is passed first time, but failed second time!
   * [ ] _tn_arch_stack_init: take CORCON from real CORCON value
   * [ ] add support of MODCON and the like


